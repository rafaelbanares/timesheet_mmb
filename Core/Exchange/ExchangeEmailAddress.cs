﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Exchange
{
    [Serializable]
    public class ExchangeEmailAddress
    {
        public string Address{get;set;}
        public string Name{get;set;}
    }
}
