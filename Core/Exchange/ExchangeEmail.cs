﻿using System.Collections.Generic;
using System.Net.Mail;
using System;

namespace Core.Exchange
{
    [Serializable]
    public class ExchangeEmail : ExchangeItem
    {
        public ExchangeEmailAddress Sender { get; set; }
        public ExchangeEmailAddress From { get; set; }
        public IEnumerable<ExchangeEmailAddress> To { get; set; }
        public IEnumerable<ExchangeEmailAddress> CC { get; set; }
        public IEnumerable<ExchangeEmailAddress> BCC { get; set; }
    }

}
