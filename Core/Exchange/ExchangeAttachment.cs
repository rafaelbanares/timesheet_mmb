﻿
using System;
namespace Core.Exchange
{

    [Serializable]
    public abstract class ExchangeAttachment
    {
        public string Name { get; set; }
    }

    [Serializable]
    public class ExchangeFileAttachment : ExchangeAttachment
    {
        public byte[] Data { get; set; }
    }

    [Serializable]
    public class ExchangeItemAttachment : ExchangeAttachment
    {
        public string Id { get; set; }
    }
}
