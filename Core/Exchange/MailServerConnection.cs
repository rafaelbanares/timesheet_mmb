﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Exchange
{
    public class MailServerConnection
    {
        public MailServerConnection(string login, string password)
        {
            Login = login;
            Password = password;
        }

        private string Login;
        private string Password;
        private const string WebServiceUrl = "https://[put url here].asmx";


        public List<ExchangeEmail> GetUnreadEmails()
        {
            return ExchangeHelper.GetUnreadEmails(WebServiceUrl, Login, Password, 10);
        }

        public List<ExchangeEmail> GetUnreadEmails(int itemCount)
        {
            return ExchangeHelper.GetUnreadEmails(WebServiceUrl, Login, Password, itemCount);
        }

        public void Reply(string itemId, string message)
        {
            ExchangeHelper.Reply(WebServiceUrl, Login, Password, itemId, message);
        }

        public void Forward(string itemId, string to, string message)
        {
            ExchangeHelper.Forward(WebServiceUrl, Login, Password, itemId, to, message);
        }

        public void MarkAsRead(string itemId)
        {
            ExchangeHelper.MarkAsRead(WebServiceUrl, Login, Password, itemId);
        }

        public void Delete(IEnumerable<string> itemIds)
        {
            ExchangeHelper.Delete(WebServiceUrl, Login, Password, itemIds);
        }

        public void Send(ExchangeEmail email)
        {
            ExchangeHelper.Send(WebServiceUrl, Login, Password, email);
        }

        public void Delete(string itemId)
        {
            ExchangeHelper.Delete(WebServiceUrl, Login, Password, itemId);
        }
    }
}
