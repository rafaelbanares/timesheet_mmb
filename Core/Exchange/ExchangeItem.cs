﻿using System;
using System.Collections.Generic;

namespace Core.Exchange
{
    [Serializable]
    public abstract class ExchangeItem
    {
        public string Id { get; internal set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ReceivedOn { get; set; }
        public DateTime SentOn { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtmlBody { get; set; }
        public IEnumerable<string> Categories { get; set; }
        public bool IsRead { get; set; }
        public IEnumerable<ExchangeAttachment> Attachments { get; set; }
        public IDictionary<string, string> Headers { get; set; }
    }
}
