﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers
{
    public static class TypeHelper
    {
        public static Type GetType(string typeName)
        {
            System.Type type = System.Type.GetType(typeName);

            if (type == null)
            {
                var types = from assembly in System.AppDomain.CurrentDomain.GetAssemblies()
                            from assemblyType in assembly.GetTypes()
                            where assemblyType.FullName == typeName
                            select assemblyType;

                type = types.FirstOrDefault();
            }

            return type;
        }

        public static IEnumerable<Type> GetTypes(Func<Type, bool> where)
        {
            return System.AppDomain.CurrentDomain.GetAssemblies()
                 .SelectMany(x => x.GetTypes())
                 .Where(where);
        }
    }
}
