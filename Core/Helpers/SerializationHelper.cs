using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;
using System.Reflection;

namespace Core
{
    /// <summary>
    /// Helper class exposing static methods to serialize objects.
    /// </summary>
    public static class SerializationHelper
    {
        public static void FileSerialize(Object obj, string filePath)
        {
            FileStream fileStream = null;
            try
            {
                fileStream = new FileStream(filePath, FileMode.Create);
                BinaryFormatter b = new BinaryFormatter();
                b.Serialize(fileStream, obj);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
            }
        }
         
        public static T FileDeSerialize<T>(string filePath)
        {
            FileStream fileStream = null;
            Object obj;
            try
            {
                if (File.Exists(filePath) == false) throw new FileNotFoundException("The file" + " was not found.", filePath);
                fileStream = new FileStream(filePath, FileMode.Open);
                BinaryFormatter b = new BinaryFormatter();
                obj = b.Deserialize(fileStream);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
            }
            return (T)obj;
        }
        
        /// <summary>
        /// Serializes a given <paramref name="obj"/> to bytes.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>An array of bytes representing the object serialized.</returns>
        public static byte[] SerializeToBytes(object obj)
        {
            try
            {
                if (obj == null) return null;
                //SoapFormatter formatter = new SoapFormatter();
                var formatter = new BinaryFormatter();
                var stream = new MemoryStream();
                formatter.Serialize(stream, obj);
                var buffer = stream.ToArray();
                stream.Close();
                return buffer;
            }
            catch (Exception e)
            {
                throw new SerializationException("Error while serializing object to bytes " + obj + " : " + e.Message, e);
            }
        }

        /// <summary>
        /// Unserializes an object from an array of bytes.
        /// </summary>
        /// <param name="buffer">The buffer representing the object serialized.</param>
        /// <returns>The object.</returns>
        public static object UnserializeFromBytes(byte[] buffer)
        {
            try
            {
                if (buffer == null) return null;
                //SoapFormatter formatter = new SoapFormatter();
                var formatter = new BinaryFormatter();

                var stream = new MemoryStream();
                stream.Write(buffer, 0, buffer.Length);
                stream.Position = 0;
                var result = formatter.Deserialize(stream);
                return result;
            }
            catch (Exception e)
            {
                throw new SerializationException("Error while unserializing from bytes : " + e.Message, e);
            }
        }

        /// <summary>
        /// Serializes the given object in a <see cref="System.String"/> representation.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>An <see cref="System.String"/> representing the object serialized.</returns>
        public static string Serialize(object obj)
        {
            var coder = new ASCIIEncoding();
            var serializedBytes = SerializeToBytes(obj);
            return coder.GetString(serializedBytes);
        }

        /// <summary>
        /// Unserializes an object from a <see cref="System.String"/>.
        /// </summary>
        /// <param name="str">The serialized <see cref="System.String"/>.</param>
        /// <returns>The object.</returns>
        public static object Unserialize(string str)
        {
            var decoder = new ASCIIEncoding();
            var serializedBytes = decoder.GetBytes(str);
            return UnserializeFromBytes(serializedBytes);
        }

        /// <summary>
        /// Serializes a given <paramref name="objToSerialize"/> and write the result
        /// in the <paramref name="filePath"/> file.
        /// </summary>
        /// <param name="objToSerialize">The object to serialize.</param>
        /// <param name="filePath">Name of the output file.</param>
        public static void BinarySerializeToDisk(object objToSerialize, string fileName)
        {
            FileHelper.WriteBinary(fileName, SerializeToBytes(objToSerialize));
        }

        /// <summary>
        /// Deserializes from disk.
        /// </summary>
        /// <param name="filePath">Name of the file containing the serialized object.</param>
        /// <returns>The object.</returns>
        public static object BinaryDeserializeFromDisk(string fileName)
        {
            if (!File.Exists(fileName)) return null;
            return UnserializeFromBytes(FileHelper.ReadBinary(fileName));
        }

        /// <summary>
        /// This method serialize an object in Xml format, at specified path.
        /// </summary>
        /// <param name="objToSerialize">The concreteFeatureType of the object to serialize.</param>
        /// <param name="filePath">The file name with the path to write to : ex 'myfile.xml' or 'c:\frameworkDatabase\myFile.xml'.</param>
        public static void XmlSerializeToDisk(object objToSerialize, string fileName)
        {
            var t = objToSerialize.GetType();
            var serializer = new XmlSerializer(t);
            TextWriter writer = new StreamWriter(fileName);

            // Serialize the object, and close the TextWriter.
            serializer.Serialize(writer, objToSerialize);
            writer.Close();
        }


        /// <summary>
        /// This method deserialize an object in Xml format, from the specified path. 
        /// </summary>
        /// <param name="t">The type of the object to deserialize</param>
        /// <param name="filePath">The file name with the path to read from : ex 'myfile.xml' or 'c:\frameworkDatabase\myFile.xml'.</param>
        /// <returns>Returns an object from the deserialization.</returns>
        public static object XmlDeserializeFromDisk(Type t, string fileName)
        {
            if (!File.Exists(fileName)) return null;
            var serializer = new XmlSerializer(t);
            TextReader reader = new StreamReader(fileName);

            // Deserialize the object, and close the TextWriter.
            var o = serializer.Deserialize(reader);
            reader.Close();
            return o;
        }

        [Serializable]
        public class SerializationAdapter : ISerializable
        {
            [NonSerialized]
            private object obj;
            public SerializationAdapter(Object o)
            {
                obj = o;
            }

            private SerializationAdapter(SerializationInfo info, StreamingContext context)
            {
                obj = Assembly.GetExecutingAssembly().CreateInstance(info.GetString("class"));
                PropertyInfo[] allp = obj.GetType().GetProperties();
                foreach (PropertyInfo p in allp)
                {
                    if (p.PropertyType == typeof(int))
                    {
                        p.SetValue(obj, info.GetInt32(p.Name), null);
                    }
                    else if (p.PropertyType == typeof(double))
                    {
                        p.SetValue(obj, info.GetDouble(p.Name), null);
                    }
                    else if (p.PropertyType == typeof(DateTime))
                    {
                        p.SetValue(obj, info.GetDateTime(p.Name), null);
                    }
                    else if (p.PropertyType == typeof(Decimal))
                    {
                        p.SetValue(obj, info.GetDecimal(p.Name), null);
                    }
                    else if (p.PropertyType == typeof(Boolean))
                    {
                        p.SetValue(obj, info.GetBoolean(p.Name), null);
                    }
                    else if (p.PropertyType == typeof(string))
                    {
                        p.SetValue(obj, info.GetString(p.Name), null);
                    }
                    else
                    {
                        // ignore for now
                    }
                }
            }

            public void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                //Debug.WriteLine("class: " + obj.GetType().FullName);
                info.AddValue("class", obj.GetType().FullName);
                PropertyInfo[] allp = obj.GetType().GetProperties();
                foreach (PropertyInfo p in allp)
                {
                    //Debug.WriteLine("prop: " + p.Name);
                    info.AddValue(p.Name, p.GetValue(obj, null));
                }
            }

            public object Obj
            {
                get
                {
                    return obj;
                }
            }
        }
    }
}