﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Globalization;
using System.Text;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.ComponentModel.DataAnnotations;
using System.Management;
using System.Diagnostics;

namespace Core
{
    /// <summary>
    /// Represents an Active Directory user for search result.
    /// </summary>
    [Serializable]
    public class ActiveDirectoryUserSearchResult
    {
        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>The path.</value>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the full login.
        /// </summary>
        /// <value>The full login.</value>
        public string FullLogin { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets the old style domain.
        /// </summary>
        /// <value>The old style domain.</value>
        public string OldStyleDomain
        {
            get
            {
                return Path.Split(",").Where(x => x.StartsWith("DC=")).First().Replace("DC=", "");
                //if (FullLogin.IsFilled() && FullLogin.Contains("@")) return FullLogin.Split('@')[1].Split('.')[0];
                //return FullLogin;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this concreteFeatureType.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this concreteFeatureType.
        /// </returns>
        public override string ToString()
        {
            var res = "DisplayName: " + DisplayName + Environment.NewLine
                      + "Email: " + Email + Environment.NewLine
                      + "Phone: " + Phone + Environment.NewLine
                      + "Path: " + Path + Environment.NewLine
                      + Environment.NewLine;

            return res;
        }

        /// <summary>
        /// Converts the current <c>ADUserSearchResult</c> object to a <c>ADUser</c>
        /// object.
        /// </summary>
        /// <returns>
        /// A <c>ADUser</c> object builds from the current <c>ADUserSearchResult</c>
        /// object.
        /// </returns>
        public ActiveDirectoryUser ToAdUser(string adUserName, string adPassword)
        {
            return ActiveDirectoryHelper.GetUser(Path, adUserName, adPassword);
        }
    }

    /// <summary>
    /// Represents an Active Directory user.
    /// </summary>
    [Serializable]
    public class ActiveDirectoryUser : ActiveDirectoryUserSearchResult
    {
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the GUID.
        /// </summary>
        /// <value>The GUID.</value>
        /// 
        [Key]
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        /// <value>The login.</value>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>The domain.</value>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>The company.</value>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        /// <value>The department.</value>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        /// <value>The job title.</value>
        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the mobile phone.
        /// </summary>
        /// <value>The mobile phone.</value>
        public string MobilePhone { get; set; }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The fax.</value>
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        /// <value>The zip code.</value>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        public string CountryCode { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this concreteFeatureType.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this concreteFeatureType.
        /// </returns>
        public override string ToString()
        {
            var res = "DisplayName: " + DisplayName + Environment.NewLine
                      + "Description: " + Description + Environment.NewLine
                      + Environment.NewLine
                      + "OldStyleDomain: " + OldStyleDomain + Environment.NewLine
                      + "Login: " + Login + Environment.NewLine
                      + "FullLogin: " + FullLogin + Environment.NewLine
                      + Environment.NewLine
                      + "FirstName: " + FirstName + Environment.NewLine
                      + "LastName: " + LastName + Environment.NewLine
                      + Environment.NewLine
                      + "Company: " + Company + Environment.NewLine
                      + "Department: " + Department + Environment.NewLine
                      + "JobTitle: " + JobTitle + Environment.NewLine
                      + Environment.NewLine
                      + "Email: " + Email + Environment.NewLine
                      + "Phone: " + Phone + Environment.NewLine
                      + "MobilePhone: " + MobilePhone + Environment.NewLine
                      + "Fax: " + Fax + Environment.NewLine
                      + Environment.NewLine
                      + "Address: " + Address + Environment.NewLine
                      + "ZipCode: " + ZipCode + Environment.NewLine
                      + "City: " + City + Environment.NewLine
                      + "State: " + State + Environment.NewLine
                      + "Country: " + Country + Environment.NewLine
                      + "CountryCode: " + CountryCode + Environment.NewLine
                      + Environment.NewLine;

            return res;
        }
    }

    /// <summary>
    /// Active Directory helper class.
    /// </summary>
    public static class ActiveDirectoryHelper
    {
        /// <summary>
        /// Generates a new login from the given <paramref name="firstName"/> and
        /// <paramref name="lastName"/>.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents the login name generated from
        /// the <paramref name="firstName"/> and <paramref name="lastName"/> parameters.
        /// </returns>
        public static string GenerateLogin(string firstName, string lastName)
        {
            firstName.CanNotBeEmpty();
            lastName.CanNotBeEmpty();

            var result = firstName + " " + lastName;

            result = result.Normalize(NormalizationForm.FormD);

            result = result.ToLowerInvariant();
            result = result.Replace(" ", "_");
            result = result.Replace("'", "");
            result = result.Replace("́", "");
            result = result.Replace(",", "");
            result = result.Replace("/", "");
            result = result.Replace("\\", "");
            result = result.Replace("<", "");
            result = result.Replace(">", "");
            result = result.Replace("~", "");
            result = result.Replace("?", "");
            result = result.Replace("!", "");
            result = result.Replace("*", "");

            var sb = new StringBuilder();

            foreach (var t in result)
            {
                var uc = CharUnicodeInfo.GetUnicodeCategory(t);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(t);
                }
            }

            result = (sb.ToString().Normalize(NormalizationForm.FormC));

            return result;
        }

        /// <summary>
        /// Sets the password.
        /// </summary>
        /// <param name="adminLogin">The admin login.</param>
        /// <param name="adminPassword">The admin password.</param>
        /// <param name="userPath">The user path.</param>
        /// <param name="password">The password.</param>
        public static void SetPassword(string adminLogin
                                       , string adminPassword
                                       , string userPath
                                       , string password
            )
        {
            using (var user = new DirectoryEntry(userPath))
            {
                user.Username = adminLogin;
                user.Password = adminPassword;
                user.Invoke("SetPassword", new object[] { password });
                user.CommitChanges();
            }
        }

        /// <summary>
        /// Enables the user specified by the <paramref name="userPath"/> parameter.
        /// </summary>
        /// <param name="adminLogin">The admin login.</param>
        /// <param name="adminPassword">The admin password.</param>
        /// <param name="userPath">The user path.</param>
        public static void Enable(string adminLogin
                                  , string adminPassword
                                  , string userPath
            )
        {
            using (var user = new DirectoryEntry(userPath))
            {
                user.Username = adminLogin;
                user.Password = adminPassword;
                var val = (int)user.Properties["userAccountControl"].Value;
                user.Properties["userAccountControl"].Value = val & ~0x2;
                user.CommitChanges();
            }
        }

        /// <summary>
        /// Disables the user specified by the <paramref name="userPath"/> parameter.
        /// </summary>
        /// <param name="adminLogin">The admin login.</param>
        /// <param name="adminPassword">The admin password.</param>
        /// <param name="userPath">The user path.</param>
        public static void Disable(string adminLogin
                                   , string adminPassword
                                   , string userPath
            )
        {
            using (var user = new DirectoryEntry(userPath))
            {
                user.Username = adminLogin;
                user.Password = adminPassword;
                var val = (int)user.Properties["userAccountControl"].Value;
                user.Properties["userAccountControl"].Value = val | ~0x2;
                user.CommitChanges();
            }
        }

        /// <summary>
        /// Creates a user in Active Directory.
        /// </summary>
        /// <param name="adminLogin">The admin login.</param>
        /// <param name="adminPassword">The admin password.</param>
        /// <param name="organisationUnitPath">The organisation unit path.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="login">The login.</param>
        /// <param name="password">The password.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="description">The description.</param>
        /// <param name="email">The email.</param>
        /// <param name="organisation">The organisation.</param>
        /// <param name="department">The department.</param>
        /// <param name="jobTitle">The job title.</param>
        /// <param name="phone">The phone.</param>
        /// <param name="mobilePhone">The mobile phone.</param>
        /// <param name="fax">The fax.</param>
        /// <param name="address">The address.</param>
        /// <param name="zipCode">The zip code.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="country">The country.</param>
        /// <returns>An <see cref="ActiveDirectoryUser"/> that represents the created user.</returns>
        public static ActiveDirectoryUser CreateUser(
            string adminLogin
            , string adminPassword
            , string organisationUnitPath
            , string domain
            , string login
            , string password
            , string firstName
            , string lastName
            , string displayName
            , string description
            , string email
            , string organisation
            , string department
            , string jobTitle
            , string phone
            , string mobilePhone
            , string fax
            , string address
            , string zipCode
            , string city
            , string state
            , string countryCode
            , string country
            )
        {
            domain.CanNotBeEmpty();
            login.CanNotBeEmpty();
            password.CanNotBeEmpty();
            firstName.CanNotBeEmpty();
            lastName.CanNotBeEmpty();
            displayName.CanNotBeEmpty();
            email.CanNotBeEmpty();

            string objectPath;

            using (var dirEntry = new DirectoryEntry(organisationUnitPath, adminLogin, adminPassword))
            {
                var newUser = dirEntry.Children.Add("CN=" + login, "user");
                newUser.Properties["userPrincipalName"].Value = login + "@" + domain;
                newUser.Properties["samAccountName"].Value = login;

                newUser.Properties["givenName"].Value = firstName;
                newUser.Properties["sn"].Value = lastName;
                newUser.Properties["displayName"].Value = displayName;
                if (description.IsFilled()) newUser.Properties["description"].Value = description;

                newUser.Properties["mail"].Value = email;
                if (organisation.IsFilled()) newUser.Properties["company"].Value = organisation;
                if (department.IsFilled()) newUser.Properties["department"].Value = department;
                if (jobTitle.IsFilled()) newUser.Properties["title"].Value = jobTitle;

                if (phone.IsFilled()) newUser.Properties["telephoneNumber"].Value = phone;
                if (mobilePhone.IsFilled()) newUser.Properties["mobile"].Value = mobilePhone;
                if (fax.IsFilled()) newUser.Properties["facsimileTelephoneNumber"].Value = fax;

                if (address.IsFilled()) newUser.Properties["streetAddress"].Value = address;
                if (zipCode.IsFilled()) newUser.Properties["postalCode"].Value = zipCode;
                if (city.IsFilled()) newUser.Properties["l"].Value = city;
                if (state.IsFilled()) newUser.Properties["st"].Value = state;
                if (countryCode.IsFilled()) newUser.Properties["c"].Value = countryCode;
                if (country.IsFilled()) newUser.Properties["co"].Value = country;

                newUser.CommitChanges();

                objectPath = newUser.Path;

                newUser.Invoke("SetPassword", new object[] { password });
                newUser.CommitChanges();


                //UF_DONT_EXPIRE_PASSWD 0x10000
                var exp = (int)newUser.Properties["userAccountControl"].Value;
                newUser.Properties["userAccountControl"].Value = exp | 0x0001;
                newUser.CommitChanges();

                //UF_ACCOUNTDISABLE 0x0002
                var val = (int)newUser.Properties["userAccountControl"].Value;
                newUser.Properties["userAccountControl"].Value = val & ~0x0002;
                newUser.CommitChanges();


                newUser.Close();
            }


            return (objectPath != null) ? GetUser(objectPath, adminLogin, adminPassword) : null;
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userPath">The user path.</param>
        /// <param name="adUserName">The user name to use when authenticating to Active Directory.</param>
        /// <param name="adPassword">The password to use when authenticating to Active Directory.</param>
        /// <returns></returns>
        internal static ActiveDirectoryUser GetUser(string userPath, string adUserName, string adPassword)
        {
            using (var userEntry = new DirectoryEntry(userPath, adUserName, adPassword))
            {
                userEntry.AuthenticationType = AuthenticationTypes.Secure | AuthenticationTypes.FastBind |
                                               AuthenticationTypes.ReadonlyServer;
                var ad = new ActiveDirectoryUser
                           {
                               Guid = new Guid(userEntry.NativeGuid),
                               Path = userEntry.Path,
                               FullLogin = ReadProperty(userEntry, "userPrincipalName"),
                               Login = ReadProperty(userEntry, "sAMAccountName"),
                               FirstName = ReadProperty(userEntry, "givenName"),
                               LastName = ReadProperty(userEntry, "sn"),
                               DisplayName = ReadProperty(userEntry, "displayName"),
                               Description = ReadProperty(userEntry, "description"),
                               Email = ReadProperty(userEntry, "mail"),
                               Company = ReadProperty(userEntry, "company"),
                               Department = ReadProperty(userEntry, "department"),
                               JobTitle = ReadProperty(userEntry, "title"),
                               Phone = ReadProperty(userEntry, "telephoneNumber"),
                               MobilePhone = ReadProperty(userEntry, "mobile"),
                               Fax = ReadProperty(userEntry, "facsimileTelephoneNumber"),
                               Address = ReadProperty(userEntry, "streetAddress"),
                               ZipCode = ReadProperty(userEntry, "postalCode"),
                               City = ReadProperty(userEntry, "l"),
                               State = ReadProperty(userEntry, "st"),
                               Country = ReadProperty(userEntry, "co"),
                               CountryCode = ReadProperty(userEntry, "c"),
                           };

                ad.Domain = ad.OldStyleDomain;
                ad.UserName = ad.OldStyleDomain + @"\" + ad.Login;
                return ad;
            }
        }

        private static string ReadProperty(DirectoryEntry entry, string propertyName)
        {
            return entry.Properties.Contains(propertyName) ? entry.Properties[propertyName].Value.ToString() : "";
        }

        private static List<ActiveDirectoryUserSearchResult> LoadResults(DirectorySearcher searcher)
        {
            var result = new List<ActiveDirectoryUserSearchResult>();
            searcher.CacheResults = false;
            searcher.PageSize = 1000;
            searcher.PropertiesToLoad.Clear();
            searcher.PropertiesToLoad.Add("name");
            searcher.PropertiesToLoad.Add("mail");
            searcher.PropertiesToLoad.Add("telephoneNumber");
            searcher.PropertiesToLoad.Add("userPrincipalName");


            foreach (SearchResult adRes in searcher.FindAll())
            {
                string path;
                string displayName;
                string email;
                string telephone;
                string fullLogin;
                try
                {
                    path = adRes.Path;
                }
                catch
                {
                    path = "";
                }
                try
                {
                    displayName = adRes.Properties["name"][0].ToString();
                }
                catch
                {
                    displayName = "";
                }
                try
                {
                    email = adRes.Properties["mail"][0].ToString();
                }
                catch
                {
                    email = "";
                }
                try
                {
                    telephone = adRes.Properties["telephoneNumber"][0].ToString();
                }
                catch
                {
                    telephone = "";
                }
                try
                {
                    fullLogin = adRes.Properties["userPrincipalName"][0].ToString();
                }
                catch
                {
                    fullLogin = "";
                }
                result.Add(
                    new ActiveDirectoryUserSearchResult
                    {
                        Path = path,
                        DisplayName = displayName,
                        Email = email,
                        Phone = telephone,
                        FullLogin = fullLogin
                    });
            }
            return result;
        }

        /// <summary>
        /// Checks if the <paramref name="loginToCreate"/> already exist.
        /// </summary>
        /// <param name="forestName">Name of the forest.</param>
        /// <param name="loginToCreate">The login to create.</param>
        /// <returns><c>true</c> if the login already exists in Active Directory; otherwise <c>false</c>.</returns>
        public static bool LoginExists(string forestName, string loginToCreate, string adUserName, string adPassword)
        {
            var currentForest = Forest.GetForest(new DirectoryContext(DirectoryContextType.Forest, forestName, adUserName, adPassword));
            foreach (Domain d in currentForest.Domains)
            {
                using (var search = d.FindDomainController().GetDirectorySearcher())
                {
                    search.Filter = String.Format("(SAMAccountName={0})", loginToCreate);
                    search.PropertiesToLoad.Add("cn");
                    var result = search.FindOne();
                    if (result != null) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the users list matching with the <paramref name="searchedName"/>.
        /// </summary>
        /// <param name="forestName">Name of the forest.</param>
        /// <param name="searchedName">The searched name.</param>
        /// <returns></returns>
        public static List<ActiveDirectoryUserSearchResult> GetUsersList(string forestName, string searchedName, string adUserName, string adPassword)
        {
            var filter = "(&(objectCategory=person)(objectClass=user)(name=*" + searchedName + "*))";

            List<ActiveDirectoryUserSearchResult> result = null;

            var ctx = new DirectoryContext(DirectoryContextType.Forest, forestName, adUserName, adPassword);
            var gc = GlobalCatalog.FindOne(ctx);

            using (var searcher = gc.GetDirectorySearcher())
            {
                searcher.Filter = filter;
                result = LoadResults(searcher);
            }

            return result;
        }

        /// <summary>
        /// Gets the users list matching with the <paramref name="searchedName"/>.
        /// </summary>
        /// <param name="forestName">Name of the forest.</param>
        /// <param name="searchedName">The searched name.</param>
        /// <returns></returns>
        public static List<ActiveDirectoryUserSearchResult> GetUsersListInDomain(string domainName, string searchedName, string adUserName, string adPassword)
        {
            var filter = "(&(objectCategory=person)(objectClass=user)(|(name=*" + searchedName + "*)(mail=*" + searchedName + "*)))";

            List<ActiveDirectoryUserSearchResult> result = null;

            var ctx = new DirectoryContext(DirectoryContextType.Domain, domainName, adUserName, adPassword);

            var gc = DomainController.FindOne(ctx);

            using (var searcher = gc.GetDirectorySearcher())
            {
                searcher.Filter = filter;
                result = LoadResults(searcher);
            }

            return result;
        }


        static ActiveDirectoryHelper()
        {
            ////Debug.WriteLine("Active Directory Initialization....");

            //Debug.Indent();

            ////Debug.WriteLine(string.Format("Computer name : {0}", Environment.MachineName));
            ////Debug.WriteLine(string.Format("Computer domain : {0}", Domain.GetComputerDomain().Name));
            //var forest = Forest.GetCurrentForest();
            ////Debug.WriteLine(string.Format("Current forest is {0}", forest.Name));
            //Debug.Indent();
            //foreach (Domain domain in forest.Domains)
            //{
            //    //Debug.WriteLine(string.Format("Found domain {0}", domain.Name));
            //}
            //Debug.Unindent();
            //Debug.Unindent();
        }

        static Dictionary<string, ActiveDirectoryUser> _localCache = new Dictionary<string, ActiveDirectoryUser>();
        static object locker = new object();

        public static ActiveDirectoryUser GetUserByLogin(string domainName, string searchedLogin, string adUserName, string adPassword)
        {
            ActiveDirectoryUser result = null;

            var userName = domainName + @"\" + searchedLogin;
            //Debug.WriteLine(string.Format("GetUserByLogin {0}", userName));

            lock (locker)
            {
                if (_localCache.ContainsKey(userName))
                {
                    //Debug.WriteLine("retrieved from cache");
                    result = _localCache[userName];
                }
            }

            if (result != null) return result;

            var filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" + searchedLogin + "))";

            List<ActiveDirectoryUserSearchResult> adSearchResult = null;

            try
            {
                var ctx = new DirectoryContext(DirectoryContextType.Domain, domainName, adUserName, adPassword);

                var dc = DomainController.FindOne(ctx);

                using (var searcher = dc.GetDirectorySearcher())
                {
                    searcher.Filter = filter;
                    adSearchResult = LoadResults(searcher);
                }

                if (adSearchResult.Count > 0)
                {
                    //Debug.WriteLine(string.Format("User {0} found in Context={1} ", searchedLogin, ctx.Name));
                    //Debug.WriteLine(adSearchResult[0].FullLogin);
                    //Debug.WriteLine(adSearchResult[0].DisplayName);

                    result = adSearchResult[0].ToAdUser(adUserName, adPassword);

                    result.Domain = domainName;

                    result.UserName = result.Domain + @"\" + result.Login;
                    lock (locker)
                    {
                        if (!_localCache.ContainsKey(result.UserName))
                        {
                            //Debug.WriteLine(result.UserName + " added to cache");
                            _localCache.Add(result.UserName, result);
                        }
                    }
                    return result;
                }

                //Debug.WriteLine(string.Format("User {0} NOT found in Context={1} ", searchedLogin, ctx.Name));

            }
            catch (ActiveDirectoryObjectNotFoundException x)
            {
                x.Log("Active Directory", x.Message);
                throw x;
            }
            catch (Exception x)
            {
                x.Log("Active Directory");
                throw x;
            }

            return null;
        }

        public static bool CreateLocalAdminWindowsAccount(string userName, string password, string displayName, string description)
        {
            try
            {
                using (PrincipalContext context = new PrincipalContext(ContextType.Machine))
                {
                    // User exists ?
                    UserPrincipal up = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);
                    if (up == null)
                    {
                        UserPrincipal user = new UserPrincipal(context);
                        user.SetPassword(password);
                        user.DisplayName = displayName;
                        user.Name = userName;
                        user.Description = description;
                        user.UserCannotChangePassword = true;
                        user.PasswordNeverExpires = true;
                        user.Save();

                        up = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                        AddToLocalGroup(context, System.Security.Principal.WellKnownSidType.BuiltinUsersSid, userName);
                        AddToLocalGroup(context, System.Security.Principal.WellKnownSidType.BuiltinAdministratorsSid, userName);

                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private static void AddToLocalGroup(PrincipalContext ctx, System.Security.Principal.WellKnownSidType wellknownGroupSid, string userName)
        {
            var groupSid = new System.Security.Principal.SecurityIdentifier(wellknownGroupSid, null);
            string groupName = null;
            using (GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, IdentityType.Sid, groupSid.Value))
            {
                groupName = group.SamAccountName;
            }

            DirectoryEntry localMachine = new DirectoryEntry("WinNT://" + Environment.MachineName + ", Computer");
            DirectoryEntry admGroup = localMachine.Children.Find(groupName, "group");
            DirectoryEntry usr = localMachine.Children.Find(userName, "User");

            if (admGroup != null && usr != null)
            {
                admGroup.Invoke("Add", new Object[] { usr.Path });
                admGroup.CommitChanges();
            }
        }
    }
}