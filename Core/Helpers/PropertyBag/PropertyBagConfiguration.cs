﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Core.Helpers.PropertyBag
{
    [Serializable]
    public class PropertyBagConfigurationProperty
    {
        public PropertyBagConfigurationProperty() { }

        public string key;
        public string category;
        public object value;
        public PropertySpecType type;
        public string description;
    }

    [Serializable]
    public class PropertyBagConfiguration : Dictionary<string, PropertyBagConfigurationProperty>
    {
        public PropertyBagConfiguration() { }

        public static PropertyBagConfiguration Read(string location)
        {
            if (location == null) return (PropertyBagConfiguration)ConfigurationManager.GetSection("PropertiesSections");
            else return PropertiesSectionsHandler.CreateFromFile(location);
        }
    }
}
