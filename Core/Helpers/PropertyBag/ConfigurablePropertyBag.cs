﻿
namespace Core.Helpers.PropertyBag
{
    public class ConfigurablePropertyBag : PropertyBag
    {
        private PropertyBagConfiguration _config = null;

        public ConfigurablePropertyBag()
            : base()
        {
            Reset(null);
        }

        public ConfigurablePropertyBag(string location)
            : base()
        {
            Reset(location);
        }

        public PropertyBagConfigurationProperty this[string key]
        {
            get { return _config[key]; }
            set { _config[key] = value; }
        }

        public PropertyBagConfiguration Config
        {
            get { return _config; }
            set 
            {
                _config = value;
                SetPropertySpecs();
            }
        }

        public void Reset(string location)
        {
            Config = PropertyBagConfiguration.Read(location);
        }

        private void SetPropertySpecs()
        {
            this.Properties.Clear();
            foreach (PropertyBagConfigurationProperty prop in _config.Values)
            {
                PropertySpec spec = new PropertySpec(prop.key, prop.type);
                spec.Category = prop.category;
                spec.DefaultValue = prop.value;
                spec.Description = prop.description;
                this.Properties.Add(spec);
            }
        }

        protected override void OnGetValue(PropertySpecEventArgs e)
        {
            e.Value = _config[e.Property.Name].value;
        }

        protected override void OnSetValue(PropertySpecEventArgs e)
        {
            _config[e.Property.Name].value = e.Value;
        }
    }

}
