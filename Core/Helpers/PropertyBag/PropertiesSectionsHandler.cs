﻿using System.Configuration;
using System.Xml;

namespace Core.Helpers.PropertyBag
{
    public class PropertiesSectionsHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            return ReadFromConfigs(section);
        }

        public static PropertyBagConfiguration CreateFromFile(string filePath)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            settings.IgnoreWhitespace = true;
            XmlNode node = null;
            using (XmlReader reader = XmlReader.Create(filePath, settings))
            {
                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = false;
                node = doc.ReadNode(reader);
                while (node.Name != "PropertiesSections") 
                    node = doc.ReadNode(reader);
            }

            return ReadFromConfigs(node);
        }

        private static PropertyBagConfiguration ReadFromConfigs(XmlNode section)
        {
            PropertyBagConfiguration result = null;
            string currentconfigName = section.GetValue<string>("currentconfig");
            foreach (XmlNode childNode in section.ChildNodes)
            {
                string configName = childNode.GetValue<string>("name", true);
                if (currentconfigName == configName)
                {
                    result = ReadConfig(childNode);
                    break;
                }
            }
            return result;
        }

        private static PropertyBagConfiguration ReadConfig(XmlNode section)
        {
            PropertyBagConfiguration result = new PropertyBagConfiguration();
            foreach (XmlNode childNode in section.ChildNodes)
            {
                PropertyBagConfigurationProperty property = new PropertyBagConfigurationProperty();
                property.category = childNode.GetValue<string>("category", true);
                property.key = childNode.GetValue<string>("key", true);
                property.type = childNode.GetValue<PropertySpecType>("type", true);
                property.value = childNode.GetValue<string>("value", false);
                property.description = childNode.GetValue<string>("description", false);
                result.Add(property.key, property);
            }
            return result;
        }


    }
}
