﻿using System.Collections.Generic;


namespace Core.Helpers.PropertyBag
{
    public class PropertyTable : PropertyBag
    {
        private Dictionary<string, object> propValues;

        public PropertyTable() : base()
        {
            propValues = new Dictionary<string, object>();
            Properties.ItemAdded += new ObservableList<PropertySpec>.ItemAddedHandler(Properties_ItemAdded);
        }

        void Properties_ItemAdded(ObservableList<PropertySpec> sender, PropertySpec item, int index)
        {
            propValues[item.Name] = item.DefaultValue;
        }

        public object this[string key]
        {
            get { return propValues[key]; }
            set { propValues[key] = value; }
        }

        protected override void OnGetValue(PropertySpecEventArgs e)
        {
            e.Value = propValues[e.Property.Name];
            base.OnGetValue(e);
        }
        protected override void OnSetValue(PropertySpecEventArgs e)
        {
            propValues[e.Property.Name] = e.Value;
            base.OnSetValue(e);
        }
    }
}