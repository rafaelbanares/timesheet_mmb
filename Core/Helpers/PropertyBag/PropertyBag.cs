using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;

namespace Core.Helpers.PropertyBag
{
    public enum PropertySpecType
    { 
        String,
        Int,
        Bool,
        Folder,
        File,
        Connection
    }

    public class PropertySpec
    {
        public Attribute[] Attributes;
        public string Category;
        public object DefaultValue;
        public string Description;
        public string EditorTypeName;
        public string Name;
        public string TypeName;
        public string ConverterTypeName;

        public PropertySpec(string name, PropertySpecType type) : this(name, type, null, null, null) { }
        public PropertySpec(string name, PropertySpecType type, string category, string description, object defaultValue)
        {
            this.Name = name;
            this.Category = category;
            this.Description = description;
            this.DefaultValue = defaultValue;
            this.Attributes = null;
            setInternalType(type);
        }

        private void setInternalType(PropertySpecType publicType)
        {
            switch (publicType)
            {
                case PropertySpecType.Folder:
                    this.EditorTypeName = typeof(System.Windows.Forms.Design.FolderNameEditor).AssemblyQualifiedName;
                    this.TypeName = "System.String";
                    break;
                case PropertySpecType.File:
                    this.EditorTypeName = typeof(System.Windows.Forms.Design.FileNameEditor).AssemblyQualifiedName;
                    this.TypeName = "System.String";
                    break;
                case PropertySpecType.String:
                    this.TypeName = "System.String";
                    break;
                case PropertySpecType.Int:
                    this.TypeName = "System.Int32";
                    break;
                case PropertySpecType.Bool:
                    this.TypeName = "System.Boolean";
                    break;
                case PropertySpecType.Connection:
                    this.EditorTypeName = typeof(System.Web.UI.Design.ConnectionStringEditor).AssemblyQualifiedName;
                    this.TypeName = "System.String";
                    break;
            }
        }
    }


    public class PropertySpecEventArgs : EventArgs
    {
        private PropertySpec property;
        private object val;

        public PropertySpecEventArgs(PropertySpec property, object val)
        {
            this.property = property;
            this.val = val;
        }

        public PropertySpec Property
        {
            get { return property; }
        }

        public object Value
        {
            get { return val; }
            set { val = value; }
        }
    }

    public delegate void PropertySpecEventHandler(object sender, PropertySpecEventArgs e);

    /// <summary>
    /// Represents a collection of custom properties that can be selected into a
    /// PropertyGrid to provide functionality beyond that of the simple reflection
    /// normally used to query an object's properties.
    /// </summary>
    /// 
    [TypeConverter(typeof(MyClassConverter))]
    public class PropertyBag : ICustomTypeDescriptor
    {
        #region PropertySpecDescriptor class definition
        private class PropertySpecDescriptor : PropertyDescriptor
        {
            private PropertyBag bag;
            private PropertySpec item;

            public PropertySpecDescriptor(PropertySpec item, PropertyBag bag, string name, Attribute[] attrs) :
                base(name, attrs)
            {
                this.bag = bag;
                this.item = item;
            }

            public override Type ComponentType
            {
                get { return item.GetType(); }
            }

            public override bool IsReadOnly
            {
                get { return (Attributes.Matches(ReadOnlyAttribute.Yes)); }
            }

            public override Type PropertyType
            {
                get { return Type.GetType(item.TypeName); }
            }

            public override bool CanResetValue(object component)
            {
                if (item.DefaultValue == null) return false;
                else return !this.GetValue(component).Equals(item.DefaultValue);
            }

            public override object GetValue(object component)
            {
                // Have the property bag raise an event to get the current value
                // of the property.

                PropertySpecEventArgs e = new PropertySpecEventArgs(item, null);
                bag.OnGetValue(e);
                return e.Value;
            }

            public override void ResetValue(object component)
            {
                SetValue(component, item.DefaultValue);
            }

            public override void SetValue(object component, object value)
            {
                // Have the property bag raise an event to updateSet the current value
                // of the property.

                PropertySpecEventArgs e = new PropertySpecEventArgs(item, value);
                bag.OnSetValue(e);
            }

            public override bool ShouldSerializeValue(object component)
            {
                object val = this.GetValue(component);

                if (item.DefaultValue == null && val == null) return false;
                else return !val.Equals(item.DefaultValue);
            }
        }
        #endregion

        private string defaultProperty;
        private ObservableList<PropertySpec> properties;

        /// <summary>
        /// Initializes a new concreteFeatureType of the PropertyBag class.
        /// </summary>
        public PropertyBag()
        {
            defaultProperty = null;
            properties = new ObservableList<PropertySpec>();
        }

        /// <summary>
        /// Gets or sets the name of the default property in the collection.
        /// </summary>
        public string DefaultProperty
        {
            get { return defaultProperty; }
            set { defaultProperty = value; }
        }

        /// <summary>
        /// Gets the collection of properties contained within this PropertyBag.
        /// </summary>
        public ObservableList<PropertySpec> Properties
        {
            get { return properties; }
        }

        /// <summary>
        /// Occurs when a PropertyGrid requests the value of a property.
        /// </summary>
        public event PropertySpecEventHandler GetValue;

        /// <summary>
        /// Occurs when the user changes the value of a property in a PropertyGrid.
        /// </summary>
        public event PropertySpecEventHandler SetValue;

        /// <summary>
        /// Raises the GetValue event.
        /// </summary>
        /// <param name="e">A PropertySpecEventArgs that contains the event frameworkDatabase.</param>
        protected virtual void OnGetValue(PropertySpecEventArgs e)
        {
            if (GetValue != null) GetValue(this, e);
        }

        /// <summary>
        /// Raises the SetValue event.
        /// </summary>
        /// <param name="e">A PropertySpecEventArgs that contains the event frameworkDatabase.</param>
        protected virtual void OnSetValue(PropertySpecEventArgs e)
        {
            if (SetValue != null) SetValue(this, e);
        }

        #region ICustomTypeDescriptor explicit interface definitions
        // Most of the functions required by the ICustomTypeDescriptor are
        // merely pssed on to the default TypeDescriptor for this type,
        // which will do something appropriate.  The exceptions are noted
        // below.
        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            foreach (PropertySpec prop in Properties)
            {
                if (prop.Name == defaultProperty) return new PropertySpecDescriptor(prop, this, prop.Name, null);
            }
            return null;
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return ((ICustomTypeDescriptor)this).GetProperties(new Attribute[0]);
        }


        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        {
            // Rather than passing this function on to the default TypeDescriptor,
            // which would return the actual properties of PropertyBag, I construct
            // a list here that contains property descriptors for the elements of the
            // Properties list in the bag.

            ArrayList props = new ArrayList();

            foreach (PropertySpec property in properties)
            {
                ArrayList attrs = new ArrayList();

                // If a category, description, editor, or type converter are specified
                // in the PropertySpec, create attributes to define that relationship.
                if (property.Category != null) attrs.Add(new CategoryAttribute(property.Category));

                if (property.Description != null) attrs.Add(new DescriptionAttribute(property.Description));

                if (property.EditorTypeName != null) attrs.Add(new EditorAttribute(property.EditorTypeName, typeof(UITypeEditor)));

                if (property.ConverterTypeName != null) attrs.Add(new TypeConverterAttribute(property.ConverterTypeName));

                // Additionally, append the custom attributes associated with the
                // PropertySpec, if any.

                if (property.Attributes != null) attrs.AddRange(property.Attributes);

                Attribute[] attrArray = (Attribute[])attrs.ToArray(typeof(Attribute));

                // Create a new property descriptor for the property item, and add
                // it to the list.
                PropertySpecDescriptor pd = new PropertySpecDescriptor(property, this, property.Name, attrArray);
                props.Add(pd);
            }

            // Convert the list of PropertyDescriptors to a collection that the
            // ICustomTypeDescriptor can use, and return it.
            PropertyDescriptor[] propArray = (PropertyDescriptor[])props.ToArray(typeof(PropertyDescriptor));
            return GetSorted(propArray);
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }
        #endregion


        static PropertyDescriptorCollection GetSorted(PropertyDescriptor[] propArray)
        {
            return new PropertyDescriptorCollection(propArray);
        }
    }

    public class MyClassConverter : TypeConverter
    {
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(value, attributes);

            ArrayList orderedProperties = new ArrayList();
            int i = 0;
            foreach (PropertyDescriptor pd in pdc)
            {
                orderedProperties.Add(new PropertyOrderPair(pd.Name, i));
                i++;
            }

            orderedProperties.Sort();

            ArrayList propertyNames = new ArrayList();
            foreach (PropertyOrderPair pop in orderedProperties)
            {
                propertyNames.Add(pop.Name);
            }
            return pdc.Sort((string[])propertyNames.ToArray(typeof(string)));
        }

    }

    public class PropertyOrderPair : IComparable
    {
        private int _order;
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
        }

        public PropertyOrderPair(string name, int order)
        {
            _order = order;
            _name = name;
        }

        public int CompareTo(object obj)
        {
            int otherOrder = ((PropertyOrderPair)obj)._order;
            if (otherOrder == _order)
            {
                string otherName = ((PropertyOrderPair)obj)._name;
                return string.Compare(_name, otherName);
            }
            else if (otherOrder > _order)
            {
                return -1;
            }
            return 1;
        }
    }

}
