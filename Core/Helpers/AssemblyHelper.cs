﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Core
{
    /// <summary>
    /// Set of helper methods to deal with Assembly and types
    /// </summary>
    public static class AssemblyHelper
    {
        #region Assembly

        public static void LoadAssemblies(string directoryPath)
        {
            var loadedAssemblyNames = AppDomain.CurrentDomain
                                        .GetAssemblies()
                                        .Select(x => x.GetName().Name);

            if (directoryPath != null)
            {
                var directoryInfo = new DirectoryInfo(directoryPath);
                if (directoryInfo.Exists)
                {
                    var files = directoryInfo
                        .GetFiles("*.dll", SearchOption.AllDirectories)
                        .Select(y => new { FullName = y.FullName, Name = Path.GetFileNameWithoutExtension(y.Name) })
                        .Where(x => !loadedAssemblyNames.Contains(x.Name))
                       ;

                    files.ForEach(x =>
                        {
                            try
                            {
                                Assembly.LoadFrom(x.FullName);
                            }
                            catch (BadImageFormatException)
                            {

                            }
                            catch (FileLoadException)
                            {

                            }

                        });
                }
            }
        }

        public static Type GetType(string directoryPath, string typeName)
        {
            var result = Type.GetType(typeName, false, true);
            if (result == null)
            {
                LoadAssemblies(directoryPath);
                result = Type.GetType(typeName, false, true);
            }
            return result;
        }

        public static Type GetType(string typeName)
        {
            return GetType(ContextHelper.BinPath(), typeName);
        }


        /// <summary>
        /// Extract all assemblies in a given directory for types matching the given criteria
        /// </summary>
        /// <param name="directoryPath">path to look into</param>
        /// <param name="criteriaType">A base class, an interface or an attribute</param>
        /// <returns>a list of Type</returns>
        public static IEnumerable<Type> GetTypes(string directoryPath, Type criteriaType)
        {
            var result = new List<Type>();
            LoadAssemblies(directoryPath);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                var elems = assembly.GetTypes(criteriaType);
                if (elems.IsNotNull()) result.AddRange(elems);
            }
            return result;
        }

        /// <summary>
        /// Extract all assemblies in 'bin' directory for types matching the given criteria
        /// </summary>
        /// <param name="criteriaType">A base class, an interface or an attribute</param>
        /// <returns>a list of Type</returns>
        public static IEnumerable<Type> GetTypes(Type criteriaType)
        {
            return GetTypes(ContextHelper.BinPath(), criteriaType);
        }


        #endregion
    }
}