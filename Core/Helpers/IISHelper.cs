﻿using System.Collections.Generic;
using System.DirectoryServices;
using System.IO;
using System.Runtime.InteropServices;

namespace Core
{
    public class IISConstants
    {
        public static string IISServiceName = "W3SVC";
        public static string RootVirtualDirectoryName = "ROOT";
        public static string RootVirtualDirectoryApplicationName = "Default CoreApplication";
        public static int DefaultWebSiteId = 1;
    }

    public class IISWebService
    {
        private string _machineName;
        private string _login;
        private string _password;
        private DirectoryEntry _entry;

        public IISWebService(string MachineName)
            : this(MachineName, null, null)
        {
        }

        public IISWebService(string MachineName, string Login, string Password)
        {
            _machineName = MachineName;
            _login = Login;
            _password = Password;

            _entry = new DirectoryEntry("IIS://" + _machineName + "/" + IISConstants.IISServiceName);

            if (_login != null)
            {
                _entry.Username = _login;
                _entry.Password = _password;
            }
        }

        public IISWebSite GetDefaultWebSite()
        {
            return GetWebSite(IISConstants.DefaultWebSiteId);
        }

        public IISWebSite GetWebSite(string SiteName)
        {
            return GetWebSite(GetWebSiteID(SiteName));
        }

        public IISWebSite GetWebSite(int SiteId)
        {
            IISWebSite result = null;
            foreach (DirectoryEntry childEntry in _entry.Children)
            {
                if (childEntry.Name == SiteId.ToString())
                {
                    childEntry.Username = _entry.Username;
                    //TODO: REMOVE IF INTEGRITY IS PRESERVED; childEntry.Password = _entry.Password;
                    result = new IISWebSite(childEntry);
                    break;
                }
            }
            return result;
        }

        public List<string> GetWebSitesNames()
        {
            List<string> result = new List<string>();
            try
            {
                foreach (DirectoryEntry childEntry in _entry.Children)
                {
                    if (childEntry.Properties["ServerComment"].Value != null)
                        result.Add(childEntry.Properties["ServerComment"].Value.ToString());
                }
            }
            catch (COMException)
            {
                // DO NOTHING, WEBSERVER DOES NOT EXIST
            }

            return result;
        }

        private int GetWebSiteID(string SiteName)
        {
            int result = -1;
            foreach (DirectoryEntry childEntry in _entry.Children)
            {
                if (childEntry.Properties.Contains("ServerComment"))
                {
                    if (childEntry.Properties["ServerComment"].Value.ToString() == SiteName)
                    {
                        result = int.Parse(childEntry.Name);
                        break;
                    }
                }
            }
            return result;
        }

        private int GetNewSiteId()
        {
            int result = 0;
            int current = -1;
            foreach (DirectoryEntry childEntry in _entry.Children)
            {
                current = int.Parse(childEntry.Name);
                if (current > result) result = current;
            }
            return result + 1;
        }

        public void Delete(IISWebSite site)
        {
            site.Entry.DeleteTree();
        }

        public void Delete(IISWebDirectory directory)
        {
            directory.Entry.DeleteTree();
        }

        public IISWebSite CreateWebSite(string SiteName, int Port, string PathToRoot)
        {
            int SiteId = GetWebSiteID(SiteName);
            DirectoryEntry siteEntry;

            if (SiteId != -1)
            {
                siteEntry = new DirectoryEntry(_entry.Path + "/" + SiteId.ToString());
                siteEntry.CommitChanges();
            }
            else
            {
                SiteId = GetNewSiteId();
                siteEntry = _entry.Children.Add(SiteId.ToString(), "IIsWebServer");
                siteEntry.Properties["ServerComment"].Value = SiteName;
            }

            siteEntry.Username = _entry.Username;
            //TODO: REMOVE IF INTEGRITY IS PRESERVED; siteEntry.Password = _entry.Password;

            siteEntry.Properties["KeyType"].Value = "IIsWebServer";
            siteEntry.Properties["ServerBindings"].Value = ":" + Port.ToString() + ":";
            siteEntry.Properties["ServerState"].Value = 2;
            siteEntry.Properties["FrontPageWeb"].Value = 1;
            siteEntry.Properties["DefaultDoc"].Value = "Default.aspx";
            siteEntry.Properties["SecureBindings"].Value = ":443:";
            siteEntry.Properties["ServerAutoStart"].Value = 1;
            siteEntry.Properties["ServerSize"].Value = 1;
            siteEntry.CommitChanges();

            DirectoryEntry virtualDirectoryEntry;
            try
            {
                virtualDirectoryEntry = siteEntry.Children.Add(IISConstants.RootVirtualDirectoryName, "IIsWebVirtualDir");
                virtualDirectoryEntry.CommitChanges();
            }
            catch
            {
                virtualDirectoryEntry = new DirectoryEntry(siteEntry.Path + "/" + IISConstants.RootVirtualDirectoryName);
            }
            virtualDirectoryEntry.Properties["Path"].Value = Path.GetFullPath(PathToRoot);
            virtualDirectoryEntry.Properties["DefaultDoc"].Value = "Default.aspx";
            virtualDirectoryEntry.CommitChanges();

            virtualDirectoryEntry.Invoke("AppCreate2", new object[] { 2 });
            virtualDirectoryEntry.Properties["AppFriendlyName"].Value = IISConstants.RootVirtualDirectoryApplicationName;
            virtualDirectoryEntry.CommitChanges();

            return GetWebSite(SiteId);
        }
    }

    public class IISWebSite
    {
        private DirectoryEntry _siteEntry;
        private DirectoryEntry _rootVirtualDirectoryEntry;

        public string PhysicalLocation
        {
            get { return _rootVirtualDirectoryEntry.Properties["Path"].Value.ToString(); }
        }

        public IISWebSite(DirectoryEntry entry)
        {
            _siteEntry = entry;
            _rootVirtualDirectoryEntry = new DirectoryEntry(_siteEntry.Path + "/" + IISConstants.RootVirtualDirectoryName);
            _rootVirtualDirectoryEntry.Username = entry.Username;

            //TODO: REMOVE IF INTEGRITY IS PRESERVED; _rootVirtualDirectoryEntry.Password = entry.Password;
        }

        internal DirectoryEntry Entry
        {
            get { return _siteEntry; }
        }

        public int ID
        {
            get { return int.Parse(_siteEntry.Name); }
        }

        public string Name
        {
            get { return _siteEntry.Properties["ServerComment"].Value.ToString(); }
        }

        public IISWebDirectory GetDirectory(string DirectoryName)
        {
            IISWebDirectory result = null;
            foreach (DirectoryEntry childEntry in _rootVirtualDirectoryEntry.Children)
            {
                if (childEntry.Name == DirectoryName)
                {
                    childEntry.Username = _rootVirtualDirectoryEntry.Username;
                    //TODO: REMOVE IF INTEGRITY IS PRESERVED; childEntry.Password = _rootVirtualDirectoryEntry.Password;
                    result = new IISWebDirectory(childEntry);
                    break;
                }
            }
            return result;
        }

        public IISWebDirectory CreateVirtualDirectory(string Name, string PathToDirectory)
        {
            DirectoryEntry virtualDirectoryEntry;
            try
            {
                virtualDirectoryEntry = _rootVirtualDirectoryEntry.Children.Add(Name, "IIsWebVirtualDir");
                virtualDirectoryEntry.CommitChanges();
            }
            catch
            {
                virtualDirectoryEntry = new DirectoryEntry(_rootVirtualDirectoryEntry.Path + "/" + Name);
            }
            virtualDirectoryEntry.Properties["Path"].Value = Path.GetFullPath(PathToDirectory);
            virtualDirectoryEntry.Properties["DefaultDoc"].Value = "Default.aspx";
            virtualDirectoryEntry.CommitChanges();

            return GetDirectory(Name);
        }

        public IISWebDirectory CreateDirectory(string Name)
        {
            DirectoryEntry directoryEntry;
            try
            {
                directoryEntry = _rootVirtualDirectoryEntry.Children.Add(Name, "IIsWebDirectory");
                directoryEntry.CommitChanges();
            }
            catch
            {
                directoryEntry = new DirectoryEntry(_rootVirtualDirectoryEntry.Path + "/" + Name);
            }
            directoryEntry.CommitChanges();
            directoryEntry.Properties["DefaultDoc"].Value = "Default.aspx";
            directoryEntry.CommitChanges();

            return GetDirectory(Name);
        }
    }

    public class IISWebDirectory
    {
        private DirectoryEntry _entry;

        internal DirectoryEntry Entry
        {
            get { return _entry; }
        }

        public IISWebDirectory(DirectoryEntry entry)
        {
            _entry = entry;
        }

        public string Name
        {
            get { return _entry.Name; }
        }

        public IISWebDirectory GetDirectory(string DirectoryName)
        {
            IISWebDirectory result = null;
            foreach (DirectoryEntry childEntry in _entry.Children)
            {
                if (childEntry.Name == DirectoryName)
                {
                    childEntry.Username = _entry.Username;
                    //TODO: REMOVE IF INTEGRITY IS PRESERVED; childEntry.Password = _entry.Password;
                    result = new IISWebDirectory(childEntry);
                    break;
                }
            }
            return result;
        }

        public IISWebDirectory CreateVirtualDirectory(string Name, string PathToDirectory)
        {
            DirectoryEntry virtualDirectoryEntry;
            try
            {
                virtualDirectoryEntry = _entry.Children.Add(Name, "IIsWebVirtualDir");
                virtualDirectoryEntry.CommitChanges();
            }
            catch
            {
                virtualDirectoryEntry = new DirectoryEntry(_entry.Path + "/" + Name);
            }
            virtualDirectoryEntry.Properties["Path"].Value = Path.GetFullPath(PathToDirectory);
            virtualDirectoryEntry.Properties["DefaultDoc"].Value = "Default.aspx";
            virtualDirectoryEntry.CommitChanges();

            return GetDirectory(Name);
        }

        public IISWebDirectory CreateDirectory(string Name)
        {
            DirectoryEntry directoryEntry;
            try
            {
                directoryEntry = _entry.Children.Add(Name, "IIsWebDirectory");
                directoryEntry.CommitChanges();
            }
            catch
            {
                directoryEntry = new DirectoryEntry(_entry.Path + "/" + Name);
            }
            directoryEntry.CommitChanges();
            directoryEntry.Properties["DefaultDoc"].Value = "Default.aspx";
            directoryEntry.CommitChanges();

            return GetDirectory(Name);
        }

        public void CreateApplication()
        {
            _entry.Invoke("AppCreate2", new object[] { 2 });
            _entry.Properties["AppFriendlyName"].Value = _entry.Name;
            //			_entry.Properties["AccessExecute"].Value = true;
            _entry.CommitChanges();
        }

        public void RemoveApplication()
        {
            _entry.Invoke("AppDelete");
            _entry.CommitChanges();
        }
    }
}
