namespace Core.Helpers.Reflection
{
    public interface IDynamicConstructor
    {
        object Invoke(object[] arguments);
    }

    public interface IDynamicField
    {
        object GetValue(object target);
        void SetValue(object target, object value);
    }

    public interface IDynamicProperty : IDynamicField
    {
        object GetValue(object target, params object[] index);
        void SetValue(object target, object value, params object[] index);
    }

    public interface IDynamicIndexer
    {
        object GetValue(object target, int index);
        object GetValue(object target, object index);
        object GetValue(object target, object[] index);
        void SetValue(object target, int index, object value);
        void SetValue(object target, object index, object value);
        void SetValue(object target, object[] index, object value);
    }

    public interface IDynamicMethod
    {
        object Invoke(object target, params object[] arguments);
    }

}

