﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Core
{
    public static class ImageHelper
    {
        /// <summary>
        /// Converts the bytes to image.
        /// </summary>
        /// <param name="fileBytes">The file bytes.</param>
        /// <returns></returns>
        public static Image ConvertBytesToImage(byte[] fileBytes)
        {
            Image image;
            using (var memoryStream = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                image = Image.FromStream(memoryStream, true);
            }
            return image;
        }

        /// <summary>
        /// Converts the image to bytes.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <returns></returns>
        public static byte[] ConvertImageToBytes(Image image, ImageFormat imageFormat)
        {
            byte[] imageBytes;

            using (var memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, imageFormat);
                imageBytes = memoryStream.ToArray();
            }

            return imageBytes;
        }

        /// <summary>
        /// Gets the scale factor to resize proportionally an image.
        /// </summary>
        /// <param name="width">The current width of the image.</param>
        /// <param name="height">The current height of the image.</param>
        /// <param name="targetMaxSize">Max size for the target image.</param>
        /// <returns></returns>
        public static double GetScaleFactor(double width, double height, double targetMaxSize)
        {
            if (width <= targetMaxSize && height <= targetMaxSize)
            {
                return 1.0;
            }

            var scaleWidth = targetMaxSize/width;
            var scaleHeight = targetMaxSize/height;

            return scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
        }

        /// <summary>
        /// Scales the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="scaleFactor">The scale factor.</param>
        /// <returns>An image proportionally resized.</returns>
        public static Image ScaleImage(Image image, double scaleFactor)
        {
            var sourceWidth = image.Size.Width;
            var sourceHeight = image.Size.Height;

            var destWidth = (int) (sourceWidth*scaleFactor);
            var destHeight = (int) (sourceHeight*scaleFactor);

            var bitmap = new Bitmap(destWidth, destHeight);
            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            var graphics = Graphics.FromImage(bitmap);
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            graphics.DrawImage(
                image,
                new Rectangle(0, 0, destWidth, destHeight),
                new Rectangle(0, 0, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            graphics.Dispose();

            return bitmap;
        }

        /// <summary>
        /// Returns <c>true</c> whether the <paramref name="contentType"/> corresponds
        /// to the content type of an image.
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static bool IsImageFile(string contentType)
        {
            if (!string.IsNullOrEmpty(contentType))
            {
                contentType = contentType.ToLowerInvariant();
                return contentType.StartsWith("image");
            }

            return false;
        }

        /// <summary>
        /// Returns the size image from an array of bytes (representing the image).
        /// </summary>
        /// <param name="imageBytes"></param>
        /// <returns></returns>
        public static Size GetImageSize(byte[] imageBytes)
        {
            var originalImage = ConvertBytesToImage(imageBytes);
            return originalImage.Size;
        }

        /// <summary>
        /// Format the <paramref name="sizeImage"/> size with the following template: width x height.
        /// Can be used to display the size.
        /// </summary>
        /// <param name="sizeImage"></param>
        /// <returns></returns>
        public static string TransformSizeToInformation(Size sizeImage)
        {
            return string.Format("{0}x{1}", sizeImage.Width, sizeImage.Height);
        }
    }
}