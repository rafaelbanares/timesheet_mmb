﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;

namespace Core.Helpers
{
    public class SqlServerHelper
    {
        private static int commandTimeOut = 1200;
        private static string sqlAuthConnectionString = "Data Source={0};User Id={1};Password={2};Initial Catalog={3};";
        private static string winAuthConnectionString = "Data Source={0};Integrated Security=SSPI;Initial Catalog={1};";

        public static void ExecuteMassiveStatement(string sql, string serverName, string dbName)
        {
            ExecuteMassiveStatement(sql, string.Format(winAuthConnectionString, serverName, dbName));
        }

        public static void ExecuteMassiveStatement(string sql, string serverName, string dbName, string user, string password)
        {
            ExecuteMassiveStatement(sql, string.Format(sqlAuthConnectionString, serverName, dbName, user, password));
        }

        public static void ExecuteMassiveStatement(string sql, string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand cmd;
            string commandText = "";

            String[] lines = sql.Split(new Char[] { '\n' });
            StringBuilder buffer = new StringBuilder();
            foreach (string l in lines)
            {
                string line = l.Trim();
                if (line.ToLower(CultureInfo.InvariantCulture).Equals("go"))
                {
                    commandText = buffer.ToString();
                    if (commandText.Length > 0)
                    {
                        try
                        {
                            cmd = new SqlCommand(buffer.ToString(), connection);
                            cmd.CommandTimeout = commandTimeOut;
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                        finally { }
                    }
                    buffer = new StringBuilder();
                }
                else if (!line.StartsWith("--"))
                {
                    buffer.Append(line);
                    buffer.Append("\n");
                }
            }
            commandText = buffer.ToString();
            if (commandText.Length > 0)
            {
                try
                {
                    cmd = new SqlCommand(commandText, connection);
                    cmd.CommandTimeout = commandTimeOut;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                finally { }
            }
            connection.Close();
        }
    }
}
