using System;
using System.Collections;
using System.Data;
using System.Xml;
using System.Web;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Collections.Concurrent;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.IO.MemoryMappedFiles;
using System.ComponentModel;
using Core.Helpers.Threading;
using Core.Helpers.Logging;
using System.Security.Principal;
using System.Security.AccessControl;

namespace Core.Helpers.Caching
{
    [Serializable]
    public class IndexEntry : ISerializable
    {
        public string Key { get; set; }
        public DateTime ExpiresOn { get; set; }
        public DateTime InsertedOn { get; set; }

        public IndexEntry()
        {

        }

        protected IndexEntry(SerializationInfo info, StreamingContext context)
        {
            Key = info.GetString("Key");
            ExpiresOn = info.GetDateTime("ExpiresOn");
            InsertedOn = info.GetDateTime("InsertedOn");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Key", Key);
            info.AddValue("ExpiresOn", ExpiresOn);
            info.AddValue("InsertedOn", InsertedOn);
        }
    }

    public class OnDiskCache<T> : Singleton<T>, ICache
        where T : OnDiskCache<T>
    {
        const string IndexFileName = "index";
        volatile int WriteRequest = 0;
        System.Threading.Timer Timer;
        //BackgroundWorker writeIndexInvoker;

        bool mutexWasCreated = false;
        string mutexName = @"Global\" + typeof(T).FullName + "_" + Core.Helpers.Environment.ApplicationName;
        static string CacheDirectory { get; set; }

        ConcurrentDictionary<string, IndexEntry> index = new ConcurrentDictionary<string, IndexEntry>();
        Mutex oMutex = null;

        protected OnDiskCache()
            : this(Path.Combine(Constants.Directories.RootDirectory, "Cache"))
        {

        }

        protected OnDiskCache(string directory)
        {
            string name;
            if (Thread.CurrentPrincipal.IsNotNull() && Thread.CurrentPrincipal.Identity.IsAuthenticated)
                name = "Thread Identity: " + Thread.CurrentPrincipal.Identity.Name;
            else
                name = "Windows Identity: " + WindowsIdentity.GetCurrent().Name;

            //Logger.Debug("OnDiskCache-Identity: " + mutexName, name, "OnDiskCache");

            CreateMutex();

            CacheDirectory = directory;
            if (!Directory.Exists(CacheDirectory)) Directory.CreateDirectory(CacheDirectory);
            ReadIndex();
            //RemoveExpired();
            Timer = new System.Threading.Timer(TimerCallBack, null, 0, 10000);
        }

        [STAThread]
        private void CreateMutex()
        {
            bool doesNotExist = false;
            bool unauthorized = false;

            string user = ContextHelper.GetUserName();

            try
            {
                //Logger.Debug("OnDiskCache-GetMutex: " + mutexName, "Opening existing mutex", "OnDiskCache");
                oMutex = Mutex.OpenExisting(mutexName, MutexRights.FullControl);
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                doesNotExist = true;
            }
            catch (UnauthorizedAccessException)
            {
                unauthorized = true;
            }
            catch (Exception ex)
            {
                ex.Log();
                throw ex;
            }

            if (doesNotExist)
            {
                //Logger.Debug("OnDiskCache-GetMutex: " + mutexName, "Creating new mutex", "OnDiskCache");
                MutexSecurity mSec = new MutexSecurity();

                var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                var mutexsecurity = new MutexSecurity();
                mutexsecurity.AddAccessRule(new MutexAccessRule(sid, MutexRights.FullControl, AccessControlType.Allow));

                oMutex = new Mutex(false, mutexName, out mutexWasCreated, mutexsecurity);
                GC.KeepAlive(oMutex);
            }
            else if (unauthorized)
            {
                //Logger.Debug("OnDiskCache-GetMutex: " + mutexName, "Unauthorized. Setting mutex security", "OnDiskCache");
                try
                {
                    oMutex = Mutex.OpenExisting(mutexName,
                        MutexRights.ReadPermissions | MutexRights.ChangePermissions);

                    MutexSecurity mSec = oMutex.GetAccessControl();

                    MutexAccessRule rule = new MutexAccessRule(user,
                        MutexRights.FullControl,
                        AccessControlType.Allow);
                    mSec.AddAccessRule(rule);

                    oMutex.SetAccessControl(mSec);

                    oMutex = Mutex.OpenExisting(mutexName, MutexRights.FullControl);
                }
                catch (Exception ex)
                {
                    ex.Log();
                    throw ex;
                }
            }

            #region For Debugging only

            //MutexSecurity security = oMutex.GetAccessControl();
            //foreach (MutexAccessRule ar in
            //security.GetAccessRules(true, true, typeof(NTAccount)))
            //{
            //    Logger.Debug("OnDiskCache-Mutex Security: " + typeof(T).FullName,
            //        string.Format("User: {0}, Type: {1}, Rights: {2}", ar.IdentityReference, ar.AccessControlType, ar.MutexRights),
            //        "OnDiskCache");
            //}

            #endregion

        }

        static string GetObjectPath(string objectName)
        {
            return Path.Combine(CacheDirectory, objectName + ".dat");
        }

        protected override void Cleanup()
        {
            if (Timer.IsNotNull()) Timer.Dispose();
            base.Cleanup();
        }

        void TimerCallBack(object state)
        {
            RemoveExpired();
        }

        void ReadIndex()
        {
            var indexData = ReadObject(GetObjectPath(IndexFileName), IndexFileName) as IndexEntry[];
            if (indexData != null) Parallel.For(0, indexData.Length, i => index.AddOrUpdate(indexData[i].Key, indexData[i], (key, oldValue) => indexData[i]));
        }

        void WriteIndex()
        {
            ++WriteRequest;
            ThreadUtil.Queue(() => DelayedWrite(WriteRequest));
        }

        void DelayedWrite(int CurrentWriteRequest)
        {
            Thread.SpinWait(1000);

            if (CurrentWriteRequest == WriteRequest)
            {
                var indexData = index.Values.ToArray();
                WriteObject(GetObjectPath(IndexFileName), IndexFileName, indexData);
                WriteRequest = 0;
            }
        }

        void RemoveExpired()
        {
            var limit = DateTime.UtcNow;
            var expired = index.Values.Where(x => x.ExpiresOn < limit).Select(x => x.Key).ToArray();
            expired.ForEach(x => DeleteObject(x));
        }

        void AddObject(string key, object value)
        {
            AddObject(key, value, DateTime.MaxValue);
        }

        void AddObject(string key, object value, DateTime expiresOn)
        {
            var entry = new IndexEntry { Key = key, ExpiresOn = expiresOn, InsertedOn = DateTime.UtcNow };
            index.AddOrUpdate(key, entry, (x, oldValue) => entry);
            ThreadUtil.Queue(() => WriteObject(GetObjectPath(key), key, value));
            WriteIndex();
        }

        void AddObject(string key, object value, TimeSpan lifetime)
        {
            AddObject(key, value, DateTime.UtcNow.Add(lifetime));
        }

        void RemoveKeys(IEnumerable<string> keys)
        {
            keys.AsParallel().ForAll(x => DeleteObject(x));
        }

        void Empty()
        {
            var keys = index.Keys.ToArray();
            index.Clear();
            WriteIndex();
            keys.AsParallel().ForAll(x => ThreadUtil.Queue(() => DeleteObject(x)));
        }


        object GetObject(string key)
        {
            IndexEntry entry = null;
            index.TryGetValue(key, out entry);
            if (entry == null) return null;
            if (entry.ExpiresOn < DateTime.UtcNow) return null;
            return ReadObject(GetObjectPath(key), key);
        }

        void DeleteObject(string key)
        {
            IndexEntry entry = null;
            if (index.TryRemove(key, out entry))
            {
                WriteIndex();
                ThreadUtil.Queue(() => DeleteFile(GetObjectPath(key)));
            }
        }
        public IEnumerable<IndexEntry> GetIndex()
        {
            return index.Values;
        }
        public bool IsExpired(string Key)
        {
            IndexEntry entry = null;
            index.TryGetValue(Key, out entry);
            if (entry == null) return true;
            if (entry.ExpiresOn < DateTime.UtcNow) return true;
            return false;
        }

        public Tuple<DateTime, DateTime> GetExpirationInfo(string Key)
        {
            IndexEntry entry = null;
            index.TryGetValue(Key, out entry);
            if (entry == null) return null;
            return Tuple.Create(entry.InsertedOn, entry.ExpiresOn);
        }

        public IEnumerable<string> Keys
        {
            get
            {
                return index.Keys;
            }
        }

        public int Count
        {
            get { return index.Count; }
        }

        public object Get(string Key)
        {
            return GetObject(Key);
        }

        public void Insert(string Key, object obj)
        {
            AddObject(Key, obj);
        }

        public void Insert(string Key, object obj, TimeSpan timespan)
        {
            AddObject(Key, obj, timespan);
        }

        public void Remove(string Key)
        {
            DeleteObject(Key);
        }

        public void Clear()
        {
            Empty();
        }

        public void RemoveAll(IEnumerable<string> key)
        {
            RemoveKeys(key);
        }

        public void Insert(string Key, object obj, DateTime absoluteUTCExpiration)
        {
            AddObject(Key, obj, absoluteUTCExpiration);
        }


        object ReadObject(string path, string objectName)
        {
            object result = null;
            try
            {
                try
                {
                    oMutex.WaitOne(-1, true);
                    MemoryStream ms;
                    using (var map = MemoryMappedFile.CreateFromFile(path, FileMode.Open, objectName))
                    using (var accessor = map.CreateViewStream())
                    {
                        ms = accessor.LoadIntoMemoryStream();

                        if (ms != null)
                        {
                            // deserialize
                            BinaryFormatter bf = new BinaryFormatter();
                            result = bf.Deserialize(ms);
                            ms.Close();
                        }
                    }
                }
                finally
                {
                    oMutex.ReleaseMutex();
                }
            }
            catch { return null; }
            return result;
        }

        void WriteObject(string path, string objectName, object InObject)
        {
            try
            {
                if (!path.IsFilled()) path = string.Empty;

                // serialize
                MemoryStream ms = new MemoryStream();
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, InObject);
                var buffer = ms.GetBuffer();
                ms.Close();

                try
                {
                    oMutex.WaitOne(-1, true);
                    using (var map = MemoryMappedFile.CreateFromFile(path, FileMode.Create, objectName, buffer.LongLength, MemoryMappedFileAccess.ReadWrite))
                    using (var accessor = map.CreateViewStream(0, buffer.LongLength, MemoryMappedFileAccess.ReadWrite))
                    {
                        accessor.Write(buffer, 0, buffer.Length);
                    }
                }
                finally
                {
                    oMutex.ReleaseMutex();
                }
            }
            catch (ArgumentException aex)
            {
                Logger.Error("Exception : " + aex.Message, "File path: " + path + System.Environment.NewLine + aex.Format());
                aex.LogToTrace();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }

        static void DeleteFile(string path)
        {
            if (File.Exists(path)) File.Delete(path);
        }
    }

    public class OnDiskCache : OnDiskCache<OnDiskCache>
    {
    }
}
