﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Helpers.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Core.Helpers.Caching
{
    public class StaticDataInitializationTask : IStartupTask
    {
        public void OnStart()
        {
            StaticDataHelper.EnsureAll();
        }
    }

    static class StaticDataHelper
    {
        static object locker = new object();
        static bool IsInitializing;

        public static void EnsureAll()
        {
            lock (locker)
            {
                if (!IsInitializing)
                {
                    IsInitializing = true;

                    var s = DateTime.Now;
                    var tasks = new List<Task>();

                    System.Diagnostics.Trace.WriteLine("GlobalCache: Initialize started at " + s.ToShortTimeString());

                    var itemTypes = typeof(object).GetMatchingTypes(u => u.BaseType != null && u.BaseType.IsGenericType && (u.BaseType.GetGenericTypeDefinition() == typeof(StaticData<,>)));
                    try
                    {
                        Parallel.ForEach(itemTypes, (t)
                            =>
                        {
                            var itemType = t as Type;
                            //if (!System.Threading.Thread.CurrentThread.Name.IsFilled()) Thread.CurrentThread.Name = ("GlobalCache: " + itemType.FullName);
                           // Debug.WriteLine("GlobalCache: Initializing " + itemType.FullName + " at " + DateTime.Now.ToShortTimeString());
                            itemType.CallStaticMethod("SelfBuild", null); // .GetStaticValue("Data");
                           // Debug.WriteLine("GlobalCache: Initialized " + itemType.FullName + " at " + DateTime.Now.ToShortTimeString());
                        });
                    }
                    catch (AggregateException aex)
                    {
                        foreach (Exception ex in aex.InnerExceptions)
                            ex.Log("GlobalCache");
                    }
                    System.Diagnostics.Trace.WriteLine("GlobalCache: Initialize completed in " + (DateTime.Now - s).TotalMilliseconds.ToString());
                    IsInitializing = false;
                }
            }
        }
    }

}
