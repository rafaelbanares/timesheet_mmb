﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;

namespace Core.Helpers.Caching
{
    public interface ICache
    {
        IEnumerable<string> Keys { get; }
        int Count { get; }

        void Clear();
        object Get(string Key);
        void Insert(string Key, object obj);
        void Insert(string Key, object obj, TimeSpan timespan);
        void Insert(string Key, object obj, DateTime absoluteUTCExpiration);
        void Remove(string Key);
        void RemoveAll(IEnumerable<string> key);
    }
}
