﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Core.Helpers.Generic;
using System.Diagnostics;
using System.IO;
using Core.Helpers.Logging;
using System.Runtime.Serialization.Formatters.Binary;
using Core.Helpers.Threading;
using Core.Model;

namespace Core.Helpers.Caching
{
    public abstract class StaticData<U, T>
        where U : StaticData<U, T>
        where T : class
    {
        protected internal abstract T GetFresh();
        protected internal abstract TimeSpan LifeTime { get; }

        volatile static State CurrentState = State.Ready;
        volatile static object dataLocker = new object();
        volatile static object stateLocker = new object();
        static DateTime LastPreventiveCheck = DateTime.Now;

        private static string CacheKey
        {
            get
            {
                if (_self == null) throw new FrameworkException("can't use CacheKey before self is initialized");
                return _self.GetType().FullName;
            }

        }

        static T _data = null;
        static StaticData<U, T> _self = null;


        public static T Data
        {
            get
            {
                if (CurrentState != State.Ready)
                {
                    lock (dataLocker)
                    {
                        if (CurrentState != State.Ready)
                        {
                            if (_self == null)
                            {
                                SelfBuild();
                            }

                            var isExpired = _self.IsExpired();
                            if (_data == null || isExpired)
                            {
                                if (isExpired) _data = null;
                                ForceRefresh();
                            }

                        }
                    }
                }
                if ((DateTime.Now - LastPreventiveCheck).Seconds > 1)
                    ThreadUtil.Queue(() => CheckForPreventiveRefresh());

                return _data;
            }
        }

        private static void CheckForPreventiveRefresh()
        {
            _self.Refresh(true);
            LastPreventiveCheck = DateTime.Now;
        }

        private static void ForceRefresh()
        {
            _self.Refresh(false);
        }

        public StaticData() { }


        public static void PseudoInvalidate()
        {
            lock (stateLocker)
            {
                CurrentState = State.Refreshing;
                _data = null;
                CurrentState = State.None;

            }
        }

        public static void Invalidate()
        {
            if (_self != null) OnDiskCache.Instance.Remove(CacheKey);
        }

        public static void SelfBuild()
        {
            Type genericType = typeof(StaticData<,>);
            Type[] typeArgs = { typeof(U), typeof(T) };
            Type constructed = genericType.MakeGenericType(typeArgs);

            var lst = constructed
                .GetMatchingTypes()
                .GetOne<StaticData<U, T>>();

            _self = lst.First();
            _self.Refresh(false);
        }

        public static void ForceManualRefresh()
        {
            if (_self != null) _self.DoRealRefresh();
        }

        internal bool IsExpired()
        {
            return OnDiskCache.Instance.IsExpired(CacheKey);
        }

        internal void DoRealRefresh()
        {
            Exception occured = null;
            lock (stateLocker)
            {
                CurrentState = State.Refreshing;
                try
                {
                    Logger.Info("Refreshing Cache for " + CacheKey);
                    T newCopy = GetFresh();
                    var actualLifetime = (Core.Helpers.Environment.InCassini) ? 1.Days() : LifeTime;
                    OnDiskCache.Instance.Insert(CacheKey, newCopy, actualLifetime);
                    _data = newCopy;
                }
                catch (Exception x)
                {
                    Logger.Error("Unable to Refresh Cache for " + CacheKey, x.GetExplicitBaseException().Format());
                    occured = x;
                }
                CurrentState = State.Ready;
            }
            if (occured != null) throw new FrameworkException("Unable to populate cache : " + CacheKey + ". Check logs for more details...");
        }

        internal void DoBackgroundRefresh()
        {
            try
            {
                Logger.Info("Background refresh of Cache for " + CacheKey);
                T newCopy = GetFresh();
                var actualLifetime = (Core.Helpers.Environment.InCassini) ? 1.Days() : LifeTime;
                OnDiskCache.Instance.Insert(CacheKey, newCopy, actualLifetime);
                lock (stateLocker)
                {
                    CurrentState = State.Refreshing;
                    _data = newCopy;
                }
            }
            catch (Exception x)
            {
                Logger.Error("Unable to Background refresh Cache for " + CacheKey, x.Format());
            }
            CurrentState = State.Ready;
        }

        internal void Refresh(bool IsCheck)
        {
            if (!IsCheck)
            {
                T newCopy = (T)OnDiskCache.Instance.Get(CacheKey);
                if (newCopy.IsNull())
                {
                    DoRealRefresh();
                }
                else
                {
                    lock (stateLocker)
                    {
                        CurrentState = State.Refreshing;
                        _data = newCopy;
                        CurrentState = State.Ready;
                    }
                }
            }
            else
            {
                var expirationData = OnDiskCache.Instance.GetExpirationInfo(CacheKey);
                if (expirationData != null)
                {
                    var plannedTimedInCache = LifeTime;
                    var timeSpentInCache = (DateTime.UtcNow - expirationData.Item1);
                    var percent = (timeSpentInCache.TotalSeconds / plannedTimedInCache.TotalSeconds);
                    Console.WriteLine(percent);
                    if (percent > .9)
                    {
                        DoBackgroundRefresh();
                    }
                }
            }
        }

        public enum State
        {
            None,
            Ready,
            Refreshing,
        }

    }
}
