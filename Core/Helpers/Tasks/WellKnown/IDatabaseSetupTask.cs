﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using System.Reflection;
using Core.Features;
using System.Diagnostics;

namespace Core.Helpers.Tasks.WellKnown
{
    public abstract class IDatabaseSetupTask : ISetupTask
    {
        public virtual ConnectionInfos ConnectionInfos { get { return Finder.FrameworkDatabaseConnections.Main; } }
        public abstract string Script { get; }

        public override string Name
        {
            get { return this.GetType().Name; }
        }

        public override string Description
        {
            get { return string.Empty; }
        }

        public override void OnSetup()
        {
            // TODO : Log somewhere execution of the script
            //Debug.WriteLine("Executing sript:" + Name);
            ConnectionInfos.ExecuteScript(Script); 
        }
    }
    public abstract class IEmbeddedDatabaseSetupTask : IDatabaseSetupTask
    {
        public override string Script
        {
            get
            {
                var scriptname = this.GetType().Namespace + "." + ScriptName.AppendInCase(".sql");
                var script = this.GetType().Assembly.GetResourceContent(scriptname);
                if (script.IsNull()) throw new ApplicationException("Invalid resource name for script " + scriptname + ". Are you sure it is Embedded Resource?");

                return script;
            }
        }
        public abstract string ScriptName { get; }

        public override string Name
        {
            get { return this.GetType().Name; }
        }

        public override string Description
        {
            get { return string.Empty; }
        }

        public override void OnSetup()
        {
            ConnectionInfos.ExecuteScript(Script);
        }
    }

    public abstract class IDatabaseConnectionCaptureSetupTask : ISetupTask
    {
        protected override IEnumerable<Field> GetFieldsToCapture()
        {
            var res = new List<Field>();
            res.Add(new Field { Name = "Server name", Mandatory = true, Description = "SQL Service host (e.g:CHDB008.b2b.sgs.com or CHDB008 or CHDB008\\InstanceName or)" });
            res.Add(new Field { Name = "Database name", Mandatory = false, Description = "Name of the Certnet database (e.g. Cernet_1_1_2008)" });
            res.Add(new Field { Name = "Username", Description = "Name of the SQL server User." });
            res.Add(new Field { Name = "Password", Description = "Password of the SQL server User." });
            return res;
        }

        public override void Validate()
        {
            var message = string.Empty;

            var serverName = Fields.First(x => x.Name == "Server name").Value.Safe();
            var dbName = Fields.First(x => x.Name == "Database name").Value.Safe();
            var userName = Fields.First(x => x.Name == "Username").Value.Safe();
            var password = Fields.First(x => x.Name == "Password").Value.Safe();

            if (string.IsNullOrEmpty(serverName.Safe().Trim()))
            {
                message += "Server name must be provided" + System.Environment.NewLine;
            }

            if (message.Length > 0) throw new ApplicationException(System.Environment.NewLine + message);

            var ci = new ConnectionInfos { ServerName = serverName, DatabaseName = dbName, UserName = userName, Password = password };
            if (!ci.Test())
            {
                throw new ApplicationException("Unable to connect to database! Review configuration, then try again. If problem persists check network connectivity and sql server authorizations.");
            }

        }

        public override void OnSetup()
        {
            var ci = new ConnectionInfos { ServerName = Fields[0].Value, DatabaseName = Fields[1].Value, UserName = Fields[2].Value, Password = Fields[3].Value };
            try
            {
                OnConnectionStringCaptured(ci.ToString());
            }
            catch (Exception x)
            {
                x.Log("Setup", String.Format("Unable to capture database connection string : {0}", ci.ToString()));
            }
        }

        protected abstract void OnConnectionStringCaptured(string connectionString);

    }

    public abstract class IDatabaseCreationSetupTask : ISetupTask
    {
        protected abstract string DatabaseName { get; }
        protected abstract string UserName { get; }
        protected abstract string Password { get; }
        protected abstract void OnDatabaseCreated(string connectionString);

        public override void OnSetup()
        {
            var serverName = Fields.First(x => x.Name == "Server name").Value.Safe();
            var ci = new ConnectionInfos { ServerName = Fields[0].Value, DatabaseName = DatabaseName, UserName = UserName, Password = Password };
            try
            {
                ci.SetupDatabase();
                OnDatabaseCreated(ci.ToString());
            }
            catch (Exception x)
            {
                x.Log("Setup", String.Format("Unable to setup database {0} on server {1}", DatabaseName, serverName));
                throw x;
            }

        }

        protected override IEnumerable<Field> GetFieldsToCapture()
        {
            var res = new List<Field>();
            res.Add(new Field { Name = "Server name", Mandatory = true, Description = "SQL Service host (e.g:CHDB008.b2b.sgs.com or CHDB008 or CHDB008\\InstanceName or)" });
            return res;
        }

        public override void Validate()
        {
            var message = string.Empty;

            var serverName = Fields.First(x => x.Name == "Server name").Value.Safe();

            if (string.IsNullOrEmpty(serverName.Safe().Trim()))
            {
                message += "Server name must be provided" + System.Environment.NewLine;
            }

            if (message.Length > 0) throw new ApplicationException(System.Environment.NewLine + message);

            var ci = new ConnectionInfos { ServerName = serverName };
            if (!ci.Test())
            {
                throw new ApplicationException("Unable to connect to server! You must be sysadmin to perform such operation. If problem persists check network connectivity and sql server authorizations.");
            }
        }
    }

}
