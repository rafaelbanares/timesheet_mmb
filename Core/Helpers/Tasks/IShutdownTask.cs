﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model.Tasks
{
    public interface IShutdownTask
    {
        void OnStop();
    }
}
