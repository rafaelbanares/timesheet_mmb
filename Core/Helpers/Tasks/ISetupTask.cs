﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Core.Helpers.Tasks
{
    public abstract class ISetupTask
    {
        public abstract string Name { get; }
        public abstract string Description { get; }
        public abstract DateTime CreatedOn { get; }
        public abstract void OnSetup();

        protected virtual IEnumerable<Field> GetFieldsToCapture()
        {
            return null;
        }

        public virtual void Validate() { }

        private List<Field> _capturedData = null;
        public List<Field> Fields
        {
            get
            {
                if (_capturedData == null)
                {
                    var flds = GetFieldsToCapture();
                    _capturedData = (flds != null) ? new List<Field>( flds ) : new List<Field>();
                }
                return _capturedData;
            }
            set { _capturedData = value; }
        }
    }


    [Serializable]
    public class Field
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Mandatory { get; set; }
        public string Value { get; set; }
    }
}
