﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers.Tasks
{
    public class RunTasksResult
    {
        internal RunTasksResult()
        {
            RanTasks = new List<string>();
        }
        public bool IsSuccess { get; set; }
        public string FailedTask { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> RanTasks { get; set; } 
    }
}
