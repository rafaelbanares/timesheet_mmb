﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers.Tasks
{
    public interface IStartupTask
    {
        void OnStart();
    }
}
