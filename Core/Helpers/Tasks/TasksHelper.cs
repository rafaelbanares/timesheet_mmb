﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Security.Principal;

namespace Core.Helpers.Tasks
{
    [Serializable]
    public class DoneTask
    {
        public string Type { get; set; }
        public DateTime On { get; set; }
        public string By { get; set; }
        public Field[] Params { get; set; }
    }

    public static class TasksHelper
    {
        static readonly string TasksDirectory = Path.Combine(Core.Constants.Directories.RootDirectory, "Tasks");
        static TasksHelper()
        {
            if (!Directory.Exists(TasksDirectory)) Directory.CreateDirectory(TasksDirectory);

        }
        public static List<DoneTask> GetDoneTasks()
        {
            List<DoneTask> res = new List<DoneTask>();
            var done = (DoneTask[])SerializationHelper.BinaryDeserializeFromDisk(Path.Combine(TasksDirectory, Environment.ApplicationName + ".dat"));
            if (done != null) res.AddRange(done);
            return res;
        }

        static void MarkAsDone(ISetupTask task)
        {
            MarkAsDone(new List<ISetupTask>() { task });
        }

        static void MarkAsDone(IEnumerable<ISetupTask> tasks)
        {
            var actual = GetDoneTasks();

            actual.AddRange(
                tasks.Select(
                    x => new DoneTask { On = DateTime.UtcNow, Type = x.GetType().FullName, Params = x.Fields.ToArray() }
                    ));

            SerializationHelper.BinarySerializeToDisk(actual.ToArray(), Path.Combine(TasksDirectory, Environment.ApplicationName + ".dat"));
        }

        public static IEnumerable<ISetupTask> GetRemainingTasks()
        {
            var done = GetDoneTasks().Select(x => x.Type);

            return typeof(ISetupTask)
                            .GetMatchingTypes(x => !x.IsAbstract || x.IsInterface)
                            .GetOne<ISetupTask>()
                            .Where(x => !done.Contains(x.GetType().FullName))
                            .OrderBy(x => x.CreatedOn);
        }

        public static RunTasksResult RunSetupTasks()
        {
            return RunSetupTasks(null);
        }

        public static RunTasksResult RunSetupTasks(IEnumerable<ISetupTask> configured)
        {

            RunTasksResult result = new RunTasksResult();
            result.IsSuccess = true;
            var currentTask = string.Empty;

            Trace.WriteLine("*******************");
            Trace.WriteLine("Running Setup Tasks");
            Trace.WriteLine("*******************");
            Trace.Indent();
            var allTasksToBeDone = Merge(TasksHelper.GetRemainingTasks(), configured);
            WindowsIdentity winId = (WindowsIdentity)ContextHelper.GetUserIdentity();
            WindowsImpersonationContext ctx = null;
            try
            {
                // Start impersonating
                ctx = winId.Impersonate();
                foreach (var task in allTasksToBeDone.OrderBy(c => c.CreatedOn))
                {
                    currentTask = task.GetType().FullName;
                    Trace.WriteLine("");
                    Trace.WriteLine("Running task: " + currentTask);
                    Trace.WriteLine("--------------");
                    task.OnSetup();
                    Trace.WriteLine("success: " + currentTask);
                    MarkAsDone(task);
                    result.RanTasks.Add(currentTask);
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.FailedTask = currentTask;
                result.ErrorMessage = ex.Format();
                Trace.WriteLine("failed");
                ex.Log();
            }
            finally
            {
                if (ctx != null) ctx.Undo();
            }
            return result;
        }

        static IEnumerable<ISetupTask> Merge(IEnumerable<ISetupTask> notConfigured, IEnumerable<ISetupTask> configured)
        {
            var result = notConfigured.ToList();
            if (configured != null)
            {
                configured.ForEach(c =>
                {
                    var i = result.IndexOf(result.Where(x => x.Name == c.Name && x.CreatedOn == c.CreatedOn).First());
                    result[i] = c;
                });
            }
            return result;
        }

        public static void RunStartupTasks()
        {
            var tasks = typeof(IStartupTask)
                            .GetMatchingTypes(x => !x.IsAbstract || !x.IsInterface)
                            .GetOne<IStartupTask>()
                            ;
            tasks.ForEach(x =>
                {
                    x.OnStart();
                });
        }
    }
}
