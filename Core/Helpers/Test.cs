﻿using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Remoting.Messaging;
using Castle.DynamicProxy;
using System.Collections.Generic;
using Core.Helpers;
using System.ComponentModel;
namespace AttributesAndInterception
{
    #region Interfaces
    /// Simple interface to describe what a Change Tracker looks like
    public interface IChangeTracker
    {
        void LogChange(string field, string newVal);
    }

    /// This is just a tag to mark input attributes
    public interface IInputAttribute { }

    /// This is just a tag to mark output attributes
    public interface IOutputAttribute { }

    /// le Decorator interface.  Used to enforce high level definition of attributes that are 
    /// used to modify the return or input value in some way.  One would expect the modification
    /// to be related to formatting or something, but it is rather arbitrary.
    public interface IDecorator
    {
        object Parse(object value);
    }
    #endregion

    #region DataClass
    /// This is our example class.
    public class DataClass : MarshalByRefObject, IChangeTracker
    {
        /// Internal change tracking implementation
        public ArrayList _Changes = new ArrayList();
        public ArrayList Changes
        {
            get
            {
                return _Changes;
            }
        }

        /// Handy little method that shows the internal value represented
        /// by the myVal property.  This is done so you can see the difference
        /// between what is stored locally and what comes out of the property
        public string Showval()
        {
            return _myVal;
        }

        /// Our local field of choice
        private string _myVal = string.Empty;

        /// This is the Property of choice.  You can arrange the attributes in one line or however you wish, really.  
        /// I felt they were more readable this way.
        [Required()]
        [FormatValidation(@"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}")]
        [StripNonNumeric()]
        [LogChanges("_myVal")]
        [StringOutputFormat(@"(\d{3})(\d{3})(\d{4})", "$1-$2-$3")]
        public string myVal
        {
            get { return _myVal; }
            set { _myVal = value; }
        }

        /// This is the normal way of handling this sort of thing, 
        /// put all of the logic in the individual property.
        public string myVal_Classic
        {
            get
            {
                return _myVal.Substring(0, 3) + "-" + _myVal.Substring(3, 3) + "-" + _myVal.Substring(7, 4);
            }
            set
            {
                bool isValid = true;
                isValid = isValid && !Validation.isEmpty(value);
                isValid = isValid && Validation.isMatch(value, @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}");

                if (isValid)
                {
                    LogChange("myVal", value);
                    _myVal = Utility.StripNonNumeric(value);
                }
            }
        }

        /// This is our implementation of Change tracking.
        public void LogChange(string field, string newVal)
        {
            FieldInfo fi = GetType().GetField(field);
            if (!(fi == null))
            {
                object currentValue = fi.GetValue(this);
                Changes.Add(new object[] { field, currentValue, newVal });
            }
        }

        /// This is a static factory for this specific class that creates
        /// a new class of this type and passes it to the proxy, then returns to
        /// us a strongly typed transparent proxy.  This isn't really necessary, 
        /// as you can do the same thing from anywhere in your code.  I just 
        /// find this method cleaner. (BTW, if anyone knows how to do the equivalent 
        /// of this.GetType() from a STATIC method, let me know, please)
        public static DataClass Instance()
        {
            return CustomProxy<DataClass>.Instance();
        }
    }
    #endregion

    #region Custom Proxy
    public class CustomProxy<T> : RealProxy, IDisposable where T : MarshalByRefObject
    {
        public CustomProxy()
            : base(typeof(T))
        {
            //We have to attach our object to the proxy.  
            //This isn't an automagic thing, probably because it isn't always relevant.
            AttachServer((MarshalByRefObject)typeof(T).GetOne());
        }

        public CustomProxy(T subject)
            : base(subject.GetType())
        {
            //We have to attach our object to the proxy.  
            //This isn't an automagic thing, probably because it isn't always relevant.
            AttachServer((MarshalByRefObject)subject);
        }

        public void Dispose()
        {
            //For proper cleanup, let's detach our server. 
            //I am sure this is done in GC, but why risk it?
            DetachServer();
        }

        /// Here is the magic of our proxy.
        /// We create a new instance of the class, then get a transparent proxy of our server.
        /// Note that we are returning object, but this is actually of whatever type our obj
        /// parameter is.
        public static T Instance()
        {
            return (T)new CustomProxy<T>().GetTransparentProxy();
        }

        /// In the Handle Method we do our interception work. 
        public override IMessage Invoke(IMessage msg)
        {
            ///the MethodCallMessageWrapper provides read/write access to the method call arguments. 
            MethodCallMessageWrapper mc = new MethodCallMessageWrapper((IMethodCallMessage)msg);

            ///This is the reflected method base of the called method. 
            MethodInfo mi = (MethodInfo)mc.MethodBase;

            ///This is the object we are proxying. 
            MarshalByRefObject owner = GetUnwrappedServer();

            ///Some basic initializations for later use 
            IMessage retval = null;
            bool IsGet = false;
            object outVal = null;
            if (!(owner == null)) //Safety First!
            {
                ///Let's see if the method we have intercepted is an accessor of a property for our object 
                PropertyInfo pi = GetMethodProperty(mi, owner, out IsGet);
                ///if it is null, we are NOT in a property accessor 
                if (!(pi == null))
                {
                    ///IsGet is set by the GetMethodProperty method. It tells us if we  are in the get accessor 
                    if (IsGet)
                    {
                        ///In this implementation, I have  not done any pre or post processing for output attributes.  This could
                        ///be implemented, though to conditionally return something other than the stored value.  As it stands here, we simply
                        ///invoke the get accessor and store the result into our holding variable.
                        outVal = mi.Invoke(owner, mc.Args);

                        //Ok, so we	are in the get accessor, do we have any output attributes assigned to this property? 
                        if (pi.IsDefined(typeof(IOutputAttribute), true))
                        {
                            ///We do indeed! 
                            ///Now we get all of the custom attributes that inherit from IOutputAttribute.
                            IOutputAttribute[] outputattrs = (IOutputAttribute[])pi.GetCustomAttributes(typeof(IOutputAttribute), true);
                            foreach (IOutputAttribute outputattr in outputattrs)
                            {
                                ///Pretty self explanatory here.  Check the types and execute them accordingly.  Calling order
                                ///would be a deterministic thing and would depend on some high level business requirements. (i.e.
                                ///formatters come after security modifiers, etc).
                                if (outputattr is StringOutputFormat) outVal = ((StringOutputFormat)outputattr).Parse(outVal);
                            }
                        }
                    }
                    else
                    {
                        ///This is the else clause for the [if(IsGet)] statement above.  So we are in the set accessor.
                        ///we initialize a local var to the first (and presumably only) input argument for the method call.
                        object inputParm = mc.InArgs[0];

                        ///Since we *can* do validation, we should assume that we are.  This flag defaults to true. The concept here 
                        ///is that we are considered valid unless someone proves us wrong.
                        bool isValid = true;

                        ///So do we have any Validators on this property?
                        if (pi.IsDefined(typeof(ValidationAttributeBase), true))
                        {
                            ///We do indeed!  Let's get them using the base class (don't you love inheritance?).
                            ValidationAttributeBase[] validators = (ValidationAttributeBase[])pi.GetCustomAttributes(typeof(ValidationAttributeBase), true);

                            foreach (ValidationAttributeBase validator in validators)
                            {
                                ///So now we just run the isValid method for each validator and sum it against the existing isValid state.
                                isValid = isValid && validator.isValid(inputParm);
                            }
                        }

                        ///Are we still valid? If we are not, we do nothing.  It's like the set never happened.  Of course, this could
                        ///be handled in other ways.  You could throw and invalid value exception, or store the invalid value and log
                        ///the validation issue, etc.
                        if (isValid)
                        {
                            ///Yes, we are valid.  Now, let's do our other pre-processing stuff

                            ///Do we have any IDecorators?
                            if (pi.IsDefined(typeof(IDecorator), true))
                            {
                                ///Yes, we do, let's get them
                                IDecorator[] decorators = (IDecorator[])pi.GetCustomAttributes(typeof(IDecorator), true);
                                foreach (IDecorator decorator in decorators)
                                {
                                    ///Double checking that the decorator is intended for input processing, then execute it
                                    if (decorator is IInputAttribute) inputParm = decorator.Parse(inputParm);
                                }
                            }

                            ///Do we have a LogChanges attribute and does the owner implement IChangeTracker?
                            if (pi.IsDefined(typeof(LogChanges), true) && owner is IChangeTracker)
                            {
                                ///LogChange is defined in the AtributeUsage to NOT allow multiple.  So if there are ANY, 
                                ///then there will be only one.
                                LogChanges logChange = ((LogChanges[])pi.GetCustomAttributes(typeof(LogChanges), true))[0];
                                logChange.DoLog(((IChangeTracker)owner), inputParm);
                            }

                            ///We could have changed our inputparm, so let's assign the changed value to the arguments array
                            mc.Args[0] = inputParm;

                            //Now, we can invoke out set accessor
                            outVal = mi.Invoke(owner, mc.Args);
                        }
                    }
                }
                ///This is the else for the [if(!(pi == null))] statement above.  This means that the current method 
                ///is NOT a property accessor.
                else
                {
                    ///Pass the call to the method and get our return value
                    outVal = mi.Invoke(owner, mc.Args);
                }
            }

            /// The caller is expecting to get something out of the method invocation.  So we need to construct a return message
            /// This is pretty simple, pass back out most of the stuff that was passed into us, and hand it the outVal as well.
            retval = new ReturnMessage(outVal, mc.Args, mc.Args.Length, mc.LogicalCallContext, mc);
            return retval;
        }


        /// This is a utility function that we use in Handle to get the PropertyInfo for a Method.
        /// Interestingly the property info object knows how to find it's accessors, but the accessors do not
        /// know what property they belong to.
        protected static PropertyInfo GetMethodProperty(MethodInfo methodInfo, object owner, out bool IsGet)
        {
            ///We step through each property in the owner class
            foreach (PropertyInfo aProp in owner.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                MethodInfo mi = null;

                ///first get the get accesssor for the property
                ///if it is the same method info as the methodInfo we passed in, then we know we
                ///have the Get accessor and we mark isGet as such and return our PropertyInfo object.
                mi = aProp.GetGetMethod(true);
                if (mi != null && mi.Equals(methodInfo))
                {
                    IsGet = true;
                    return aProp;
                }

                ///Now we get the set method and check if it matches out methodInfo parameter
                mi = aProp.GetSetMethod(true);
                if (mi != null && mi.Equals(methodInfo))
                {
                    IsGet = false;
                    return aProp;
                }
            }

            ///If we get this far, it's because we did not match anyone's get or set accessor, so we are just a lowly 
            ///normal method with no friends.  So set isGet to false and return null.
            IsGet = false;
            return null;
        }
    }
    #endregion

    #region Attributes

    #region Validation Attributes
    /// This is the abstract base for the validation attributes.
    /// Why did I use a base class instead of the attribute?  because I wanted to define the 
    /// basic Inherited attribute usage parameters on this class one time and not have 
    /// to define it on every class that inherits from here.
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public abstract class ValidationAttributeBase : Attribute, IInputAttribute
    {
        public abstract bool isValid(object value);
    }

    public class RequiredAttribute : ValidationAttributeBase
    {
        public override bool isValid(object value)
        {
            /// We pass through the value to a static utility class to do the generic
            /// isEmpty check. I strongly support this kind of development, btw.  
            /// This provides a central Business/Data rule kind of layer for cross 
            /// context usage.  
            return !Validation.isEmpty(value);
        }
    }

    public class FormatValidationAttribute : ValidationAttributeBase
    {
        string _Format = string.Empty;

        public FormatValidationAttribute(string format)
        {
            _Format = format;
        }

        public override bool isValid(object value)
        {
            return Validation.isMatch(value, _Format);
        }
    }
    #endregion

    /// This is not a Validator, so it inherits straight from the Attribute class.   
    /// If I was intending on having a large collection of decorator attributes, as 
    /// one would expect based on the fact that I defined an interface for it, I would
    /// create a base class like I did for validators.
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class StripNonNumeric : Attribute, IInputAttribute, IDecorator
    {
        public object Parse(object value)
        {
            return Utility.StripNonNumeric(value.ToString());
        }
    }

    /// This is basically for audit or CSLA change history stuff.  Note how this one 
    /// basically passes the functionality to owner object to store it's own changes.
    /// It also enforces that it *CAN* do this by requiring that the owner of the target
    /// field implement the IChangeTracker interface.
    /// Interfaces rock!  :P
    public class LogChanges : Attribute, IInputAttribute
    {
        string _Field = string.Empty;
        public LogChanges(string field)
        {
            _Field = field;
        }

        public void DoLog(IChangeTracker owner, object newVal)
        {
            owner.LogChange(_Field, newVal.ToString());
        }
    }

    /// This is another form of Decorator, this one specifically for output.  I think 
    /// I could have made the decorator handling more generic in the Handle method for
    /// the proxy and used this class for both input and output format decoration, but 
    /// hey, lots of correct ways to do the same thing, right?
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class StringOutputFormat : Attribute, IOutputAttribute, IDecorator
    {
        string _ExtractPattern = string.Empty;
        string _Format = string.Empty;
        public StringOutputFormat(string extractPattern, string format)
        {
            _ExtractPattern = extractPattern;
            _Format = format;
        }

        public object Parse(object value)
        {
            /// This is one of the lovely functions of Regex.  It's like String.Format on steroids.
            return System.Text.RegularExpressions.Regex.Replace(value.ToString(), _ExtractPattern, _Format);
        }
    }

    #endregion

    #region Utility Classes
    public class Validation
    {
        /// This field checks against known types and compare them against the known defaults.  
        /// I only did a few here, presumably, you would want a check for EVERY built in type 
        /// and any custom types you might create.  Also, you could consider making a series 
        /// of NULLABLE types, such as NullableInt32 and NullableBool.  
        public static bool isEmpty(object val)
        {
            bool retval = false;
            try
            {
                if (val is string && val.Equals(string.Empty)) retval = true;
                if (val is Guid && val.Equals(Guid.Empty)) retval = true;
                if (val is DateTime && val.Equals(DateTime.MinValue)) retval = true;
            }
            catch
            {
                retval = false;
            }
            return retval;
        }


        /// Simple regex match
        public static bool isMatch(object val, string format)
        {
            bool retval = true;
            try
            {
                return System.Text.RegularExpressions.Regex.IsMatch(val.ToString(), format);
            }
            catch
            {
                retval = false;
            }
            return retval;
        }
    }

    public class Utility
    {
        /// This iterates through each character in the passed in value and, if it is not a Digit,
        /// pulls it out of the retval, then returns the retval.  You could do the same thing with 
        /// a RegEx.Replace call, I imagine. 
        public static string StripNonNumeric(string val)
        {
            string retval = val;
            foreach (char c in val.ToCharArray())
            {
                if (!char.IsDigit(c)) retval = retval.Replace(c.ToString(), string.Empty);
            }
            return retval;
        }
    }
    #endregion

    public class Console
    {
        public static void Main(string[] args)
        {
            DataClass dc = DataClass.Instance(); //Create our class
            dc.myVal = "972-555-1212"; //Set the myVal property
            System.Console.WriteLine(dc.Showval()); //show the internal stored value
            System.Console.WriteLine(dc.myVal); //show the Property get version
            System.Console.ReadLine(); //press enter to exit the console
        }
    }
}