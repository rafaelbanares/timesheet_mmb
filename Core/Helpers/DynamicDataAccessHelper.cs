﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Dynamic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Reflection;
using System.ComponentModel;
namespace Core.Helpers
{
    public class DynamicQuery
    {
        DbProviderFactory _factory;
        string _connectionString;

        public DynamicQuery(ConnectionInfos connectionInfos)
        {
            _factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
            _connectionString = connectionInfos.ToConnectionString();
        }

        public DynamicQuery(DbProviderFactory factory, string connectionString)
        {
            _factory = factory;
            _connectionString = connectionString;
        }

        public object Scalar(string sql, params object[] args)
        {
            object result = null;
            using (var conn = OpenConnection())
            {
                result = CreateCommand(sql, conn, args).ExecuteScalar();
                conn.Close();
            }
            return result;
        }

        public void NonQuery(string sql, params object[] args)
        {
            using (var conn = OpenConnection())
            {
                CreateCommand(sql, conn, args).ExecuteNonQuery();
                conn.Close();
            }

        }

        public IEnumerable<T> Query<T>(string sql, params object[] args) where T : class, new()
        {
            return DynamicQ(sql, args)
                .Select(DynamicQuery.To<T>);
        }

        List<dynamic> DynamicQForReal(string sql, params object[] args)
        {
            ////Debug.WriteLine("For Real");
            var res = new List<dynamic>();
            using (var conn = OpenConnection())
            {
                var rdr = CreateCommand(sql, conn, args).ExecuteReader(CommandBehavior.CloseConnection);
                while (rdr.Read())
                {
                    res.Add(rdr.RecordToExpando());
                }
            }
            return res;
        }

        public IEnumerable<dynamic> DynamicQ(string sql, params object[] args)
        {
            ////Debug.WriteLine("");
            //long start = System.Diagnostics.Stopwatch.GetTimestamp();
            return DynamicQForReal(sql, args);
            //var hash = sql.GetHashCode().ToString() ;
            //if(args !=null) args.ForEach(x => x.GetHashCode().ToString());
            //var res = ContextHelper.Get<List<dynamic>>(hash, () => DynamicQForReal(sql, args));
            //var duration = new TimeSpan(System.Diagnostics.Stopwatch.GetTimestamp() - start).TotalMilliseconds;
            //////Debug.Write(duration.ToString() + "ms ---> ");
            ////Debug.WriteLine(sql);
            //return res;
        }

        protected internal DbConnection OpenConnection()
        {
            var conn = _factory.CreateConnection();
            conn.ConnectionString = _connectionString;
            conn.Open();
            return conn;
        }

        public DbCommand CreateCommand(string sql, DbConnection conn, params object[] args)
        {
            DbCommand result = null;
            result = _factory.CreateCommand();
            result.Connection = conn;
            result.CommandText = sql;
            if (args != null && args.Length > 0) result.AddParams(args);
            return result;
        }


        public int Execute(DbCommand command)
        {
            return Execute(new DbCommand[] { command });
        }
        /// <summary>
        /// Executes a series of DBCommands in a transaction
        /// </summary>
        public int Execute(IEnumerable<DbCommand> commands)
        {
            var result = 0;
            using (var conn = OpenConnection())
            {
                using (var tx = conn.BeginTransaction())
                {
                    foreach (var cmd in commands)
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tx;
                        result += cmd.ExecuteNonQuery();
                    }
                    tx.Commit();
                }
                conn.Close();
            }
            return result;
        }

        public static T To<T>(dynamic o) where T :  new()
        {
            var dic = o as IDictionary<string, object>;

            if (dic == null) return default(T);

            var props = typeof(T).GetPublicProperties().Where(x => x.CanWrite);

            var newObject = typeof(T).GetOne<T>();

            foreach (var prop in props)
            {
                object val;
                if (dic.TryGetValue(prop.Name, out val))
                {
                    if (val != null)
                    {
                        try
                        {
                            prop.SetValue(newObject, val, null);
                        }
                        catch
                        {
                            if (prop.PropertyType != val.GetType())
                            {
                                try
                                {
                                    prop.SetValue(newObject, Convert.ChangeType(val, prop.PropertyType), null);
                                }
                                finally { }
                            }
                        }
                    }
                }
            }

            return newObject;
        }
    }

    public class DynamicView : DynamicQuery
    {
        public string DBObjectName { get; protected set; }
        public IEnumerable<string> Keys { get; protected set; }

        public DynamicView(ConnectionInfos connectionInfos, string viewName, string primaryKeyField)
            : base(connectionInfos)
        {
            DBObjectName = viewName == "" ? this.GetType().Name : viewName;
            Keys = primaryKeyField.Split(',').Select(x => x.Trim()).ToArray();
        }

        public IEnumerable<dynamic> All(string where = "", string orderBy = "", int limit = 0, string columns = "*", params object[] args)
        {
            string sql = limit > 0 ? "select top " + limit + " {0} from {1} " : "select {0} from {1} ";
            if (!string.IsNullOrEmpty(where))
                sql += where.Trim().StartsWith("where", StringComparison.CurrentCultureIgnoreCase) ? where : "where " + where;
            if (!String.IsNullOrEmpty(orderBy))
                sql += orderBy.Trim().StartsWith("order by", StringComparison.CurrentCultureIgnoreCase) ? orderBy : " order by " + orderBy;
            return DynamicQ(string.Format(sql, columns, DBObjectName), args);
        }

        public IEnumerable<T> All<T>(string where = "", string orderBy = "", int limit = 0, string columns = "*", params object[] args) where T : class, new()
        {
            return All(where, orderBy, limit, columns, args)
               .Select(DynamicQuery.To<T>);
        }

        public dynamic Paged(string where = "", string orderBy = "", string columns = "*", int pageSize = 20, int currentPage = 1, params object[] args)
        {
            var keyList = Keys.ToString(",");
            var pageStart = (currentPage - 1) * pageSize;

            dynamic result = new ExpandoObject();
            var countSQL = string.Format("SELECT COUNT(1) FROM {0}", DBObjectName);

            if (String.IsNullOrEmpty(orderBy)) orderBy = keyList;

            if (!string.IsNullOrEmpty(where))
            {
                if (!where.Trim().StartsWith("WHERE", StringComparison.CurrentCultureIgnoreCase))
                {
                    where = "WHERE " + where;
                }
            }

            countSQL += where;
            result.TotalRecords = Scalar(countSQL, args);
            result.TotalPages = result.TotalRecords / pageSize;
            if (result.TotalRecords % pageSize > 0) result.TotalPages += 1;

            var sql = string.Format(@"WITH _paged AS(SELECT TOP ({3} * {5}) {0}, ROW_NUMBER() OVER(ORDER BY {6}) AS _RowNum FROM {1} {2} ) SELECT  {0} FROM _paged WHERE _RowNum >= ({5} - 1) * {3} + 1 ORDER BY {6};", columns, DBObjectName, where, pageSize, pageStart, currentPage, orderBy);
            result.Items = DynamicQ(sql, args);
            return result;
        }
    }

    public class DynamicTable : DynamicView
    {
        public IEnumerable<string> ReadOnlyFields { get; set; }

        public DynamicTable(ConnectionInfos connectionInfos, string tableName, string primaryKeyField)
            : base(connectionInfos, tableName, primaryKeyField)
        {
            ReadOnlyFields = Enumerable.Empty<string>();
        }

        public DynamicTable(ConnectionInfos connectionInfos, string tableName, string primaryKeyField, string readonlyFields)
            : base(connectionInfos, tableName, primaryKeyField)
        {
            ReadOnlyFields = readonlyFields.Split(',').Select(x => x.Trim()).ToArray();
        }

        public object Save(object o)
        {
            return Save(o, false);
        }

        public object Save(object o, bool insertOnly)
        {
            dynamic result = 0;
            using (var conn = OpenConnection())
            {
                var cmd = CreateSaveCommand(o, insertOnly);
                cmd.Connection = conn;
                result = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return result;
        }

        public void Delete(object o)
        {
            using (var conn = OpenConnection())
            {
                var cmd = CreateDeleteCommand(o);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public int Delete(string where = "", params object[] args)
        {
            return Execute(CreateDeleteCommand(where: where, args: args));
        }

        public int Truncate()
        {
            return Execute(CreateTruncateCommand());
        }

        private DbCommand CreateSaveCommand(object o, bool insertOnly)
        {
            var properties = o.ToDictionary(true && !o.GetType().IsAnonymous());

            if (!(properties.Count > 0))
                throw new InvalidOperationException("Can't parse this object to the database - there are no properties updateSet");

            if (Keys.Any(k => !properties.Keys.Contains(k)))
                throw new InvalidOperationException("Object does not have all defined keys in dynamic table constructor.");

            var props = o.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(ReadOnlyAttribute), true).Cast<ReadOnlyAttribute>().Select(y => y.IsReadOnly).FirstOrDefault())
                .Select(x => x.Name);

            ReadOnlyFields = ReadOnlyFields.Concat(props);

            if (ReadOnlyFields.IsNotNull())
            {
                var toRemove = properties.Where(y => ReadOnlyFields.Contains(y.Key)).Select(x => x.Key).ToList();
                toRemove.ForEach(y => properties.Remove(y));
            }

            var sbinsertKeys = new List<string>();
            var sbinsertVals = new List<string>();
            var sbupdateSet = new List<string>();
            var sbupdateWhere = new List<string>();

            var existsStub = "EXISTS(SELECT 1 FROM {0} WHERE {1})";
            var insertStub = "INSERT INTO {0} ({1}) VALUES ({2})";
            var updateStub = "UPDATE {0} SET {1} WHERE {2}";

            DbCommand result = CreateCommand(insertStub, null);

            int counter = 0;

            foreach (var item in properties)
            {
                sbinsertKeys.Add(string.Format("[{0}]", item.Key));
                sbinsertVals.Add(string.Format("@{0}", counter.ToString()));
                if (!Keys.Contains(item.Key))
                {
                    sbupdateSet.Add(string.Format("[{0}] = @{1}", item.Key, counter.ToString()));
                }
                else
                {
                    sbupdateWhere.Add(string.Format("[{0}] = @{1}", item.Key, counter.ToString()));
                }

                result.AddParam(item.Value);
                counter++;
            }

            var keys = string.Join(", ", sbinsertKeys.ToArray());
            var vals = string.Join(", ", sbinsertVals.ToArray());

            var updateSet = "";
            if (sbupdateSet.Any())
                updateSet = string.Join(",", sbupdateSet.ToArray());

            var updateWhere = string.Join(" AND ", sbupdateWhere.ToArray());

            var existsSql = string.Format(existsStub, DBObjectName, updateWhere);
            var insertSql = string.Format(insertStub, DBObjectName, keys, vals);
            var updateSql = string.Format(updateStub, DBObjectName, updateSet, updateWhere);

            var finalSql = insertSql;

            if (insertOnly)
            {
                finalSql = string.Format("if (NOT {0}) begin {1} end", existsSql, insertSql);
            }
            else
            {
                if (!string.IsNullOrEmpty(updateSet))
                {
                    finalSql = string.Format("if ({0}) begin {1} end else begin {2} end", existsSql, updateSql, insertSql);
                }
                else
                {
                    // case of link tables
                    if (Keys.Count() == properties.Count)
                    {
                        finalSql = string.Format("if (NOT {0}) begin {1} end", existsSql, insertSql);
                    }
                }
            }

            result.CommandText = finalSql;

            return result;
        }

        private DbCommand CreateDeleteCommand(string where = "", params object[] args)
        {
            where.CanNotBeNullOrEmpty();

            var sql = string.Format("DELETE FROM {0} ", DBObjectName);
            sql += where.Trim().StartsWith("Where", StringComparison.CurrentCultureIgnoreCase) ? where : "WHERE " + where;
            return CreateCommand(sql, null, args);
        }

        private DbCommand CreateDeleteCommand(object o)
        {
            DbCommand result = null;
            var properties = o.ToDictionary();

            if (!(properties.Count > 0))
                throw new InvalidOperationException("Can't parse this object to the database - there are no properties updateSet");

            var stub = "DELETE FROM {0} WHERE {1}";

            var sbWhere = new List<string>();
            result = CreateCommand(stub, null);

            int counter = 0;
            foreach (var item in properties)
            {
                if (Keys.Contains(item.Key))
                {
                    sbWhere.Add(string.Format("[{0}] = @{1}", item.Key, counter.ToString()));
                    result.AddParam(item.Value);
                    counter++;
                }
            }

            var where = string.Join(" AND ", sbWhere.ToArray());

            var sql = string.Format(stub, DBObjectName, where);
            result.CommandText = sql;

            return result;
        }

        private DbCommand CreateTruncateCommand()
        {
            var sql = string.Format("TRUNCATE TABLE {0}", DBObjectName);
            return CreateCommand(sql, null);
        }
    }
}
