﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;
using CSScriptLibrary;
using csscript;
using System.Reflection;
using System.Collections.Concurrent;

namespace Core.Helpers
{
    public static class ScriptHelper
    {
        static ConcurrentDictionary<int, AsmHelper> compiledAssemblies = new ConcurrentDictionary<int, AsmHelper>();

        public static RunScriptResult Run(string script, string method, object[] parameters)
        {
            RunScriptResult result = new RunScriptResult();
            try
            {
                AsmHelper helper = compiledAssemblies.GetOrAdd(script.GetHashCode(), (newHashcode) =>
                    {
                        var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).Select(x => x.Location).ToArray();
                        var dynAssembly = CSScript.LoadCode(script, assemblies);
                        return new AsmHelper(dynAssembly);
                    });

                //var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).Select(x => x.Location).ToArray();
                //var dynAssembly = CSScript.LoadCode(script, assemblies);
                // var helper = new AsmHelper(dynAssembly);
                result.Output = helper.Invoke(method, parameters).ToString();
                result.CompilationResult = ScriptCompilationResult.Success;
            }
            catch (CompilerException x)
            {
                StringBuilder output = new StringBuilder();
                CompilerErrorCollection errors = (CompilerErrorCollection)x.Data["Errors"];
                foreach (CompilerError err in errors)
                {
                    output.AppendLine(String.Format("{0}({1},{2}): {3}",
                                        err.IsWarning ? "Warning" : "Error",
                                        err.Line,
                                        err.Column,
                                        err.ErrorText));
                }
                result.Output = output.ToString();
                result.CompilationResult = ScriptCompilationResult.Failed;
            }
            return result;
        }
    }

    public enum ScriptCompilationResult
    {
        Failed,
        Success
    }

    public class RunScriptResult
    {
        public ScriptCompilationResult CompilationResult { get; internal set; }
        public string Output { get; internal set; }
    }
}


