﻿using System;
using Core.Features;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using System.Diagnostics;
using System.Threading;
using System.IO;
using Core.Helpers.Data;
using System.Data.Common;
using System.Collections;
using System.Data.SQLite;
using Core.Data.Concrete;
using Core.Data;
using Core.Data.Concrete.SQLite;

namespace Core.Helpers.Logging
{
    public static class Logger
    {
        static IDatabase database;
        static ITable2<LogEntry> data;
        static object locker = new object();
        static Logger()
        {
            var path = Path.Combine(Constants.Directories.DataDirectory, "Logs.s3db");
            EnsureFile(path);
            database = new SQLiteDatabase(@"Data Source=" + path);
            EnsureTable();
            data = new DatabaseTable<LogEntry>(database, "LogEntry");
        }


        static void EnsureFile(string path)
        {
            if (!File.Exists(path))
            {
                lock (locker)
                {
                    if (!File.Exists(path)) File.Create(path).Close();
                }
            }
        }

        static void EnsureTable()
        {
            lock (locker)
            {
                var d = new DynamicQuery2(database);
                var createScript = @"create table if not exists LogEntry(Id text primary key, LoggedOn datetime, UserName Text, RunningAs Text, Category Text, Title Text, Message Text, Severity Text);";
                d.NonQuery(createScript);
            }
        }


        public static IEnumerable<LogEntry> All()
        {
            return data.All();
        }

        public static IEnumerable<LogEntry> Where(Func<LogEntry, bool> where)
        {
            return All().Where(where).OrderByDescending(x => x.LoggedOn).ToList();
        }

        static string GetKey(LogEntry entry)
        {
            return entry.LoggedOn.ToString("yyyyMMddHHmmss-" + Stopwatch.GetTimestamp().ToString());
        }

        public static void DropAll()
        {
            DropEntriesOlderThan(0.Seconds());
        }

        public static void DropEntriesOlderThan(TimeSpan span)
        {
            var limit = DateTime.UtcNow.Subtract(span);
            data.Delete("LoggedOn < @0", new object[] { limit });
        }

        public static void Clear()
        {
            data.DeleteAll();
        }

        public static void Save(LogEntry entry)
        {
            entry.LoggedOn = DateTime.UtcNow;
            try
            {
                entry.UserName = Operator.Real;
                entry.RunningAs = (Operator.Current != Operator.Real) ? Operator.Current : null;
            }
            catch
            {
                entry.UserName = "Unknown";
            }
            entry.Id = GetKey(entry);
            try
            {
                data.Insert(entry);
            }
            catch (Exception x)
            {
                x.LogToTrace();
            }
            finally
            {
               
            }
        }

        public static void Log(string title, string message, string category, LogSeverity severity)
        {
            //Core.Helpers.ThreadUtil.FireAndForget(() =>
            Core.Helpers.ThreadUtil.Queue(() =>
                Save(new LogEntry { Category = category, Title = title, Message = message, Severity = severity.ToString() })
            );
        }

        public static void Log(string title, string message, LogSeverity severity)
        {
            Log(title, message, Assembly.GetExecutingAssembly().FullName, severity);
        }

        public static void Error(string title, string message, string category)
        {
            Log(title, message, category, LogSeverity.Error);
        }

        public static void Error(string title, string message)
        {
            Log(title, message, LogSeverity.Error);
        }

        public static void Error(string title)
        {
            Log(title, string.Empty, LogSeverity.Error);
        }

        public static void Info(string title, string message, string category)
        {
            Log(title, message, category, LogSeverity.Information);
        }

        public static void Info(string title, string message)
        {
            Log(title, message, LogSeverity.Information);
        }

        public static void Info(string title)
        {
            Log(title, string.Empty, LogSeverity.Information);
        }

        public static void Debug(string title, string message, string category)
        {
            Log(title, message, category, LogSeverity.Debug);
        }

        public static void Debug(string title, string message)
        {
            Log(title, message, LogSeverity.Debug);
        }

        public static void Debug(string title)
        {
            Log(title, string.Empty, LogSeverity.Debug);
        }

        public static LogEntry Get(string id)
        {
            return data.Query("Id = @0", new object[] { id }).FirstOrDefault();
        }
    }
}
