﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers.Logging
{
    [Serializable]
    public class LogEntry
    {
        public string Id { get; set; }
        public DateTime LoggedOn { get; set; }
        public string UserName { get; set; }
        public string RunningAs { get; set; }
        public string Category { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Severity { get; set; }
    }
}
