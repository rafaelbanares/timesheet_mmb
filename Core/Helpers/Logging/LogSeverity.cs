using System;

namespace Core.Helpers.Logging
{
    [Serializable]
    public enum LogSeverity
    {
        Debug,
        Information,
        Warning,
        Error,
        Critical,
        Security
    }
}