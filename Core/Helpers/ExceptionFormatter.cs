using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Core
{
    /// <summary>
    /// Exception formatter class.
    /// </summary>
    public class ExceptionFormatter
    {
        private readonly StringBuilder _writer;
        private int _innerDepth;

        private readonly Exception _exception;

        private static readonly ArrayList IgnoredProperties =
            new ArrayList(new[] {"Source", "Message", "HelpLink", "InnerException", "StackTrace"});

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="ExceptionFormatter"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        protected ExceptionFormatter(Exception exception)
        {
            if (exception == null) throw new ArgumentNullException("exception");
            _writer = new StringBuilder();
            _exception = exception;
        }

        /// <summary>
        /// Gets the writer.
        /// </summary>
        /// <value>The writer.</value>
        protected StringBuilder Writer
        {
            get { return _writer; }
        }

        /// <summary>
        /// Get Exception property.
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
        }

        /// <summary>
        /// Static Format exception method.
        /// </summary>
        /// <param name="exception"><see cref="Exception"/></param>
        /// <returns>String</returns>
        public static string Format(Exception exception)
        {
            var formatter = new ExceptionFormatter(exception);
            formatter.WriteException(exception, null);
            return formatter.Writer.ToString();
        }

        /// <summary>
        /// Writes the exception.
        /// </summary>
        /// <param name="exceptionToFormat">The exception to format.</param>
        /// <param name="outerException">The outer exception.</param>
        protected void WriteException(Exception exceptionToFormat, Exception outerException)
        {
            if (exceptionToFormat == null) throw new ArgumentNullException("exceptionToFormat");

            WriteExceptionType(exceptionToFormat.GetType());
            WriteMessage(exceptionToFormat.Message);
            WriteSource(exceptionToFormat.Source);
            WriteReflectionInfo(exceptionToFormat);
            WriteStackTrace(exceptionToFormat.StackTrace);


            var inner = exceptionToFormat.InnerException;

            if (inner != null)
            {
                // recursive call
                Writer.AppendLine("Inner Exception:");
                _innerDepth++;
                WriteException(inner, exceptionToFormat);
                _innerDepth--;
            }
        }

        /// <summary>
        /// Writes the reflection info.
        /// </summary>
        /// <param name="exceptionToFormat">The exception to format.</param>
        protected void WriteReflectionInfo(Exception exceptionToFormat)
        {
            if (exceptionToFormat == null) throw new ArgumentNullException("exceptionToFormat");

            var type = exceptionToFormat.GetType();
            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);
            object value;

            foreach (var property in
                properties.Where(prop => prop.CanRead && IgnoredProperties.IndexOf(prop.Name) == -1))
            {
                try
                {
                    value = property.GetValue(exceptionToFormat, null);
                }
                catch (TargetInvocationException)
                {
                    value = "Property not accessible, target invocation failed";
                }
                WritePropertyInfo(property, value);
            }

            foreach (var field in fields)
            {
                try
                {
                    value = field.GetValue(exceptionToFormat);
                }
                catch (TargetInvocationException)
                {
                    value = "Field not accessible, target invocation failed";
                }
                WriteFieldInfo(field, value);
            }
        }

        /// <summary>
        /// Writes the type of the exception.
        /// </summary>
        /// <param name="exceptionType">Type of the exception.</param>
        protected void WriteExceptionType(Type exceptionType)
        {
            IndentAndWriteLine("Type: {0}", exceptionType.FullName);
        }

        /// <summary>
        /// Writes the message.
        /// </summary>
        /// <param name="message">The message.</param>
        protected void WriteMessage(string message)
        {
            IndentAndWriteLine("Message: {0}", message);
        }

        /// <summary>
        /// Writes the source.
        /// </summary>
        /// <param name="source">The source.</param>
        protected void WriteSource(string source)
        {
            IndentAndWriteLine("Source: {0}", source);
        }

        /// <summary>
        /// Writes the property info.
        /// </summary>
        /// <param name="propertyInfo">The property info.</param>
        /// <param name="value">The value.</param>
        protected void WritePropertyInfo(PropertyInfo propertyInfo, object value)
        {
            Indent();
            Writer.Append(propertyInfo.Name);
            Writer.Append(" : ");
            Writer.AppendLine((value != null) ? value.ToString() : "NULL");
        }

        /// <summary>
        /// Writes the field info.
        /// </summary>
        /// <param name="fieldInfo">The field info.</param>
        /// <param name="value">The value.</param>
        protected void WriteFieldInfo(FieldInfo fieldInfo, object value)
        {
            Indent();
            Writer.Append(fieldInfo.Name);
            Writer.Append(" : ");
            Writer.AppendLine((value != null) ? value.ToString() : "NULL");
        }

        /// <summary>
        /// Writes the stack trace.
        /// </summary>
        /// <param name="stackTrace">The stack trace.</param>
        protected void WriteStackTrace(string stackTrace)
        {
            Indent();
            Writer.Append("Stack trace:");
            Writer.Append(" : ");
            if (string.IsNullOrEmpty(stackTrace))
            {
                Writer.AppendLine("Unavailable stack trace");
            }
            else
            {
                // The stack trace has all '\n's prepended with a number
                // of tabs equal to the InnerDepth property in order
                // to make the formatting pretty.
                var indentation = new String('\t', _innerDepth);
                var indentedStackTrace = stackTrace.Replace("\n", "\n" + indentation);

                Writer.AppendLine(indentedStackTrace);
                Writer.AppendLine();
            }
        }

        /// <summary>
        /// Indents.
        /// </summary>
        protected virtual void Indent()
        {
            for (var i = 0; i < _innerDepth; i++)
            {
                Writer.Append("\t");
            }
        }

        private void IndentAndWriteLine(string format, params object[] arg)
        {
            Indent();
            _writer.AppendFormat(format, arg);
            _writer.AppendLine();
        }
    }
}