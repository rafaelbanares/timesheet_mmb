﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;

namespace Core
{

    public static class ShareHelper
    {

        /// <summary>
        /// Creates.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <param name="maximumAllowed">The maximum allowed.</param>
        /// <param name="description">The description.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public static Win32Share Create(string path, string name, Win32Share.ShareType type, uint maximumAllowed, string description, string password)
        {
            var mc = new ManagementClass("Win32_Share");
            var parameters = new object[] { path, name, (uint)type, maximumAllowed, description, password, null };

            var result = mc.InvokeMethod("Create", parameters);
            var r = (Win32Share.MethodStatus) Convert.ToUInt32(result);
            switch (r)
            {
                case Win32Share.MethodStatus.Success:
                case Win32Share.MethodStatus.DuplicateShare:
                    return GetNamedShare(name);
            }
            return null;
        }

        /// <summary>
        /// Gets all shares.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Win32Share> GetAllShares()
        {
            var mc = new ManagementClass("Win32_Share");
            var moc = mc.GetInstances();

            return (from ManagementObject mo in moc select new Win32Share(mo));
        }

        /// <summary>
        /// Gets the named share.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Win32Share GetNamedShare(string name)
        {
            var shares = GetAllShares();
            return shares.FirstOrDefault(s => s.Name == name);
        }

        /// <summary>
        /// Delete the named share.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static bool DeleteNamedShare(string name)
        {
            var share = GetNamedShare(name);
            var r = share.Delete();
            return (r == Win32Share.MethodStatus.Success);
        }
    }

    /// <summary>
    /// This class allows to manipulate share files.
    /// </summary>
    public class Win32Share
    {
        /// <summary>
        /// Method status
        /// </summary>
        public enum MethodStatus : uint
        {
            /// <summary>
            /// Success
            /// </summary>
            Success = 0, //Success
            /// <summary>
            /// Access denied
            /// </summary>
            AccessDenied = 2, //Access denied
            /// <summary>
            /// Unknown failure
            /// </summary>
            UnknownFailure = 8, //Unknown failure
            /// <summary>
            /// Invalid name
            /// </summary>
            InvalidName = 9, //Invalid name
            /// <summary>
            /// Invalid level
            /// </summary>
            InvalidLevel = 10, //Invalid level
            /// <summary>
            /// Invalid parameter
            /// </summary>
            InvalidParameter = 21, //Invalid parameter
            /// <summary>
            /// Duplicate share
            /// </summary>
            DuplicateShare = 22, //Duplicate share
            /// <summary>
            /// Redirected path
            /// </summary>
            RedirectedPath = 23, //Redirected path
            /// <summary>
            /// Unknown device or directory
            /// </summary>
            UnknownDevice = 24, //Unknown device or directory
            /// <summary>
            /// Net name not found
            /// </summary>
            NetNameNotFound = 25 //Net name not found
        }

        /// <summary>
        /// Share type
        /// </summary>
        public enum ShareType : uint
        {
            /// <summary>
            /// Disk Drive
            /// </summary>
            DiskDrive = 0x0, //Disk Drive
            /// <summary>
            /// Print Queue
            /// </summary>
            PrintQueue = 0x1, //Print Queue
            /// <summary>
            /// Device
            /// </summary>
            Device = 0x2, //Device
            /// <summary>
            /// IPC
            /// </summary>
            Ipc = 0x3, //IPC
            /// <summary>
            /// Disk Drive Admin
            /// </summary>
            DiskDriveAdmin = 0x80000000, //Disk Drive Admin
            /// <summary>
            /// Print Queue Admin
            /// </summary>
            PrintQueueAdmin = 0x80000001, //Print Queue Admin
            /// <summary>
            /// Device Admin
            /// </summary>
            DeviceAdmin = 0x80000002, //Device Admin
            /// <summary>
            /// IPC Admin
            /// </summary>
            IpcAdmin = 0x80000003 //IPC Admin
        }

        private readonly ManagementObject _mWinShareObject;

        internal Win32Share(ManagementObject obj)
        {
            _mWinShareObject = obj;
        }

        #region Wrap Win32_Share properties

        /// <summary>
        /// Gets the access mask.
        /// </summary>
        /// <value>The access mask.</value>
        public uint AccessMask
        {
            get { return Convert.ToUInt32(_mWinShareObject["AccessMask"]); }
        }

        /// <summary>
        /// Gets a value indicating whether the maximum is allowed.
        /// </summary>
        /// <value><c>true</c> if the maximum is allowed; otherwise, <c>false</c>.</value>
        public bool AllowMaximum
        {
            get { return Convert.ToBoolean(_mWinShareObject["AllowMaximum"]); }
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption
        {
            get { return Convert.ToString(_mWinShareObject["Caption"]); }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return Convert.ToString(_mWinShareObject["Description"]); }
        }

        /// <summary>
        /// Gets the install date.
        /// </summary>
        /// <value>The install date.</value>
        public DateTime InstallDate
        {
            get { return Convert.ToDateTime(_mWinShareObject["InstallDate"]); }
        }

        /// <summary>
        /// Gets the maximum allowed.
        /// </summary>
        /// <value>The maximum allowed.</value>
        public uint MaximumAllowed
        {
            get { return Convert.ToUInt32(_mWinShareObject["MaximumAllowed"]); }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return Convert.ToString(_mWinShareObject["Name"]); }
        }

        /// <summary>
        /// Gets the path.
        /// </summary>
        /// <value>The path.</value>
        public string Path
        {
            get { return Convert.ToString(_mWinShareObject["Path"]); }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status
        {
            get { return Convert.ToString(_mWinShareObject["Status"]); }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>The type.</value>
        public ShareType Type
        {
            get { return (ShareType) Convert.ToUInt32(_mWinShareObject["Type"]); }
        }

        #endregion

        #region Wrap Methods

        /// <summary>
        /// Deletes.
        /// </summary>
        /// <returns></returns>
        public MethodStatus Delete()
        {
            var result = _mWinShareObject.InvokeMethod("Delete", new object[] {});
            var r = Convert.ToUInt32(result);

            return (MethodStatus) r;
        }

        // TODO: Implement here GetAccessMask and SetShareInfo similarly to the above

        #endregion



    }
}