using System;
using Core.Helpers.Reflection;

namespace Core
{
    /// <summary>
    /// Singleton class.
    /// </summary>
    /// <typeparam name="T">Type of object</typeparam>
    public class Singleton<T> : IDisposable where T : class
    {
        private bool _mDisposed;

        /// <summary>
        /// Gets a value indicating whether this <see cref="Singleton&lt;T&gt;"/> is disposed.
        /// </summary>
        /// <value><c>true</c> if disposed; otherwise, <c>false</c>.</value>
        protected bool Disposed
        {
            get
            {
                lock (this)
                {
                    return _mDisposed;
                }
            }
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="Singleton&lt;T&gt;"/> class.
        /// </summary>
        protected Singleton()
        {
        }

        /// <summary>
        /// Static Get Instance property
        /// </summary>
        /// <value>The concreteFeatureType.</value>
        public static T Instance
        {
            get { return SingletonCreator.Instance; }
        }

        /// <summary>
        /// Singleton creator class
        /// </summary>
        private class SingletonCreator
        {
            internal static readonly T Instance = (T)FastObjectFactory.Create(typeof(T));
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, 
        /// or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            lock (this)
            {
                if (_mDisposed == false)
                {
                    Cleanup();
                    _mDisposed = true;

                    GC.SuppressFinalize(this);
                }
            }
        }

        /// <summary>
        /// Cleanups this concreteFeatureType.
        /// </summary>
        protected virtual void Cleanup()
        {
            // override to provide cleanup
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Singleton&lt;T&gt;"/> is reclaimed by garbage collection.
        /// </summary>
        ~Singleton()
        {
            Cleanup();
        }
    }
}