using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Core
{
    /// <summary>
    /// ObservableList class enables default event handlers.
    /// </summary>
    /// <typeparam name="T">Type of object</typeparam>
	public class ObservableList<T> : List<T>
	{
        public delegate void ItemAddingHandler(ObservableList<T> sender, T item, CancelEventArgs args);
        public delegate void ItemAddedHandler(ObservableList<T> sender, T item, int index);
        public delegate void ItemRemovingHandler(ObservableList<T> sender, T item, CancelEventArgs args);
        public delegate void ItemRemovedHandler(ObservableList<T> sender, T item, int index);
        /// <summary>
        /// Event raised when an item has been added.
        /// </summary>
        public event ItemAddedHandler ItemAdded;
        /// <summary>
        /// Event raised when an item has been added.
        /// </summary>
        public event ItemAddingHandler ItemAdding;

        /// <summary>
        /// Event raised when an item has been removed.
        /// </summary>
        public event ItemRemovedHandler ItemRemoved;

        /// <summary>
        /// Event raised when an item has been removed.
        /// </summary>
        public event ItemRemovingHandler ItemRemoving;

		#region IList<T> Members

		public new int IndexOf(T item)
		{
			return base.IndexOf(item);
		}

		public new void Insert(int index, T item)
		{
            if (CanInsert(item))
            {
                base.Insert(index, item);
                if (ItemAdded != null) ItemAdded(this, item, index);
            }
		}

        private bool CanInsert(T item)
        {
            bool result = true;
            if (ItemAdding != null)
            {
                CancelEventArgs args = new CancelEventArgs();
                ItemAdding(this, item, args);
                result = !args.Cancel;
            }
            return result;
        }

        private bool CanRemove(T item)
        {
            bool result = true;
            if (ItemRemoving != null)
            {
                CancelEventArgs args = new CancelEventArgs();
                ItemRemoving(this, item, args);
                result = !args.Cancel;
            }
            return result;
        }

		public new void AddRange(IEnumerable<T> collection)
		{
			foreach (T item in collection) this.Add(item);
		}

		public new int RemoveAll(Predicate<T> match)
		{
			List<T> matchResult = base.FindAll(match);
			foreach (T item in matchResult) this.Remove(item);
			return matchResult.Count;
		}

		public new void InsertRange(int index, IEnumerable<T> collection)
		{
			int i = index;
			foreach (T item in collection)
			{
				this.Insert(i, item);
				i++;
			}
		}

		public new void RemoveRange(int index, int count)
		{
			for (int i = 0; i < count; i++) this.RemoveAt(index);
		}

		public new void RemoveAt(int index)
		{
			T item = this[index];
            if (CanRemove(item))
            {
                base.RemoveAt(index);
                if (ItemRemoved != null) ItemRemoved(this, item, index);
            }
		}

		public new T this[int index]
		{
			get
			{
				return base[index];
			}
			set
			{
				base[index] = value;
			}
		}

		#endregion

		#region ICollection<T> Members

		public new void Add(T item)
		{
			int index = this.Count;
			this.Insert(index, item);
		}

		public new void Clear()
		{
			while (this.Count > 0) this.RemoveAt(0);
		}

		public new bool Contains(T item)
		{
			return base.Contains(item);
		}

		public new void CopyTo(T[] array, int arrayIndex)
		{
			base.CopyTo(array, arrayIndex);
		}

		public new int Count
		{
			get { return base.Count; }
		}

		public new bool Remove(T item)
		{
			this.RemoveAt(base.IndexOf(item));
			return true;
		}

		#endregion

		#region IEnumerable<T> Members

		public new IEnumerator<T> GetEnumerator()
		{
			return base.GetEnumerator();
		}

		#endregion

		public static implicit operator string[](ObservableList<T> list)
		{
			string[] result = new string[list.Count];
			int i = 0;
			foreach (T item in list)
			{
				result[i] = item.ToString();
				i++;
			}
			return result;
		}
	}

}
