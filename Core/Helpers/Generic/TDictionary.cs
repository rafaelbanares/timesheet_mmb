﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Core
{
    /// <summary>
    /// Represents a thread-safe generic dictionary.
    /// </summary>
    /// <typeparam name="TKey">The type of updateSet in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of values in the dictionary.</typeparam>
    public class TDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        #region Variables

        /// <summary>
        /// Lock for the dictionary
        /// </summary>
        private readonly ReaderWriterLockSlim Lock_Dictionary = new ReaderWriterLockSlim();

        /// <summary>
        /// The base dictionary
        /// </summary>
        private readonly Dictionary<TKey, TValue> _mDictionary;

        // Variables

        #endregion

        #region Init

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="TDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        public TDictionary()
        {
            _mDictionary = new Dictionary<TKey, TValue>();
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="TDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity of the dictionary.</param>
        public TDictionary(int capacity)
        {
            _mDictionary = new Dictionary<TKey, TValue>(capacity);
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="TDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="comparer">The comparer to use when comparing updateSet.</param>
        public TDictionary(IEqualityComparer<TKey> comparer)
        {
            _mDictionary = new Dictionary<TKey, TValue>(comparer);
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="TDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary whose updateSet and values are copied to this object.</param>
        public TDictionary(IDictionary<TKey, TValue> dictionary)
        {
            _mDictionary = new Dictionary<TKey, TValue>(dictionary);
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="TDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity of the dictionary.</param>
        /// <param name="comparer">The comparer to use when comparing updateSet.</param>
        public TDictionary(int capacity, IEqualityComparer<TKey> comparer)
        {
            _mDictionary = new Dictionary<TKey, TValue>(capacity, comparer);
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="TDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary whose updateSet and values are copied to this object.</param>
        /// <param name="comparer">The comparer to use when comparing updateSet.</param>
        public TDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
        {
            _mDictionary = new Dictionary<TKey, TValue>(dictionary, comparer);
        }

        // Init

        #endregion

        #region IDictionary<TKey,TValue> Members

        #region GetValueAddIfNotExist

        /// <summary>
        /// Returns the value of <paramref name="key"/>. If <paramref name="key"/>
        /// does not exist, <paramref name="func"/> is performed and added to the 
        /// dictionary
        /// </summary>
        /// <param name="key">the key to check</param>
        /// <param name="func">the delegate to call if key does not exist</param>
        public TValue GetValueAddIfNotExist(TKey key, Func<TValue> func)
        {
            // enter a write lock, to make absolutely sure that the key
            // is added/deleted from the time we check if it exists
            // to the time we add it if it doesn't exist
            Lock_Dictionary.EnterWriteLock();
            try
            {
                TValue rVal;

                // if we have the value, get it and exit
                if (_mDictionary.TryGetValue(key, out rVal))
                    return rVal;

                // not found, so do the function to get the value
                rVal = func.Invoke();

                // add to the dictionary
                _mDictionary.Add(key, rVal);

                // return the value
                return rVal;
            }

            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        // GetValueAddIfNotExist

        #endregion

        #region Add

        /// <summary>
        /// Adds an item to the dictionary
        /// </summary>
        /// <param name="key">the key to add</param>
        /// <param name="value">the value to add</param>
        public void Add(TKey key, TValue value)
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                _mDictionary.Add(key, value);
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                _mDictionary.Add(item.Key, item.Value);
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        // Add

        #endregion

        #region AddIfNotExists

        /// <summary>
        /// Adds the value if it's key does not already exist. Returns
        /// true if the value was added
        /// </summary>
        /// <param name="key">the key to check, add</param>
        /// <param name="value">the value to add if the key does not already exist</param>
        public bool AddIfNotExists(TKey key, TValue value)
        {
            var rVal = false;

            Lock_Dictionary.EnterWriteLock();
            try
            {
                // if not exist, then add it
                if (!_mDictionary.ContainsKey(key))
                {
                    // add the value and updateSet the flag
                    _mDictionary.Add(key, value);
                    rVal = true;
                }
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }

            return rVal;
        }

        /// <summary>
        /// Adds the list of value if the updateSet do not already exist.
        /// </summary>
        /// <param name="updateSet">the updateSet to check, add</param>
        /// <param name="defaultValue">the value to add if the key does not already exist</param>
        public void AddIfNotExists(IEnumerable<TKey> keys, TValue defaultValue)
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                foreach (var key in keys)
                {
                    // if not exist, then add it
                    if (!_mDictionary.ContainsKey(key))
                        _mDictionary.Add(key, defaultValue);
                }
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        // AddIfNotExists

        #endregion

        #region UpdateValueIfKeyExists

        /// <summary>
        /// Updates the value of the key if the key exists. Returns true if updated
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newValue"></param>
        public bool UpdateValueIfKeyExists(TKey key, TValue newValue)
        {
            var rVal = false;

            Lock_Dictionary.EnterWriteLock();
            try
            {
                // if we have the key, then update it
                if (_mDictionary.ContainsKey(key))
                {
                    _mDictionary[key] = newValue;
                    rVal = true;
                }
            }

            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }

            return rVal;
        }

        // UpdateValueIfKeyExists

        #endregion

        #region Contains

        /// <summary>
        /// Returns true if the key value pair exists in the dictionary
        /// </summary>
        /// <param name="item">key value pair to find</param>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            Lock_Dictionary.EnterReadLock();
            try
            {
                return ((_mDictionary.ContainsKey(item.Key)) &&
                        (_mDictionary.ContainsValue(item.Value)));
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }
        }

        // Contains

        #endregion

        #region ContainsKey

        /// <summary>
        /// Returns true if the key exists in the dictionary
        /// </summary>
        /// <param name="key">the key to find in the dictionary</param>
        public bool ContainsKey(TKey key)
        {
            Lock_Dictionary.EnterReadLock();
            try
            {
                return _mDictionary.ContainsKey(key);
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }
        }

        // ContainsKey

        #endregion

        #region ContainsValue

        /// <summary>
        /// Returns true if the dictionary contains this value
        /// </summary>
        /// <param name="value">the value to find</param>
        public bool ContainsValue(TValue value)
        {
            Lock_Dictionary.EnterReadLock();
            try
            {
                return _mDictionary.ContainsValue(value);
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }
        }

        // ContainsValue

        #endregion

        #region Keys

        /// <summary>
        /// Returns the updateSet as a collection
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                Lock_Dictionary.EnterReadLock();
                try
                {
                    return _mDictionary.Keys;
                }
                finally
                {
                    Lock_Dictionary.ExitReadLock();
                }
            }
        }

        // Keys

        #endregion

        #region Remove

        /// <summary>
        /// Removes the element with this key name
        /// </summary>
        /// <param name="key">the key to remove</param>
        public bool Remove(TKey key)
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                return _mDictionary.Remove(key);
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        /// <summary>
        /// Removes the element with this key name and value. Returns
        /// true if the item was removed.
        /// </summary>
        /// <param name="item">the key to remove</param>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                // skip if the key doesn't exist
                TValue tempVal;
                if (!_mDictionary.TryGetValue(item.Key, out tempVal))
                    return false;

                // skip if the value's don't match
                if (!tempVal.Equals(item.Value))
                    return false;

                return _mDictionary.Remove(item.Key);
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        /// <summary>
        /// Removes items from the dictionary that match a pattern. Returns true
        /// on success
        /// </summary>
        /// <param name="predKey">Optional expression based on the updateSet</param>
        /// <param name="predValue">Option expression based on the values</param>
        public bool Remove(Predicate<TKey> predKey, Predicate<TValue> predValue)
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                // exit if no updateSet
                if (_mDictionary.Keys.Count == 0)
                    return true;

                // holds the list of items to be deleted
                var deleteList = new List<TKey>();

                // process updateSet
                foreach (var key in _mDictionary.Keys)
                {
                    var isMatch = false;

                    // add the item to the list if it matches the predicate
                    if (predKey != null)
                        isMatch = (predKey(key));

                    // if this item's value matches, add it
                    if ((!isMatch) && (predValue != null) && (predValue(_mDictionary[key])))
                        isMatch = true;

                    // add to the list if we have a match
                    if (isMatch)
                        deleteList.Add(key);
                }

                // delete all the items from the list
                foreach (var item in deleteList)
                    _mDictionary.Remove(item);
            }

            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }

            return true;
        }

        // Remove

        #endregion

        #region TryGetValue

        /// <summary>
        /// Attemtps to return the value found at element <paramref name="key"/>
        /// If no value is found, returns false
        /// </summary>
        /// <param name="key">the key to find</param>
        /// <param name="value">the value returned if the key is found</param>
        public bool TryGetValue(TKey key, out TValue value)
        {
            Lock_Dictionary.EnterReadLock();
            try
            {
                return _mDictionary.TryGetValue(key, out value);
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }
        }

        // TryGetValue

        #endregion

        #region Values

        /// <summary>
        /// Returns a collection of the values in the dictionary
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                Lock_Dictionary.EnterReadLock();
                try
                {
                    return _mDictionary.Values;
                }
                finally
                {
                    Lock_Dictionary.ExitReadLock();
                }
            }
        }

        // Values

        #endregion

        #region this

        /// <summary>
        /// Gets or sets the <see cref="TValue"/> with the specified key.
        /// </summary>
        /// <value></value>
        public TValue this[TKey key]
        {
            get
            {
                Lock_Dictionary.EnterReadLock();
                try
                {
                    return _mDictionary[key];
                }
                finally
                {
                    Lock_Dictionary.ExitReadLock();
                }
            }
            set
            {
                Lock_Dictionary.EnterWriteLock();
                try
                {
                    _mDictionary[key] = value;
                }
                finally
                {
                    Lock_Dictionary.ExitWriteLock();
                }
            }
        }

        // this

        #endregion

        #region Reload

        /// <summary>
        /// Clears the dictionary
        /// </summary>
        public void Clear()
        {
            Lock_Dictionary.EnterWriteLock();
            try
            {
                _mDictionary.Clear();
            }
            finally
            {
                Lock_Dictionary.ExitWriteLock();
            }
        }

        // Reload

        #endregion

        #region CopyTo

        /// <summary>
        /// Copies the items of the dictionary to a key value pair array
        /// </summary>
        /// <param name="array">the key value pair collection to copy into</param>
        /// <param name="arrayIndex">the index to begin copying to</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            Lock_Dictionary.EnterReadLock();
            try
            {
                _mDictionary.ToArray().CopyTo(array, arrayIndex);
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }
        }

        // CopyTo

        #endregion

        #region Count

        /// <summary>
        /// Returns the number of items in the dictionary
        /// </summary>
        public int Count
        {
            get
            {
                Lock_Dictionary.EnterReadLock();
                try
                {
                    return _mDictionary.Count;
                }
                finally
                {
                    Lock_Dictionary.ExitReadLock();
                }
            }
        }

        // Count

        #endregion

        #region IsReadOnly

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
        /// </returns>
        public bool IsReadOnly
        {
            get { return false; }
        }

        // IsReadOnly

        #endregion

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            Dictionary<TKey, TValue> localDict;

            Lock_Dictionary.EnterReadLock();
            try
            {
                // pop local
                localDict = new Dictionary<TKey, TValue>(_mDictionary);
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }

            // get the enumerator
            return ((IEnumerable<KeyValuePair<TKey, TValue>>) localDict).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            Dictionary<TKey, TValue> localDict;

            Lock_Dictionary.EnterReadLock();
            try
            {
                // pop local
                localDict = new Dictionary<TKey, TValue>(_mDictionary);
            }
            finally
            {
                Lock_Dictionary.ExitReadLock();
            }

            // get the enumerator
            return localDict.GetEnumerator();
        }

        #endregion
    }
}