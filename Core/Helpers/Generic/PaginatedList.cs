﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Paginated list of elements
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    [DataContract]
    public class PaginatedList<TEntity> : IPaginatedCollectionExtended<object>, IPaginatedCollectionExtended, IEnumerable<TEntity>
    {
        private List<TEntity> _collection;

        /// <summary>
        /// Gets the collection.
        /// </summary>
        /// <value>The collection.</value>
        [DataMember]
        public List<TEntity> Collection
        {
            get { return _collection ?? (_collection = new List<TEntity>()); }
        }

        /// <summary>
        /// Gets or sets the informations.
        /// </summary>
        /// <value>The informations.</value>
        public object Informations { get; set; }

        /// <summary>
        /// Gets the object informations.
        /// </summary>
        /// <value>The object informations.</value>
        public object ObjectInformations
        {
            get { return Informations; }
        }

        /// <summary>
        /// Gets the object collection.
        /// </summary>
        /// <value>The object collection.</value>
        public List<object> ObjectCollection
        {
            get { return _collection.Cast<object>().ToList(); }
        }

        /// <summary>
        /// Gets the number of elements in the collection
        /// </summary>
        public int CollectionCount
        {
            get
            {
                if (_collection == null)
                    _collection = new List<TEntity>();
                return _collection.Count;
            }
        }

        /// <summary>
        /// Gets the current page index
        /// </summary>
        [DataMember]
        public int PageIndex { get; private set; }

        /// <summary>
        /// Gets the page size
        /// </summary>
        [DataMember]
        public int PageSize { get; private set; }

        /// <summary>
        /// Gets the Total number of elements
        /// </summary>
        [DataMember]
        public int TotalCount { get; private set; }

        /// <summary>
        /// Gets the total number of pages
        /// </summary>
        [DataMember]
        public int TotalPages { get; private set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PaginatedList()
        {
            _collection = new List<TEntity>();
        }

        /// <summary>
        /// Initializing constructor
        /// </summary>
        /// <param name="list">List of <see cref="TEntity" /></param>
        /// <param name="pageSize">page size</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="totalCount">total number of elements</param>
        public PaginatedList(IEnumerable<TEntity> list, int pageSize, int pageIndex, int totalCount)
            : this()
        {
            if (_collection == null)
                _collection = new List<TEntity>();

            if (list != null)
                _collection.AddRange(list);

            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPages = (int) Math.Ceiling((double) totalCount/pageSize);
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }
    }

    /// <summary>
    /// Paginated list of elements with additionnal informations
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TInformation"></typeparam>
    [DataContract]
    public class PaginatedList<TEntity, TInformation>
        : IPaginatedCollection<TEntity>, IPaginatedCollection,
          IPaginatedCollectionExtended<TInformation>, IPaginatedCollectionExtended
        where TInformation : class, new()
    {
        private List<TEntity> _collection;
        private TInformation _information;

        /// <summary>
        /// Gets the collection.
        /// </summary>
        /// <value>The collection</value>
        [DataMember]
        public List<TEntity> Collection
        {
            get { return _collection ?? (_collection = new List<TEntity>()); }
        }

        /// <summary>
        /// Gets or sets the informations.
        /// </summary>
        /// <value>The informations.</value>
        [DataMember]
        public TInformation Informations
        {
            get { return _information ?? (_information = new TInformation()); }
            set { _information = value; }
        }

        /// <summary>
        /// Gets the object collection.
        /// </summary>
        /// <value>The object collection.</value>
        public List<object> ObjectCollection
        {
            get { return _collection.Cast<object>().ToList(); }
        }

        /// <summary>
        /// Gets the object informations.
        /// </summary>
        /// <value>The object informations.</value>
        public object ObjectInformations
        {
            get { return _information; }
        }

        /// <summary>
        /// Gets the number of elements in the collection
        /// </summary>
        public int CollectionCount
        {
            get
            {
                if (_collection == null)
                    _collection = new List<TEntity>();
                return _collection.Count;
            }
        }

        /// <summary>
        /// Gets the current page index
        /// </summary>
        [DataMember]
        public int PageIndex { get; private set; }

        /// <summary>
        /// Gets the page size
        /// </summary>
        [DataMember]
        public int PageSize { get; private set; }

        /// <summary>
        /// Gets the Total number of elements
        /// </summary>
        [DataMember]
        public int TotalCount { get; private set; }

        /// <summary>
        /// Gets the total number of pages
        /// </summary>
        [DataMember]
        public int TotalPages { get; private set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PaginatedList()
        {
            _collection = new List<TEntity>();
            _information = new TInformation();
        }

        /// <summary>
        /// Initializing constructor
        /// </summary>
        /// <param name="list">List of <see cref="TEntity" /></param>
        /// <param name="pageSize">page size</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="totalCount">total number of elements</param>
        public PaginatedList(IEnumerable<TEntity> list, int pageSize, int pageIndex, int totalCount)
            : this()
        {
            if (list != null)
                _collection.AddRange(list);

            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPages = (int) Math.Ceiling((double) totalCount/pageSize);
        }

        /// <summary>
        /// Initializing constructor
        /// </summary>
        /// <param name="list">List of <see cref="TEntity" /></param>
        /// <param name="pageSize">page size</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="totalCount">total number of elements</param>
        /// <param name="information"></param>
        public PaginatedList(IEnumerable<TEntity> list, int pageSize, int pageIndex, int totalCount,
                             TInformation information)
            : this()
        {
            if (list != null)
                _collection.AddRange(list);

            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPages = (int) Math.Ceiling((double) totalCount/pageSize);

            _information = information;
        }
    }
}