﻿using System.Collections.Generic;

namespace Core
{
    /// <summary>
    /// Paginated collection interface
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IPaginatedCollection<TEntity>
    {
        /// <summary>
        /// Gets the collection fo elements
        /// </summary>
        List<TEntity> Collection { get; }
    }

    public interface IPaginatedCollection
    {
        List<object> ObjectCollection { get; }

        /// <summary>
        /// Gets the number of ellemts in the collection
        /// </summary>
        int CollectionCount { get; }

        /// <summary>
        ///  Gets the current page index
        /// </summary>
        int PageIndex { get; }

        /// <summary>
        /// Gets the page size
        /// </summary>
        int PageSize { get; }

        /// <summary>
        /// Gets the total number of elemets
        /// </summary>
        int TotalCount { get; }

        /// <summary>
        /// Gets the total number of pages
        /// </summary>
        int TotalPages { get; }
    }

    /// <summary>
    /// Paginated collection interface
    /// </summary>
    /// <typeparam name="TInformation" />
    public interface IPaginatedCollectionExtended<TInformation>
    {
        TInformation Informations { get; set; }
    }

    public interface IPaginatedCollectionExtended
    {
        List<object> ObjectCollection { get; }

        object ObjectInformations { get; }

        /// <summary>
        /// Gets the number of ellemts in the collection
        /// </summary>
        int CollectionCount { get; }

        /// <summary>
        ///  Gets the current page index
        /// </summary>
        int PageIndex { get; }

        /// <summary>
        /// Gets the page size
        /// </summary>
        int PageSize { get; }

        /// <summary>
        /// Gets the total number of elemets
        /// </summary>
        int TotalCount { get; }

        /// <summary>
        /// Gets the total number of pages
        /// </summary>
        int TotalPages { get; }
    }
}