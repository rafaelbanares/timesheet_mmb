﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace Core
{
    /// <summary>
    /// Represents a threaded List<>
    /// </summary>
    public class TList<T> : ICollection<T>
    {
        #region Variables

        /// <summary>
        /// The container list that holds the actual frameworkDatabase
        /// </summary>
        private readonly List<T> _mTList;

        /// <summary>
        /// The lock used when accessing the list
        /// </summary>
        private readonly ReaderWriterLockSlim _lockList = new ReaderWriterLockSlim();

        // Variables

        #endregion

        #region Properties

        private int _mDisposed;

        /// <summary>
        /// When true, we have already disposed of the object
        /// </summary>
        private bool Disposed
        {
            get { return Thread.VolatileRead(ref _mDisposed) == 1; }
            set { Thread.VolatileWrite(ref _mDisposed, value ? 1 : 0); }
        }

        // Properties

        #endregion

        #region Init

        /// <summary>
        /// Creates an empty threaded list object
        /// </summary>
        public TList()
        {
            _mTList = new List<T>();
        }

        /// <summary>
        /// Creates an empty threaded list object
        /// </summary>
        /// <param name="capacity">the number of elements the list can initially store</param>
        public TList(int capacity)
        {
            _mTList = new List<T>(capacity);
        }

        /// <summary>
        /// Creates an empty threaded list object
        /// </summary>
        /// <param name="collection">a collection of objects which are copied into the threaded list</param>
        public TList(IEnumerable<T> collection)
        {
            _mTList = new List<T>(collection);
        }

        // Init

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator to iterate through the collection
        /// </summary>
        public IEnumerator GetEnumerator()
        {
            // http://www.csharphelp.com/archives/archive181.html

            List<T> localList;

            // init enumerator
            _lockList.EnterReadLock();
            try
            {
                // create a copy of m_TList
                localList = new List<T>(_mTList);
            }
            finally
            {
                _lockList.ExitReadLock();
            }

            // get the enumerator
            return localList.GetEnumerator();
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>
        /// Returns an enumerator to iterate through the collection
        /// </summary>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            // http://www.csharphelp.com/archives/archive181.html

            List<T> localList;

            // init enumerator
            _lockList.EnterReadLock();
            try
            {
                // create a copy of m_TList
                localList = new List<T>(_mTList);
            }
            finally
            {
                _lockList.ExitReadLock();
            }

            // get the enumerator
            return ((IEnumerable<T>) localList).GetEnumerator();
        }

        #endregion

        #region IDisposable Members

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        /// <summary>
        /// Dispose of the object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (Disposed) return;

            // If disposing equals true, dispose all managed
            // and unmanaged resources.
            if (disposing)
            {
                // Dispose managed resources.
            }

            // Note disposing has been done.
            Disposed = true;
        }

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~TList()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds an item to the threaded list
        /// </summary>
        /// <param name="item">the item to add to the end of the collection</param>
        public void Add(T item)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Add(item);
            }

            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        // Add

        #endregion

        #region AddRange

        /// <summary>
        /// Adds the elements of collection to the end of the threaded list
        /// </summary>
        /// <param name="collection">the collection to add to the end of the list</param>
        public void AddRange(IEnumerable<T> collection)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.AddRange(collection);
            }

            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        // AddRange

        #endregion

        #region AddIfNotExist

        /// <summary>
        /// Adds an item to the threaded list if it is not already in the list.
        /// Returns true if added to the list, false if the item already existed
        /// in the list
        /// </summary>
        /// <param name="item">the item to add to the end of the collection</param>
        public bool AddIfNotExist(T item)
        {
            _lockList.EnterWriteLock();
            try
            {
                // check if it exists already
                if (_mTList.Contains(item))
                    return false;

                // add the item and return true
                _mTList.Add(item);
                return true;
            }

            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        // AddIfNotExist

        #endregion

        #region AsReadOnly

        /// <summary>
        /// Returns a read-only collection of the current threaded list
        /// </summary>
        public ReadOnlyCollection<T> AsReadOnly()
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.AsReadOnly();
            }

            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // AsReadOnly

        #endregion

        #region BinarySearch

        /// <summary>
        /// Searches the collection using the default comparator and returns the zero-based index of the item found
        /// </summary>
        /// <param name="item">the item to search for</param>
        public int BinarySearch(T item)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.BinarySearch(item);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Searches the collection using the default comparator and returns the zero-based index of the item found
        /// </summary>
        /// <param name="item">the item to search for</param>
        /// <param name="comparer">the IComparer to use when searching, or null to use the default</param>
        public int BinarySearch(T item, IComparer<T> comparer)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.BinarySearch(item, comparer);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Searches the collection using the default comparator and returns the zero-based index of the item found
        /// </summary>
        /// <param name="index">the zero-based index to start searching from</param>
        /// <param name="count">the number of records to search</param>
        /// <param name="item">the item to search for</param>
        /// <param name="comparer">the IComparer to use when searching, or null to use the default</param>
        public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.BinarySearch(index, count, item, comparer);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // BinarySearch

        #endregion

        #region Capacity

        /// <summary>
        /// Gets or sets the initial capacity of the list
        /// </summary>
        public int Capacity
        {
            get
            {
                _lockList.EnterReadLock();
                try
                {
                    return _mTList.Capacity;
                }

                finally
                {
                    _lockList.ExitReadLock();
                }
            }
            set
            {
                _lockList.EnterWriteLock();
                try
                {
                    _mTList.Capacity = value;
                }

                finally
                {
                    _lockList.ExitWriteLock();
                }
            }
        }

        // Capacity

        #endregion

        #region Reload

        /// <summary>
        /// Removes all items from the threaded list
        /// </summary>
        public void Clear()
        {
            _lockList.EnterReadLock();
            try
            {
                _mTList.Clear();
            }

            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // Reload

        #endregion

        #region Contains

        /// <summary>
        /// Returns true if the collection contains this item
        /// </summary>
        /// <param name="item">the item to find in the collection</param>
        public bool Contains(T item)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.Contains(item);
            }

            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // Contains

        #endregion

        #region ConvertAll

        /// <summary>
        /// Converts the elements of the threaded list to another type, and returns a list of the new type
        /// </summary>
        /// <typeparam name="TOutput">the destination type</typeparam>
        /// <param name="converter">delegate to convert the items to a new type</param>
        public List<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.ConvertAll(converter);
            }

            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // ConvertAll

        #endregion

        #region CopyTo

        /// <summary>
        /// Copies the elements of this threaded list to a one-dimension array of the same type
        /// </summary>
        /// <param name="array">the destination array</param>
        /// <param name="arrayIndex">index at which copying begins</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            _lockList.EnterReadLock();
            try
            {
                _mTList.CopyTo(array, arrayIndex);
            }

            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // CopyTo

        #endregion

        #region Count

        /// <summary>
        /// Returns a count of the number of elements in this collection
        /// </summary>
        public int Count
        {
            get
            {
                _lockList.EnterReadLock();
                try
                {
                    return _mTList.Count;
                }

                finally
                {
                    _lockList.ExitReadLock();
                }
            }
        }

        // Count

        #endregion

        #region Exists

        /// <summary>
        /// Determines whether an item exists which meets the match criteria
        /// </summary>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public bool Exists(Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.Exists(match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // Exists

        #endregion

        #region Search

        /// <summary>
        /// Searches for an element that matches the criteria
        /// </summary>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public T Find(Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.Find(match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // Search

        #endregion

        #region FindAll

        /// <summary>
        /// Searches for elements that match the criteria
        /// </summary>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public List<T> FindAll(Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindAll(match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // FindAll

        #endregion

        #region FindIndex

        /// <summary>
        /// Returns the index of the element which matches the criteria
        /// </summary>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public int FindIndex(Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindIndex(match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the index of the element which matches the criteria
        /// </summary>
        /// <param name="startIndex">the zero-based index starting the search</param>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public int FindIndex(int startIndex, Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindIndex(startIndex, match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the index of the element which matches the criteria
        /// </summary>
        /// <param name="startIndex">the zero-based index starting the search</param>
        /// <param name="count">the number of elements to search</param>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public int FindIndex(int startIndex, int count, Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindIndex(startIndex, count, match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // FindIndex

        #endregion

        #region FindLast

        /// <summary>
        /// Searches for the last element in the collection that matches the criteria
        /// </summary>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public T FindLast(Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindLast(match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // FindLast

        #endregion

        #region FindLastIndex

        /// <summary>
        /// Returns the last index of the element which matches the criteria
        /// </summary>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public int FindLastIndex(Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindLastIndex(match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the last index of the element which matches the criteria
        /// </summary>
        /// <param name="startIndex">the zero-based index starting the search</param>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public int FindLastIndex(int startIndex, Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindLastIndex(startIndex, match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the last index of the element which matches the criteria
        /// </summary>
        /// <param name="startIndex">the zero-based index starting the search</param>
        /// <param name="count">the number of elements to search</param>
        /// <param name="match">delegate that defines the conditions to search for</param>
        public int FindLastIndex(int startIndex, int count, Predicate<T> match)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.FindLastIndex(startIndex, count, match);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        // FindLastIndex

        #endregion

        #region ForEach

        /// <summary>
        /// Peforms the action on each element of the list
        /// </summary>
        /// <param name="action">the action to perfom</param>
        public void ForEach(Action<T> action)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.ForEach(action);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        // ForEach

        #endregion

        /// <summary>
        /// Creates a shallow copy of the range of elements in the source
        /// </summary>
        /// <param name="index">index to start from</param>
        /// <param name="count">number of elements to return</param>
        /// <returns></returns>
        public List<T> GetRange(int index, int count)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.GetRange(index, count);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Searches the list and returns the index of the item found in the list
        /// </summary>
        /// <param name="item">the item to find</param>
        public int IndexOf(T item)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.IndexOf(item);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Searches the list and returns the index of the item found in the list
        /// </summary>
        /// <param name="item">the item to find</param>
        /// <param name="index">the zero-based index to begin searching from</param>
        public int IndexOf(T item, int index)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.IndexOf(item, index);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Searches the list and returns the index of the item found in the list
        /// </summary>
        /// <param name="item">the item to find</param>
        /// <param name="index">the zero-based index to begin searching from</param>
        /// <param name="count">the number of elements to search</param>
        public int IndexOf(T item, int index, int count)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.IndexOf(item, index, count);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Inserts the item into the list
        /// </summary>
        /// <param name="index">the index at which to insert the item</param>
        /// <param name="item">the item to insert</param>
        public void Insert(int index, T item)
        {
            _lockList.ExitWriteLock();
            try
            {
                _mTList.Insert(index, item);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Insert a range of objects into the list
        /// </summary>
        /// <param name="index">index to insert at</param>
        /// <param name="range">range of values to insert</param>
        public void InsertRange(int index, IEnumerable<T> range)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.InsertRange(index, range);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Returns the last index of the item in the list
        /// </summary>
        /// <param name="item">the item to find</param>
        public int LastIndexOf(T item)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.LastIndexOf(item);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the last index of the item in the list
        /// </summary>
        /// <param name="item">the item to find</param>
        /// <param name="index">the index at which to start searching</param>
        public int LastIndexOf(T item, int index)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.LastIndexOf(item, index);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Returns the last index of the item in the list
        /// </summary>
        /// <param name="item">the item to find</param>
        /// <param name="index">the index at which to start searching</param>
        /// <param name="count">number of elements to search</param>
        public int LastIndexOf(T item, int index, int count)
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.LastIndexOf(item, index, count);
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Removes this item from the list
        /// </summary>
        /// <param name="item">the item to remove</param>
        public bool Remove(T item)
        {
            _lockList.EnterWriteLock();
            try
            {
                return _mTList.Remove(item);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Removes all the matching items from the list
        /// </summary>
        /// <param name="match">the pattern to search on</param>
        public int RemoveAll(Predicate<T> match)
        {
            _lockList.EnterWriteLock();
            try
            {
                return _mTList.RemoveAll(match);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Removes the item at the specified index
        /// </summary>
        /// <param name="index">the index of the item to remove</param>
        public void RemoveAt(int index)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.RemoveAt(index);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Removes the items from the list
        /// </summary>
        /// <param name="index">the index of the item to begin removing</param>
        /// <param name="count">the number of items to remove</param>
        public void RemoveRange(int index, int count)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.RemoveRange(index, count);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Reverses the order of elements in the list
        /// </summary>
        public void Reverse()
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Reverse();
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Reverses the order of elements in the list
        /// </summary>
        /// <param name="index">the index to begin reversing at</param>
        /// <param name="count">the number of elements to reverse</param>
        public void Reverse(int index, int count)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Reverse(index, count);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Sorts the items in the list
        /// </summary>
        public void Sort()
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Sort();
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Sorts the items in the list
        /// </summary>
        /// <param name="comparison">the comparison to use when comparing elements</param>
        public void Sort(Comparison<T> comparison)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Sort(comparison);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Sorts the items in the list
        /// </summary>
        /// <param name="comparer">the comparer to use when comparing elements</param>
        public void Sort(IComparer<T> comparer)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Sort(comparer);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Sorts the items in the list
        /// </summary>
        /// <param name="index">the index to begin sorting at</param>
        /// <param name="count">the number of elements to sort</param>
        /// <param name="comparer">the comparer to use when sorting</param>
        public void Sort(int index, int count, IComparer<T> comparer)
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.Sort(index, count, comparer);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Copies the elements of the list to an array
        /// </summary>
        public T[] ToArray()
        {
            _lockList.EnterReadLock();
            try
            {
                return _mTList.ToArray();
            }
            finally
            {
                _lockList.ExitReadLock();
            }
        }

        /// <summary>
        /// Sets the capacity to the actual number of elements in the list, if that 
        /// number is less than the threshold
        /// </summary>
        public void TrimExcess()
        {
            _lockList.EnterWriteLock();
            try
            {
                _mTList.TrimExcess();
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }

        /// <summary>
        /// Determines whether all members of the list matches the conditions in the predicate
        /// </summary>
        /// <param name="match">the delegate which defines the conditions for the search</param>
        public bool TrueForAll(Predicate<T> match)
        {
            _lockList.EnterWriteLock();
            try
            {
                return _mTList.TrueForAll(match);
            }
            finally
            {
                _lockList.ExitWriteLock();
            }
        }
    }
}