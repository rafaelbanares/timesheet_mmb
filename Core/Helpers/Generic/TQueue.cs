﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Core
{
    /// <summary>
    /// Represents a First-In, First-Out thread-safe collection of objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TQueue<T> : ICollection
    {
        #region Variables

        /// <summary>
        /// The private q which holds the actual frameworkDatabase
        /// </summary>
        private readonly Queue<T> _mQueue;

        /// <summary>
        /// Lock for the Q
        /// </summary>
        private readonly ReaderWriterLockSlim _lockQ = new ReaderWriterLockSlim();

        /// <summary>
        /// Used only for the SyncRoot properties
        /// </summary>
        private readonly object _objSyncRoot = new object();

        // Variables

        #endregion

        #region Init

        /// <summary>
        /// Initializes the Queue
        /// </summary>
        public TQueue()
        {
            _mQueue = new Queue<T>();
        }

        /// <summary>
        /// Initializes the Queue
        /// </summary>
        /// <param name="capacity">the initial number of elements the queue can contain</param>
        public TQueue(int capacity)
        {
            _mQueue = new Queue<T>(capacity);
        }

        /// <summary>
        /// Initializes the Queue
        /// </summary>
        /// <param name="collection">the collection whose members are copied to the Queue</param>
        public TQueue(IEnumerable<T> collection)
        {
            _mQueue = new Queue<T>(collection);
        }

        // Init

        #endregion

        #region IEnumerable<T> Members

        /// <summary>
        /// Returns an enumerator that enumerates through the collection
        /// </summary>
        public IEnumerator<T> GetEnumerator()
        {
            Queue<T> localQ;

            // init enumerator
            _lockQ.EnterReadLock();
            try
            {
                // create a copy of m_TList
                localQ = new Queue<T>(_mQueue);
            }
            finally
            {
                _lockQ.ExitReadLock();
            }

            // get the enumerator
            return ((IEnumerable<T>) localQ).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that enumerates through the collection
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            Queue<T> localQ;

            // init enumerator
            _lockQ.EnterReadLock();
            try
            {
                // create a copy of m_TList
                localQ = new Queue<T>(_mQueue);
            }
            finally
            {
                _lockQ.ExitReadLock();
            }

            // get the enumerator
            return localQ.GetEnumerator();
        }

        #endregion

        #region ICollection Members

        #region CopyTo

        /// <summary>
        /// Copies the Queue's elements to an existing array
        /// </summary>
        /// <param name="array">the one-dimensional array to copy into</param>
        /// <param name="index">the zero-based index at which copying begins</param>
        public void CopyTo(Array array, int index)
        {
            _lockQ.EnterReadLock();
            try
            {
                // copy
                _mQueue.ToArray().CopyTo(array, index);
            }

            finally
            {
                _lockQ.ExitReadLock();
            }
        }

        /// <summary>
        /// Copies the Queue's elements to an existing array
        /// </summary>
        /// <param name="array">the one-dimensional array to copy into</param>
        /// <param name="index">the zero-based index at which copying begins</param>
        public void CopyTo(T[] array, int index)
        {
            _lockQ.EnterReadLock();
            try
            {
                // copy
                _mQueue.CopyTo(array, index);
            }

            finally
            {
                _lockQ.ExitReadLock();
            }
        }

        // CopyTo

        #endregion

        #region Count

        /// <summary>
        /// Returns the number of items in the Queue
        /// </summary>
        public int Count
        {
            get
            {
                _lockQ.EnterReadLock();
                try
                {
                    return _mQueue.Count;
                }

                finally
                {
                    _lockQ.ExitReadLock();
                }
            }
        }

        // Count

        #endregion

        #region IsSynchronized

        public bool IsSynchronized
        {
            get { return true; }
        }

        // IsSynchronized

        #endregion

        #region SyncRoot

        public object SyncRoot
        {
            get { return _objSyncRoot; }
        }

        // SyncRoot

        #endregion

        #endregion

        #region Enqueue

        /// <summary>
        /// Adds an item to the queue
        /// </summary>
        /// <param name="item">the item to add to the queue</param>
        public void Enqueue(T item)
        {
            _lockQ.EnterWriteLock();
            try
            {
                _mQueue.Enqueue(item);
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        // Enqueue

        #endregion

        #region Dequeue

        /// <summary>
        /// Removes and returns the item in the beginning of the queue
        /// </summary>
        public T Dequeue()
        {
            _lockQ.EnterWriteLock();
            try
            {
                return _mQueue.Dequeue();
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        // Dequeue

        #endregion

        #region EnqueueAll

        /// <summary>
        /// Enqueues the list of items
        /// </summary>
        /// <param name="itemsToQueue">list of items to enqueue</param>
        public void EnqueueAll(IEnumerable<T> itemsToQueue)
        {
            _lockQ.EnterWriteLock();
            try
            {
                // loop through and add each item
                foreach (var item in itemsToQueue)
                    _mQueue.Enqueue(item);
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        /// <summary>
        /// Enqueues the list of items
        /// </summary>
        /// <param name="itemsToQueue">list of items to enqueue</param>
        public void EnqueueAll(TList<T> itemsToQueue)
        {
            _lockQ.EnterWriteLock();
            try
            {
                // loop through and add each item
                foreach (T item in itemsToQueue)
                    _mQueue.Enqueue(item);
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        // EnqueueAll

        #endregion

        #region DequeueAll

        /// <summary>
        /// Dequeues all the items and returns them as a thread safe list
        /// </summary>
        public TList<T> DequeueAll()
        {
            _lockQ.EnterWriteLock();
            try
            {
                // create return object
                var returnList = new TList<T>();

                // dequeue until everything is out
                while (_mQueue.Count > 0)
                    returnList.Add(_mQueue.Dequeue());

                // return the list
                return returnList;
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        // DequeueAll

        #endregion

        #region Reload

        /// <summary>
        /// Removes all items from the queue
        /// </summary>
        public void Clear()
        {
            _lockQ.EnterWriteLock();
            try
            {
                _mQueue.Clear();
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        // Reload

        #endregion

        #region Contains

        /// <summary>
        /// Determines whether the item exists in the Queue
        /// </summary>
        /// <param name="item">the item to find in the queue</param>
        public bool Contains(T item)
        {
            _lockQ.EnterReadLock();
            try
            {
                return _mQueue.Contains(item);
            }

            finally
            {
                _lockQ.ExitReadLock();
            }
        }

        // Contains

        #endregion

        #region Peek

        /// <summary>
        /// Returns the item at the start of the Queue without removing it
        /// </summary>
        public T Peek()
        {
            _lockQ.EnterReadLock();
            try
            {
                return _mQueue.Peek();
            }

            finally
            {
                _lockQ.ExitReadLock();
            }
        }

        // Peek

        #endregion

        #region ToArray

        /// <summary>
        /// Copies the Queue to a new array
        /// </summary>
        public T[] ToArray()
        {
            _lockQ.EnterReadLock();
            try
            {
                return _mQueue.ToArray();
            }

            finally
            {
                _lockQ.ExitReadLock();
            }
        }

        // ToArray

        #endregion

        #region TrimExcess

        /// <summary>
        /// Sets the capacity of the Queue to the current number of items, if that number
        /// is less than 90 percent of the current capacity
        /// </summary>
        public void TrimExcess()
        {
            _lockQ.EnterWriteLock();
            try
            {
                _mQueue.TrimExcess();
            }

            finally
            {
                _lockQ.ExitWriteLock();
            }
        }

        // TrimExcess

        #endregion
    }
}