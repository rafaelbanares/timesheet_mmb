﻿namespace Core
{
    /// <summary>
    /// Represents an hexadecimal converter.
    /// </summary>
    public static class HexadecimalConverter
    {
        private static readonly string[] FLookupTable;

        static HexadecimalConverter()
        {
            FLookupTable = new string[256];
            for (var i = 0; i < FLookupTable.Length; i++)
                FLookupTable[i] = i.ToString("X2");
        }

        /// <summary>
        /// Converts the <paramref name="bytes"/> to an hexadecimal string.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>A <see cref="System.String"/> that represents the <paramref name="bytes"/>.</returns>
        public static string ToHexString(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return "";

            var lChars = new char[bytes.Length*2];
            for (int i = 0, j = -1; i < bytes.Length; i++)
            {
                var lHex = FLookupTable[bytes[i]];
                lChars[++j] = lHex[0];
                lChars[++j] = lHex[1];
            }

            return new string(lChars);
        }
    }
}