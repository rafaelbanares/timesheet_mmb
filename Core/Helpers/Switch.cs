﻿using System;
namespace Core
{
    public class Switch
    {
        public object Object { get; private set; }

        public Switch(object o)
        {
            Object = o;
        }

        public Switch Case(object o, Action<object> a)
        {
            return Case(o, a, false);
        }

        public Switch Case(object o, Action<object> a, bool fallThrough)
        {
            return Case(x => Equals(x, o), a, fallThrough);
        }

        public Switch Case(Func<object, bool> c, Action<object> a)
        {
            return Case(c, a, false);
        }

        public Switch Case(Func<object, bool> c, Action<object> a, bool fallThrough)
        {
            if (this == null)
            {
                return null;
            }
            if (c(this.Object))
            {
                a(this.Object);
                return fallThrough ? this : null;
            }

            return this;
        }

        public Switch Case<T>(Action<T> a) where T : class
        {
            return Case(o => true, a, false);
        }

        public Switch Case<T>(Action<T> a, bool fallThrough) where T : class
        {
            return Case(o => true, a, fallThrough);
        }

        public Switch Case<T>(Func<T, bool> c, Action<T> a) where T : class
        {
            return Case(c, a, false);
        }

        public Switch Case<T>(Func<T, bool> c, Action<T> a, bool fallThrough) where T : class
        {
            if (this == null)
            {
                return null;
            }
            var t = this.Object as T;
            if (t != null)
            {
                if (c(t))
                {
                    a(t);
                    return fallThrough ? this : null;
                }
            }

            return this;
        }

        public void Default(Action<object> a)
        {
            if (this != null)
            {
                a(this.Object);
            }
        }
    }

    public class Switch<T>
    {
        public Switch(T o)
        {
            Object = o;
        }

        public T Object { get; private set; }

        public void Default(Action<T> a)
        {
            if (this != null)
            {
                a(this.Object);
            }
        }
    }


    public class Switch<T, R>
    {
        public Switch(T o)
        {
            Object = o;
        }

        public T Object { get; private set; }
        public bool HasValue { get; private set; }
        public R Value { get; private set; }

        public void Set(R value)
        {
            Value = value;
            HasValue = true;
        }

        public Switch<T, R> Case(T t, Func<T, R> f)
        {
            return Case(x => Equals(x, t), f);
        }

        public Switch<T, R> Case(Func<T, bool> c, Func<T, R> f)
        {
            if (!this.HasValue && c(this.Object))
            {
                this.Set(f(this.Object));
            }

            return this;
        }

        public R Default(Func<T, R> f)
        {
            if (!this.HasValue)
            {
                this.Set(f(this.Object));
            }

            return this.Value;
        }
    }
}