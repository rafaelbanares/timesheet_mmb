﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amib.Threading;

namespace Core.Helpers.Threading
{
    public class Pool : Singleton<Pool>
    {
        SmartThreadPool _smartPool;

        public Pool()
        {
            _smartPool = new SmartThreadPool(
                new STPStartInfo
                {
                    MinWorkerThreads = 1,
                    MaxWorkerThreads = 10,
                    UseCallerHttpContext = true,
                    UseCallerCallContext = true
                });
        }

        public static SmartThreadPool Manager
        {
            get { return Pool.Instance._smartPool; }
        }

        protected override void Cleanup()
        {
            base.Cleanup();
            if (_smartPool != null) _smartPool.Dispose();
        }
    }
}
