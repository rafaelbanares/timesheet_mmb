﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Security.Principal;
using System.ServiceProcess;
using System.Diagnostics;
//using Core.Helpers.Healthcheck;

namespace Core
{
    [Serializable]
    public class ConnectionInfos 
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string ToConnectionString()
        {
            return ToConnectionString(600);
        }

        public string ToConnectionString(int? timeout)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = this.ServerName;
            if (this.DatabaseName != null) builder.InitialCatalog = this.DatabaseName;
            if (!string.IsNullOrEmpty( this.UserName))
            {
                builder.UserID = this.UserName;
                builder.Password = this.Password;
            }
            else builder.IntegratedSecurity = true;

            builder.ConnectTimeout = timeout.HasValue ? timeout.Value : 600;
            builder.MinPoolSize = 10;
            return builder.ConnectionString;
        }

        public override string ToString()
        {
            return ToConnectionString();
        }

        public bool Test()
        {
            System.Diagnostics.Trace.WriteLine("Testing connection : " + this.ToConnectionString());

            bool isConnectionValid = false;
            
            SqlConnection connection = new SqlConnection(this.ToConnectionString(10));
            try
            {
                //connection.ConnectionTimeout = 10;
                connection.Open();
                isConnectionValid = (connection.State == System.Data.ConnectionState.Open);
            }
            catch (SqlException x)
            {
                // Unable to connect
                x.LogToTrace();
            }
            finally
            {
                connection.Close();
            }

            //Debug.WriteLine("Testing connection result : " + isConnectionValid.ToString());

            return isConnectionValid;
        }

        public int ExecuteScript(string script)
        {
            if (string.IsNullOrEmpty(script))
                return 0;

            var scripts = SplitSqlScript(script);

            if (scripts == null || scripts.Count() == 0)
                return 0;

            var affected = 0;

            using (SqlConnection conn = new SqlConnection(this.ToConnectionString()))
            {
                conn.Open();


                scripts.ForEach(x =>
                {
                    using (SqlCommand cmd = new SqlCommand(x, conn))
                    {
                        affected += cmd.ExecuteNonQuery();
                    }
                });

                conn.Close();
            }

            return affected;
        }

        public object ExecuteScalar(string commandText)
        {
            object result = null;
            using (SqlConnection conn = new SqlConnection(this.ToConnectionString()))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(commandText, conn))
                {
                    result = cmd.ExecuteScalar();
                }

                conn.Close();
            }
            return result;
        }

        public DataSet ExecuteDataSet(string script)
        {
            if (string.IsNullOrEmpty(script))
                return null;

            var scripts = SplitSqlScript(script);

            if (scripts == null || scripts.Count() == 0)
                return null;

            DataSet result = null;
            int i = 0;
            using (SqlConnection conn = new SqlConnection(this.ToConnectionString()))
            {
                conn.Open();
                scripts.ForEach(x =>
                {
                    if (result.IsNull()) result = new DataSet();
                    var dt = new DataTable(i.ToString());
                    using (SqlDataAdapter adp = new SqlDataAdapter(x, conn))
                    {
                        adp.Fill(dt);
                    }
                    result.Tables.Add(dt);
                    i++;
                });

                conn.Close();
            }

            return result;
        }

        public bool TestServer()
        {
            return GetMaster().Test();
        }

        public void SetupDatabase()
        {
            bool canConnectToServer = false;
            bool creationProcessWentWell = false;

            //Debug.WriteLine("Trying to create database " + DatabaseName);

            /*
             If the database is not existing and we have to perform setup
             then there is a few chance that SQL user was setup
                First priorirty: We need to connect to server !
                   let's remove sql auth (if needed) and work in Integrated mode 
                   then let's impersonate the guy behind
             */

            var connInIntegrated = this.SwitchToIntegrated();
           // Debug.Indent();
            // The guy connected at this stage is supposedly administrator of the database server
            // code execution is impersonated to have it working under his privilege

            //WindowsIdentity winId = (WindowsIdentity)ContextHelper.GetUserIdentity();
            //WindowsImpersonationContext ctx = null;
            try
            {
                // Start impersonating
                //ctx = winId.Impersonate();
                // Now impersonating
                // Access resources using the identity of the authenticated user

                // test server access
                if (connInIntegrated.TestServer())
                {
                    //Debug.WriteLine("Can connect to server" );
                    canConnectToServer = true;

                    // test database exists
                    if (!connInIntegrated.Test())
                    {
                        //Debug.WriteLine("Database not existing, trying to create");
                        // database does not exists
                        // must create
                        // Create database
                        connInIntegrated.GetMaster().ExecuteScript(String.Format("Create Database [{0}]", this.DatabaseName));
                        while (!connInIntegrated.Test()) System.Threading.Thread.Sleep(500);
                        //Debug.WriteLine("Database created");
                    }

                    // now we have ensured that database is created with integrated auth
                    // let's test actual connection string 

                    //Debug.WriteLine("Testing access again");
                    if (!Test())
                    {
                        // if still not working let's switch back to Integrated to setup the user
                        if (UserName.IsFilled())
                        {
                            //Debug.WriteLine("Login setup");
                            connInIntegrated.SetupLogin(UserName, Password);
                        }
                    }


                    SqlConnection.ClearAllPools();

                    creationProcessWentWell = Test();
                }
            }
            // Prevent exceptions from propagating
            catch
            {
            }
            finally
            {
                // Revert impersonation
                //if (ctx != null) ctx.Undo();
            }
           // Debug.Unindent();

            if (!canConnectToServer) throw new ApplicationException(String.Format("You are not allowed to connect to {0} or server does not exists.", ServerName));
            if (!creationProcessWentWell) throw new ApplicationException(String.Format("You have no sufficient privileges in {0} to perform such operation.", ServerName));
        }

        public void SetupLogin(string username, string password)
        {
            // test database allows SQL Auth
            if (!AllowsUserNameAuthentication())
            {
                //Debug.WriteLine("Switching server to Mixed authentication");
                // switch DB server to SQL authentication
                SetUserNameAuthentication();

                //Debug.WriteLine("Restarting SQL Server");

                var service = GetSqlServerService();

                service.Stop();
                //Debug.WriteLine("Stopping....");
                while (service.Status != ServiceControllerStatus.Stopped)
                {
                    System.Threading.Thread.Sleep(500);
                    service.Refresh();
                }
                service.Start();
                //Debug.WriteLine("Starting....");
                while (service.Status != ServiceControllerStatus.Running)
                {
                    System.Threading.Thread.Sleep(500);
                    service.Refresh();
                }
            }

            SqlConnection.ClearAllPools();

            //Debug.WriteLine("Create login");
            CreateLogin(username, password);
            AddLoginToAdmin(username);
        }

        public static ConnectionInfos Get(string connectionString)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            ConnectionInfos infos = new ConnectionInfos();
            infos.ServerName = builder.DataSource;
            infos.DatabaseName = builder.InitialCatalog;
            if (!builder.IntegratedSecurity)
            {
                infos.UserName = builder.UserID;
                infos.Password = builder.Password;
            }
            return infos;
        }

        public static ConnectionInfos GetFromConfigFile(string connectionStringName)
        {
            return Get(ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString);
        }

        public static ConnectionInfos Get(string ServerName, string DatabaseName)
        {
            return Get(ServerName, DatabaseName, null, null);
        }

        public static ConnectionInfos Get(string ServerName, string DatabaseName, string UserName, string Password)
        {
            ConnectionInfos infos = new ConnectionInfos();
            infos.ServerName = ServerName;
            infos.DatabaseName = DatabaseName;
            infos.UserName = UserName;
            infos.Password = Password;
            return infos;
        }

        static IEnumerable<string> SplitSqlScript(string script)
        {
            Regex splitter = new Regex(@"\n\s*(GO|go|Go|gO)\s*\n?");
            string[] scripts = splitter.Split(script);

            if (scripts == null || scripts.Length == 0 || (scripts.Length == 1 && !scripts[0].IsFilled()))
                return null;

            var res = scripts.Where(s => s.IsFilled() && s.Trim().ToLowerInvariant() != "go");
            //if (res.Any(x => x.ToLowerInvariant().Contains("go")))
            //{
            //    var toto = res.First(x => x.ToLowerInvariant().Contains("go"));
            //    System.Diagnostics.Debugger.Break();
            //}
            return res;
        }

        bool AllowsUserNameAuthentication()
        {
            var result = false;
            var script = @"EXEC master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'LoginMode', @AuthenticationMode OUTPUT";

            using (SqlConnection conn = new SqlConnection(this.ToConnectionString()))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(script, conn))
                {
                    var parm = new SqlParameter("AuthenticationMode", SqlDbType.Int);
                    parm.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(parm);
                    cmd.ExecuteNonQuery();
                    switch ((int)parm.Value)
                    {
                        case 1: result = false; break;
                        case 2: result = true; break;
                    }
                }

                conn.Close();
            }

            return result;
        }

        void SetUserNameAuthentication()
        {
            //var sqlServices = GetSqlServerServices(DBServer.Text);
            var script = @"exec master..xp_instance_regwrite N'HKEY_LOCAL_MACHINE',N'SOFTWARE\Microsoft\MSSQLServer\MSSQLServer','LoginMode', N'REG_DWORD', 2";
            ExecuteScript(script);
        }

        void CreateLogin(string userName, string password)
        {
            var script = String.Format("IF NOT EXISTS(SELECT name FROM master.dbo.syslogins WHERE name = '{0}') CREATE LOGIN {0} WITH PASSWORD = '{1}';", userName, password);
            ExecuteScript(script);
        }

        void AddLoginToAdmin(string userName)
        {
            var script = String.Format("EXEC master..sp_addsrvrolemember @loginame = N'{0}', @rolename = N'sysadmin';", userName);
            ExecuteScript(script);
        }

        ConnectionInfos GetMaster()
        {
            var copy = (ConnectionInfos)this.MemberwiseClone();
            copy.DatabaseName = "master";
            return copy;
        }

        ConnectionInfos ChangeDatabase(string DatabaseName)
        {
            var copy = (ConnectionInfos)this.MemberwiseClone();
            copy.DatabaseName = DatabaseName;
            return copy;
        }

        ConnectionInfos SwitchToIntegrated()
        {
            var copy = (ConnectionInfos)this.MemberwiseClone();
            copy.UserName = null;
            copy.Password = null;
            return copy;
        }

        ServiceController GetSqlServerService()
        {
            bool isNamedInstance = (InstanceName != MachineName);
            var services = System.ServiceProcess.ServiceController.GetServices(MachineName);
            var serviceName = (InstanceName != MachineName) ? "MSSQL$" + InstanceName.ToUpper() : "MSSQLSERVER";
            return services.Where(x => x.ServiceName == serviceName).FirstOrDefault();
        }

        string MachineName
        {
            get
            {
                var splitted = ServerName.Split('\\');
                if (splitted.Length > 1)
                {
                    return splitted[0];
                }
                return ServerName;
            }
        }

        string InstanceName
        {
            get
            {
                var splitted = ServerName.Split('\\');
                if (splitted.Length > 1)
                {
                    return splitted[1];
                }
                return ServerName;
            }
        }
    }
}
