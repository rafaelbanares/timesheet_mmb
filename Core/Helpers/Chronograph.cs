using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Helpers;
using System.Diagnostics;

namespace Core
{
    /// <summary>
    /// Defines the types of output for the <see cref="Chronograph"/> class.
    /// </summary>
    public enum ChronographOutput
    {
        /// <summary>
        /// Standard output
        /// </summary>
        Standard = 0,
        /// <summary>
        /// HTML output
        /// </summary>
        Html,
        /// <summary>
        /// Hidden HTML output
        /// </summary>
        HtmlHidden
    }

    /// <summary>
    /// Represents a useful timer class typically to measure the application performance.
    /// Multiple timers can be managed.
    /// This class is a singleton. Use the <c>Instance</c> property to get the concreteFeatureType.
    /// </summary>
    public sealed class Chronograph
    {
        private static Chronograph _instance;
        private ChronoTimer _timer;

        /// <summary>
        /// Gets the concreteFeatureType.
        /// </summary>
        /// <value>The concreteFeatureType.</value>
        public static Chronograph Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof (Chronograph))
                    {
                        if (_instance == null) _instance = new Chronograph();
                    }
                }
                return _instance;
            }
        }

        private static void KillInstance()
        {
            _instance = null;
        }

        private Chronograph()
        {
            _timer = new ChronoTimer("Overall");
        }

        ~Chronograph()
        {
            _timer = null;
        }

        /// <summary>
        /// Starts the specified chrono.
        /// </summary>
        /// <param name="chronoName">Name of the chrono.</param>
        public void Start(string chronoName)
        {
            _timer.StartChild(chronoName);
        }

        /// <summary>
        /// Stops the specified chrono.
        /// </summary>
        /// <param name="chronoName">Name of the chrono.</param>
        public void Stop(string chronoName)
        {
            _timer.Stop(chronoName);
        }

        /// <summary>
        /// Stops all.
        /// </summary>
        public void Stop()
        {
            _timer.Stop("Overall");
        }

        /// <summary>
        /// Gets the output.
        /// </summary>
        /// <returns>A <see cref="System.String"/> object representing the output.</returns>
        public string GetOutput()
        {
            return GetOutput(ChronographOutput.Standard);
        }

        /// <summary>
        /// Gets the output.
        /// </summary>
        /// <param name="output">The output type.</param>
        /// <returns>A <see cref="System.String"/> object representing the output.</returns>
        public string GetOutput(ChronographOutput output)
        {
            var beginLine = "";
            var endLine = "";

            switch (output)
            {
                case ChronographOutput.Standard:
                    endLine = "\n";
                    break;
                case ChronographOutput.Html:
                    endLine = "<br>";
                    break;
                case ChronographOutput.HtmlHidden:
                    beginLine = "<!-- ";
                    endLine = " -->\n";
                    break;
            }

            var result = "";
            var al = GetResult();

            foreach (string s in al) result += beginLine + s + endLine;

            return result;
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <returns></returns>
        public ArrayList GetResult()
        {
            Stop();
            KillInstance();
            return _timer.GetResult(0, _timer.Elapsed);
        }

        /// <summary>
        /// Represents a chrono.
        /// </summary>
        private class ChronoTimer
        {
            /// <summary>
            /// Is running.
            /// </summary>
            private bool _running;

            /// <summary>
            /// The name of the chrono
            /// </summary>
            private readonly string _name;

            /// <summary>
            /// Start time
            /// </summary>
            private Stopwatch _startTime;

            /// <summary>
            /// The list of timers.
            /// </summary>
            private readonly List<ChronoTimer> _children;

            /// <summary>
            /// Elapsed
            /// </summary>
            public TimeSpan Elapsed = new TimeSpan(0);


            /// <summary>
            /// Initializes a new concreteFeatureType of the <see cref="ChronoTimer"/> class.
            /// </summary>
            /// <param name="name">The name of the chrono.</param>
            public ChronoTimer(string name)
            {
                _children = new List<ChronoTimer>();
                _name = name;
                _startTime = Stopwatch.StartNew();
                _running = true;
            }

            /// <summary>
            /// Starts the chrono.
            /// </summary>
            /// <param name="chronoName">Name of the chrono.</param>
            public void StartChild(string chronoName)
            {
                var found = false;
                foreach (var timer in _children)
                {
                    if (timer._name == chronoName)
                    {
                        timer._startTime = Stopwatch.StartNew();
                        timer._running = true;
                        found = true;
                        break;
                    }
                    if (timer._running)
                    {
                        timer.StartChild(chronoName);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    var timer = new ChronoTimer(chronoName);
                    _children.Add(timer);
                }
            }

            /// <summary>
            /// Stops the specified chrono.
            /// </summary>
            /// <param name="chronoName">Name of the chrono.</param>
            public void Stop(string chronoName)
            {
                if (_name == chronoName)
                {
                    foreach (var child in _children.Where(child => child._running))
                    {
                        child.Stop(child._name);
                    }
                    _startTime.Stop();
                    Elapsed += _startTime.Elapsed;
                    _running = false;
                }
                else
                {
                    foreach (var child in _children.Where(child => child._running))
                    {
                        child.Stop(chronoName);
                    }
                }
            }

            /// <summary>
            /// Gets the result.
            /// </summary>
            /// <param name="decalage">The decalage.</param>
            /// <param name="overall">The overall.</param>
            /// <returns></returns>
            public ArrayList GetResult(int decalage, TimeSpan overall)
            {
                var result = new ArrayList();

                var prepend = "";
                for (var i = 0; i < decalage; i++) prepend += "     ";

                result.Add(prepend + _name);
                if (_children.Count > 0)
                {
                    result.Add(prepend + "{");
                    foreach (string s in from child in _children
                                         select child.GetResult(decalage + 1, overall)
                                         into childResult from string s in childResult select s)
                    {
                        result.Add(s);
                    }
                    result.Add(prepend + "}");
                }

                var timeSpent = ": " + Elapsed.TotalMilliseconds.ToString("0.00ms") + " (" +
                                (Elapsed.TotalMilliseconds / overall.TotalMilliseconds).ToString("0.00%") + ")";
                result[result.Count - 1] += timeSpent;

                return result;
            }
        }
    }
}