﻿using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace Core
{
    public static class RegistryHelper
    {
        private static bool FolderExists(string Folder)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\" + Folder);
            return key.IsNotNull();
        }


        private static bool CreateFolder(string Folder)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software", true);
            var folder = key.CreateSubKey(Folder);
            return folder.IsNotNull();
        }

        private static void EnsureFolder(string Folder)
        {
            if (!FolderExists(Folder)) CreateFolder(Folder);
        }

        public static T Get<T>(string Folder, string KeyName) 
        {
            RegistryKey sk1 = Registry.LocalMachine.OpenSubKey("Software\\" + Folder);

            if (sk1 == null)
            {
                return default(T);
            }
            else
            {
                try
                {
                    return sk1.GetValue(KeyName.ToUpper()).ToString().FromJSON<T>();
                }
                catch (Exception)
                {
                    return default(T);
                }
            }
        }


        public static T Get<T>(string Folder, string KeyName, T defaultValue)
        {
            var val = Get<T>(Folder, KeyName);
            if (val.IsNull() || Comparer<T>.Default.Compare(val, default(T)) == 0)
            {
                Set(Folder, KeyName, defaultValue);
            }
            return Get<T>(Folder, KeyName);
        }

        public static bool Set(string Folder, string KeyName, object Value)
        {
            try
            {
                EnsureFolder(Folder);
                RegistryKey sk1 = Registry.LocalMachine.OpenSubKey("Software\\" + Folder, true);
                sk1.SetValue(KeyName.ToUpper(), Value.ToJSON());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Drop(string Folder, string KeyName)
        {
            try
            {
                RegistryKey sk1 = Registry.LocalMachine.CreateSubKey("Software\\" + Folder);

                if (sk1 == null) return true;
                else sk1.DeleteValue(KeyName);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DropAll(string Folder)
        {
            try
            {
                RegistryKey sk1 = Registry.LocalMachine.OpenSubKey("Software\\" + Folder);

                if (sk1 != null)
                    Registry.LocalMachine.DeleteSubKeyTree("Software\\" + Folder);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

}
