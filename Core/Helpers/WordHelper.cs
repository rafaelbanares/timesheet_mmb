﻿using System;
using System.Collections.Generic;
using System.IO;
using Core.Documents;
using Core.Documents.Word;
using DocumentFormat.OpenXml.Packaging;

namespace Core
{
    /// <summary>
    /// Docx Builder From Template
    /// </summary>
    public static class WordHelper
    {
        /// <summary>
        /// Process the Docx template in Memory and save the result to the target file 
        /// </summary>
        /// <param name="targetFile">The target file.</param>
        public static void Transform(string sourceFile, string targetFile, Dictionary<string, object> data)
        {
            Transform(sourceFile, targetFile, data, null);
        }

        public static void Transform(string sourceFile, string targetFile, Dictionary<string, object> data, ITranslationProvider translationProvider)
        {
            sourceFile.CanNotBeEmpty();
            targetFile.CanNotBeEmpty();
            data.CanNotBeNull();

            if (File.Exists(sourceFile))
            {
                MemoryStream _template;
                using (var templateStream = FileHelper.GetFileStream(sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    _template = templateStream.LoadIntoMemoryStream();
                }

                DataCollectionProcessor _processor = new DataCollectionProcessor(data);
                if (translationProvider.IsNotNull()) _processor.TranslationProvider = translationProvider;
                var outputStream = GenerateOutputStream(targetFile);

                try
                {
                    using (var source = _template.LoadIntoMemoryStream())
                    {
                        var docX = WordprocessingDocument.Open(source, true);
                        _processor.Process(docX);
                        docX.Close();

                        source.Seek(0, SeekOrigin.Begin);
                        source.CopyStream(outputStream);
                    }

                    outputStream.Close();
                    outputStream.Dispose();
                }
                catch (Exception)
                {
                    outputStream.Close();
                    outputStream.Dispose();

                    if (File.Exists(targetFile))
                    {
                        try
                        {
                            File.Delete(targetFile);
                        }
                        catch
                        {
                        }
                    }

                    throw;
                }
            }
        }


        private static Stream GenerateOutputStream(string outputFilePath)
        {
            try
            {
                Stream outputStream = File.Open(outputFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                return outputStream;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("output file cannot be created (details: " + ex.Message + ")", "outputFilePath", ex);
            }
        }
    }
}