﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace Core
{
    public class PerfCounterResult
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float Value { get; set; }
    }

    public class PerfCounter
    {
        public string Category { get; set; }
        public string Name { get; set; }
        public string InstanceName { get; set; }
        public string Description { get; set; }

        public static IEnumerable<PerfCounterResult> Read(PerfCounter[] items)
        {
            Dictionary<PerfCounter, float> result = new Dictionary<PerfCounter, float>();
            List<PerformanceCounter> lst = new List<PerformanceCounter>();

            items.ForEach(x =>
            {
                var cnt = new PerformanceCounter(x.Category, x.Name, x.InstanceName);
                lst.Add(cnt);
                cnt.NextValue();
                result.Add(x, 0);
            });

            Thread.Sleep(1000);

            items.ForEach(x =>
                 result[x] = lst.First(c => c.CategoryName == x.Category && c.CounterName == x.Name && c.InstanceName == x.InstanceName).NextValue()
                );

            return result.Select(x=>new PerfCounterResult { Name = x.Key.Name, Description = x.Key.Description, Value=x.Value });
        }
    }
}
