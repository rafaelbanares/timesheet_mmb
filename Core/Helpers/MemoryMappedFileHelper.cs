﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.IO;
//using System.IO.MemoryMappedFiles;
//using System.Runtime.Serialization.Formatters.Binary;

//namespace Core.Helpers
//{
//    public static class MemoryMappedFileHelper
//    {
//        static Mutex oMutex = new Mutex(false, "MemoryMappedFileHelper");

//        static MemoryMappedFileHelper()
//        {
//        }


//        public static object ReadObject(string path, string objectName)
//        {
//            object result = null;
//            try
//            {
//                oMutex.WaitOne();
//                MemoryStream ms;
//                using (var map = MemoryMappedFile.CreateFromFile(path, FileMode.Open, objectName))
//                using (var accessor = map.CreateViewStream())
//                {
//                    ms = accessor.LoadIntoMemoryStream();
//                }

//                if (ms != null)
//                {
//                    // deserialize
//                    BinaryFormatter bf = new BinaryFormatter();
//                    result = bf.Deserialize(ms);
//                    ms.Close();
//                }
//            }
//            catch { return null; }
//            finally
//            {
//                oMutex.ReleaseMutex();
//            }
//            return result;
//        }

//        public static void WriteObject(string path, string objectName, object InObject)
//        {
//            try
//            {
//                // serialize
//                MemoryStream ms = new MemoryStream();
//                BinaryFormatter bf = new BinaryFormatter();
//                bf.Serialize(ms, InObject);
//                var buffer = ms.GetBuffer();
//                ms.Close();

//                oMutex.WaitOne();
//                using (var map = MemoryMappedFile.CreateFromFile(path, FileMode.Create, objectName, buffer.LongLength, MemoryMappedFileAccess.ReadWrite))
//                using (var accessor = map.CreateViewStream(0, buffer.LongLength, MemoryMappedFileAccess.ReadWrite))
//                {
//                    accessor.Write(buffer, 0, buffer.Length);
//                }
//            }
//            finally
//            {
//                oMutex.ReleaseMutex();
//            }
//        }

//        public static void DeleteObject(string path)
//        {
//            if (File.Exists(path)) File.Delete(path);
//        }
//    }
//}
