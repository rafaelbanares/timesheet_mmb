﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Core
{
    /// <summary>
    /// Exposes helper methods for Entity Framework
    /// </summary>
    public static class EntityFrameworkHelper
    {
        /// <summary>
        /// Builds the or expression. Allows to evaluate the "contains" or "in" test operation.
        /// </summary>
        /// <typeparam name="TElement">The type of the element.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="valueSelector">The value selector.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static Expression<Func<TElement, bool>> BuildOrExpression<TElement, TValue>(
            Expression<Func<TElement, TValue>> valueSelector, IEnumerable<TValue> values)
        {
            valueSelector.CanNotBeNull();
            values.CanNotBeNull();

            var p = valueSelector.Parameters.Single();

            if (!values.Any())
                return e => false;

            var equals = values.Select(value =>
                                       (Expression) Expression.Equal(
                                           valueSelector.Body,
                                           Expression.Constant(
                                               value,
                                               typeof (TValue)
                                               )
                                                        )
                );

            var body = equals.Aggregate(Expression.Or);

            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }
    }
}