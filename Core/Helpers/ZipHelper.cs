﻿using System;
using System.Collections;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.GZip;

namespace Core
{
    /// <summary>
    /// Helper class to manipulate ZIP files
    /// </summary>
    public static class ZipHelper
    {
        /// <summary>
        /// Zips the files.
        /// </summary>
        /// <param name="inputFolderPath">The input folder path.</param>
        /// <param name="outputPathAndFile">The output path and file.</param>
        /// <param name="password">The password.</param>
        public static void ZipFiles(string inputFolderPath, string outputPathAndFile)
        {
            ZipFiles(inputFolderPath, outputPathAndFile, null, true);
        }        
        
        /// <summary>
        /// Zips the files.
        /// </summary>
        /// <param name="inputFolderPath">The input folder path.</param>
        /// <param name="outputPathAndFile">The output path and file.</param>
        /// <param name="password">The password.</param>
        public static void ZipFiles(string inputFolderPath, string outputPathAndFile, string password)
        {
            ZipFiles(inputFolderPath, outputPathAndFile, password, true);
        }

        /// <summary>
        /// Zips the files.
        /// </summary>
        /// <param name="inputFolderPath">The input folder path.</param>
        /// <param name="outputPathAndFile">The output path and file.</param>
        /// <param name="password">The password.</param>
        /// <param name="includeBaseFolder">if updateSet to <c>true</c>, includes the base folder.</param>
        public static void ZipFiles(string inputFolderPath, string outputPathAndFile, string password,
                                    bool includeBaseFolder)
        {
            var ar = GenerateFileList(inputFolderPath); // generate file list
            if (inputFolderPath != null)
            {
                var trimLength = includeBaseFolder
                                     ? (Directory.GetParent(inputFolderPath)).ToString().Length
                                     : new DirectoryInfo(inputFolderPath).ToString().Length;
                // find number of chars to remove     // from orginal file path
                trimLength += 1; //remove '\'
                FileStream ostream;
                byte[] obuffer;
                using (var oZipStream = new ZipOutputStream(File.Create(outputPathAndFile)))
                {
                    // create zip stream
                    if (!string.IsNullOrEmpty(password))
                        oZipStream.Password = password;
                    oZipStream.SetLevel(9); // maximum compression
                    oZipStream.UseZip64 = UseZip64.Off;
                    // If yes: file cannot be opened with Windows "Compressed Folders"
                    foreach (string fil in ar) // for each file, generate a zipentry
                    {
                        var oZipEntry = new ZipEntry(fil.Remove(0, trimLength));
                        oZipStream.PutNextEntry(oZipEntry);

                        if (!fil.EndsWith(@"/"))
                        {
                            using (ostream = File.OpenRead(fil))
                            {
                                obuffer = new byte[ostream.Length];
                                ostream.Read(obuffer, 0, obuffer.Length);
                                oZipStream.Write(obuffer, 0, obuffer.Length);
                            }
                        }
                    }
                    oZipStream.Finish();
                }
            }
        }


        private static ArrayList GenerateFileList(string dir)
        {
            var fils = new ArrayList();
            var empty = true;
            foreach (var file in Directory.GetFiles(dir)) // add each file in directory
            {
                fils.Add(file);
                empty = false;
            }

            if (empty)
            {
                if (Directory.GetDirectories(dir).Length == 0)
                    // if directory is completely empty, add it
                {
                    fils.Add(dir + @"/");
                }
            }

            foreach (var dirs in Directory.GetDirectories(dir)) // recursive
            {
                foreach (var obj in GenerateFileList(dirs))
                {
                    fils.Add(obj);
                }
            }
            return fils; // return file list
        }


        /// <summary>
        /// Unzips the file.
        /// </summary>
        /// <param name="zipPathAndFile">The zip file.</param>
        /// <param name="outputFolder">The output folder.</param>
        /// <param name="password">The password.</param>
        /// <param name="deleteZipFile">if updateSet to <c>true</c>, deletes the ZIP file</param>
        public static void UnZipFiles(string zipPathAndFile, string outputFolder, string password, bool deleteZipFile)
        {
            UnZipFiles(File.OpenRead(zipPathAndFile), outputFolder, password);
            if (deleteZipFile) File.Delete(zipPathAndFile);
        }

        public static void UnZipFiles(Stream zipStream, string outputFolder, string password)
        {
            var s = new ZipInputStream(zipStream);
            if (!string.IsNullOrEmpty(password))
                s.Password = password;
            ZipEntry theEntry;

            while ((theEntry = s.GetNextEntry()) != null)
            {
                var directoryName = outputFolder;
                var fileName = Path.GetFileName(theEntry.Name);
                // create directory 
                if (directoryName != "")
                {
                    Directory.CreateDirectory(directoryName);
                }
                if (fileName != String.Empty)
                {
                    if (theEntry.Name.IndexOf(".ini") < 0)
                    {
                        var fullPath = directoryName + "\\" + theEntry.Name;
                        fullPath = fullPath.Replace("\\ ", "\\");
                        var fullDirPath = Path.GetDirectoryName(fullPath);
                        if (!Directory.Exists(fullDirPath)) Directory.CreateDirectory(fullDirPath);
                        var streamWriter = File.Create(fullPath);
                        var data = new byte[2048];
                        while (true)
                        {
                            var size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                        streamWriter.Close();
                    }
                }
            }
            s.Close();
           
        }

        /// <summary>
        /// Unzips the files.
        /// </summary>
        /// <param name="zipPathAndFile">The zip file.</param>
        /// <param name="outputFolder">The output folder.</param>
        public static void UnZipFiles(string zipPathAndFile, string outputFolder)
        {
            UnZipFiles(zipPathAndFile, outputFolder, null, false);
        }

        public static byte[] Zip(byte[] uncompressedData)
        {
            using (var outputMs = new MemoryStream())
            {
                using (var outputZip = new GZipOutputStream(outputMs))
                { 
                    var entry = new ZipEntry("frameworkDatabase");

                    outputZip.Write(uncompressedData, 0, uncompressedData.Length);
                    outputZip.Finish();

                    return outputMs.ToByteArray();
                }
            }
        }

        public static byte[] UnZip(byte[] compressedData)
        {
            byte[] buffer = new byte[4096];

            using(var inputMs = new MemoryStream(compressedData))
            {
                using (var inputZip = new GZipInputStream(inputMs))
                {
                    using (var outputMs = new MemoryStream())
                    {
                        StreamUtils.Copy(inputZip, outputMs, buffer);

                        return outputMs.ToByteArray();
                    }
                }
            }
        }
    }
}