using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Win32.Security;
using System.DirectoryServices.AccountManagement;

namespace Core
{
    /// <summary>
    /// Set of helper methods to deal with file sytem directories
    /// </summary>
    public static class DirectoryHelper
    {
        /// <summary>
        /// Create a shared folder on local machine
        /// </summary>
        /// <param name="path">Path to the folder (folder is created if not already existing)</param>
        /// <param name="shareName">Name of the share (visible to other machines)</param>
        /// <param name="overwrite">if the Share folder is already existing it will be recreated (this allows to use same share name but change the path)</param>
        public static void CreateSharedFolder(string path, string shareName, bool overwrite)
        {
            path.CanNotBeEmpty();
            path = path.TrimEnd("\\".ToCharArray());
            shareName.CanNotBeEmpty();

            if (overwrite) RemoveSharedFolder(shareName, true);
            CreateDirectory(path);

            string EveryoneName = null;
            var EveryOneSid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
            using (PrincipalContext context = new PrincipalContext(ContextType.Machine))
            {
                using (GroupPrincipal everyone = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, EveryOneSid.Value))
                {
                    EveryoneName = everyone.SamAccountName;
                }
            }
            SetFullPermissions(EveryoneName, path);

            var result = ShareHelper.Create(path, shareName, Win32Share.ShareType.DiskDrive, 5, "", null);
            if(result == null) throw new ApplicationException("Unable to create shared folder");
        }
        /// <summary>
        /// Create a shared folder on local machine
        /// </summary>
        /// <param name="path">Path to the folder (folder is created if not already existing)</param>
        /// <param name="shareName">Name of the share (visible to other machines)</param>
        public static void CreateSharedFolder(string path, string shareName)
        {
            CreateSharedFolder(path, shareName, false);
        }

        /// <summary>
        /// Removes a shared folder from the local machine
        /// </summary>
        /// <param name="shareName">Name of the share</param>
        public static void RemoveSharedFolder(string shareName)
        {
            RemoveSharedFolder(shareName, false);
        }

        /// <summary>
        /// Removes a shared folder from the local machine
        /// </summary>
        /// <param name="shareName">Name of the share</param>
        /// <param name="withPhysicalFolderDelete">Forces deletion of the folder once the share is removed</param>
        public static void RemoveSharedFolder(string shareName, bool withPhysicalFolderDelete)
        {
            var share = ShareHelper.GetNamedShare(shareName);
            if (share != null)
            {
                if (withPhysicalFolderDelete)
                {
                    var path = share.Path;
                    Delete(path);
                }
                share.Delete();
            }
        }

        private static void SetFullPermissions(string user, string folderpath)
        {
            var secDesc = SecurityDescriptor.GetFileSecurity(folderpath, SECURITY_INFORMATION.DACL_SECURITY_INFORMATION);
            var dacl = secDesc.Dacl;
            var sidUser = new Sid(user);
            dacl.AddAce(new AceAccessAllowed(sidUser, AccessType.GENERIC_ALL | AccessType.STANDARD_RIGHTS_ALL,
                                             AceFlags.OBJECT_INHERIT_ACE | AceFlags.CONTAINER_INHERIT_ACE));
            const DirectoryAccessType daType =
                DirectoryAccessType.FILE_WRITE_ATTRIBUTES | DirectoryAccessType.FILE_WRITE_EA |
                DirectoryAccessType.DELETE | DirectoryAccessType.WRITE_OWNER | DirectoryAccessType.WRITE_DAC;
            const AccessType aType = (AccessType) daType;
            dacl.AddAce(new AceAccessDenied(sidUser, aType));
            secDesc.SetDacl(dacl);
            secDesc.SetFileSecurity(folderpath, SECURITY_INFORMATION.DACL_SECURITY_INFORMATION);
        }

        /// <summary>
        /// Delete a directory
        /// </summary>
        /// <param name="directory">Path to the directory</param>
        public static void Delete(string directory)
        {
            try
            {
                Directory.Delete(directory, true);
            }
            catch (DirectoryNotFoundException)
            {
                // Nothing to do cause we are trying to delete an unexisting directory
            }
        }

        /// <summary>
        /// Deep copy from one folder to another
        /// </summary>
        /// <param name="sourceDirectory">Source directory path</param>
        /// <param name="destinationDirectory">Destination directory path</param>
        public static void CopyTo(string sourceDirectory, string destinationDirectory)
        {
            if (!Directory.Exists(destinationDirectory)) Directory.CreateDirectory(destinationDirectory);

            var source = new DirectoryInfo(sourceDirectory);
            var files = source.GetFiles();
            foreach (var fi in files) fi.CopyTo(destinationDirectory.AppendInCase("\\") + fi.Name, true);
            var directories = source.GetDirectories();
            foreach (var di in directories)
                CopyTo(sourceDirectory.AppendInCase("\\") + di.Name, destinationDirectory.AppendInCase("\\") + di.Name);
        }

        /// <summary>
        /// Deep copy from one folder to another excluding some specified extensions
        /// </summary>
        /// <param name="sourceDirectory">Source directory path</param>
        /// <param name="destinationDirectory">Destination directory path</param>
        /// <param name="forbiddenExtensions">List of forbidden extensions</param>
        public static void CopyTo(string sourceDirectory, string destinationDirectory, List<string> forbiddenExtensions)
        {
            if (!Directory.Exists(destinationDirectory)) Directory.CreateDirectory(destinationDirectory);

            var source = new DirectoryInfo(sourceDirectory);
            var files = source.GetFiles();

            foreach (var fi in from fi in files
                               let fi1 = fi
                               where forbiddenExtensions == null || !forbiddenExtensions.Exists(e => e.ToLowerInvariant() == fi1.Extension.ToLowerInvariant())
                               select fi)
            {
                fi.CopyTo(destinationDirectory.AppendInCase("\\") + fi.Name, true);
            }

            var directories = source.GetDirectories();
            foreach (var di in directories)
                CopyTo(sourceDirectory.AppendInCase("\\") + di.Name, destinationDirectory.AppendInCase("\\") + di.Name);
        }

        /// <summary>
        /// Create a directory structure
        /// </summary>
        /// <param name="path">e.g.: "C:\Temp\Dir1\Dir2"</param>
        public static void CreateDirectory(string path)
        {
            path.CanNotBeEmpty();

            var fi = new FileInfo(path);
            if (fi.Extension.IsFilled())
            {
                path = fi.FullName.TrimEnd(fi.Name);
            }
            var di = new DirectoryInfo(path);
            if (!di.Exists)
            {
                var splitted = path.Split("\\".ToCharArray());
                var current = "";
                foreach (var split in splitted)
                {
                    if (!string.IsNullOrEmpty(split))
                    {
                        current += split.AppendInCase("\\");
                        if (!Directory.Exists(current)) Directory.CreateDirectory(current);
                    }
                }
            }
        }
    }
}