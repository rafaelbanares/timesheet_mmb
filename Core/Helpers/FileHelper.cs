using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Linq;
using Microsoft.Win32;

namespace Core
{
    /// <summary>
    /// Static FileHelper class.
    /// </summary>
    public static class FileHelper
    {
        public static MemoryStream GetFileStream(string path, FileMode mode, FileAccess access, FileShare share)
        {
            MemoryStream memStream;
            using (Stream fileStream = File.Open(path, mode, access, share))
            {
                memStream = fileStream.LoadIntoMemoryStream();
            }
            return memStream;
        }

        /// <summary>
        /// Determines whether the specified file exists.
        /// </summary>
        /// <param name="filePath">The file to check.</param>
        /// <returns>
        /// <c>true</c> if the caller has the required permissions and path contains the name
        /// of an existing file; otherwise, <c>false</c>. This method also returns <c>false</c> if
        /// path is null, an invalid path, or a zero-length string. If the caller does
        /// not have sufficient permissions to read the specified file, no exception
        /// is thrown and the method returns false regardless of the existence of path.
        /// </returns>
        public static bool Exists(string filePath)
        {
            return File.Exists(filePath);
        }

        /// <summary>
        /// Creates a new file, writes the specified byte array to the file, and then
        /// closes the file. If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to write to.</param>
        /// <param name="frameworkDatabase">The bytes to write to the file.</param>
        public static void WriteBinary(string path, byte[] data)
        {
            File.WriteAllBytes(path, data);
        }

        /// <summary>
        /// Opens a binary file, reads the contents of the file into a byte array, and
        /// then closes the file.
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <returns>A byte array containing the contents of the file.</returns>
        public static byte[] ReadBinary(string path)
        {
            return File.ReadAllBytes(path);
        }

        /// <summary>
        /// Creates a new file, writes the specified string to the file, and then
        /// closes the file. If the target file already exists, the <paramref name="str"/>
        /// string is appened to the file.
        /// </summary>
        /// <param name="path">The file to write to.</param>
        /// <param name="str">The string to write to the file.</param>
        public static void Write(string path, string str)
        {
            Write(path, str, false);
        }

        /// <summary>
        /// Creates a new file, writes the specified string to the file, and then
        /// closes the file.
        /// If the <paramref name="overWrite"/> parameter is <c>true</c>, the target
        /// file is overwritten; otherwise, the <paramref name="str"/> string is
        /// appened to the file.
        /// </summary>
        /// <param name="path">The file to write to.</param>
        /// <param name="str">The string to write to the file.</param>
        /// <param name="overWrite">
        /// If updateSet to <c>true</c>, the target file is overwritten; otherwise, the string
        /// is appened.
        /// </param>
        public static void Write(string path, string str, bool overWrite)
        {
            //if (overWrite) File.WriteAllText(path, str);
            //else File.AppendAllText(path, str);
            using (StreamWriter writer = new StreamWriter(path, !overWrite))
            {
                writer.Write(str);
            }
        }

        /// <summary>
        /// Creates a new file, writes the specified string to the file, and then
        /// closes the file.
        /// If the <paramref name="overWrite"/> parameter is <c>true</c>, the target
        /// file is overwritten; otherwise, the <paramref name="str"/> string is
        /// appened to the file.
        /// Before to write the text to the file, the variables defined by the
        /// <paramref name="variables"/> parameter are replaced in the <paramref name="str"/> 
        /// string.
        /// </summary>
        /// <param name="path">The file to write to.</param>
        /// <param name="str">The string to write to the file.</param>
        /// <param name="overWrite">
        /// If updateSet to <c>true</c>, the target file is overwritten; otherwise, the string
        /// is appened.
        /// </param>
        /// <param name="variables">The variables.</param>
        public static void Write(string path, string str, bool overWrite, NameValueCollection variables)
        {
            Write(path, ExpandVariables(str, variables), overWrite);
        }

        /// <summary>
        /// Creates a new file, writes the specified string to the file, and then
        /// closes the file.
        /// If the <paramref name="overWrite"/> parameter is <c>true</c>, the target
        /// file is overwritten; otherwise, the <paramref name="str"/> string is
        /// appened to the file.
        /// Before to write the text to the file, the <paramref name="variableName"/> is 
        /// replaced in the <paramref name="str"/> by its <paramref name="value"/>.
        /// </summary>
        /// <param name="path">The file to write to.</param>
        /// <param name="str">The string to write to the file.</param>
        /// <param name="overWrite">
        /// If updateSet to <c>true</c>, the target file is overwritten; otherwise, the string
        /// is appened.
        /// </param>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="value">The value of the variable.</param>
        public static void Write(string path, string str, bool overWrite, string variableName, string value)
        {
            var variables = new NameValueCollection {{variableName, value}};
            Write(path, str, overWrite, variables);
        }

        /// <summary>
        /// Reads the file from the beginning to the end.
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <returns>
        /// The file as a string, from the beginning to the end.
        /// </returns>
        public static string Read(string path)
        {
            var readString = string.Empty;
            if (!Exists(path))
                return readString;

            using (var fi = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var sr = new StreamReader(fi, Encoding.Default))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Creates an empty file.
        /// If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to write to.</param>
        public static void Empty(string path)
        {
            Write(path, String.Empty, true);
        }

        /// <summary>
        /// Reads the file from the beginning to the end and replaces
        /// the <paramref name="variables"/> by their value.
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <param name="variables">The variables.</param>
        /// <returns>The file as a string, from the beginning to the end.</returns>
        public static string Read(string path, NameValueCollection variables)
        {
            var original = Read(path);
            var result = ExpandVariables(original, variables);
            return result;
        }

        /// <summary>
        /// Reads the file from the beginning to the end and replaces
        /// the <paramref name="variables"/> by their value.
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <param name="variables">The variables.</param>
        /// <param name="openingTag">The opening tag for variables.</param>
        /// <param name="closingTag">The closing tag for variables.</param>
        /// <returns>The file as a string, from the beginning to the end.</returns>
        public static string Read(string path, NameValueCollection variables, string openingTag, string closingTag)
        {
            var original = Read(path);
            var result = ExpandVariables(original, variables, openingTag, closingTag);
            return result;
        }

        /// <summary>
        /// Reads the file from the beginning to the end and replaces
        /// the <paramref name="variableName"/> by its <paramref name="value"/>
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="value">The value of the variable.</param>
        /// <returns>The file as a string, from the beginning to the end.</returns>
        public static string Read(string path, string variableName, string value)
        {
            var variables = new NameValueCollection {{variableName, value}};
            return Read(path, variables);
        }

        /// <summary>
        /// Replaces the variables in the specified string by their value.
        /// </summary>
        /// <param name="original">A <c>System.String</c> object.</param>
        /// <param name="variables">The variables.</param>
        /// <param name="openingTag">The opening tag.</param>
        /// <param name="closingTag">The closing tag.</param>
        /// <returns>The specified string after replacing the variables.</returns>
        public static string ExpandVariables(string original, NameValueCollection variables, string openingTag,
                                             string closingTag)
        {
            if ((variables != null) && (original != null))
            {
                for (var i = 0; i < variables.Count; i++)
                {
                    original = original.Replace(openingTag + variables.Keys[i] + closingTag, variables[i]);
                }
            }
            return original;
        }

        /// <summary>
        /// Replaces the variables in the specified string by their value.
        /// </summary>
        /// <param name="original">A <c>System.String</c> object.</param>
        /// <param name="variables">The variables.</param>
        /// <returns>The specified string after replacing the variables.</returns>
        public static string ExpandVariables(string original, NameValueCollection variables)
        {
            return ExpandVariables(original, variables, "%", "%");
        }

        /// <summary>
        /// Permanently deletes a file.
        /// </summary>
        /// <param name="file">The file to delete.</param>
        public static void Delete(string file)
        {
            if (File.Exists(file))
            {
                var fi = new FileInfo(file);
                fi.Delete();
            }
        }

        /// <summary>
        /// Displays the size of the file.
        /// </summary>
        /// <param name="fileSize">Size of the file.</param>
        /// <returns></returns>
        public static string DisplayFileSize(long fileSize)
        {
            var result = FormatSizeWithUnit(fileSize.ToString(), "B");

            if (fileSize >= 1024 && fileSize < 1048576) // Kilo bytes
            {
                var sizeKb = fileSize/1024M;
                result = FormatSizeWithUnit(sizeKb.ToString("F1"), "KB");
            }
            else if (fileSize >= 1048576 && fileSize < 1073741824) // Mega bytes
            {
                var sizeMb = fileSize/1048576M;
                result = FormatSizeWithUnit(sizeMb.ToString("F2"), "MB");
            }
            else if (fileSize >= 1073741824) // Giga bytes
            {
                var sizeGb = fileSize/1073741824M;
                result = FormatSizeWithUnit(sizeGb.ToString("F3"), "GB");
            }

            return result;
        }

        /// <summary>
        /// Returns <c>true</c> whether the <paramref name="contentType"/> corresponds
        /// to the content type of an image.
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static bool IsImageFile(string contentType)
        {
            if (!string.IsNullOrEmpty(contentType))
            {
                contentType = contentType.ToLowerInvariant();
                return contentType.StartsWith("image");
            }

            return false;
        }

        private static string FormatSizeWithUnit(string size, string unit)
        {
            return string.Format("{0} {1}", size, unit);
        }


        //public static string GetMimeType(string filename)
        //{
        //    string mime = "application/octetstream";
        //    string ext = System.IO.Path.GetExtension(filename).ToLower();
        //    Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
        //    if (rk != null && rk.GetValue("Content Type") != null)
        //        mime = rk.GetValue("Content Type").ToString();
        //    return mime;
        //}

        public static string GetMimeType(string ExtensionOrFileName)
        {
            ExtensionOrFileName.CanNotBeNull();
            var Extension = ExtensionOrFileName.Split('.').Any() ? ExtensionOrFileName.Split('.').Last() : ExtensionOrFileName;
            Extension = Extension.PrependInCase(".").ToLower();

            switch(Extension)
            {
                case ".": return "application/octet-stream";
                case ".323": return "text/h323";
                case ".acx": return "application/internet-property-stream";
                case ".ai": return "application/postscript";
                case ".aif": return "audio/x-aiff";
                case ".aifc": return "audio/x-aiff";
                case ".aiff": return "audio/x-aiff";
                case ".asf": return "video/x-ms-asf";
                case ".asr": return "video/x-ms-asf";
                case ".asx": return "video/x-ms-asf";
                case ".au": return "audio/basic";
                case ".avi": return "video/x-msvideo";
                case ".axs": return "application/olescript";
                case ".bas": return "text/plain";
                case ".bcpio": return "application/x-bcpio";
                case ".bin": return "application/octet-stream";
                case ".bmp": return "image/bmp";
                case ".c": return "text/plain";
                case ".cat": return "application/vnd.ms-pkiseccat";
                case ".cdf": return "application/x-cdf";
                case ".cer": return "application/x-x509-ca-cert";
                case ".class": return "application/octet-stream";
                case ".clp": return "application/x-msclip";
                case ".cmx": return "image/x-cmx";
                case ".cod": return "image/cis-cod";
                case ".cpio": return "application/x-cpio";
                case ".crd": return "application/x-mscardfile";
                case ".crl": return "application/pkix-crl";
                case ".crt": return "application/x-x509-ca-cert";
                case ".csh": return "application/x-csh";
                case ".css": return "text/css";
                case ".dcr": return "application/x-director";
                case ".der": return "application/x-x509-ca-cert";
                case ".dir": return "application/x-director";
                case ".dll": return "application/x-msdownload";
                case ".dms": return "application/octet-stream";
                case ".doc": return "application/msword";
                case ".dot": return "application/msword";
                case ".dvi": return "application/x-dvi";
                case ".dxr": return "application/x-director";
                case ".eps": return "application/postscript";
                case ".etx": return "text/x-setext";
                case ".evy": return "application/envoy";
                case ".exe": return "application/octet-stream";
                case ".fif": return "application/fractals";
                case ".flr": return "x-world/x-vrml";
                case ".gif": return "image/gif";
                case ".gtar": return "application/x-gtar";
                case ".gz": return "application/x-gzip";
                case ".h": return "text/plain";
                case ".hdf": return "application/x-hdf";
                case ".hlp": return "application/winhlp";
                case ".hqx": return "application/mac-binhex40";
                case ".hta": return "application/hta";
                case ".htc": return "text/x-component";
                case ".htm": return "text/html";
                case ".html": return "text/html";
                case ".htt": return "text/webviewhtml";
                case ".ico": return "image/x-icon";
                case ".ief": return "image/ief";
                case ".iii": return "application/x-iphone";
                case ".ins": return "application/x-internet-signup";
                case ".isp": return "application/x-internet-signup";
                case ".jfif": return "image/pipeg";
                case ".jpe": return "image/jpeg";
                case ".jpeg": return "image/jpeg";
                case ".jpg": return "image/jpeg";
                case ".js": return "application/x-javascript";
                case ".latex": return "application/x-latex";
                case ".lha": return "application/octet-stream";
                case ".lsf": return "video/x-la-asf";
                case ".lsx": return "video/x-la-asf";
                case ".lzh": return "application/octet-stream";
                case ".m13": return "application/x-msmediaview";
                case ".m14": return "application/x-msmediaview";
                case ".m3u": return "audio/x-mpegurl";
                case ".man": return "application/x-troff-man";
                case ".mdb": return "application/x-msaccess";
                case ".me": return "application/x-troff-me";
                case ".mht": return "message/rfc822";
                case ".mhtml": return "message/rfc822";
                case ".mid": return "audio/mid";
                case ".mny": return "application/x-msmoney";
                case ".mov": return "video/quicktime";
                case ".movie": return "video/x-sgi-movie";
                case ".mp2": return "video/mpeg";
                case ".mp3": return "audio/mpeg";
                case ".mpa": return "video/mpeg";
                case ".mpe": return "video/mpeg";
                case ".mpeg": return "video/mpeg";
                case ".mpg": return "video/mpeg";
                case ".mpp": return "application/vnd.ms-project";
                case ".mpv2": return "video/mpeg";
                case ".ms": return "application/x-troff-ms";
                case ".mvb": return "application/x-msmediaview";
                case ".nws": return "message/rfc822";
                case ".oda": return "application/oda";
                case ".p10": return "application/pkcs10";
                case ".p12": return "application/x-pkcs12";
                case ".p7b": return "application/x-pkcs7-certificates";
                case ".p7c": return "application/x-pkcs7-mime";
                case ".p7m": return "application/x-pkcs7-mime";
                case ".p7r": return "application/x-pkcs7-certreqresp";
                case ".p7s": return "application/x-pkcs7-signature";
                case ".pbm": return "image/x-portable-bitmap";
                case ".pdf": return "application/pdf";
                case ".pfx": return "application/x-pkcs12";
                case ".pgm": return "image/x-portable-graymap";
                case ".pko": return "application/ynd.ms-pkipko";
                case ".pma": return "application/x-perfmon";
                case ".pmc": return "application/x-perfmon";
                case ".pml": return "application/x-perfmon";
                case ".pmr": return "application/x-perfmon";
                case ".pmw": return "application/x-perfmon";
                case ".pnm": return "image/x-portable-anymap";
                case ".pot,": return "application/vnd.ms-powerpoint";
                case ".ppm": return "image/x-portable-pixmap";
                case ".pps": return "application/vnd.ms-powerpoint";
                case ".ppt": return "application/vnd.ms-powerpoint";
                case ".prf": return "application/pics-rules";
                case ".ps": return "application/postscript";
                case ".pub": return "application/x-mspublisher";
                case ".qt": return "video/quicktime";
                case ".ra": return "audio/x-pn-realaudio";
                case ".ram": return "audio/x-pn-realaudio";
                case ".ras": return "image/x-cmu-raster";
                case ".rgb": return "image/x-rgb";
                case ".rmi": return "audio/mid";
                case ".roff": return "application/x-troff";
                case ".rtf": return "application/rtf";
                case ".rtx": return "text/richtext";
                case ".scd": return "application/x-msschedule";
                case ".sct": return "text/scriptlet";
                case ".setpay": return "application/updateSet-payment-initiation";
                case ".setreg": return "application/updateSet-registration-initiation";
                case ".sh": return "application/x-sh";
                case ".shar": return "application/x-shar";
                case ".sit": return "application/x-stuffit";
                case ".snd": return "audio/basic";
                case ".spc": return "application/x-pkcs7-certificates";
                case ".spl": return "application/futuresplash";
                case ".src": return "application/x-wais-source";
                case ".sst": return "application/vnd.ms-pkicertstore";
                case ".stl": return "application/vnd.ms-pkistl";
                case ".stm": return "text/html";
                case ".svg": return "image/svg+xml";
                case ".sv4cpio": return "application/x-sv4cpio";
                case ".sv4crc": return "application/x-sv4crc";
                case ".swf": return "application/x-shockwave-flash";
                case ".t": return "application/x-troff";
                case ".tar": return "application/x-tar";
                case ".tcl": return "application/x-tcl";
                case ".tex": return "application/x-tex";
                case ".texi": return "application/x-texinfo";
                case ".texinfo": return "application/x-texinfo";
                case ".tgz": return "application/x-compressed";
                case ".tif": return "image/tiff";
                case ".tiff": return "image/tiff";
                case ".tr": return "application/x-troff";
                case ".trm": return "application/x-msterminal";
                case ".tsv": return "text/tab-separated-values";
                case ".txt": return "text/plain";
                case ".uls": return "text/iuls";
                case ".ustar": return "application/x-ustar";
                case ".vcf": return "text/x-vcard";
                case ".vrml": return "x-world/x-vrml";
                case ".wav": return "audio/x-wav";
                case ".wcm": return "application/vnd.ms-works";
                case ".wdb": return "application/vnd.ms-works";
                case ".wks": return "application/vnd.ms-works";
                case ".wmf": return "application/x-msmetafile";
                case ".wps": return "application/vnd.ms-works";
                case ".wri": return "application/x-mswrite";
                case ".wrl": return "x-world/x-vrml";
                case ".wrz": return "x-world/x-vrml";
                case ".xaf": return "x-world/x-vrml";
                case ".xbm": return "image/x-xbitmap";
                case ".xla": return "application/vnd.ms-excel";
                case ".xlc": return "application/vnd.ms-excel";
                case ".xlm": return "application/vnd.ms-excel";
                case ".xls": return "application/vnd.ms-excel";
                case ".xlt": return "application/vnd.ms-excel";
                case ".xlw": return "application/vnd.ms-excel";
                case ".xof": return "x-world/x-vrml";
                case ".xpm": return "image/x-xpixmap";
                case ".xwd": return "image/x-xwindowdump";
                case ".z": return "application/x-compress";
                case ".zip": return "application/zip";

                case ".docx" : return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".dotx" : return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
                case ".pptx" : return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case ".ppsx" : return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
                case ".potx" : return "application/vnd.openxmlformats-officedocument.presentationml.template";
                case ".xlsx" : return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xltx" : return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";

            }
            

            string mimeType = "application/octetstream";
            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(Extension);
            if (regKey != null)
            {
                object contentType = regKey.GetValue("Content Type");
                if (contentType != null) mimeType = contentType.ToString();
            }
            return mimeType;
        }

    }
}