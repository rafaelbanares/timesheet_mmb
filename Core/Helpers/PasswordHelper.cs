﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Core
{
    /// <summary>
    /// Helper class exposing static methods to generate password.
    /// </summary>
    public static class PasswordHelper
    {
        /// <summary>
        /// Generates a list of passwords. The number of passwords is defined
        /// by the the <paramref name="passwordCount"/> value.
        /// </summary>
        /// <param name="passwordCount">The password count.</param>
        /// <param name="passwordLength">Length of the passwords.</param>
        /// <returns>A list of passwords.</returns>
        public static List<string> Generate(int passwordCount, int passwordLength)
        {
            var result = new List<string>(); 
            for (var i = 0; i < passwordCount; i++)
            {
                result.Add(Generate());
            }

            return result;
        }

        /// <summary>
        /// Generates a password.
        /// </summary>
        /// <returns>A new password.</returns>
        public static string Generate()
        {
            string[] words = {
                                 "Bern"
                                 , "Washington"
                                 , "Paris"
                                 , "London"
                                 , "Seattle"
                                 , "Tokyo"
                                 , "Brussels"
                                 , "Brasilia"
                                 , "Beijing"
                                 , "Santiago"
                                 , "Prague"
                                 , "Cairo"
                                 , "Berlin"
                                 , "Athens"
                                 , "Budapest"
                                 , "Jakarta"
                                 , "Dublin"
                                 , "Rome"
                                 , "Seoul"
                                 , "Mexico"
                                 , "Oslo"
                                 , "Panama"
                                 , "Lima"
                                 , "Manila"
                                 , "Lisbon"
                                 , "Moscow"
                                 , "Singapore"
                                 , "Madrid"
                                 , "Caracas"
                             };

            var special = "@!#*".ToCharArray();
            var numbers = "0123456789".ToCharArray();

            var password = string.Empty;

            var gen = new RNGCryptoServiceProvider();

            var randomNumber = new byte[4];
            gen.GetBytes(randomNumber);

            password += words[Convert.ToInt32(randomNumber[0])%words.Length];
            password += special[Convert.ToInt32(randomNumber[1])%special.Length];
            password += numbers[Convert.ToInt32(randomNumber[2])%numbers.Length];
            password += numbers[Convert.ToInt32(randomNumber[3])%numbers.Length];

            return password;
        }
    }
}