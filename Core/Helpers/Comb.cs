﻿using System;

namespace Core
{
    public static class Comb
    {
        /// <summary>
        /// Initializes a new concrete of the <c>System.Guid</c> class.
        /// </summary>
        /// <returns>A new <c>System.Guid</c> object.</returns>
        public static Guid NewGuid()
        {
            var now = DateTime.Now;
            var days = now.Subtract(new DateTime(1900, 1, 1)).Days;
            var milliSeconds = (int) (now.Subtract(DateTime.Today).TotalMilliseconds/(10/3));
            var buffer = Guid.NewGuid().ToByteArray();
            var daysAsBytes = BitConverter.GetBytes(days);
            for (var i = 0; i < 2; i++)
            {
                buffer[10 + i] = daysAsBytes[1 - i];
            }

            var milliSecondsAsBytes = BitConverter.GetBytes(milliSeconds);
            for (var i = 0; i < 3; i++)
            {
                buffer[12 + i] = milliSecondsAsBytes[3 - i];
            }
            return new Guid(buffer);
        }
    }
}