﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Data;
using System.IO;
using Core.Data.Concrete.SQLite;
using Core.Data.Concrete;
using System.Diagnostics;

namespace Core.Helpers.Configuration
{
    public static class ConfigurationData
    {
        static IDatabase database;
        static ITable2<ConfigurationRecord> data;
        static object locker = new object();

        static ConfigurationData()
        {
            var path = Path.Combine(Constants.Directories.DataDirectory, "Configuration.s3db");
            EnsureFile(path);
            database = new SQLiteDatabase(@"Data Source=" + path);
            EnsureTable();
            data = new DatabaseTable<ConfigurationRecord>(database, "ConfigurationRecord");
        }

        static void EnsureFile(string path)
        {
            if (!File.Exists(path))
            {
                lock (locker)
                {
                    if (!File.Exists(path)) File.Create(path).Close();
                }
            }
        }

        static void EnsureTable()
        {
            lock (locker)
            {
                var d = new DynamicQuery2(database);
                var createScript = @"create table if not exists ConfigurationRecord(Id primary key, Value Text);";
                d.NonQuery(createScript);
            }
        }

        public static void Upsert(string Key, string Value)
        {
            data.Save(new ConfigurationRecord { Id = Key, Value = Value });
        }

        public static void Delete(string Key)
        {
            data.Delete("Id = @0", new object[] { Key });
        }

        public static string Get(string Key)
        {
            var rec =  data.Query("Id = @0", new object[] { Key }).FirstOrDefault();
            return (rec != null) ? rec.Value : null;
        }

        public static IEnumerable<ConfigurationRecord> All()
        {
            return data.All();
        }
    }
}
