﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Features;
using System.Diagnostics;
using Core.Helpers;
using System.IO;
using System.Threading;
using System.Collections.Concurrent;

namespace Core.Helpers.Configuration
{
    public static class Config
    {

        static Config()
        {
            EnsureLocalCache();
        }

        static Dictionary<string, string> _localCahe;
        static object locker = new object();

        static void EnsureLocalCache()
        {
            if (_localCahe == null)
            {
                lock (locker)
                {
                    if (_localCahe == null) _localCahe = ConfigurationData.All().ToDictionary(x => x.Id, x => x.Value);
                }
            }
        }

        public static IEnumerable<KeyValuePair<string, string>> All()
        {
            return _localCahe;
        }

        public static T Get<T>(string key, T defaultValue)
        {
            var val = Get<T>(key);
            if (val.IsNull() || Comparer<T>.Default.Compare(val, default(T)) == 0)
            {
                EnsureExistence(key, defaultValue);
            }
            return Get<T>(key);
        }

        public static T Get<T>(string key)
        {
            EnsureLocalCache();
            T result = default(T);
            if (_localCahe.ContainsKey(key)) result = _localCahe[key].FromJSON<T>();
            return result;
        }

        public static string Get(string key)
        {
            return Get<string>(key);
        }

        static void EnsureExistence<T>(string key, T defaultValue)
        {
            if (!_localCahe.ContainsKey(key))
            {
                lock (locker)
                {
                    if (!_localCahe.ContainsKey(key))
                    {
                        // ensure repo doesnt have it....
                        var value = ConfigurationData.Get(key);
                        if (value == null)
                        {
                            value = defaultValue.ToJSON();
                            Core.Helpers.ThreadUtil.Queue(() => ConfigurationData.Upsert(key, value));
                            //ThreadUtil.FireAndForget(() => ConfigurationData.Upsert(key, value));
                        }
                        _localCahe.Add(key, value);
                    }
                }
            }
        }

        public static void Set(string key, object value)
        {
            var serializedValue = value.ToJSON();
            lock (locker)
            {
                if (_localCahe.ContainsKey(key)) _localCahe[key] = serializedValue;
                else _localCahe.Add(key, serializedValue);
                Core.Helpers.ThreadUtil.Queue(() => ConfigurationData.Upsert(key, serializedValue));
                //ThreadUtil.FireAndForget(() => ConfigurationData.Upsert(key, serializedValue));
            }
        }

        public static void Drop(string key)
        {
            if (_localCahe.ContainsKey(key))
            {
                lock (locker)
                {
                    if (_localCahe.ContainsKey(key))
                    {
                        _localCahe.Remove(key);

                        Core.Helpers.ThreadUtil.Queue(() => ConfigurationData.Delete(key));
                         //ThreadUtil.FireAndForget(() => ConfigurationData.Delete(key));
                    }
                }
            }
        }

    }
}
