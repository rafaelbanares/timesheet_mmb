﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Core.Helpers.Configuration
{
    [Serializable]
    public class ConfigurationRecord
    {
        [Key]
        public string Id { get; set; }
        public string Value { get; set; }
    }
}
