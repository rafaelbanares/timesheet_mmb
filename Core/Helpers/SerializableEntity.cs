﻿using System;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Core
{
    /// <summary>
    /// Summary description for SerializableEntity
    /// </summary>
    [Serializable]
    [XmlSchemaProvider("MySchema")]
    public class SerializableEntity<T> : IXmlSerializable, ISerializable where T : class, new()
    {
        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="SerializableEntity&lt;T&gt;"/> class.
        /// </summary>
        public SerializableEntity()
        {
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="SerializableEntity&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public SerializableEntity(T entity)
        {
            Entity = entity;
        }

        private T _entity;

        /// <summary>
        /// Gets or sets the entity.
        /// </summary>
        /// <value>The entity.</value>
        public T Entity
        {
            get { return _entity; }
            set { _entity = value; }
        }

        #region IXmlSerializable Members

        private const string Ns = "http://tempuri.org/";

        /// <summary>
        /// This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"/> to the class.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Xml.Schema.XmlSchema"/> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"/> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"/> method.
        /// </returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// This is the method named by the XmlSchemaProviderAttribute applied to the type.
        /// </summary>
        /// <param name="xs">The XML schema updateSet.</param>
        /// <returns></returns>
        public static XmlQualifiedName MySchema(XmlSchemaSet xs)
        {
            // This method is called by the framework to get the schema for this type.
            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            var xw = new XmlTextWriter(sw);

            // generate the schema for type T
            xw.WriteStartDocument();
            xw.WriteStartElement("schema");
            xw.WriteAttributeString("targetNamespace", "http://tempuri.org/");
            xw.WriteAttributeString("xmlns", "http://www.w3.org/2001/XMLSchema");
            xw.WriteStartElement("complexType");
            xw.WriteAttributeString("name", typeof(T).Name);
            xw.WriteStartElement("sequence");

            var infos = typeof(T).GetProperties();
            foreach (var pi in infos)
            {
                var isAssociation = pi.GetCustomAttributes(true).Any(a => a.GetType() == typeof(AssociationAttribute));
                //only use the property which is not an Association
                if (!isAssociation)
                {
                    xw.WriteStartElement("element");
                    xw.WriteAttributeString("name", pi.Name);
                    if (pi.PropertyType.IsGenericType)
                    {
                        var types = pi.PropertyType.GetGenericArguments();
                        xw.WriteAttributeString("type", "" + GetXsdType(types[0].FullName));
                    }
                    else
                    {
                        xw.WriteAttributeString("type", "" + GetXsdType(pi.PropertyType.FullName));
                    }
                    xw.WriteEndElement();
                }
            }
            xw.WriteEndElement();
            xw.WriteEndElement();
            xw.WriteEndElement();
            xw.WriteEndDocument();
            xw.Close();

            var schemaSerializer = new XmlSerializer(typeof(XmlSchema));
            var sr = new StringReader(sb.ToString());
            var s = (XmlSchema)schemaSerializer.Deserialize(sr);
            xs.XmlResolver = new XmlUrlResolver();
            xs.Add(s);

            return new XmlQualifiedName(typeof(T).Name, Ns);
        }

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">
        /// The <see cref="T:System.Xml.XmlReader"/> stream from which the object 
        /// is deserialized.
        /// </param>
        public void ReadXml(XmlReader reader)
        {
            _entity = new T();
            var pinfos = _entity.GetType().GetProperties();

            if (reader.LocalName == typeof(T).Name)
            {
                reader.MoveToContent();
                var inn = reader.ReadOuterXml();
                var sr = new StringReader(inn);
                var tr = new XmlTextReader(sr);
                tr.Read();
                while (tr.Read())
                {
                    var elementName = tr.LocalName;
                    var value = tr.ReadString();
                    //tr.Read();
                    foreach (var pi in pinfos)
                    {
                        if (pi.Name == elementName)
                        {
                            var tc = TypeDescriptor.GetConverter(pi.PropertyType);
                            pi.SetValue(_entity, tc.ConvertFromString(value), null);
                        }
                    }
                }
                //<ProductID>1</ProductID><ProductName>Chai</ProductName><SupplierID>1</SupplierID><CategoryID>1</CategoryID><QuantityPerUnit>10 boxes x 20 bags</QuantityPerUnit><UnitPrice>18.0000</UnitPrice><UnitsInStock>39</UnitsInStock><UnitsOnOrder>0</UnitsOnOrder><ReorderLevel>10</ReorderLevel><Discontinued>false</Discontinued>
            }

            //while (reader.Read())
            //{
            //    if (reader.IsStartElement())
            //    {
            //        string elementName = reader.LocalName;
            //        string value = reader.ReadString();
            //        foreach (PropertyInfo pi in pinfos)
            //        {
            //            if (pi.Name == elementName)
            //            {
            //                TypeConverter tc = TypeDescriptor.GetConverter(pi.PropertyType);
            //                pi.SetValue(_entity, tc.ConvertFromString(value), null);
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
        public void WriteXml(XmlWriter writer)
        {
            var pinfos = _entity.GetType().GetProperties();

            foreach (var pi in pinfos)
            {
                var isAssociation = pi.GetCustomAttributes(true).Any(obj => obj.GetType() == typeof(AssociationAttribute));
                if (!isAssociation)
                {
                    if (pi.GetValue(_entity, null) != null)
                    {
                        writer.WriteStartElement(pi.Name);
                        writer.WriteValue(pi.GetValue(_entity, null));
                        writer.WriteEndElement();
                    }
                }
            }
        }

        private static string GetXsdType(string nativeType)
        {
            var xsdTypes = new[]
                           {
                               "boolean",
                               "unsignedByte",
                               "dateTime",
                               "decimal",
                               "Double",
                               "short",
                               "int",
                               "long",
                               "Byte",
                               "Float",
                               "string",
                               "unsignedShort",
                               "unsignedInt",
                               "unsignedLong",
                               "anyURI"
                           };
            var nativeTypes = new[]
                              {
                                  "System.Boolean",
                                  "System.Byte",
                                  "System.DateTime",
                                  "System.Decimal",
                                  "System.Double",
                                  "System.Int16",
                                  "System.Int32",
                                  "System.Int64",
                                  "System.SByte",
                                  "System.Single",
                                  "System.String",
                                  "System.UInt16",
                                  "System.UInt32",
                                  "System.UInt64",
                                  "System.Uri"
                              };

            for (var i = 0; i < nativeTypes.Length; i++)
            {
                if (nativeType == nativeTypes[i])
                {
                    return xsdTypes[i];
                }
            }
            return "";
        }

        #endregion

        #region ISerializable Members

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="SerializableEntity&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        public SerializableEntity(SerializationInfo info, StreamingContext context)
        {
            _entity = new T();
            var properties = _entity.GetType().GetProperties();

            var enumerator = info.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var se = enumerator.Current;
                foreach (var pi in properties)
                {
                    if (pi.Name == se.Name)
                    {
                        pi.SetValue(_entity, info.GetValue(se.Name, pi.PropertyType), null);
                    }
                }
            }
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the frameworkDatabase needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with frameworkDatabase.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">
        /// The caller does not have the required permission.
        /// </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            var infos = _entity.GetType().GetProperties();

            foreach (var pi in infos)
            {
                var isAssociation = pi.GetCustomAttributes(true).Any(obj => obj.GetType() == typeof(AssociationAttribute));
                if (!isAssociation)
                {
                    if (pi.GetValue(_entity, null) != null)
                    {
                        info.AddValue(pi.Name, pi.GetValue(_entity, null));
                    }
                }
            }
        }

        #endregion
    }
}