using System;
using System.Diagnostics;
using System.IO;

namespace Core
{
    /// <summary>
    /// Exposes a updateSet of methods to execute command.
    /// </summary>
    public static class CommandHelper
    {
        /// <summary>
        /// Runs the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>A <see cref="System.Int32"/> integer that represents the error code.</returns>
        public static int Run(string command, string arguments)
        {
            return Run(command, arguments, ".");
        }

        public static int Run(string command)
        {
            return Run(command, "", ".");
        }
        /// <summary>
        /// Runs the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="directory">The directory.</param>
        /// <returns>A <see cref="System.Int32"/> integer that represents the error code.</returns>
        public static int Run(string command, string arguments, string directory)
        {
            return Run(command, arguments, directory, false);
        }

        /// <summary>
        /// Runs the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="directory">The directory.</param>
        /// <param name="waitForResult">if updateSet to <c>true</c> [wait for result].</param>
        /// <returns>A <see cref="System.Int32"/> integer that represents the error code.</returns>
        public static int Run(string command, string arguments, string directory, bool waitForResult)
        {
            return Run(command, arguments, directory, waitForResult, null);
        }

        /// <summary>
        /// Runs the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="directory">The directory.</param>
        /// <param name="waitForResult">If updateSet to <c>true</c>, the method waits for the result.</param>
        /// <param name="logFileName">Name of the log file.</param>
        /// <returns>A <see cref="System.Int32"/> integer that represents the error code.</returns>
        public static int Run(string command, string arguments, string directory, bool waitForResult, string logFileName)
        {
            var log = ((logFileName != null) && (logFileName.Trim().Length > 0));

            var logPath = "";
            var psiParams = "/Q /C cd /D \"" + directory + "\" && " + command + " " + arguments;
            if (log)
            {
                logPath = logFileName.Trim();

                var header = "*********************************************************************" +
                             Environment.NewLine;
                header += "*    Command: " + command + Environment.NewLine;
                header += "*    Args: " + arguments + Environment.NewLine;
                header += "*    Started on: " + DateTime.Now.ToShortDateString() + " " +
                          DateTime.Now.ToShortTimeString() + Environment.NewLine;
                header += "*********************************************************************" + Environment.NewLine;
                header += Environment.NewLine;

                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    writer.Write(header);
                }

               psiParams += " >> \"" + logPath + "\"";
            }

            var psi = new ProcessStartInfo("cmd.exe", psiParams) {WindowStyle = ProcessWindowStyle.Hidden};
            var p = Process.Start(psi);
            var result = 0;
            if (waitForResult)
            {
                if (p != null)
                {
                    p.WaitForExit();
                    result = p.ExitCode;
                }
            }
            if (log)
            {
                if (waitForResult)
                {

                    using (StreamWriter writer = new StreamWriter(logPath, true))
                    {
                        writer.Write("Command stopped with exit code (" + result + ") at " +
                                     DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() +
                                     Environment.NewLine);
                    }

                
                }

                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    writer.Write(Environment.NewLine);
                }
            }
            return result;
        }
    }
}