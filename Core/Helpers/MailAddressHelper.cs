﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Core
{
    /// <summary>
    /// Static MailAddressHelper class.
    /// </summary>
    public static class MailAddressHelper
    {
        /// <summary>
        /// Static GetAddresses method.
        /// </summary>
        /// <param name="addresses">String</param>
        /// <returns><see cref="MailAddressCollection"/></returns>
        public static MailAddressCollection GetAddresses(string addresses)
        {
            var result = new MailAddressCollection();
            if (!String.IsNullOrEmpty(addresses))
            {
                var splitted = addresses.Split(";".ToCharArray());
                foreach (var address in splitted)
                {
                    var trimAddress = address.Trim();
                    if (IsValidEmailAddress(trimAddress)) result.Add(new MailAddress(trimAddress));
                    else throw new ApplicationException("Invalid Email Address: " + trimAddress);
                }
            }
            return result;
        }

        /// <summary>
        /// Static IsValidEmailAddress method.
        /// </summary>
        /// <param name="emailAddress">String</param>
        /// <returns>Boolean</returns>
        public static bool IsValidEmailAddress(string emailAddress)
        {
            var expression = new Regex(Core.Constants.RegularExpressions.EmailAddress);
            return expression.IsMatch(emailAddress);
        }
    }
}