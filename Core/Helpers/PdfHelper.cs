﻿using Aspose.Cells;
using Aspose.Words;

namespace Core
{

    public static class PdfHelper
    {
        ///My Orders
        ///Below are all of your orders...
        ///Order ID	Order Date	Product	License	Qty	Status	 
        ///101124065620	11/24/2010	Aspose.Total for .NET	Developer Enterprise Subscription	1	Paid	Review

        /// 
        /// <summary>
        /// http://www.aspose.com/documentation/.net-components/aspose.words-for-.net/howto-convert-a-document-to-pdf.html
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        public static void ConvertWordToPdf(string sourcePath, string targetPath)
        {
            //Instantiate the License class
            Aspose.Words.License license = new Aspose.Words.License();
            //Pass only the name of the license file embedded in the assembly
            license.SetLicense("Aspose.Total.lic");

            Document doc = new Document(sourcePath);
            doc.Save(targetPath, Aspose.Words.SaveFormat.Pdf);
        }

        /// <summary>
        /// Convert xlsx file to pdf format with aspose component, calculating formulas
        /// http://www.aspose.com/community/files/51/.net-components/aspose.cells-for-.net/entry265178.aspx
        /// </summary>
        /// <param name="sourcePath">excel file path</param>
        /// <param name="targetPath">pdf file path</param>
        public static void ConvertExcelToPdf(string sourcePath, string targetPath)
        {
            //Instantiate the License class
            Aspose.Cells.License license = new Aspose.Cells.License();
            //Pass only the name of the license file embedded in the assembly
            license.SetLicense("Aspose.Total.lic");
            
            Workbook workbook = new Workbook(sourcePath);

            //Force cell vertical Auto fit to get all frameworkDatabase if multi lines
            foreach (Worksheet sheet in workbook.Worksheets)
                sheet.AutoFitRows();

            //Force formula calculation for computed cells formating or conditional formating
            workbook.CalculateFormula(true);

            //Export to pdf and save file to disk
            workbook.Save(targetPath, Aspose.Cells.SaveFormat.Pdf);
        }
    }
}