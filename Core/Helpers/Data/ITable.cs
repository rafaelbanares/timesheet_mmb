﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers.Data
{
    public interface ITable<T> : IView<T> where T : new()
    {
        U Save<U>(U o) where U : T, new();
        void InsertMany<U>(List<U> o) where U : T, new();
        void Delete(object o);
        int Delete(string where = "", params object[] args);
        int DeleteAll();
        int Truncate();
    }
}
