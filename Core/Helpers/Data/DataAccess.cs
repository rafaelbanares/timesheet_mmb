﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using System.Data.Common;
using System.ComponentModel.DataAnnotations;
using Core.Helpers;

namespace Core.Helpers.Data
{
    public class View<T> : DynamicQuery, IView<T> where T : new()
    {
        public string DBObjectName { get; private set; }
        public IEnumerable<string> Keys { get; private set; }

        public View(ConnectionInfos connectionInfos, string viewName)
            : base(connectionInfos)
        {
            DBObjectName = viewName.IsFilled() ? viewName : typeof(T).ClassName().PrependInCase("Fwk_").TrimEnd("Table");
            Keys = RuntimeDetectionOfKeyFields();
        }

        public View(DbProviderFactory factory, string connectionString, string viewName)
            : base(factory, connectionString)
        {
            DBObjectName = viewName.IsFilled() ? viewName : typeof(T).ClassName().PrependInCase("Fwk_").TrimEnd("Table");
            Keys = RuntimeDetectionOfKeyFields();
        }

        public View(ConnectionInfos connectionInfos, string viewName, string primaryKeyField)
            : base(connectionInfos)
        {
            DBObjectName = viewName.IsFilled() ? viewName : typeof(T).ClassName().PrependInCase("Fwk_").TrimEnd("Table");
            Keys = primaryKeyField.IsFilled() ? primaryKeyField.Split(',').Select(x => x.Trim()) : RuntimeDetectionOfKeyFields();
        }

        public View(DbProviderFactory factory, string connectionString, string viewName, string primaryKeyField)
            : base(factory, connectionString)
        {
            DBObjectName = viewName.IsFilled() ? viewName : typeof(T).ClassName().PrependInCase("Fwk_").TrimEnd("Table");
            Keys = primaryKeyField.IsFilled() ? primaryKeyField.Split(',').Select(x => x.Trim()) : RuntimeDetectionOfKeyFields();
        }

        private IEnumerable<string> RuntimeDetectionOfKeyFields()
        {
            return typeof(T)
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(KeyAttribute), true).Cast<KeyAttribute>().Select(y => y.IsNotNull()).FirstOrDefault())
                .Select(x => x.Name);
        }


        public IEnumerable<T> All()
        {
            return All<T>();
        }

        public IEnumerable<T> All(string orderBy)
        {
            return All<T>(orderBy);
        }

        public IEnumerable<T> All(int startPage, int pageSize)
        {
            return All<T>(startPage, pageSize);
        }

        public IEnumerable<T> All(string orderBy, int startPage, int pageSize)
        {
            return All<T>(orderBy, startPage, pageSize);
        }

        public IEnumerable<T> Query(string where = "", params object[] args)
        {
            return Query<T>(where, args);
        }

        public PaginatedList<T> Paged(string where = "", string orderBy = "", int pageSize = 20, int currentPage = 1, params object[] args)
        {
            return Paged<T>(where, orderBy, pageSize, currentPage, args);
        }


        public IEnumerable<U> All<U>() where U : T, new()
        {
            string sql = string.Format("select * from {0}", DBObjectName);
            return base.DynamicQ(sql, null).Select(DynamicQuery.To<U>);
        }

        public IEnumerable<U> All<U>(string orderBy) where U : T, new()
        {
            string sql = string.Format("select * from {0} order by {1}", DBObjectName, orderBy);
            return base.DynamicQ(sql, null).Select(x => (U)DynamicQuery.To<U>(x));
        }

        public IEnumerable<U> All<U>(int startPage, int pageSize) where U : T, new()
        {
            return Paged<U>(null, null, pageSize, startPage).Collection;
        }

        public IEnumerable<U> All<U>(string orderBy, int startPage, int pageSize) where U : T, new()
        {
            return Paged<U>(null, orderBy, pageSize, startPage).Collection;
        }

        public new IEnumerable<U> Query<U>(string where = "", params object[] args) where U : T, new()
        {
            string sql = "select * from {0} ";
            if (!string.IsNullOrEmpty(where))
                sql += where.Trim().StartsWith("where", StringComparison.CurrentCultureIgnoreCase) ? where : "where " + where;
            return base.DynamicQ(string.Format(sql, DBObjectName), args).Select(DynamicQuery.To<U>); ;
        }

        public PaginatedList<U> Paged<U>(string where = "", string orderBy = "", int pageSize = 20, int currentPage = 1, params object[] args) where U : T, new()
        {
            var keyList = Keys.ToString(",");
            var pageStart = (currentPage - 1) * pageSize;

            dynamic result = new ExpandoObject();
            var countSQL = string.Format("SELECT COUNT(1) FROM {0}", DBObjectName);

            if (String.IsNullOrEmpty(orderBy)) orderBy = keyList;

            if (!string.IsNullOrEmpty(where))
            {
                if (!where.Trim().StartsWith("WHERE", StringComparison.CurrentCultureIgnoreCase))
                {
                    where = "WHERE " + where;
                }
            }

            countSQL += where;
            result.TotalRecords = Scalar(countSQL, args);
            result.TotalPages = result.TotalRecords / pageSize;
            if (result.TotalRecords % pageSize > 0) result.TotalPages += 1;

            var sql = string.Format(@"WITH _paged AS(SELECT TOP ({3} * {5}) {0}, ROW_NUMBER() OVER(ORDER BY {6}) AS _RowNum FROM {1} {2} ) SELECT  {0} FROM _paged WHERE _RowNum >= ({5} - 1) * {3} + 1 ORDER BY {6};", "*", DBObjectName, where, pageSize, pageStart, currentPage, orderBy);
            result.Items = DynamicQ(sql, args).Select(DynamicQuery.To<U>);

            return new PaginatedList<U>(result.Items, pageSize, currentPage, result.TotalRecords);
        }

    }

    public class Table<T> : View<T>, ITable<T> where T : new()
    {
        public IEnumerable<string> ReadOnlyFields { get; private set; }

        public Table(ConnectionInfos connectionInfos, string tableName)
            : this(connectionInfos, tableName, null, null)
        {
        }

        public Table(DbProviderFactory factory, string connectionString, string tableName)
            : this(factory, connectionString, tableName, null, null)
        {
        }

        public Table(ConnectionInfos connectionInfos)
            : this(connectionInfos, null, null, null)
        {
        }
        public Table(DbProviderFactory factory, string connectionString)
            : this(factory, connectionString, null, null, null)
        {
        }

        public Table(ConnectionInfos connectionInfos, string tableName, string primaryKeyField)
            : this(connectionInfos, tableName, primaryKeyField, null)
        {
        }
        public Table(DbProviderFactory factory, string connectionString, string tableName, string primaryKeyField)
            : this(factory, connectionString, tableName, primaryKeyField, null)
        {
        }

        public Table(ConnectionInfos connectionInfos, string tableName, string primaryKeyField, string readOnlyFields)
            : base(connectionInfos, tableName, primaryKeyField)
        {
            ReadOnlyFields = readOnlyFields.IsFilled() ? readOnlyFields.Split(',').Select(x => x.Trim()).ToArray() : RuntimeDetectionOfReadOnlyFields();
        }
        public Table(DbProviderFactory factory, string connectionString, string tableName, string primaryKeyField, string readOnlyFields)
            : base(factory, connectionString, tableName, primaryKeyField)
        {
        }

        private IEnumerable<string> RuntimeDetectionOfReadOnlyFields()
        {
            return typeof(T)
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(ReadOnlyAttribute), true).Cast<ReadOnlyAttribute>().Select(y => y.IsReadOnly).FirstOrDefault())
                .Select(x => x.Name);
        }


        public U Save<U>(U o) where U : T, new()
        {
            return Save(o, false);
        }

        public void InsertMany<U>(List<U> o) where U : T, new()
        {
            if (o != null && o.Count > 0)
            {
                using (var conn = OpenConnection())
                {
                    var cmd = CreateInsertManyCommand(o);
                    cmd.Connection = conn;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public U Save<U>(U o, bool insertOnly) where U : T, new()
        {
            o.CallMethod("Validate");

            dynamic result = 0;
            using (var conn = OpenConnection())
            {
                var cmd = CreateSaveCommand(o, insertOnly);
                cmd.Connection = conn;
                result = cmd.ExecuteNonQuery();
            }
            return o;
        }

        public void Delete(object o)
        {
            using (var conn = OpenConnection())
            {
                var cmd = CreateDeleteCommand(o);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
            }
        }

        public int Delete(string where = "", params object[] args)
        {
            return Execute(CreateDeleteCommand(where: where, args: args));
        }

        public int DeleteAll()
        {
            return Execute(CreateDeleteCommand());
        }

        public int Truncate()
        {
            return Execute(CreateTruncateCommand());
        }

        private DbCommand CreateInsertManyCommand<U>(List<U> l) where U : T, new()
        {
            if (l != null && l.Count > 0)
            {
                var first = l.First();
                var properties = first.ToDictionary(true && !first.GetType().IsAnonymous());

                if (!(properties.Count > 0))
                    throw new InvalidOperationException("Can't parse this object to the database - there are no properties updateSet");

                if (Keys.Any(k => !properties.Keys.Contains(k)))
                    throw new InvalidOperationException("Object does not have all defined keys in dynamic table constructor.");

                var props = first.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(x => x.GetCustomAttributes(typeof(ReadOnlyAttribute), true).Cast<ReadOnlyAttribute>().Select(y => y.IsReadOnly).FirstOrDefault())
                    .Select(x => x.Name);

                ReadOnlyFields = ReadOnlyFields.Concat(props);

                List<string> toRemove = new List<string>();
                if (ReadOnlyFields.IsNotNull())
                {
                    toRemove = properties.Where(y => ReadOnlyFields.Contains(y.Key)).Select(x => x.Key).ToList();
                    toRemove.ForEach(y => properties.Remove(y));
                }

                var sbinsertKeys = new List<string>();
                var insertStub = "INSERT INTO {0} ({1}) VALUES {2}";

                DbCommand result = CreateCommand(insertStub, null);

                int counter = 0;
                foreach (var item in properties)
                {
                    sbinsertKeys.Add(string.Format("[{0}]", item.Key));
                    counter++;
                }

                var keys = string.Join(", ", sbinsertKeys.ToArray());


                int propCount = properties.Count;
                counter = 0;
                var vals = new List<String>();
                foreach (var toInsert in l)
                {
                    var sbinsertVals = new List<string>();
                    var itemproperties = toInsert.ToDictionary(true && !toInsert.GetType().IsAnonymous());

                    toRemove.ForEach(y => itemproperties.Remove(y));

                    foreach (var item in itemproperties)
                    {
                        sbinsertVals.Add(string.Format("@{0}", counter.ToString()));
                        result.AddParam(item.Value);
                        counter++;
                    }
                    vals.Add(string.Format("({0})", sbinsertVals.ToString(",")));
                }

                var insertSql = string.Format(insertStub, DBObjectName, keys, vals.ToString(","));


                result.CommandText = insertSql;

                return result;
            }
            return null;
        }


        private DbCommand CreateSaveCommand(object o, bool insertOnly)
        {
            var properties = o.ToDictionary(true && !o.GetType().IsAnonymous());

            if (!(properties.Count > 0))
                throw new InvalidOperationException("Can't parse this object to the database - there are no properties updateSet");

            if (Keys.Any(k => !properties.Keys.Contains(k)))
                throw new InvalidOperationException("Object does not have all defined keys in dynamic table constructor.");

            var props = o.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(ReadOnlyAttribute), true).Cast<ReadOnlyAttribute>().Select(y => y.IsReadOnly).FirstOrDefault())
                .Select(x => x.Name);

            ReadOnlyFields = ReadOnlyFields.Concat(props);

            if (ReadOnlyFields.IsNotNull())
            {
                var toRemove = properties.Where(y => ReadOnlyFields.Contains(y.Key)).Select(x => x.Key).ToList();
                toRemove.ForEach(y => properties.Remove(y));
            }

            var sbinsertKeys = new List<string>();
            var sbinsertVals = new List<string>();
            var sbupdateSet = new List<string>();
            var sbupdateWhere = new List<string>();

            var existsStub = "EXISTS(SELECT 1 FROM {0} WHERE {1})";
            var insertStub = "INSERT INTO {0} ({1}) VALUES ({2})";
            var updateStub = "UPDATE {0} SET {1} WHERE {2}";

            DbCommand result = CreateCommand(insertStub, null);

            int counter = 0;

            foreach (var item in properties)
            {
                sbinsertKeys.Add(string.Format("[{0}]", item.Key));
                sbinsertVals.Add(string.Format("@{0}", counter.ToString()));
                if (!Keys.Contains(item.Key))
                {
                    sbupdateSet.Add(string.Format("[{0}] = @{1}", item.Key, counter.ToString()));
                }
                else
                {
                    sbupdateWhere.Add(string.Format("[{0}] = @{1}", item.Key, counter.ToString()));
                }

                result.AddParam(item.Value);
                counter++;
            }

            var keys = string.Join(", ", sbinsertKeys.ToArray());
            var vals = string.Join(", ", sbinsertVals.ToArray());

            var updateSet = "";
            if (sbupdateSet.Any())
                updateSet = string.Join(",", sbupdateSet.ToArray());

            var updateWhere = string.Join(" AND ", sbupdateWhere.ToArray());

            var existsSql = string.Format(existsStub, DBObjectName, updateWhere);
            var insertSql = string.Format(insertStub, DBObjectName, keys, vals);
            var updateSql = string.Format(updateStub, DBObjectName, updateSet, updateWhere);

            var finalSql = insertSql;

            if (insertOnly)
            {
                finalSql = string.Format("if (NOT {0}) begin {1} end", existsSql, insertSql);
            }
            else
            {
                if (!string.IsNullOrEmpty(updateSet))
                {
                    finalSql = string.Format("if ({0}) begin {1} end else begin {2} end", existsSql, updateSql, insertSql);
                }
                else
                {
                    // case of link tables
                    if (Keys.Count() == properties.Count)
                    {
                        finalSql = string.Format("if (NOT {0}) begin {1} end", existsSql, insertSql);
                    }
                }
            }

            result.CommandText = finalSql;

            return result;
        }

        private DbCommand CreateDeleteCommand()
        {
            var sql = string.Format("DELETE FROM {0}", DBObjectName);
            return CreateCommand(sql, null);
        }

        private DbCommand CreateDeleteCommand(string where = "", params object[] args)
        {
            where.CanNotBeNullOrEmpty();

            var sql = string.Format("DELETE FROM {0} ", DBObjectName);
            sql += where.Trim().StartsWith("Where", StringComparison.CurrentCultureIgnoreCase) ? where : "WHERE " + where;
            return CreateCommand(sql, null, args);
        }

        private DbCommand CreateDeleteCommand(object o)
        {
            DbCommand result = null;
            var properties = o.ToDictionary();

            if (!(properties.Count > 0))
                throw new InvalidOperationException("Can't parse this object to the database - there are no properties updateSet");

            var stub = "DELETE FROM {0} WHERE {1}";

            var sbWhere = new List<string>();
            result = CreateCommand(stub, null);

            int counter = 0;
            foreach (var item in properties)
            {
                if (Keys.Contains(item.Key))
                {
                    sbWhere.Add(string.Format("[{0}] = @{1}", item.Key, counter.ToString()));
                    result.AddParam(item.Value);
                    counter++;
                }
            }

            var where = string.Join(" AND ", sbWhere.ToArray());

            var sql = string.Format(stub, DBObjectName, where);
            result.CommandText = sql;

            return result;
        }

        private DbCommand CreateTruncateCommand()
        {
            var sql = string.Format("TRUNCATE TABLE {0}", DBObjectName);
            return CreateCommand(sql, null);
        }
    }
}
