﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
namespace Core.Helpers.Data.Mocking
{
    public class MockTable<KeyType, T> : IView<T>, ITable<T> where T : new()
    {
        public Dictionary<KeyType, T> data = new Dictionary<KeyType, T>();
        private string path = Core.Constants.Directories.TempDirectory + "\\Mock" + typeof(T).Name + ".dat";

        public string Key { get; private set; }

        public MockTable(string primaryKeyField)
        {
            Key = primaryKeyField;
            data = Read(path);
        }

        public IEnumerable<T> All()
        {
            return All<T>();
        }

        public IEnumerable<T> All(string orderBy)
        {
            return All<T>(orderBy);
        }

        public IEnumerable<T> All(int startPage, int pageSize)
        {
            return All<T>(startPage, pageSize);
        }

        public IEnumerable<T> All(string orderBy, int startPage, int pageSize)
        {
            return All<T>(orderBy, startPage, pageSize);
        }

        public IEnumerable<T> Query(string where = "", params object[] args)
        {
            return Query<T>(where, args);
        }

        public PaginatedList<T> Paged(string where = "", string orderBy = "", int pageSize = 20, int currentPage = 1, params object[] args)
        {
            return Paged<T>(where, orderBy, pageSize, currentPage, args);
        }


        public IEnumerable<U> All<U>() where U : T, new()
        {
            return data.Values.Select(x => (U)DynamicQuery.To<U>(x.ToExpando()));
        }

        public IEnumerable<U> All<U>(string orderBy) where U : T, new()
        {
            return data.Values.OrderBy(orderBy).Select(x => (U)DynamicQuery.To<U>(x));
        }

        public IEnumerable<U> All<U>(int startPage, int pageSize) where U : T, new()
        {
            return Paged<U>(null, null, pageSize, startPage).Collection;
        }

        public IEnumerable<U> All<U>(string orderBy, int startPage, int pageSize) where U : T, new()
        {
            return Paged<U>(null, orderBy, pageSize, startPage).Collection;
        }

        public IEnumerable<U> Query<U>(string where = "", params object[] args) where U : T, new()
        {
            throw new NotImplementedException();
            //if (args != null)
            //{
            //    args.Count().ForEach(x =>
            //    {
            //        var val = args[x].ToString();
            //        if (args[x] is string || args[x] is DateTime)
            //        {
            //            val = "'" + val + "'";
            //        }
            //        if (args[x] is Guid)
            //        {
            //            val = "TO_GUID('" + val + "')";
            //        }
            //        where = where.Replace("@" + x, val);
            //    });
            //}

            //DataContext dataContext = new DataContext();
            //dataContext.Tables.Add(frameworkDatabase.Values, "Data");
            //Query query = new Query(dataContext);
            //query.Text = "select * from Data where " + where;
            //var dt = query.ExecuteDataTable();

            //return dt.ToList<U>();

           // return frameworkDatabase.Values.AsEnumerable().Query<T, dynamic>("select * from this where " + where).Select(DynamicQuery.To<U>);
            // return frameworkDatabase.Values.AsQueryable().Where(where, args).Select(x => (U)DynamicQuery.To<U>(x));
        }

        public PaginatedList<U> Paged<U>(string where = "", string orderBy = "", int pageSize = 20, int currentPage = 1, params object[] args) where U : T, new()
        {
            var results = Query<U>(where, args).OrderBy(orderBy);
            return new PaginatedList<U>(results.Skip(currentPage * pageSize).Take(pageSize), pageSize, currentPage, results.Count());
        }

        void PersistDico()
        {
            Write(data, path);
        }

        public U Save<U>(U o) where U : T, new()
        {
            return Save(o, false);
        }

        public U Save<U>(U o, bool insertOnly) where U : T, new()
        {
            o.CallMethod("Validate");
            var key = o.GetValue<KeyType>(Key);
            data[key] = o;
            PersistDico();
            return o;
        }

        public void Delete(object o)
        {
            var key = o.GetValue<KeyType>(Key);
            data.Remove(key);
            PersistDico();
        }

        public int Delete(string where = "", params object[] args)
        {
            var results = Query(where, args).Select(x => x.GetValue<KeyType>(Key)).ToList();
            results.ForEach(x => data.Remove(x));
            PersistDico();
            return results.Count;
        }

        public int DeleteAll()
        {
            var cnt = data.Count;
            data.Clear();
            PersistDico();
            return cnt;
        }

        public int Truncate()
        {
            return DeleteAll();
        }

        static void Write(Dictionary<KeyType, T> dictionary, string file)
        {
            using (FileStream fs = File.OpenWrite(file))
            using (BinaryWriter writer = new BinaryWriter(fs))
            {
                // Put count.
                writer.Write(dictionary.Count);
                // Write pairs.
                foreach (var pair in dictionary)
                {
                    writer.Write(pair.Key.ToJSON());
                    writer.Write(pair.Value.ToJSON());
                }
            }
        }

        static Dictionary<KeyType, T> Read(string file)
        {
            var result = new Dictionary<KeyType, T>();
            if (File.Exists(file))
            {
                using (FileStream fs = File.OpenRead(file))
                using (BinaryReader reader = new BinaryReader(fs))
                {
                    // Get count.
                    int count = reader.ReadInt32();
                    // Read in all pairs.
                    for (int i = 0; i < count; i++)
                    {
                        KeyType key = reader.ReadString().FromJSON<KeyType>();
                        T value = reader.ReadString().FromJSON<T>();
                        result[key] = value;
                    }
                }
            }
            return result;
        }


        public void InsertMany<U>(List<U> o) where U : T, new()
        {
            throw new NotImplementedException();
        }
    }
}
