﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Core.Exchange;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Exchange.WebServices.Autodiscover;

namespace Core
{
    public class ExchangeHelper
    {
        static ExchangeService GetService(string ExchangeServerUrl, string login, string password)
        {
            ExchangeService service = new Microsoft.Exchange.WebServices.Data.ExchangeService(ExchangeVersion.Exchange2007_SP1);
            service.Url = new Uri(ExchangeServerUrl);
            //service.PreAuthenticate = true;
            service.Credentials = (login.IsFilled() && password.IsFilled()) ? new NetworkCredential(login, password) : (NetworkCredential)System.Net.CredentialCache.DefaultCredentials;
            return service;
        }

        public static List<ExchangeEmail> MyUnreadMails(string ExchangeServerUrl)
        {
            return GetUnreadEmails(ExchangeServerUrl, null, null);
        }

        public static List<ExchangeEmail> GetUnreadEmails(string ExchangeServerUrl, string login, string password)
        {
            return GetUnreadEmails(ExchangeServerUrl, login, password, 10);
        }

        public static List<ExchangeEmail> GetUnreadEmails(string ExchangeServerUrl, string login, string password, int itemCount)
        {
            var service = GetService(ExchangeServerUrl, login, password);

            List<ExchangeEmail> result = new List<ExchangeEmail>();
            ItemView vw = new ItemView(itemCount);
            var searchFilter = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false);

            FindItemsResults<Item> msgs = service.FindItems(WellKnownFolderName.Inbox, searchFilter, vw);
            msgs.AsParallel().ForAll(msg => result.Add(((EmailMessage)msg).Wrap()));
            return result;
        }

        public static void MarkAsRead(string ExchangeServerUrl, string login, string password, string itemId)
        {
            var service = GetService(ExchangeServerUrl, login, password);
            var exchangeId = new ItemId(itemId);

            try
            {
                var msg = EmailMessage.Bind(service, exchangeId);
                msg.IsRead = true;
                msg.Update(ConflictResolutionMode.AlwaysOverwrite);
            }
            catch (Exception x)
            {
                x.LogToTrace();
            }
        }


        public static void Delete(string ExchangeServerUrl, string login, string password, IEnumerable<string> itemIds)
        {
            if (itemIds.IsNotNull() && (itemIds.Count() > 0))
            {
                var service = GetService(ExchangeServerUrl, login, password);
                var exchangeIds = itemIds.Select(u => new ItemId(u));
                service.DeleteItems(exchangeIds, DeleteMode.HardDelete, null, null);
            }
        }

        public static void Delete(string ExchangeServerUrl, string login, string password, string itemId)
        {
            Delete(ExchangeServerUrl, login, password, new List<string> { itemId });
        }

        public static void Forward(string ExchangeServerUrl, string login, string password, string itemId, string to, string message)
        {
            var service = GetService(ExchangeServerUrl, login, password);
            var exchangeId = new ItemId(itemId);

            var msg = EmailMessage.Bind(service, exchangeId);

            var propSet = new PropertySet(BasePropertySet.IdOnly);
            propSet.Add(ItemSchema.Body);
            msg.Load(propSet);

            var forward = msg.CreateForward();
            forward.ToRecipients.Add(new EmailAddress(to));
            forward.Body = new MessageBody(BodyType.HTML, message + Environment.NewLine + msg.Body.Text);
            forward.Send();
        }

        public static void Reply(string ExchangeServerUrl, string login, string password, string itemId, string message)
        {
            var service = GetService(ExchangeServerUrl, login, password);
            var exchangeId = new ItemId(itemId);
            var msg = EmailMessage.Bind(service, exchangeId);
            var propSet = new PropertySet(BasePropertySet.IdOnly);
            propSet.Add(ItemSchema.Body);
            msg.Load(propSet);

            var reply = msg.CreateReply(false);
            reply.Body = new MessageBody(BodyType.HTML, message + Environment.NewLine + msg.Body.Text);
            reply.Send();
        }


        static List<ExchangeEmail> GetEmail(string ExchangeServerUrl, string login, string password, string itemId)
        {
            var service = GetService(ExchangeServerUrl, login, password);

            List<ExchangeEmail> result = new List<ExchangeEmail>();
            ItemView vw = new ItemView(1);
            var searchFilter = new SearchFilter.IsEqualTo(EmailMessageSchema.Id, new ItemId(itemId));
            FindItemsResults<Item> msgs = service.FindItems(WellKnownFolderName.Inbox, searchFilter, vw);
            foreach (EmailMessage msg in msgs)
            {
                result.Add(msg.Wrap());
                msg.IsRead = true;
                msg.Save();
            }

            return result;
        }

        internal static void Send(string ExchangeServerUrl, string login, string password, ExchangeEmail email)
        {
            var service = GetService(ExchangeServerUrl, login, password);

            EmailMessage message = new EmailMessage(service);
            message.Subject = email.Subject.Safe();
            message.Body = email.Body.Safe();

            email.To.ForEach(x => message.ToRecipients.Add(x.Address));
            email.CC.ForEach(x => message.CcRecipients.Add(x.Address));
            email.BCC.ForEach(x => message.BccRecipients.Add(x.Address));

            email.Attachments.ForEach(x =>
                {
                    new Switch(x)
                        .Case<ExchangeFileAttachment>(f => message.Attachments.AddFileAttachment(f.Name, f.Data))
                        ;
                });

            message.SendAndSaveCopy();
        }
    }
}
