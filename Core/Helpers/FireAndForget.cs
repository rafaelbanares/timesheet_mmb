﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers
{
    /// <summary>
    /// Provides threadsafe, non-blocking methods to perform operations using the Fire and Forget pattern
    /// </summary>
    public static class ThreadUtil
    {

        public static void Queue(Action action)
        {
            System.Threading.WaitCallback callback = state => action();
            System.Threading.ThreadPool.QueueUserWorkItem(callback);
        }

        /// <summary>
        /// Callback used to call <code>EndInvoke</code> on the asynchronously
        /// invoked DelegateWrapper.
        /// </summary>
        private static AsyncCallback callback2 = EndWrapperInvoke2;

        /// <summary>
        /// An instance of DelegateWrapper which calls InvokeWrappedDelegate,
        /// which in turn calls the DynamicInvoke method of the wrapped
        /// delegate.
        /// </summary>
        private static DelegateWrapper2 wrapperInstance2 = new DelegateWrapper2(InvokeWrappedDelegate2);

        /// <summary>
        /// Executes the specified delegate with the specified arguments
        /// asynchronously on a thread pool thread.
        /// </summary>
        public static void FireAndForget(Action action)
        {
            // Handle the wrapper asynchronously, which will then
            // execute the wrapped delegate synchronously (in the
            // thread pool thread)
            wrapperInstance2.BeginInvoke(action, callback2, null);
        }

        /// <summary>
        /// Invokes the wrapped delegate synchronously
        /// </summary>
        private static void InvokeWrappedDelegate2(Action action)
        {
            action.DynamicInvoke(null);
        }


        private static void EndWrapperInvoke2(IAsyncResult ar)
        {
            wrapperInstance2.EndInvoke(ar);
            ar.AsyncWaitHandle.Close();
        }

        #region Nested type: DelegateWrapper

        /// <summary>	
        /// Delegate to wrap another delegate and its arguments
        /// </summary>
        private delegate void DelegateWrapper2(Action action);

        #endregion
    }
}
