﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Runtime.Caching;

namespace Core.Helpers
{
    public static class CacheHelper
    {
        static MemoryCache cache = MemoryCache.Default;

        static string GetInternalKey(string Key)
        {
            return ContextHelper.GetUserName().Replace("\\", "_") + "_" + Key.Trim();
        }

        public static T Get<T>(string key, T defaultValue)
        {
            key = GetInternalKey(key);
            if (cache.Contains(key)) return (T) cache.Get(key);
            return defaultValue;
        }

        public static T Get<T>(string key)
        {
            return Get<T>(key, default(T));
        }

        public static T GetAndDrop<T>(string key)
        {
            var val = Get<T>(key);
            Invalidate(key);
            return val;
        }

        public static void Set<T>(string key, T value)
        {
            var lim = cache.CacheMemoryLimit;
            key = GetInternalKey(key);
            if (cache.Contains(key)) cache.Remove(key);
            if (value != null) cache.Add(key, value, new CacheItemPolicy { SlidingExpiration = 10.Minutes() });
        }

        public static void Invalidate(string key)
        {         
            key = GetInternalKey(key);
            cache.Remove(key);            
        }
    }
}
