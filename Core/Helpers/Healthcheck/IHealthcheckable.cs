﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Helpers.Healthcheck
{
    public interface IHealthchecker
    {
        void Test();
        string TestName { get; }
        string SuggestionText { get; }
    }
}
