﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Emailing;

namespace Core.Helpers.Healthcheck.Internals
{
    public class SmtpChecker : IHealthchecker
    {
        public void Test()
        {
            if (!SmtpClientHelper.TestConnect())
            {
                throw new ApplicationException("Unable to connect to Smtp Server with following details : " + SmtpClientHelper.Configuration.SmtpInfo.ToString());
            }
        }

        public string TestName
        {
            get { return "Smtp connection"; }
        }

        public string SuggestionText
        {
            get { return "Check smtp server is up, firewall is open, security is valid"; }
        }
    }
}
