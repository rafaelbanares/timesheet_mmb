﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Helpers.Configuration;

namespace Core.Helpers.Healthcheck.Internals
{
    public class ConfigChecker : IHealthchecker
    {
        public void Test()
        {
            Config.Set("healtcheckTest", "1");
            Config.Drop("healtcheckTest");
        }

        public string TestName
        {
            get { return "Read/write configuration repository"; }
        }

        public string SuggestionText
        {
            get { return "Re-run setup"; }
        }
    }
}
