﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Core
{
    public enum LogonType
    {
        /// <summary>
        /// This logon type is intended for users who will be interactively using the computer, such as a user being logged on  
        /// by a terminal server, remote shell, or similar process.
        /// This logon type has the additional expense of caching logon information for disconnected operations;
        /// therefore, it is inappropriate for some client/server applications,
        /// such as a mail server.
        /// </summary>
        Logon32LogonInteractive = 2,

        /// <summary>
        /// This logon type is intended for high performance servers to authenticate plaintext passwords.
        /// The LogonUser function does not cache credentials for this logon type.
        /// </summary>
        Logon32LogonNetwork = 3,


        /// <summary>
        /// This logon type is intended for batch servers, where processes may be executing on behalf of a user without
        /// their direct intervention. This type is also for higher performance servers that process many plaintext
        /// authentication attempts at a time, such as mail or Web servers.
        /// The LogonUser function does not cache credentials for this logon type.
        /// </summary>
        Logon32LogonBatch = 4,

        /// <summary>
        /// Indicates a service-type logon. The account provided must have the service privilege enabled.
        /// </summary>
        Logon32LogonService = 5,

        /// <summary>
        /// This logon type is for GINA DLLs that log on users who will be interactively using the computer.
        /// This logon type can generate a unique audit record that shows when the workstation was unlocked.
        /// </summary>
        Logon32LogonUnlock = 7,

        /// <summary>
        /// This logon type preserves the name and password in the authentication package, which allows the server to make
        /// connections to other network servers while impersonating the client. A server can accept plaintext credentials
        /// from a client, call LogonUser, verify that the user can access the system across the network, and still
        /// communicate with other servers.
        /// NOTE: Windows NT:  This value is not supported.
        /// </summary>
        Logon32LogonNetworkCleartext = 8, // Win2K or higher

        /// <summary>
        /// This logon type allows the caller to clone its current token and specify new credentials for outbound connections.
        /// The new logon session has the same local identifier but uses different credentials for other network connections.
        /// NOTE: This logon type is supported only by the LOGON32_PROVIDER_WINNT50 logon provider.
        /// NOTE: Windows NT:  This value is not supported.
        /// </summary>
        Logon32LogonNewCredentials = 9 // Win2K or higher

    } ;

    public enum LogonProvider
    {
        Logon32ProviderDefault = 0,
        Logon32ProviderWinnt35 = 1,
        Logon32ProviderWinnt40 = 2,
        Logon32ProviderWinnt50 = 3
    } ;

    public enum ImpersonationLevel
    {
        SecurityAnonymous = 0,
        SecurityIdentification = 1,
        SecurityImpersonation = 2,
        SecurityDelegation = 3
    }

    public enum BuiltinUser
    {
        None,
        LocalService,
        LocalSystem,
        NetworkService
    }

    internal class Win32NativeMethods
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern int LogonUser(string lpszUserName,
                                           string lpszDomain,
                                           string lpszPassword,
                                           int dwLogonType,
                                           int dwLogonProvider,
                                           ref IntPtr phToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken,
                                                int impersonationLevel,
                                                ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);
    }
    
    public class DomainImpersonator : IDisposable
    {
        private WindowsImpersonationContext _wic;


        public DomainImpersonator(string userName, string domainName, string password)
        {
            Impersonate(userName, domainName, password);
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="DomainImpersonator"/> class.
        /// </summary>
        public DomainImpersonator()
        {
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="DomainImpersonator"/> class.
        /// </summary>
        public DomainImpersonator(BuiltinUser builtinUser)
        {
            Impersonate(builtinUser);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            UndoImpersonation();
        }

        public void Impersonate(BuiltinUser builtinUser)
        {
            Impersonate(String.Empty, "NT AUTHORITY", String.Empty, LogonType.Logon32LogonService, LogonProvider.Logon32ProviderDefault, builtinUser);
        }

        /// <summary>
        /// Impersonates the specified user account.
        /// </summary>
        ///
        public void Impersonate(string userName, string domainName, string password)
        {
            Impersonate(userName, domainName, password, LogonType.Logon32LogonInteractive,
                        LogonProvider.Logon32ProviderDefault, BuiltinUser.None);
        }

        /// <summary>
        /// Impersonates the specified user account.
        /// </summary>
        ///
        public void Impersonate(string userName, string domainName, string password, LogonType logonType,
                                LogonProvider logonProvider, BuiltinUser builtinUser)
        {
            UndoImpersonation();

            switch (builtinUser)
            {
                case BuiltinUser.None: if (String.IsNullOrEmpty(userName)) return; break;
                case BuiltinUser.LocalService: userName = "LOCAL SERVICE"; break;
                case BuiltinUser.NetworkService: userName = "NETWORK SERVICE"; break;
                case BuiltinUser.LocalSystem: userName = "SYSTEM"; break;
            }

            var logonToken = IntPtr.Zero;
            var logonTokenDuplicate = IntPtr.Zero;
            try
            {
                // revert to the application pool identity, saving the identity of the current requestor
                _wic = WindowsIdentity.Impersonate(IntPtr.Zero);

                // do logon & impersonate
                if (Win32NativeMethods.LogonUser(userName,
                                                 domainName,
                                                 password,
                                                 (int) logonType,
                                                 (int) logonProvider,
                                                 ref logonToken) != 0)
                {
                    if (
                        Win32NativeMethods.DuplicateToken(logonToken, (int) ImpersonationLevel.SecurityImpersonation,
                                                          ref logonTokenDuplicate) != 0)
                    {
                        var wi = new WindowsIdentity(logonTokenDuplicate);
                        wi.Impersonate();
                            // discard the returned identity context (which is the context of the application pool)
                    }
                    else
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                }
                else
                    throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            finally
            {
                if (logonToken != IntPtr.Zero)
                    Win32NativeMethods.CloseHandle(logonToken);

                if (logonTokenDuplicate != IntPtr.Zero)
                    Win32NativeMethods.CloseHandle(logonTokenDuplicate);
            }
        }

        /// <summary>
        /// Stops impersonation.
        /// </summary>
        private void UndoImpersonation()
        {
            // restore saved requestor identity
            if (_wic != null)
                _wic.Undo();
            _wic = null;
        }
    }


}