﻿using System;
using System.Messaging;
using System.Runtime.InteropServices;

namespace Core
{
    public static class MsmqHelper
    {
        public static void EnsureQueue(string queueName)
        {
            queueName = String.Format(@".\Private$\{0}", queueName);

            if (!MessageQueue.Exists(queueName))
            {
                var queue = MessageQueue.Create(queueName, true);
                queue.Authenticate = false;
                queue.EncryptionRequired = EncryptionRequired.None;
                queue.SetPermissions("Everyone", MessageQueueAccessRights.FullControl, AccessControlEntryType.Allow);
            }
        }


        public static int GetPendingMessageForQueue(string queueFormatName)
        {
            if (!MessageQueue.Exists(queueFormatName)) return 0;
            MessageQueue queue = new MessageQueue(queueFormatName);
            return (int)queue.GetCount();

        }

        [DllImport("kernel32")]
        static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool FreeLibrary(IntPtr hModule);

        public static void Setup()
        {

            bool loaded;
            try
            {
                IntPtr handle = LoadLibrary("Mqrt.dll");

                if (handle == IntPtr.Zero || handle.ToInt32() == 0)
                {
                    loaded = false;
                }
                else
                {
                    loaded = true;
                    FreeLibrary(handle);
                }
            }
            catch
            {
                loaded = false;
            }

            if (!loaded)
            {
                var commandLine = string.Empty;

                if (Environment.OSVersion.Version.Major < 6) // Windows XP or earlier
                {
                    string fileName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "MSMQAnswer.ans");

                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fileName))
                    {
                        //writer.WriteLine("[Version]");
                        //writer.WriteLine("Signature = \"$Windows NT$\"");
                        //writer.WriteLine();
                        //writer.WriteLine("[Global]");
                        //writer.WriteLine("FreshMode = Custom");
                        //writer.WriteLine("MaintenanceMode = RemoveAll");
                        //writer.WriteLine("UpgradeMode = UpgradeOnly");
                        //writer.WriteLine();
                        writer.WriteLine("[Components]");
                        writer.WriteLine("msmq_Core = ON");
                        writer.WriteLine("msmq_LocalStorage = ON");
                        writer.WriteLine("msmq_ADIntegrated = ON");
                        writer.WriteLine("msmq_TriggersService = ON");
                        writer.WriteLine("msmq_HTTPSupport = OFF");
                        writer.WriteLine("msmq_RoutingSupport = OFF");
                        writer.WriteLine("msmq_MQDSService = OFF");

                    }

                    commandLine = "sysocmgr.exe /i:%windir%\\inf\\sysoc.inf /u:\"" + fileName + "\"";

                }
                else // Vista or later
                {
                    commandLine = "ocsetup.exe MSMQ-Container;MSMQ-Server /passive";
                    
                }
                CommandHelper.Run(commandLine, "");
            }
        }
    }
}
