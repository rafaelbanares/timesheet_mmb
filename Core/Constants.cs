﻿using System;
using System.IO;
using System.Reflection;
namespace Core
{
    public static class Constants
    {
        public static class DBUser
        {
            public const string Name = "FrameworkUser";
            public const string Password = "Hk@176mdiw!j25*";
        }

        public static class WinUser
        {
            public const string Name = "IUSR_RSCA";
            public const string Password = "123456";
            public const string DisplayName = "SQL Server Reporting Services Account";
            public const string Description = "Built-in account for SQL Server Reporting Services Client Access";
        }

        public static class Directories
        {
            static Directories()
            {
                if (!Directory.Exists(RootDirectory)) Directory.CreateDirectory(RootDirectory);
                if (!Directory.Exists(TraceDirectory)) Directory.CreateDirectory(TraceDirectory);
                if (!Directory.Exists(TempDirectory)) Directory.CreateDirectory(TempDirectory);
                if (!Directory.Exists(DataDirectory)) Directory.CreateDirectory(DataDirectory);
            }

            public static readonly string RootDirectory = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData), "MMB", Core.Helpers.Environment.ApplicationName);
            public static readonly string TraceDirectory = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData), "MMB", Core.Helpers.Environment.ApplicationName, "Trace");
            public static readonly string TempDirectory = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData), "MMB", Core.Helpers.Environment.ApplicationName, "Temp");
            public static readonly string DataDirectory = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData), "MMB", Core.Helpers.Environment.ApplicationName, "Data");
        }

        public static class Files
        {
            public static readonly string Trace = Path.Combine(Directories.TraceDirectory, "trace.txt");
        }

       
        
        /// <summary>
        /// Constants for regular expressions
        /// </summary>
        public static class RegularExpressions
        {
            /// <summary>
            /// Regular expression to validate an email address
            /// </summary>
            public const string EmailAddress =
                @"^[a-zA-Z][\w'\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$";

            /// <summary>
            /// Regular expression to validate a URL
            /// </summary>
            public const string Url =
                @"^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$";

            /// <summary>
            /// Regular expression to validate a decimal
            /// EDIT: 04/26/2012
            /// Include comma and allow decimals without leading zeros
            /// </summary>
            public const string Decimal = @"^[+\-]?(([\d]+(,[0-9]{3})*(\.\d*)?)|\.\d+)([+-]?[0-9]+)?";                

            /// <summary>
            /// Regular expression to validate file name. This expression
            /// can be used to remove/replace forbidden characters.
            /// </summary>
            public const string ForbiddenFileCharacters = @"[:<>?*""/\\]";
        }

        /// <summary>
        /// Static Framework Error Bus class.
        /// </summary>
        public static class ErrorMessages
        {
            public const string AttributeRequired = "Attribute '{0}' is required in node '{1}'";
            public const string InvalidTypeValue = "'{0}' is not of expected type '{1}'";
            public const string ChildSectionRequired = "Child section '{0}' is required in section '{1}'";
        }


    }
}