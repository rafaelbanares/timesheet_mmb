﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Diagnostics;
using System.Web;
using Core.Model.Utilities;

namespace Core.Model.Utilities
{
    public class Snapshot
    {
        private Snapshot()
        { }

        public static Snapshot Get()
        {
            var res = new Snapshot();

            LocalUserWithAdminRights.Do(
                () =>
                    {
                        res.ApplicationName = Core.Helpers.Environment.ApplicationName;
                        res.ApplicationServer = Environment.MachineName;
                        
                        var _proc = Process.GetCurrentProcess();
                        var ts = (DateTime.Now - _proc.StartTime);
                        res.UpTime = String.Format("{0}:{1}:{2}", (int)ts.TotalHours, ts.Minutes.ToString().PadLeft(2, '0'), ts.Seconds.ToString().PadLeft(2, '0'));
                        res.Counters =  All();

                        res.PhysicalActualMemory = Core.FileHelper.DisplayFileSize(_proc.WorkingSet64);
                        res.PhysicalPeakMemory = Core.FileHelper.DisplayFileSize(_proc.PeakWorkingSet64);
                        res.VirtualActualMemory = Core.FileHelper.DisplayFileSize(_proc.VirtualMemorySize64);
                        res.VirtualPeakMemory = Core.FileHelper.DisplayFileSize(_proc.PeakVirtualMemorySize64);
                        res.Identity = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                        //try
                        //{
                        //    res.ProcessHistory = ProcessModelInfo
                        //        .GetHistory(10)
                        //        .Select(x =>
                        //            new ServiceHistory
                        //            {
                        //                StartTime = x.StartTime.ToShortDateString(),
                        //                Age = String.Format("{0}:{1}:{2}", (int)x.Age.TotalHours, x.Age.Minutes.ToString().PadLeft(2, '0'), x.Age.Seconds.ToString().PadLeft(2, '0')),
                        //                PeakMemoryUsed = Core.FileHelper.DisplayFileSize(x.PeakMemoryUsed),
                        //                ShutdownReason = x.ShutdownReason.ToString()
                        //            });
                        //}
                        //catch(Exception x)
                        //{
                        //    x.Log();
                        //}
                    }
                );

            return res;
        }

        public string ApplicationName { get; private set; }
        public string Identity { get; private set; }
        public string ApplicationServer { get; private set; }
        public string UpTime { get; private set; }
        public IEnumerable<PerfCounterResult> Counters { get; private set; }
        //public IEnumerable<ServiceHistory> ProcessHistory { get; private set; }
        public string PhysicalActualMemory { get; private set; }
        public string PhysicalPeakMemory { get; private set; }
        public string VirtualActualMemory { get; private set; }
        public string VirtualPeakMemory { get; private set; }

        public class ServiceHistory
        {
            public string StartTime { get; internal set; }
            public string Age { get; internal set; }
            public string PeakMemoryUsed { get; internal set; }
            public string ShutdownReason { get; internal set; }
        }


        static IEnumerable<PerfCounterResult> All()
        {
            var items = new List<PerfCounter>();
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Requests Total", InstanceName = "__Total__", Description = "The total number of requests since the application was started." });
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Requests Succeeded", InstanceName = "__Total__", Description = "The number of requests that executed successfully (status code 200)." });
            items.Add(new PerfCounter { Category = "ASP.NET", Name = "Requests Queued", Description = "The number of requests waiting for service from the queue. When this number starts to increment linearly with respect to client load, the Web server computer has reached the limit of concurrent requests that it can process. The default maximum for this counter is 5,000. You can change this setting in the computer's Machine.config file." });
            items.Add(new PerfCounter { Category = "ASP.NET", Name = "Requests Rejected", Description = "The number of requests rejected because the request queue was full." });
            items.Add(new PerfCounter { Category = "ASP.NET", Name = "Worker Processes Running", Description = "The number of worker processes running on the server computer." });
            items.Add(new PerfCounter { Category = "ASP.NET", Name = "Worker Process Restarts", Description = "The number of times a worker process has been restarted on the server computer. A worker process can be restarted if it fails unexpectedly or when it is intentionally recycled. In the case of unexpected increases in this counter you should investigate as soon as possible." });
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Requests Not Authorized", InstanceName = "__Total__", Description = "he number of requests that failed due to no authorization (status code 401)." });
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Requests Timed Out", InstanceName = "__Total__", Description = "The number of requests that timed out (status code 500)." });
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Requests Not Found", InstanceName = "__Total__", Description = "The number of requests that failed because resources were not found (status code 404, 414)." });
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Requests/Sec", InstanceName = "__Total__", Description = "The number of requests executed per second. This represents the current throughput of the application. Under constant load, this number should remain within a certain range, barring other server work (such as garbage collection, cache cleanup thread, external server tools, and so on)." });
            items.Add(new PerfCounter { Category = "ASP.NET Applications", Name = "Pipeline Instance Count", InstanceName = "__Total__", Description = "The number of active request pipeline instances for the specified ASP.NET application. Since only one execution thread can run within a pipeline instance, this number gives the maximum number of concurrent requests that are being processed for a given application. In most circumstances it is better for this number to be low when under load, which signifies that the CPU is well utilized." });
            items.Add(new PerfCounter { Category = "ASP.NET", Name = "Application Restarts", Description = "Number of times the application has been restarted during the web server's lifetime." });
            items.Add(new PerfCounter { Category = "PhysicalDisk", Name = "Avg. Disk Queue Length", InstanceName = "_Total", Description = " average number of both read and write requests that were queued for the selected disk during the sample interval." });
            items.Add(new PerfCounter { Category = "PhysicalDisk", Name = "Avg. disk sec/Read", InstanceName = "_Total", Description = "Average time, in seconds, of a read of frameworkDatabase to the disk." });
            items.Add(new PerfCounter { Category = "PhysicalDisk", Name = "Avg. disk sec/Write", InstanceName = "_Total", Description = "Average time, in seconds, of a write of frameworkDatabase to the disk." });
            return PerfCounter.Read(items.ToArray());
        }
    }
}
