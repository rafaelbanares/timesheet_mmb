﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Security.Principal;

namespace Core.Model.Utilities
{
    public static class LocalUserWithAdminRights
    {
        public static void Do(Action job)
        {
            using (var imp = new DomainImpersonator(Core.Constants.WinUser.Name, Environment.MachineName, Core.Constants.WinUser.Password))
            {
                job.Invoke();
            }
        }
    }

    public static class CurrentlyLoggedInUser
    {
        public static void Do(Action job)
        {
            WindowsIdentity winId = (WindowsIdentity)ContextHelper.GetUserIdentity();
            WindowsImpersonationContext ctx = null;
            ctx = winId.Impersonate();
            job.Invoke();
            ctx.Undo();
        }
    }

    public static class LocalService
    {
        public static void Do(Action job)
        {
            using (var imp = new DomainImpersonator(BuiltinUser.LocalService))
            {
                job.Invoke();
            }
        }
    }

    public static class LocalSystem
    {
        public static void Do(Action job)
        {
            using (var imp = new DomainImpersonator(BuiltinUser.LocalSystem))
            {
                job.Invoke();
            }
        }
    }

    public static class NetworkService
    {
        public static void Do(Action job)
        {
            using (var imp = new DomainImpersonator(BuiltinUser.NetworkService))
            {
                job.Invoke();
            }
        }
    }
}
