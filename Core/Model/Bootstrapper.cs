﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Isam.Esent.Collections.Generic;
using Core.Model.Tasks;
using System.IO;
using System.Threading;

namespace Core.Model
{
    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            var writer = new TextWriterTraceListener(Console.Out);
            Trace.Listeners.Add(writer);

            var traceFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "LastBootStapTrace.txt");
            try
            {
                File.Delete(traceFilePath);
            }
            catch (Exception x)
            {
                Trace.WriteLine("Unable to delete Trace file");
                x.LogToTrace();
            }


            Trace.WriteLine(string.Format("Trace starting at {0}", traceFilePath));
            var fw = new TextWriterTraceListener(File.CreateText(traceFilePath));

            Trace.Listeners.Add(fw);

            Trace.WriteLine("Bootstrapper Initialization....");
            Trace.Indent();
            Trace.WriteLine("Temp path : " + Path.GetTempPath());

            if (Thread.CurrentPrincipal == null) Trace.WriteLine("Thread.CurrentPrincipal is null");
            else if (Thread.CurrentPrincipal.Identity == null) Trace.WriteLine("Thread.CurrentPrincipal.Identity is null");
            else Trace.WriteLine("Thread.CurrentPrincipal.Identity.Name : " + Thread.CurrentPrincipal.Identity.Name);


            Trace.Unindent();
        }

        public delegate void ApplicationHandler(Core.Model.ICoreApplication app);
        public static event ApplicationHandler OnApplicationEnvironmentalSetupNeeded;
        public static event ApplicationHandler OnApplicationInitialized;
        public static event ApplicationHandler OnApplicationMounting;
        public static event ApplicationHandler OnApplicationMounted;

        private static readonly object _lock = new object();
        private static bool _started;

        public static void Start(Core.Model.ICoreApplication application, string[] arguments)
        {
            lock (_lock)
            {
                if (_started) return;
                var traceFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), application.Name + "_trace.txt");

                var fw = new TextWriterTraceListener(File.CreateText(traceFilePath));
                Trace.Listeners.Add(fw);

                Trace.WriteLine(string.Format("Trace starting at {0}", traceFilePath));
                Trace.WriteLine("Bootstrapper start requested....");
                Trace.Indent();

                if (Context.CoreApplication.IsNull()) // This is a check to prevent multiple possible calls from Client applications
                {
                    // Link Context and application 
                    Context.Initialize(application);

                    if (!System.Threading.Thread.CurrentThread.Name.IsFilled()) System.Threading.Thread.CurrentThread.Name = "Bootstrapper main thread";
                    if (arguments.IsNotNull()) application.Arguments = arguments;

                    try
                    {
                        Trace.WriteLine("Make 'basic' initialization");
                        ActiveDirectoryHelper.CreateLocalAdminWindowsAccount(Constants.WinUser.Name, Constants.WinUser.Password, Constants.WinUser.DisplayName, Constants.WinUser.Description);

                        // Retrieving last run informations
                        Trace.WriteLine("Retrieving last run informations");
                        var LastRunStart = RegistryHelper.Get<DateTime?>(application.Name, "LastRunStart", null);
                        var LastRunStop = RegistryHelper.Get<DateTime?>(application.Name, "LastRunStop");

                        // First time setup on this machine
                        // Some environmental setup might be required 
                        // stuff like looking for a database server, etc...
                        if (LastRunStart.IsNull())
                        {
                            Trace.WriteLine("Last Run is null: Raise OnApplicationEnvironmentalSetupNeeded");
                            if (OnApplicationEnvironmentalSetupNeeded != null) OnApplicationEnvironmentalSetupNeeded(application);
                            return;
                        }

                        // Create Database if needed
                        if (application.DBConnection.IsNotNull())
                        {
                            Trace.WriteLine("Application.DBConnection is set");
                            application.DBConnection.CreateIfNotExists();
                            application.ArchiveDBConnection.CreateIfNotExists();
                        }
                        else
                        {
                            Trace.TraceWarning("Application.DBConnection is NOT set. Whether the application does not need any database, whether it is not been properly set during environmental setup");
                        }

                        // Setup Tasks
                        Trace.WriteLine("Launching Setup tasks");
                        Trace.Indent();
                        TasksHelper.DoSetup();
                        Trace.Unindent();

                        // Registering Last Run Start date
                        RegistryHelper.Set(application.Name, "LastRunStart", DateTime.UtcNow);


                        // Loading available features
                        Trace.WriteLine("Features injection");
                        Trace.Indent();
                        FeaturesHelper.Inject(ref application);
                        Trace.Unindent();

                        // If LastRunStop is null and application was already started
                        // then application did not properly stopped at last run
                        if (!LastRunStart.IsNull())
                        {
                            if (LastRunStop.IsNull() || (LastRunStop < LastRunStart)) Trace.WriteLine("CoreApplication did not properly stopped at last run");
                            else
                            {
                                var span = LastRunStop.Value - LastRunStart.Value;
                                Trace.WriteLine(String.Format("Last application run lasted {0} days {1} hours {2} minutes {3} seconds.", span.Days, span.Hours, span.Minutes, span.Seconds));
                            }
                        }

                        if (OnApplicationInitialized != null) OnApplicationInitialized(application);

                        // Executing startup tasks
                        Trace.WriteLine("Startup tasks Exscution");
                        Trace.Indent();
                        TasksHelper.DoStart();
                        Trace.Unindent();


                        if (OnApplicationMounting != null) OnApplicationMounting(application);
                        application.Mounted();
                        if (OnApplicationMounted != null) OnApplicationMounted(application);

                        _started = true;

                    }
                    catch (Exception ex)
                    {
                        ex.LogToTrace();
                        throw ex;
                    }
                }

                else
                {
                    Trace.WriteLine("Context already initialized");
                }
                Trace.Unindent();
            }
        }


        public static void EnvironmentalSetupCompleted(ICoreApplication app)
        {
            Trace.WriteLine("EnvironmentalSetupCompleted Signaled !!!");
            Context.CoreApplication = null;
            RegistryHelper.Set(app.Name, "LastRunStart", DateTime.UtcNow);
            Trace.WriteLine("Restarting Bootstrapper !!!");
            Bootstrapper.Start(app, null);
        }


        public static void Stop()
        {
            // Executing shutdown tasks
            TasksHelper.DoShutdown();
            if (Context.CoreApplication.IsNotNull()) RegistryHelper.Set(Context.CoreApplication.Name, "LastRunStop", DateTime.UtcNow);
        }
    }
}
