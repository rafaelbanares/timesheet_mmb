﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using Core.Model;
using Core.Helpers.Logging;

namespace Core.Helpers.Logging
{
    public class PurgeLogsTask : IScheduledTask
    {
        public void Execute(Schedule jobSchedule)
        {
            int number;
            if (!int.TryParse(jobSchedule.Arguments, out number))
            {
                throw new FrameworkException("You must specify a numeric argument representing the maximum logging entry age, in days.");
            }
            Logger.DropEntriesOlderThan(number.Days());
        }
    }
}
