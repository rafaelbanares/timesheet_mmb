﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using Core.WinService.Scheduling.Cron;

namespace Core.Helpers.Logging
{
    public class PurgeLogSchedule : PredefinedSchedule
    {
        public static readonly Guid ScheduleId = new Guid("217E14FC-BEFB-4A04-ACDA-533842A40D0F");

        public PurgeLogSchedule()
            : base(typeof(PurgeLogsTask), ScheduleId, "Purge logs older than 30 days", CronBuilder.CreateMonthlyTrigger(), "30")
        {
        }
    }
}
