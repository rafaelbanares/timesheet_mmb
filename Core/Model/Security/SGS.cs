﻿using System;
using System.Collections.Generic;
using Core.Model;

namespace Core.Model.Security
{
    public class SGSx
    {
        public class WellKnownDomainUser
        {
            public static void Do(Action job)
            {
                var splitted = _AD_ACCOUNT_USERNAME.Split('\\');
                using (var imp = new DomainImpersonator(splitted[1], SGSDomain.Eame.ToString(), _AD_ACCOUNT_PASSWORD))
                {
                    job.Invoke();
                }
            }
        }

        [Flags]
        public enum ADSearchScope
        {
            Intranet = 1,
            Extranet = 2,
            Both = Intranet | Extranet
        }

        public static SGSDomain GetDomain(string domainName)
        {
            if (domainName.ToLower().StartsWith("eame")) return SGSDomain.Eame;
            if (domainName.ToLower().StartsWith("amr")) return SGSDomain.Amr;
            if (domainName.ToLower().StartsWith("apac")) return SGSDomain.Apac;
            if (domainName.ToLower().StartsWith("b2b")) return SGSDomain.B2b;
            throw new Exception("Unknown SGS domain name");
        }

        private const string _AD_ACCOUNT_USERNAME = @"eame.global.sgs.com\svc_ipp";
        private const string _AD_ACCOUNT_PASSWORD = "ipp2010";

        public static List<ActiveDirectoryUserSearchResult> Search(SGSDomain domain, string SearchedName)
        {
            SearchedName.CanNotBeEmpty();
            if (SearchedName.Length < 2) throw new Exception("Must at least be 2 chars length");

            List<ActiveDirectoryUserSearchResult> result = new List<ActiveDirectoryUserSearchResult>();

            if (domain == SGSDomain.Eame)
                result.AddRange(ActiveDirectoryHelper.GetUsersListInDomain("eame.global.sgs.com", SearchedName, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD));

            if (domain == SGSDomain.Amr)
                result.AddRange(ActiveDirectoryHelper.GetUsersListInDomain("amr.global.sgs.com", SearchedName, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD));

            if (domain == SGSDomain.Apac)
                result.AddRange(ActiveDirectoryHelper.GetUsersListInDomain("apac.global.sgs.com", SearchedName, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD));

            if (domain == SGSDomain.B2b)
                result.AddRange(ActiveDirectoryHelper.GetUsersListInDomain("b2b.sgs.com", SearchedName, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD));

            return result;
        }

        public static ActiveDirectoryUser GetUserByLogin(string domainName, string searchedLogin)
        {
            return ActiveDirectoryHelper.GetUserByLogin(domainName, searchedLogin, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD);
        }

        public static ActiveDirectoryUser CurrentUser
        {
            get
            {
                var splitted = Operator.Current.Split('\\');
                return ActiveDirectoryHelper.GetUserByLogin(splitted[0], splitted[1], _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD);
            }
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userPath">The user path.</param>
        /// <returns></returns>
        public static ActiveDirectoryUser GetUserByPath(string userPath)
        {
            return ActiveDirectoryHelper.GetUser(userPath, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD);
        }

        public static ActiveDirectoryUser CreateUser(
            string FirstName
            , string LastName
            , string Description
            , string Email
            , string Organisation
            , string Department
            , string JobTitle
            , string Phone
            , string MobilePhone
            , string Fax
            , string Address
            , string ZipCode
            , string City
            , string State
            , string CountryCode
            , string Country
            , AccountCreatedNotificationSender notifier
            )
        {
            FirstName.CanNotBeEmpty();
            LastName.CanNotBeEmpty();
            Email.CanNotBeEmpty();
            Email.CanNotContain("@sgs.com");

            string OUPath = "LDAP://b2b.sgs.com/OU=IPP,DC=b2b,DC=sgs,DC=com";
            string loginToCreate = ActiveDirectoryHelper.GenerateLogin(FirstName, LastName);
            string password = PasswordHelper.Generate();
            string displayName = FirstName + " " + LastName;

            loginToCreate.CanNotBeEmpty();

            int i = 1;
            var isNotValid = true;

            while (isNotValid)
            {
                // while loginToCreate length > AD username maxlength (20)
                while (loginToCreate.Length > 20)
                {
                    i = 1;
                    // if FirstName length > 1, then truncate it
                    if (FirstName.Length > 1)
                        FirstName = FirstName.Substring(0, FirstName.Length - 1);
                    // else truncate LastName
                    else
                        LastName = LastName.Substring(0, LastName.Length - 1);
                    // regenerate login
                    loginToCreate = ActiveDirectoryHelper.GenerateLogin(FirstName, LastName);
                }

                // check if generated login already exists
                isNotValid = ActiveDirectoryHelper.LoginExists("b2b.sgs.com", loginToCreate, _AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD);

                // if valid continue user creation process
                if (!isNotValid)
                    continue;

                // increment login with index
                if (i > 1)
                    loginToCreate = loginToCreate.Substring(0, loginToCreate.Length - (i - 1));

                loginToCreate = loginToCreate + i.ToString();
                i++;
            }

            ActiveDirectoryUser result = null;

            result = ActiveDirectoryHelper.CreateUser(_AD_ACCOUNT_USERNAME, _AD_ACCOUNT_PASSWORD, OUPath, "b2b.sgs.com", loginToCreate, password, FirstName, LastName, displayName, Description, Email, Organisation, Department, JobTitle, Phone, MobilePhone, Fax, Address, ZipCode, City, State, CountryCode, Country);

            if (notifier != null) notifier.Notify(result, password);

            return result;
        }
    }
}
