﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model.Security
{
    public abstract class Authorization
    {
        public virtual bool IsAuthorized() { return Operator.IsSuperUser(); }
          
        public void MustHave()
        {
            MustHave(string.Format("[[FWK.You do not have sufficient privileges to ]]: {0}", Description));
        }

        public void MustHave(string errorMessage)
        {
            if (!IsAuthorized()) throw new NotAuthorizedException(errorMessage);
        }

        public virtual string Description
        {
            get { return "[[Security." + ToString() + "]]"; }
        }

        public override string ToString()
        {
            Type t = this.GetType();
            string operationName = t.Name;
            if (t.IsNested) operationName = t.DeclaringType.Name + "." + operationName;
            return operationName;
        }
    }
}
