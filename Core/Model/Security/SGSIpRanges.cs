﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model.Security
{
    public class IpRange : IComparable<IpRange>, IEquatable<IpRange>
    {
        public string Range { get; set; }
        public string Name { get; set; }

        internal Version Version { get { return Version.Parse(Range); } }
        public int CompareTo(IpRange other)
        {
            return this.Version.CompareTo(other.Version);
        }

        public bool Equals(IpRange other)
        {
            return this.Version.Equals(other.Version);
        }
    }

    public static class SGSIpRanges
    {
        public static IpRange GetRange(string IPAddress)
        {
            var current = new IpRange { Range = IPAddress };

            return All()
                .Where(x => x.Version <= current.Version)
                .OrderBy(x => x)
                .LastOrDefault();
        }


        public static IEnumerable<IpRange> All()
        {
            yield return SGSIpRanges.Albania;
            yield return SGSIpRanges.Algeria;
            yield return SGSIpRanges.Angola;
            yield return SGSIpRanges.Argentina;
            yield return SGSIpRanges.Australia;
            yield return SGSIpRanges.Austria;
            yield return SGSIpRanges.Azerbaijan;
            yield return SGSIpRanges.Bangladesh;
            yield return SGSIpRanges.Belarus;
            yield return SGSIpRanges.Belgium;
            yield return SGSIpRanges.Benin;
            yield return SGSIpRanges.Bolivia;
            yield return SGSIpRanges.BosniaAndHerzegowina;
            yield return SGSIpRanges.Botswana;
            yield return SGSIpRanges.Brazil;
            yield return SGSIpRanges.Bulgaria;
            yield return SGSIpRanges.BurkinaFaso;
            yield return SGSIpRanges.Burundi;
            yield return SGSIpRanges.Cambodia;
            yield return SGSIpRanges.Cameroon;
            yield return SGSIpRanges.Canada;
            yield return SGSIpRanges.CentralAfricanRepublic;
            yield return SGSIpRanges.Chile;
            yield return SGSIpRanges.China;
            yield return SGSIpRanges.ChinaReserved;
            yield return SGSIpRanges.Colombia;
            yield return SGSIpRanges.Congo;
            yield return SGSIpRanges.Croatia;
            yield return SGSIpRanges.CzechRepublic;
            yield return SGSIpRanges.Ecuador;
            yield return SGSIpRanges.Egypt;
            yield return SGSIpRanges.Estonia;
            yield return SGSIpRanges.Finland;
            yield return SGSIpRanges.France;
            yield return SGSIpRanges.Gambia;
            yield return SGSIpRanges.Geneva;
            yield return SGSIpRanges.GenevaStandby;
            yield return SGSIpRanges.Georgia;
            yield return SGSIpRanges.Germany;
            yield return SGSIpRanges.Ghana;
            yield return SGSIpRanges.Greece;
            yield return SGSIpRanges.Guatemala;
            yield return SGSIpRanges.Guinea;
            yield return SGSIpRanges.Haiti;
            yield return SGSIpRanges.HongKong;
            yield return SGSIpRanges.Hungary;
            yield return SGSIpRanges.India;
            yield return SGSIpRanges.Indonesia;
            yield return SGSIpRanges.Iran;
            yield return SGSIpRanges.Ireland;
            yield return SGSIpRanges.Italy;
            yield return SGSIpRanges.IvoryCoast;
            yield return SGSIpRanges.Japan;
            yield return SGSIpRanges.Jordan;
            yield return SGSIpRanges.Kazakhstan;
            yield return SGSIpRanges.Kenya;
            yield return SGSIpRanges.Latvia;
            yield return SGSIpRanges.Lebanon;
            yield return SGSIpRanges.LibyanArabJamahiriya;
            yield return SGSIpRanges.Lithuania;
            yield return SGSIpRanges.Madagascar;
            yield return SGSIpRanges.Malawi;
            yield return SGSIpRanges.Malaysia;
            yield return SGSIpRanges.Mali;
            yield return SGSIpRanges.Mauritania;
            yield return SGSIpRanges.Mauritius;
            yield return SGSIpRanges.Mexico;
            yield return SGSIpRanges.Moldova;
            yield return SGSIpRanges.Mongolia;
            yield return SGSIpRanges.Morocco;
            yield return SGSIpRanges.Mozambique;
            yield return SGSIpRanges.Namibia;
            yield return SGSIpRanges.Netherlands;
            yield return SGSIpRanges.NewZealand;
            yield return SGSIpRanges.Nigeria;
            yield return SGSIpRanges.Nordics;
            yield return SGSIpRanges.Norway;
            yield return SGSIpRanges.OBSDataCenters;
            yield return SGSIpRanges.Pakistan;
            yield return SGSIpRanges.Panama;
            yield return SGSIpRanges.PapuaNewGuinea;
            yield return SGSIpRanges.Paraguay;
            yield return SGSIpRanges.Peru;
            yield return SGSIpRanges.PhilippinesMakati;
            yield return SGSIpRanges.PhilippinesManila;
            yield return SGSIpRanges.Poland;
            yield return SGSIpRanges.Portugal;
            yield return SGSIpRanges.RemoteAccess;
            yield return SGSIpRanges.RemoteAccessOBS;
            yield return SGSIpRanges.RemoteAccessSSLVPN;
            yield return SGSIpRanges.Romania;
            yield return SGSIpRanges.Russia;
            yield return SGSIpRanges.SaudiArabia;
            yield return SGSIpRanges.Scotland;
            yield return SGSIpRanges.Senegal;
            yield return SGSIpRanges.SerbiaAndMontenegro;
            yield return SGSIpRanges.Singapore;
            yield return SGSIpRanges.Slovakia;
            yield return SGSIpRanges.Slovenia;
            yield return SGSIpRanges.SouthAfrica;
            yield return SGSIpRanges.SouthKorea;
            yield return SGSIpRanges.Spain;
            yield return SGSIpRanges.SpecificUsage;
            yield return SGSIpRanges.SriLanka;
            yield return SGSIpRanges.Sudan;
            yield return SGSIpRanges.Sweden;
            yield return SGSIpRanges.Switzerland;
            yield return SGSIpRanges.Taiwan;
            yield return SGSIpRanges.Tanzania;
            yield return SGSIpRanges.Thailand;
            yield return SGSIpRanges.Togo;
            yield return SGSIpRanges.Tunisia;
            yield return SGSIpRanges.Turkey;
            yield return SGSIpRanges.Turkmenistan;
            yield return SGSIpRanges.Uganda;
            yield return SGSIpRanges.Ukraine;
            yield return SGSIpRanges.UnitedArabEmirates;
            yield return SGSIpRanges.UnitedKingdom;
            yield return SGSIpRanges.Uruguay;
            yield return SGSIpRanges.USA10;
            yield return SGSIpRanges.USA11;
            yield return SGSIpRanges.USA12;
            yield return SGSIpRanges.USA13;
            yield return SGSIpRanges.USA14;
            yield return SGSIpRanges.USA15;
            yield return SGSIpRanges.USA16;
            yield return SGSIpRanges.USA2;
            yield return SGSIpRanges.USA3;
            yield return SGSIpRanges.USA4;
            yield return SGSIpRanges.USA5;
            yield return SGSIpRanges.USA6;
            yield return SGSIpRanges.USA7;
            yield return SGSIpRanges.USA8;
            yield return SGSIpRanges.USA9;
            yield return SGSIpRanges.Uzbekistan;
            yield return SGSIpRanges.VancoReserved;
            yield return SGSIpRanges.Venezuela;
            yield return SGSIpRanges.Vietnam;
            yield return SGSIpRanges.Zambia;
            yield return SGSIpRanges.Zimbabwe;
            yield return SGSIpRanges.OutsideSGSBoundaries;
        }

        public static IpRange Geneva = new IpRange { Range = "10.0.0.0", Name = "Geneva" };
        public static IpRange GenevaStandby = new IpRange { Range = "10.1.0.0", Name = "Geneva - Standby" };
        public static IpRange Nordics = new IpRange { Range = "10.2.0.0", Name = "Nordics" };
        public static IpRange Spain = new IpRange { Range = "10.3.0.0", Name = "Spain" };
        public static IpRange Italy = new IpRange { Range = "10.5.0.0", Name = "Italy" };
        public static IpRange Norway = new IpRange { Range = "10.7.0.0", Name = "Norway" };
        public static IpRange Germany = new IpRange { Range = "10.8.0.0", Name = "Germany" };
        public static IpRange Finland = new IpRange { Range = "10.9.0.0", Name = "Finland" };
        public static IpRange Sweden = new IpRange { Range = "10.10.0.0", Name = "Sweden" };
        public static IpRange Belgium = new IpRange { Range = "10.11.0.0", Name = "Belgium" };
        public static IpRange Netherlands = new IpRange { Range = "10.12.0.0", Name = "Netherlands" };
        public static IpRange France = new IpRange { Range = "10.13.0.0", Name = "France" };
        public static IpRange Austria = new IpRange { Range = "10.14.0.0", Name = "Austria" };
        public static IpRange Greece = new IpRange { Range = "10.15.0.0", Name = "Greece" };
        public static IpRange UnitedKingdom = new IpRange { Range = "10.16.0.0", Name = "United Kingdom" };
        public static IpRange Portugal = new IpRange { Range = "10.17.0.0", Name = "Portugal" };
        public static IpRange Switzerland = new IpRange { Range = "10.18.0.0", Name = "Switzerland" };
        public static IpRange Scotland = new IpRange { Range = "10.19.0.0", Name = "Scotland" };
        public static IpRange CzechRepublic = new IpRange { Range = "10.20.0.0", Name = "Czech Republic" };
        public static IpRange Belarus = new IpRange { Range = "10.21.0.0", Name = "Belarus" };
        public static IpRange Georgia = new IpRange { Range = "10.22.0.0", Name = "Georgia" };
        public static IpRange Ireland = new IpRange { Range = "10.23.0.0", Name = "Ireland" };
        public static IpRange Lithuania = new IpRange { Range = "10.24.0.0", Name = "Lithuania" };
        public static IpRange Poland = new IpRange { Range = "10.25.0.0", Name = "Poland" };
        public static IpRange Slovakia = new IpRange { Range = "10.26.0.0", Name = "Slovakia" };
        public static IpRange Ukraine = new IpRange { Range = "10.27.0.0", Name = "Ukraine" };
        public static IpRange Estonia = new IpRange { Range = "10.28.0.0", Name = "Estonia" };
        public static IpRange Romania = new IpRange { Range = "10.29.0.0", Name = "Romania" };
        public static IpRange Croatia = new IpRange { Range = "10.30.0.0", Name = "Croatia" };
        public static IpRange Hungary = new IpRange { Range = "10.31.0.0", Name = "Hungary" };
        public static IpRange Latvia = new IpRange { Range = "10.32.0.0", Name = "Latvia" };
        public static IpRange Iran = new IpRange { Range = "10.33.0.0", Name = "Iran" };
        public static IpRange Algeria = new IpRange { Range = "10.34.0.0", Name = "Algeria" };
        public static IpRange Madagascar = new IpRange { Range = "10.35.0.0", Name = "Madagascar" };
        public static IpRange BosniaAndHerzegowina = new IpRange { Range = "10.36.0.0", Name = "Bosnia and Herzegowina" };
        public static IpRange SerbiaAndMontenegro = new IpRange { Range = "10.37.0.0", Name = "Serbia and Montenegro" };
        public static IpRange Turkmenistan = new IpRange { Range = "10.38.0.0", Name = "Turkmenistan" };
        public static IpRange Moldova = new IpRange { Range = "10.39.0.0", Name = "Moldova" };
        public static IpRange Turkey = new IpRange { Range = "10.40.0.0", Name = "Turkey" };
        public static IpRange Russia = new IpRange { Range = "10.41.0.0", Name = "Russia" };
        public static IpRange Uzbekistan = new IpRange { Range = "10.42.0.0", Name = "Uzbekistan" };
        public static IpRange Bulgaria = new IpRange { Range = "10.43.0.0", Name = "Bulgaria" };
        public static IpRange Azerbaijan = new IpRange { Range = "10.44.0.0", Name = "Azerbaijan" };
        public static IpRange Kazakhstan = new IpRange { Range = "10.45.0.0", Name = "Kazakhstan" };
        public static IpRange Slovenia = new IpRange { Range = "10.46.0.0", Name = "Slovenia" };
        public static IpRange CentralAfricanRepublic = new IpRange { Range = "10.47.0.0", Name = "Central African Republic" };
        public static IpRange Senegal = new IpRange { Range = "10.48.0.0", Name = "Senegal" };
        public static IpRange Cameroon = new IpRange { Range = "10.49.0.0", Name = "Cameroon" };
        public static IpRange IvoryCoast = new IpRange { Range = "10.50.0.0", Name = "Ivory Coast" };
        public static IpRange Guinea = new IpRange { Range = "10.51.0.0", Name = "Guinea" };
        public static IpRange Kenya = new IpRange { Range = "10.52.0.0", Name = "Kenya" };
        public static IpRange Malawi = new IpRange { Range = "10.53.0.0", Name = "Malawi" };
        public static IpRange Tanzania = new IpRange { Range = "10.54.0.0", Name = "Tanzania" };
        public static IpRange Uganda = new IpRange { Range = "10.55.0.0", Name = "Uganda" };
        public static IpRange Zambia = new IpRange { Range = "10.56.0.0", Name = "Zambia" };
        public static IpRange UnitedArabEmirates = new IpRange { Range = "10.57.0.0", Name = "United Arab Emirates" };
        public static IpRange BurkinaFaso = new IpRange { Range = "10.58.0.0", Name = "Burkina Faso" };
        public static IpRange Congo = new IpRange { Range = "10.59.0.0", Name = "Congo" };
        public static IpRange Egypt = new IpRange { Range = "10.60.0.0", Name = "Egypt" };
        public static IpRange Mali = new IpRange { Range = "10.61.0.0", Name = "Mali" };
        public static IpRange Mauritania = new IpRange { Range = "10.62.0.0", Name = "Mauritania" };
        public static IpRange Mozambique = new IpRange { Range = "10.63.0.0", Name = "Mozambique" };
        public static IpRange Tunisia = new IpRange { Range = "10.64.0.0", Name = "Tunisia" };
        public static IpRange Benin = new IpRange { Range = "10.65.0.0", Name = "Benin" };
        public static IpRange SouthAfrica = new IpRange { Range = "10.66.0.0", Name = "South Africa" };
        public static IpRange Nigeria = new IpRange { Range = "10.67.0.0", Name = "Nigeria" };
        public static IpRange Botswana = new IpRange { Range = "10.68.0.0", Name = "Botswana" };
        public static IpRange Angola = new IpRange { Range = "10.69.0.0", Name = "Angola" };
        public static IpRange Namibia = new IpRange { Range = "10.70.0.0", Name = "Namibia" };
        public static IpRange Jordan = new IpRange { Range = "10.71.0.0", Name = "Jordan" };
        public static IpRange SaudiArabia = new IpRange { Range = "10.72.0.0", Name = "Saudi Arabia" };
        public static IpRange Lebanon = new IpRange { Range = "10.73.0.0", Name = "Lebanon" };
        public static IpRange Morocco = new IpRange { Range = "10.74.0.0", Name = "Morocco" };
        public static IpRange Mauritius = new IpRange { Range = "10.75.0.0", Name = "Mauritius" };
        public static IpRange Ghana = new IpRange { Range = "10.76.0.0", Name = "Ghana" };
        public static IpRange Togo = new IpRange { Range = "10.77.0.0", Name = "Togo" };
        public static IpRange Gambia = new IpRange { Range = "10.78.0.0", Name = "Gambia" };
        public static IpRange Burundi = new IpRange { Range = "10.79.0.0", Name = "Burundi" };
        public static IpRange Zimbabwe = new IpRange { Range = "10.80.0.0", Name = "Zimbabwe" };
        public static IpRange Albania = new IpRange { Range = "10.81.0.0", Name = "Albania" };
        public static IpRange Sudan = new IpRange { Range = "10.83.0.0", Name = "Sudan" };
        public static IpRange LibyanArabJamahiriya = new IpRange { Range = "10.84.0.0", Name = "Libyan Arab Jamahiriya" };
        public static IpRange RemoteAccessOBS = new IpRange { Range = "10.88.0.0", Name = "Remote access - OBS" };
        public static IpRange RemoteAccess = new IpRange { Range = "10.89.0.0", Name = "Remote access" };
        public static IpRange OBSDataCenters = new IpRange { Range = "10.90.0.0", Name = "OBS Data Centers" };
        public static IpRange RemoteAccessSSLVPN = new IpRange { Range = "10.91.0.0", Name = "Remote access SSL-VPN IP pool" };
        public static IpRange USA2 = new IpRange { Range = "10.100.0.0", Name = "USA 2" };
        public static IpRange USA3 = new IpRange { Range = "10.101.0.0", Name = "USA 3" };
        public static IpRange USA4 = new IpRange { Range = "10.102.0.0", Name = "USA 4" };
        public static IpRange USA5 = new IpRange { Range = "10.103.0.0", Name = "USA 5" };
        public static IpRange Canada = new IpRange { Range = "10.104.0.0", Name = "Canada" };
        public static IpRange USA6 = new IpRange { Range = "10.105.0.0", Name = "USA 6" };
        public static IpRange USA7 = new IpRange { Range = "10.106.0.0", Name = "USA 7" };
        public static IpRange USA8 = new IpRange { Range = "10.107.0.0", Name = "USA 8" };
        public static IpRange USA9 = new IpRange { Range = "10.108.0.0", Name = "USA 9" };
        public static IpRange USA10 = new IpRange { Range = "10.109.0.0", Name = "USA 10" };
        public static IpRange USA11 = new IpRange { Range = "10.110.0.0", Name = "USA 11" };
        public static IpRange USA12 = new IpRange { Range = "10.111.0.0", Name = "USA 12" };
        public static IpRange USA13 = new IpRange { Range = "10.112.0.0", Name = "USA 13" };
        public static IpRange USA14 = new IpRange { Range = "10.113.0.0", Name = "USA 14" };
        public static IpRange USA15 = new IpRange { Range = "10.114.0.0", Name = "USA 15" };
        public static IpRange USA16 = new IpRange { Range = "10.115.0.0", Name = "USA 16" };
        public static IpRange Argentina = new IpRange { Range = "10.173.0.0", Name = "Argentina" };
        public static IpRange Brazil = new IpRange { Range = "10.174.0.0", Name = "Brazil" };
        public static IpRange Chile = new IpRange { Range = "10.175.0.0", Name = "Chile" };
        public static IpRange Colombia = new IpRange { Range = "10.176.0.0", Name = "Colombia" };
        public static IpRange Peru = new IpRange { Range = "10.177.0.0", Name = "Peru" };
        public static IpRange Venezuela = new IpRange { Range = "10.178.0.0", Name = "Venezuela" };
        public static IpRange Ecuador = new IpRange { Range = "10.179.0.0", Name = "Ecuador" };
        public static IpRange Bolivia = new IpRange { Range = "10.180.0.0", Name = "Bolivia" };
        public static IpRange Guatemala = new IpRange { Range = "10.181.0.0", Name = "Guatemala" };
        public static IpRange Mexico = new IpRange { Range = "10.182.0.0", Name = "Mexico" };
        public static IpRange Panama = new IpRange { Range = "10.183.0.0", Name = "Panama" };
        public static IpRange Uruguay = new IpRange { Range = "10.184.0.0", Name = "Uruguay" };
        public static IpRange Paraguay = new IpRange { Range = "10.185.0.0", Name = "Paraguay" };
        public static IpRange Haiti = new IpRange { Range = "10.186.0.0", Name = "Haiti" };
        public static IpRange ChinaReserved = new IpRange { Range = "10.196.0.0", Name = "China - reserved" };
        public static IpRange Singapore = new IpRange { Range = "10.200.0.0", Name = "Singapore" };
        public static IpRange Japan = new IpRange { Range = "10.202.0.0", Name = "Japan" };
        public static IpRange Australia = new IpRange { Range = "10.203.0.0", Name = "Australia" };
        public static IpRange HongKong = new IpRange { Range = "10.204.0.0", Name = "Hong Kong" };
        public static IpRange China = new IpRange { Range = "10.205.0.0", Name = "China" };
        public static IpRange PhilippinesManila = new IpRange { Range = "10.206.0.0", Name = "Philippines Manila" };
        public static IpRange Malaysia = new IpRange { Range = "10.207.0.0", Name = "Malaysia" };
        public static IpRange NewZealand = new IpRange { Range = "10.208.0.0", Name = "New Zealand" };
        public static IpRange Indonesia = new IpRange { Range = "10.209.0.0", Name = "Indonesia" };
        public static IpRange India = new IpRange { Range = "10.210.0.0", Name = "India" };
        public static IpRange SouthKorea = new IpRange { Range = "10.211.0.0", Name = "South Korea" };
        public static IpRange SriLanka = new IpRange { Range = "10.212.0.0", Name = "Sri Lanka" };
        public static IpRange Taiwan = new IpRange { Range = "10.213.0.0", Name = "Taiwan" };
        public static IpRange PapuaNewGuinea = new IpRange { Range = "10.214.0.0", Name = "Papua New Guinea" };
        public static IpRange Pakistan = new IpRange { Range = "10.215.0.0", Name = "Pakistan" };
        public static IpRange Cambodia = new IpRange { Range = "10.216.0.0", Name = "Cambodia" };
        public static IpRange Bangladesh = new IpRange { Range = "10.217.0.0", Name = "Bangladesh" };
        public static IpRange Mongolia = new IpRange { Range = "10.218.0.0", Name = "Mongolia" };
        public static IpRange Thailand = new IpRange { Range = "10.219.0.0", Name = "Thailand" };
        public static IpRange PhilippinesMakati = new IpRange { Range = "10.220.0.0", Name = "Philippines Makati" };
        public static IpRange Vietnam = new IpRange { Range = "10.230.0.0", Name = "Vietnam" };
        public static IpRange VancoReserved = new IpRange { Range = "10.254.0.0", Name = "Vanco reserved" };
        public static IpRange SpecificUsage = new IpRange { Range = "10.255.0.0", Name = "Specific usage" };
        public static IpRange OutsideSGSBoundaries = new IpRange { Range = "10.255.255.255", Name = "Outside SGS boundaries" };

    }
}
