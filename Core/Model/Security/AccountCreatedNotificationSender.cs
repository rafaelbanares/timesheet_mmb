﻿
namespace Core.Model.Security
{
    public abstract class AccountCreatedNotificationSender
    {
        public abstract void Notify(ActiveDirectoryUser newUser, string newUserPassword);
    }
}
