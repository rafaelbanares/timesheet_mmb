﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Runtime.Serialization;

namespace Core.Model.Security
{
    [Serializable]
    public class NotAuthorizedException : FrameworkException
    {
        public NotAuthorizedException() { }

        public NotAuthorizedException(string message) : base(message) { }

        public NotAuthorizedException(string message, Exception inner) : base(message, inner) { }
    }
}
