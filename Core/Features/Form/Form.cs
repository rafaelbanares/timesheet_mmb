﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features;
using Core.Model;

namespace Core.Features.Form
{
    public class Form
    {
        public Guid Id { get; internal set; }
        public string Name { get; set; }
        public int Version { get; internal set; }
        public string SourceCode { get; set; }
        public string CreatedBy { get; internal set; }
        public DateTime CreatedOn { get; internal set; }
        
        public static Form Create(string Name, string SourceCode)
        {
            return new Form { Name = Name, SourceCode = SourceCode };
        }

        public Form Save()
        {
            return Form.Save(this);
        }

        public static Form Save(Form Form)
        {
            Form formToSave = null;
            if (!Form.Id.IsDefault())
            {
                formToSave = Form.Create(Form.Name, Form.SourceCode);
            }
            else formToSave = Form;

            Validate(formToSave);

            return Finder.FormRepository.Insert(formToSave);
        }

        private static void Validate(Form Form)
        {
            if (Form.Id.IsDefault()) Form.Id = Comb.NewGuid();
            Form.CreatedOn = DateTime.UtcNow;
            Form.CreatedBy = Operator.Current;

            if (!Form.Name.IsFilled()) throw new ApplicationException("Must provide a name to your form");
            if (!Form.SourceCode.IsFilled()) throw new ApplicationException("Must provide some source code for your form");
        }

        public static IEnumerable<Form> All(bool GetNewVersionsOnly)
        {            
            return Finder.FormRepository.All(GetNewVersionsOnly);            
        }

        public static Form Get(Guid Id)
        {
            return Finder.FormRepository.Get(Id);
        }

        public static Form Get(string Name)
        {
            return Finder.FormRepository.Get(Name, null);
        }

        public static string GetSourceCode(Guid Id)
        {
            return Finder.FormRepository.Get(Id).SourceCode;
        }

        public static string GetSourceCode(string Name)
        {
            return Finder.FormRepository.Get(Name, null).SourceCode;
        }

        public static string GetSourceCode(string Name, int Version)
        {
            return Finder.FormRepository.Get(Name, Version).SourceCode;
        }

        public static void Delete(Guid Id)
        {
            Finder.FormRepository.Delete(Id);
        }
    }
}
