﻿using System;
using System.Collections.Generic;

namespace Core.Features.Form
{
    public interface IFormRepository
    {        
        IEnumerable<Form> All(bool GetNewVersionsOnly);
        Form Get(Guid Id);        
        Form Get(String Name, int? Version);        
        Form Insert(Form form);
        void Delete(Guid Id);        
    }
}
