﻿using System;
using System.Collections.Generic;

namespace Core.Features.Scheduling
{
    public interface IScheduleRepository
    {
        IEnumerable<Schedule> All();
        void Delete(Schedule schedule);
        void Upsert(Schedule schedule);
        void Upsert(ScheduleLog scheduleLog);
        void InsertOneTime(Schedule schedule);
        Schedule Get(Guid scheduleId);
        DateTime? LastRun(Guid scheduleId);
        IEnumerable<ScheduleLog> GetLogsFor(Guid scheduleId);
    }
}
