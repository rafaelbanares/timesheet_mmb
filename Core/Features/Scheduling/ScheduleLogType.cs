﻿
namespace Core.Features.Scheduling
{
    public enum ScheduleLogType
    {
        Success,
        Information,
        Warning,
        Error
    }
}
