﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using Core.Features;
using Core.Helpers.Tasks;

namespace Core.Features.Scheduling
{

    /// <summary>
    /// Populate repository with all classes implementing BusinessGivenAuthorization
    /// </summary>
    public class EnsureRepositorySynchronization : IStartupTask
    {
        public void OnStart()
        {
            if (Finder.SchedulerRepository != null)
            {
                typeof(PredefinedSchedule)
                    .GetMatchingTypes()
                    .Where(x => !x.IsAbstract && !x.IsInterface)
                    .ForEach(x => ((PredefinedSchedule)x.GetOne()).Save());
            }
        }
    }
}
