﻿using System;
using Core.Features;

namespace Core.Features.Scheduling
{
    public partial class ScheduleLog
    {
        public static void Save(ScheduleLog item)
        {
            if (item.Id.IsDefault())
            {
                item.Id = Comb.NewGuid();
            }
            
            Finder.SchedulerRepository.Upsert(item);
        }
    }
}
