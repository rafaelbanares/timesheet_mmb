﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Features;

namespace Core.Features.Scheduling
{
    public partial class Schedule
    {
        public static IEnumerable<Type> AvailableTasks()
        {
            return typeof(IScheduledTask).GetMatchingTypes().Where(x => !x.IsAbstract && !x.IsInterface);
        }

        public static IEnumerable<Schedule> ActiveSchedules()
        {
            return All().Where(x => x.IsActive);
        }

        public static IEnumerable<Schedule> All()
        {
            return Finder.SchedulerRepository.All();
        }

        public static Guid Create(string Name, string ScheduledTaskType, string ScheduledTaskAssembly, string Cron, string Arguments)
        {
            Schedule result = new Schedule { Id = Comb.NewGuid(), Name = Name, ScheduledTaskType = ScheduledTaskType, ScheduledTaskAssembly = ScheduledTaskAssembly, Cron = Cron, Arguments = Arguments, IsActive = true };
            result.Save();
            return result.Id;
        }

        public static Schedule Get(Guid taskId)
        {
            return Finder.SchedulerRepository.Get(taskId);
        }

        public static void Run(Guid taskId)
        {
            Get(taskId).Run();
        }

        public static void Delete(Schedule task)
        {
            Finder.SchedulerRepository.Delete(task);
        }

        public static void Activate(Guid taskId)
        {
            var task = Finder.SchedulerRepository.Get(taskId);
            if (task.IsNotNull())
            {
                task.IsActive = true;
                task.Save();
            }
        }

        public static void Deactivate(Guid taskId)
        {
            var task = Finder.SchedulerRepository.Get(taskId);
            if (task.IsNotNull())
            {
                task.IsActive = false;
                task.Save();
            }
        }

        public static void Log(Guid TaskId, ScheduleLogType MessageType, string message)
        {
            Console.WriteLine(MessageType);
            Console.WriteLine(message);

            var item = new ScheduleLog
            {
                Data = message,
                LoggedOn = DateTime.UtcNow,
                LogType = MessageType,
                ScheduledTaskId = TaskId
            };

            try
            {
                ScheduleLog.Save(item);
            }
            finally { }
        }
    }
}
