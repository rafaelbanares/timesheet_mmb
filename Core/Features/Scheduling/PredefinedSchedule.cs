﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.WinService.Scheduling.Cron;
using Core.Features;

namespace Core.Features.Scheduling
{
    public class PredefinedSchedule : Schedule
    {
        private PredefinedSchedule() { }

        public PredefinedSchedule(Type t, Guid id, string name, CronExpression cron)
        {
            this.ScheduledTaskAssembly = t.Assembly.GetName().Name;
            this.ScheduledTaskType = t.FullName;
            this.Id = id;
            this.Name = name;
            this.Cron = cron.ToString();
            this.IsActive = true;
            this.Arguments = "";
        }

        public PredefinedSchedule(Type t, Guid id, string name, CronExpression cron, string arguments)
            : this(t, id, name, cron)
        {
            this.Arguments = arguments;
        }

        public override void Save()
        {
            this.Validate();

            Finder.SchedulerRepository.InsertOneTime(this);
        }
    }
}
