﻿
namespace Core.Features.Scheduling
{
    public interface IScheduledTask
    {
        void Execute(Schedule jobSchedule);
    }
}
