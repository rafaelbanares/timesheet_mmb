﻿using System;
using Core.Features;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Core.Features.Scheduling
{
    public partial class ScheduleLog
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime LoggedOn { get; set; }
        public ScheduleLogType LogType { get; set; }
        public string Data { get; set; }
        public Guid ScheduledTaskId { get; set; }
    }
}
