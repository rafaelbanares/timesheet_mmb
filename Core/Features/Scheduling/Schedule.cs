﻿using System;
using System.Linq;
using Core.Features;
using Core.WinService.Scheduling.Cron;
using System.Collections.Generic;

namespace Core.Features.Scheduling
{
    public partial class Schedule
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }

        public string Name { get; set; }
        public string ScheduledTaskType { get; set; }
        public string ScheduledTaskAssembly { get; set; }

        public string Cron { get; set; }
        public string Arguments { get; set; }

        public Schedule()
        {

        }

        public DateTime LastRun
        {
            get
            {
                var date = Finder.SchedulerRepository.LastRun(this.Id);

                return date.HasValue ? date.Value : DateTime.MinValue;
            }
        }

        internal void Validate()
        {
            if (this.Id.IsDefault()) 
                this.Id = Comb.NewGuid();

            this.Cron.CanNotBeNullOrEmpty();
            this.ScheduledTaskType.CanNotBeNullOrEmpty();
            this.ScheduledTaskAssembly.CanNotBeNullOrEmpty();

            this.Name.CanNotBeNullOrEmpty();

            if (Type.GetType(this.ScheduledTaskType + ", " + this.ScheduledTaskAssembly).IsNull())
                throw new ApplicationException("Task is invalid : HandlerType is unknown");
        }

        public virtual void Save()
        {
            this.Validate();

            Finder.SchedulerRepository.Upsert(this);
        }

        public bool IsDifferentThan(Schedule job)
        {
            return (this.IsActive != job.IsActive) || (this.ScheduledTaskType != job.ScheduledTaskType) || (this.ScheduledTaskAssembly != job.ScheduledTaskAssembly) || (this.Arguments != job.Arguments) || (this.Cron != job.Cron);
        }

        public void Run()
        {
            try
            {
                this.Log(ScheduleLogType.Information, "Triggered");
                string location = AppDomain.CurrentDomain.BaseDirectory;
                Type t = AssemblyHelper.GetType(location, ScheduledTaskType + ", " + ScheduledTaskAssembly);

                if (t.IsNull())
                {
                    this.Log(ScheduleLogType.Error, string.Format( "Unable to load task : {0} {1} from : {2}", ScheduledTaskType, ScheduledTaskAssembly, location));
                }
                else
                {
                    var item = Activator.CreateInstance(t, null, null);
                    IScheduledTask task = item as IScheduledTask;
                    if (task != null)
                    {
                        task.Execute(this);
                    }
                }
            }
            catch (Exception x)
            {
                x.Log();

                this.Log(ScheduleLogType.Error, x.Message);
            }
        }

        public void Log(Exception exception)
        {
            Log(this.Id, ScheduleLogType.Error, exception.Format());
        }

        public void Log(ScheduleLogType type, string message)
        {
            Log(this.Id, type, message);
        }

        public IEnumerable<ScheduleLog> GetLogs()
        {
            return Finder.SchedulerRepository.GetLogsFor(this.Id);
        }
    }    
}
