﻿using Core.Model;
using Core.Helpers.Logging;

namespace System
{
    public static class CoreFeaturesExceptionExtensions
    {
        public static void Log(this Exception ex)
        {
            var toLog = ex.InnerMost();
            Logger.Error("Exception : " + toLog.Message, toLog.Format());
            toLog.LogToTrace();
        }

        public static void Log(this Exception ex, string category)
        {
            var toLog = ex.InnerMost();
            Logger.Error(toLog.Message, toLog.Format(), category);
            toLog.LogToTrace();
        }

        public static void Log(this Exception ex, string category, string message)
        {
            var toLog = ex.InnerMost();
            Logger.Error(message, toLog.Message + Environment.NewLine + toLog.Format(), category);
            toLog.LogToTrace();
        }

        static Exception InnerMost(this Exception ex)
        {
            if (ex.InnerException != null) return ex.InnerException.InnerMost();
            return ex;
        }
    }
}