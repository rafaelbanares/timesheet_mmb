﻿using System;
using System.Linq;
using Core.Features.Multilingual;
using Core.Features.Scheduling;
using Core.Features.Security;
using Core.Features.DocumentsManagement;
using Core.Features.Scripting;
using Core.Features.Emailing;
using Core.Features.ExtendedProperty;
using Core.Features.Form;
using Core.Features.DatabaseProvider;
using System.Collections.Concurrent;
using Core.Features.Workflow;

namespace Core.Features
{
    public static partial class Finder
    {        
        static ConcurrentDictionary<Type, object> items = new ConcurrentDictionary<Type, object>();
        
        static T Get<T>()
        {
            object result;
            var t = typeof(T);

            result = items.GetOrAdd(t, (key) =>
                    {
                        var type = t
                        .GetMatchingTypes(AppDomain.CurrentDomain.BaseDirectory, u => !u.IsAbstract && !u.IsInterface)
                        .FirstOrDefault();
                        if (type != null)
                        {
                            return type.GetOne();
                        }
                        return null;
                    }

            );
            return (T)result;
        }

        public static IFrameworkDatabaseConnections FrameworkDatabaseConnections
        {
            get
            {
                return Get<IFrameworkDatabaseConnections>();
            }
        }

        public static IUserLanguageProvider LanguageProvider
        {
            get
            {
                return Get<IUserLanguageProvider>();
            }
        }

        public static IExtendedPropertyRepository ExtendedPropertyRepository
        {
            get
            {
                return Get<IExtendedPropertyRepository>();
            }
        }

        public static IFormRepository FormRepository
        {
            get
            {
                return Get<IFormRepository>();
            }
        }

        public static IEmailRepository EmailRepository
        {
            get
            {
                return Get<IEmailRepository>();
            }
        }

        public static IScheduleRepository SchedulerRepository
        {
            get
            {
                return Get<IScheduleRepository>();
            }
        }

        public static IScriptRepository ScriptRepository
        {
            get
            {
                return Get<IScriptRepository>();
            }
        }

        public static ILanguageRepository LanguageRepository
        {
            get
            {
                return Get<ILanguageRepository>();
            }
        }

        public static ITranslationRepository TranslationRepository
        {
            get
            {
                return Get<ITranslationRepository>();
            }
        }

       

        public static ISecurityManagementRepository SecurityManagementRepository
        {
            get
            {
                return Get<ISecurityManagementRepository>();
            }
        }

        public static IDocumentRepository DocumentRepository
        {
            get
            {
                return Get<IDocumentRepository>();
            }
        }

        public static IWorkflowRepository WorkflowRepository
        {
            get
            {
                return Get<IWorkflowRepository>();
            }
        }
    }
}
