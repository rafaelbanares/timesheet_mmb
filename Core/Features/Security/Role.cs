﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Security
{
    [Serializable]
    public partial class Role : IEquatable<Role>
    {
        public Guid Id { get; set; }
        public string ModuleId { get; set; }
        public bool IsBusinessFunction { get; set; }
        public int Sequence { get; set; }

        public Role Save()
        {
            return Role.Save(this);
        }

        public IEnumerable<BusinessGivenAuthorization> Operations
        {
            get
            {
                return Core.Features.Finder.SecurityManagementRepository.OperationsForRole(this);
            }
        }

        public bool CanPerform(BusinessGivenAuthorization op)
        {
            return this.Operations.Contains(op);
        }

        public void AddOperation(BusinessGivenAuthorization operation)
        {
            Core.Features.Finder.SecurityManagementRepository.AddOperationToRole(operation, this);
        }

        public void RemoveOperation(BusinessGivenAuthorization operation)
        {
            Core.Features.Finder.SecurityManagementRepository.RemoveOperationFromRole(operation, this);
        }

        public bool Equals(Role other)
        {
            if (other == null)
                return false;

            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
