﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;

namespace Core.Features.Security
{
    public partial class BusinessGivenAuthorization
    {
        public static IEnumerable<BusinessGivenAuthorization> All()
        {
            return Core.Features.Finder.SecurityManagementRepository.AllOperations();
        }

        public static BusinessGivenAuthorization Save(BusinessGivenAuthorization auth)
        {
            if (auth.Id.IsDefault())
            {
                throw new FrameworkException(String.Format("BusinessGivenAuthorization must have a fixed Id defined in code. review code for {0}", auth.GetType().FullName));
            }

            return Core.Features.Finder.SecurityManagementRepository.Save(auth);
        }
    }
}
