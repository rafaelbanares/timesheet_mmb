﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using Core.Features;
using Core.Helpers.Tasks;

namespace Core.Features.Security
{

    /// <summary>
    /// Populate repository with all classes implementing BusinessGivenAuthorization
    /// </summary>
    public class EnsureRepositorySynchronization : IStartupTask
    {
        public void OnStart()
        {
            var lstAuthorizationTypes = typeof(BusinessGivenAuthorization).GetMatchingTypes(x => x.IsClass && !x.IsAbstract);
            lstAuthorizationTypes.ForEach(x => BusinessGivenAuthorization.Save(x.GetOne<BusinessGivenAuthorization>()));
        }
    }
}
