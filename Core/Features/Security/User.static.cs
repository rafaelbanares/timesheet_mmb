﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using Core.Model.Security;
using System.Diagnostics;
using System.DirectoryServices.ActiveDirectory;

namespace Core.Features.Security
{
    public partial class User
    {
        /// <summary>
        /// Returns current User
        /// </summary>
        public static User Current
        {
            get
            {
                return GetCurrent<User>();
            }
        }

        /// <summary>
        /// Returns current User, using a derived class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetCurrent<T>() where T : User, new()
        {
            return User.Get<T>(Operator.Current);
        }

        /// <summary>
        /// Returns user by Unique identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User Get(Guid id)
        {
            return Get<User>(id);
        }

        /// <summary>
        /// Returns user by Unique identifier, using a derived class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T Get<T>(Guid id) where T : User, new()
        {
            return (Finder.SecurityManagementRepository != null)
            ? Finder.SecurityManagementRepository.GetUser<T>(id)
            : null;
        }

        /// <summary>
        /// Returns user by domain\login
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User Get(string domainLogin)
        {
            return Get<User>(domainLogin);
        }

        /// <summary>
        /// Returns user by domain\login, using a derived class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T Get<T>(string domainLogin) where T : User, new()
        {
            var vals = domainLogin.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            if (vals.Length != 2) throw new ArgumentException("domainLogin must be of the form Domain\\Login");

            return Get<T>(vals[0], vals[1]);
        }

        /// <summary>
        /// Returns user by domain and login
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User Get(string domain, string login)
        {
            return Get<User>(domain, login);
        }

        /// <summary>
        /// Returns user by domain and login, using a derived class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T Get<T>(string domain, string login) where T : User, new()
        {
            ActiveDirectoryUser adUser;
            T dbUser;
            try
            {
                adUser = new ActiveDirectoryUser(); //SGS.GetUserByLogin(domain, login);
                dbUser = User.Get<T>(adUser.Guid) ?? new T();
                return Update<T>(adUser, dbUser);
            }
            catch (ActiveDirectoryObjectNotFoundException)
            {
                return GetByLogin<T>(domain + "\\" + login);
            }
            catch(Exception x)
            {
                x.LogToTrace();
                var message = "User " + domain + "\\" + login + " found neither in Active directory or database";
                throw new FrameworkException(message);
            }
        }

        public static IEnumerable<T> GetByEmail<T>(string email) where T : User, new()
        {
            return Core.Features.Finder.SecurityManagementRepository.GetUsersByEmail<T>(email);
        }

        public static T GetByLogin<T>(string login) where T : User, new()
        {
            return Core.Features.Finder.SecurityManagementRepository.GetUser<T>(login);
        }

        public static IEnumerable<T> All<T>() where T : User, new()
        {
            return Core.Features.Finder.SecurityManagementRepository.AllUsers<T>();
        }

        public static T Update<T>(ActiveDirectoryUser from, T to) where T : User, new()
        {
            to.Address = from.Address;
            to.City = from.City;
            to.Company = from.Company;
            to.Country = from.Country;
            to.CountryCode = from.CountryCode;
            to.Department = from.Department;
            to.Description = from.Description;
            to.DisplayName = from.DisplayName;
            to.Domain = from.Domain;
            to.Email = from.Email;
            to.Fax = from.Fax;
            to.FirstName = from.FirstName;
            to.FullLogin = from.FullLogin;
            to.Guid = from.Guid;
            to.JobTitle = from.JobTitle;
            to.LastName = from.LastName;
            to.Login = from.Login;
            to.MobilePhone = from.MobilePhone;
            to.Path = from.Path;
            to.Phone = from.Phone;
            to.UserName = from.UserName;
            to.State = from.State;
            to.ZipCode = from.ZipCode;
            to.UserName = from.UserName;
            return to;
        }
    }
}
