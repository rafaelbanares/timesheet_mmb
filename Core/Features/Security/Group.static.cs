﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Security
{
    public partial class Group
    {
        public static Group Save(Group group)
        {
            if (group.Id.IsDefault())
            {
                group.Id = Comb.NewGuid();
            }

            return Core.Features.Finder.SecurityManagementRepository.Save(group);
        }
    }
}
