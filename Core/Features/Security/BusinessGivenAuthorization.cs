﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;

namespace Core.Features.Security
{
    [Serializable]
    public partial class BusinessGivenAuthorization : Core.Model.Security.Authorization, IEquatable<BusinessGivenAuthorization>
    {
        public virtual Guid Id { get; set; }
        public string Type { get; set; }
        public string Assembly { get; set; }
        public string ModuleId { get; set; }

        public override sealed bool IsAuthorized()
        {
           return base.IsAuthorized() || User.Current.CanPerform(this);
        }

        public BusinessGivenAuthorization()
        {
            var t = this.GetType();
            this.Type = t.FullName;
            this.Assembly = t.Assembly.GetName().Name;
        }


        public bool Equals(BusinessGivenAuthorization other)
        {
            if (other == null)
                return false;

            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        
        public BusinessGivenAuthorization Save()
        {
            return BusinessGivenAuthorization.Save(this);
        }
    }
}
