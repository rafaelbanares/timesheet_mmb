﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System;

namespace Core.Features.Security
{
    public interface ISecurityManagementRepository
    {
        T GetUser<T>(Guid Id) where T : User, new();
        T GetUser<T>(string UserName) where T : User, new();

        T Save<T>(T user) where T : User, new();
        void Delete(User user);
        
        Group Save(Group group);
        void Delete(Group group);

        Role Save(Role role);
        void Delete(Role role);

        BusinessGivenAuthorization Save(BusinessGivenAuthorization operation);

        void AddRoleToGroup(Role role, Group group);
        void AddRoleToUser(Role role, User user);
        void AddUserToGroup(User user, Group group);
        void AddOperationToRole(BusinessGivenAuthorization operation, Role role);

        void RemoveRoleFromGroup(Role role, Group group);
        void RemoveRoleFromUser(Role role, User user);
        void RemoveUserFromGroup(User user, Group group);
        void RemoveOperationFromRole(BusinessGivenAuthorization operation, Role role);

        IEnumerable<BusinessGivenAuthorization> OperationsForUser(User user);
        IEnumerable<BusinessGivenAuthorization> OperationsForRole(Role role);
        IEnumerable<BusinessGivenAuthorization> OperationsForGroup(Group group);

        IEnumerable<Group> GroupsForUser(User user);
        IEnumerable<Role> RolesForUser(User user);
        IEnumerable<Role> RolesForGroup(Group group);

        IEnumerable<T> UsersForGroup<T>(Group group) where T : User, new();

        IEnumerable<Group> AllGroups();
        IEnumerable<Role> AllRoles();
        IEnumerable<T> AllUsers<T>() where T : User, new();
        IEnumerable<T> GetUsersByEmail<T>(string email) where T : User, new();
        IEnumerable<BusinessGivenAuthorization> AllOperations();
    }
}
