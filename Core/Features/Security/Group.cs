﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Security
{
    [Serializable]
    public partial class Group : IEquatable<Group>
    {
        public Guid Id { get; set; }
        public string ModuleId { get; set; }
        public bool IsSystem { get; set; }
        public bool IsDynamic { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DynamicMembersSQL { get; set; }
        public string Owner { get; set; }

        public Group Save()
        {
            return Group.Save(this);
        }
        
        public void AddRole(Role role)
        {
            Core.Features.Finder.SecurityManagementRepository.AddRoleToGroup(role, this);
        }

        public void RemoveRole(Role role)
        {
            Core.Features.Finder.SecurityManagementRepository.RemoveRoleFromGroup(role, this);
        }

        public IEnumerable<BusinessGivenAuthorization> Operations
        {
            get
            {
                return Core.Features.Finder.SecurityManagementRepository.OperationsForGroup(this);
            }
        }

        public IEnumerable<Role> Roles
        {
            get
            {
                return Core.Features.Finder.SecurityManagementRepository.RolesForGroup(this);
            }
        }

        public IEnumerable<User> Users
        {
            get
            {
                return Core.Features.Finder.SecurityManagementRepository.UsersForGroup<User>(this);
            }
        }

        public bool CanPerform(BusinessGivenAuthorization op)
        {
            return this.Operations.Contains(op);
        }

        public bool Equals(Group other)
        {
            if (other == null)
                return false;

            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
