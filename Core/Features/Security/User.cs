﻿using System.Linq;
using System.Collections.Generic;
using System;
using Core.Model;

namespace Core.Features.Security
{
    public partial class User : ActiveDirectoryUser, IEquatable<User>
    {
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public string LanguageCode { get; set; }

        public string TimeZone { get; set; }

        public User Save()
        {
            var result = this;
            if(Finder.SecurityManagementRepository != null)
                result = Finder.SecurityManagementRepository.Save(this);
            return result;
        }

        public void Delete()
        {
            if (Finder.SecurityManagementRepository != null) Finder.SecurityManagementRepository.Delete(this);
        }

        public IEnumerable<Role> Roles
        {
            get
            {
                return (Finder.SecurityManagementRepository != null)
                     ? Core.Features.Finder.SecurityManagementRepository.RolesForUser(this)
                     : new List<Role>();
            }
        }

        public IEnumerable<Group> Groups
        {
            get
            {
                return (Finder.SecurityManagementRepository != null)
                     ? Core.Features.Finder.SecurityManagementRepository.GroupsForUser(this)
                     : new List<Group>();
            }
        }

        public bool CanPerform(BusinessGivenAuthorization operation)
        {
            var res = ContextHelper.Get<IEnumerable<BusinessGivenAuthorization>>("OPERATIONS" + UserName, () => Core.Features.Finder.SecurityManagementRepository.OperationsForUser(this));
            return res.Contains(operation);
        }

        public bool HasRole(Role role)
        {
            return Roles.Contains(role);
        }

        public User AddToGroup(Group group)
        {
            if (Finder.SecurityManagementRepository != null) Core.Features.Finder.SecurityManagementRepository.AddUserToGroup(this, group);

            return this;
        }

        public User RemoveFromGroup(Group group)
        {
            if (Finder.SecurityManagementRepository != null) Core.Features.Finder.SecurityManagementRepository.RemoveUserFromGroup(this, group);

            return this;
        }

        public User AddToRole(Role role)
        {
            if (Finder.SecurityManagementRepository != null) Core.Features.Finder.SecurityManagementRepository.AddRoleToUser(role, this);

            return this;
        }

        public User RemoveFromRole(Role role)
        {
            if (Finder.SecurityManagementRepository != null) Core.Features.Finder.SecurityManagementRepository.RemoveRoleFromUser(role, this);

            return this;
        }

        public bool Equals(User other)
        {
            if (other == null || this.Guid.IsDefault() || other.Guid.IsDefault())
                return false;

            return this.Guid == other.Guid;
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
    }
}