﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Security
{
    public partial class Role
    {
        public static Role Save(Role role)
        {
            if (role.Id.IsDefault())
            {
                role.Id = Comb.NewGuid();
            }

            return Core.Features.Finder.SecurityManagementRepository.Save(role);
        }

        public static void Delete(Role role)
        {
            ValidateDelete(role);

            Core.Features.Finder.SecurityManagementRepository.Delete(role);
        }

        internal static void ValidateDelete(Role role)
        {
            if (Core.Features.Finder.SecurityManagementRepository.OperationsForRole(role).Any())
            {
                throw new DeleteException("Role", "[[FWK.Role has linked operations]]");
            }
        }
    }
}
