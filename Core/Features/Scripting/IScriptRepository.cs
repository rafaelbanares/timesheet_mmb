﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Scripting
{
    public interface IScriptRepository
    {
        IEnumerable<Script> All();
        Script Get(string Name);
        void Save(Script item);
        void Delete(string Name);
    }
}
