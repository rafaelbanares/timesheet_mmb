﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using System.Transactions;

namespace Core.Features.Scripting
{
    public class RunSavedScriptTask : IScheduledTask
    {
        public void Execute(Schedule job)
        {
            if (job.Arguments.IsNotNull())
            {
                var name = job.Arguments;
                var script = Script.Get(name);

                if (script != null)
                {
                    var cnx = Finder.FrameworkDatabaseConnections.Main;

                    try
                    {
                        var affected = cnx.ExecuteScript(script.Body);

                        string message = string.Format("Script {0}: ran {1} affected rows.", script.Name, affected);

                        job.Log(ScheduleLogType.Success, message);
                    }
                    catch (Exception x)
                    {
                        job.Log(ScheduleLogType.Error, String.Format("Script {0}: {1}", script.Name, x.Format()));
                    }
                }
                else
                {
                    job.Log(ScheduleLogType.Error, "Invalid Scheduled Task Argument: script not found.");
                }
            }
            else
            {
                job.Log(ScheduleLogType.Error, "Null Scheduled Task Argument: must a provide a valid script name");
            }
        }
    }
}
