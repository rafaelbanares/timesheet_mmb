﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Core.Features.DocumentsManagement;
using Core.Features;

namespace Core.Features.Scripting
{
    public partial class Script
    {
        public Script()
        {
            Name = "New";
        }

        public string Name { get; set; }
        public string Body { get; set; }
        public ScriptType Type { get; set; }
        
        public void Save()
        {
            Script.Save(this);
        }

        public bool IsNew { get { return Name == "New"; } }
    }

    public enum ScriptType
    { 
        Sql,
        Csharp
    }
}
