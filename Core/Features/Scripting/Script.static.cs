﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.DocumentsManagement;
using System.Transactions;
using Core.Features;

namespace Core.Features.Scripting
{
    public partial class Script
    {
        public static Script Save(Script item)
        {
            Validate(item);
            Finder.ScriptRepository.Save(item);
            return item;
        }

        public static IEnumerable<Script> All()
        {
            return Finder.ScriptRepository.All();
        }

        public static Script Get(string Name)
        {
            return Finder.ScriptRepository.Get(Name);
        }

        public static void Delete(string Name)
        {
            Finder.ScriptRepository.Delete(Name);
        }
        
        static void Validate(Script item)
        {
            if(item.IsNew || item.Name.IsNullOrEmpty()) throw new ApplicationException("[[FWK.A script must ne named to be persisted]]");
            if (item.Body.IsNullOrEmpty()) throw new ApplicationException("[[FWK.Script body must be filled]]");
        }


    }
}
