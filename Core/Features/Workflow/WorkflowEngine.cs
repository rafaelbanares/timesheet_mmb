﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Workflow
{
    public static class WorkflowEngine
    {
        public static void TriggerEvent<T>(Guid businessObjectIdentifier, Event eventName)
        {
            var instance = GetInstanceByBusinessObjectId<T>(businessObjectIdentifier);
            instance.TriggerEvent(eventName);
        }
        
        public static Instance CreateInstance<T>(string FriendlyName, Guid businessObjectIdentifier)
        {
            return Instance.CreateInstance<T>(FriendlyName, businessObjectIdentifier, null, null);
        }

        public static Instance CreateInstance<T>(string FriendlyName, Guid businessObjectIdentifier, Guid? parentId, Guid? predecessorInstanceId)
        {
            return Instance.CreateInstance<T>(FriendlyName, businessObjectIdentifier, parentId, predecessorInstanceId);
        }

        public static Instance GetInstance(Guid id)
        {
            return Persistence.GetInstance(id);
        }

        public static Instance GetInstanceByBusinessObjectId<T>(Guid businessObjectIdentifier)
        {
            var instance = Persistence.GetInstanceByBusinessObjectId(businessObjectIdentifier, typeof(T).FullName);
            if (instance == null)
            {
                instance = Instance.CreateInstance<T>(typeof(T).Name, businessObjectIdentifier, null, null);
            }
            return instance;
        }

        public static List<Instance> OpenedToUser(string assignedTo)
        {
            return Persistence.GetUserInstances(assignedTo).ToList();
        }

        public static List<Instance> OpenedToAnyUser()
        {
            return Persistence.GetAllUserInstances().ToList();
        }

    }
}
