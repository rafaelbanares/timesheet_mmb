﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Helpers.Data;

namespace Core.Features.Workflow
{
    public interface IWorkflowRepository
    {
        ITable<Instance> Instances { get; }
    }
}
