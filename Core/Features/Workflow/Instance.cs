﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Core.Features.Workflow
{
    [Serializable]
    public class Instance
    {
        [Key]
        public Guid Id { get; set; }
        public Guid BusinessObjectId { get; set; }
        public string ProcessType { get; set; }
        public string CurrentStageName { get; set; }
        public DateTime? StartedOn { get; set; }
        public DateTime? StageEnteredOn { get; set; }
        public DateTime? StageReEnteredOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public int Priority { get; set; }

        public Guid? ParentId { get; set; }
        public Guid? PredecessorId { get; set; }
        public string AssignedTo { get; set; }
        public string FriendlyName { get; set; }
        public bool IsHumanStage { get; set; }

        public Stage GetCurrentStage()
        {
            return Descriptor.GetStages(TypeHelper.GetType(ProcessType)).Where(x => x.Name == CurrentStageName).FirstOrDefault();
        }

        public Instance CreateChild<T>(string FriendlyName, Guid businessObjectIdentifier)
        {
            return CreateChild<T>(FriendlyName, businessObjectIdentifier, null);
        }

        public Instance CreateChild<T>(string FriendlyName, Guid businessObjectIdentifier, Guid? predecessorInstanceId)
        {
            return CreateInstance<T>(FriendlyName, businessObjectIdentifier, this.Id, predecessorInstanceId);
        }

        public void IncreasePriority(int increment)
        {
            Priority += increment;
        }

        public void DecreasePriority(int decrement)
        {
            if (Priority > decrement) Priority -= decrement;
            else Priority = 0;
        }

        public void Save()
        {
            Persistence.Save(this);
        }

        internal static Instance CreateInstance<T>(string FriendlyName, Guid businessObjectIdentifier, Guid? parentId, Guid? predecessorInstanceId)
        {
            var res = new Instance();
            res.Id = Comb.NewGuid();
            res.FriendlyName = FriendlyName;
            res.BusinessObjectId = businessObjectIdentifier;
            res.PredecessorId = predecessorInstanceId;
            res.ParentId = parentId;
            res.ProcessType = typeof(T).FullName;
            res.StartedOn = DateTime.UtcNow;
            res.Save();
            res.Initialize();

            return res;
        }

        internal Instance GetPredecessor()
        {
            return PredecessorId.HasValue ? Persistence.GetInstance(PredecessorId.Value) : null;
        }

        internal List<Instance> GetSuccessors()
        {
            return Persistence.GetSuccessors(Id).ToList();
        }

        internal Instance GetParent()
        {
            return ParentId.HasValue ? Persistence.GetInstance(ParentId.Value) : null;
        }

        internal List<Instance> GetChildren()
        {
            return Persistence.GetChildren(Id).ToList();
        }

        void OnPredecessorCompleted()
        {
            this.GoToStage(Descriptor.GetInitialStage(TypeHelper.GetType(ProcessType)));
        }

        private void OnChildCompleted()
        {
            CheckAutoTransitions();
        }

        private void Initialize()
        {
            if (!PredecessorId.HasValue) this.GoToStage(Descriptor.GetInitialStage(TypeHelper.GetType(ProcessType)));
        }

        private IEnumerable<T> GetActualTransitions<T>() where T : Transition
        {
            var type = TypeHelper.GetType(ProcessType);
            return Descriptor
                    .GetTransitions(type)
                    .Where(x => x.GetType().Match(typeof(T)) && x.FromStage.Name == CurrentStageName)
                    .Select(x => (T)x);
        }

        internal void TriggerEvent(Event notification)
        {
            var transitionToApply =
                GetActualTransitions<TriggeredTransition>()
                .Where(x => x.Trigger == notification)
                .FirstOrDefault();

            if (transitionToApply != null)
            {
                transitionToApply.ApplyOn(this);
            }
        }

        internal void GoToStage(Stage stage)
        {
            var oldStage = CurrentStageName;
            CurrentStageName = stage.Name;

            IsHumanStage = stage.IsHuman;
            if (oldStage != CurrentStageName)
            {
                StageEnteredOn = DateTime.UtcNow;
                StageReEnteredOn = null;
                stage.Entered(this);
            }
            else
            {
                StageReEnteredOn = DateTime.UtcNow;
                stage.ReEntered(this);
            }


            var type = TypeHelper.GetType(ProcessType);

            var isFinal = Descriptor.GetFinalStages(type).Contains(stage);
            if (isFinal) CompletedOn = DateTime.UtcNow;
            Save();

            if (isFinal)
            {
                foreach (var successor in GetSuccessors()) successor.OnPredecessorCompleted();
                if (GetParent() != null) GetParent().OnChildCompleted();
            }

            CheckAutoTransitions();
        }


        private void CheckAutoTransitions()
        {
            var type = TypeHelper.GetType(ProcessType);
            var transitionToApply =
                GetActualTransitions<AutomaticTransition>()
                .Where(x => x.IsApplicable(this))
                .FirstOrDefault();

            if (transitionToApply != null)
            {
                transitionToApply.ApplyOn(this);
            }

        }

    }
}
