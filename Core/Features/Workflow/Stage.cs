﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Core.Features.Workflow
{
    public static class StageExtensions
    {
        public static T AsInitial<T>(this T item) where T : Stage
        {
            item.IsInitial = true;
            return item;
        }

        public static T AsFinal<T>(this T item) where T : Stage
        {
            item.IsFinal = true;
            return item;
        }

        public static T WhenEnter<T>(this T item, Expression<Action<Stage, Instance>> action) where T : Stage
        {
            item.OnEnter = action;
            return item;
        }

        public static T WhenReEnter<T>(this T item, Expression<Action<Stage, Instance>> action) where T : Stage
        {
            item.OnReEnter = action;
            return item;
        }
    }

    public abstract class Stage : IEquatable<Stage>
    {
        protected Stage() { }
        public Expression<Action<Stage, Instance>> OnEnter { get; set; }
        public Expression<Action<Stage, Instance>> OnReEnter { get; set; }
        public string Name { get; internal set; }
        public bool IsInitial { get; internal set; }
        public bool IsFinal { get; internal set; }
        protected internal abstract bool IsHuman { get; }

        public override string ToString()
        {
            return Name;
        }


        public bool Equals(Stage other)
        {
            return other.Name == this.Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        protected internal virtual void Entered(Instance instance)
        {
            if (OnEnter != null)
            {
                var deleg = OnEnter.Compile();
                deleg(this, instance);
            }
        }

        protected internal virtual void ReEntered(Instance instance)
        {
            if (OnReEnter != null)
            {
                var deleg = OnReEnter.Compile();
                deleg(this, instance);
            }
        }

        public override bool Equals(object obj)
        {
            Stage item = obj as Stage;
            if (item != null)
            {
                return Equals(item);
            }
            else
            {
                return false;
            }
        }
        public static bool operator ==(Stage item1, Stage item2)
        {
            if (object.ReferenceEquals(item1, item2)) return true;
            if (object.ReferenceEquals(item1, null)) return false;
            if (object.ReferenceEquals(item2, null)) return false;

            return item1.Equals(item2);
        }

        public static bool operator !=(Stage emp1, Stage emp2)
        {
            if (object.ReferenceEquals(emp1, emp2)) return false;
            if (object.ReferenceEquals(emp1, null)) return true;
            if (object.ReferenceEquals(emp2, null)) return true;

            return !emp1.Equals(emp2);
        }
    }

    public class SystemStage : Stage
    {
        protected SystemStage() { }

        public static SystemStage Create(string Name)
        {
            return new SystemStage { Name = Name };
        }

        public Stage AsFinal()
        {
            IsFinal = true;
            return this;
        }

        protected internal override bool IsHuman
        {
            get { return false; }
        }
    }

    public class UserStage : Stage
    {
        protected UserStage()
        {
            Actions = new List<UIAction>();
        }

        protected Expression<Func<Instance, string>> AssignedTo { get; set; }
        public List<UIAction> Actions { get; set; }

        protected internal override void Entered(Instance instance)
        {
            var deleg = AssignedTo.Compile();
            instance.AssignedTo = deleg(instance);
            base.Entered(instance);
        }

        protected internal override void ReEntered(Instance instance)
        {
            var deleg = AssignedTo.Compile();
            instance.AssignedTo = deleg(instance);
            base.ReEntered(instance);
        }

        public UserStage AddAction(UIAction action)
        {
            Actions.Add(action);
            return this;
        }

        public static UserStage Create(string Name, Expression<Func<Instance, string>> AssignedToDelegate)
        {
            return new UserStage { Name = Name, AssignedTo = AssignedToDelegate };
        }

        protected internal override bool IsHuman
        {
            get { return true; }
        }
    }

    public class GroupStage : UserStage
    {
        private GroupStage() { }

        public static GroupStage Create(string Name, Expression<Func<Instance, string>> AssignedToDelegate)
        {
            return new GroupStage { Name = Name, AssignedTo = AssignedToDelegate };
        }
    }
}
