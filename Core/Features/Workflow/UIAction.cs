﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Core.Features.Workflow
{
    public abstract class UIAction
    {
        public string DisplayName { get; set; }
        public string ActionName { get; set; }
        public abstract void Handle(Guid instance);
    }

    public class RunCodeAction : UIAction
    {
        public Expression<Action<Guid>> OnClick { get; set; }

        public static UIAction Create(string displayName, string actionName, Expression<Action<Guid>> onClick)
        {
            return new RunCodeAction { DisplayName = displayName, ActionName = actionName, OnClick = onClick };
        }

        public override void Handle(Guid id)
        {
            var deleg = OnClick.Compile();
            deleg(id);
        }
    }

    public class RaiseEventAction : UIAction
    {
        /// <remarks></remarks>
        public Event Event { get; set; }

        public static UIAction Create(string displayName, string actionName, Event Event)
        {
            return new RaiseEventAction { DisplayName = displayName, ActionName = actionName, Event = Event };
        }

        public override void Handle(Guid id)
        {
            var instance = WorkflowEngine.GetInstance(id);
            instance.TriggerEvent(Event);
        }
    }
}
