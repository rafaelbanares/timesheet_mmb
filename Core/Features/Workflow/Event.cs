﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Workflow
{
    public class Event : IEquatable<Event>
    {
        private Event() { }
        public string Name { get; internal set; }

        public static Event Create(string Name)
        {
            return new Event { Name = Name };
        }

        public override string ToString()
        {
            return Name;
        }

        public bool Equals(Event other)
        {
            return other.Name == this.Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Event item = obj as Event;
            if (item != null)
            {
                return Equals(item);
            }
            else
            {
                return false;
            }
        }
        public static bool operator ==(Event item1, Event item2)
        {
            if (object.ReferenceEquals(item1, item2)) return true;
            if (object.ReferenceEquals(item1, null)) return false;
            if (object.ReferenceEquals(item2, null)) return false;

            return item1.Equals(item2);
        }

        public static bool operator !=(Event emp1, Event emp2)
        {
            if (object.ReferenceEquals(emp1, emp2)) return false;
            if (object.ReferenceEquals(emp1, null)) return true;
            if (object.ReferenceEquals(emp2, null)) return true;

            return !emp1.Equals(emp2);
        }
    }
}
