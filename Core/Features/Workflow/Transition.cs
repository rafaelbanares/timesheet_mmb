﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Core.Features.Workflow
{
    public static class TransitionExtensions
    {
        public static T WhenExecute<T>(this T item, Expression<Action<Transition, Instance>> action) where T : Transition
        {
            item.OnExecute = action;
            return item;
        }

        //public static T CanExecute<T>(this T item, Expression<Func<Transition, Instance, bool>> action) where T : Transition
        //{
        //    item.CanExecute = action;
        //    return item;
        //}
    }

    public abstract class Transition
    {
        public string Name { get; protected set; }
        public Stage FromStage { get; protected set; }
        public Stage ToStage { get; protected set; }
        public abstract bool IsAutomatic { get; }
        public Expression<Action<Transition, Instance>> OnExecute { get; set; }
       // public Expression<Func<Transition, Instance, bool>> CanExecute { get; set; }

        public void ApplyOn(Instance instance)
        {
            //if (CanExecute != null)
            //{
            //    var canExecDeleg = CanExecute.Compile();
            //    if (!canExecDeleg(this, instance)) return;
            //}
            if (OnExecute != null)
            {
                var onExecDeleg = OnExecute.Compile();
                onExecDeleg(this, instance);
            }
            instance.GoToStage(ToStage);
        }
    }

    public abstract class AutomaticTransition : Transition
    {
        public override bool IsAutomatic
        {
            get { return true; }
        }
        public abstract bool IsApplicable(Instance instance);
    }

    public class AlwaysTransition : AutomaticTransition
    {
        public static AlwaysTransition Create(string name, Expression<Action<Transition, Instance>> onExecute, Stage fromStage, Stage toStage)
        {
            return new AlwaysTransition { FromStage = fromStage, ToStage = toStage, OnExecute = onExecute, Name = name };
        }

        public override bool IsApplicable(Instance instance)
        {
            return true;
        }
    }

    public class TriggeredTransition : Transition
    {
        private TriggeredTransition() { }
        public Event Trigger { get; internal set; }

        public static Transition Create(Event trigger, Stage from, Stage to)
        {
            return new TriggeredTransition { Name = trigger.Name, Trigger = trigger, FromStage = from, ToStage = to };
        }

        public override bool IsAutomatic
        {
            get { return false; }
        }
    }

    public enum TimeoutType
    {
        ProcessStarted,
        StageEntered,
        StageReentered
    }

    public class TimedTransition : Transition
    {
        public TimeSpan Timeout { get; private set; }
        public TimeoutType RelativeTo { get; private set; }
        private TimedTransition() { }

        public static Transition Create(string Name, TimeSpan timeout, TimeoutType relativeTo, Stage from, Stage to)
        {
            return new TimedTransition { Name = Name, Timeout = timeout, RelativeTo = relativeTo, FromStage = from, ToStage = to };
        }

        public override bool IsAutomatic
        {
            get { return false; }
        }
    }

    public class ConditionalTransition : AutomaticTransition
    {
        public Expression<Func<Instance, bool>> Condition { get; private set; }

        private ConditionalTransition() { }

        public static Transition Create(string Name, Expression<Func<Instance, bool>> condition, Stage from, Stage to)
        {
            return new ConditionalTransition { Name = Name, Condition = condition, FromStage = from, ToStage = to };
        }

        public override bool IsApplicable(Instance instance)
        {
            var deleg = Condition.Compile();
            return deleg(instance);
        }
    }

    public class AllChildrenCompleteTransition : AutomaticTransition
    {
        private AllChildrenCompleteTransition() { }

        public static Transition Create(Stage from, Stage to)
        {
            return new AllChildrenCompleteTransition { Name = "All children are completed", FromStage = from, ToStage = to };
        }

        public override bool IsApplicable(Instance instance)
        {
            bool allCompleted = instance.GetChildren().Count > 0;
            foreach (var child in instance.GetChildren())
            {
                if (!child.CompletedOn.HasValue)
                {
                    allCompleted = false;
                    break;
                }
            }
            return allCompleted;
        }
    }

    public class OneChildCompleteTransition : AutomaticTransition
    {
        private OneChildCompleteTransition() { }

        public static Transition Create(Stage from, Stage to)
        {
            return new OneChildCompleteTransition { Name = "One child is complete", FromStage = from, ToStage = to };
        }

        public override bool IsApplicable(Instance instance)
        {
            bool oneCompleted = false;
            foreach (var child in instance.GetChildren())
            {
                if (child.CompletedOn.HasValue)
                {
                    oneCompleted = true;
                    break;
                }
            }
            return oneCompleted;
        }
    }
}
