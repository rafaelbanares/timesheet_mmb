﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Workflow
{
    class Descriptor
    {
        public static List<Stage> GetStages(Type t)
        {
            return t.GetFields().Where(x => x.FieldType.Match(typeof(Stage))).Select(x => (Stage)x.GetValue(null)).ToList();
        }

        public static List<Transition> GetTransitions(Type t)
        {
            return t.GetFields().Where(x => x.FieldType.Match(typeof(Transition))).Select(x => (Transition)x.GetValue(null)).ToList();
        }

        public static Stage GetInitialStage(Type t)
        {
            return GetStages(t).Where(x => x.IsInitial).First();
        }

        public static List<Stage> GetFinalStages(Type t)
        {
            return GetStages(t).Where(x => x.IsFinal).ToList();
        }
    }
}
