﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Workflow
{
    public static class Persistence
    {
        public static Instance GetInstanceByBusinessObjectId(Guid BusinessObjectId, string processTypeName)
        {
            return Finder.WorkflowRepository.Instances.Query("BusinessObjectId = @0 and ProcessType = @1", BusinessObjectId, processTypeName).FirstOrDefault();
        }

        public static Instance GetInstance(Guid Id)
        {
            return Finder.WorkflowRepository.Instances.Query("Id = @0", Id).FirstOrDefault();
        }

        public static List<Instance> GetInstances(List<Guid> Ids)
        {
            if (Ids.IsFilled())
            {
                var where = Ids.Aggregate("", (x, y) => x + "'" + y.ToString() + "', ");
                if (where.Length > 0) where = where.Substring(0, where.Length - 2);
                return Finder.WorkflowRepository.Instances.Query("Id in (@0)", where).ToList();
            } 
            return null;
        }

        public static IEnumerable<Instance> GetUserInstances(string AssignedTo)
        {
            return Finder.WorkflowRepository.Instances.Query("AssignedTo = @0 and IsHumanStage = 1", AssignedTo);
        }

        public static IEnumerable<Instance> GetAllUserInstances()
        {
            return Finder.WorkflowRepository.Instances.Query("IsHumanStage = 1");
        }
        
        public static IEnumerable<Instance> GetSuccessors(Guid Id)
        {
            return Finder.WorkflowRepository.Instances.Query("PredecessorId = @0", Id);
        }

        public static IEnumerable<Instance> GetChildren(Guid Id)
        {
            return Finder.WorkflowRepository.Instances.Query("ParentId = @0", Id);
        }

        public static void Save(Instance instance)
        {
            Finder.WorkflowRepository.Instances.Save(instance);
        }
    }
}
