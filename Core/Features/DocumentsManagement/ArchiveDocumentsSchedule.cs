﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using Core.WinService.Scheduling.Cron;

namespace Core.Features.DocumentsManagement
{
    public class ArchiveDocumentsSchedule : PredefinedSchedule
    {
        public static readonly Guid ScheduleId = new Guid("348D995F-325C-4C89-A0AB-D3EFFC98E0A8");

        public ArchiveDocumentsSchedule()
            : base(typeof(ArchiveDocumentsTask), ScheduleId, "Archive documents", CronBuilder.CreateHourlyTrigger(0, 58, 2), "")
        {}
    }
}
