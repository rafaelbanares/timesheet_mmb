﻿using System;
using System.Linq;
using Core.Features.Security;

namespace Core.Features.DocumentsManagement
{
    public partial class DocumentArchive
    {
        public Guid Id { get; set; }
        public byte[] BinaryData { get; set; }
        public DateTime ArchiveDate { get; set; }
    }

    public partial class Document 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string Extension { get; set; }
        public string MimeType { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool? ArchiveStatus { get; set; }


        public Document()
        {
            Id = Comb.NewGuid();
            CreatedOn = DateTime.UtcNow;
            CreatedBy = Core.Model.Operator.Current;
        }

        private byte[] binary;
        public byte[] BinaryData
        {
            get
            {
                return binary;
            }
            set
            {
                if (value != null)
                {
                    Size = value.Length;
                    binary = value;
                }
            }
        }

        public void Validate()
        {
            if (!Name.IsFilled()) throw new ApplicationException("Documents must have a name");
            if (Name.Contains("\\")) Name = Name.Split("\\").Last();
        }

        public Document Save()
        {
            return Document.Save(this);
        }

        public Document Archive()
        {
            return Document.Archive(this);
        }
    }
}
