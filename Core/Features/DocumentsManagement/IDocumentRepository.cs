﻿using System;
using System.Collections.Generic;

namespace Core.Features.DocumentsManagement
{
    public interface IDocumentRepository
    {
        /// <summary>
        /// Gets document by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Document GetDocument(Guid Id);

        T GetDocument<T>(Guid Id) where T : Document, new();
        IEnumerable<T> GetDocuments<T>(IEnumerable<Guid> Ids) where T : Document, new();

        /// <summary>
        /// Gets frameworkDatabase for a document
        /// </summary>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        byte[] GetBinary(Guid documentId);

        /// <summary>
        /// Saves a document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        /// <remarks>Generates an id if document doesn't have one</remarks>
        Document Save(Document document);

        DocumentArchive Save(DocumentArchive document);

        IEnumerable<Document> DocumentsToBeArchived();
    }
}
