﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Runtime.Serialization;

namespace Core.Features.DocumentsManagement
{
    [Serializable]
    public class DocumentSaveException : SaveException
    {
        public DocumentSaveException() : base("Document") { }

        public DocumentSaveException(string message) : base("Document", message) { }

        public DocumentSaveException(string message, Exception inner) : base("Document", message, inner) { }
    }
}
