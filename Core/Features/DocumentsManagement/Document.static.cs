﻿using System;
using System.Linq;
using System.Collections.Generic;
using Core.Model;
using Core.Features;
using System.IO;

namespace Core.Features.DocumentsManagement
{
    public partial class Document
    {
        public static Document Create(string name, byte[] content)
        {
            return Create<Document>(name, content);
        }

        public static T Create<T>(string name, byte[] content) where T : Document, new()
        {
            T d = new T();

            d.Name = name;
            d.BinaryData = content;
            d.Size = content.Length;
            d.Extension = Path.GetExtension(name);
            d.MimeType = FileHelper.GetMimeType(d.Extension);

            return d;
        }

        public static Document Create(string path)
        {
            return Create<Document>(path);
        }

        public static T Create<T>(string path) where T : Document, new()
        {
            return Create<T>(Path.GetFileName(path), FileHelper.ReadBinary(path));
        }

        public static T Get<T>(Guid Id) where T : Document, new()
        {
            return Finder.DocumentRepository.GetDocument<T>(Id);
        }

        public static List<T> GetList<T>(List<Guid> Ids) where T : Document, new()
        {
            return (List<T>)Finder.DocumentRepository.GetDocuments<T>(Ids);
        }

        public static Document Get(Guid Id)
        {
            return Get<Document>(Id);
        }

        public static List<Document> GetList(List<Guid> Ids)
        {
            return GetList<Document>(Ids);
        }

        public static byte[] GetBinary(Guid documentId)
        {
            return Finder.DocumentRepository.GetBinary(documentId);
        }

        public static Document Archive(Document document)
        {
            document.ArchiveStatus = false;

            return Save(document);
        }

        public static Document Save(Document document)
        {
            Validate(document);

            if (document.Id.IsDefault())
            {
                document.Id = Comb.NewGuid();
            }

            return Finder.DocumentRepository.Save(document);
        }

        internal static void Validate(Document document)
        {
            if (document.ArchiveStatus.IsNotNull())
            {
                new DocumentSaveException(Constants.ARCHIVED_DOCUMENT_IS_READONLY);
            }

            document.ThrowIfNull(new DocumentSaveException(Constants.MUST_NOT_BE_NULL));


            if (document.CreatedOn.IsDefault())
            {
                document.CreatedOn = DateTime.UtcNow;
            }

            document.CreatedBy = Operator.Current;

            document.Name.ThrowIfNull(new DocumentSaveException(Constants.NAME_MUST_NOT_BE_NULL));
            if (document.Name.Contains("\\")) document.Name = document.Name.Split("\\").Last();
        }

        public static class Constants
        {
            public const string ARCHIVED_DOCUMENT_IS_READONLY = "[[FWK.Archived document is read-only]]";
            public const string MUST_NOT_BE_NULL = "[[FWK.Document must not be null]]";
            public const string NAME_MUST_NOT_BE_NULL = "[[FWK.Document name must not be null]]";
        }
    }
}
