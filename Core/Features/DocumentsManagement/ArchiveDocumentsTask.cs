﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using Core.Features;
using Core.Helpers.Logging;
using Core.Features.Security;
using Core.Model;
using System.Transactions;

namespace Core.Features.DocumentsManagement
{
    public class ArchiveDocumentsTask : IScheduledTask
    {
        public void Execute(Schedule job)
        {
            using (var t = new TransactionScope())
            {
                var archives = new List<DocumentArchive>();

                var docToBeArchived = Finder.DocumentRepository.DocumentsToBeArchived();

                foreach (var doc in docToBeArchived)
                {
                    archives.Add(new DocumentArchive
                    {
                        Id = doc.Id,
                        BinaryData = doc.BinaryData.Compress(),
                        ArchiveDate = DateTime.UtcNow
                    });

                    doc.ArchiveStatus = true;
                    doc.BinaryData = new byte[0];

                    doc.Save();
                }

                archives.ForEach(a => Finder.DocumentRepository.Save(a));

                t.Complete();
            }
        }
    }
}
