﻿using System;
using Core.Features;

namespace Core.Features.DocumentsManagement
{
    public class ForDownload
    {
        public Document Document { get; set; }
        public byte[] Binary { get; set; }

        public static ForDownload GetById(Guid Id)
        {
            // for ease of usage
            // this method will look in documents repo
            // if not found it will then look into the versions repo
            if (Finder.DocumentRepository.IsNotNull())
            {
                ForDownload result = new ForDownload();

                Document doc = Finder.DocumentRepository.GetDocument(Id);
                if (doc.IsNotNull())
                {
                    result.Document = doc;
                    result.Binary = Finder.DocumentRepository.GetBinary(doc.Id);
                    return result;
                }
            }

            return null;
        }
    }
}
