﻿using System;
using System.Runtime.Serialization;
using Core.Model;

namespace Core.Features.DocumentsManagement
{
    [Serializable]
    public class DocumentNotFoundException : FrameworkException
    {
        public DocumentNotFoundException() { }
        public DocumentNotFoundException(string message) : base(message) { }
        public DocumentNotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}