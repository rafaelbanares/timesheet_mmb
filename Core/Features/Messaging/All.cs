﻿using System;
using System.Collections.Generic;

namespace Core.Features.Messaging
{
    public interface IMessagingService
    {
        IResponse Handle(IRequest request);
    }

    public interface IClientApplication
    {
        IResponse Notify(IRequest request);
    }

    public interface IRequest
    {
        Guid? CorrelationId { get; set; }
        string ApplicationId { get; set; }
        string Requestor { get; set; } // AD username
        DateTime SentOn { get; set; }
        string Subject { get; set; } // will usually be the Message Type. Describe somehow the type of the XmlContent for routing purposes
        string XmlContent { get; set; } // could easily be handled by : http://www.amazedsaint.com/2010/02/introducing-elasticobject-for-net-40.html
    }
    
    public interface IResponse
    {
        Guid RequestId { get; set; }
        IList<string> Messages { get; set; }
        Outcome Outcome { get; set; }
        DateTime TreatedOn { get; set; }
        string XmlContent { get; set; }
    }

    public interface ILogRequest : IRequest
    {
        Guid RequestId { get; set; }
    }

    public interface ILogResponse : IResponse
    {
        IList<ILogEntry> Log { get; set; }
    }

    public interface ILogEntry
    {
        DateTime On { get; set; }
        string Username { get; set; }
        string Event { get; set; } // Meaningful to end user
        string AdditionalData { get; set; } // Might be meaningfull to client system (eg.: for ITechnicalRequest)
    }


    public enum Outcome
    { 
        Success,
        Error
    }
}
