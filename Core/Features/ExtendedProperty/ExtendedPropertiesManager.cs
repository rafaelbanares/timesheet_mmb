﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features;

namespace Core.Features.ExtendedProperty
{
    static class ExtendedPropertiesManager
    {
        static List<ExtendedProperty> AllProperties = new List<ExtendedProperty>();
        static object locker = new object();
        static bool isInitialized = false;

        static ExtendedPropertiesManager()
        {
            lock (locker)
            {
                if (isInitialized) return;
                AllProperties.AddRange(Finder.ExtendedPropertyRepository.All());
                isInitialized = true;
            }
        }


        public static T Get<T>(string className, string id, string key, T defaultValue)
        {
            var property = AllProperties.Where(u => u.Class == className && u.InstanceId == id && u.Key == key).FirstOrDefault();
            if (property.IsNull())
            {
                Set(className, id, key, defaultValue);
                return defaultValue;
            }
            return property.Value.FromJSON<T>();
        }

        public static T Get<T>(string className, string id, string key)
        {
            return Get(className, id, key, default(T));
        }

        public static void Drop(string className, string id, string key)
        {
            var property = AllProperties.Where(u => u.Class == className && u.InstanceId == id && u.Key == key).FirstOrDefault();
            if (property.IsNotNull())
            {
                AllProperties.Remove(property);
                Finder.ExtendedPropertyRepository.Delete(className, id, key);
            }
        }

        public static void Set<T>(string className, string id, string key, T value)
        {
            var property = AllProperties.Where(u => u.Class == className && u.InstanceId == id && u.Key == key).FirstOrDefault();
            if (property.IsNull())
            {
                property = new ExtendedProperty { Class = className, InstanceId = id, Key = key, Value = value.ToJSON() };
                AllProperties.Add(property);
            }
            else property.Value = value.ToJSON();
            Finder.ExtendedPropertyRepository.Upsert(property);

        }
    }
}
