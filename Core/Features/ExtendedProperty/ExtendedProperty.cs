﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Core.Features.ExtendedProperty
{
    public class ExtendedProperty 
    {
        [Key]
        public string Class { get; set; }
        [Key]
        public string InstanceId { get; set; }
        [Key]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
