﻿using System.Collections.Generic;
using Core.Helpers.Data;

namespace Core.Features.ExtendedProperty
{
    public interface IExtendedPropertyRepository
    {
        //void Upsert(string className, string instanceId, string key, string value);
        void Upsert(Core.Features.ExtendedProperty.ExtendedProperty property);
        void Delete(string className, string instanceId, string key);
        IEnumerable<ExtendedProperty> All();
    }
}
