﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace Core.Features.ExtendedProperty
{
    public static class ExtendedPropertiesExtension
    {
        public static T GetExtendedProperty<T>(this object item, string key, T defaultValue) 
        {
            var itemType = item.GetType();
            var className = itemType.FullName;
            var keyValues = itemType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(KeyAttribute), true).IsFilled())
                .Select(y => item.GetValue(y.Name).ToString())
                .ToString("|");

            return ExtendedPropertiesManager.Get(className, keyValues, key, defaultValue);
        }

        public static T GetExtendedProperty<T>(this object item, string key) 
        {
            var itemType = item.GetType();
            var className = itemType.FullName;
            var keyValues = itemType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(KeyAttribute), true).IsFilled())
                .Select(y => item.GetValue(y.Name).ToString())
                .ToString("|");

            return ExtendedPropertiesManager.Get<T>(className, keyValues, key);
        }

        public static void DropExtendedProperty(this object item, string key)
        {
            var itemType = item.GetType();
            var className = itemType.FullName;
            var keyValues = itemType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(KeyAttribute), true).IsFilled())
                .Select(y => item.GetValue(y.Name).ToString())
                .ToString("|");

            ExtendedPropertiesManager.Drop(className, keyValues, key);
        }

        public static void SetExtendedProperty<T>(this object item, string key, T value)
        {
            var itemType = item.GetType();
            var className = itemType.FullName;
            var keyValues = itemType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => x.GetCustomAttributes(typeof(KeyAttribute), true).IsFilled())
                .Select(y => item.GetValue(y.Name).ToString())
                .ToString("|");

            ExtendedPropertiesManager.Set(className, keyValues, key, value);
        }

        public static void SetExtendedProperty<T>(this Type item, string key, T value)
        {
            ExtendedPropertiesManager.Set(item.FullName, null, key, value);
        }

        public static void DropExtendedProperty(this Type item, string key)
        {
            ExtendedPropertiesManager.Drop(item.FullName, null, key);
        }

        public static T GetExtendedProperty<T>(this Type item, string key, T defaultValue)
        {
            return ExtendedPropertiesManager.Get<T>(item.FullName, null, key, defaultValue);
        }

        public static T GetExtendedProperty<T>(this Type item, string key)
        {
            return ExtendedPropertiesManager.Get<T>(item.FullName, null, key);
        }
    }
}
