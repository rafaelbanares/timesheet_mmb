﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using Core.Exchange;

namespace Core.Features.Exchange
{
    public abstract class MonitorExchangeInboxBase : IScheduledTask
    {
        protected abstract string UserName { get; }
        protected abstract string Password { get; }

        protected abstract bool OnNewMessage(ExchangeEmail email);
        protected abstract void OnError(Exception x);
        protected abstract void OnError(Exception x, ExchangeEmail email);

        public void Execute(Schedule job)
        {
            try
            {
                var conn = new MailServerConnection(UserName, Password);

                var emailsToBeDeleted = new List<string>();

                conn.GetUnreadEmails().ForEach(email =>
                {
                    try
                    {
                        if (OnNewMessage(email))
                        {
                            emailsToBeDeleted.Add(email.Id);
                        }
                    }
                    catch (Exception x)
                    {
                        emailsToBeDeleted.Add(email.Id);
                        OnError(x, email);
                    }
                });

                conn.Delete(emailsToBeDeleted);
            }
            catch (Exception x)
            {
                OnError(x);
            }
        }
    }
}
