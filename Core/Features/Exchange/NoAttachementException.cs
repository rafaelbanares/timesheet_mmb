﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;

namespace Core.Features.Exchange
{
    public class NoAttachementException : FrameworkException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoAttachementException"/> class.
        /// </summary>
        public NoAttachementException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoAttachementException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public NoAttachementException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoAttachementException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public NoAttachementException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
