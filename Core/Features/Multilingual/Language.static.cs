﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace Core.Features.Multilingual
{
    public partial class Language
    {
        static Language()
        {
            MultiLingualCache.CacheInvalidated += new CacheInvalidatedDelegate(MultiLingualCache_CacheInvalidated);
        }

        static void MultiLingualCache_CacheInvalidated()
        {
            defaultLanguage = null;
            enabled = null;
        }

        public static Language Get(string Code)
        { 
            return MultiLingualCache.AllLanguages().FirstOrDefault(x=> x.Code == Code);
        }

        static Language defaultLanguage = null;
        public static Language ApplicationDefault
        {
            get
            {
                if (defaultLanguage == null)
                { 
                    defaultLanguage = MultiLingualCache.AllLanguages().FirstOrDefault(x => x.IsDefault);
                }
                return defaultLanguage;
            }
        }

        static IEnumerable<Language> enabled = null;
        public static IEnumerable<Language> Enabled()
        {
            if (enabled == null)
            {
                enabled = MultiLingualCache.AllLanguages().Where(x => x.IsEnabled);
            }
            return enabled;
        }

        internal void Validate(Language item)
        {
            item.Code.CanNotBeNull();
            if (IsDefault && IsEnabled)
                throw new ApplicationException("[[FWK.A language can not be the Default one and be Disabled at same time]]");
        }
    }
}
