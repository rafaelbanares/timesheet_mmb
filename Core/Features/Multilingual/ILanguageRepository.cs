﻿using System.Collections.Generic;

namespace Core.Features.Multilingual
{
    public interface ILanguageRepository
    {
        IEnumerable<Language> AllLanguages();
    }
}
