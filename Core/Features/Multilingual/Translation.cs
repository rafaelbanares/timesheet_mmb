﻿
namespace Core.Features.Multilingual
{
    public partial class Translation
    {
        public string Key { get; set; }
        public string LanguageCode { get; set; }
        public string Value { get; set; }
    }
}
