﻿using System.Collections.Generic;

namespace Core.Features.Multilingual
{
    public interface ITranslationRepository
    {
        IEnumerable<Translation> AllTranslations();
        void Insert(Translation translation);
    }
}
