﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class StringExtensions2
    {
        public static string Translate(this string item)
        {
            return Core.Features.Multilingual.Translation.Translate(item);
        }
    }
}
