﻿using System;

namespace Core.Features.Multilingual
{
    public partial class Language
    {
        public string Code { get; set; }
        public bool IsDefault { get; set; }
        public bool IsEnabled { get; set; }
        public string Description { get; set; }

       
    }
}
