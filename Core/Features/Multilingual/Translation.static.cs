﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Core.Model;
using Core.Helpers.Configuration;
using Core.Features;

namespace Core.Features.Multilingual
{
    public partial class Translation
    {
        static bool ExceptionRaisedOnce = false;

        public const string ResourceKeyRegEx = @"[\[]{2,3}[\w]+\.[\w\s\*\/\\\?\.\(\)\-#:;&'\+\{\}]*[\]]{2,3}";

        /// <summary>
        /// Translates the stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static string Translate(string stream)
        {
            var provider = Finder.LanguageProvider; 
            if (!provider.IsNull())
            {
                return Translate(stream, provider.GetUserLanguage(Operator.Current).Code);
            }
            if (!ExceptionRaisedOnce)
            {
                var x = new ApplicationException("To use Translate method without arguments you should make your Application implement IUserLanguageProvider");
                x.Log();
                ExceptionRaisedOnce = true;
            }
            if (Language.ApplicationDefault.IsNull())
            {
                return Translate(stream, null);
            }
            return Translate(stream, Language.ApplicationDefault.Code);
        }

        /// <summary>
        /// Translates the stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        /// 
        static Regex r = new Regex(ResourceKeyRegEx, RegexOptions.Compiled);

        public static string Translate(string stream, string LanguageCode)
        {
            if (!LanguageCode.IsFilled()) return stream;
            return r.Replace(stream, m => GetTranslation(m.Value, LanguageCode));
          //  return Regex.Replace(stream, ResourceKeyRegEx, m => GetTranslation(m.Value, LanguageCode), RegexOptions.Compiled);
        }

        /// <summary>
        /// Gets the translation for the given resource key (label).
        /// The format of the <paramref name="resourceKey"/> must match with
        /// one the following patterns: [[label]] or [[[label]]].
        /// The first pattern represents the long translation and the second
        /// pattern represents the short translation of the label.
        /// </summary>
        /// <param name="resourceKey">The resource key.</param>
        /// <returns>
        /// The translation corresponding to the <paramref name="resourceKey"/> 
        /// parameter.
        /// </returns>
        public static string GetTranslation(string resourceKey)
        {
            var provider = Finder.LanguageProvider;
            if (!provider.IsNull())
            {
                return GetTranslation(resourceKey, provider.GetUserLanguage(Operator.Current).Code);
            }
            var x = new ApplicationException("To use Translate method without arguments you should make your Application implement IUserLanguageProvider");
            x.Log();
            return GetTranslation(resourceKey, Language.ApplicationDefault.Code);
        }

        /// <summary>
        /// Gets the translation for the given resource key (label) and languageId.
        /// The format of the <paramref name="resourceKey"/> must match with
        /// one the following patterns: [[label]] or [[[label]]].
        /// The first pattern represents the long translation and the second
        /// pattern represents the short translation of the label.
        /// If the translation is not found in the given language id, the default
        /// language id is used.
        /// </summary>
        /// <param name="resourceKey">The resource key.</param>
        /// <param name="languageId">The language id.</param>
        /// <returns>
        /// The translation corresponding to the <paramref name="resourceKey"/> 
        /// parameter.
        /// </returns>
        public static string GetTranslation(string resourceKey, string languageCode)
        {
            if (resourceKey == null || resourceKey.Length == 0) return null;
            if (resourceKey.StartsWith("[[")) resourceKey = resourceKey.TrimStart("[[").TrimEnd("]]");

            string label = null;
            
            if (languageCode.IsNotNull())
            {
                var lbl = MultiLingualCache.AllTranslations().AsParallel().FirstOrDefault(x => String.Equals(x.Key , resourceKey, StringComparison.InvariantCultureIgnoreCase) && x.LanguageCode == languageCode);
                if (lbl != null) label = lbl.Value;

               // label = MultiLingualCache.AllTranslations().Where(x => x.Key == resourceKey && x.LanguageCode == languageCode).Select(y => y.Value).FirstOrDefault();

                if (label == null && languageCode != Language.ApplicationDefault.Code)
                {
                    lbl = MultiLingualCache.AllTranslations().AsParallel().FirstOrDefault(x => String.Equals(x.Key, resourceKey, StringComparison.InvariantCultureIgnoreCase) && x.LanguageCode == Language.ApplicationDefault.Code);
                    if (lbl != null) label = lbl.Value;
                }
            }

            if (label == null)
            {
                var index = resourceKey.LastIndexOf('.');
                label = resourceKey.Substring(index + 1, resourceKey.Length - index - 1);

                if (label != null && Config.Get("TRANSLATION_INSERT_MISSING_LABELS", true))
                {
                    // Insert the label into the Framework database
                    Finder.TranslationRepository.Insert(
                        new Translation
                        {
                            Key = resourceKey,
                            Value = label,
                            LanguageCode = Language.ApplicationDefault.Code
                        });

                    MultiLingualCache.Reload();
                }
            }

            return label;
        }

        /// <summary>
        /// Extracts the label code from bracketed format.
        /// </summary>
        /// <param name="bracketed">The bracketed.</param>
        /// <returns></returns>
        public string ExtractLabelCodeFromBracketed(string bracketed)
        {
            if (bracketed == null || bracketed.Length == 0 || bracketed.IndexOf("[[") < 0) return bracketed;

            bool isShort = bracketed.IndexOf("[[[") >= 0;
            return isShort ? bracketed.Substring(3, bracketed.Length - 6) : bracketed.Substring(2, bracketed.Length - 4);
        }
    }
}
