﻿using System;
using System.Collections.Generic;
using Core.Features;

namespace Core.Features.Multilingual
{
    delegate void CacheInvalidatedDelegate();
    static class MultiLingualCache
    {
        public static event CacheInvalidatedDelegate CacheInvalidated; 

        private static readonly List<Language> _languagesCache = new List<Language>();
        private static readonly List<Translation> _translationsCache = new List<Translation>();

        static MultiLingualCache()
        {
            Reload();
        }

        internal static void Reload()
        {
            _languagesCache.Clear();
            _translationsCache.Clear();
            if (Finder.LanguageRepository.IsNotNull())
                _languagesCache.AddRange(Finder.LanguageRepository.AllLanguages());
            if (Finder.TranslationRepository.IsNotNull())
                _translationsCache.AddRange(Finder.TranslationRepository.AllTranslations());
            if (CacheInvalidated != null)
                CacheInvalidated();
        }

        public static IEnumerable<Language> AllLanguages()
        {
            return _languagesCache;
        }

        public static IEnumerable<Translation> AllTranslations()
        {
            return _translationsCache;
        }
    }


}
