﻿
namespace Core.Features.Multilingual
{
    public interface IUserLanguageProvider
    {
        Language GetUserLanguage(string userName);
    }
}
