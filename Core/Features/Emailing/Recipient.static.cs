﻿using System;
using Core.Features;

namespace Core.Features.Emailing
{
    public partial class Recipient
    {
        public static Recipient Save(Recipient recipient)
        {
            if (recipient.Id.IsDefault())
            {
                recipient.Id = Comb.NewGuid();
            }

            return Finder.EmailRepository.Save(recipient);
        }

        internal static void Validate(Recipient recipient)
        {
            recipient.ThrowIfNull(new EmailSaveException("[[FWK.Email recipient cannot be null]]"));
            recipient.EmailId.ThrowIfDefault(new EmailSaveException("[[FWK.Email recipient must be linked to an email]]"));
            recipient.Address.ThrowIfNull(new EmailSaveException("[[FWK.Email recipient address cannot be null]]"));
            recipient.Type.ThrowIfDefault(new EmailSaveException("[[FWK.Email recipient type must be set]]"));
        }
    }
}
