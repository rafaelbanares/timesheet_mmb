﻿using System;

namespace Core.Features.Emailing
{
    public partial class Recipient
    {
        public Guid Id { get; set; }
        public Guid EmailId { get; set; }
        public RecipientType Type { get; set; }
        public string Address { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? TryDate { get; set; }
        public string Data { get; set; }
        public int NumberOfTries { get; set; }
         
        public enum RecipientType
        {
            To = 1,
            Cc = 2,
            Bcc = 4
        }

        public void Save()
        {
            Recipient.Save(this);
        }
    }
}
