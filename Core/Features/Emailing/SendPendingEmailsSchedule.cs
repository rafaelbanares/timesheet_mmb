﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using Core.WinService.Scheduling.Cron;

namespace Core.Features.Emailing
{
    public class SendPendingEmailsSchedule : PredefinedSchedule
    {
        public static readonly Guid ScheduleId = new Guid("5C895CB6-4B4F-4530-B8A5-AF4170EB2E64");

        public SendPendingEmailsSchedule()
            : base(typeof(SendPendingEmailsTask), ScheduleId, "Send pending emails", CronBuilder.CreateHourlyTrigger(0, 58, 2), "")
        {}
    }
}
