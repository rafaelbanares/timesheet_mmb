﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using Core.Features;
using Core.Helpers.Logging;
using Core.Features.Security;
using Core.Model;

namespace Core.Features.Emailing
{
    public class SendPendingEmailsTask : IScheduledTask
    {
        public void Execute(Schedule job)
        {
            try
            {
                PendingEmailsSender.Send();
            }
            catch (Exception ex)
            {
                job.Log(ex);
            }
        }
    }
}
