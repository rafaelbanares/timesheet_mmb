﻿using System;
using Core.Model;
using System.Net.Mail;
using System.Collections.Generic;
using System.Transactions;

namespace Core.Features.Emailing
{
    public partial class EmailDbo
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SenderAddress { get; set; }
        public int Priority { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsTest { get; set; }
    }
}
