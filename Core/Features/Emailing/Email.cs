﻿using System;
using Core.Model;
using System.Net.Mail;
using System.Collections.Generic;
using System.Transactions;
using Core.Features.DocumentsManagement;
using System.IO;
using Core.Helpers.Configuration;
using Core.Features;
using Core.Model.Security;

namespace Core.Features.Emailing
{
    public partial class Email
    {
        private EmailDbo innerEmail = new EmailDbo();
        private List<Recipient> recipients = new List<Recipient>();
        private List<Document> documents = new List<Document>();

        public static Email Create(string subject, string body)
        {
            return new Email(subject, body);
        }

        public Email(string subject, string body)
        {
            innerEmail.IsTest = SmtpClientHelper.Configuration.IsTestMode;
            if (innerEmail.IsTest)
            {
                //innerEmail.SenderAddress = SGS.CurrentUser.Email;
            }

            innerEmail.Subject = subject;
            innerEmail.Body = body;
        }

        public Email From(string email)
        {
            innerEmail.SenderAddress = email;

            return this;
        }

        public Email To(string email)
        {
            return AddRecipient(email, Recipient.RecipientType.To);
        }

        public Email To(MailAddressCollection to)
        {
            to.ForEach(x => To(x.Address));

            return this;
        }

        public Email Cc(string email)
        {
            return AddRecipient(email, Recipient.RecipientType.Cc);
        }

        public Email Cc(MailAddressCollection cc)
        {
            cc.ForEach(x => Cc(x.Address));

            return this;
        }

        public Email Bcc(string email)
        {
            return AddRecipient(email, Recipient.RecipientType.Bcc);
        }

        public Email Bcc(MailAddressCollection bcc)
        {
            bcc.ForEach(x => Bcc(x.Address));

            return this;
        }

        public Email Attach(Guid documentId)
        {
            documents.Add(Document.Get(documentId));
            return this;
        }

        public Email Attach(List<Guid> documentIds)
        {
            documentIds.ForEach(id => documents.Add(Document.Get(id)));
            return this;
        }

        public Email Attach(string path)
        {
            documents.Add(Document.Create(path));
            
            return this;
        }

        public Email Attach(string name, byte[] data)
        {
            documents.Add(Document.Create(name, data));

            return this;
        }

        public Guid Send()
        {
            using (var t = new TransactionScope())
            {
                EmailDbo.Save(innerEmail);

                recipients.ForEach(x =>
                {
                    x.EmailId = innerEmail.Id;
                    x.Save();
                });

                documents.ForEach(x =>
                {
                    EmailDbo.Attach(innerEmail.Id, x.Save().Id);
                });

                t.Complete();
            }

            return innerEmail.Id;
        }

        private Email AddRecipient(string address, Recipient.RecipientType type )
        {
            recipients.Add(new Recipient
            {
                Address = address,
                Type = type
            });

            return this;
        }
    }
}
