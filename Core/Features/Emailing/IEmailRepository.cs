﻿using System;
using System.Collections.Generic;

namespace Core.Features.Emailing
{
    public interface IEmailRepository
    {
        EmailDbo GetEmail(Guid id);
        IEnumerable<EmailDbo> GetPendingEmails(int maxtries);
        IEnumerable<Recipient> GetRecipients(Guid emailId);
        IEnumerable<Guid> GetAttachments(Guid emailId);
        Recipient Save(Recipient email);
        EmailDbo Save(EmailDbo email);
        void Attach(Guid emailId, Guid documentId);
    }
}
