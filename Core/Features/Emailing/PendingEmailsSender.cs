﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Features.Scheduling;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using Core.Features;
using Core.Helpers.Logging;
using Core.Features.Security;
using Core.Model;

namespace Core.Features.Emailing
{
    public static class PendingEmailsSender 
    {
        public static void Send()
        {
            var emails = EmailDbo.GetPendingEmails();
            //Debug.WriteLine("SendMailJob: " + emails.Count() + " pending emails");
            foreach (var mail in emails)
            {
                var message = new MailMessage();
                try
                {
                    EmailSendingAttempt(mail);

                    message.Subject = mail.Subject;
                    if (Regex.IsMatch(mail.SenderAddress, Core.Constants.RegularExpressions.EmailAddress))
                    {
                        var persons = User.GetByEmail<User>(mail.SenderAddress);

                        var personEntity = persons.FirstOrDefault() ?? User.Get(mail.CreatedBy);

                        message.From = new MailAddress(mail.SenderAddress, personEntity.IsNotNull() ? personEntity.FullName : mail.SenderAddress);
                    }
                    else
                    {
                        message.From = new MailAddress(SmtpClientHelper.Configuration.SenderEmail, mail.SenderAddress);
                    }

                    var recipients = EmailDbo.GetRecipients(mail.Id);

                    if (mail.IsTest)
                    {
                        var createByUser = User.Get(mail.CreatedBy);
                        if (createByUser.IsNotNull()) message.To.Add(createByUser.Email);
                        else message.To.Add(User.Get(Operator.Current).Email);
                    }
                    else
                    {
                        foreach (var recipient in recipients.Where(x => x.SendDate.IsNull() && x.NumberOfTries < EmailDbo.MaxTries))
                        {
                            if (MailAddressHelper.IsValidEmailAddress(recipient.Address))
                            {
                                if (recipient.Type == Recipient.RecipientType.To) message.To.Add(recipient.Address);
                                else if (recipient.Type == Recipient.RecipientType.Cc) message.CC.Add(recipient.Address);
                                else if (recipient.Type == Recipient.RecipientType.Bcc) message.Bcc.Add(recipient.Address);
                            }
                        }
                    }


                    message.Body = mail.Body;
                    message.IsBodyHtml = true;
                    message.Priority = ConvertImportance(mail.Priority);

                    foreach (var att in EmailDbo.GetAttachments(mail.Id))
                    {
                        var ms = new MemoryStream(att.BinaryData);
                        var attachment = new Attachment(ms, att.MimeType)
                        {
                            Name = att.Name
                        };

                        message.Attachments.Add(attachment);
                    }

                    if (mail.IsTest)
                    {
                        var fileName = "Recipients.txt";
                        var contentType = "text/plain";

                        // TO
                        string content = "From: " + Environment.NewLine;
                        content += mail.SenderAddress + Environment.NewLine;

                        content += "To: ";
                        content += GetRecipientsByType(recipients, Recipient.RecipientType.To);
                        // CC
                        content += "Cc: ";
                        content += GetRecipientsByType(recipients, Recipient.RecipientType.Cc);
                        // BCC
                        content += "Bcc: ";
                        content += GetRecipientsByType(recipients, Recipient.RecipientType.Bcc);

                        var encoder = new UTF8Encoding();
                        var ms = new MemoryStream(encoder.GetBytes(content));
                        var attachment = new Attachment(ms, contentType)
                        {
                            Name = fileName
                        };

                        message.Attachments.Add(attachment);
                    }

                    SmtpClientHelper.Send(message);

                    EmailSuccessfullySent(mail);
                }
                catch (System.Net.Mail.SmtpFailedRecipientsException mailExc)
                {
                    //Log Error
                    if (mailExc.InnerExceptions != null)
                    {
                        foreach (SmtpFailedRecipientException sfrEx in mailExc.InnerExceptions)
                        {
                            HandleRecipientFailed(mail, sfrEx);
                        }
                    }
                }
                catch (System.Net.Mail.SmtpFailedRecipientException mailExc)
                {
                    //Log Error
                    HandleRecipientFailed(mail, mailExc);
                }
            }
        }

        private static MailPriority ConvertImportance(int? importance)
        {
            var result = MailPriority.Normal;
            if (importance.HasValue)
            {
                switch (importance.Value)
                {
                    case 0:
                        result = MailPriority.Low;
                        break;
                    case 1:
                        result = MailPriority.Normal;
                        break;
                    case 2:
                        result = MailPriority.High;
                        break;
                }
            }
            return result;
        }

        private static string ExtractRecipientAddress(SmtpFailedRecipientException exception)
        {
            return exception.FailedRecipient.Replace(">", "").Replace("<", "").Replace(" ", "");
        }

        private static void HandleRecipientFailed(EmailDbo mail, SmtpFailedRecipientException sfrEx)
        {
            string address = ExtractRecipientAddress(sfrEx);

            var recipientEntity = EmailDbo.GetRecipients(mail.Id).Where(x => x.Address.Equals(address, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            if (recipientEntity != null)
            {

                if (sfrEx.StatusCode == SmtpStatusCode.Ok)
                {
                    recipientEntity.SendDate = DateTime.UtcNow;
                }
                else
                {
                    recipientEntity.NumberOfTries++;
                    recipientEntity.Data = ExceptionFormatter.Format(sfrEx);
                }

                Recipient.Save(recipientEntity);
            }
        }

        private static string GetRecipientsByType(IEnumerable<Recipient> recipients, Recipient.RecipientType recipientType)
        {
            string content = string.Empty;

            var toRecipients = recipients
                .Where(r => r.Type == recipientType &&
                            MailAddressHelper.IsValidEmailAddress(r.Address))
                .Select(r => r.Address);

            foreach (string address in toRecipients) content += address + ";";
            content = content.TrimEnd(";".ToCharArray()) + Environment.NewLine;
            return content;
        }

        private static void EmailSendingAttempt(EmailDbo email)
        {
            if (email == null)
                return;

            EmailDbo.GetRecipients(email.Id).ToList().ForEach(x => { x.TryDate = DateTime.UtcNow; Recipient.Save(x); });
        }

        private static void EmailSuccessfullySent(EmailDbo email)
        {
            if (email == null)
                return;

            EmailDbo.GetRecipients(email.Id).ToList().ForEach(x => { x.SendDate = DateTime.UtcNow; Recipient.Save(x); });
        }
    }
}
