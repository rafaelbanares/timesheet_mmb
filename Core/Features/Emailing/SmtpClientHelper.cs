﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Helpers.Configuration;
using System.Net.Mail;
using System.Net.Sockets;

namespace Core.Features.Emailing
{
    public class SmtpClientHelper
    {
        public static class Configuration
        {  
           // public static string Server { get { return Config.Get("SMTP_SERVER", "10.90.0.135"); } }
            public static string Server { get { return Config.Get("SMTP_SERVER", "smtp-rns.sgs.net"); } }
            public static int Port { get { return Config.Get("SMTP_PORT", 25); } }
            public static string UserName { get { return Config.Get("SMTP_USER", @"eame\t_equant_test7"); } }
            public static string Password { get { return Config.Get("SMTP_PASSWORD", "Exchange2009"); } }
            public static bool UseDefaultCredentials { get { return Config.Get("SMTP_USEDEFAULTCREDENTIALS", false); } }
            public static bool IsTestMode { get { return Config.Get("SMTP_TEST_MODE", true); } }
            public static string SenderEmail { get { return Config.Get("SMTP_SENDER_EMAIL", "sgs@sgs.com"); } }

            public static SmtpInfos SmtpInfo
            {
                get
                {
                    return new SmtpInfos
                    {
                        ServerName = Configuration.Server,
                        Port = Configuration.Port,
                        UserName = Configuration.UseDefaultCredentials ? null : Configuration.UserName,
                        Password = Configuration.UseDefaultCredentials ? null : Configuration.Password
                    };
                }
            }
        }

        public static SmtpClient GetClient()
        {
            var client = new SmtpClient(Configuration.Server, Configuration.Port);
            if (Configuration.UseDefaultCredentials) client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            else client.Credentials = new System.Net.NetworkCredential(Configuration.UserName, Configuration.Password);
          //  client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            return client;
        }

        public static bool TestConnect()
        {
            bool isConnectionValid = false;
            TcpClient client = new TcpClient();

            try
            {
                client.Connect(Configuration.Server, Configuration.Port);
                isConnectionValid = client.Connected;
                client.Close();
            }
            catch
            {
                // invalid connection
            }
            finally
            {
                client.Close();
            }
            return isConnectionValid;
        }

        public static void Send(MailMessage message)
        {
            GetClient().Send(message);
        }

        public static void SendMail(string recipient, string cc, string title, string message)
        {
            SmtpHelper.Send(Configuration.SmtpInfo, Configuration.SenderEmail, recipient, cc, title, message, null);
        }
    }
}
