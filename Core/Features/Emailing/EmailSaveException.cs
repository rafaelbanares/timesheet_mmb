﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Runtime.Serialization;

namespace Core.Features.Emailing
{
    [Serializable]
    public class EmailSaveException : FrameworkException
    {
        public EmailSaveException() { }

        public EmailSaveException(string message) : base(message) { }

        public EmailSaveException(string message, Exception inner) : base(message, inner) { }
    }
}
