﻿using System;
using System.Collections.Generic;
using Core.Helpers.Configuration;
using Core.Model;
using Core.Features.Security;
using Core.Features;

namespace Core.Features.Emailing
{
    public partial class EmailDbo
    {
        internal static int MaxTries
        {
            get
            {
                return Config.Get<int>("EMAIL_MAX_TRIES", 3);
            }
        }

        public static void Attach(Guid emailId, Guid documentId)
        {
            Finder.EmailRepository.Attach(emailId, documentId);
        }

        public static EmailDbo Get(Guid id)
        {
            return Finder.EmailRepository.GetEmail(id);
        }

        public static IEnumerable<EmailDbo> GetPendingEmails()
        {
            return Finder.EmailRepository.GetPendingEmails(MaxTries);
        }

        public static IEnumerable<Recipient> GetRecipients(Guid emailId)
        {
            return Finder.EmailRepository.GetRecipients(emailId);
        }

        public static IEnumerable<EmailAttachment> GetAttachments(Guid emailId)
        {
            var atts = new List<EmailAttachment>();

            foreach (var id in Finder.EmailRepository.GetAttachments(emailId))
            {
                var doc = Finder.DocumentRepository.GetDocument(id);
                var bin = Finder.DocumentRepository.GetBinary(id);

                atts.Add(new EmailAttachment { BinaryData = bin, Name = doc.Name, MimeType = doc.MimeType });
            }

            return atts;
        }

        public static EmailDbo Save(EmailDbo email)
        {
            Validate(email);

            if (email.Id.IsDefault())
            {
                email.Id = Comb.NewGuid();
            }

            return Finder.EmailRepository.Save(email);
        }

        internal static void Validate(EmailDbo email)
        {
            email.ThrowIfNull(new EmailSaveException(Constants.EMAIL_CANNOT_BE_NULL));
            if (email.CreatedBy.IsNullOrEmpty())
            {
                email.CreatedBy = Operator.Current;
            }
            if (email.CreatedOn.IsDefault())
            {
                email.CreatedOn = DateTime.UtcNow;
            }
            if (email.SenderAddress.IsNull())
            {
                email.SenderAddress = SmtpClientHelper.Configuration.SenderEmail;
            }

            email.Body.ThrowIfNull(new EmailSaveException(Constants.EMAIL_BODY_CANNOT_BE_NULL));
            email.Subject.ThrowIfNull(new EmailSaveException(Constants.EMAIL_SUBJECT_CANNOT_BE_NULL));
        }

        public static class Constants
        {
            public static string EMAIL_CANNOT_BE_NULL = "[[FWK.Email cannot be null]]";
            public static string EMAIL_BODY_CANNOT_BE_NULL = "[[FWK.Email body cannot be null]]";
            public static string EMAIL_SENDER_ADDRESS_CANNOT_BE_NULL = "[[FWK.Email sender address cannot be null]]";
            public static string EMAIL_SUBJECT_CANNOT_BE_NULL = "[[FWK.Email subject cannot be null]]";
        }
    }
}
