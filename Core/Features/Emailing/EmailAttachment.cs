﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.Emailing
{
    public class EmailAttachment
    {
        public string Name { get; set; }
        public string MimeType { get; set; }
        public byte[] BinaryData { get; set; }
    }
}
