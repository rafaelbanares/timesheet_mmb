﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Features.DatabaseProvider
{
    public interface IFrameworkDatabaseConnections 
    {
        ConnectionInfos Main { get; }
        ConnectionInfos Archive { get; }
    }
}
