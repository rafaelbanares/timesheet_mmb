﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Transactions;
using System.Collections.Concurrent;
using Core.Helpers;
using System.DirectoryServices.AccountManagement;

namespace Core.Bus
{
    public interface IHandleMessageOfType<T>
    {
        void Handle(T message);
    }

    public interface IAmAutoSubscriber
    {
        string PublisherEndpoint { get; }
        bool IsActive { get; }
    }

    public interface IAmAutoSubscriberOfType<T> : IHandleMessageOfType<T>, IAmAutoSubscriber
    { }

    public partial interface IServiceBus
    {
        void Send(object message, string RemoteEndpoint);
        void Publish(object message);
        void Subscribe<T>(string RemoteEndpoint);
        void Unsubscribe<T>(string RemoteEndpoint);
        string LocalEndpoint { set; }
    }

    public partial class ServiceBus : IServiceBus
    {
        private static IMessageFormatter formatter = new BinaryMessageFormatter();
        private static readonly IServiceBus _bus = new ServiceBus();
        private MessageQueue _queue;
        private volatile bool _isStarted = false;
        private object locker = new object();
        private ServiceBus() { }

        public static IServiceBus Current { get { return _bus; } }
        internal List<MessageHandlerRegistration> _messageHandlers = new List<MessageHandlerRegistration>();
        ConcurrentDictionary<string, Subscription> _subscriptions = new ConcurrentDictionary<string, Subscription>();

        public void Send(object message, string RemoteEndpoint)
        {
            if (message == null) throw new Exception("ServiceBus does not see the point in Sending null messages yet :-)");
            if (string.IsNullOrWhiteSpace(RemoteEndpoint)) throw new Exception(string.Format("ServiceBus says: Where shall I Send message {0}?", message.GetType().FullName));
            ServiceBusCommand.Create(message).Send(RemoteEndpoint);
        }

        public void Publish(object message)
        {
            if (message == null) throw new Exception("ServiceBus does not see the point in Publishing null messages yet :-)");
            ServiceBusCommand.Create(message).Publish(this);
        }

        public void Subscribe<T>(string RemoteEndpoint)
        {
            Subscribe(typeof(T), RemoteEndpoint);
        }

        public void Unsubscribe<T>(string RemoteEndpoint)
        {
            Unsubscribe(typeof(T), RemoteEndpoint);
        }

        void Subscribe(Type messageType, string RemoteEndpoint)
        {
            if (messageType == null) throw new Exception("ServiceBus does not support Subscription to null message type yet :-)");
            if (string.IsNullOrWhiteSpace(RemoteEndpoint)) throw new Exception(string.Format("ServiceBus says: Where shall I Subscribe to {0}?", messageType.FullName));
            ServiceBusSubscriptionCommand.Create(messageType, true, IncomingQueue).Send(RemoteEndpoint);
        }

        public void Unsubscribe(Type messageType, string RemoteEndpoint)
        {
            if (messageType == null) throw new Exception("ServiceBus does not support Unsubscription to null message type yet :-)");
            if (string.IsNullOrWhiteSpace(RemoteEndpoint)) throw new Exception(string.Format("ServiceBus says: Where shall I Unsubscribe to {0}?", messageType.FullName));
            ServiceBusSubscriptionCommand.Create(messageType, false, IncomingQueue).Send(RemoteEndpoint);
        }

        private EndpointDescription _localEndpoint;
        public string LocalEndpoint
        {
            set
            {
                Stop();
                _localEndpoint = GetEndpointDescription(value);
                Start();
            }
        }

        EndpointDescription IncomingQueue
        {
            get { return _localEndpoint; }
        }

        EndpointDescription ErrorStore
        {
            get
            {
                return IncomingQueue != null ? IncomingQueue.GetChildQueueDescriptionFor("errors") : null;
            }
        }

        EndpointDescription SubscriptionStore
        {
            get
            {
                return IncomingQueue != null ? IncomingQueue.GetChildQueueDescriptionFor("subscriptions") : null;
            }
        }

        internal class EndpointDescription
        {
            public string Machine { get; set; }
            public string Bucket { get; set; }
            public string QueueName { get; set; }

            public MessageQueue Queue
            {
                get { return new MessageQueue(MsmqAddress) { MessageReadPropertyFilter = { Label = true, AppSpecific = true } }; }
            }

            public string MsmqAddress
            {
                get { return Machine + "\\" + Bucket + "\\" + QueueName; }
            }

            public string SweetName
            {
                get { return QueueName + "@" + Machine; }
            }

            public override string ToString()
            {
                return SweetName;
            }

            public EndpointDescription GetChildQueueDescriptionFor(string appendix)
            {
                var result = new EndpointDescription();
                result.Machine = this.Machine;
                result.Bucket = this.Bucket;
                result.QueueName = this.QueueName + "_" + appendix;
                return result;
            }

            public void CreateIfNotExists(bool transactional)
            {
                if (!MessageQueue.Exists(this.MsmqAddress))
                {
                    ConsoleInfo("Creating queue: " + this.SweetName);
                    var queue = MessageQueue.Create(this.MsmqAddress, transactional);
                    string EveryoneName = null;
                    string AnonymousName = null;
                    var EveryOneSid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                    var AnonymousSID = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.AnonymousSid, null);
                    using (PrincipalContext context = new PrincipalContext(ContextType.Machine))
                    {
                        using (GroupPrincipal everyone = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, EveryOneSid.Value))
                        {
                            EveryoneName = everyone.SamAccountName;
                        }
                        using (GroupPrincipal anonymous = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, AnonymousSID.Value))
                        {
                            AnonymousName = anonymous.SamAccountName;
                        }
                    }

                    queue.SetPermissions(EveryoneName, MessageQueueAccessRights.FullControl);
                    queue.SetPermissions(AnonymousName,
                        MessageQueueAccessRights.ReceiveMessage |
                        MessageQueueAccessRights.PeekMessage |
                        MessageQueueAccessRights.WriteMessage);
                }
            }
        }

        private static EndpointDescription GetEndpointDescription(string value)
        {
            var machine = ".";
            var bucket = "private$";
            var queue = value;
            var found = false;
            if (value.Contains("@"))
            {
                var splitted = value.Split('@');
                machine = splitted[1];
                queue = splitted[0];
                found = true;
            }
            if (!found && value.Contains("\\"))
            {
                var splitted = value.Split("\\");
                machine = splitted[0];
                bucket = splitted[1];
                queue = splitted[2];
            }

            if (string.Equals(machine, "localhost", StringComparison.OrdinalIgnoreCase) || string.Equals(machine, "127.0.0.1", StringComparison.OrdinalIgnoreCase)) machine = ".";
            return new EndpointDescription { Bucket = bucket, Machine = machine, QueueName = queue };
        }

        internal void RegisterSubscriber(Subscription subscription)
        {
            ConsoleInfo("Received subscription demand for " + subscription);
            if (!_subscriptions.Values.Any(x => x == subscription))
            {
                var msmqMessage = new Message { Recoverable = true, Label = subscription };
                var msgQ = SubscriptionStore.Queue;
                using (var tx = new TransactionScope())
                {
                    msgQ.Send(msmqMessage, MessageQueueTransactionType.Automatic);
                    if (!_subscriptions.TryAdd(msmqMessage.Id, subscription))
                        throw new Exception("ServiceBus can't record this subscription : " + subscription);
                    tx.Complete();
                }
            }
        }

        internal void UnregisterSubscriber(Subscription subscription)
        {
            ConsoleInfo("Received unsubscribe demand for " + subscription);
            var id = _subscriptions.Where(x => x.Value == subscription).Select(x => x.Key).FirstOrDefault();
            if (id != null)
            {
                using (var tx = new TransactionScope())
                {
                    var _subscriptionQueue = SubscriptionStore.Queue;
                    _subscriptionQueue.ReceiveById(id, MessageQueueTransactionType.Automatic);
                    Subscription sub;
                    _subscriptions.TryRemove(id, out sub);
                    if (sub != null) tx.Complete();
                    else throw new Exception("ServiceBus can't unregister this subscription : " + subscription);
                }
            }
        }

        void Stop()
        {
            if (_isStarted)
            {
                lock (locker)
                {
                    if (!_isStarted)
                    {
                        _queue.PeekCompleted -= QueuePeekCompleted;
                        _isStarted = false;
                    }
                }
            }
        }

        void Start()
        {
            if (!_isStarted)
            {
                lock (locker)
                {
                    if (!_isStarted)
                    {
                        IncomingQueue.CreateIfNotExists(true);
                        ErrorStore.CreateIfNotExists(true);
                        SubscriptionStore.CreateIfNotExists(true);

                        _queue = IncomingQueue.Queue;
                        _queue.PeekCompleted += new PeekCompletedEventHandler(QueuePeekCompleted);
                        RetrieveSubscribers();

                        RegisterSubscriptions();
                        RegisterMessageHandlers();

                        _isStarted = true;
                        _queue.BeginPeek();
                    }
                }
            }
        }

        void RetrieveSubscribers()
        {
            var _subscriptionQueue = SubscriptionStore.Queue;
            MessageEnumerator enumerator = _subscriptionQueue.GetMessageEnumerator2();
            while (enumerator.MoveNext())
            {
                var current = enumerator.Current;
                _subscriptions.AddOrUpdate(current.Id, current.Label, (x, y) => y);
            }
            enumerator.Close();
        }

        void RegisterSubscriptions()
        {
            _messageHandlers.Clear();

            var subscriptions = typeof(IAmAutoSubscriber).GetMatchingTypes(x => !x.IsAbstract && !x.IsInterface);

            subscriptions.ForEach(subscription =>
            {
                var autoSubscriber = subscription.GetOne<IAmAutoSubscriber>();
                var subscribesTo = subscription
                                .GetInterfaces()
                                .Where(i => i.Name.Contains(typeof(IHandleMessageOfType<>).Name));

                if (subscribesTo.Count() == 0)
                {
                    ConsoleInfo("To use IAmAutoSubscriber you should use it coordination with IHandleMessageOfType<T> or directly implement IAmAutoSubscriberOfType<T>");
                }
                subscribesTo.ForEach(x =>
                {
                    var handledObjectType = x.GetGenericArguments().First();
                    if (autoSubscriber.IsActive) Subscribe(handledObjectType, autoSubscriber.PublisherEndpoint);
                    else Unsubscribe(handledObjectType, autoSubscriber.PublisherEndpoint);
                });
            });
        }

        void RegisterMessageHandlers()
        {
            _messageHandlers.Clear();

            var handlerTypes = typeof(IHandleMessageOfType<>)
                .GetMatchingTypes(x => !x.IsAbstract && !x.IsInterface);

            handlerTypes.ForEach(handler =>
            {
                handler
                .GetInterfaces()
                .Where(i => i.Name.Contains(typeof(IHandleMessageOfType<>).Name))
                .ForEach(interf =>
                {
                    var handledObjectType = interf.GetGenericArguments().First();
                    var internalHandler = typeof(MessageHandlerRegistration<>).MakeGenericType(new[] { handledObjectType });
                    var reg = internalHandler.GetOne<MessageHandlerRegistration>();
                    reg.HandledMessageType = handledObjectType;
                    reg.Handler = handler.GetOne();

                    _messageHandlers.Add(reg);
                });
            });
        }

        void QueuePeekCompleted(object sender, PeekCompletedEventArgs e)
        {
            var cmq = (MessageQueue)sender;
            cmq.EndPeek(e.AsyncResult);

            Message msg = null; //keep outside scope to move this to the error log 
            try
            {
                msg = cmq.Receive();
                if (msg == null) throw new InvalidOperationException("Null message should not be possible");
                if (msg.AppSpecific == 0)
                {
                    var internalHandler = TypeHelper.GetType(msg.Label).GetOne<ServiceBusMessageBase>();
                    internalHandler.Handle(this, msg);
                }
            }
            catch (Exception ex)
            {
                ConsoleError("Exception in Peek: " + ex.Message);
                ConsoleInfo(ex.StackTrace);

                if (msg != null)
                    using (var scope = new TransactionScope())
                    {
                        using (var myQueue = new MessageQueue(cmq.MachineName + "\\" + cmq.QueueName + "_errors"))
                        {
                            myQueue.Send(msg, MessageQueueTransactionType.Automatic);
                        }
                        scope.Complete();
                    }
            }
            cmq.Refresh();
            cmq.BeginPeek();
        }

        private static void ConsoleInfo(string text)
        {
           // Console.ForegroundColor = ConsoleColor.Yellow;
            System.Diagnostics.Trace.WriteLine(text);
            //Console.ResetColor();
        }

        private static void ConsoleError(string text)
        {
            //Console.ForegroundColor = ConsoleColor.Red;
            System.Diagnostics.Trace.WriteLine(text);
            //Console.ResetColor();
        }

        [Serializable]
        public abstract class ServiceBusMessageBase
        {
            public abstract void Handle(ServiceBus sender, Message msmqMessage);

            public void Send(string RemoteEndpoint)
            {
                var msmqMessage = new Message { Recoverable = true, Label = this.GetType().FullName };
                msmqMessage.Formatter = formatter;
                msmqMessage.Body = this;
                var msgQ = GetEndpointDescription(RemoteEndpoint).Queue;
                using (var tx = new TransactionScope())
                {
                    msgQ.Send(msmqMessage, MessageQueueTransactionType.Automatic);
                    tx.Complete();
                }
            }
        }

        [Serializable]
        public class ServiceBusSubscriptionCommand : ServiceBusMessageBase
        {
            public string CommandType { get; set; }
            public string SubscriberQueue { get; set; }
            public bool IsSubscription { get; set; }

            public override void Handle(ServiceBus sender, Message msmqMessage)
            {
                msmqMessage.Formatter = formatter;
                var ServiceBusMessage = msmqMessage.Body as ServiceBusSubscriptionCommand;

                var subscription = new Subscription { CommandType = ServiceBusMessage.CommandType, Destination = ServiceBusMessage.SubscriberQueue };
                if (ServiceBusMessage.IsSubscription)
                {
                    sender.RegisterSubscriber(subscription);
                }
                else
                {
                    sender.UnregisterSubscriber(subscription);
                }
            }

            internal static ServiceBusSubscriptionCommand Create(Type messageType, bool IsSubscription, EndpointDescription subscriberEndpoint)
            {
                return new ServiceBusSubscriptionCommand
                {
                    CommandType = messageType.FullName,
                    SubscriberQueue = subscriberEndpoint.MsmqAddress,
                    IsSubscription = IsSubscription,
                };
            }
        }


        [Serializable]
        public class ServiceBusCommand : ServiceBusMessageBase
        {
            public object Data { get; set; }
            public string DataType { get; set; }
            public string[] Interfaces { get; set; }
            public string[] BaseTypes { get; set; }

            public static ServiceBusCommand Create(object message)
            {
                var messageType = message.GetType();

                return new ServiceBusCommand
                {
                    Data = message,
                    Interfaces = messageType.GetInterfaces().Select(x => x.FullName).ToArray(),
                    DataType = messageType.FullName,
                    BaseTypes = messageType.GetBaseTypes().Select(x => x.FullName).ToArray()
                };
            }

            public override void Handle(ServiceBus sender, Message msmqMessage)
            {
                msmqMessage.Formatter = formatter;
                var ServiceBusMessage = msmqMessage.Body as ServiceBusCommand;

                var handlers = sender._messageHandlers.Where(x =>
                    x.HandledMessageType.FullName == ServiceBusMessage.DataType
                    || ServiceBusMessage.Interfaces.Contains(x.HandledMessageType.FullName)
                    || ServiceBusMessage.BaseTypes.Contains(x.HandledMessageType.FullName)
                    );

                //call the delegates for this message
                handlers.ForEach(x => x.InternalHandle(ServiceBusMessage));
            }

            internal void Publish(ServiceBus sender)
            {
                var messageType = TypeHelper.GetType(DataType);
                var destinations = sender._subscriptions.Values.Where(x => messageType.Match(x.GetSubscriptionType())).Select(x => x.Destination).ToList();
                destinations.ForEach(x => base.Send(x));
            }
        }

        internal class Subscription : IEquatable<Subscription>
        {
            public string CommandType { get; set; }
            public string Destination { get; set; }


            Type subscribtionCommandType = null;
            public Type GetSubscriptionType()
            {
                if (subscribtionCommandType == null) subscribtionCommandType = TypeHelper.GetType(CommandType);
                return subscribtionCommandType;
            }

            public override string ToString()
            {
                return CommandType + "|" + GetEndpointDescription(Destination).MsmqAddress;
            }

            public bool Equals(Subscription other)
            {
                if (other == null) return false;
                return (this.CommandType == other.CommandType && this.Destination == other.Destination);
            }

            public override int GetHashCode()
            {
                return CommandType.GetHashCode() + Destination.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                var item = obj as Subscription;
                return (item != null) ? this.Equals((Subscription)obj) : false;
            }

            public static bool operator ==(Subscription item1, Subscription item2)
            {
                if (object.ReferenceEquals(item1, item2)) return true;
                if (object.ReferenceEquals(item1, null)) return false;
                if (object.ReferenceEquals(item2, null)) return false;

                return item1.Equals(item2);
            }

            public static bool operator !=(Subscription item1, Subscription item2)
            {
                return !item1.Equals(item2);
            }

            public static implicit operator Subscription(string item)
            {
                if (String.IsNullOrEmpty(item)) return null;
                var splitted = item.Split('|');
                if (splitted.Length != 2) throw new Exception("ServiceBus says: no way to cast the given string as it does not represent a valid Subscription");
                return new Subscription { CommandType = splitted[0], Destination = splitted[1] };
            }

            public static implicit operator string(Subscription item)
            {
                if (item == null) return null;
                return item.ToString();
            }

        }

        internal abstract class MessageHandlerRegistration
        {
            public object Handler { get; set; }
            public Type HandledMessageType { get; set; }
            public abstract void InternalHandle(ServiceBusCommand Message);
        }


        class MessageHandlerRegistration<T> : MessageHandlerRegistration where T : class
        {
            public override void InternalHandle(ServiceBusCommand Message)
            {
                (Handler as IHandleMessageOfType<T>).Handle((T)Message.Data);
            }
        }
    }
}