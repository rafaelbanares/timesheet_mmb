﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace Core.Data
{
    public interface IDatabaseAdministrationImplementationDetails
    {
       // DbConnectionStringBuilder ConnectionStringBuilder { get; }
        string CreateDatabase(string Name);
    }
}
