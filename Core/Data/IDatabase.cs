﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data
{
    public interface IDatabase
    {
        string ConnectionString { get; }
        IDatabaseQueryingImplementationDetails QueryingImplementationDetails { get; }
        IDatabaseAdministrationImplementationDetails AdministrationImplementationDetails { get; }
    }
}
