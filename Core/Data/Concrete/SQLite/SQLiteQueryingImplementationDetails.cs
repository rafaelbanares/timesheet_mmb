﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace Core.Data.Concrete.SQLite
{
    public class SQLiteQueryingImplementationDetails : DatabaseQueryingImplementationDetailsBase
    {
        public override DbProviderFactory ProviderFactory { get { return new System.Data.SQLite.SQLiteFactory(); } }

        public override bool SupportsPagedStatements
        {
            get { return false; }
        }

        public override string GetPagedStatement(string objectName, string orderBy, int startingRecord, int recordCount, string columns = "*", string where = "")
        {
            throw new NotImplementedException();
        }

        public override bool SupportsUpsertStatement
        {
            get { return true; }
        }

        public override string GetUpsertStatement(string objectName, IEnumerable<string> keyColumnNames, IEnumerable<string> editableColumnNames)
        {
            var merged = keyColumnNames.Union(editableColumnNames).Distinct().ToList();

            var insertColumns = string.Join(", ", merged);
            var values = string.Join(", ", merged.Count().ToList(x => "@" + x.ToString()));

            var insertStatement = string.Format("insert or ignore into {0}({1}) values({2});", objectName, insertColumns, values);


            var keysStatement = string.Join(" and ", merged.Count().ToList(x => keyColumnNames.Contains(merged[x]) ? merged[x] + " = @" + x.ToString() : null).Where(x=> x!= null));
            var columnsStatement = string.Join(", ", merged.Count().ToList(x => editableColumnNames.Contains(merged[x]) ? merged[x] + " = @" + x.ToString() : null).Where(x => x != null));

            var updateStatement = string.Format("update {0} set {1} where {2};", objectName, columnsStatement, keysStatement);

            var sql =insertStatement + updateStatement;
            return sql;
        }
    }
}
