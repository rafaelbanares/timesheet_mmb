﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data.Concrete.SQLite
{
    public class SQLiteDatabase : DatabaseBase
    {
        public SQLiteDatabase(string connectionString)
            : base(connectionString)
        { }

        public override IDatabaseQueryingImplementationDetails QueryingImplementationDetails
        {
            get { return new SQLiteQueryingImplementationDetails(); }
        }

        public override IDatabaseAdministrationImplementationDetails AdministrationImplementationDetails
        {
            get { return new SQLiteAdministrationImplementationDetails(); }
        }
    }
}
