﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using Core.Helpers;
using Core.Data.Concrete;

namespace Core.Data.Concrete.SQLite
{
    public class SQLiteAdministrationImplementationDetails : DatabaseAdministrationImplementationDetailsBase
    {
        public override string CreateDatabase(string Name)
        {
            var path = Path.Combine(Constants.Directories.DataDirectory, Name + ".db");
            this.GetType().Assembly.SaveResourceAs(this.GetType().Namespace + ".empty.db", path);
            return "Data Source=" + path;
        }
    }
}
