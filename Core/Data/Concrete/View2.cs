﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;

namespace Core.Data.Concrete
{
    public class DatabaseView<T> : DynamicQuery2, IView2<T> where T : new()
    {
        protected string DatabaseObjectName { get; set; }
        protected IEnumerable<string> Keys { get; private set; }

        public DatabaseView(IDatabase database, string viewName, string primaryKeyField)
            : base(database)
        {
            DatabaseObjectName = viewName.IsFilled() ? viewName : typeof(T).ClassName().PrependInCase("Fwk_").TrimEnd("Table").TrimEnd("DTO");
            Keys = primaryKeyField.IsFilled() ? primaryKeyField.Split(',').Select(x => x.Trim()) : RuntimeDetectionOfKeyFields();
        }

        public DatabaseView(IDatabase database, string viewName)
            : this(database, viewName, null)
        {
        }

        public DatabaseView(IDatabase database)
            : this(database, null)
        {
        }


        private IEnumerable<string> RuntimeDetectionOfKeyFields()
        {
            return typeof(T)
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x =>
                    x.GetCustomAttributes(typeof(KeyAttribute), true)
                    .Cast<KeyAttribute>()
                    .Select(y => y.IsNotNull()).FirstOrDefault())
                .Select(x => x.Name);
        }

        public IEnumerable<T> All()
        {
            return All<T>(); ;
        }

        public IEnumerable<T> All(string orderBy)
        {
            return All<T>(orderBy); ;
        }

        public IEnumerable<T> All(int startPage, int pageSize)
        {
            return All<T>(startPage, pageSize); ;
        }

        public IEnumerable<T> All(string orderBy, int startPage, int pageSize)
        {
            return All<T>(orderBy, startPage, pageSize); ;
        }

        public IEnumerable<T> Query(string where = "", params object[] args)
        {
            return Query<T>(where, args);
        }

        public IPagedResult<T> Paged(string where = "", string orderBy = "", int pageNumber = 1, int pageSize = 20, params object[] args)
        {
            return Paged<T>(where, orderBy, pageNumber, pageSize, args);
        }

        public IEnumerable<U> All<U>() where U :  T, new()
        {
            return Query<U>("", null); ;
        }

        public IEnumerable<U> All<U>(string orderBy) where U :  T, new()
        {
            string sql = ImplementationDetails.GetSelectStatement(DatabaseObjectName, "*", "", orderBy);
            return base.Query<U>(sql, null);
        }

        public IEnumerable<U> All<U>(int startPage, int pageSize) where U :  T, new()
        {
            return Paged<U>("", "", startPage, pageSize, null).Data;
        }

        public IEnumerable<U> All<U>(string orderBy, int startPage, int pageSize) where U :  T, new()
        {
            return Paged<U>("", orderBy, startPage, pageSize, null).Data;
        }

        public IEnumerable<U> Query<U>(string where = "", params object[] args) where U :  T, new()
        {
            string sql = ImplementationDetails.GetSelectStatement(DatabaseObjectName, "*", where, "");
            return base.Query<U>(sql, args);
        }

        public IPagedResult<U> Paged<U>(string where = "", string orderBy = "", int pageNumber = 1, int pageSize = 20, params object[] args) where U : T, new()
        {
            var keyList = Keys.ToString(",");
            var recordStart = (pageNumber - 1) * pageSize;

            if (String.IsNullOrEmpty(orderBy)) orderBy = keyList;

            var countSQL = ImplementationDetails.GetCountStatement(DatabaseObjectName, where);
            var total = Scalar(countSQL, args);
            int TotalRecords = 0;

            if (total is int) TotalRecords = (int)total;
            else if (total is long) TotalRecords = Convert.ToInt32(total);

            IEnumerable<U> results;
            if (ImplementationDetails.SupportsPagedStatements)
            {
                var sql = ImplementationDetails.GetPagedStatement(DatabaseObjectName, orderBy, recordStart, pageSize, "*", where);
                results = base.Query<U>(sql, args);
            }
            else
            {
                var sql = ImplementationDetails.GetSelectStatement(DatabaseObjectName, "*", where, orderBy);
                results = base.Query<U>(sql, args).Skip(recordStart).Take(pageSize);
            }
            return new PagedResult<U>(results, pageNumber, TotalRecords);
        }
    }
}
