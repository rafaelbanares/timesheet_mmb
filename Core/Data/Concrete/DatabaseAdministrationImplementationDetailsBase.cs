﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data.Concrete
{
    public abstract class DatabaseAdministrationImplementationDetailsBase : IDatabaseAdministrationImplementationDetails
    {
        public abstract string CreateDatabase(string Name);
    }
}
