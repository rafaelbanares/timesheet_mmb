﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data.Concrete
{
    public abstract class DatabaseBase : IDatabase
    {
        public string ConnectionString { get; private set; }
        public DatabaseBase(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public abstract IDatabaseQueryingImplementationDetails QueryingImplementationDetails { get; }
        public abstract IDatabaseAdministrationImplementationDetails AdministrationImplementationDetails { get; }
    }
}
