﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data.Concrete.SqlServer
{
    public class SqlServerDatabase : DatabaseBase
    {
        public SqlServerDatabase(string connectionString)
            : base(connectionString)
        { }

        public override IDatabaseQueryingImplementationDetails QueryingImplementationDetails
        {
            get { return new SqlServerQueryingImplementationDetails(); }
        }

        public override IDatabaseAdministrationImplementationDetails AdministrationImplementationDetails
        {
            get { return new SqlServerAdministrationImplementationDetails(); }
        }
    }
}
