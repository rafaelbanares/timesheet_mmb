﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace Core.Data
{
    public interface IDatabaseQueryingImplementationDetails
    {
        bool SupportsPagedStatements { get; }
        bool SupportsUpsertStatement { get; }
        DbProviderFactory ProviderFactory { get; }
        string GetSelectStatement(string objectName, string columns = "*", string where = "", string orderBy = "");
        string GetPagedStatement(string objectName, string orderBy, int startingRecord, int recordCount, string columns = "*", string where = "");
        string GetCountStatement(string objectName, string where = "");
        string GetDeleteStatement(string objectName, string where = "");
        string GetInsertStatement(string objectName, IEnumerable<string> columnNames);
        string GetUpdateStatement(string objectName, IEnumerable<string> keyColumnNames, IEnumerable<string> updatedColumnNames);
        string GetUpsertStatement(string objectName, IEnumerable<string> keyColumnNames, IEnumerable<string> editableColumnNames);
    }
}
