﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data
{
    public interface IPagedResult<out T>
    {
        IEnumerable<T> Data { get; }
        int PageNumber { get;  }
        int TotalRecords { get; }
        int FirstRecordIndex { get; }
        int PageSize { get; }
    }

    public class PagedResult<T> : IPagedResult<T>
    {
        public PagedResult(IEnumerable<T> data, int pageNumber, int totalRecords)
        {
            Data = data;
            PageNumber = pageNumber;
            TotalRecords = totalRecords;
        }

        public IEnumerable<T> Data
        {
            get;
            private set;
        }

        public int PageNumber
        {
            get;
            private set;
        }

        public int TotalRecords
        {
            get;
            private set;
        }

        public int FirstRecordIndex
        {
            get { return (PageNumber - 1) * PageSize; }
        }

        public int PageSize
        {
            get { return Data.Count(); }
        }
    }
}
