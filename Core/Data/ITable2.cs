﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data
{
    public interface ITable2<T> : IView2<T> 
    {
        U Update<U>(U o) where U : T, new();
        U Insert<U>(U o) where U : T, new();
        U Save<U>(U o) where U : T, new();
        void Delete(object o);
        void Delete(string where = "", params object[] args);
        void DeleteAll();
    }
}
