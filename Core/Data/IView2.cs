﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Data
{
    public interface IView2<T>
    {
        IEnumerable<T> All();
        IEnumerable<T> All(string orderBy);
        IEnumerable<T> All(int startPage, int pageSize);
        IEnumerable<T> All(string orderBy, int startPage, int pageSize);
        IEnumerable<T> Query(string where = "", params object[] args);
        IPagedResult<T> Paged(string where = "", string orderBy = "", int pageNumber = 1, int pageSize = 20, params object[] args);

        IEnumerable<U> All<U>() where U :  T, new();
        IEnumerable<U> All<U>(string orderBy) where U :  T, new();
        IEnumerable<U> All<U>(int startPage, int pageSize) where U : T, new();
        IEnumerable<U> All<U>(string orderBy, int startPage, int pageSize) where U : T, new();
        IEnumerable<U> Query<U>(string where = "", params object[] args) where U : T, new();
        IPagedResult<U> Paged<U>(string where = "", string orderBy = "", int pageNumber = 1, int pageSize = 20, params object[] args) where U :T, new();
    }
}
