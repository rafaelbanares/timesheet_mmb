﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Castle.DynamicProxy;

namespace Core.Plugins
{
    public class PlugInRemoteController<T> : MarshalByRefObject
    where T : class
    {
        public PlugInRemoteController() { }

        public T BridgeEnd = default(T);
        public T localProxy;
        public void Load(string pluginTypeFullName)
        {
            var t = AssemblyHelper.GetType(pluginTypeFullName);
            var match = t.GetMatchingTypes().Where(u => !u.IsInterface && !u.IsAbstract).FirstOrDefault();
            BridgeEnd = (T)match.GetOne();

            ProxyGenerator gen = new ProxyGenerator();
            localProxy = gen.CreateInterfaceProxyWithoutTarget<T>(new Interceptor2<T>() { BridgeEnd = this });

        }

        public object Invoke(IInvocation invocation)
        {
            var i = invocation as IChangeProxyTarget;
            i.ChangeInvocationTarget(BridgeEnd);


            var t = localProxy as IProxyTargetAccessor;
            var y = t.DynProxyGetTarget();
            //var changeProxyTarget = invocation as IChangeProxyTarget;
            //changeProxyTarget.ChangeInvocationTarget(localProxy);
            //invocation.Proceed();
            return invocation.ReturnValue;
        }

    }
    public class PlugInBridge<T>
    where T : class
    {
        internal PlugInBridge(string folder, Type pluginType, AppDomain domain, PlugInRemoteController<T> controller)
        {
            BridgedType = pluginType;
            Folder = folder;
            Domain = domain;
            RemoteController = controller;
            Name = domain.FriendlyName;
        }

        public string Name { get; internal set; }
        public string Folder { get; internal set; }
        public AppDomain Domain { get; internal set; }
        public Type BridgedType { get; internal set; }
        internal PlugInRemoteController<T> RemoteController;

        public T LocalController;

        public static PlugInBridge<T> CreateFromFolder(string name, string folder)
        {
            var pluginType = typeof(T);

            AppDomainSetup ads = new AppDomainSetup();
            var f = new FileInfo(folder.AppendInCase("\\"));
            ads.ApplicationBase = f.Directory.FullName;
            ads.PrivateBinPath = f.Directory.FullName;
            ads.DisallowBindingRedirects = false;
            ads.DisallowCodeDownload = true;
            ads.ShadowCopyDirectories = f.Directory.FullName;
            ads.ShadowCopyFiles = "true";

            var domain = AppDomain.CreateDomain(name, null, ads);

            PlugInRemoteController<T> loader = (PlugInRemoteController<T>)domain.CreateInstanceAndUnwrap(typeof(PlugInRemoteController<T>).Assembly.FullName, typeof(PlugInRemoteController<T>).FullName);
            loader.Load(pluginType.FullName);


            var infos = new PlugInBridge<T>(folder, pluginType, domain, loader);

            ProxyGenerator gen = new ProxyGenerator();
            infos.LocalController = gen.CreateInterfaceProxyWithoutTarget<T>(new Interceptor<T>() { Bridge = infos });

            return infos;
        }
    }
    [Serializable]
    public class Interceptor<T> : IInterceptor
         where T : class
    {
        public PlugInBridge<T> Bridge { get; set; }

        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Before local call");
            invocation.ReturnValue = Bridge.RemoteController.Invoke(invocation);
            Console.WriteLine("After local call");
        }
    }
    [Serializable]
    public class Interceptor2<T> : IInterceptor
         where T : class
    {
        public PlugInRemoteController<T> BridgeEnd { get; set; }

        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Before distant call");
            var t = invocation as IChangeProxyTarget;
            t.ChangeInvocationTarget(BridgeEnd.BridgeEnd);
            Console.WriteLine("After distant call");
        }
    }
}
