﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Plugins
{
    public class PlugInLocalController<T> : IControllersContract
        where T:class
    {
        PlugInBridge<T> DomainInfos { get; set; }

        internal PlugInLocalController(PlugInBridge<T> domainInfos)
        {
            DomainInfos = domainInfos;
        }

        public T Get<T>(string memberName)
        {
            return DomainInfos.RemoteController.Get<T>(memberName);
        }

        public void Set(string memberName, object value)
        {
            DomainInfos.RemoteController.Set(memberName, value);
        }

        public object CallMethod(string voidName, object[] args)
        {
            return DomainInfos.RemoteController.CallMethod(voidName, args);
        }
    }
}
