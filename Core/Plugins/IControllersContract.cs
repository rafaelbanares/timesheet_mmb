﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.DynamicProxy;

namespace Core.Plugins
{
    interface IControllersContract
    {
        T Get<T>(string memberName);
        void Set(string memberName, object value);
        object CallMethod(string voidName, object[] args);
        
    }
}
