﻿using System;
using System.Linq;
using System.Reflection;
using Castle.DynamicProxy;

namespace Core.Plugins
{
    public class PlugInRemoteController : MarshalByRefObject
    {
        public PlugInRemoteController() { }

        public object BridgeEnd = null;

        public void Load(string pluginTypeFullName)
        {
            var t = AssemblyHelper.GetType(pluginTypeFullName);
            var match = t.GetMatchingTypes().Where(u => !u.IsInterface && !u.IsAbstract).FirstOrDefault();
            BridgeEnd = match.GetOne();
        }


        public object Invoke(IInvocation invocation)
        {
            MethodInfo mi = invocation.Method;
            var obj = mi.Invoke(BridgeEnd, invocation.Arguments);
            return obj;
        }
    }
}
