﻿using System;
using System.IO;
using Castle.DynamicProxy;

namespace Core.Plugins
{
    public class PlugIn<T>
        where T : class
    {
        public string Name { get; internal set; }
        public string Folder { get; internal set; }
        public AppDomain Domain { get; internal set; }
        public Type BridgedType { get; internal set; }
        PlugInRemoteController RemoteController;

        PlugIn(string name, string folder, Type pluginType, AppDomain domain)
        {
            Name = name;
            Folder = folder;
            BridgedType = pluginType;
            Domain = domain;

            RemoteController = (PlugInRemoteController)domain.CreateInstanceAndUnwrap(typeof(PlugInRemoteController).Assembly.FullName, typeof(PlugInRemoteController).FullName);
            RemoteController.Load(pluginType.FullName);

            ProxyGenerator px = new ProxyGenerator();
            Proxy = px.CreateInterfaceProxyWithoutTarget<T>(new Interceptor(RemoteController));

        }

        public T Proxy { get; internal set; }

        public static PlugIn<T> CreateFromFolder(string name, string folder)
        {
            folder = folder.AppendInCase("\\");

            var pluginType = typeof(T);
            
            AppDomainSetup ads = new AppDomainSetup();
            var f = new FileInfo(folder);
            ads.ApplicationBase = f.Directory.FullName;
            ads.ApplicationName = name;
            ads.CachePath = @"c:\Temp\FwkModules";
            ads.PrivateBinPathProbe = "True";
            ads.PrivateBinPath = f.Directory.FullName;

            AppDomain.CurrentDomain.SetupInformation.PrivateBinPath = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath.AppendInCase(";").Append(f.Directory.FullName);
            ads.DisallowBindingRedirects = false;
            ads.DisallowCodeDownload = false;

            ads.ShadowCopyDirectories = f.Directory.FullName;
            ads.ShadowCopyFiles = "True";
            
            

            var domain = AppDomain.CreateDomain(name, null, ads);

            var infos = new PlugIn<T>(name, folder, pluginType, domain);

            return infos;
        }

        class Interceptor : IInterceptor
        {
            PlugInRemoteController RemoteController;

            public Interceptor(PlugInRemoteController controller)
            {
                RemoteController = controller;
            }
            #region IInterceptor Members

            public void Intercept(IInvocation invocation)
            {
                var res = RemoteController.Invoke(invocation);
                invocation.ReturnValue = res;
            }

            #endregion
        }
    }
}
