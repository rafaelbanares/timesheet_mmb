﻿namespace Core.Documents.Word
{
    /// <summary>
    /// Docx Namespaces
    /// </summary>
    public class DocxNamespaces
    {
        public static readonly string W = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        public static readonly string VE = "http://schemas.openxmlformats.org/markup-compatibility/2006";
        public static readonly string O = "urn:schemas-microsoft-com:office:office";
        public static readonly string R = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
        public static readonly string M = "http://schemas.openxmlformats.org/officeDocument/2006/math";
        public static readonly string V = "urn:schemas-microsoft-com:vml";
        public static readonly string WP = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";
        public static readonly string w10 = "urn:schemas-microsoft-com:office:word";
    }
}