﻿using System;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Core.Documents.Word
{
    /// <summary>
    /// Custom Xml Field Node Class
    /// </summary>
    public class Field : CustomXmlBase
    {
        /// <summary>
        /// Gets the value element.
        /// </summary>
        /// <value>The value element.</value>
        public XElement ValueElement { get; private set; }

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>The key.</value>
        public string Key { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public object Value { get; private set; }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="Field"/> class.
        /// </summary>
        /// <param name="element">The element.</param>
        public Field(XElement element)
            : base(element)
        {
            if (element.LastAttribute.Value != "Field")
                throw new ArgumentException("Value of attribute 'element' must be 'Field'.", "element");

            ValueElement = element.XPathSelectElement(".//w:t", Namespace);
            if (ValueElement == null)
                return;

            Key = ValueElement.Value;

            if (!Key.IsFilled())
                return;
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <see cref="Field"/> class.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="frameworkDatabase">The frameworkDatabase.</param>
        public Field(XElement element, object data)
            : base(element)
        {
            if (element.LastAttribute.Value != "Field")
                throw new ArgumentException("Value of attribute 'element' must be 'Field'.", "element");

            var isSimpleType = data.GetType().IsPrimitive || data.GetType().Equals(typeof (string));

            ValueElement = element.XPathSelectElement(".//w:t", Namespace);
            if (ValueElement == null)
                return;

            Key = ValueElement.Value;
            
            if (!Key.IsFilled())
                return;

            Value = isSimpleType ? data : data.GetValue<string>(Key);

            ValueElement.Value = Value == null ? string.Empty : (string) Value;
        }
    }
}