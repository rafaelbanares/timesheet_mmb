﻿
namespace Core.Documents.Internal
{
    class OfficeComment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }
    }
}
