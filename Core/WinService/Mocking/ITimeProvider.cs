using System;

namespace Core.WinService
{
    public interface ITimeProvider
    {
        DateTime Now { get; }
    }
}