﻿using System.Collections;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace Core.WinService
{
    internal static class WinServiceInstaller
    {
        public static void Install(ServiceInfo serviceInfo)
        {
            Install(true, serviceInfo);
        }

        public static void UnInstall(ServiceInfo serviceInfo)
        {
            Install(false, serviceInfo);
        }

        private static Installer CreateInstaller(ServiceInfo serviceInfo)
        {
            var installer = new Installer();

            var serviceInstaller = new ServiceInstaller();
            var serviceProcessInstaller = new ServiceProcessInstaller();

            serviceInstaller.Description = serviceInfo.Description;
            serviceInstaller.StartType = serviceInfo.ServiceStartMode;
            serviceInstaller.DisplayName = serviceInfo.DisplayName;
            serviceInstaller.ServiceName = serviceInfo.ServiceName;

            if (serviceInfo.DependsOn != null && serviceInfo.DependsOn.Length > 0)
                serviceInstaller.ServicesDependedOn = serviceInfo.DependsOn;

            serviceProcessInstaller.Account = serviceInfo.ServiceAccount;
            serviceProcessInstaller.Username = serviceInfo.UserName;
            serviceProcessInstaller.Password = serviceInfo.Password;

            installer.Installers.Add(serviceProcessInstaller);
            installer.Installers.Add(serviceInstaller);

            return installer;
        }

        private static void Install(bool install, ServiceInfo serviceInfo)
        {
            using (var transactedInstaller = new TransactedInstaller())
            {
                using (var installer = CreateInstaller(serviceInfo))
                {
                    transactedInstaller.Installers.Add(installer);

                    var path = string.Format("/assemblypath={0}", Assembly.GetEntryAssembly().Location);

                    transactedInstaller.Context = new InstallContext("", new[] {path});

                    if (install)
                        transactedInstaller.Install(new Hashtable());
                    else
                        transactedInstaller.Uninstall(null);
                }
            }
        }
    }
}