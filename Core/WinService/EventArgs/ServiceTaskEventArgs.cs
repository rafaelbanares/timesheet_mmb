using System;

namespace Core.WinService
{
    public class ServiceTaskEventArgs : EventArgs
    {
        public ServiceTask ServiceTask { get; set; }

        public ServiceTaskEventArgs(ServiceTask serviceTask)
        {
            ServiceTask = serviceTask;
        }
    }
}