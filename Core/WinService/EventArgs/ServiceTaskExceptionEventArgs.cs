using System;

namespace Core.WinService
{
    public class ServiceTaskExceptionEventArgs : EventArgs
    {
        public ServiceTask ServiceTask { get; set; }
        public Exception Exception { get; set; }

        public ServiceTaskExceptionEventArgs(ServiceTask serviceTask, Exception e)
        {
            ServiceTask = serviceTask;
            Exception = e;
        }
    }
}