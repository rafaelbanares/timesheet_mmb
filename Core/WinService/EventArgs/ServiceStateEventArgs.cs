using System;

namespace Core.WinService
{
    public class ServiceStateEventArgs : EventArgs
    {
        public ServiceTask ServiceTask { get; set; }
        public ServiceState State { get; set; }

        public ServiceStateEventArgs(ServiceTask serviceTask, ServiceState state)
        {
            State = state;
            ServiceTask = serviceTask;
        }
    }
}