namespace Core.WinService.Scheduling.Cron
{
    public class DaysOfWeekCronEntry : CronEntryBase
    {
        public DaysOfWeekCronEntry(string expression)
        {
            Initialize(expression, 0, 6);
        }
    }
}