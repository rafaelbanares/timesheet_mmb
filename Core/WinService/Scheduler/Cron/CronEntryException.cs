using System;

namespace Core.WinService.Scheduling.Cron
{
    public class CronEntryException : Exception
    {
        public CronEntryException(string message)
            : base(message)
        {
        }
    }
}