using System;

namespace Core.WinService.Scheduling
{
    public class CyclicScheduler : Scheduler
    {
        public CyclicScheduler(string scheduleId, TimeSpan interval) : base(scheduleId)
        {
            Interval = interval;
        }

        public TimeSpan Interval { get; set; }

        public override bool ShouldRun()
        {
            var lastRun = LastRun;

            if ((TimeProvider.Now - lastRun) >= Interval)
            {
                LastRun = TimeProvider.Now;
                return true;
            }

            return false;
        }
    }
}