using System;

namespace Core.WinService.Scheduling
{
    public interface IScheduleHistoryStore
    {
        DateTime LastRun(string taskId);
        void SetLastRun(string taskId, DateTime lastRun);
    }
}