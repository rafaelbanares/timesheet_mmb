using System;

namespace Core.WinService.Scheduling
{
    [Flags]
    public enum ScheduleWeekDays
    {
        Sunday = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6
    }
}