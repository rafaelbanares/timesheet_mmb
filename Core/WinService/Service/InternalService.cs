using System.ServiceProcess;

namespace Core.WinService
{
    internal class InternalService : ServiceBase
    {
        private readonly Service _service;

        public InternalService(Service service)
        {
            _service = service;

            ServiceName = service.ServiceInfo.ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            _service.StartServiceTasks();
        }

        protected override void OnStop()
        {
            _service.StopServiceTasks();
        }

        protected override void OnPause()
        {
        }

        protected override void OnContinue()
        {
        }
    }
}