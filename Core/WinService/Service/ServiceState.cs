namespace Core.WinService
{
    public enum ServiceState
    {
        Starting,
        Started,
        Stopping,
        Stopped,
        TaskStarting,
        TaskStarted,
        TaskStopping,
        TaskStopped
    }
}