using System;
using Core.WinService.Scheduling;

namespace Core.WinService
{
    public abstract class CyclicServiceTask : ScheduledServiceTask
    {
        protected CyclicServiceTask(int intervalSeconds, bool synchronous)
            : this(TimeSpan.FromSeconds(intervalSeconds), synchronous)
        {
        }

        protected CyclicServiceTask(TimeSpan interval, bool synchronous)
            : base(new CyclicScheduler(null, interval), synchronous)
        {
        }
    }
}