using System;
using System.Threading;

namespace Core.WinService
{
    public abstract class ServiceTask
    {
        public event EventHandler<ServiceTaskExceptionEventArgs> ExceptionOccurred;

        private Thread _thread;

        private bool _synchronous;
        private object _syncObject;

        public virtual bool Synchronous
        {
            get { return _synchronous; }
            set { _synchronous = value; }
        }

        public bool IsRunning { get; private set; }

        public virtual bool StopRequested { get; set; }

        protected abstract void RunTask();

        protected void OnException(Exception e)
        {
        }

        protected ServiceTask(bool synchronous)
        {
            _synchronous = synchronous;
        }

        public void Start(object syncObject)
        {
            _syncObject = syncObject;
            _thread = new Thread(Run) {IsBackground = true};
            StopRequested = false;
            _thread.Start();
        }

        public void Stop()
        {
            StopRequested = true;
        }

        public void WaitUntilFinished()
        {
            if(_thread != null) _thread.Join(5000);
        }

        protected virtual bool WaitForNextRun()
        {
            while (!StopRequested)
                Thread.Sleep(100);

            return false;
        }

        private void Run()
        {
            IsRunning = true;

            while (!StopRequested)
            {
                if (!WaitForNextRun())
                    continue;

                try
                {
                    if (Synchronous)
                    {
                        lock (_syncObject)
                        {
                            RunTask();
                        }
                    }
                    else
                    {
                        RunTask();
                    }
                }
                catch (Exception e)
                {
                    OnException(e);

                    if (ExceptionOccurred != null)
                        ExceptionOccurred(this, new ServiceTaskExceptionEventArgs(this, e));
                }
            }

            IsRunning = false;
        }
    }
}