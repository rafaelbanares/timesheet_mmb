using System.Threading;
using Core.WinService.Scheduling;

namespace Core.WinService
{
    public abstract class ScheduledServiceTask : ServiceTask
    {
        private readonly Scheduler _scheduler;

        protected ScheduledServiceTask(Scheduler schedule, bool synchronous) : base(synchronous)
        {
            _scheduler = schedule;
        }

        public Scheduler Scheduler
        {
            get { return _scheduler; }
        }

        protected override bool WaitForNextRun()
        {
            while (!StopRequested)
            {
                if (Scheduler.ShouldRun())
                    return true;

                Thread.Sleep(100);
            }

            return false;
        }
    }
}