﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Runtime.Serialization;

namespace Core.Features
{
    [Serializable]
    public class DeleteException : FrameworkException
    {
        public string ObjectName { get; set; }

        public DeleteException(string objectName)
        {
            ObjectName = objectName;
        }

        public DeleteException(string objectName, string message)
            : base(message)
        {
            ObjectName = objectName;
        }

        public DeleteException(string objectName, string message, Exception inner)
            : base(message, inner)
        {
            ObjectName = objectName;
        }
    }
}
