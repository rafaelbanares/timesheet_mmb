﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Model;
using System.Runtime.Serialization;

namespace Core.Features
{
    [Serializable]
    public class SaveException : FrameworkException
    {
        public string ObjectName { get; set; }

        public SaveException(string objectName)
        {
            ObjectName = objectName;
        }

        public SaveException(string objectName, string message)
            : base(message)
        {
            ObjectName = objectName;
        }

        public SaveException(string objectName, string message, Exception inner)
            : base(message, inner)
        {
            ObjectName = objectName;
        }
    }
}
