﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [Serializable]
    public class FrameworkException : Exception
    {
        /// <summary>
        /// Message that will be displayed and localized in UI
        /// </summary>
        public string UserMessage { get; set; }

        public FrameworkException() {}

        public FrameworkException(string messageToBeLogged, string messageToBeDisplayed) : base(messageToBeLogged)
        {
            UserMessage = messageToBeDisplayed;
        }

        public FrameworkException(string messageToBeLogged, string messageToBeDisplayed, Exception inner) : base(messageToBeLogged, inner) 
        {
            UserMessage = messageToBeDisplayed;
        }

        public FrameworkException(string message) : this(message, message) { }

        public FrameworkException(string message, Exception inner) : this(message, message, inner) { }
    }
}