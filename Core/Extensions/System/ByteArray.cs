﻿
using System.IO;
using Core;
namespace System
{
    public static class ByteArrayExtensions
    {
        public static MemoryStream ToMemoryStream(this byte[] item)
        {
            var mem = new MemoryStream();
            mem.Write(item, 0, item.Length);
            return mem;
        }


        public static string ToReadable(this byte[] value)
        {
            return System.Text.Encoding.UTF8.GetString(value);
        }

        public static void SaveAs(this byte[] value, string path)
        {
           File.WriteAllBytes(path, value);
        }

        public static byte[] Compress(this byte[] value)
        {
            return ZipHelper.Zip(value);
        }

        public static byte[] DeCompress(this byte[] value)
        {
            return ZipHelper.UnZip(value);
        }
    }
}
