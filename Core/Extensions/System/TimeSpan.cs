﻿namespace System
{
    public static class TimeSpanExtensions
    {
        /// <summary>
        /// Multiply the specified TimeSpan value by the provided multiplicator
        /// </summary>
        /// <param name="value">The TimeSpan value to be multiply</param>
        /// <param name="multiplicator">The number to multiply the TimeSpan by</param>
        /// <returns></returns>
        public static TimeSpan Multiply(this TimeSpan value, int multiplicator)
        {
            return TimeSpan.FromTicks(value.Ticks * multiplicator);
        }

        public static string Format(this TimeSpan value)
        {
            var result = string.Empty;
            bool daysSet = (value.Days != 0);
            bool hoursSet = (value.Hours != 0);
            bool minutesSet = (value.Minutes != 0);
            bool secondsSet = (value.Seconds != 0);

            if (daysSet) result += " " + value.Days + " day" + (value.Days > 1 ? "s" : "");
            if (hoursSet || (daysSet && (minutesSet || secondsSet))) result += " " + value.Hours + " hour" + (value.Hours > 1 ? "s" : "");
            if (minutesSet || ((daysSet || hoursSet) && (secondsSet))) result += " " + value.Minutes + " minute" + (value.Minutes > 1 ? "s" : "");
            if (secondsSet) result += " " + value.Seconds + " second" + (value.Seconds > 1 ? "s" : "");

            return result.Trim();
        }

    }
}