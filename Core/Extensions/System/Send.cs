﻿#region Copyright (c) 2009, Andre Loker <mail@andreloker.de>
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public static class ObjectSendExtensions
{
    /// <summary>
    /// Invokes a method on the <paramref name="target"/>
    /// </summary>
    /// <param name="target">The target, must not be <c>null</c></param>
    /// <param name="methodName">Name of the method, must not be <c>null</c></param>
    /// <param name="args">The arguments passed to the method.</param>
    /// <remarks>
    /// If the target type contains multiple overload of the given <paramref name="methodName"/>
    /// <see cref="Send"/> tries to find the best match.
    /// </remarks>
    /// <exception cref="ArgumentException">
    /// No method with the given <paramref name="methodName"/> was found or the invocation
    /// is ambiguous, ie. multiple methods match.
    /// </exception>
    public static object Send(this object target, string methodName, params object[] args)
    {
        return Send<object>(target, methodName, args);
    }

    /// <summary>
    /// Invokes a method on the <paramref name="target"/>
    /// </summary>
    /// <param name="target">The target, must not be <c>null</c></param>
    /// <param name="methodName">Name of the method, must not be <c>null</c></param>
    /// <param name="args">The arguments passed to the method.</param>
    /// <remarks>
    /// If the target type contains multiple overload of the given <paramref name="methodName"/>
    /// <see cref="Send"/> tries to find the best match.
    /// </remarks>
    /// <exception cref="ArgumentException">
    /// No method with the given <paramref name="methodName"/> was found or the invocation
    /// is ambiguous, ie. multiple methods match.
    /// </exception>
    /// <returns>The value returned from the invoked method cast to a 
    /// <typeparamref name="T"/>
    /// </returns>
    public static T Send<T>(this object target, string methodName, params object[] args)
    {
        if (target == null)
        {
            throw new ArgumentNullException("target");
        }

        if (methodName == null)
        {
            throw new ArgumentNullException("methodName");
        }

        var type = target.GetType();
        var methods = GetMethodCandidates(methodName, type);
        var methodToInvoke = FindBestFittingMethod(methods, args);
        return InvokeFunction<T>(target, methodToInvoke, args);
    }

    static IEnumerable<MethodInfo> GetMethodCandidates(string methodName, Type type)
    {
        return from method in type.GetMethods(BindingFlags.Public | BindingFlags.Instance)
               where method.Name == methodName
               select method;
    }

    static MethodInfo FindBestFittingMethod(IEnumerable<MethodInfo> methods, object[] args)
    {
        var highestScore = -1;
        var matchingMethodCount = 0;
        MethodInfo selectedMethod = null;

        foreach (var method in methods)
        {
            var methodScore = RateMethodMatch(method.GetParameters(), args);
            if (methodScore > highestScore)
            {
                matchingMethodCount = 1;
                highestScore = methodScore;
                selectedMethod = method;
            }
            else if (methodScore == highestScore)
            {
                // count the number of matches, match count > 1 => ambiguous call
                matchingMethodCount++;
            }
        }

        if (matchingMethodCount > 1)
        {
            throw new ArgumentException("Ambiguous method invocation");
        }
        return selectedMethod;
    }
    /// <returns>0 if the arguments don't match the parameters; a score > 0 otherwise.</returns>
    static int RateMethodMatch(ParameterInfo[] parameters, object[] args)
    {
        var argsLength = args != null ? args.Length : 0;
        if (parameters.Length == argsLength)
        {
            return argsLength == 0 ? 1 : RateParameterMatches(parameters, args);
        }
        return 0;
    }

    static int RateParameterMatches(ParameterInfo[] parameters, object[] args)
    {
        var score = 0;
        for (var i = 0; i < args.Length; ++i)
        {
            var typeMatchScore = RateParameterMatch(parameters[i], args[i]);
            if (typeMatchScore == 0)
            {
                return 0;
            }
            score += typeMatchScore;
        }
        return score;
    }
    static int RateParameterMatch(ParameterInfo parameter, object arg)
    {
        var parameterType = parameter.ParameterType;
        return arg == null ? RateNullArgument(parameterType) : RateNonNullArgument(arg, parameterType);
    }

    static int RateNullArgument(Type parameterType)
    {
        return CanBeNull(parameterType) ? 1 : 0;
    }

    static bool CanBeNull(Type type)
    {
        return !type.IsValueType || IsNullableType(type);
    }

    static bool IsNullableType(Type type)
    {
        return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
    }

    static int RateNonNullArgument(object arg, Type parameterType)
    {
        var argType = arg.GetType();
        if (argType == parameterType)
        {
            // perfect match!
            return 2;
        }
        if (parameterType.IsAssignableFrom(argType))
        {
            // at least convertible to parameter type
            return 1;
        }
        return 0;
    }

    static T InvokeFunction<T>(object target, MethodInfo method, object[] args)
    {
        if (method == null)
        {
            throw new ArgumentException("Method not found");
        }
        return (T)method.Invoke(target, args);
    }
}