﻿using System.Diagnostics;
using Core;

namespace System
{
    /// <summary>
    /// Extension methods for exception.
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Formats the specified exception.
        /// Useful to log an exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns></returns>
        public static string Format(this Exception ex)
        {
            return ExceptionFormatter.Format(ex);
        }

        /// <summary>
        /// Logs the specified exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <param name="title">The title.</param>
        public static void LogToTrace(this Exception ex)
        {
            Trace.WriteLine("");
            Trace.WriteLine(String.Format("Title: {0}" + Environment.NewLine + "Exception: {1}", ex.Message, ex.Format()));
            Trace.WriteLine("");
        }

        /// <summary>
        /// Returns the explicit <see cref="System.Exception"/> that is the root cause of an exception.
        /// </summary>
        /// <remarks>
        /// If the InnerException property of the current exception is a null reference 
        /// or a <see cref="System.NullReferenceException"/>, returns the current exception.
        /// </remarks>
        /// <param name="ex">The last exception thrown.</param>
        /// <returns>
        /// The first explicit exception thrown in a chain of exceptions.
        /// </returns>
        public static Exception GetExplicitBaseException(this Exception ex)
        {
            Exception innerEx = ex.InnerException;
            while (innerEx != null && !(innerEx is NullReferenceException))
            {
                ex = innerEx;
                innerEx = innerEx.InnerException;
            }
            return ex;
        }
    }
}