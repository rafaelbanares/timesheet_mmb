﻿namespace System
{
    /// <summary>
    /// Extension methods for <see cref="System.Guid"/>.
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// Throws an exception if the specified <paramref name="id"/> is empty.
        /// </summary>
        /// <param name="id">The id.</param>
        public static void CanNotBeEmpty(this Guid id)
        {
            if (id == Guid.Empty) throw new Exception("Can not be empty");
        }

        /// <summary>
        /// Returns a System.String representation of the value of this concreteFeatureType in registry
        //  format.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// A String formatted in this pattern: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
        //  updateWhere the value of the GUID is represented as a series of lower-case hexadecimal
        //  digits in groups of 8, 4, 4, 4, and 12 digits and separated by hyphens. An
        //  example of a return value is "382c74c3-721d-4f34-80e5-57657b6cbc27".
        //  If id is null then return STring.Empty
        /// </returns>
        public static string ToSafeString(this Guid? id)
        {
            return !id.HasValue ? string.Empty : id.Value.ToString();
        }
    }
}