﻿using System.Linq.Expressions;
using Core;
namespace System.Linq
{
    /// <summary>
    /// Extension methods for IQueryable.
    /// </summary>
    public static class QueryableExtensions
    {
        /// <summary>
        /// Returns a subset of elements that satisfy the given condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="predicate">The predicate.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static IQueryable<T> Where<T>(this IQueryable<T> source, string predicate, params object[] values)
        {
            return (IQueryable<T>) Where((IQueryable) source, predicate, values);
        }

        /// <summary>
        /// Returns a subset of elements that satisfy the given condition.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="predicate">The predicate.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static IQueryable Where(this IQueryable source, string predicate, params object[] values)
        {
            source.CanNotBeNull();
            predicate.CanNotBeEmpty();

            var lambda = Core.DynamicExpression.ParseLambda(source.ElementType, typeof (bool), predicate, values);

            return source.Provider.CreateQuery(
                Expression.Call(
                    typeof (Queryable), "Where",
                    new[] {source.ElementType},
                    source.Expression, Expression.Quote(lambda)));
        }

        /// <summary>
        /// Transforms each input element.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static IQueryable Select(this IQueryable source, string selector, params object[] values)
        {
            source.CanNotBeNull();
            selector.CanNotBeEmpty();

            var lambda = Core.DynamicExpression.ParseLambda(source.ElementType, null, selector, values);

            return source.Provider.CreateQuery(
                Expression.Call(
                    typeof (Queryable), "Select",
                    new[] {source.ElementType, lambda.Body.Type},
                    source.Expression, Expression.Quote(lambda)));
        }

        /// <summary>
        /// Sorts the sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="ordering">The ordering.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string ordering, params object[] values)
        {
            return (IQueryable<T>) OrderBy((IQueryable) source, ordering, values);
        }

        /// <summary>
        /// Sorts the sequence.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="ordering">The ordering.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static IQueryable OrderBy(this IQueryable source, string ordering, params object[] values)
        {
            source.CanNotBeNull();
            ordering.CanNotBeEmpty();

            var parameters = new[]
                                 {
                                     Expression.Parameter(source.ElementType, "")
                                 };

            var parser = new ExpressionParser(parameters, ordering, values);
            var orderings = parser.ParseOrdering();
            var queryExpr = source.Expression;

            var methodAsc = "OrderBy";
            var methodDesc = "OrderByDescending";

            foreach (var o in orderings)
            {
                queryExpr = Expression.Call(
                    typeof (Queryable), o.Ascending ? methodAsc : methodDesc,
                    new[] {source.ElementType, o.Selector.Type},
                    queryExpr, Expression.Quote(Expression.Lambda(o.Selector, parameters)));

                methodAsc = "ThenBy";
                methodDesc = "ThenByDescending";
            }

            return source.Provider.CreateQuery(queryExpr);
        }

        /// <summary>
        /// Returns the first <paramref name="count"/> elements and discards
        /// the rest.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        public static IQueryable Take(this IQueryable source, int count)
        {
            source.CanNotBeNull();
            return source.Provider.CreateQuery(
                Expression.Call(
                    typeof (Queryable), "Take",
                    new[] {source.ElementType},
                    source.Expression, Expression.Constant(count)));
        }

        /// <summary>
        /// Ignores the first <paramref name="count"/> elements and returns the rest.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        public static IQueryable Skip(this IQueryable source, int count)
        {
            source.CanNotBeNull();
            return source.Provider.CreateQuery(
                Expression.Call(
                    typeof (Queryable), "Skip",
                    new[] {source.ElementType},
                    source.Expression, Expression.Constant(count)));
        }

        /// <summary>
        /// Groups a sequence into subsequences.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <param name="elementSelector">The element selector.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static IQueryable GroupBy(this IQueryable source, string keySelector, string elementSelector,
                                         params object[] values)
        {
            source.CanNotBeNull();
            keySelector.CanNotBeEmpty();
            elementSelector.CanNotBeEmpty();

            var keyLambda = Core.DynamicExpression.ParseLambda(source.ElementType, null, keySelector, values);
            var elementLambda = Core.DynamicExpression.ParseLambda(source.ElementType, null, elementSelector, values);

            return source.Provider.CreateQuery(
                Expression.Call(
                    typeof (Queryable), "GroupBy",
                    new[] {source.ElementType, keyLambda.Body.Type, elementLambda.Body.Type},
                    source.Expression, Expression.Quote(keyLambda), Expression.Quote(elementLambda)));
        }

        /// <summary>
        /// Returns <c>true</c> if any elements satisfy the expression.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static bool Any(this IQueryable source)
        {
            source.CanNotBeNull();
            return (bool) source.Provider.Execute(
                Expression.Call(
                    typeof (Queryable), "Any",
                    new[] {source.ElementType}, source.Expression));
        }

        /// <summary>
        /// Returns the number of elements in the input sequence.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static int Count(this IQueryable source)
        {
            source.CanNotBeNull();
            return (int)source.Provider.Execute(
                Expression.Call(
                    typeof(Queryable), "Count",
                    new[] { source.ElementType }, source.Expression));
        }
    }
}