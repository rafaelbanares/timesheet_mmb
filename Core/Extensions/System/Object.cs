﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using System.IO;
using System.ComponentModel.DataAnnotations;
using Core.Helpers;


namespace System
{
    /// <summary>
    /// Extension methods for object.
    /// </summary>
    public static class ObjectExtensions
    {
        public static void WireEventsTo(this object from, object to)
        {
            var events = to
                .GetType()
                    .GetEvents(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public)
                    .Select(x => x.Name)
                    .Intersect(from.GetType().GetEvents().Select(x => x.Name));

                events.ForEach(x =>
                {
                    FieldInfo fi = from.GetType().GetField(x, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
                    var ei = to.GetType().GetEvent(x);
                    if (fi.IsNotNull())
                    {
                        Delegate del = (Delegate)fi.GetValue(from);
                        if (del.IsNotNull())
                        {
                            del.GetInvocationList().ForEach(d => ei.AddEventHandler(to, del));
                        }
                    }
                }
                );
        }

        public static T ForceCast<T>(this object o) where T : class, new()
        {
            return Core.Helpers.DynamicQuery.To<T>(o.ToDictionary());
        }

        /// <summary>
        /// Turns the object into an ExpandoObject
        /// </summary>
        public static dynamic ToExpando(this object o, bool excludeReadOnlyProperties = false)
        {
            var result = new ExpandoObject();
            var d = result as IDictionary<string, object>; //work with the Expando as a Dictionary
            if (o.GetType() == typeof(ExpandoObject)) return o; //shouldn't have to... but just in case
            if (o.GetType() == typeof(NameValueCollection))
            {
                var nv = (NameValueCollection)o;
                nv.Cast<string>().Select(key => new KeyValuePair<string, object>(key, nv[key])).ToList().ForEach(i => d.Add(i));
            }
            else
            {
                var props = o.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(x => !excludeReadOnlyProperties || x.CanWrite);

                foreach (var item in props)
                {
                    d.Add(item.Name, item.GetValue(o, null));
                }
            }
            return result;
        }
        /// <summary>
        /// Turns the object into a Dictionary
        /// </summary>
        public static IDictionary<string, object> ToDictionary(this object thingy, bool excludeReadOnlyProperties = false)
        {
            return (IDictionary<string, object>)thingy.ToExpando(excludeReadOnlyProperties);
        }

        static string _version = null;
        public static string Version(this object value)
        {
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(Reflection.Assembly.GetCallingAssembly().Location).FileVersion;
            var splitted = version.Split('.');

            var stringVer = splitted[0] + "." +splitted[1]+ "." + splitted[2];
            if (!Core.Helpers.Configuration.Config.Get("IsLiveEnvironment", false))
            {
                stringVer += "." + splitted[3];
            }
            return stringVer;
        }

        public static T Parse<T>(this object value) where T : IConvertible
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static T Parse<T>(this object value, IFormatProvider provider) where T : IConvertible
        {
            return (T)Convert.ChangeType(value, typeof(T), provider);
        }

        public static void CanNotBeNull(this object item)
        {
            if (null == item) throw new Exception("Can not be null");
        }

        public static void MustMatch(this object item, Type t)
        {
            item.CanNotBeNull();
            item.MustMatch(t, item.GetType() + " must match type " + t.Name);
        }

        public static void MustMatch(this object item, Type t, string ExceptionMessage)
        {
            item.CanNotBeNull();
            if (!(item.GetType().Match(t))) throw new Exception(ExceptionMessage);
        }

        public static bool IsDefault<T>(this T item)
        {
            return Object.Equals(default(T), item);
        }

        public static bool IsNotDefault<T>(this T item)
        {
            return !item.IsDefault();
        }

        public static bool IsNull(this object item)
        {
            return (null == item);
        }

        public static bool IsNotNull(this object item)
        {
            return (null != item);
        }

        public static void ThrowIfNull<T>(this T value, string variableName) where T : class
        {
            if (value is string && (value as string).IsNullOrEmpty() || value == null)
            {
                throw new NullReferenceException(string.Format("Value is Null: {0}", variableName));
            }
        }

        public static void ThrowIfNull<T>(this T value, Exception ex) where T : class
        {
            if (value == null)
            {
                throw ex;
            }
        }

        public static void ThrowIfDefault<T>(this T value, Exception ex)
        {
            if (Object.Equals(default(T), value))
            {
                throw ex;
            }
        }

        public static object EnumTypeTo(this object value, Type t)
        {
            if (!t.IsGenericType && t.IsEnum)
            {
                try
                {
                    return value.GetType().IsValueType ? Enum.ToObject(t, value) : Enum.Parse(t, value.ToString());
                }
                catch
                {
                    return null;
                }
            }

            try
            {
                return Convert.ChangeType(value, t);
            }
            catch
            {
                return null;
            }
        }


        public static object ChangeType(this object item, Type t)
        {
            if (item == null)
                return null;

            if (t == typeof(Guid))
            {
                return new Guid(item.ToString());
            }

            if (t.IsEnum)
            {
                return item.EnumTypeTo(t);
            }

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                // It's a nullable type, and not null, so that means it can be converted to its underlying type,
                // so overwrite the passed-in conversion type with this underlying type
                var nullableConverter = new NullableConverter(t);
                t = nullableConverter.UnderlyingType;

                return t.IsEnum ? item.EnumTypeTo(t) : item.ChangeType(t);
            }

            return t == typeof(String) ? item.ToString() : Convert.ChangeType(item, t);
        }


        public static object GetValue(this object o, string memberName)
        {
            object result = null;
            o.CanNotBeNull();
            memberName.CanNotBeEmpty();

            if (o is IDictionary)
            {
                var dict = (o as IDictionary);
                return dict.Contains(memberName) ? dict[memberName] : null;
            }

            var objectType = o.GetType();
            var pi = objectType.GetProperty(memberName);
            if (pi != null)
            {

                result = pi.GetValue(o, null);
                if (result.IsNull())
                {
                    var defValue = pi.GetCustomAttributes(typeof(DefaultValueAttribute), true);
                    if (defValue.IsFilled())
                    {
                        result = (defValue[0] as DefaultValueAttribute).Value;
                    }
                }
                return result;
            }

            var fi = objectType.GetField(memberName);
            if (fi != null)
            {
                result = fi.GetValue(o);
                if (result.IsNull())
                {
                    var defValue = fi.GetCustomAttributes(typeof(DefaultValueAttribute), true);
                    if (defValue.IsFilled())
                    {
                        result = (defValue[0] as DefaultValueAttribute).Value;
                    }
                }
            }

            return result;
        }

        public static object CallMethod(this object o, string voidName)
        {
            return o.CallMethod(voidName, null);
        }

        public static object CallMethod(this object o, string voidName, object[] parameters)
        {
            o.CanNotBeNull();
            voidName.CanNotBeEmpty();

            var objectType = o.GetType();

            var mi = objectType.GetMethod(voidName);
            if ((mi != null)) return mi.Invoke(o, parameters);
            return null;
        }

        public static T GetValue<T>(this object o, string memberName)
        {
            var t = typeof(T);
            return (T)o.GetValue(memberName).ChangeType(t);
        }

        public static bool SetValue(this object o, string memberName, object value)
        {
            o.CanNotBeNull();
            memberName.CanNotBeEmpty();

            var objectType = o.GetType();
            var pi = objectType.GetProperty(memberName);
            if ((pi != null) && (pi.CanWrite))
            {
                pi.SetValue(o, value, null);
                return true;
            }

            var fi = objectType.GetField(memberName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (fi != null)
            {
                fi.SetValue(o, value);
                return true;
            }

            fi = objectType.GetField("_" + memberName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (fi != null)
            {
                fi.SetValue(o, value);
                return true;
            }

            return false;
        }

        public static string ToJSON(this object obj)
        {
            return ServiceStack.Text.JsonSerializer.SerializeToString(obj, obj.GetType());
        }

        public static T FromJSON<T>(this string json)
        {
            return ServiceStack.Text.JsonSerializer.DeserializeFromString<T>(json);
        }

        //public static string ToJson(this object obj)
        //{
        //    return Core.Helpers.Serialization.Json.JSON.Instance.ToJSON(obj);
        //}

        //public static T ToObject<T>(this string json)
        //{
        //    return Core.Helpers.Serialization.Json.JSON.Instance.ToObject<T>(json);
        //}

        public static string ToXML(this object obj)
        {
            XmlSerializer s = new XmlSerializer(obj.GetType());
            using (StringWriter writer = new StringWriter())
            {
                s.Serialize(writer, obj);
                return writer.ToString();
            }
        }

        public static T FromXML<T>(this string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(data))
            {
                object obj = s.Deserialize(reader);
                return (T)obj;
            }
        }

        internal static string GetSQLType(this object type, int columnSize, int numericPrecision, int numericScale)
        {
            switch (type.ToString())
            {
                case "System.String":
                    return "NVARCHAR(" + ((columnSize == -1) ? "4000" : columnSize.ToString()) + ")";

                case "System.Decimal":
                    if (numericScale > 0)
                        return "REAL";
                    else if (numericPrecision > 10)
                        return "BIGINT";
                    else
                        return "INT";

                case "System.Double":
                case "System.Single":
                    return "REAL";

                case "System.Int64":
                    return "BIGINT";

                case "System.Int16":
                case "System.Int32":
                    return "INT";

                case "System.Guid":
                    return "UNIQUEIDENTIFIER";

                case "System.DateTime":
                    return "DATETIME";
                case "System.Boolean":
                    return "BIT";

                case "System.Byte":
                    return "TINYINT";


                default:
                    throw new Exception(type.ToString() + " not implemented.");
            }
        }

    }
}