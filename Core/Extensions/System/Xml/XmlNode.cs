﻿using System;
using System.Configuration;
using System.Reflection;
using System.Xml;

namespace Core
{
    /// <summary>
    /// Static XmlNodeExtensions class.
    /// </summary>
    public static class XmlNodeExtensions
    {
        /// <summary>
        /// Static GetValue method.
        /// </summary>
        /// <typeparam name="T">Type of the object</typeparam>
        /// <param name="node"><see cref="XmlNode"/></param>
        /// <param name="attributeName">String</param>
        /// <param name="isRequired">Boolean</param>
        /// <returns>Type of object</returns>
        public static T GetValue<T>(this XmlNode node, string attributeName, bool isRequired)
        {
            T result = default(T);

            XmlNode a = node.Attributes.RemoveNamedItem(attributeName);

            if ((a == null) || (String.IsNullOrEmpty(a.Value)))
            {
                if (isRequired) throw new ConfigurationErrorsException(String.Format(Core.Constants.ErrorMessages.AttributeRequired, attributeName, node.Name));
                return result;
            }
            if (result is int)
            {
                int outval = 0;
                if (int.TryParse(a.Value, out outval)) return (T)(object)outval;
                else throw new ConfigurationErrorsException(String.Format(Core.Constants.ErrorMessages.InvalidTypeValue, a, result.GetType().ToString()));
            }
            if (result is bool)
            {
                bool outval = false;
                if (bool.TryParse(a.Value, out outval)) return (T)(object)outval;
                else throw new ConfigurationErrorsException(String.Format(Core.Constants.ErrorMessages.InvalidTypeValue, a, result.GetType().ToString()));
            }
            if (result is Enum)
            {
                if (Enum.IsDefined(typeof(T), a.Value)) return (T)Enum.Parse(typeof(T), a.Value, true);
                else throw new ConfigurationErrorsException(String.Format(Core.Constants.ErrorMessages.InvalidTypeValue, a, result.GetType().ToString()));
            }
            else result = (T)(object)a.Value;
            return result;
        }

        /// <summary>
        /// Static GetValue method.
        /// </summary>
        /// <typeparam name="T">Type of the object</typeparam>
        /// <param name="node"><see cref="XmlNode"/></param>
        /// <param name="attributeName">String</param>
        /// <returns>Type of object</returns>
        public static T GetValue<T>(this XmlNode node, string attributeName)
        {
            return GetValue<T>(node, attributeName, false);
        }

        /// <summary>
        /// Static GetChildSection method.
        /// </summary>
        /// <param name="node"><see cref="XmlNode"/></param>
        /// <param name="sectionName">String</param>
        /// <param name="isRequired">Boolean</param>
        /// <returns><see cref="XmlNode"/></returns>
        public static XmlNode GetChildSection(this XmlNode node, string sectionName, bool isRequired)
        {
            foreach (XmlNode childNode in node.ChildNodes) if (childNode.Name == sectionName) return childNode;
            if (isRequired) throw new ConfigurationErrorsException(String.Format(Core.Constants.ErrorMessages.ChildSectionRequired, sectionName, node.Name));
            return null;
        }

        /// <summary>
        /// Static GetChildSection method.
        /// </summary>
        /// <param name="node"><see cref="XmlNode"/></param>
        /// <param name="sectionName">String</param>
        /// <returns><see cref="XmlNode"/></returns>
        public static XmlNode GetChildSection(this XmlNode node, string sectionName)
        {
            return GetChildSection(node, sectionName, false);
        }

        /// <summary>
        /// Static LoadSectionFromFile method.
        /// </summary>
        /// <param name="filePath">String</param>
        /// <param name="sectionName">String</param>
        /// <returns><see cref="XmlNode"/></returns>
        public static XmlNode LoadSectionFromFile(string filePath, string sectionName)
        {
            string xmlConfiguration = FileHelper.Read(filePath);
            XmlDocument document = new XmlDocument();
            document.LoadXml(xmlConfiguration);
            XmlNode node = document.SelectSingleNode(sectionName);
            return node;
        }

        /// <summary>
        /// Static SetObjectProperties method.
        /// </summary>
        /// <param name="node"><see cref="XmlNode"/></param>
        /// <param name="obj"><see cref="object"/></param>
        public static void SetObjectProperties(this XmlNode node, object obj)
        {
            foreach (XmlAttribute attribute in node.Attributes)
            {
                PropertyInfo property = obj.GetType().GetProperty(attribute.Name);
                if (property != null)
                {
                    if (property.PropertyType.IsEnum)
                    {
                        if (Enum.IsDefined(property.PropertyType, attribute.Value))
                            property.SetValue(obj, Enum.Parse(property.PropertyType, attribute.Value, true), null);
                    }
                    else property.SetValue(obj, Convert.ChangeType(attribute.Value, property.PropertyType), null);
                }
            }
        }
    }
}
