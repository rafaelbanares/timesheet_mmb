﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Core
{
    /// <summary>
    /// Extension methods for XElement
    /// </summary>
    public static class XElementExtensions
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xElement">The x element.</param>
        /// <param name="xpath">The xpath query.</param>
        /// <returns></returns>
        public static T GetValue<T>(this XElement xElement, string xpath)
        {
            if (xElement == null)
                return default(T);

            var element = xElement.XPathSelectElement(xpath);
            if (element == null || element.Value.IsNullOrEmpty())
                return default(T);

            return (T) element.Value.ChangeType(typeof (T));
        }


        /// <summary>
        /// Gets the values.
        /// </summary>
        /// <param name="xElement">The x element.</param>
        /// <param name="xpath">The xpath.</param>
        /// <returns>List of Pair. First: Node value, Second: Node attributes</returns>
        public static List<Dictionary<string, string>> GetValues(this XElement xElement, string xpath)
        {
            var result = new List<Dictionary<string, string>>();
            if (xElement == null)
                return result;

            var elements = xElement.XPathSelectElements(xpath);
            if (elements == null)
                return result;

            result.AddRange(from item in elements
                            select item.Attributes().ToList().ToDictionary(k => k.Name.ToString(), v => v.Value)
                            );

            return result;
        }
    }
}