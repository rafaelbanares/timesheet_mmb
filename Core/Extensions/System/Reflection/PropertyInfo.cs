﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Core
{
    public static class PropertyInfoExtensions
    {

        public static List<T> GetAttributes<T>(this PropertyInfo item) where T : Attribute
        {
            List<T> result = new List<T>();
            foreach (var attribute in item.GetCustomAttributes(typeof(T), true))
            {
                result.Add(attribute as T);
            }

            return result;
        }
    }
}
