﻿using System.Globalization;

namespace System.Reflection
{
    /// <summary>
    /// Extension methods for method.
    /// </summary>
    public static class MethodExtensions
    {
        /// <summary>
        /// Invokes the method or constructor represented by the current concreteFeatureType, using
        /// the specified parameters.
        /// </summary>
        /// <typeparam name="T">return type</typeparam>
        /// <param name="method">
        /// Method or constructor to invoke
        /// </param>
        /// <param name="obj">
        ///  The object on which to invoke the method or constructor. If a method is static,
        ///  this argument is ignored. If a constructor is static, this argument must
        ///  be null or an concreteFeatureType of the class that defines the constructor.
        /// </param>
        /// <param name="parameters">
        ///  An argument list for the invoked method or constructor. This is an array
        ///  of objects with the same number, order, and type as the parameters of the
        ///  method or constructor to be invoked. If there are no parameters, parameters
        ///  should be null.  If the method or constructor represented by this concreteFeatureType
        ///  takes a ref parameter (ByRef in Visual Basic), no special attribute is required
        ///  for that parameter in order to invoke the method or constructor using this
        ///  function. Any object in this array that is not explicitly initialized with
        ///  a value will contain the default value for that object type. For reference-type
        ///  elements, this value is null. For value-type elements, this value is 0, 0.0,
        ///  or false, depending on the specific element type.
        /// </param>
        /// <returns>
        /// An object of type T containing the return value of the invoked method, or null in the
        /// case of a constructor.
        /// </returns>
        public static T Invoke<T>(this MethodInfo method, object obj, object[] parameters)
        {
            method.CanNotBeNull();

            var result = (T) method.Invoke(obj, parameters);
            return result;
        }

        /// <summary>
        /// Invokes the method or constructor, using the specified parameters.
        /// </summary>
        /// <typeparam name="T">return type</typeparam>
        /// <param name="method">
        /// Method or constructor to invoke
        /// </param>
        /// <param name="parameters">
        ///  An argument list for the invoked method or constructor. This is an array
        ///  of objects with the same number, order, and type as the parameters of the
        ///  method or constructor to be invoked. If there are no parameters, parameters
        ///  should be null.  If the method or constructor represented by this concreteFeatureType
        ///  takes a ref parameter (ByRef in Visual Basic), no special attribute is required
        ///  for that parameter in order to invoke the method or constructor using this
        ///  function. Any object in this array that is not explicitly initialized with
        ///  a value will contain the default value for that object type. For reference-type
        ///  elements, this value is null. For value-type elements, this value is 0, 0.0,
        ///  or false, depending on the specific element type.
        /// </param>
        /// <returns>
        /// An object of type T containing the return value of the invoked method, or null in the
        /// case of a constructor.
        /// </returns>
        public static T Invoke<T>(this MethodInfo method, object[] parameters)
        {
            method.CanNotBeNull();

            var result = (T)method.Invoke(null, BindingFlags.Static, null, parameters, CultureInfo.CurrentCulture);
            return result;
        }

        public static T Invoke<T>(this MethodInfo method)
        {
            method.CanNotBeNull();

            var result = (T)method.Invoke(null, BindingFlags.Static, null, null, CultureInfo.CurrentCulture);
            return result;
        }
    }
}