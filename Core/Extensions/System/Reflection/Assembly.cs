﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace System.Reflection
{
    /// <summary>
    /// Extension methods for assembly.
    /// </summary>
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Look for any type matching the given criteria in the given assembly
        /// </summary>
        /// <param name="assembly">assembly to look into</param>
        /// <param name="criteriaType">A base class, an interface or an attribute</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetTypes(this Assembly assembly, Type criteriaType)
        {
            assembly.CanNotBeNull();
            criteriaType.CanNotBeNull();
            if (assembly.IsDynamic) return null;
            try
            {

                return assembly.GetExportedTypes().Where(t => t.Match(criteriaType));
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Type> GetMatchingTypes(this Assembly assembly, Type criteriaType)
        {
            assembly.CanNotBeNull();
            criteriaType.CanNotBeNull();
            try
            {
                return assembly.GetTypes(criteriaType).Where(u => u != criteriaType);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the resource stream.
        /// </summary>
        /// <param name="item">The assembly.</param>
        /// <param name="resourceName">Name of the resource.</param>
        /// <returns></returns>
        public static StreamReader GetResourceStream(this Assembly item, string resourceName)
        {
            var list = item.GetManifestResourceNames().Where(resName => resName.ToLower().EndsWith(resourceName.ToLower()));

            if(list.Count() == 0)
            {
                throw new ApplicationException("Unable to find embedded resource : " + resourceName);
            }

            return list.Select(resName => new StreamReader(item.GetManifestResourceStream(resName))).FirstOrDefault();
        }

        /// <summary>
        /// Gets the content of the resource.
        /// </summary>
        /// <param name="item">The assembly.</param>
        /// <param name="resourceName">Name of the resource.</param>
        /// <returns></returns>
        public static string GetResourceContent(this Assembly item, string resourceName)
        {
            var sr = item.GetResourceStream(resourceName);
            var data = sr.ReadToEnd();
            sr.Close();
            return data;
        }

        /// <summary>
        /// Gets the content of the resource.
        /// </summary>
        /// <param name="item">The assembly.</param>
        /// <param name="resourceName">Name of the resource.</param>
        /// <returns></returns>
        public static void SaveResourceAs(this Assembly item, string resourceName, string filePath)
        {
            var list = item.GetManifestResourceNames().Where(resName => resName.ToLower().EndsWith(resourceName.ToLower()));
            if (list.Count() == 0)
            {
                throw new ApplicationException("Unable to find embedded resource : " + resourceName);
            }
            list.Select(resName => item.GetManifestResourceStream(resName)).First().SaveAs(filePath);
        }
    }
}