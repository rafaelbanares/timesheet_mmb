﻿namespace System
{
    /// <summary>
    /// Extension methods for boolean.
    /// </summary>
    public static class BooleanExtensions
    {
        /// <summary>
        /// Transforms a nullable boolean to a boolean value.
        /// If the parameter has no value, the method returns false.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool ToBool(this bool? value)
        {
            return (value.HasValue && value.Value);
        }

        /// <summary>
        /// Transforms a nullable boolean to a boolean value.
        /// The given default value is returned if the <paramref name="value" /> parameter is null.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value used when the parameter is null.</param>
        /// <returns></returns>
        public static bool ToBool(this bool? value, bool defaultValue)
        {
            return value.HasValue ? value.Value : defaultValue;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this concreteFeatureType.
        /// </summary>
        /// <param name="value">if updateSet to <c>true</c> [value].</param>
        /// <param name="lowerCase">if updateSet to <c>true</c> [lower case].</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this concreteFeatureType.
        /// </returns>
        public static string ToString(this bool value, bool lowerCase)
        {
            if (lowerCase)
                return value.ToString().ToLowerInvariant();
            else
                return value.ToString();
        }
    }
}