﻿using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Core;
using System.Linq.Expressions;

namespace System.Collections.Generic
{
    /// <summary>
    /// Extension methods for IEnumerable.
    /// </summary>
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> ToEnumerable<T>(this T o)
        {
            return new List<T> { o };
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            if (enumeration.IsNotNull() && enumeration.Any())
            {
                foreach (T item in enumeration)
                {
                    action(item);
                }
            }
        }

        //[System.Diagnostics.DebuggerStepThrough]
        //public static bool While<T>(this IEnumerable<T> enumeration, Func<T, bool> whileAction) 
        //{
        //    if (enumeration.IsNotNull() && enumeration.Any())
        //    {
        //        foreach (T item in enumeration)
        //        {
        //            if (!whileAction(item)) return true;
        //        }
        //    }
        //    return false;
        //}        
        
        /// <summary>
        /// Returns a <c>System.String</c> object representing each item
        /// of the current <c>IEnumerable</c> object separated by the 
        /// <paramref name="separator"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">A <c>IEnumerable</c> reference.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        public static string ToString<T>(this IEnumerable<T> item, string separator)
        {
            var result = string.Empty;
            foreach (var s in item) result += s + separator;
            result = result.TrimEnd(separator.ToCharArray());
            return result;
        }

        /// <summary>
        /// Returns a <c>System.String</c> object, which composes 
        /// the current <c>IEnumerable</c> object for 'IN' SQL command value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">A <c>IEnumerable</c> reference.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        public static string ToINSQLValue<T>(this IEnumerable<T> item)
        {
            var separator = ",";
            var result = string.Empty;
            foreach (var s in item) result += "'" + s + "'" + separator;
            result = result.TrimEnd(separator.ToCharArray());
            return result;
        }

        /// <summary>
        /// Determines whether a collection is null or empty (count = 0).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns>
        /// 	<c>true</c> if the collection is null or empty; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection)
        {
            return ((collection == null) || (!collection.Any()));
        }

        /// <summary>
        /// Throws an exception if the specified <paramref name="collection"/> is null or empty.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        public static void CanNotBeNullOrEmpty<T>(this IEnumerable<T> collection)
        {
            if (collection.IsNullOrEmpty())
                throw new Exception("Can not be null or empty");
        }

        /// <summary>
        /// Returns a list of <c>System.Guid</c> objects builded from the current
        /// string collection object. Each <c>System.String</c> object
        /// must be a string representation of a <c>System.Guid</c>.
        /// </summary>
        /// <param name="collection">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static IEnumerable<Guid> ToGuidList(this IEnumerable<string> collection)
        {
            if (collection.IsNull()) return new List<Guid>();
            return collection.Select(x => x.ToGuid());
        }

        /// <summary>
        /// Determines whether the specified collection is filled.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns>
        /// 	<c>true</c> if the specified collection is filled; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFilled<T>(this IEnumerable<T> collection)
        {
            return !collection.IsNullOrEmpty();
        }

        /// <summary>
        /// EnsureExistence the returned IEnumerable is not null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns>
        /// 	the given collection or a new empty one.
        /// </returns>
        public static IEnumerable<T> Safe<T>(this IEnumerable<T> collection)
        {
            return collection ?? new List<T>();
        }

        /// <summary>
        /// Throws an exception if the given collection is null or empty. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        public static void CanNotBeEmpty<T>(this IEnumerable<T> collection)
        {
            if (collection.IsNullOrEmpty()) throw new Exception("Can not be null or empty");
        }

        /// <summary>
        /// Determines the item having the min value.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <returns></returns>
        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source,
                                                   Func<TSource, TKey> selector)
        {
            return source.MinBy(selector, Comparer<TKey>.Default);
        }

        /// <summary>
        /// Determines the item having the min value.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns></returns>
        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source,
                                                   Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            selector.CanNotBeNull();
            comparer.CanNotBeNull();

            if (!source.Any())
                return default(TSource);

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence was empty");
                }
                var min = sourceIterator.Current;
                var minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }
                return min;
            }
        }


        /// <summary>
        /// Determines the item having the max value.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <returns></returns>
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
                                                   Func<TSource, TKey> selector)
        {
            return source.MaxBy(selector, Comparer<TKey>.Default);
        }

        /// <summary>
        /// Determines the item having the max value.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns></returns>
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
                                                   Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            selector.CanNotBeNull();
            comparer.CanNotBeNull();

            if (!source.Any())
                return default(TSource);

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence was empty");
                }
                var max = sourceIterator.Current;
                var maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }
                return max;
            }
        }


        private static TSerializableEntity ToTSerializableEntity<TEntity, TSerializableEntity>(TEntity entity)
            where TEntity : class, new()
            where TSerializableEntity : SerializableEntity<TEntity>, new()
        {
            return new TSerializableEntity {Entity = entity};
        }

        /// <summary>
        /// Returns the input typed as System.Collections.Generic.IEnumerable<T>.
        /// </summary>
        /// <typeparam name="TSource">The sequence to type as System.Collections.Generic.IEnumerable<T>.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the elements of source.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>The input sequence typed as System.Collections.Generic.IEnumerable<T>.</returns>
        public static IEnumerable<TSerializableEntity> AsEnumerable<TSource, TSerializableEntity>(
            this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return source.ToList<TSource, TSerializableEntity>();
        }

        /// <summary>
        /// Returns the element at a specified index in a sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The element at the specified position in the source sequence.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
        /// <param name="index">The zero-based index of the element to retrieve.</param>
        /// <returns>Returns the element at a specified index in a sequence.</returns>
        public static TSerializableEntity ElementAt<TSource, TSerializableEntity>(this IEnumerable<TSource> source,
                                                                                  int index)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.ElementAt(index));
        }

        /// <summary>
        /// Returns the element at a specified index in a sequence or a default value
        /// if the index is out of range.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
        /// <param name="index">The zero-based index of the element to retrieve.</param>
        /// <returns>
        /// <c>default(TSource)</c> if the index is outside the bounds of the source sequence;
        /// otherwise, the element at the specified position in the source sequence.
        /// </returns>
        public static TSerializableEntity ElementAtOrDefault<TSource, TSerializableEntity>(
            this IEnumerable<TSource> source, int index)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.ElementAtOrDefault(index));
        }

        /// <summary>
        /// Returns the first element of a sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">The System.Collections.Generic.IEnumerable<T> to return the first element of.</param>
        /// <returns>The first element in the specified sequence.</returns>
        public static TSerializableEntity First<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.First());
        }

        /// <summary>
        /// Returns the first element in a sequence that satisfies a specified condition.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns>
        /// The first element in the sequence that passes the test in the specified predicate
        /// function.
        /// </returns>
        public static TSerializableEntity First<TSource, TSerializableEntity>(this IEnumerable<TSource> source,
                                                                              Func<TSource, bool> predicate)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.First(predicate));
        }

        /// <summary>
        /// Returns the first element of a sequence, or a default value if the sequence
        /// contains no elements.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">The System.Collections.Generic.IEnumerable<T> to return the first element of.</param>
        /// <returns>
        /// <c>default(TSource)</c> if source is empty; otherwise, the first element in source.
        /// </returns>
        public static TSerializableEntity FirstOrDefault<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.FirstOrDefault());
        }

        /// <summary>
        /// Returns the first element of the sequence that satisfies a condition or a
        /// default value if no such element is found.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns>
        /// <c>default(TSource)</c> if source is empty or if no element passes the test specified
        /// by predicate; otherwise, the first element in source that passes the test
        /// specified by predicate.
        /// </returns>
        public static TSerializableEntity FirstOrDefault<TSource, TSerializableEntity>(this IEnumerable<TSource> source,
                                                                                       Func<TSource, bool> predicate)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.FirstOrDefault(predicate));
        }

        /// <summary>
        /// Returns the last element of a sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return the last element of.</param>
        /// <returns>The value at the last position in the source sequence.</returns>
        public static TSerializableEntity Last<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.Last());
        }

        /// <summary>
        /// Returns the last element of a sequence that satisfies a specified condition.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns>
        /// The last element in the sequence that passes the test in the specified predicate
        /// function.
        /// </returns>
        public static TSerializableEntity Last<TSource, TSerializableEntity>(this IEnumerable<TSource> source,
                                                                             Func<TSource, bool> predicate)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.Last(predicate));
        }

        /// <summary>
        /// Returns the last element of a sequence, or a default value if the sequence
        /// contains no elements.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return the last element of.</param>
        /// <returns>
        /// <c>default(TSource)</c> if the source sequence is empty; otherwise, the last element
        /// </returns>
        public static TSerializableEntity LastOrDefault<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.LastOrDefault());
        }

        /// <summary>
        /// Returns the last element of a sequence that satisfies a condition or a default
        /// value if no such element is found.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return an element from.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns>
        /// <c>default(TSource)</c> if the sequence is empty or if no elements pass the test
        /// in the predicate function; otherwise, the last element that passes the test
        /// in the predicate function.
        /// </returns>
        public static TSerializableEntity LastOrDefault<TSource, TSerializableEntity>(this IEnumerable<TSource> source,
                                                                                      Func<TSource, bool> predicate)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.LastOrDefault(predicate));
        }

        /// <summary>
        /// Inverts the order of the elements in a sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">A sequence of values to reverse.</param>
        /// <returns>
        /// A sequence whose elements correspond to those of the input sequence in reverse order.
        /// </returns>
        public static List<TSerializableEntity> Reverse<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return source.Reverse().ToList<TSource, TSerializableEntity>();
        }

        /// <summary>
        /// Returns the only element of a sequence, and throws an exception if there
        /// is not exactly one element in the sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return the single element of.</param>
        /// <returns>The single element of the input sequence.</returns>
        public static TSerializableEntity Single<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.Single());
        }

        /// <summary>
        /// Returns the only element of a sequence that satisfies a specified condition,
        /// and throws an exception if more than one such element exists.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return a single element from.</param>
        /// <param name="predicate">A function to test an element for a condition.</param>
        /// <returns>The single element of the input sequence that satisfies a condition.</returns>
        public static TSerializableEntity Single<TSource, TSerializableEntity>(this IEnumerable<TSource> source,
                                                                               Func<TSource, bool> predicate)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.Single(predicate));
        }

        /// <summary>
        /// Returns the only element of a sequence, or a default value if the sequence
        /// is empty; this method throws an exception if there is more than one element
        /// in the sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return the single element of.</param>
        /// <returns>
        /// The single element of the input sequence, or default(TSource) if the sequence
        /// contains no elements.
        /// </returns>
        public static TSerializableEntity SingleOrDefault<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.SingleOrDefault());
        }

        /// <summary>
        /// Returns the only element of a sequence that satisfies a specified condition
        /// or a default value if no such element exists; this method throws an exception
        /// if more than one element satisfies the condition.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to return a single element from.</param>
        /// <param name="predicate">A function to test an element for a condition.</param>
        /// <returns>
        /// The single element of the input sequence that satisfies the condition, or
        /// <c>default(TSource)</c> if no such element is found.
        /// </returns>
        public static TSerializableEntity SingleOrDefault<TSource, TSerializableEntity>(
            this IEnumerable<TSource> source, Func<TSource, bool> predicate)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return ToTSerializableEntity<TSource, TSerializableEntity>(source.SingleOrDefault(predicate));
        }

        /// <summary>
        /// Creates an array from a System.Collections.Generic.IEnumerable<T>.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable<T> to create an array from.</param>
        /// <returns></returns>
        public static TSerializableEntity[] ToArray<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return source.ToList<TSource, TSerializableEntity>().ToArray();
        }

        /// <summary>
        /// Creates a System.Collections.Generic.List<T> from an System.Collections.Generic.IEnumerable<T>.
        /// </summary>
        /// <typeparam name="TSource"> The type of the elements of source.</typeparam>
        /// <typeparam name="TSerializableEntity">The type of the serializable entity.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>
        /// A System.Collections.Generic.List<T> that contains elements from the input sequence.
        /// </returns>
        public static List<TSerializableEntity> ToList<TSource, TSerializableEntity>(this IEnumerable<TSource> source)
            where TSource : class, new()
            where TSerializableEntity : SerializableEntity<TSource>, new()
        {
            return source.Select(entity => ToTSerializableEntity<TSource, TSerializableEntity>(entity)).ToList();
        }

        public static DataTable ToDataTable(this IEnumerable list, string dataTableName)
        {
            DataTable res = ToDataTable(list);
            res.TableName = dataTableName;
            return res;
        }
        public static DataTable ToDataTable(this IEnumerable list)
        {
            if (list is DataTable) return (DataTable)list;
            if (list is DataView) return ((DataView)list).Table;

            DataTable dt = new DataTable();
            var IsFirst = true;
            IEnumerable<PropertyInfo> properties = null;
            IEnumerable<FieldInfo> fields = null;

            foreach (object obj in list)
            {
                if (IsFirst)
                {
                    Type t = obj.GetType();
                    properties = t.GetProperties().Where(p => p.CanRead && p.GetIndexParameters().Length == 0);
                    fields = t.GetFields().Where(f => f.IsPublic);
                    foreach (PropertyInfo pi in properties)
                    {
                        dt.Columns.Add(pi.Name, Nullable.GetUnderlyingType(pi.PropertyType) ?? pi.PropertyType);
                    }
                    foreach (FieldInfo field in fields)
                    {
                        dt.Columns.Add(field.Name, Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType);
                    }
                    IsFirst = false;
                }

                DataRow dr = dt.NewRow();
                if (properties.IsNotNull())
                {
                    foreach (PropertyInfo pi in properties)
                    {
                        object value = pi.GetValue(obj, null) ?? DBNull.Value;
                        dr[pi.Name] = value;
                    }
                }
                if (fields.IsNotNull())
                {
                    foreach (FieldInfo field in fields)
                    {
                        object value = field.GetValue(obj) ?? DBNull.Value;
                        dr[field.Name] = value;
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static string StringConcatenate<T>(this IEnumerable<T> source, Func<T, string> func)
        {
            StringBuilder sb = new StringBuilder();
            foreach (T item in source)
                sb.Append(func(item));
            return sb.ToString();
        }

        public static string StringConcatenate(this IEnumerable<string> source)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string item in source)
                sb.Append(item);
            return sb.ToString();
        }

        public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> source, int count)
        {
            List<T> saveList = new List<T>();
            int saved = 0;
            foreach (T item in source)
            {
                if (saved < count)
                {
                    saveList.Add(item);
                    ++saved;
                    continue;
                }
                saveList.Add(item);
                T toReturn = saveList.First();
                saveList.Remove(toReturn);
                yield return toReturn;
            }
            yield break;
        }

        /// <summary>
        /// Apply an OrderBy rule that is based on a sort property.
        /// </summary>
        /// <typeparam name="T">The type of the objects that are stored in the collection.</typeparam>
        /// <param name="collection">The collection to sort.</param>
        /// <param name="sortProperty">The property to sort on.</param>
        /// <returns>A sorter collection.</returns>
        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> collection, string sortProperty)
        {
            //Get the collection type
            Type dataSourceType = collection.GetType();

            // Determine the frameworkDatabase type of the items in the frameworkDatabase source at runtime.
            Type dataItemType = typeof(object);
            if (dataSourceType.HasElementType)
                dataItemType = dataSourceType.GetElementType();
            else if (dataSourceType.IsGenericType)
                dataItemType = dataSourceType.GetGenericArguments()[0];

            // Create an instance of the GenericSorter class passing in the frameworkDatabase item type.
            Type sorterType = typeof(GenericSorter<>).MakeGenericType(dataItemType);
            var sorterObject = Activator.CreateInstance(sorterType);

            // Now I can call the "Sort" method passing in my runtime types.
            return sorterType.GetMethod("Sort", new[] { dataSourceType, typeof(string) }).Invoke(sorterObject, new object[] { collection, sortProperty }) as IEnumerable<T>;
        }

        /// <summary>
        /// Sort a collection based on a sort property.
        /// </summary>
        /// <typeparam name="T">The type of the objects that are stored in the collection.</typeparam>
        /// <param name="list">The list to sort.</param>
        /// <param name="sortProperty">The property to sort on.</param>
        /// <returns>A sorter collection.</returns>
        public static void Sort<T>(this List<T> list, string sortProperty)
        {
            IEnumerable<T> sorted = list.OrderBy(sortProperty).ToList();
            list.Clear();
            list.AddRange(sorted);
        }

        internal class GenericSorter<T>
        {
            public IEnumerable<T> Sort(IEnumerable<T> source, string sortBy)
            {
                var param = Expression.Parameter(typeof(T), "item");
                var sortExpression = Expression.Lambda<Func<T, object>>
                    (Expression.Convert(Expression.Property(param, sortBy), typeof(object)), param);
                return source.AsQueryable().OrderBy(sortExpression);
            }
        }
    }
}
