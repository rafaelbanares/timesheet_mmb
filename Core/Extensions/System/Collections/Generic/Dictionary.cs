﻿
namespace System.Collections.Generic
{
    /// <summary>
    /// Extension methods for date.
    /// </summary>
    public static class DictionaryExtensions
    {
        public static void AddRange<T, U>(this Dictionary<T, U> item, Dictionary<T, U> range)
        {
            if (range.IsNotNull())
            {
                foreach (var elem in range) item.Add(elem.Key, elem.Value);
            }
        }
    }
}