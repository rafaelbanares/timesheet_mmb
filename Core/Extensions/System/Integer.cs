﻿
using System.Collections.Generic;
namespace System
{
    /// <summary>
    /// Extension methods for decimal.
    /// </summary>
    public static class IntegerExtensions
    {
        /// <summary>
        /// Throws an exception if the given value is not positive.
        /// </summary>
        /// <param name="item">The value.</param>
        public static void MustBePositive(this int item)
        {
            if (!(item > 0)) throw new Exception("Must be positive");
        }

        /// <summary>
        /// Throws an exception if the given value is not in the range [min, max].
        /// </summary>
        /// <param name="item">The value.</param>
        /// <param name="min">The min value.</param>
        /// <param name="max">The max value.</param>
        public static void MustBeInRange(this int item, int min, int max)
        {
            if (!(item >= min && item <= max))
                throw new Exception(String.Format("Must be in range [{0},{1}]", min, max));
        }

        public static string Spaces(this int item)
        {
            var result = String.Empty;
            for (int i = 0; i < item; i++) result += " ";
            return result;
        }

        public static void ForEach(this int item, Action<int> deleg)
        {
            for (int i = 0; i < item; i++) deleg(i);
        }

        public static List<int> ToList(this int item)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < item; i++) result.Add(i);
            return result;
        }

        public static List<T> ToList<T>(this int item, Func<int, T> populateFunction)
        {
            List<T> result = new List<T>();
            for (int i = 0; i < item; i++) result.Add(populateFunction(i));
            return result;
        }

        public static Dictionary<int, T> ToDictionary<T>(this int item, Func<int, T> populateFunction)
        {
            Dictionary<int, T> result = new Dictionary<int, T>();
            for (int i = 0; i < item; i++) result.Add(i, populateFunction(i));
            return result;
        }

        public static IEnumerable<T> Select<T>(this int item, Func<int, T> deleg)
        {
            for (int i = 0; i < item; i++) yield return deleg(i);
        }

        public static TimeSpan Days(this int item)
        {
            return new TimeSpan(item, 0, 0, 0, 0);
        }

        public static TimeSpan Years(this int item)
        {
            var dt = DateTime.Now.AddYears(item);
            return (dt - DateTime.Now).Duration();
        }

        public static TimeSpan Hours(this int item)
        {
            return new TimeSpan(0, item, 0, 0, 0);
        }

        public static TimeSpan Minutes(this int item)
        {
            return new TimeSpan(0, 0, item, 0, 0);
        }

        public static TimeSpan Seconds(this int item)
        {
            return new TimeSpan(0, 0, 0, item, 0);
        }

        public static TimeSpan Milliseconds(this int item)
        {
            return new TimeSpan(0, 0, 0, 0, item);
        }

        public static bool ToBool(this int item)
        {
            return Convert.ToBoolean(item);
        }
    }
}