﻿using System.Globalization;

namespace System
{
    /// <summary>
    /// Extension methods for decimal.
    /// </summary>
    public static class DecimalExtensions
    {
        /// <summary>
        /// Rounds a decimal value to a specified number of fractional digits.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="precision">The precision.</param>
        /// <returns></returns>
        public static decimal? Rounded(this decimal? value, int precision)
        {
            return !value.HasValue ? (decimal?) null : value.Value.Rounded(precision);
        }

        /// <summary>
        /// Rounds a decimal value to a specified number of fractional digits.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="precision">The precision.</param>
        /// <returns></returns>
        public static decimal Rounded(this decimal value, int precision)
        {
            return Math.Round(value, precision);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this concreteFeatureType.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="useInvariantCulture">if updateSet to <c>true</c> [use invariant culture].</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this concreteFeatureType.
        /// </returns>
        public static string ToString(this decimal value, bool useInvariantCulture)
        {
            return useInvariantCulture ? value.ToString(CultureInfo.InvariantCulture.NumberFormat) : value.ToString();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this concreteFeatureType.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="useInvariantCulture">if updateSet to <c>true</c> [use invariant culture].</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this concreteFeatureType.
        /// </returns>
        public static string ToString(this decimal? value, bool useInvariantCulture)
        {
            return value.ToString(useInvariantCulture, "-");
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this concreteFeatureType.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="useInvariantCulture">if updateSet to <c>true</c> [use invariant culture].</param>
        /// <param name="nullText">The null text.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this concreteFeatureType.
        /// </returns>
        public static string ToString(this decimal? value, bool useInvariantCulture, string nullText)
        {
            if (!value.HasValue) return nullText;
            return useInvariantCulture
                       ? value.Value.ToString(CultureInfo.InvariantCulture)
                       : value.Value.ToString();
        }
    }
}