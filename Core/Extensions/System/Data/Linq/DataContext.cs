﻿using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System.Data.Linq
{
    public static class DataContextExtensions
    {
        public static void DeleteByPK<TSource>(this DataContext context, TSource entity)
                where TSource : class
        {
            var primaryKeys = GetPrimaryKey<TSource>(entity);
            if(!primaryKeys.Any())
                throw new NotSupportedException(typeof(TSource).ToString() + " has no Primary Key");

            var tableName = GetTableName<TSource>(entity);

            var buffer = new StringBuilder();
            buffer.Append("DELETE FROM [")
                  .Append(tableName)
                  .Append("] WHERE ");

            foreach(var pk in primaryKeys) 
            {
                buffer.Append(pk.Key.Name)
                      .Append(" = ")
                      .Append(FormatValue(pk.Value))
                      .Append(" AND ");
            }

            buffer.Length = buffer.Length - 5;

            context.ExecuteCommand(buffer.ToString());
        }

        private static Dictionary<PropertyInfo, object> GetPrimaryKey<TSource>(TSource entity)
        {
            PropertyInfo[] infos = typeof(TSource).GetProperties();
            Dictionary<PropertyInfo, object> primaryKeys = new Dictionary<PropertyInfo, object>();
            foreach (PropertyInfo info in infos)
            {
                var column = info.GetCustomAttributes(false)
                                 .Where(x => x.GetType() == typeof(ColumnAttribute))
                                 .FirstOrDefault(x => ((ColumnAttribute)x).IsPrimaryKey && ((ColumnAttribute)x).DbType.Contains("NOT NULL"));

                if (column != null)
                {
                    primaryKeys.Add(info, info.GetValue(entity, null));
                }
            }

            return primaryKeys;
        }

        private static string GetTableName<TSource>(TSource entity) 
        {
            var tableName = typeof(TSource).GetCustomAttributes(false)
                                           .Where(x => x.GetType() == typeof(TableAttribute))
                                           .Cast<TableAttribute>()
                                           .FirstOrDefault()
                                           .Name;
            if (tableName.StartsWith("dbo."))
                tableName = tableName.Substring("dbo.".Length);

            return tableName;
        }

        private static string FormatValue(object value) 
        {
            if(value is int || value is decimal || value is float || value is double)
                return value.ToString();
            else if(value is string || value is Guid)
                return string.Format("'{0}'", value);
            else if(value is bool)
                return (bool)value ? "1" : "0";
            else if(value is DateTime)
                return string.Format("N'{0}'", ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss:fffffff"));
            else return string.Empty;

        }

        public static void DiscardChanges(this DataContext context)
        {
            var changes = context.GetChangeSet();
            List<ITable> updatedTables = new List<ITable>();
            foreach(var update in changes.Updates)
            {
                var tbl = context.GetTableWithInheritance(update.GetType());
                if(!updatedTables.Contains(tbl))
                {
                    updatedTables.Add(tbl);
                    context.Refresh(RefreshMode.OverwriteCurrentValues, tbl);
                }
            }
            foreach(var insertion in changes.Inserts)
            {
                context.GetTableWithInheritance(insertion.GetType()).DeleteOnSubmit(insertion);
            }
            foreach(var deletion in changes.Deletes)
            {
                context.GetTableWithInheritance(deletion.GetType()).InsertOnSubmit(deletion);
            }
            context.SubmitChanges();
        }

        private static ITable GetTableWithInheritance(this DataContext data, Type type)
        {
            try
            {
                return data.GetTable(type);
            }
            catch
            {
                return GetTableWithInheritance(data, type.BaseType); 
            }
        }
    }
}
