﻿using System.Data.Common;
using System.Data.EntityClient;

namespace System.Data.Objects
{
    public static class ObjectContextExtensions
    {
        /// <summary>
        /// Creates a store command based on the connection used by given object context.
        /// </summary>
        /// <param name="context">Object context.</param>
        /// <param name="commandText">Command text.</param>
        /// <param name="parameters">Parameters to pass to the store command.</param>
        /// <returns>Store command.</returns>
        public static DbCommand CreateStoreCommand(this ObjectContext context, string commandText,
                                                   params object[] parameters)
        {
            return CreateStoreCommand(context, commandText, CommandType.Text, parameters);
        }

        /// <summary>
        /// Creates a store command based on the connection used by given object context.
        /// </summary>
        /// <param name="context">Object context.</param>
        /// <param name="commandText">Command text.</param>
        /// <param name="commandType">Command type.</param>
        /// <param name="parameters">Parameters to pass to the store command.</param>
        /// <returns>Store command.</returns>
        public static DbCommand CreateStoreCommand(this ObjectContext context, string commandText,
                                                   CommandType commandType, params object[] parameters)
        {
            context.CanNotBeNull();
            commandText.CanNotBeEmpty();

            var entityConnection = (EntityConnection) context.Connection;
            var storeConnection = entityConnection.StoreConnection;
            var storeCommand = storeConnection.CreateCommand();

            // setup command
            storeCommand.CommandText = commandText;
            storeCommand.CommandType = commandType;
            if (null != parameters)
            {
                storeCommand.Parameters.AddRange(parameters);
            }

            // pass through command timeout as appropriate
            if (context.CommandTimeout.HasValue)
            {
                storeCommand.CommandTimeout = context.CommandTimeout.Value;
            }

            return storeCommand;
        }
    }
}