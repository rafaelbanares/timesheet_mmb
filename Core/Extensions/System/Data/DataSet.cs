﻿
using System.IO;
using ExtremeML.Packaging;
using ExtremeML.Spreadsheet.Address;

namespace System.Data
{
    public static class DataSetExtensions
    {
        public static T NotInSet<T>(this T firstSet, T secondSet) where T : DataSet, new()
        {
            T result = null;
            foreach (DataTable dt in firstSet.Tables)
            {
                DataTable notInTable = null;
                if (secondSet.Tables.Contains(dt.TableName))
                    notInTable = dt.NotIn(secondSet.Tables[dt.TableName]);
                else notInTable = dt.NotIn(dt.Clone());
                if (notInTable.IsNotNull())
                {
                    if (result.IsNull()) result = new T();
                    result.Tables.Add(notInTable);
                }
            }
            return result;
        }

        public static byte[] ToExcel<T>(this T ds, bool WithHeaders) where T : DataSet
        {
            byte[] result = null;
            using (var output = new MemoryStream())
            {
                using (var package = SpreadsheetDocumentWrapper.Create(output))
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        var sheet = package.WorkbookPart.WorksheetParts.Add(dt.TableName);
                        int colNum = 0;
                        int rowNum = 0;
                        if (WithHeaders)
                        {
                            foreach (DataColumn dc in dt.Columns)
                            {
                                sheet.Worksheet.SetCellValue(new GridReference(0, colNum), dc.ColumnName);
                                colNum++;
                            }
                            rowNum++;
                        }

                        foreach (DataRow dr in dt.Rows)
                        {
                            colNum = 0;

                            foreach (DataColumn dc in dt.Columns)
                            {
                                sheet.Worksheet.SetCellValue(new GridReference(rowNum, colNum), dr[dc]);
                                colNum++;
                            }
                            rowNum++;
                        }
                    }
                }
                result = output.ToByteArray();
            }

            return result;
        }






    }
}
