﻿using System.Text;

namespace System.Data
{
    static class DataColumnArrayExtensions
    {

        public static string GetFilter(this DataColumn[] columnsForFiltering)
        {
            StringBuilder result = new StringBuilder();
            bool useQuotes;
            for (int i = 0; i < columnsForFiltering.Length; i++)
            {
                DataColumn keyColumn = columnsForFiltering[i];
                if (i > 0) result.Append(" AND ");
                switch (keyColumn.DataType.ToString())
                {
                    case "System.String":
                    case "System.DateTime":
                        useQuotes = true;
                        break;
                    default:
                        useQuotes = false;
                        break;
                }
                result.Append("(" + keyColumn.ColumnName + " = ");
                if (useQuotes) result.Append("'");
                result.Append("{" + i.ToString() + "}");
                if (useQuotes) result.Append("'");
                result.Append(")");
            }
            return result.ToString();
        }

    }
}
