﻿using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace System.Data
{
    public static class DataTableExtensions
    {
        public static void ForEach(this DataTable data, Action<DataRow> action)
        {
            foreach (DataRow dr in data.Rows) action(dr);
        }

        public static List<T> ToList<T>(this DataTable data) where T : new()
        {
            var t = typeof(T);
            List<T> result = new List<T>();
            foreach (var row in data.AsEnumerable())
            {
                var obj = new T();
                foreach (DataColumn dc in data.Columns)
                {
                    try
                    {
                        obj.SetValue(dc.ColumnName, row[dc]);
                    }
                    catch { }
                }
                result.Add(obj);
            }
            return result;
        }

        public static T Intersect<T>(this T data, T withData) where T : DataTable, new()
        {
            if (withData.Rows.Count > data.Rows.Count)
            {
                T tempData = data;
                data = withData;
                withData = tempData;
                tempData = null;
            }

            return Filter(data, withData, FilterType.In);
        }

        public static T NotIn<T>(this T data, T withData) where T : DataTable, new()
        {
            return Filter(data, withData, FilterType.NotIn);
        }

        public static T ExclusiveOr<T>(this T data, T withData) where T : DataTable, new()
        {
            T inter = Intersect(data, withData);
            T result = data.NotIn<T>(inter);
            T secondSet = withData.NotIn<T>(inter);
            foreach (DataRow dr in secondSet.Rows) result.ImportRow(dr);
            return result;
        }

        public static T Union<T>(this T data, T withData) where T : DataTable, new()
        {
            T inter = data.Intersect(withData);
            T result = data.NotIn<T>(inter);
            foreach (DataRow dr in withData.Rows) result.ImportRow(dr);
            return result;
        }

        private enum FilterType
        {
            In,
            NotIn
        }

        private static T Filter<T>(this T data, T withData, FilterType filterType) where T : DataTable, new()
        {
            T result = (T)data.Copy();

            DataColumn[] columsForFiltering = GetColumnsForFiltering(result);
            string filter = columsForFiltering.GetFilter();

            string addedColumnName = "Column" + Guid.NewGuid().ToString();
            result.Columns.Add(new DataColumn(addedColumnName, typeof(int)));

            foreach (DataRow dr in result.Rows)
            {
                dr[addedColumnName] = withData.Select(String.Format(filter, GetData(columsForFiltering, dr))).Length;
            }
            string operand = (filterType == FilterType.In) ? "=" : ">";
            DataRow[] rowsToRemove = result.Select("[" + addedColumnName + "]" + operand + "0");
            foreach (DataRow dr in rowsToRemove) result.Rows.Remove(dr);

            result.Columns.Remove(addedColumnName);
            return result;
        }

        public static void Truncate(this DataTable set)
        {
            foreach (DataRow dr in set.Rows) dr.Delete();
            set.AcceptChanges();
        }

        public static string GetDropTableSql(this DataTable set)
        {
            return "DROP TABLE [" + set.TableName + "] \n";
        }


        public static string GetInsertStatement(this DataTable set)
        {
            var result = string.Empty;

            string sql = "Insert into [" + set.TableName + "] (\n";
            // columns


            foreach (DataColumn column in set.Columns)
            {
                sql += "[" + column.ColumnName + "],";
            }
            sql = sql.TrimEnd(new char[] { ',' });
            sql += ") values (";

            foreach (DataColumn column in set.Columns)
            {
                sql += "?,";
            }
            sql = sql.TrimEnd(new char[] { ',' });
            sql += ")";

            return sql;

        }

        public static string GetCreateTableSql(this DataTable set)
        {
            var result = string.Empty;

            string sql = "CREATE TABLE [" + set.TableName + "] (\n";
            // columns


            foreach (DataColumn column in set.Columns)
            {
                sql += "[" + column.ColumnName + "] " + column.GetSQLType() + ",\n";
            }
            sql = sql.TrimEnd(new char[] { ',', '\n' }) + "\n";

            // primary updateSet
            if (set.PrimaryKey.Length > 0)
            {
                sql += "CONSTRAINT [PK_" + set.TableName + "] PRIMARY KEY CLUSTERED (";
                foreach (DataColumn column in set.PrimaryKey)
                {
                    sql += "[" + column.ColumnName + "],";
                }
                sql = sql.TrimEnd(new char[] { ',' }) + ")";
            }
            sql += ")\n";


            return sql;

        }


        private static object[] GetData(DataColumn[] columnsForFiltering, DataRow dr)
        {
            ArrayList result = new ArrayList();
            foreach (DataColumn dc in columnsForFiltering) result.Add(dr[dc]);
            return result.ToArray();
        }

        private static DataColumn[] GetColumnsForFiltering(DataTable data)
        {
            if (data.PrimaryKey.Length > 0) return data.PrimaryKey;
            List<DataColumn> result = new List<DataColumn>();
            foreach (DataColumn dc in data.Columns) result.Add(dc);
            return result.ToArray();
        }
    }
}
