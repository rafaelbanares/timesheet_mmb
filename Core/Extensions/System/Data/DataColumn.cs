﻿
namespace System.Data
{
    public static class DataColumnExtensions
    {
        internal static string GetSQLType(this DataColumn column)
        {
            return column.DataType.GetSQLType(column.MaxLength, 10, 2);
        }
    }
}
