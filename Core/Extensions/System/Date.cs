﻿
namespace System
{
    /// <summary>
    /// Extension methods for date.
    /// </summary>
    public static class DateExtensions
    {
        public static DateTime FirstDayOfMonth(this DateTime value)
        {
            return value.Date.AddDays(1 - value.Day);
        }
        
        public static DateTime FirstDayOfYear(this DateTime value)
        {
            return value.FirstDayOfMonth().AddMonths(1 - value.Month);
        }
        /// <summary>
        /// Returns a string representing the short date format (yyyy-MM-dd).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToShort(this DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// Returns a string representing the short date format (yyyy-MM-dd).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToShort(this DateTime? value)
        {
            return value.HasValue ? value.Value.ToShort() : "";
        }

        /// <summary>
        /// Returns a string representing the long date format (yyyy-MM-dd HH:mm).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToLong(this DateTime value)
        {
            return value.ToString("yyyy-MM-dd HH:mm");
        }

        /// <summary>
        /// Returns a string representing the long date format (yyyy-MM-dd HH:mm).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToLong(this DateTime? value)
        {
            return value.HasValue ? value.Value.ToLong() : "";
        }
    }
}