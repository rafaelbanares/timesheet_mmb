﻿using System;
using Core.Exchange;

namespace Microsoft.Exchange.WebServices.Data
{
    static class EmailMessageExtensions
    {
        public static ExchangeEmail Wrap(this Microsoft.Exchange.WebServices.Data.EmailMessage item)
        {
            item.CanNotBeNull();
            item.Load();

            ExchangeEmail result = new ExchangeEmail();
            result.Id = item.Id.UniqueId;
            result.CreatedOn = item.DateTimeCreated;
            result.ReceivedOn = item.DateTimeReceived;
            result.SentOn = item.DateTimeSent;
            result.Headers = item.InternetMessageHeaders.Wrap();
            result.Subject = item.Subject;
            result.Sender = item.Sender.Wrap();
            result.From = item.From.Wrap();
            result.Body = item.Body.Text;
            result.IsHtmlBody = (item.Body.BodyType == Microsoft.Exchange.WebServices.Data.BodyType.HTML);
            result.To = item.ToRecipients.Wrap();
            result.BCC = item.BccRecipients.Wrap();
            result.CC = item.CcRecipients.Wrap();
            result.Attachments = item.Attachments.Wrap();
            result.Categories = item.Categories.Wrap();
            return result;
        }
    }
}
