﻿using System;
using System.Collections.Generic;
using Core.Exchange;

namespace Microsoft.Exchange.WebServices.Data
{
    static class AttachmentCollectionExtensions
    {
        public static IEnumerable<ExchangeAttachment> Wrap(this Microsoft.Exchange.WebServices.Data.AttachmentCollection item)
        {
            List<ExchangeAttachment> result = new List<ExchangeAttachment>();
            if (item.IsNotNull())
                foreach (var it in item) result.Add(it.Wrap());
            return result;
        }
    }
}
