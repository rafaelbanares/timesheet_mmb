﻿using System;
using System.Collections.Generic;

namespace Microsoft.Exchange.WebServices.Data
{
    static class InternetMessageHeaderCollectionExtensions
    {

        public static IDictionary<string, string> Wrap(this Microsoft.Exchange.WebServices.Data.InternetMessageHeaderCollection item)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var it in item)
            {
                if (result.ContainsKey(it.Name))
                    result[it.Name] += Environment.NewLine + it.Value;
                else result.Add(it.Name, it.Value);
            }
            return result;
        }
    }
}
