﻿using System;
using Core.Exchange;

namespace Microsoft.Exchange.WebServices.Data
{
    static class AttachmentExtensions
    {


        public static ExchangeAttachment Wrap(this Microsoft.Exchange.WebServices.Data.Attachment item)
        {
            item.CanNotBeNull();
            item.Load();

            if (item is Microsoft.Exchange.WebServices.Data.FileAttachment)
            {
                var attach = item as Microsoft.Exchange.WebServices.Data.FileAttachment;
                ExchangeFileAttachment result = new ExchangeFileAttachment();
                result.Name = attach.Name;
                result.Data = attach.Content;
                return result;
            }
            else
            {
                var attach = item as Microsoft.Exchange.WebServices.Data.ItemAttachment;
                ExchangeItemAttachment result = new ExchangeItemAttachment();
                result.Name = attach.Name;
                result.Id = attach.Id;
                return result;
            }
        }
    }
}
