﻿using System;
using System.Net.Mail;
using Core.Exchange;

namespace Microsoft.Exchange.WebServices.Data
{
    static class EmailAddressExtensions
    {
        public static ExchangeEmailAddress Wrap(this Microsoft.Exchange.WebServices.Data.EmailAddress item)
        {
            item.CanNotBeNull();
            try
            {
                return new ExchangeEmailAddress { Address = item.Address, Name = item.Name };
            }
            catch (Exception x)
            { 
            
            }
            return null;
        }
    }
}
