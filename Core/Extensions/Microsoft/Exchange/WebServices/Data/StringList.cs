﻿using System.Collections.Generic;

namespace Microsoft.Exchange.WebServices.Data
{
    static class StringListExtensions
    {
        public static IEnumerable<string> Wrap(this Microsoft.Exchange.WebServices.Data.StringList item)
        {
            List<string> result = new List<string>();
            foreach (var it in item) result.Add(it);
            return result;
        }
    }
}
