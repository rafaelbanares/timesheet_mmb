﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Core.Exchange;

namespace Microsoft.Exchange.WebServices.Data
{
    static class EmailAddressCollectionExtensions
    {
        public static IEnumerable<ExchangeEmailAddress> Wrap(this Microsoft.Exchange.WebServices.Data.EmailAddressCollection item)
        {
            List<ExchangeEmailAddress> result = new List<ExchangeEmailAddress>();
            if (item.IsNotNull())
                foreach (var it in item) result.Add(it.Wrap());
            return result;
        }
    }
}
