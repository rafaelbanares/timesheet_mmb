﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using Bns.Attendance;

namespace Bns.SpecificProcess
{
    public class ClientSpecificProcess 
    {
        public string Specific { get; set; }

        AttendanceProcessBulk process;
        DBCredential dbCredential;

        public ClientSpecificProcess(DBCredential dbc)
        {
            dbCredential = dbc;
            process = new AttendanceProcessBulk(dbc);
        }

        public void ExecuteBulk()
        {
            process.ExecuteBulk();
            if (Specific != "" && process.ProcessData.Tables.Count > 0)
                processBulkSpecific();
        }
        public void ExecuteOneTrans(string employeeid, DateTime attendancedate, string shiftcode, string restdaycode, string[] trans)
        {
            process.ExecuteOneTrans(employeeid, attendancedate, shiftcode, restdaycode, trans);
            if (Specific != "" && process.ProcessData.Tables.Count > 0)
                processOneTransSpecific();
        }

        private void processBulkSpecific()
        {
            AttendanceSpecific att = new AttendanceSpecific(dbCredential, process.ProcessData);
            if (Specific == "LOWE_ATTENDANCE_PROCESS")
                att.ProcessLowe();
        }

        private void processOneTransSpecific()
        {
            AttendanceSpecific att = new AttendanceSpecific(dbCredential, process.ProcessData);
            if (Specific == "LOWE_ATTENDANCE_PROCESS")
                att.ProcessLowe();
        }

    }
}
