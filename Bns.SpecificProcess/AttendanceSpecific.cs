﻿using System;
using System.Collections.Generic;
using System.Text;

using Bns.Attendance;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Bns.SpecificProcess
{
    class AttendanceSpecific
    {
        DBCredential dbCredential;
        DataSet dsData;
        

        public AttendanceSpecific(DBCredential dbc, DataSet ds)
        {
            dbCredential = dbc;
            dsData = ds;
        }

        public void ProcessLowe()
        {
            //ProcessExcuseOfAbsences();
            //ProcessTimeCredits();
            ProcesesAdjustTardyUTtoLeave();
        }

        private void ProcessExcuseOfAbsences()
        {
            /*
             * needed for batch update:
            Excuse of Absent
                the employee rendered a full sixteen (16) hours of work on the day before the absence; 
                or the employee worked a full seven (7) hours on a Saturday, Sunday or an Official Holiday; 
                or the employee rendered overtime work beyond 12:00 a.m. for three consecutive days, 
                provided that the employee timed in before 10:30 a.m. for those three days.
            */

            DataRow[] drAtt = dsData.Tables[0].Select(" in1 is null or out1 is null ");
            foreach (DataRow dr in drAtt)
                dr.SetModified();

            //... needed for batch update, for time credits
            SqlConnection connection = new SqlConnection(dbCredential.Connstring);
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand command = new SqlCommand(dbCredential.MainDB + ".dbo.usa_AttendanceAbsentOffset", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@CompanyID", dbCredential.CompanyID));
                command.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Char, 15)).SourceColumn = "EmployeeID";
                command.Parameters.Add(new SqlParameter("@Date", SqlDbType.SmallDateTime)).SourceColumn = "Date";
                command.Parameters.Add(new SqlParameter("@DBname", dbCredential.DB));
                command.Parameters.Add(new SqlParameter("@LastUpdBy", dbCredential.UID));
                command.Parameters.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

                adapter.UpdateCommand = command;

                //... if single employee disable batch transaction.
                //adapter.UpdateBatchSize = singleemp ? 1 : 0;
                adapter.UpdateBatchSize = drAtt.Length == 1 ? 1 : 0;
                //adapter.UpdateBatchSize = 0;
                command.UpdatedRowSource = UpdateRowSource.None;
                command.CommandTimeout = 0;

                adapter.Update(drAtt);
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.ToString()));
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        private void ProcessTimeCredits()
        {
            string timecreditDays = ConfigurationManager.AppSettings.Get("Lowe-TimeCreditDays");

            DataRow[] drAtt = dsData.Tables[0].Select(" TardyExempt = 0 AND OTExempt = 1 and in1 is not null and out1 is not null");
            foreach (DataRow dr in drAtt)
                dr.SetModified();

            //... needed for batch update, for time credits
            SqlConnection connection = new SqlConnection(dbCredential.Connstring);
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand command = new SqlCommand(dbCredential.MainDB + ".dbo.usa_AttendanceTimeCreditsAdjustTardy", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@CompanyID", dbCredential.CompanyID));
                command.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Char, 15)).SourceColumn = "EmployeeID";
                command.Parameters.Add(new SqlParameter("@Date", SqlDbType.SmallDateTime)).SourceColumn = "Date";
                command.Parameters.Add(new SqlParameter("@tcdays", timecreditDays));
                command.Parameters.Add(new SqlParameter("@LastUpdBy", dbCredential.UID));
                command.Parameters.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
                command.Parameters.Add(new SqlParameter("@DBname", dbCredential.DB));

                adapter.UpdateCommand = command;

                //... if single employee disable batch transaction.
                //adapter.UpdateBatchSize = singleemp ? 1 : 0;
                adapter.UpdateBatchSize = drAtt.Length == 1 ? 1 : 0;
                //adapter.UpdateBatchSize = 0;
                command.UpdatedRowSource = UpdateRowSource.None;
                command.CommandTimeout = 0;

                adapter.Update(drAtt);
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.ToString()));
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        private void ProcesesAdjustTardyUTtoLeave()
        {
            // * usa_AttendanceOffsetTardyLeave
            //... needed for batch update, to offset tardiness and undertime into vacation leave
            DataRow[] drAtt = dsData.Tables[0].Select(" TardyExempt = 0 "); // and in1 is not null and out1 is not null");
            foreach (DataRow dr in drAtt)
                dr.SetModified();

            //... needed for batch update, for time credits
            SqlConnection connection = new SqlConnection(dbCredential.Connstring);
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                //SqlCommand command = new SqlCommand(dbCredential.MainDB + ".dbo.usa_AttendanceOffsetTardyLeave", connection);
                SqlCommand command = new SqlCommand(dbCredential.DB + ".dbo.usa_AttendanceOffsetTardyLeave", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@CompanyID", dbCredential.CompanyID));
                command.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Char, 15)).SourceColumn = "EmployeeID";
                command.Parameters.Add(new SqlParameter("@Date", SqlDbType.SmallDateTime)).SourceColumn = "Date";
                command.Parameters.Add(new SqlParameter("@OTExempt", SqlDbType.Bit)).SourceColumn = "OTExempt";
                //command.Parameters.Add(new SqlParameter("@DBname", dbCredential.DB));

                adapter.UpdateCommand = command;

                //... if single employee disable batch transaction.
                //adapter.UpdateBatchSize = singleemp ? 1 : 0;
                adapter.UpdateBatchSize = drAtt.Length == 1 ? 1 : 0;
                //adapter.UpdateBatchSize = 0;
                command.UpdatedRowSource = UpdateRowSource.None;
                command.CommandTimeout = 0;

                adapter.Update(drAtt);
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.ToString()));
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }
        
    }
}

