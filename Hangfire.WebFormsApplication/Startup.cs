﻿using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartupAttribute(typeof(Hangfire.WebFormsApplication.Startup))]
namespace Hangfire.WebFormsApplication
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);

            //GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");
            GlobalConfiguration.Configuration.UseSqlServerStorage("Server=.\\SQLEXPRESS2014; UID=sa; PWD=immortal; database=HangfireTest");

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate(
                () => Console.WriteLine("{0} Recurring job completed successfully!", DateTime.Now.ToString()),
                Cron.Minutely);
        }
    }
}
