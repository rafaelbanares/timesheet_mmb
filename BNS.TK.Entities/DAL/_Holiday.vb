
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

Namespace BNS.TK.Entities

    Public MustInherit Class _Holiday
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "Holiday"
            Me.MappingName = "Holiday"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_HolidayLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_Holiday.Parameters.ID, ID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_HolidayLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property CompanyID() As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property ID() As SqlParameter
                Get
                    Return New SqlParameter("@ID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property [Date]() As SqlParameter
                Get
                    Return New SqlParameter("@Date", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Description() As SqlParameter
                Get
                    Return New SqlParameter("@Description", SqlDbType.VarChar, 50)
                End Get
            End Property

            Public Shared ReadOnly Property Legal() As SqlParameter
                Get
                    Return New SqlParameter("@Legal", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Halfday() As SqlParameter
                Get
                    Return New SqlParameter("@Halfday", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property HalfdayAM() As SqlParameter
                Get
                    Return New SqlParameter("@HalfdayAM", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedBy() As SqlParameter
                Get
                    Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate() As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy() As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate() As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const CompanyID As String = "CompanyID"
            Public Const ID As String = "ID"
            Public Const [Date] As String = "Date"
            Public Const Description As String = "Description"
            Public Const Legal As String = "Legal"
            Public Const Halfday As String = "Halfday"
            Public Const HalfdayAM As String = "HalfdayAM"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Holiday.PropertyNames.CompanyID
                    ht(ID) = _Holiday.PropertyNames.ID
                    ht([Date]) = _Holiday.PropertyNames.Date
                    ht(Description) = _Holiday.PropertyNames.Description
                    ht(Legal) = _Holiday.PropertyNames.Legal
                    ht(Halfday) = _Holiday.PropertyNames.Halfday
                    ht(HalfdayAM) = _Holiday.PropertyNames.HalfdayAM
                    ht(CreatedBy) = _Holiday.PropertyNames.CreatedBy
                    ht(CreatedDate) = _Holiday.PropertyNames.CreatedDate
                    ht(LastUpdBy) = _Holiday.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _Holiday.PropertyNames.LastUpdDate

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const CompanyID As String = "CompanyID"
            Public Const ID As String = "ID"
            Public Const [Date] As String = "Date"
            Public Const Description As String = "Description"
            Public Const Legal As String = "Legal"
            Public Const Halfday As String = "Halfday"
            Public Const HalfdayAM As String = "HalfdayAM"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Holiday.ColumnNames.CompanyID
                    ht(ID) = _Holiday.ColumnNames.ID
                    ht([Date]) = _Holiday.ColumnNames.Date
                    ht(Description) = _Holiday.ColumnNames.Description
                    ht(Legal) = _Holiday.ColumnNames.Legal
                    ht(Halfday) = _Holiday.ColumnNames.Halfday
                    ht(HalfdayAM) = _Holiday.ColumnNames.HalfdayAM
                    ht(CreatedBy) = _Holiday.ColumnNames.CreatedBy
                    ht(CreatedDate) = _Holiday.ColumnNames.CreatedDate
                    ht(LastUpdBy) = _Holiday.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _Holiday.ColumnNames.LastUpdDate

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const CompanyID As String = "s_CompanyID"
            Public Const ID As String = "s_ID"
            Public Const [Date] As String = "s_Date"
            Public Const Description As String = "s_Description"
            Public Const Legal As String = "s_Legal"
            Public Const Halfday As String = "s_Halfday"
            Public Const HalfdayAM As String = "s_HalfdayAM"
            Public Const CreatedBy As String = "s_CreatedBy"
            Public Const CreatedDate As String = "s_CreatedDate"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property CompanyID() As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property ID() As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.ID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.ID, Value)
            End Set
        End Property

        Public Overridable Property [Date]() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Date)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Date, Value)
            End Set
        End Property

        Public Overridable Property Description() As String
            Get
                Return MyBase.GetString(ColumnNames.Description)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Description, Value)
            End Set
        End Property

        Public Overridable Property Legal() As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Legal)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Legal, Value)
            End Set
        End Property

        Public Overridable Property Halfday() As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Halfday)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Halfday, Value)
            End Set
        End Property

        Public Overridable Property HalfdayAM() As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.HalfdayAM)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.HalfdayAM, Value)
            End Set
        End Property

        Public Overridable Property CreatedBy() As String
            Get
                Return MyBase.GetString(ColumnNames.CreatedBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CreatedBy, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy() As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_CompanyID() As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ID() As String
            Get
                If Me.IsColumnNull(ColumnNames.ID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.ID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ID)
                Else
                    Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Date() As String
            Get
                If Me.IsColumnNull(ColumnNames.Date) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Date)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Date)
                Else
                    Me.Date = MyBase.SetDateTimeAsString(ColumnNames.Date, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Description() As String
            Get
                If Me.IsColumnNull(ColumnNames.Description) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Description)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Description)
                Else
                    Me.Description = MyBase.SetStringAsString(ColumnNames.Description, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Legal() As String
            Get
                If Me.IsColumnNull(ColumnNames.Legal) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Legal)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Legal)
                Else
                    Me.Legal = MyBase.SetBooleanAsString(ColumnNames.Legal, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Halfday() As String
            Get
                If Me.IsColumnNull(ColumnNames.Halfday) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Halfday)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Halfday)
                Else
                    Me.Halfday = MyBase.SetBooleanAsString(ColumnNames.Halfday, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_HalfdayAM() As String
            Get
                If Me.IsColumnNull(ColumnNames.HalfdayAM) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.HalfdayAM)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.HalfdayAM)
                Else
                    Me.HalfdayAM = MyBase.SetBooleanAsString(ColumnNames.HalfdayAM, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedBy() As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedBy)
                Else
                    Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate() As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy() As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate() As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff() As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property [Date]() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Date, Parameters.Date)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Description() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Description, Parameters.Description)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Legal() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Legal, Parameters.Legal)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Halfday() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Halfday, Parameters.Halfday)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property HalfdayAM() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.HalfdayAM, Parameters.HalfdayAM)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property ID() As WhereParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property [Date]() As WhereParameter
                Get
                    If _Date_W Is Nothing Then
                        _Date_W = TearOff.Date
                    End If
                    Return _Date_W
                End Get
            End Property

            Public ReadOnly Property Description() As WhereParameter
                Get
                    If _Description_W Is Nothing Then
                        _Description_W = TearOff.Description
                    End If
                    Return _Description_W
                End Get
            End Property

            Public ReadOnly Property Legal() As WhereParameter
                Get
                    If _Legal_W Is Nothing Then
                        _Legal_W = TearOff.Legal
                    End If
                    Return _Legal_W
                End Get
            End Property

            Public ReadOnly Property Halfday() As WhereParameter
                Get
                    If _Halfday_W Is Nothing Then
                        _Halfday_W = TearOff.Halfday
                    End If
                    Return _Halfday_W
                End Get
            End Property

            Public ReadOnly Property HalfdayAM() As WhereParameter
                Get
                    If _HalfdayAM_W Is Nothing Then
                        _HalfdayAM_W = TearOff.HalfdayAM
                    End If
                    Return _HalfdayAM_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As WhereParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Private _CompanyID_W As WhereParameter = Nothing
            Private _ID_W As WhereParameter = Nothing
            Private _Date_W As WhereParameter = Nothing
            Private _Description_W As WhereParameter = Nothing
            Private _Legal_W As WhereParameter = Nothing
            Private _Halfday_W As WhereParameter = Nothing
            Private _HalfdayAM_W As WhereParameter = Nothing
            Private _CreatedBy_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _CompanyID_W = Nothing
                _ID_W = Nothing
                _Date_W = Nothing
                _Description_W = Nothing
                _Legal_W = Nothing
                _Halfday_W = Nothing
                _HalfdayAM_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff() As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property [Date]() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Date, Parameters.Date)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Description() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Description, Parameters.Description)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Legal() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Legal, Parameters.Legal)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Halfday() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Halfday, Parameters.Halfday)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property HalfdayAM() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.HalfdayAM, Parameters.HalfdayAM)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property ID() As AggregateParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property [Date]() As AggregateParameter
                Get
                    If _Date_W Is Nothing Then
                        _Date_W = TearOff.Date
                    End If
                    Return _Date_W
                End Get
            End Property

            Public ReadOnly Property Description() As AggregateParameter
                Get
                    If _Description_W Is Nothing Then
                        _Description_W = TearOff.Description
                    End If
                    Return _Description_W
                End Get
            End Property

            Public ReadOnly Property Legal() As AggregateParameter
                Get
                    If _Legal_W Is Nothing Then
                        _Legal_W = TearOff.Legal
                    End If
                    Return _Legal_W
                End Get
            End Property

            Public ReadOnly Property Halfday() As AggregateParameter
                Get
                    If _Halfday_W Is Nothing Then
                        _Halfday_W = TearOff.Halfday
                    End If
                    Return _Halfday_W
                End Get
            End Property

            Public ReadOnly Property HalfdayAM() As AggregateParameter
                Get
                    If _HalfdayAM_W Is Nothing Then
                        _HalfdayAM_W = TearOff.HalfdayAM
                    End If
                    Return _HalfdayAM_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As AggregateParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Private _CompanyID_W As AggregateParameter = Nothing
            Private _ID_W As AggregateParameter = Nothing
            Private _Date_W As AggregateParameter = Nothing
            Private _Description_W As AggregateParameter = Nothing
            Private _Legal_W As AggregateParameter = Nothing
            Private _Halfday_W As AggregateParameter = Nothing
            Private _HalfdayAM_W As AggregateParameter = Nothing
            Private _CreatedBy_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _CompanyID_W = Nothing
                _ID_W = Nothing
                _Date_W = Nothing
                _Description_W = Nothing
                _Legal_W = Nothing
                _Halfday_W = Nothing
                _HalfdayAM_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_HolidayInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.ID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_HolidayUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_HolidayDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Date)
            p.SourceColumn = ColumnNames.Date
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Description)
            p.SourceColumn = ColumnNames.Description
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Legal)
            p.SourceColumn = ColumnNames.Legal
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Halfday)
            p.SourceColumn = ColumnNames.Halfday
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.HalfdayAM)
            p.SourceColumn = ColumnNames.HalfdayAM
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedBy)
            p.SourceColumn = ColumnNames.CreatedBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

