
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _Employee
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "Employee"
			Me.MappingName = "Employee"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_EmployeeLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal EmployeeID As String) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_Employee.Parameters.CompanyID, CompanyID)
		parameters.Add(_Employee.Parameters.EmployeeID, EmployeeID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_EmployeeLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property SwipeID As SqlParameter
			Get
				Return New SqlParameter("@SwipeID", SqlDbType.VarChar, 50)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayrollType As SqlParameter
			Get
				Return New SqlParameter("@PayrollType", SqlDbType.Char, 1)
			End Get
		End Property
		
		Public Shared ReadOnly Property Approver As SqlParameter
			Get
				Return New SqlParameter("@Approver", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ApproverLeave As SqlParameter
			Get
				Return New SqlParameter("@ApproverLeave", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property ApproverOT As SqlParameter
			Get
				Return New SqlParameter("@ApproverOT", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property ApprvrOTCompID As SqlParameter
			Get
				Return New SqlParameter("@ApprvrOTCompID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property ApprvrLvCompID As SqlParameter
			Get
				Return New SqlParameter("@ApprvrLvCompID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level1 As SqlParameter
			Get
				Return New SqlParameter("@Level1", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level2 As SqlParameter
			Get
				Return New SqlParameter("@Level2", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level3 As SqlParameter
			Get
				Return New SqlParameter("@Level3", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level4 As SqlParameter
			Get
				Return New SqlParameter("@Level4", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastName As SqlParameter
			Get
				Return New SqlParameter("@LastName", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property FirstName As SqlParameter
			Get
				Return New SqlParameter("@FirstName", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property MiddleName As SqlParameter
			Get
				Return New SqlParameter("@MiddleName", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property FullName As SqlParameter
			Get
				Return New SqlParameter("@FullName", SqlDbType.VarChar, 94)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmploymentType As SqlParameter
			Get
				Return New SqlParameter("@EmploymentType", SqlDbType.Char, 1)
			End Get
		End Property
		
		Public Shared ReadOnly Property Gender As SqlParameter
			Get
				Return New SqlParameter("@Gender", SqlDbType.Char, 1)
			End Get
		End Property
		
		Public Shared ReadOnly Property Address1 As SqlParameter
			Get
				Return New SqlParameter("@Address1", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Address2 As SqlParameter
			Get
				Return New SqlParameter("@Address2", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Position As SqlParameter
			Get
				Return New SqlParameter("@Position", SqlDbType.VarChar, 50)
			End Get
		End Property
		
		Public Shared ReadOnly Property CostCenter As SqlParameter
			Get
				Return New SqlParameter("@CostCenter", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property BirthDate As SqlParameter
			Get
				Return New SqlParameter("@BirthDate", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property DateEmployed As SqlParameter
			Get
				Return New SqlParameter("@DateEmployed", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property DateRegularized As SqlParameter
			Get
				Return New SqlParameter("@DateRegularized", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property DateTerminated As SqlParameter
			Get
				Return New SqlParameter("@DateTerminated", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Rank As SqlParameter
			Get
				Return New SqlParameter("@Rank", SqlDbType.Char, 1)
			End Get
		End Property
		
		Public Shared ReadOnly Property TardyExempt As SqlParameter
			Get
				Return New SqlParameter("@TardyExempt", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property UTExempt As SqlParameter
			Get
				Return New SqlParameter("@UTExempt", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OTExempt As SqlParameter
			Get
				Return New SqlParameter("@OTExempt", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property NonSwiper As SqlParameter
			Get
				Return New SqlParameter("@NonSwiper", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ShiftCode As SqlParameter
			Get
				Return New SqlParameter("@ShiftCode", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property RestDay As SqlParameter
			Get
				Return New SqlParameter("@RestDay", SqlDbType.VarChar, 7)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level1Disp As SqlParameter
			Get
				Return New SqlParameter("@Level1Disp", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level2Disp As SqlParameter
			Get
				Return New SqlParameter("@Level2Disp", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level3Disp As SqlParameter
			Get
				Return New SqlParameter("@Level3Disp", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level4Disp As SqlParameter
			Get
				Return New SqlParameter("@Level4Disp", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Password As SqlParameter
			Get
				Return New SqlParameter("@Password", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property PwdNeverExpire As SqlParameter
			Get
				Return New SqlParameter("@PwdNeverExpire", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PwdExpireDate As SqlParameter
			Get
				Return New SqlParameter("@PwdExpireDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property AccountIsLocked As SqlParameter
			Get
				Return New SqlParameter("@AccountIsLocked", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property UserLevel As SqlParameter
			Get
				Return New SqlParameter("@UserLevel", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Email As SqlParameter
			Get
				Return New SqlParameter("@Email", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Question As SqlParameter
			Get
				Return New SqlParameter("@Question", SqlDbType.VarChar, 500)
			End Get
		End Property
		
		Public Shared ReadOnly Property Answer As SqlParameter
			Get
				Return New SqlParameter("@Answer", SqlDbType.VarChar, 50)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const SwipeID As String = "SwipeID"
        Public Const PayrollType As String = "PayrollType"
        Public Const Approver As String = "Approver"
        Public Const ApproverLeave As String = "ApproverLeave"
        Public Const ApproverOT As String = "ApproverOT"
        Public Const ApprvrOTCompID As String = "ApprvrOTCompID"
        Public Const ApprvrLvCompID As String = "ApprvrLvCompID"
        Public Const Level1 As String = "Level1"
        Public Const Level2 As String = "Level2"
        Public Const Level3 As String = "Level3"
        Public Const Level4 As String = "Level4"
        Public Const LastName As String = "LastName"
        Public Const FirstName As String = "FirstName"
        Public Const MiddleName As String = "MiddleName"
        Public Const FullName As String = "FullName"
        Public Const EmploymentType As String = "EmploymentType"
        Public Const Gender As String = "Gender"
        Public Const Address1 As String = "Address1"
        Public Const Address2 As String = "Address2"
        Public Const Position As String = "Position"
        Public Const CostCenter As String = "CostCenter"
        Public Const BirthDate As String = "BirthDate"
        Public Const DateEmployed As String = "DateEmployed"
        Public Const DateRegularized As String = "DateRegularized"
        Public Const DateTerminated As String = "DateTerminated"
        Public Const Rank As String = "Rank"
        Public Const TardyExempt As String = "TardyExempt"
        Public Const UTExempt As String = "UTExempt"
        Public Const OTExempt As String = "OTExempt"
        Public Const NonSwiper As String = "NonSwiper"
        Public Const ShiftCode As String = "ShiftCode"
        Public Const RestDay As String = "RestDay"
        Public Const Level1Disp As String = "Level1Disp"
        Public Const Level2Disp As String = "Level2Disp"
        Public Const Level3Disp As String = "Level3Disp"
        Public Const Level4Disp As String = "Level4Disp"
        Public Const Password As String = "Password"
        Public Const PwdNeverExpire As String = "PwdNeverExpire"
        Public Const PwdExpireDate As String = "PwdExpireDate"
        Public Const AccountIsLocked As String = "AccountIsLocked"
        Public Const UserLevel As String = "UserLevel"
        Public Const Email As String = "Email"
        Public Const Question As String = "Question"
        Public Const Answer As String = "Answer"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(CompanyID) = _Employee.PropertyNames.CompanyID
				ht(EmployeeID) = _Employee.PropertyNames.EmployeeID
				ht(SwipeID) = _Employee.PropertyNames.SwipeID
				ht(PayrollType) = _Employee.PropertyNames.PayrollType
				ht(Approver) = _Employee.PropertyNames.Approver
				ht(ApproverLeave) = _Employee.PropertyNames.ApproverLeave
				ht(ApproverOT) = _Employee.PropertyNames.ApproverOT
				ht(ApprvrOTCompID) = _Employee.PropertyNames.ApprvrOTCompID
				ht(ApprvrLvCompID) = _Employee.PropertyNames.ApprvrLvCompID
				ht(Level1) = _Employee.PropertyNames.Level1
				ht(Level2) = _Employee.PropertyNames.Level2
				ht(Level3) = _Employee.PropertyNames.Level3
				ht(Level4) = _Employee.PropertyNames.Level4
				ht(LastName) = _Employee.PropertyNames.LastName
				ht(FirstName) = _Employee.PropertyNames.FirstName
				ht(MiddleName) = _Employee.PropertyNames.MiddleName
				ht(FullName) = _Employee.PropertyNames.FullName
				ht(EmploymentType) = _Employee.PropertyNames.EmploymentType
				ht(Gender) = _Employee.PropertyNames.Gender
				ht(Address1) = _Employee.PropertyNames.Address1
				ht(Address2) = _Employee.PropertyNames.Address2
				ht(Position) = _Employee.PropertyNames.Position
				ht(CostCenter) = _Employee.PropertyNames.CostCenter
				ht(BirthDate) = _Employee.PropertyNames.BirthDate
				ht(DateEmployed) = _Employee.PropertyNames.DateEmployed
				ht(DateRegularized) = _Employee.PropertyNames.DateRegularized
				ht(DateTerminated) = _Employee.PropertyNames.DateTerminated
				ht(Rank) = _Employee.PropertyNames.Rank
				ht(TardyExempt) = _Employee.PropertyNames.TardyExempt
				ht(UTExempt) = _Employee.PropertyNames.UTExempt
				ht(OTExempt) = _Employee.PropertyNames.OTExempt
				ht(NonSwiper) = _Employee.PropertyNames.NonSwiper
				ht(ShiftCode) = _Employee.PropertyNames.ShiftCode
				ht(RestDay) = _Employee.PropertyNames.RestDay
				ht(Level1Disp) = _Employee.PropertyNames.Level1Disp
				ht(Level2Disp) = _Employee.PropertyNames.Level2Disp
				ht(Level3Disp) = _Employee.PropertyNames.Level3Disp
				ht(Level4Disp) = _Employee.PropertyNames.Level4Disp
				ht(Password) = _Employee.PropertyNames.Password
				ht(PwdNeverExpire) = _Employee.PropertyNames.PwdNeverExpire
				ht(PwdExpireDate) = _Employee.PropertyNames.PwdExpireDate
				ht(AccountIsLocked) = _Employee.PropertyNames.AccountIsLocked
				ht(UserLevel) = _Employee.PropertyNames.UserLevel
				ht(Email) = _Employee.PropertyNames.Email
				ht(Question) = _Employee.PropertyNames.Question
				ht(Answer) = _Employee.PropertyNames.Answer
				ht(CreatedBy) = _Employee.PropertyNames.CreatedBy
				ht(CreatedDate) = _Employee.PropertyNames.CreatedDate
				ht(LastUpdBy) = _Employee.PropertyNames.LastUpdBy
				ht(LastUpdDate) = _Employee.PropertyNames.LastUpdDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const SwipeID As String = "SwipeID"
        Public Const PayrollType As String = "PayrollType"
        Public Const Approver As String = "Approver"
        Public Const ApproverLeave As String = "ApproverLeave"
        Public Const ApproverOT As String = "ApproverOT"
        Public Const ApprvrOTCompID As String = "ApprvrOTCompID"
        Public Const ApprvrLvCompID As String = "ApprvrLvCompID"
        Public Const Level1 As String = "Level1"
        Public Const Level2 As String = "Level2"
        Public Const Level3 As String = "Level3"
        Public Const Level4 As String = "Level4"
        Public Const LastName As String = "LastName"
        Public Const FirstName As String = "FirstName"
        Public Const MiddleName As String = "MiddleName"
        Public Const FullName As String = "FullName"
        Public Const EmploymentType As String = "EmploymentType"
        Public Const Gender As String = "Gender"
        Public Const Address1 As String = "Address1"
        Public Const Address2 As String = "Address2"
        Public Const Position As String = "Position"
        Public Const CostCenter As String = "CostCenter"
        Public Const BirthDate As String = "BirthDate"
        Public Const DateEmployed As String = "DateEmployed"
        Public Const DateRegularized As String = "DateRegularized"
        Public Const DateTerminated As String = "DateTerminated"
        Public Const Rank As String = "Rank"
        Public Const TardyExempt As String = "TardyExempt"
        Public Const UTExempt As String = "UTExempt"
        Public Const OTExempt As String = "OTExempt"
        Public Const NonSwiper As String = "NonSwiper"
        Public Const ShiftCode As String = "ShiftCode"
        Public Const RestDay As String = "RestDay"
        Public Const Level1Disp As String = "Level1Disp"
        Public Const Level2Disp As String = "Level2Disp"
        Public Const Level3Disp As String = "Level3Disp"
        Public Const Level4Disp As String = "Level4Disp"
        Public Const Password As String = "Password"
        Public Const PwdNeverExpire As String = "PwdNeverExpire"
        Public Const PwdExpireDate As String = "PwdExpireDate"
        Public Const AccountIsLocked As String = "AccountIsLocked"
        Public Const UserLevel As String = "UserLevel"
        Public Const Email As String = "Email"
        Public Const Question As String = "Question"
        Public Const Answer As String = "Answer"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(CompanyID) = _Employee.ColumnNames.CompanyID
				ht(EmployeeID) = _Employee.ColumnNames.EmployeeID
				ht(SwipeID) = _Employee.ColumnNames.SwipeID
				ht(PayrollType) = _Employee.ColumnNames.PayrollType
				ht(Approver) = _Employee.ColumnNames.Approver
				ht(ApproverLeave) = _Employee.ColumnNames.ApproverLeave
				ht(ApproverOT) = _Employee.ColumnNames.ApproverOT
				ht(ApprvrOTCompID) = _Employee.ColumnNames.ApprvrOTCompID
				ht(ApprvrLvCompID) = _Employee.ColumnNames.ApprvrLvCompID
				ht(Level1) = _Employee.ColumnNames.Level1
				ht(Level2) = _Employee.ColumnNames.Level2
				ht(Level3) = _Employee.ColumnNames.Level3
				ht(Level4) = _Employee.ColumnNames.Level4
				ht(LastName) = _Employee.ColumnNames.LastName
				ht(FirstName) = _Employee.ColumnNames.FirstName
				ht(MiddleName) = _Employee.ColumnNames.MiddleName
				ht(FullName) = _Employee.ColumnNames.FullName
				ht(EmploymentType) = _Employee.ColumnNames.EmploymentType
				ht(Gender) = _Employee.ColumnNames.Gender
				ht(Address1) = _Employee.ColumnNames.Address1
				ht(Address2) = _Employee.ColumnNames.Address2
				ht(Position) = _Employee.ColumnNames.Position
				ht(CostCenter) = _Employee.ColumnNames.CostCenter
				ht(BirthDate) = _Employee.ColumnNames.BirthDate
				ht(DateEmployed) = _Employee.ColumnNames.DateEmployed
				ht(DateRegularized) = _Employee.ColumnNames.DateRegularized
				ht(DateTerminated) = _Employee.ColumnNames.DateTerminated
				ht(Rank) = _Employee.ColumnNames.Rank
				ht(TardyExempt) = _Employee.ColumnNames.TardyExempt
				ht(UTExempt) = _Employee.ColumnNames.UTExempt
				ht(OTExempt) = _Employee.ColumnNames.OTExempt
				ht(NonSwiper) = _Employee.ColumnNames.NonSwiper
				ht(ShiftCode) = _Employee.ColumnNames.ShiftCode
				ht(RestDay) = _Employee.ColumnNames.RestDay
				ht(Level1Disp) = _Employee.ColumnNames.Level1Disp
				ht(Level2Disp) = _Employee.ColumnNames.Level2Disp
				ht(Level3Disp) = _Employee.ColumnNames.Level3Disp
				ht(Level4Disp) = _Employee.ColumnNames.Level4Disp
				ht(Password) = _Employee.ColumnNames.Password
				ht(PwdNeverExpire) = _Employee.ColumnNames.PwdNeverExpire
				ht(PwdExpireDate) = _Employee.ColumnNames.PwdExpireDate
				ht(AccountIsLocked) = _Employee.ColumnNames.AccountIsLocked
				ht(UserLevel) = _Employee.ColumnNames.UserLevel
				ht(Email) = _Employee.ColumnNames.Email
				ht(Question) = _Employee.ColumnNames.Question
				ht(Answer) = _Employee.ColumnNames.Answer
				ht(CreatedBy) = _Employee.ColumnNames.CreatedBy
				ht(CreatedDate) = _Employee.ColumnNames.CreatedDate
				ht(LastUpdBy) = _Employee.ColumnNames.LastUpdBy
				ht(LastUpdDate) = _Employee.ColumnNames.LastUpdDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const SwipeID As String = "s_SwipeID"
        Public Const PayrollType As String = "s_PayrollType"
        Public Const Approver As String = "s_Approver"
        Public Const ApproverLeave As String = "s_ApproverLeave"
        Public Const ApproverOT As String = "s_ApproverOT"
        Public Const ApprvrOTCompID As String = "s_ApprvrOTCompID"
        Public Const ApprvrLvCompID As String = "s_ApprvrLvCompID"
        Public Const Level1 As String = "s_Level1"
        Public Const Level2 As String = "s_Level2"
        Public Const Level3 As String = "s_Level3"
        Public Const Level4 As String = "s_Level4"
        Public Const LastName As String = "s_LastName"
        Public Const FirstName As String = "s_FirstName"
        Public Const MiddleName As String = "s_MiddleName"
        Public Const FullName As String = "s_FullName"
        Public Const EmploymentType As String = "s_EmploymentType"
        Public Const Gender As String = "s_Gender"
        Public Const Address1 As String = "s_Address1"
        Public Const Address2 As String = "s_Address2"
        Public Const Position As String = "s_Position"
        Public Const CostCenter As String = "s_CostCenter"
        Public Const BirthDate As String = "s_BirthDate"
        Public Const DateEmployed As String = "s_DateEmployed"
        Public Const DateRegularized As String = "s_DateRegularized"
        Public Const DateTerminated As String = "s_DateTerminated"
        Public Const Rank As String = "s_Rank"
        Public Const TardyExempt As String = "s_TardyExempt"
        Public Const UTExempt As String = "s_UTExempt"
        Public Const OTExempt As String = "s_OTExempt"
        Public Const NonSwiper As String = "s_NonSwiper"
        Public Const ShiftCode As String = "s_ShiftCode"
        Public Const RestDay As String = "s_RestDay"
        Public Const Level1Disp As String = "s_Level1Disp"
        Public Const Level2Disp As String = "s_Level2Disp"
        Public Const Level3Disp As String = "s_Level3Disp"
        Public Const Level4Disp As String = "s_Level4Disp"
        Public Const Password As String = "s_Password"
        Public Const PwdNeverExpire As String = "s_PwdNeverExpire"
        Public Const PwdExpireDate As String = "s_PwdExpireDate"
        Public Const AccountIsLocked As String = "s_AccountIsLocked"
        Public Const UserLevel As String = "s_UserLevel"
        Public Const Email As String = "s_Email"
        Public Const Question As String = "s_Question"
        Public Const Answer As String = "s_Answer"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property SwipeID As String
			Get
				Return MyBase.GetString(ColumnNames.SwipeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.SwipeID, Value)
			End Set
		End Property

		Public Overridable Property PayrollType As String
			Get
				Return MyBase.GetString(ColumnNames.PayrollType)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.PayrollType, Value)
			End Set
		End Property

		Public Overridable Property Approver As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.Approver)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.Approver, Value)
			End Set
		End Property

		Public Overridable Property ApproverLeave As String
			Get
				Return MyBase.GetString(ColumnNames.ApproverLeave)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ApproverLeave, Value)
			End Set
		End Property

		Public Overridable Property ApproverOT As String
			Get
				Return MyBase.GetString(ColumnNames.ApproverOT)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ApproverOT, Value)
			End Set
		End Property

		Public Overridable Property ApprvrOTCompID As String
			Get
				Return MyBase.GetString(ColumnNames.ApprvrOTCompID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ApprvrOTCompID, Value)
			End Set
		End Property

		Public Overridable Property ApprvrLvCompID As String
			Get
				Return MyBase.GetString(ColumnNames.ApprvrLvCompID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ApprvrLvCompID, Value)
			End Set
		End Property

		Public Overridable Property Level1 As String
			Get
				Return MyBase.GetString(ColumnNames.Level1)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level1, Value)
			End Set
		End Property

		Public Overridable Property Level2 As String
			Get
				Return MyBase.GetString(ColumnNames.Level2)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level2, Value)
			End Set
		End Property

		Public Overridable Property Level3 As String
			Get
				Return MyBase.GetString(ColumnNames.Level3)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level3, Value)
			End Set
		End Property

		Public Overridable Property Level4 As String
			Get
				Return MyBase.GetString(ColumnNames.Level4)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level4, Value)
			End Set
		End Property

		Public Overridable Property LastName As String
			Get
				Return MyBase.GetString(ColumnNames.LastName)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastName, Value)
			End Set
		End Property

		Public Overridable Property FirstName As String
			Get
				Return MyBase.GetString(ColumnNames.FirstName)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.FirstName, Value)
			End Set
		End Property

		Public Overridable Property MiddleName As String
			Get
				Return MyBase.GetString(ColumnNames.MiddleName)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.MiddleName, Value)
			End Set
		End Property

		Public Overridable ReadOnly Property FullName As String
			Get
				Return MyBase.GetString(ColumnNames.FullName)
			End Get
		End Property

		Public Overridable Property EmploymentType As String
			Get
				Return MyBase.GetString(ColumnNames.EmploymentType)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmploymentType, Value)
			End Set
		End Property

		Public Overridable Property Gender As String
			Get
				Return MyBase.GetString(ColumnNames.Gender)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Gender, Value)
			End Set
		End Property

		Public Overridable Property Address1 As String
			Get
				Return MyBase.GetString(ColumnNames.Address1)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Address1, Value)
			End Set
		End Property

		Public Overridable Property Address2 As String
			Get
				Return MyBase.GetString(ColumnNames.Address2)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Address2, Value)
			End Set
		End Property

		Public Overridable Property Position As String
			Get
				Return MyBase.GetString(ColumnNames.Position)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Position, Value)
			End Set
		End Property

		Public Overridable Property CostCenter As String
			Get
				Return MyBase.GetString(ColumnNames.CostCenter)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CostCenter, Value)
			End Set
		End Property

		Public Overridable Property BirthDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.BirthDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.BirthDate, Value)
			End Set
		End Property

		Public Overridable Property DateEmployed As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.DateEmployed)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.DateEmployed, Value)
			End Set
		End Property

		Public Overridable Property DateRegularized As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.DateRegularized)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.DateRegularized, Value)
			End Set
		End Property

		Public Overridable Property DateTerminated As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.DateTerminated)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.DateTerminated, Value)
			End Set
		End Property

		Public Overridable Property Rank As String
			Get
				Return MyBase.GetString(ColumnNames.Rank)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Rank, Value)
			End Set
		End Property

		Public Overridable Property TardyExempt As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.TardyExempt)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.TardyExempt, Value)
			End Set
		End Property

		Public Overridable Property UTExempt As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.UTExempt)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.UTExempt, Value)
			End Set
		End Property

		Public Overridable Property OTExempt As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.OTExempt)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.OTExempt, Value)
			End Set
		End Property

		Public Overridable Property NonSwiper As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.NonSwiper)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.NonSwiper, Value)
			End Set
		End Property

		Public Overridable Property ShiftCode As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ShiftCode)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ShiftCode, Value)
			End Set
		End Property

		Public Overridable Property RestDay As String
			Get
				Return MyBase.GetString(ColumnNames.RestDay)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.RestDay, Value)
			End Set
		End Property

		Public Overridable Property Level1Disp As String
			Get
				Return MyBase.GetString(ColumnNames.Level1Disp)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level1Disp, Value)
			End Set
		End Property

		Public Overridable Property Level2Disp As String
			Get
				Return MyBase.GetString(ColumnNames.Level2Disp)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level2Disp, Value)
			End Set
		End Property

		Public Overridable Property Level3Disp As String
			Get
				Return MyBase.GetString(ColumnNames.Level3Disp)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level3Disp, Value)
			End Set
		End Property

		Public Overridable Property Level4Disp As String
			Get
				Return MyBase.GetString(ColumnNames.Level4Disp)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level4Disp, Value)
			End Set
		End Property

		Public Overridable Property Password As String
			Get
				Return MyBase.GetString(ColumnNames.Password)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Password, Value)
			End Set
		End Property

		Public Overridable Property PwdNeverExpire As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.PwdNeverExpire)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.PwdNeverExpire, Value)
			End Set
		End Property

		Public Overridable Property PwdExpireDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PwdExpireDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PwdExpireDate, Value)
			End Set
		End Property

		Public Overridable Property AccountIsLocked As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.AccountIsLocked)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.AccountIsLocked, Value)
			End Set
		End Property

		Public Overridable Property UserLevel As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.UserLevel)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.UserLevel, Value)
			End Set
		End Property

		Public Overridable Property Email As String
			Get
				Return MyBase.GetString(ColumnNames.Email)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Email, Value)
			End Set
		End Property

		Public Overridable Property Question As String
			Get
				Return MyBase.GetString(ColumnNames.Question)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Question, Value)
			End Set
		End Property

		Public Overridable Property Answer As String
			Get
				Return MyBase.GetString(ColumnNames.Answer)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Answer, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_SwipeID As String
			Get
				If Me.IsColumnNull(ColumnNames.SwipeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.SwipeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.SwipeID)
				Else
					Me.SwipeID = MyBase.SetStringAsString(ColumnNames.SwipeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayrollType As String
			Get
				If Me.IsColumnNull(ColumnNames.PayrollType) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.PayrollType)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayrollType)
				Else
					Me.PayrollType = MyBase.SetStringAsString(ColumnNames.PayrollType, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Approver As String
			Get
				If Me.IsColumnNull(ColumnNames.Approver) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.Approver)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Approver)
				Else
					Me.Approver = MyBase.SetBooleanAsString(ColumnNames.Approver, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ApproverLeave As String
			Get
				If Me.IsColumnNull(ColumnNames.ApproverLeave) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ApproverLeave)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ApproverLeave)
				Else
					Me.ApproverLeave = MyBase.SetStringAsString(ColumnNames.ApproverLeave, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ApproverOT As String
			Get
				If Me.IsColumnNull(ColumnNames.ApproverOT) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ApproverOT)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ApproverOT)
				Else
					Me.ApproverOT = MyBase.SetStringAsString(ColumnNames.ApproverOT, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ApprvrOTCompID As String
			Get
				If Me.IsColumnNull(ColumnNames.ApprvrOTCompID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ApprvrOTCompID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ApprvrOTCompID)
				Else
					Me.ApprvrOTCompID = MyBase.SetStringAsString(ColumnNames.ApprvrOTCompID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ApprvrLvCompID As String
			Get
				If Me.IsColumnNull(ColumnNames.ApprvrLvCompID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ApprvrLvCompID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ApprvrLvCompID)
				Else
					Me.ApprvrLvCompID = MyBase.SetStringAsString(ColumnNames.ApprvrLvCompID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level1 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level1) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level1)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level1)
				Else
					Me.Level1 = MyBase.SetStringAsString(ColumnNames.Level1, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level2 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level2) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level2)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level2)
				Else
					Me.Level2 = MyBase.SetStringAsString(ColumnNames.Level2, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level3 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level3) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level3)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level3)
				Else
					Me.Level3 = MyBase.SetStringAsString(ColumnNames.Level3, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level4 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level4) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level4)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level4)
				Else
					Me.Level4 = MyBase.SetStringAsString(ColumnNames.Level4, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastName As String
			Get
				If Me.IsColumnNull(ColumnNames.LastName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastName)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastName)
				Else
					Me.LastName = MyBase.SetStringAsString(ColumnNames.LastName, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_FirstName As String
			Get
				If Me.IsColumnNull(ColumnNames.FirstName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.FirstName)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.FirstName)
				Else
					Me.FirstName = MyBase.SetStringAsString(ColumnNames.FirstName, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_MiddleName As String
			Get
				If Me.IsColumnNull(ColumnNames.MiddleName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.MiddleName)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.MiddleName)
				Else
					Me.MiddleName = MyBase.SetStringAsString(ColumnNames.MiddleName, Value)
				End If
			End Set
		End Property

		Public Overridable ReadOnly Property s_FullName As String
			Get
				If Me.IsColumnNull(ColumnNames.FullName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.FullName)
				End If
			End Get
		End Property

		Public Overridable Property s_EmploymentType As String
			Get
				If Me.IsColumnNull(ColumnNames.EmploymentType) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmploymentType)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmploymentType)
				Else
					Me.EmploymentType = MyBase.SetStringAsString(ColumnNames.EmploymentType, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Gender As String
			Get
				If Me.IsColumnNull(ColumnNames.Gender) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Gender)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Gender)
				Else
					Me.Gender = MyBase.SetStringAsString(ColumnNames.Gender, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Address1 As String
			Get
				If Me.IsColumnNull(ColumnNames.Address1) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Address1)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Address1)
				Else
					Me.Address1 = MyBase.SetStringAsString(ColumnNames.Address1, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Address2 As String
			Get
				If Me.IsColumnNull(ColumnNames.Address2) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Address2)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Address2)
				Else
					Me.Address2 = MyBase.SetStringAsString(ColumnNames.Address2, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Position As String
			Get
				If Me.IsColumnNull(ColumnNames.Position) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Position)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Position)
				Else
					Me.Position = MyBase.SetStringAsString(ColumnNames.Position, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CostCenter As String
			Get
				If Me.IsColumnNull(ColumnNames.CostCenter) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CostCenter)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CostCenter)
				Else
					Me.CostCenter = MyBase.SetStringAsString(ColumnNames.CostCenter, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_BirthDate As String
			Get
				If Me.IsColumnNull(ColumnNames.BirthDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.BirthDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.BirthDate)
				Else
					Me.BirthDate = MyBase.SetDateTimeAsString(ColumnNames.BirthDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_DateEmployed As String
			Get
				If Me.IsColumnNull(ColumnNames.DateEmployed) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.DateEmployed)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.DateEmployed)
				Else
					Me.DateEmployed = MyBase.SetDateTimeAsString(ColumnNames.DateEmployed, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_DateRegularized As String
			Get
				If Me.IsColumnNull(ColumnNames.DateRegularized) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.DateRegularized)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.DateRegularized)
				Else
					Me.DateRegularized = MyBase.SetDateTimeAsString(ColumnNames.DateRegularized, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_DateTerminated As String
			Get
				If Me.IsColumnNull(ColumnNames.DateTerminated) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.DateTerminated)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.DateTerminated)
				Else
					Me.DateTerminated = MyBase.SetDateTimeAsString(ColumnNames.DateTerminated, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Rank As String
			Get
				If Me.IsColumnNull(ColumnNames.Rank) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Rank)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Rank)
				Else
					Me.Rank = MyBase.SetStringAsString(ColumnNames.Rank, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TardyExempt As String
			Get
				If Me.IsColumnNull(ColumnNames.TardyExempt) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.TardyExempt)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TardyExempt)
				Else
					Me.TardyExempt = MyBase.SetBooleanAsString(ColumnNames.TardyExempt, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_UTExempt As String
			Get
				If Me.IsColumnNull(ColumnNames.UTExempt) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.UTExempt)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.UTExempt)
				Else
					Me.UTExempt = MyBase.SetBooleanAsString(ColumnNames.UTExempt, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OTExempt As String
			Get
				If Me.IsColumnNull(ColumnNames.OTExempt) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.OTExempt)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OTExempt)
				Else
					Me.OTExempt = MyBase.SetBooleanAsString(ColumnNames.OTExempt, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_NonSwiper As String
			Get
				If Me.IsColumnNull(ColumnNames.NonSwiper) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.NonSwiper)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.NonSwiper)
				Else
					Me.NonSwiper = MyBase.SetBooleanAsString(ColumnNames.NonSwiper, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ShiftCode As String
			Get
				If Me.IsColumnNull(ColumnNames.ShiftCode) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ShiftCode)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ShiftCode)
				Else
					Me.ShiftCode = MyBase.SetIntegerAsString(ColumnNames.ShiftCode, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_RestDay As String
			Get
				If Me.IsColumnNull(ColumnNames.RestDay) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.RestDay)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.RestDay)
				Else
					Me.RestDay = MyBase.SetStringAsString(ColumnNames.RestDay, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level1Disp As String
			Get
				If Me.IsColumnNull(ColumnNames.Level1Disp) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level1Disp)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level1Disp)
				Else
					Me.Level1Disp = MyBase.SetStringAsString(ColumnNames.Level1Disp, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level2Disp As String
			Get
				If Me.IsColumnNull(ColumnNames.Level2Disp) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level2Disp)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level2Disp)
				Else
					Me.Level2Disp = MyBase.SetStringAsString(ColumnNames.Level2Disp, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level3Disp As String
			Get
				If Me.IsColumnNull(ColumnNames.Level3Disp) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level3Disp)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level3Disp)
				Else
					Me.Level3Disp = MyBase.SetStringAsString(ColumnNames.Level3Disp, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level4Disp As String
			Get
				If Me.IsColumnNull(ColumnNames.Level4Disp) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level4Disp)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level4Disp)
				Else
					Me.Level4Disp = MyBase.SetStringAsString(ColumnNames.Level4Disp, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Password As String
			Get
				If Me.IsColumnNull(ColumnNames.Password) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Password)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Password)
				Else
					Me.Password = MyBase.SetStringAsString(ColumnNames.Password, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PwdNeverExpire As String
			Get
				If Me.IsColumnNull(ColumnNames.PwdNeverExpire) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.PwdNeverExpire)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PwdNeverExpire)
				Else
					Me.PwdNeverExpire = MyBase.SetBooleanAsString(ColumnNames.PwdNeverExpire, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PwdExpireDate As String
			Get
				If Me.IsColumnNull(ColumnNames.PwdExpireDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PwdExpireDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PwdExpireDate)
				Else
					Me.PwdExpireDate = MyBase.SetDateTimeAsString(ColumnNames.PwdExpireDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_AccountIsLocked As String
			Get
				If Me.IsColumnNull(ColumnNames.AccountIsLocked) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.AccountIsLocked)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.AccountIsLocked)
				Else
					Me.AccountIsLocked = MyBase.SetBooleanAsString(ColumnNames.AccountIsLocked, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_UserLevel As String
			Get
				If Me.IsColumnNull(ColumnNames.UserLevel) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.UserLevel)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.UserLevel)
				Else
					Me.UserLevel = MyBase.SetIntegerAsString(ColumnNames.UserLevel, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Email As String
			Get
				If Me.IsColumnNull(ColumnNames.Email) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Email)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Email)
				Else
					Me.Email = MyBase.SetStringAsString(ColumnNames.Email, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Question As String
			Get
				If Me.IsColumnNull(ColumnNames.Question) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Question)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Question)
				Else
					Me.Question = MyBase.SetStringAsString(ColumnNames.Question, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Answer As String
			Get
				If Me.IsColumnNull(ColumnNames.Answer) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Answer)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Answer)
				Else
					Me.Answer = MyBase.SetStringAsString(ColumnNames.Answer, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property SwipeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.SwipeID, Parameters.SwipeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayrollType() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayrollType, Parameters.PayrollType)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Approver() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Approver, Parameters.Approver)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ApproverLeave() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ApproverLeave, Parameters.ApproverLeave)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ApproverOT() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ApproverOT, Parameters.ApproverOT)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ApprvrOTCompID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ApprvrOTCompID, Parameters.ApprvrOTCompID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ApprvrLvCompID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ApprvrLvCompID, Parameters.ApprvrLvCompID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level1() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level1, Parameters.Level1)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level2() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level2, Parameters.Level2)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level3() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level3, Parameters.Level3)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level4() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level4, Parameters.Level4)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastName, Parameters.LastName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property FirstName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.FirstName, Parameters.FirstName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property MiddleName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.MiddleName, Parameters.MiddleName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property FullName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.FullName, Parameters.FullName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmploymentType() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmploymentType, Parameters.EmploymentType)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Gender() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Gender, Parameters.Gender)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Address1() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Address1, Parameters.Address1)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Address2() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Address2, Parameters.Address2)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Position() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Position, Parameters.Position)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CostCenter() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CostCenter, Parameters.CostCenter)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property BirthDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.BirthDate, Parameters.BirthDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property DateEmployed() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.DateEmployed, Parameters.DateEmployed)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property DateRegularized() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.DateRegularized, Parameters.DateRegularized)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property DateTerminated() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.DateTerminated, Parameters.DateTerminated)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Rank() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Rank, Parameters.Rank)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TardyExempt() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TardyExempt, Parameters.TardyExempt)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property UTExempt() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.UTExempt, Parameters.UTExempt)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OTExempt() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OTExempt, Parameters.OTExempt)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property NonSwiper() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.NonSwiper, Parameters.NonSwiper)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ShiftCode() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ShiftCode, Parameters.ShiftCode)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property RestDay() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.RestDay, Parameters.RestDay)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level1Disp() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level1Disp, Parameters.Level1Disp)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level2Disp() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level2Disp, Parameters.Level2Disp)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level3Disp() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level3Disp, Parameters.Level3Disp)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level4Disp() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level4Disp, Parameters.Level4Disp)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Password() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Password, Parameters.Password)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PwdNeverExpire() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PwdNeverExpire, Parameters.PwdNeverExpire)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PwdExpireDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PwdExpireDate, Parameters.PwdExpireDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property AccountIsLocked() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.AccountIsLocked, Parameters.AccountIsLocked)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property UserLevel() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.UserLevel, Parameters.UserLevel)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Email() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Email, Parameters.Email)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Question() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Question, Parameters.Question)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Answer() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Answer, Parameters.Answer)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property SwipeID() As WhereParameter 
			Get
				If _SwipeID_W Is Nothing Then
					_SwipeID_W = TearOff.SwipeID
				End If
				Return _SwipeID_W
			End Get
		End Property

		Public ReadOnly Property PayrollType() As WhereParameter 
			Get
				If _PayrollType_W Is Nothing Then
					_PayrollType_W = TearOff.PayrollType
				End If
				Return _PayrollType_W
			End Get
		End Property

		Public ReadOnly Property Approver() As WhereParameter 
			Get
				If _Approver_W Is Nothing Then
					_Approver_W = TearOff.Approver
				End If
				Return _Approver_W
			End Get
		End Property

		Public ReadOnly Property ApproverLeave() As WhereParameter 
			Get
				If _ApproverLeave_W Is Nothing Then
					_ApproverLeave_W = TearOff.ApproverLeave
				End If
				Return _ApproverLeave_W
			End Get
		End Property

		Public ReadOnly Property ApproverOT() As WhereParameter 
			Get
				If _ApproverOT_W Is Nothing Then
					_ApproverOT_W = TearOff.ApproverOT
				End If
				Return _ApproverOT_W
			End Get
		End Property

		Public ReadOnly Property ApprvrOTCompID() As WhereParameter 
			Get
				If _ApprvrOTCompID_W Is Nothing Then
					_ApprvrOTCompID_W = TearOff.ApprvrOTCompID
				End If
				Return _ApprvrOTCompID_W
			End Get
		End Property

		Public ReadOnly Property ApprvrLvCompID() As WhereParameter 
			Get
				If _ApprvrLvCompID_W Is Nothing Then
					_ApprvrLvCompID_W = TearOff.ApprvrLvCompID
				End If
				Return _ApprvrLvCompID_W
			End Get
		End Property

		Public ReadOnly Property Level1() As WhereParameter 
			Get
				If _Level1_W Is Nothing Then
					_Level1_W = TearOff.Level1
				End If
				Return _Level1_W
			End Get
		End Property

		Public ReadOnly Property Level2() As WhereParameter 
			Get
				If _Level2_W Is Nothing Then
					_Level2_W = TearOff.Level2
				End If
				Return _Level2_W
			End Get
		End Property

		Public ReadOnly Property Level3() As WhereParameter 
			Get
				If _Level3_W Is Nothing Then
					_Level3_W = TearOff.Level3
				End If
				Return _Level3_W
			End Get
		End Property

		Public ReadOnly Property Level4() As WhereParameter 
			Get
				If _Level4_W Is Nothing Then
					_Level4_W = TearOff.Level4
				End If
				Return _Level4_W
			End Get
		End Property

		Public ReadOnly Property LastName() As WhereParameter 
			Get
				If _LastName_W Is Nothing Then
					_LastName_W = TearOff.LastName
				End If
				Return _LastName_W
			End Get
		End Property

		Public ReadOnly Property FirstName() As WhereParameter 
			Get
				If _FirstName_W Is Nothing Then
					_FirstName_W = TearOff.FirstName
				End If
				Return _FirstName_W
			End Get
		End Property

		Public ReadOnly Property MiddleName() As WhereParameter 
			Get
				If _MiddleName_W Is Nothing Then
					_MiddleName_W = TearOff.MiddleName
				End If
				Return _MiddleName_W
			End Get
		End Property

		Public ReadOnly Property FullName() As WhereParameter 
			Get
				If _FullName_W Is Nothing Then
					_FullName_W = TearOff.FullName
				End If
				Return _FullName_W
			End Get
		End Property

		Public ReadOnly Property EmploymentType() As WhereParameter 
			Get
				If _EmploymentType_W Is Nothing Then
					_EmploymentType_W = TearOff.EmploymentType
				End If
				Return _EmploymentType_W
			End Get
		End Property

		Public ReadOnly Property Gender() As WhereParameter 
			Get
				If _Gender_W Is Nothing Then
					_Gender_W = TearOff.Gender
				End If
				Return _Gender_W
			End Get
		End Property

		Public ReadOnly Property Address1() As WhereParameter 
			Get
				If _Address1_W Is Nothing Then
					_Address1_W = TearOff.Address1
				End If
				Return _Address1_W
			End Get
		End Property

		Public ReadOnly Property Address2() As WhereParameter 
			Get
				If _Address2_W Is Nothing Then
					_Address2_W = TearOff.Address2
				End If
				Return _Address2_W
			End Get
		End Property

		Public ReadOnly Property Position() As WhereParameter 
			Get
				If _Position_W Is Nothing Then
					_Position_W = TearOff.Position
				End If
				Return _Position_W
			End Get
		End Property

		Public ReadOnly Property CostCenter() As WhereParameter 
			Get
				If _CostCenter_W Is Nothing Then
					_CostCenter_W = TearOff.CostCenter
				End If
				Return _CostCenter_W
			End Get
		End Property

		Public ReadOnly Property BirthDate() As WhereParameter 
			Get
				If _BirthDate_W Is Nothing Then
					_BirthDate_W = TearOff.BirthDate
				End If
				Return _BirthDate_W
			End Get
		End Property

		Public ReadOnly Property DateEmployed() As WhereParameter 
			Get
				If _DateEmployed_W Is Nothing Then
					_DateEmployed_W = TearOff.DateEmployed
				End If
				Return _DateEmployed_W
			End Get
		End Property

		Public ReadOnly Property DateRegularized() As WhereParameter 
			Get
				If _DateRegularized_W Is Nothing Then
					_DateRegularized_W = TearOff.DateRegularized
				End If
				Return _DateRegularized_W
			End Get
		End Property

		Public ReadOnly Property DateTerminated() As WhereParameter 
			Get
				If _DateTerminated_W Is Nothing Then
					_DateTerminated_W = TearOff.DateTerminated
				End If
				Return _DateTerminated_W
			End Get
		End Property

		Public ReadOnly Property Rank() As WhereParameter 
			Get
				If _Rank_W Is Nothing Then
					_Rank_W = TearOff.Rank
				End If
				Return _Rank_W
			End Get
		End Property

		Public ReadOnly Property TardyExempt() As WhereParameter 
			Get
				If _TardyExempt_W Is Nothing Then
					_TardyExempt_W = TearOff.TardyExempt
				End If
				Return _TardyExempt_W
			End Get
		End Property

		Public ReadOnly Property UTExempt() As WhereParameter 
			Get
				If _UTExempt_W Is Nothing Then
					_UTExempt_W = TearOff.UTExempt
				End If
				Return _UTExempt_W
			End Get
		End Property

		Public ReadOnly Property OTExempt() As WhereParameter 
			Get
				If _OTExempt_W Is Nothing Then
					_OTExempt_W = TearOff.OTExempt
				End If
				Return _OTExempt_W
			End Get
		End Property

		Public ReadOnly Property NonSwiper() As WhereParameter 
			Get
				If _NonSwiper_W Is Nothing Then
					_NonSwiper_W = TearOff.NonSwiper
				End If
				Return _NonSwiper_W
			End Get
		End Property

		Public ReadOnly Property ShiftCode() As WhereParameter 
			Get
				If _ShiftCode_W Is Nothing Then
					_ShiftCode_W = TearOff.ShiftCode
				End If
				Return _ShiftCode_W
			End Get
		End Property

		Public ReadOnly Property RestDay() As WhereParameter 
			Get
				If _RestDay_W Is Nothing Then
					_RestDay_W = TearOff.RestDay
				End If
				Return _RestDay_W
			End Get
		End Property

		Public ReadOnly Property Level1Disp() As WhereParameter 
			Get
				If _Level1Disp_W Is Nothing Then
					_Level1Disp_W = TearOff.Level1Disp
				End If
				Return _Level1Disp_W
			End Get
		End Property

		Public ReadOnly Property Level2Disp() As WhereParameter 
			Get
				If _Level2Disp_W Is Nothing Then
					_Level2Disp_W = TearOff.Level2Disp
				End If
				Return _Level2Disp_W
			End Get
		End Property

		Public ReadOnly Property Level3Disp() As WhereParameter 
			Get
				If _Level3Disp_W Is Nothing Then
					_Level3Disp_W = TearOff.Level3Disp
				End If
				Return _Level3Disp_W
			End Get
		End Property

		Public ReadOnly Property Level4Disp() As WhereParameter 
			Get
				If _Level4Disp_W Is Nothing Then
					_Level4Disp_W = TearOff.Level4Disp
				End If
				Return _Level4Disp_W
			End Get
		End Property

		Public ReadOnly Property Password() As WhereParameter 
			Get
				If _Password_W Is Nothing Then
					_Password_W = TearOff.Password
				End If
				Return _Password_W
			End Get
		End Property

		Public ReadOnly Property PwdNeverExpire() As WhereParameter 
			Get
				If _PwdNeverExpire_W Is Nothing Then
					_PwdNeverExpire_W = TearOff.PwdNeverExpire
				End If
				Return _PwdNeverExpire_W
			End Get
		End Property

		Public ReadOnly Property PwdExpireDate() As WhereParameter 
			Get
				If _PwdExpireDate_W Is Nothing Then
					_PwdExpireDate_W = TearOff.PwdExpireDate
				End If
				Return _PwdExpireDate_W
			End Get
		End Property

		Public ReadOnly Property AccountIsLocked() As WhereParameter 
			Get
				If _AccountIsLocked_W Is Nothing Then
					_AccountIsLocked_W = TearOff.AccountIsLocked
				End If
				Return _AccountIsLocked_W
			End Get
		End Property

		Public ReadOnly Property UserLevel() As WhereParameter 
			Get
				If _UserLevel_W Is Nothing Then
					_UserLevel_W = TearOff.UserLevel
				End If
				Return _UserLevel_W
			End Get
		End Property

		Public ReadOnly Property Email() As WhereParameter 
			Get
				If _Email_W Is Nothing Then
					_Email_W = TearOff.Email
				End If
				Return _Email_W
			End Get
		End Property

		Public ReadOnly Property Question() As WhereParameter 
			Get
				If _Question_W Is Nothing Then
					_Question_W = TearOff.Question
				End If
				Return _Question_W
			End Get
		End Property

		Public ReadOnly Property Answer() As WhereParameter 
			Get
				If _Answer_W Is Nothing Then
					_Answer_W = TearOff.Answer
				End If
				Return _Answer_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _SwipeID_W As WhereParameter = Nothing
		Private _PayrollType_W As WhereParameter = Nothing
		Private _Approver_W As WhereParameter = Nothing
		Private _ApproverLeave_W As WhereParameter = Nothing
		Private _ApproverOT_W As WhereParameter = Nothing
		Private _ApprvrOTCompID_W As WhereParameter = Nothing
		Private _ApprvrLvCompID_W As WhereParameter = Nothing
		Private _Level1_W As WhereParameter = Nothing
		Private _Level2_W As WhereParameter = Nothing
		Private _Level3_W As WhereParameter = Nothing
		Private _Level4_W As WhereParameter = Nothing
		Private _LastName_W As WhereParameter = Nothing
		Private _FirstName_W As WhereParameter = Nothing
		Private _MiddleName_W As WhereParameter = Nothing
		Private _FullName_W As WhereParameter = Nothing
		Private _EmploymentType_W As WhereParameter = Nothing
		Private _Gender_W As WhereParameter = Nothing
		Private _Address1_W As WhereParameter = Nothing
		Private _Address2_W As WhereParameter = Nothing
		Private _Position_W As WhereParameter = Nothing
		Private _CostCenter_W As WhereParameter = Nothing
		Private _BirthDate_W As WhereParameter = Nothing
		Private _DateEmployed_W As WhereParameter = Nothing
		Private _DateRegularized_W As WhereParameter = Nothing
		Private _DateTerminated_W As WhereParameter = Nothing
		Private _Rank_W As WhereParameter = Nothing
		Private _TardyExempt_W As WhereParameter = Nothing
		Private _UTExempt_W As WhereParameter = Nothing
		Private _OTExempt_W As WhereParameter = Nothing
		Private _NonSwiper_W As WhereParameter = Nothing
		Private _ShiftCode_W As WhereParameter = Nothing
		Private _RestDay_W As WhereParameter = Nothing
		Private _Level1Disp_W As WhereParameter = Nothing
		Private _Level2Disp_W As WhereParameter = Nothing
		Private _Level3Disp_W As WhereParameter = Nothing
		Private _Level4Disp_W As WhereParameter = Nothing
		Private _Password_W As WhereParameter = Nothing
		Private _PwdNeverExpire_W As WhereParameter = Nothing
		Private _PwdExpireDate_W As WhereParameter = Nothing
		Private _AccountIsLocked_W As WhereParameter = Nothing
		Private _UserLevel_W As WhereParameter = Nothing
		Private _Email_W As WhereParameter = Nothing
		Private _Question_W As WhereParameter = Nothing
		Private _Answer_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_SwipeID_W = Nothing
			_PayrollType_W = Nothing
			_Approver_W = Nothing
			_ApproverLeave_W = Nothing
			_ApproverOT_W = Nothing
			_ApprvrOTCompID_W = Nothing
			_ApprvrLvCompID_W = Nothing
			_Level1_W = Nothing
			_Level2_W = Nothing
			_Level3_W = Nothing
			_Level4_W = Nothing
			_LastName_W = Nothing
			_FirstName_W = Nothing
			_MiddleName_W = Nothing
			_FullName_W = Nothing
			_EmploymentType_W = Nothing
			_Gender_W = Nothing
			_Address1_W = Nothing
			_Address2_W = Nothing
			_Position_W = Nothing
			_CostCenter_W = Nothing
			_BirthDate_W = Nothing
			_DateEmployed_W = Nothing
			_DateRegularized_W = Nothing
			_DateTerminated_W = Nothing
			_Rank_W = Nothing
			_TardyExempt_W = Nothing
			_UTExempt_W = Nothing
			_OTExempt_W = Nothing
			_NonSwiper_W = Nothing
			_ShiftCode_W = Nothing
			_RestDay_W = Nothing
			_Level1Disp_W = Nothing
			_Level2Disp_W = Nothing
			_Level3Disp_W = Nothing
			_Level4Disp_W = Nothing
			_Password_W = Nothing
			_PwdNeverExpire_W = Nothing
			_PwdExpireDate_W = Nothing
			_AccountIsLocked_W = Nothing
			_UserLevel_W = Nothing
			_Email_W = Nothing
			_Question_W = Nothing
			_Answer_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpdDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property SwipeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.SwipeID, Parameters.SwipeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayrollType() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayrollType, Parameters.PayrollType)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Approver() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Approver, Parameters.Approver)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ApproverLeave() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApproverLeave, Parameters.ApproverLeave)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ApproverOT() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApproverOT, Parameters.ApproverOT)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ApprvrOTCompID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApprvrOTCompID, Parameters.ApprvrOTCompID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ApprvrLvCompID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApprvrLvCompID, Parameters.ApprvrLvCompID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level1() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level1, Parameters.Level1)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level2() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level2, Parameters.Level2)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level3() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level3, Parameters.Level3)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level4() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level4, Parameters.Level4)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastName, Parameters.LastName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property FirstName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.FirstName, Parameters.FirstName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property MiddleName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.MiddleName, Parameters.MiddleName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property FullName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.FullName, Parameters.FullName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmploymentType() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmploymentType, Parameters.EmploymentType)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Gender() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Gender, Parameters.Gender)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Address1() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Address1, Parameters.Address1)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Address2() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Address2, Parameters.Address2)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Position() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Position, Parameters.Position)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CostCenter() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CostCenter, Parameters.CostCenter)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property BirthDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.BirthDate, Parameters.BirthDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property DateEmployed() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DateEmployed, Parameters.DateEmployed)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property DateRegularized() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DateRegularized, Parameters.DateRegularized)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property DateTerminated() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DateTerminated, Parameters.DateTerminated)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Rank() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Rank, Parameters.Rank)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TardyExempt() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TardyExempt, Parameters.TardyExempt)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property UTExempt() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UTExempt, Parameters.UTExempt)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OTExempt() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTExempt, Parameters.OTExempt)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property NonSwiper() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.NonSwiper, Parameters.NonSwiper)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ShiftCode() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ShiftCode, Parameters.ShiftCode)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property RestDay() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RestDay, Parameters.RestDay)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level1Disp() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level1Disp, Parameters.Level1Disp)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level2Disp() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level2Disp, Parameters.Level2Disp)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level3Disp() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level3Disp, Parameters.Level3Disp)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level4Disp() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level4Disp, Parameters.Level4Disp)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Password() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Password, Parameters.Password)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PwdNeverExpire() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PwdNeverExpire, Parameters.PwdNeverExpire)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PwdExpireDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PwdExpireDate, Parameters.PwdExpireDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property AccountIsLocked() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AccountIsLocked, Parameters.AccountIsLocked)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property UserLevel() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UserLevel, Parameters.UserLevel)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Email() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Email, Parameters.Email)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Question() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Question, Parameters.Question)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Answer() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Answer, Parameters.Answer)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property SwipeID() As AggregateParameter 
			Get
				If _SwipeID_W Is Nothing Then
					_SwipeID_W = TearOff.SwipeID
				End If
				Return _SwipeID_W
			End Get
		End Property

		Public ReadOnly Property PayrollType() As AggregateParameter 
			Get
				If _PayrollType_W Is Nothing Then
					_PayrollType_W = TearOff.PayrollType
				End If
				Return _PayrollType_W
			End Get
		End Property

		Public ReadOnly Property Approver() As AggregateParameter 
			Get
				If _Approver_W Is Nothing Then
					_Approver_W = TearOff.Approver
				End If
				Return _Approver_W
			End Get
		End Property

		Public ReadOnly Property ApproverLeave() As AggregateParameter 
			Get
				If _ApproverLeave_W Is Nothing Then
					_ApproverLeave_W = TearOff.ApproverLeave
				End If
				Return _ApproverLeave_W
			End Get
		End Property

		Public ReadOnly Property ApproverOT() As AggregateParameter 
			Get
				If _ApproverOT_W Is Nothing Then
					_ApproverOT_W = TearOff.ApproverOT
				End If
				Return _ApproverOT_W
			End Get
		End Property

		Public ReadOnly Property ApprvrOTCompID() As AggregateParameter 
			Get
				If _ApprvrOTCompID_W Is Nothing Then
					_ApprvrOTCompID_W = TearOff.ApprvrOTCompID
				End If
				Return _ApprvrOTCompID_W
			End Get
		End Property

		Public ReadOnly Property ApprvrLvCompID() As AggregateParameter 
			Get
				If _ApprvrLvCompID_W Is Nothing Then
					_ApprvrLvCompID_W = TearOff.ApprvrLvCompID
				End If
				Return _ApprvrLvCompID_W
			End Get
		End Property

		Public ReadOnly Property Level1() As AggregateParameter 
			Get
				If _Level1_W Is Nothing Then
					_Level1_W = TearOff.Level1
				End If
				Return _Level1_W
			End Get
		End Property

		Public ReadOnly Property Level2() As AggregateParameter 
			Get
				If _Level2_W Is Nothing Then
					_Level2_W = TearOff.Level2
				End If
				Return _Level2_W
			End Get
		End Property

		Public ReadOnly Property Level3() As AggregateParameter 
			Get
				If _Level3_W Is Nothing Then
					_Level3_W = TearOff.Level3
				End If
				Return _Level3_W
			End Get
		End Property

		Public ReadOnly Property Level4() As AggregateParameter 
			Get
				If _Level4_W Is Nothing Then
					_Level4_W = TearOff.Level4
				End If
				Return _Level4_W
			End Get
		End Property

		Public ReadOnly Property LastName() As AggregateParameter 
			Get
				If _LastName_W Is Nothing Then
					_LastName_W = TearOff.LastName
				End If
				Return _LastName_W
			End Get
		End Property

		Public ReadOnly Property FirstName() As AggregateParameter 
			Get
				If _FirstName_W Is Nothing Then
					_FirstName_W = TearOff.FirstName
				End If
				Return _FirstName_W
			End Get
		End Property

		Public ReadOnly Property MiddleName() As AggregateParameter 
			Get
				If _MiddleName_W Is Nothing Then
					_MiddleName_W = TearOff.MiddleName
				End If
				Return _MiddleName_W
			End Get
		End Property

		Public ReadOnly Property FullName() As AggregateParameter 
			Get
				If _FullName_W Is Nothing Then
					_FullName_W = TearOff.FullName
				End If
				Return _FullName_W
			End Get
		End Property

		Public ReadOnly Property EmploymentType() As AggregateParameter 
			Get
				If _EmploymentType_W Is Nothing Then
					_EmploymentType_W = TearOff.EmploymentType
				End If
				Return _EmploymentType_W
			End Get
		End Property

		Public ReadOnly Property Gender() As AggregateParameter 
			Get
				If _Gender_W Is Nothing Then
					_Gender_W = TearOff.Gender
				End If
				Return _Gender_W
			End Get
		End Property

		Public ReadOnly Property Address1() As AggregateParameter 
			Get
				If _Address1_W Is Nothing Then
					_Address1_W = TearOff.Address1
				End If
				Return _Address1_W
			End Get
		End Property

		Public ReadOnly Property Address2() As AggregateParameter 
			Get
				If _Address2_W Is Nothing Then
					_Address2_W = TearOff.Address2
				End If
				Return _Address2_W
			End Get
		End Property

		Public ReadOnly Property Position() As AggregateParameter 
			Get
				If _Position_W Is Nothing Then
					_Position_W = TearOff.Position
				End If
				Return _Position_W
			End Get
		End Property

		Public ReadOnly Property CostCenter() As AggregateParameter 
			Get
				If _CostCenter_W Is Nothing Then
					_CostCenter_W = TearOff.CostCenter
				End If
				Return _CostCenter_W
			End Get
		End Property

		Public ReadOnly Property BirthDate() As AggregateParameter 
			Get
				If _BirthDate_W Is Nothing Then
					_BirthDate_W = TearOff.BirthDate
				End If
				Return _BirthDate_W
			End Get
		End Property

		Public ReadOnly Property DateEmployed() As AggregateParameter 
			Get
				If _DateEmployed_W Is Nothing Then
					_DateEmployed_W = TearOff.DateEmployed
				End If
				Return _DateEmployed_W
			End Get
		End Property

		Public ReadOnly Property DateRegularized() As AggregateParameter 
			Get
				If _DateRegularized_W Is Nothing Then
					_DateRegularized_W = TearOff.DateRegularized
				End If
				Return _DateRegularized_W
			End Get
		End Property

		Public ReadOnly Property DateTerminated() As AggregateParameter 
			Get
				If _DateTerminated_W Is Nothing Then
					_DateTerminated_W = TearOff.DateTerminated
				End If
				Return _DateTerminated_W
			End Get
		End Property

		Public ReadOnly Property Rank() As AggregateParameter 
			Get
				If _Rank_W Is Nothing Then
					_Rank_W = TearOff.Rank
				End If
				Return _Rank_W
			End Get
		End Property

		Public ReadOnly Property TardyExempt() As AggregateParameter 
			Get
				If _TardyExempt_W Is Nothing Then
					_TardyExempt_W = TearOff.TardyExempt
				End If
				Return _TardyExempt_W
			End Get
		End Property

		Public ReadOnly Property UTExempt() As AggregateParameter 
			Get
				If _UTExempt_W Is Nothing Then
					_UTExempt_W = TearOff.UTExempt
				End If
				Return _UTExempt_W
			End Get
		End Property

		Public ReadOnly Property OTExempt() As AggregateParameter 
			Get
				If _OTExempt_W Is Nothing Then
					_OTExempt_W = TearOff.OTExempt
				End If
				Return _OTExempt_W
			End Get
		End Property

		Public ReadOnly Property NonSwiper() As AggregateParameter 
			Get
				If _NonSwiper_W Is Nothing Then
					_NonSwiper_W = TearOff.NonSwiper
				End If
				Return _NonSwiper_W
			End Get
		End Property

		Public ReadOnly Property ShiftCode() As AggregateParameter 
			Get
				If _ShiftCode_W Is Nothing Then
					_ShiftCode_W = TearOff.ShiftCode
				End If
				Return _ShiftCode_W
			End Get
		End Property

		Public ReadOnly Property RestDay() As AggregateParameter 
			Get
				If _RestDay_W Is Nothing Then
					_RestDay_W = TearOff.RestDay
				End If
				Return _RestDay_W
			End Get
		End Property

		Public ReadOnly Property Level1Disp() As AggregateParameter 
			Get
				If _Level1Disp_W Is Nothing Then
					_Level1Disp_W = TearOff.Level1Disp
				End If
				Return _Level1Disp_W
			End Get
		End Property

		Public ReadOnly Property Level2Disp() As AggregateParameter 
			Get
				If _Level2Disp_W Is Nothing Then
					_Level2Disp_W = TearOff.Level2Disp
				End If
				Return _Level2Disp_W
			End Get
		End Property

		Public ReadOnly Property Level3Disp() As AggregateParameter 
			Get
				If _Level3Disp_W Is Nothing Then
					_Level3Disp_W = TearOff.Level3Disp
				End If
				Return _Level3Disp_W
			End Get
		End Property

		Public ReadOnly Property Level4Disp() As AggregateParameter 
			Get
				If _Level4Disp_W Is Nothing Then
					_Level4Disp_W = TearOff.Level4Disp
				End If
				Return _Level4Disp_W
			End Get
		End Property

		Public ReadOnly Property Password() As AggregateParameter 
			Get
				If _Password_W Is Nothing Then
					_Password_W = TearOff.Password
				End If
				Return _Password_W
			End Get
		End Property

		Public ReadOnly Property PwdNeverExpire() As AggregateParameter 
			Get
				If _PwdNeverExpire_W Is Nothing Then
					_PwdNeverExpire_W = TearOff.PwdNeverExpire
				End If
				Return _PwdNeverExpire_W
			End Get
		End Property

		Public ReadOnly Property PwdExpireDate() As AggregateParameter 
			Get
				If _PwdExpireDate_W Is Nothing Then
					_PwdExpireDate_W = TearOff.PwdExpireDate
				End If
				Return _PwdExpireDate_W
			End Get
		End Property

		Public ReadOnly Property AccountIsLocked() As AggregateParameter 
			Get
				If _AccountIsLocked_W Is Nothing Then
					_AccountIsLocked_W = TearOff.AccountIsLocked
				End If
				Return _AccountIsLocked_W
			End Get
		End Property

		Public ReadOnly Property UserLevel() As AggregateParameter 
			Get
				If _UserLevel_W Is Nothing Then
					_UserLevel_W = TearOff.UserLevel
				End If
				Return _UserLevel_W
			End Get
		End Property

		Public ReadOnly Property Email() As AggregateParameter 
			Get
				If _Email_W Is Nothing Then
					_Email_W = TearOff.Email
				End If
				Return _Email_W
			End Get
		End Property

		Public ReadOnly Property Question() As AggregateParameter 
			Get
				If _Question_W Is Nothing Then
					_Question_W = TearOff.Question
				End If
				Return _Question_W
			End Get
		End Property

		Public ReadOnly Property Answer() As AggregateParameter 
			Get
				If _Answer_W Is Nothing Then
					_Answer_W = TearOff.Answer
				End If
				Return _Answer_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _SwipeID_W As AggregateParameter = Nothing
		Private _PayrollType_W As AggregateParameter = Nothing
		Private _Approver_W As AggregateParameter = Nothing
		Private _ApproverLeave_W As AggregateParameter = Nothing
		Private _ApproverOT_W As AggregateParameter = Nothing
		Private _ApprvrOTCompID_W As AggregateParameter = Nothing
		Private _ApprvrLvCompID_W As AggregateParameter = Nothing
		Private _Level1_W As AggregateParameter = Nothing
		Private _Level2_W As AggregateParameter = Nothing
		Private _Level3_W As AggregateParameter = Nothing
		Private _Level4_W As AggregateParameter = Nothing
		Private _LastName_W As AggregateParameter = Nothing
		Private _FirstName_W As AggregateParameter = Nothing
		Private _MiddleName_W As AggregateParameter = Nothing
		Private _FullName_W As AggregateParameter = Nothing
		Private _EmploymentType_W As AggregateParameter = Nothing
		Private _Gender_W As AggregateParameter = Nothing
		Private _Address1_W As AggregateParameter = Nothing
		Private _Address2_W As AggregateParameter = Nothing
		Private _Position_W As AggregateParameter = Nothing
		Private _CostCenter_W As AggregateParameter = Nothing
		Private _BirthDate_W As AggregateParameter = Nothing
		Private _DateEmployed_W As AggregateParameter = Nothing
		Private _DateRegularized_W As AggregateParameter = Nothing
		Private _DateTerminated_W As AggregateParameter = Nothing
		Private _Rank_W As AggregateParameter = Nothing
		Private _TardyExempt_W As AggregateParameter = Nothing
		Private _UTExempt_W As AggregateParameter = Nothing
		Private _OTExempt_W As AggregateParameter = Nothing
		Private _NonSwiper_W As AggregateParameter = Nothing
		Private _ShiftCode_W As AggregateParameter = Nothing
		Private _RestDay_W As AggregateParameter = Nothing
		Private _Level1Disp_W As AggregateParameter = Nothing
		Private _Level2Disp_W As AggregateParameter = Nothing
		Private _Level3Disp_W As AggregateParameter = Nothing
		Private _Level4Disp_W As AggregateParameter = Nothing
		Private _Password_W As AggregateParameter = Nothing
		Private _PwdNeverExpire_W As AggregateParameter = Nothing
		Private _PwdExpireDate_W As AggregateParameter = Nothing
		Private _AccountIsLocked_W As AggregateParameter = Nothing
		Private _UserLevel_W As AggregateParameter = Nothing
		Private _Email_W As AggregateParameter = Nothing
		Private _Question_W As AggregateParameter = Nothing
		Private _Answer_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_SwipeID_W = Nothing
		_PayrollType_W = Nothing
		_Approver_W = Nothing
		_ApproverLeave_W = Nothing
		_ApproverOT_W = Nothing
		_ApprvrOTCompID_W = Nothing
		_ApprvrLvCompID_W = Nothing
		_Level1_W = Nothing
		_Level2_W = Nothing
		_Level3_W = Nothing
		_Level4_W = Nothing
		_LastName_W = Nothing
		_FirstName_W = Nothing
		_MiddleName_W = Nothing
		_FullName_W = Nothing
		_EmploymentType_W = Nothing
		_Gender_W = Nothing
		_Address1_W = Nothing
		_Address2_W = Nothing
		_Position_W = Nothing
		_CostCenter_W = Nothing
		_BirthDate_W = Nothing
		_DateEmployed_W = Nothing
		_DateRegularized_W = Nothing
		_DateTerminated_W = Nothing
		_Rank_W = Nothing
		_TardyExempt_W = Nothing
		_UTExempt_W = Nothing
		_OTExempt_W = Nothing
		_NonSwiper_W = Nothing
		_ShiftCode_W = Nothing
		_RestDay_W = Nothing
		_Level1Disp_W = Nothing
		_Level2Disp_W = Nothing
		_Level3Disp_W = Nothing
		_Level4Disp_W = Nothing
		_Password_W = Nothing
		_PwdNeverExpire_W = Nothing
		_PwdExpireDate_W = Nothing
		_AccountIsLocked_W = Nothing
		_UserLevel_W = Nothing
		_Email_W = Nothing
		_Question_W = Nothing
		_Answer_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpdDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_EmployeeInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.FullName.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_EmployeeUpdate]" 
		
		CreateParameters(cmd) 
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.FullName.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_EmployeeDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.SwipeID)
		p.SourceColumn = ColumnNames.SwipeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayrollType)
		p.SourceColumn = ColumnNames.PayrollType
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Approver)
		p.SourceColumn = ColumnNames.Approver
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ApproverLeave)
		p.SourceColumn = ColumnNames.ApproverLeave
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ApproverOT)
		p.SourceColumn = ColumnNames.ApproverOT
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ApprvrOTCompID)
		p.SourceColumn = ColumnNames.ApprvrOTCompID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ApprvrLvCompID)
		p.SourceColumn = ColumnNames.ApprvrLvCompID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level1)
		p.SourceColumn = ColumnNames.Level1
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level2)
		p.SourceColumn = ColumnNames.Level2
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level3)
		p.SourceColumn = ColumnNames.Level3
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level4)
		p.SourceColumn = ColumnNames.Level4
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastName)
		p.SourceColumn = ColumnNames.LastName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.FirstName)
		p.SourceColumn = ColumnNames.FirstName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.MiddleName)
		p.SourceColumn = ColumnNames.MiddleName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.FullName)
		p.SourceColumn = ColumnNames.FullName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmploymentType)
		p.SourceColumn = ColumnNames.EmploymentType
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Gender)
		p.SourceColumn = ColumnNames.Gender
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Address1)
		p.SourceColumn = ColumnNames.Address1
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Address2)
		p.SourceColumn = ColumnNames.Address2
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Position)
		p.SourceColumn = ColumnNames.Position
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CostCenter)
		p.SourceColumn = ColumnNames.CostCenter
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.BirthDate)
		p.SourceColumn = ColumnNames.BirthDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.DateEmployed)
		p.SourceColumn = ColumnNames.DateEmployed
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.DateRegularized)
		p.SourceColumn = ColumnNames.DateRegularized
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.DateTerminated)
		p.SourceColumn = ColumnNames.DateTerminated
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Rank)
		p.SourceColumn = ColumnNames.Rank
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TardyExempt)
		p.SourceColumn = ColumnNames.TardyExempt
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.UTExempt)
		p.SourceColumn = ColumnNames.UTExempt
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OTExempt)
		p.SourceColumn = ColumnNames.OTExempt
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.NonSwiper)
		p.SourceColumn = ColumnNames.NonSwiper
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ShiftCode)
		p.SourceColumn = ColumnNames.ShiftCode
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.RestDay)
		p.SourceColumn = ColumnNames.RestDay
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level1Disp)
		p.SourceColumn = ColumnNames.Level1Disp
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level2Disp)
		p.SourceColumn = ColumnNames.Level2Disp
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level3Disp)
		p.SourceColumn = ColumnNames.Level3Disp
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level4Disp)
		p.SourceColumn = ColumnNames.Level4Disp
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Password)
		p.SourceColumn = ColumnNames.Password
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PwdNeverExpire)
		p.SourceColumn = ColumnNames.PwdNeverExpire
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PwdExpireDate)
		p.SourceColumn = ColumnNames.PwdExpireDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.AccountIsLocked)
		p.SourceColumn = ColumnNames.AccountIsLocked
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.UserLevel)
		p.SourceColumn = ColumnNames.UserLevel
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Email)
		p.SourceColumn = ColumnNames.Email
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Question)
		p.SourceColumn = ColumnNames.Question
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Answer)
		p.SourceColumn = ColumnNames.Answer
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

