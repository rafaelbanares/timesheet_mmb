
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _WorkForSubProj
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "WorkForSubProj"
			Me.MappingName = "WorkForSubProj"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkForSubProjLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ProjID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_WorkForSubProj.Parameters.ProjID, ProjID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkForSubProjLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ProjID As SqlParameter
			Get
				Return New SqlParameter("@ProjID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property ShortDesc As SqlParameter
			Get
				Return New SqlParameter("@ShortDesc", SqlDbType.VarChar, 25)
			End Get
		End Property
		
		Public Shared ReadOnly Property Description As SqlParameter
			Get
				Return New SqlParameter("@Description", SqlDbType.VarChar, 150)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkForSubID As SqlParameter
			Get
				Return New SqlParameter("@WorkForSubID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ProjID As String = "projID"
        Public Const CompanyID As String = "CompanyID"
        Public Const ShortDesc As String = "ShortDesc"
        Public Const Description As String = "Description"
        Public Const WorkForSubID As String = "WorkForSubID"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ProjID) = _WorkForSubProj.PropertyNames.ProjID
				ht(CompanyID) = _WorkForSubProj.PropertyNames.CompanyID
				ht(ShortDesc) = _WorkForSubProj.PropertyNames.ShortDesc
				ht(Description) = _WorkForSubProj.PropertyNames.Description
				ht(WorkForSubID) = _WorkForSubProj.PropertyNames.WorkForSubID
				ht(CreatedBy) = _WorkForSubProj.PropertyNames.CreatedBy
				ht(CreatedDate) = _WorkForSubProj.PropertyNames.CreatedDate
				ht(LastUpdBy) = _WorkForSubProj.PropertyNames.LastUpdBy
				ht(LastUpdDate) = _WorkForSubProj.PropertyNames.LastUpdDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ProjID As String = "ProjID"
        Public Const CompanyID As String = "CompanyID"
        Public Const ShortDesc As String = "ShortDesc"
        Public Const Description As String = "Description"
        Public Const WorkForSubID As String = "WorkForSubID"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ProjID) = _WorkForSubProj.ColumnNames.ProjID
				ht(CompanyID) = _WorkForSubProj.ColumnNames.CompanyID
				ht(ShortDesc) = _WorkForSubProj.ColumnNames.ShortDesc
				ht(Description) = _WorkForSubProj.ColumnNames.Description
				ht(WorkForSubID) = _WorkForSubProj.ColumnNames.WorkForSubID
				ht(CreatedBy) = _WorkForSubProj.ColumnNames.CreatedBy
				ht(CreatedDate) = _WorkForSubProj.ColumnNames.CreatedDate
				ht(LastUpdBy) = _WorkForSubProj.ColumnNames.LastUpdBy
				ht(LastUpdDate) = _WorkForSubProj.ColumnNames.LastUpdDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ProjID As String = "s_ProjID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const ShortDesc As String = "s_ShortDesc"
        Public Const Description As String = "s_Description"
        Public Const WorkForSubID As String = "s_WorkForSubID"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ProjID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ProjID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ProjID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property ShortDesc As String
			Get
				Return MyBase.GetString(ColumnNames.ShortDesc)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ShortDesc, Value)
			End Set
		End Property

		Public Overridable Property Description As String
			Get
				Return MyBase.GetString(ColumnNames.Description)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Description, Value)
			End Set
		End Property

		Public Overridable Property WorkForSubID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.WorkForSubID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.WorkForSubID, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ProjID As String
			Get
				If Me.IsColumnNull(ColumnNames.ProjID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ProjID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ProjID)
				Else
					Me.ProjID = MyBase.SetIntegerAsString(ColumnNames.ProjID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ShortDesc As String
			Get
				If Me.IsColumnNull(ColumnNames.ShortDesc) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ShortDesc)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ShortDesc)
				Else
					Me.ShortDesc = MyBase.SetStringAsString(ColumnNames.ShortDesc, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Description As String
			Get
				If Me.IsColumnNull(ColumnNames.Description) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Description)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Description)
				Else
					Me.Description = MyBase.SetStringAsString(ColumnNames.Description, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkForSubID As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkForSubID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.WorkForSubID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkForSubID)
				Else
					Me.WorkForSubID = MyBase.SetIntegerAsString(ColumnNames.WorkForSubID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ProjID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ProjID, Parameters.ProjID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ShortDesc() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ShortDesc, Parameters.ShortDesc)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Description() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Description, Parameters.Description)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkForSubID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkForSubID, Parameters.WorkForSubID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ProjID() As WhereParameter 
			Get
				If _ProjID_W Is Nothing Then
					_ProjID_W = TearOff.ProjID
				End If
				Return _ProjID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property ShortDesc() As WhereParameter 
			Get
				If _ShortDesc_W Is Nothing Then
					_ShortDesc_W = TearOff.ShortDesc
				End If
				Return _ShortDesc_W
			End Get
		End Property

		Public ReadOnly Property Description() As WhereParameter 
			Get
				If _Description_W Is Nothing Then
					_Description_W = TearOff.Description
				End If
				Return _Description_W
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As WhereParameter 
			Get
				If _WorkForSubID_W Is Nothing Then
					_WorkForSubID_W = TearOff.WorkForSubID
				End If
				Return _WorkForSubID_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _ProjID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _ShortDesc_W As WhereParameter = Nothing
		Private _Description_W As WhereParameter = Nothing
		Private _WorkForSubID_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ProjID_W = Nothing
			_CompanyID_W = Nothing
			_ShortDesc_W = Nothing
			_Description_W = Nothing
			_WorkForSubID_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpdDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ProjID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ProjID, Parameters.ProjID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ShortDesc() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ShortDesc, Parameters.ShortDesc)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Description() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Description, Parameters.Description)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkForSubID, Parameters.WorkForSubID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ProjID() As AggregateParameter 
			Get
				If _ProjID_W Is Nothing Then
					_ProjID_W = TearOff.ProjID
				End If
				Return _ProjID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property ShortDesc() As AggregateParameter 
			Get
				If _ShortDesc_W Is Nothing Then
					_ShortDesc_W = TearOff.ShortDesc
				End If
				Return _ShortDesc_W
			End Get
		End Property

		Public ReadOnly Property Description() As AggregateParameter 
			Get
				If _Description_W Is Nothing Then
					_Description_W = TearOff.Description
				End If
				Return _Description_W
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As AggregateParameter 
			Get
				If _WorkForSubID_W Is Nothing Then
					_WorkForSubID_W = TearOff.WorkForSubID
				End If
				Return _WorkForSubID_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _ProjID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _ShortDesc_W As AggregateParameter = Nothing
		Private _Description_W As AggregateParameter = Nothing
		Private _WorkForSubID_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ProjID_W = Nothing
		_CompanyID_W = Nothing
		_ShortDesc_W = Nothing
		_Description_W = Nothing
		_WorkForSubID_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpdDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkForSubProjInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ProjID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkForSubProjUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkForSubProjDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ProjID)
		p.SourceColumn = ColumnNames.ProjID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ProjID)
		p.SourceColumn = ColumnNames.ProjID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ShortDesc)
		p.SourceColumn = ColumnNames.ShortDesc
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Description)
		p.SourceColumn = ColumnNames.Description
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkForSubID)
		p.SourceColumn = ColumnNames.WorkForSubID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

