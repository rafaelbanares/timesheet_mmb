
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

Namespace BNS.TK.Entities

    Public MustInherit Class _Approver
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "Approver"
            Me.MappingName = "Approver"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_ApproverLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal ApproverID As String) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_Approver.Parameters.CompanyID, CompanyID)
            parameters.Add(_Approver.Parameters.ApproverID, ApproverID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_ApproverLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property CompanyID() As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property ApproverID() As SqlParameter
                Get
                    Return New SqlParameter("@ApproverID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Alternative1() As SqlParameter
                Get
                    Return New SqlParameter("@Alternative1", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Alternative2() As SqlParameter
                Get
                    Return New SqlParameter("@Alternative2", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Alternative3() As SqlParameter
                Get
                    Return New SqlParameter("@Alternative3", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Alt1CompID() As SqlParameter
                Get
                    Return New SqlParameter("@Alt1CompID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Alt2CompID() As SqlParameter
                Get
                    Return New SqlParameter("@Alt2CompID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Alt3CompID() As SqlParameter
                Get
                    Return New SqlParameter("@Alt3CompID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedBy() As SqlParameter
                Get
                    Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate() As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy() As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate() As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const CompanyID As String = "CompanyID"
            Public Const ApproverID As String = "ApproverID"
            Public Const Alternative1 As String = "Alternative1"
            Public Const Alternative2 As String = "Alternative2"
            Public Const Alternative3 As String = "Alternative3"
            Public Const Alt1CompID As String = "Alt1CompID"
            Public Const Alt2CompID As String = "Alt2CompID"
            Public Const Alt3CompID As String = "Alt3CompID"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Approver.PropertyNames.CompanyID
                    ht(ApproverID) = _Approver.PropertyNames.ApproverID
                    ht(Alternative1) = _Approver.PropertyNames.Alternative1
                    ht(Alternative2) = _Approver.PropertyNames.Alternative2
                    ht(Alternative3) = _Approver.PropertyNames.Alternative3
                    ht(Alt1CompID) = _Approver.PropertyNames.Alt1CompID
                    ht(Alt2CompID) = _Approver.PropertyNames.Alt2CompID
                    ht(Alt3CompID) = _Approver.PropertyNames.Alt3CompID
                    ht(CreatedBy) = _Approver.PropertyNames.CreatedBy
                    ht(CreatedDate) = _Approver.PropertyNames.CreatedDate
                    ht(LastUpdBy) = _Approver.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _Approver.PropertyNames.LastUpdDate

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const CompanyID As String = "CompanyID"
            Public Const ApproverID As String = "ApproverID"
            Public Const Alternative1 As String = "Alternative1"
            Public Const Alternative2 As String = "Alternative2"
            Public Const Alternative3 As String = "Alternative3"
            Public Const Alt1CompID As String = "Alt1CompID"
            Public Const Alt2CompID As String = "Alt2CompID"
            Public Const Alt3CompID As String = "Alt3CompID"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Approver.ColumnNames.CompanyID
                    ht(ApproverID) = _Approver.ColumnNames.ApproverID
                    ht(Alternative1) = _Approver.ColumnNames.Alternative1
                    ht(Alternative2) = _Approver.ColumnNames.Alternative2
                    ht(Alternative3) = _Approver.ColumnNames.Alternative3
                    ht(Alt1CompID) = _Approver.ColumnNames.Alt1CompID
                    ht(Alt2CompID) = _Approver.ColumnNames.Alt2CompID
                    ht(Alt3CompID) = _Approver.ColumnNames.Alt3CompID
                    ht(CreatedBy) = _Approver.ColumnNames.CreatedBy
                    ht(CreatedDate) = _Approver.ColumnNames.CreatedDate
                    ht(LastUpdBy) = _Approver.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _Approver.ColumnNames.LastUpdDate

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const CompanyID As String = "s_CompanyID"
            Public Const ApproverID As String = "s_ApproverID"
            Public Const Alternative1 As String = "s_Alternative1"
            Public Const Alternative2 As String = "s_Alternative2"
            Public Const Alternative3 As String = "s_Alternative3"
            Public Const Alt1CompID As String = "s_Alt1CompID"
            Public Const Alt2CompID As String = "s_Alt2CompID"
            Public Const Alt3CompID As String = "s_Alt3CompID"
            Public Const CreatedBy As String = "s_CreatedBy"
            Public Const CreatedDate As String = "s_CreatedDate"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property CompanyID() As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property ApproverID() As String
            Get
                Return MyBase.GetString(ColumnNames.ApproverID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.ApproverID, Value)
            End Set
        End Property

        Public Overridable Property Alternative1() As String
            Get
                Return MyBase.GetString(ColumnNames.Alternative1)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Alternative1, Value)
            End Set
        End Property

        Public Overridable Property Alternative2() As String
            Get
                Return MyBase.GetString(ColumnNames.Alternative2)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Alternative2, Value)
            End Set
        End Property

        Public Overridable Property Alternative3() As String
            Get
                Return MyBase.GetString(ColumnNames.Alternative3)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Alternative3, Value)
            End Set
        End Property

        Public Overridable Property Alt1CompID() As String
            Get
                Return MyBase.GetString(ColumnNames.Alt1CompID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Alt1CompID, Value)
            End Set
        End Property

        Public Overridable Property Alt2CompID() As String
            Get
                Return MyBase.GetString(ColumnNames.Alt2CompID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Alt2CompID, Value)
            End Set
        End Property

        Public Overridable Property Alt3CompID() As String
            Get
                Return MyBase.GetString(ColumnNames.Alt3CompID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Alt3CompID, Value)
            End Set
        End Property

        Public Overridable Property CreatedBy() As String
            Get
                Return MyBase.GetString(ColumnNames.CreatedBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CreatedBy, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy() As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_CompanyID() As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ApproverID() As String
            Get
                If Me.IsColumnNull(ColumnNames.ApproverID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.ApproverID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ApproverID)
                Else
                    Me.ApproverID = MyBase.SetStringAsString(ColumnNames.ApproverID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Alternative1() As String
            Get
                If Me.IsColumnNull(ColumnNames.Alternative1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Alternative1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Alternative1)
                Else
                    Me.Alternative1 = MyBase.SetStringAsString(ColumnNames.Alternative1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Alternative2() As String
            Get
                If Me.IsColumnNull(ColumnNames.Alternative2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Alternative2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Alternative2)
                Else
                    Me.Alternative2 = MyBase.SetStringAsString(ColumnNames.Alternative2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Alternative3() As String
            Get
                If Me.IsColumnNull(ColumnNames.Alternative3) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Alternative3)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Alternative3)
                Else
                    Me.Alternative3 = MyBase.SetStringAsString(ColumnNames.Alternative3, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Alt1CompID() As String
            Get
                If Me.IsColumnNull(ColumnNames.Alt1CompID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Alt1CompID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Alt1CompID)
                Else
                    Me.Alt1CompID = MyBase.SetStringAsString(ColumnNames.Alt1CompID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Alt2CompID() As String
            Get
                If Me.IsColumnNull(ColumnNames.Alt2CompID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Alt2CompID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Alt2CompID)
                Else
                    Me.Alt2CompID = MyBase.SetStringAsString(ColumnNames.Alt2CompID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Alt3CompID() As String
            Get
                If Me.IsColumnNull(ColumnNames.Alt3CompID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Alt3CompID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Alt3CompID)
                Else
                    Me.Alt3CompID = MyBase.SetStringAsString(ColumnNames.Alt3CompID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedBy() As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedBy)
                Else
                    Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate() As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy() As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate() As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff() As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ApproverID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ApproverID, Parameters.ApproverID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alternative1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Alternative1, Parameters.Alternative1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alternative2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Alternative2, Parameters.Alternative2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alternative3() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Alternative3, Parameters.Alternative3)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alt1CompID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Alt1CompID, Parameters.Alt1CompID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alt2CompID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Alt2CompID, Parameters.Alt2CompID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alt3CompID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Alt3CompID, Parameters.Alt3CompID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property ApproverID() As WhereParameter
                Get
                    If _ApproverID_W Is Nothing Then
                        _ApproverID_W = TearOff.ApproverID
                    End If
                    Return _ApproverID_W
                End Get
            End Property

            Public ReadOnly Property Alternative1() As WhereParameter
                Get
                    If _Alternative1_W Is Nothing Then
                        _Alternative1_W = TearOff.Alternative1
                    End If
                    Return _Alternative1_W
                End Get
            End Property

            Public ReadOnly Property Alternative2() As WhereParameter
                Get
                    If _Alternative2_W Is Nothing Then
                        _Alternative2_W = TearOff.Alternative2
                    End If
                    Return _Alternative2_W
                End Get
            End Property

            Public ReadOnly Property Alternative3() As WhereParameter
                Get
                    If _Alternative3_W Is Nothing Then
                        _Alternative3_W = TearOff.Alternative3
                    End If
                    Return _Alternative3_W
                End Get
            End Property


            Public ReadOnly Property Alt1CompID() As WhereParameter
                Get
                    If _Alt1CompID_W Is Nothing Then
                        _Alt1CompID_W = TearOff.Alt1CompID
                    End If
                    Return _Alt1CompID_W
                End Get
            End Property

            Public ReadOnly Property Alt2CompID() As WhereParameter
                Get
                    If _Alt2CompID_W Is Nothing Then
                        _Alt2CompID_W = TearOff.Alt2CompID
                    End If
                    Return _Alt2CompID_W
                End Get
            End Property

            Public ReadOnly Property Alt3CompID() As WhereParameter
                Get
                    If _Alt3CompID_W Is Nothing Then
                        _Alt3CompID_W = TearOff.Alt3CompID
                    End If
                    Return _Alt3CompID_W
                End Get
            End Property


            Public ReadOnly Property CreatedBy() As WhereParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Private _CompanyID_W As WhereParameter = Nothing
            Private _ApproverID_W As WhereParameter = Nothing
            Private _Alternative1_W As WhereParameter = Nothing
            Private _Alternative2_W As WhereParameter = Nothing
            Private _Alternative3_W As WhereParameter = Nothing
            Private _Alt1CompID_W As WhereParameter = Nothing
            Private _Alt2CompID_W As WhereParameter = Nothing
            Private _Alt3CompID_W As WhereParameter = Nothing
            Private _CreatedBy_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _CompanyID_W = Nothing
                _ApproverID_W = Nothing
                _Alternative1_W = Nothing
                _Alternative2_W = Nothing
                _Alternative3_W = Nothing
                _Alt1CompID_W = Nothing
                _Alt2CompID_W = Nothing
                _Alt3CompID_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff() As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ApproverID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApproverID, Parameters.ApproverID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alternative1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Alternative1, Parameters.Alternative1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alternative2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Alternative2, Parameters.Alternative2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alternative3() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Alternative3, Parameters.Alternative3)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alt1CompID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Alt1CompID, Parameters.Alt1CompID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alt2CompID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Alt2CompID, Parameters.Alt2CompID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Alt3CompID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Alt3CompID, Parameters.Alt3CompID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property ApproverID() As AggregateParameter
                Get
                    If _ApproverID_W Is Nothing Then
                        _ApproverID_W = TearOff.ApproverID
                    End If
                    Return _ApproverID_W
                End Get
            End Property

            Public ReadOnly Property Alternative1() As AggregateParameter
                Get
                    If _Alternative1_W Is Nothing Then
                        _Alternative1_W = TearOff.Alternative1
                    End If
                    Return _Alternative1_W
                End Get
            End Property

            Public ReadOnly Property Alternative2() As AggregateParameter
                Get
                    If _Alternative2_W Is Nothing Then
                        _Alternative2_W = TearOff.Alternative2
                    End If
                    Return _Alternative2_W
                End Get
            End Property

            Public ReadOnly Property Alternative3() As AggregateParameter
                Get
                    If _Alternative3_W Is Nothing Then
                        _Alternative3_W = TearOff.Alternative3
                    End If
                    Return _Alternative3_W
                End Get
            End Property

            Public ReadOnly Property Alt1CompID() As AggregateParameter
                Get
                    If _Alt1CompID_W Is Nothing Then
                        _Alt1CompID_W = TearOff.Alt1CompID
                    End If
                    Return _Alt1CompID_W
                End Get
            End Property

            Public ReadOnly Property Alt2CompID() As AggregateParameter
                Get
                    If _Alt2CompID_W Is Nothing Then
                        _Alt2CompID_W = TearOff.Alt2CompID
                    End If
                    Return _Alt2CompID_W
                End Get
            End Property

            Public ReadOnly Property Alt3CompID() As AggregateParameter
                Get
                    If _Alt3CompID_W Is Nothing Then
                        _Alt3CompID_W = TearOff.Alt3CompID
                    End If
                    Return _Alt3CompID_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As AggregateParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Private _CompanyID_W As AggregateParameter = Nothing
            Private _ApproverID_W As AggregateParameter = Nothing
            Private _Alternative1_W As AggregateParameter = Nothing
            Private _Alternative2_W As AggregateParameter = Nothing
            Private _Alternative3_W As AggregateParameter = Nothing
            Private _Alt1CompID_W As AggregateParameter = Nothing
            Private _Alt2CompID_W As AggregateParameter = Nothing
            Private _Alt3CompID_W As AggregateParameter = Nothing
            Private _CreatedBy_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _CompanyID_W = Nothing
                _ApproverID_W = Nothing
                _Alternative1_W = Nothing
                _Alternative2_W = Nothing
                _Alternative3_W = Nothing
                _Alt1CompID_W = Nothing
                _Alt2CompID_W = Nothing
                _Alt3CompID_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_ApproverInsert]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_ApproverUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_ApproverDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ApproverID)
            p.SourceColumn = ColumnNames.ApproverID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ApproverID)
            p.SourceColumn = ColumnNames.ApproverID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Alternative1)
            p.SourceColumn = ColumnNames.Alternative1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Alternative2)
            p.SourceColumn = ColumnNames.Alternative2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Alternative3)
            p.SourceColumn = ColumnNames.Alternative3
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Alt1CompID)
            p.SourceColumn = ColumnNames.Alt1CompID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Alt2CompID)
            p.SourceColumn = ColumnNames.Alt2CompID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Alt3CompID)
            p.SourceColumn = ColumnNames.Alt3CompID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedBy)
            p.SourceColumn = ColumnNames.CreatedBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

