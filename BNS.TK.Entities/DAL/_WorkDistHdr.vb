
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _WorkDistHdr
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "WorkDistHdr"
            Me.MappingName = "WorkDistHdr"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkDistHdrLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal WorkDistID As Long) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_WorkDistHdr.Parameters.WorkDistID, WorkDistID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkDistHdrLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property WorkDistID() As SqlParameter
                Get
                    Return New SqlParameter("@WorkDistID", SqlDbType.BigInt, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CompanyID() As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property EmployeeID() As SqlParameter
                Get
                    Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Date1() As SqlParameter
                Get
                    Return New SqlParameter("@Date", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Orig_in1() As SqlParameter
                Get
                    Return New SqlParameter("@Orig_in1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Orig_out1() As SqlParameter
                Get
                    Return New SqlParameter("@Orig_out1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property New_in1() As SqlParameter
                Get
                    Return New SqlParameter("@New_in1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property New_out1() As SqlParameter
                Get
                    Return New SqlParameter("@New_out1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Reason() As SqlParameter
                Get
                    Return New SqlParameter("@Reason", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property Excuse_leave() As SqlParameter
                Get
                    Return New SqlParameter("@Excuse_leave", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Excuse_date() As SqlParameter
                Get
                    Return New SqlParameter("@Excuse_date", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LeaveCode() As SqlParameter
                Get
                    Return New SqlParameter("@LeaveCode", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property LeaveHours() As SqlParameter
                Get
                    Return New SqlParameter("@LeaveHours", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Status() As SqlParameter
                Get
                    Return New SqlParameter("@Status", SqlDbType.VarChar, 12)
                End Get
            End Property

            Public Shared ReadOnly Property Stage() As SqlParameter
                Get
                    Return New SqlParameter("@Stage", SqlDbType.SmallInt, 0)
                End Get
            End Property

            Public Shared ReadOnly Property AuthBy() As SqlParameter
                Get
                    Return New SqlParameter("@AuthBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Completed() As SqlParameter
                Get
                    Return New SqlParameter("@Completed", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy() As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate() As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate() As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property DeclinedReason() As SqlParameter
                Get
                    Return New SqlParameter("@DeclinedReason", SqlDbType.VarChar, 100)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const WorkDistID As String = "WorkDistID"
            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const Date1 As String = "Date"
            Public Const Orig_in1 As String = "orig_in1"
            Public Const Orig_out1 As String = "orig_out1"
            Public Const New_in1 As String = "new_in1"
            Public Const New_out1 As String = "new_out1"
            Public Const Reason As String = "reason"
            Public Const Excuse_leave As String = "excuse_leave"
            Public Const Excuse_date As String = "excuse_date"
            Public Const LeaveCode As String = "leaveCode"
            Public Const LeaveHours As String = "leaveHours"
            Public Const Status As String = "Status"
            Public Const Stage As String = "Stage"
            Public Const AuthBy As String = "AuthBy"
            Public Const Completed As String = "Completed"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const DeclinedReason As String = "DeclinedReason"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(WorkDistID) = _WorkDistHdr.PropertyNames.WorkDistID
                    ht(CompanyID) = _WorkDistHdr.PropertyNames.CompanyID
                    ht(EmployeeID) = _WorkDistHdr.PropertyNames.EmployeeID
                    ht(Date1) = _WorkDistHdr.PropertyNames.Date1
                    ht(Orig_in1) = _WorkDistHdr.PropertyNames.Orig_in1
                    ht(Orig_out1) = _WorkDistHdr.PropertyNames.Orig_out1
                    ht(New_in1) = _WorkDistHdr.PropertyNames.New_in1
                    ht(New_out1) = _WorkDistHdr.PropertyNames.New_out1
                    ht(Reason) = _WorkDistHdr.PropertyNames.Reason
                    ht(Excuse_leave) = _WorkDistHdr.PropertyNames.Excuse_leave
                    ht(Excuse_date) = _WorkDistHdr.PropertyNames.Excuse_date
                    ht(LeaveCode) = _WorkDistHdr.PropertyNames.LeaveCode
                    ht(LeaveHours) = _WorkDistHdr.PropertyNames.LeaveHours
                    ht(Status) = _WorkDistHdr.PropertyNames.Status
                    ht(Stage) = _WorkDistHdr.PropertyNames.Stage
                    ht(AuthBy) = _WorkDistHdr.PropertyNames.AuthBy
                    ht(Completed) = _WorkDistHdr.PropertyNames.Completed
                    ht(LastUpdBy) = _WorkDistHdr.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _WorkDistHdr.PropertyNames.LastUpdDate
                    ht(CreatedDate) = _WorkDistHdr.PropertyNames.CreatedDate
                    ht(DeclinedReason) = _WorkDistHdr.PropertyNames.DeclinedReason

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const WorkDistID As String = "WorkDistID"
            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const Date1 As String = "Date1"
            Public Const Orig_in1 As String = "Orig_in1"
            Public Const Orig_out1 As String = "Orig_out1"
            Public Const New_in1 As String = "New_in1"
            Public Const New_out1 As String = "New_out1"
            Public Const Reason As String = "Reason"
            Public Const Excuse_leave As String = "Excuse_leave"
            Public Const Excuse_date As String = "Excuse_date"
            Public Const LeaveCode As String = "LeaveCode"
            Public Const LeaveHours As String = "LeaveHours"
            Public Const Status As String = "Status"
            Public Const Stage As String = "Stage"
            Public Const AuthBy As String = "AuthBy"
            Public Const Completed As String = "Completed"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const DeclinedReason As String = "DeclinedReason"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(WorkDistID) = _WorkDistHdr.ColumnNames.WorkDistID
                    ht(CompanyID) = _WorkDistHdr.ColumnNames.CompanyID
                    ht(EmployeeID) = _WorkDistHdr.ColumnNames.EmployeeID
                    ht(Date1) = _WorkDistHdr.ColumnNames.Date1
                    ht(Orig_in1) = _WorkDistHdr.ColumnNames.Orig_in1
                    ht(Orig_out1) = _WorkDistHdr.ColumnNames.Orig_out1
                    ht(New_in1) = _WorkDistHdr.ColumnNames.New_in1
                    ht(New_out1) = _WorkDistHdr.ColumnNames.New_out1
                    ht(Reason) = _WorkDistHdr.ColumnNames.Reason
                    ht(Excuse_leave) = _WorkDistHdr.ColumnNames.Excuse_leave
                    ht(Excuse_date) = _WorkDistHdr.ColumnNames.Excuse_date
                    ht(LeaveCode) = _WorkDistHdr.ColumnNames.LeaveCode
                    ht(LeaveHours) = _WorkDistHdr.ColumnNames.LeaveHours
                    ht(Status) = _WorkDistHdr.ColumnNames.Status
                    ht(Stage) = _WorkDistHdr.ColumnNames.Stage
                    ht(AuthBy) = _WorkDistHdr.ColumnNames.AuthBy
                    ht(Completed) = _WorkDistHdr.ColumnNames.Completed
                    ht(LastUpdBy) = _WorkDistHdr.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _WorkDistHdr.ColumnNames.LastUpdDate
                    ht(CreatedDate) = _WorkDistHdr.ColumnNames.CreatedDate
                    ht(DeclinedReason) = _WorkDistHdr.ColumnNames.DeclinedReason

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const WorkDistID As String = "s_WorkDistID"
            Public Const CompanyID As String = "s_CompanyID"
            Public Const EmployeeID As String = "s_EmployeeID"
            Public Const Date1 As String = "s_Date1"
            Public Const Orig_in1 As String = "s_Orig_in1"
            Public Const Orig_out1 As String = "s_Orig_out1"
            Public Const New_in1 As String = "s_New_in1"
            Public Const New_out1 As String = "s_New_out1"
            Public Const Reason As String = "s_Reason"
            Public Const Excuse_leave As String = "s_Excuse_leave"
            Public Const Excuse_date As String = "s_Excuse_date"
            Public Const LeaveCode As String = "s_LeaveCode"
            Public Const LeaveHours As String = "s_LeaveHours"
            Public Const Status As String = "s_Status"
            Public Const Stage As String = "s_Stage"
            Public Const AuthBy As String = "s_AuthBy"
            Public Const Completed As String = "s_Completed"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"
            Public Const CreatedDate As String = "s_CreatedDate"
            Public Const DeclinedReason As String = "s_DeclinedReason"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property WorkDistID() As Long
            Get
                Return MyBase.GetLong(ColumnNames.WorkDistID)
            End Get
            Set(ByVal Value As Long)
                MyBase.SetLong(ColumnNames.WorkDistID, Value)
            End Set
        End Property

        Public Overridable Property CompanyID() As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property EmployeeID() As String
            Get
                Return MyBase.GetString(ColumnNames.EmployeeID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.EmployeeID, Value)
            End Set
        End Property

        Public Overridable Property Date1() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Date1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Date1, Value)
            End Set
        End Property

        Public Overridable Property Orig_in1() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Orig_in1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Orig_in1, Value)
            End Set
        End Property

        Public Overridable Property Orig_out1() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Orig_out1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Orig_out1, Value)
            End Set
        End Property

        Public Overridable Property New_in1() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.New_in1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.New_in1, Value)
            End Set
        End Property

        Public Overridable Property New_out1() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.New_out1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.New_out1, Value)
            End Set
        End Property

        Public Overridable Property Reason() As String
            Get
                Return MyBase.GetString(ColumnNames.Reason)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Reason, Value)
            End Set
        End Property

        Public Overridable Property Excuse_leave() As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Excuse_leave)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Excuse_leave, Value)
            End Set
        End Property

        Public Overridable Property Excuse_date() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Excuse_date)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Excuse_date, Value)
            End Set
        End Property

        Public Overridable Property LeaveCode() As String
            Get
                Return MyBase.GetString(ColumnNames.LeaveCode)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LeaveCode, Value)
            End Set
        End Property

        Public Overridable Property LeaveHours() As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.LeaveHours)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.LeaveHours, Value)
            End Set
        End Property

        Public Overridable Property Status() As String
            Get
                Return MyBase.GetString(ColumnNames.Status)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Status, Value)
            End Set
        End Property

        Public Overridable Property Stage() As Short
            Get
                Return MyBase.GetShort(ColumnNames.Stage)
            End Get
            Set(ByVal Value As Short)
                MyBase.SetShort(ColumnNames.Stage, Value)
            End Set
        End Property

        Public Overridable Property AuthBy() As String
            Get
                Return MyBase.GetString(ColumnNames.AuthBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.AuthBy, Value)
            End Set
        End Property

        Public Overridable Property Completed() As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Completed)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Completed, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy() As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate() As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property

        Public Overridable Property DeclinedReason() As String
            Get
                Return MyBase.GetString(ColumnNames.DeclinedReason)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.DeclinedReason, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_WorkDistID() As String
            Get
                If Me.IsColumnNull(ColumnNames.WorkDistID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetLongAsString(ColumnNames.WorkDistID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.WorkDistID)
                Else
                    Me.WorkDistID = MyBase.SetLongAsString(ColumnNames.WorkDistID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CompanyID() As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EmployeeID() As String
            Get
                If Me.IsColumnNull(ColumnNames.EmployeeID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EmployeeID)
                Else
                    Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Date1() As String
            Get
                If Me.IsColumnNull(ColumnNames.Date1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Date1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Date1)
                Else
                    Me.Date1 = MyBase.SetDateTimeAsString(ColumnNames.Date1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Orig_in1() As String
            Get
                If Me.IsColumnNull(ColumnNames.Orig_in1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Orig_in1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Orig_in1)
                Else
                    Me.Orig_in1 = MyBase.SetDateTimeAsString(ColumnNames.Orig_in1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Orig_out1() As String
            Get
                If Me.IsColumnNull(ColumnNames.Orig_out1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Orig_out1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Orig_out1)
                Else
                    Me.Orig_out1 = MyBase.SetDateTimeAsString(ColumnNames.Orig_out1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_New_in1() As String
            Get
                If Me.IsColumnNull(ColumnNames.New_in1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.New_in1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.New_in1)
                Else
                    Me.New_in1 = MyBase.SetDateTimeAsString(ColumnNames.New_in1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_New_out1() As String
            Get
                If Me.IsColumnNull(ColumnNames.New_out1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.New_out1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.New_out1)
                Else
                    Me.New_out1 = MyBase.SetDateTimeAsString(ColumnNames.New_out1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Reason() As String
            Get
                If Me.IsColumnNull(ColumnNames.Reason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Reason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Reason)
                Else
                    Me.Reason = MyBase.SetStringAsString(ColumnNames.Reason, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Excuse_leave() As String
            Get
                If Me.IsColumnNull(ColumnNames.Excuse_leave) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Excuse_leave)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Excuse_leave)
                Else
                    Me.Excuse_leave = MyBase.SetBooleanAsString(ColumnNames.Excuse_leave, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Excuse_date() As String
            Get
                If Me.IsColumnNull(ColumnNames.Excuse_date) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Excuse_date)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Excuse_date)
                Else
                    Me.Excuse_date = MyBase.SetDateTimeAsString(ColumnNames.Excuse_date, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LeaveCode() As String
            Get
                If Me.IsColumnNull(ColumnNames.LeaveCode) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LeaveCode)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LeaveCode)
                Else
                    Me.LeaveCode = MyBase.SetStringAsString(ColumnNames.LeaveCode, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LeaveHours() As String
            Get
                If Me.IsColumnNull(ColumnNames.LeaveHours) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.LeaveHours)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LeaveHours)
                Else
                    Me.LeaveHours = MyBase.SetDecimalAsString(ColumnNames.LeaveHours, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Status() As String
            Get
                If Me.IsColumnNull(ColumnNames.Status) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Status)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Status)
                Else
                    Me.Status = MyBase.SetStringAsString(ColumnNames.Status, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Stage() As String
            Get
                If Me.IsColumnNull(ColumnNames.Stage) Then
                    Return String.Empty
                Else
                    Return MyBase.GetShortAsString(ColumnNames.Stage)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Stage)
                Else
                    Me.Stage = MyBase.SetShortAsString(ColumnNames.Stage, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_AuthBy() As String
            Get
                If Me.IsColumnNull(ColumnNames.AuthBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.AuthBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.AuthBy)
                Else
                    Me.AuthBy = MyBase.SetStringAsString(ColumnNames.AuthBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Completed() As String
            Get
                If Me.IsColumnNull(ColumnNames.Completed) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Completed)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Completed)
                Else
                    Me.Completed = MyBase.SetBooleanAsString(ColumnNames.Completed, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy() As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate() As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate() As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_DeclinedReason() As String
            Get
                If Me.IsColumnNull(ColumnNames.DeclinedReason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.DeclinedReason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.DeclinedReason)
                Else
                    Me.DeclinedReason = MyBase.SetStringAsString(ColumnNames.DeclinedReason, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff() As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property WorkDistID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkDistID, Parameters.WorkDistID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Date1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Date1, Parameters.Date1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_in1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Orig_in1, Parameters.Orig_in1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_out1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Orig_out1, Parameters.Orig_out1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property New_in1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.New_in1, Parameters.New_in1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property New_out1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.New_out1, Parameters.New_out1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Reason, Parameters.Reason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excuse_leave() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Excuse_leave, Parameters.Excuse_leave)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excuse_date() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Excuse_date, Parameters.Excuse_date)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveCode() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveCode, Parameters.LeaveCode)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveHours() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveHours, Parameters.LeaveHours)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property AuthBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.AuthBy, Parameters.AuthBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Completed() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Completed, Parameters.Completed)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property WorkDistID() As WhereParameter
                Get
                    If _WorkDistID_W Is Nothing Then
                        _WorkDistID_W = TearOff.WorkDistID
                    End If
                    Return _WorkDistID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As WhereParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property Date1() As WhereParameter
                Get
                    If _Date1_W Is Nothing Then
                        _Date1_W = TearOff.Date1
                    End If
                    Return _Date1_W
                End Get
            End Property

            Public ReadOnly Property Orig_in1() As WhereParameter
                Get
                    If _Orig_in1_W Is Nothing Then
                        _Orig_in1_W = TearOff.Orig_in1
                    End If
                    Return _Orig_in1_W
                End Get
            End Property

            Public ReadOnly Property Orig_out1() As WhereParameter
                Get
                    If _Orig_out1_W Is Nothing Then
                        _Orig_out1_W = TearOff.Orig_out1
                    End If
                    Return _Orig_out1_W
                End Get
            End Property

            Public ReadOnly Property New_in1() As WhereParameter
                Get
                    If _New_in1_W Is Nothing Then
                        _New_in1_W = TearOff.New_in1
                    End If
                    Return _New_in1_W
                End Get
            End Property

            Public ReadOnly Property New_out1() As WhereParameter
                Get
                    If _New_out1_W Is Nothing Then
                        _New_out1_W = TearOff.New_out1
                    End If
                    Return _New_out1_W
                End Get
            End Property

            Public ReadOnly Property Reason() As WhereParameter
                Get
                    If _Reason_W Is Nothing Then
                        _Reason_W = TearOff.Reason
                    End If
                    Return _Reason_W
                End Get
            End Property

            Public ReadOnly Property Excuse_leave() As WhereParameter
                Get
                    If _Excuse_leave_W Is Nothing Then
                        _Excuse_leave_W = TearOff.Excuse_leave
                    End If
                    Return _Excuse_leave_W
                End Get
            End Property

            Public ReadOnly Property Excuse_date() As WhereParameter
                Get
                    If _Excuse_date_W Is Nothing Then
                        _Excuse_date_W = TearOff.Excuse_date
                    End If
                    Return _Excuse_date_W
                End Get
            End Property

            Public ReadOnly Property LeaveCode() As WhereParameter
                Get
                    If _LeaveCode_W Is Nothing Then
                        _LeaveCode_W = TearOff.LeaveCode
                    End If
                    Return _LeaveCode_W
                End Get
            End Property

            Public ReadOnly Property LeaveHours() As WhereParameter
                Get
                    If _LeaveHours_W Is Nothing Then
                        _LeaveHours_W = TearOff.LeaveHours
                    End If
                    Return _LeaveHours_W
                End Get
            End Property

            Public ReadOnly Property Status() As WhereParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Stage() As WhereParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property AuthBy() As WhereParameter
                Get
                    If _AuthBy_W Is Nothing Then
                        _AuthBy_W = TearOff.AuthBy
                    End If
                    Return _AuthBy_W
                End Get
            End Property

            Public ReadOnly Property Completed() As WhereParameter
                Get
                    If _Completed_W Is Nothing Then
                        _Completed_W = TearOff.Completed
                    End If
                    Return _Completed_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As WhereParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Private _WorkDistID_W As WhereParameter = Nothing
            Private _CompanyID_W As WhereParameter = Nothing
            Private _EmployeeID_W As WhereParameter = Nothing
            Private _Date1_W As WhereParameter = Nothing
            Private _Orig_in1_W As WhereParameter = Nothing
            Private _Orig_out1_W As WhereParameter = Nothing
            Private _New_in1_W As WhereParameter = Nothing
            Private _New_out1_W As WhereParameter = Nothing
            Private _Reason_W As WhereParameter = Nothing
            Private _Excuse_leave_W As WhereParameter = Nothing
            Private _Excuse_date_W As WhereParameter = Nothing
            Private _LeaveCode_W As WhereParameter = Nothing
            Private _LeaveHours_W As WhereParameter = Nothing
            Private _Status_W As WhereParameter = Nothing
            Private _Stage_W As WhereParameter = Nothing
            Private _AuthBy_W As WhereParameter = Nothing
            Private _Completed_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing
            Private _DeclinedReason_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _WorkDistID_W = Nothing
                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _Date1_W = Nothing
                _Orig_in1_W = Nothing
                _Orig_out1_W = Nothing
                _New_in1_W = Nothing
                _New_out1_W = Nothing
                _Reason_W = Nothing
                _Excuse_leave_W = Nothing
                _Excuse_date_W = Nothing
                _LeaveCode_W = Nothing
                _LeaveHours_W = Nothing
                _Status_W = Nothing
                _Stage_W = Nothing
                _AuthBy_W = Nothing
                _Completed_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _CreatedDate_W = Nothing
                _DeclinedReason_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff() As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property WorkDistID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkDistID, Parameters.WorkDistID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Date1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Date1, Parameters.Date1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_in1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Orig_in1, Parameters.Orig_in1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_out1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Orig_out1, Parameters.Orig_out1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property New_in1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.New_in1, Parameters.New_in1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property New_out1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.New_out1, Parameters.New_out1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Reason, Parameters.Reason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excuse_leave() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Excuse_leave, Parameters.Excuse_leave)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excuse_date() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Excuse_date, Parameters.Excuse_date)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveCode() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveCode, Parameters.LeaveCode)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveHours() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveHours, Parameters.LeaveHours)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property AuthBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AuthBy, Parameters.AuthBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Completed() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Completed, Parameters.Completed)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property WorkDistID() As AggregateParameter
                Get
                    If _WorkDistID_W Is Nothing Then
                        _WorkDistID_W = TearOff.WorkDistID
                    End If
                    Return _WorkDistID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As AggregateParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property Date1() As AggregateParameter
                Get
                    If _Date1_W Is Nothing Then
                        _Date1_W = TearOff.Date1
                    End If
                    Return _Date1_W
                End Get
            End Property

            Public ReadOnly Property Orig_in1() As AggregateParameter
                Get
                    If _Orig_in1_W Is Nothing Then
                        _Orig_in1_W = TearOff.Orig_in1
                    End If
                    Return _Orig_in1_W
                End Get
            End Property

            Public ReadOnly Property Orig_out1() As AggregateParameter
                Get
                    If _Orig_out1_W Is Nothing Then
                        _Orig_out1_W = TearOff.Orig_out1
                    End If
                    Return _Orig_out1_W
                End Get
            End Property

            Public ReadOnly Property New_in1() As AggregateParameter
                Get
                    If _New_in1_W Is Nothing Then
                        _New_in1_W = TearOff.New_in1
                    End If
                    Return _New_in1_W
                End Get
            End Property

            Public ReadOnly Property New_out1() As AggregateParameter
                Get
                    If _New_out1_W Is Nothing Then
                        _New_out1_W = TearOff.New_out1
                    End If
                    Return _New_out1_W
                End Get
            End Property

            Public ReadOnly Property Reason() As AggregateParameter
                Get
                    If _Reason_W Is Nothing Then
                        _Reason_W = TearOff.Reason
                    End If
                    Return _Reason_W
                End Get
            End Property

            Public ReadOnly Property Excuse_leave() As AggregateParameter
                Get
                    If _Excuse_leave_W Is Nothing Then
                        _Excuse_leave_W = TearOff.Excuse_leave
                    End If
                    Return _Excuse_leave_W
                End Get
            End Property

            Public ReadOnly Property Excuse_date() As AggregateParameter
                Get
                    If _Excuse_date_W Is Nothing Then
                        _Excuse_date_W = TearOff.Excuse_date
                    End If
                    Return _Excuse_date_W
                End Get
            End Property

            Public ReadOnly Property LeaveCode() As AggregateParameter
                Get
                    If _LeaveCode_W Is Nothing Then
                        _LeaveCode_W = TearOff.LeaveCode
                    End If
                    Return _LeaveCode_W
                End Get
            End Property

            Public ReadOnly Property LeaveHours() As AggregateParameter
                Get
                    If _LeaveHours_W Is Nothing Then
                        _LeaveHours_W = TearOff.LeaveHours
                    End If
                    Return _LeaveHours_W
                End Get
            End Property

            Public ReadOnly Property Status() As AggregateParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Stage() As AggregateParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property AuthBy() As AggregateParameter
                Get
                    If _AuthBy_W Is Nothing Then
                        _AuthBy_W = TearOff.AuthBy
                    End If
                    Return _AuthBy_W
                End Get
            End Property

            Public ReadOnly Property Completed() As AggregateParameter
                Get
                    If _Completed_W Is Nothing Then
                        _Completed_W = TearOff.Completed
                    End If
                    Return _Completed_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As AggregateParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Private _WorkDistID_W As AggregateParameter = Nothing
            Private _CompanyID_W As AggregateParameter = Nothing
            Private _EmployeeID_W As AggregateParameter = Nothing
            Private _Date1_W As AggregateParameter = Nothing
            Private _Orig_in1_W As AggregateParameter = Nothing
            Private _Orig_out1_W As AggregateParameter = Nothing
            Private _New_in1_W As AggregateParameter = Nothing
            Private _New_out1_W As AggregateParameter = Nothing
            Private _Reason_W As AggregateParameter = Nothing
            Private _Excuse_leave_W As AggregateParameter = Nothing
            Private _Excuse_date_W As AggregateParameter = Nothing
            Private _LeaveCode_W As AggregateParameter = Nothing
            Private _LeaveHours_W As AggregateParameter = Nothing
            Private _Status_W As AggregateParameter = Nothing
            Private _Stage_W As AggregateParameter = Nothing
            Private _AuthBy_W As AggregateParameter = Nothing
            Private _Completed_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing
            Private _DeclinedReason_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _WorkDistID_W = Nothing
                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _Date1_W = Nothing
                _Orig_in1_W = Nothing
                _Orig_out1_W = Nothing
                _New_in1_W = Nothing
                _New_out1_W = Nothing
                _Reason_W = Nothing
                _Excuse_leave_W = Nothing
                _Excuse_date_W = Nothing
                _LeaveCode_W = Nothing
                _LeaveHours_W = Nothing
                _Status_W = Nothing
                _Stage_W = Nothing
                _AuthBy_W = Nothing
                _Completed_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _CreatedDate_W = Nothing
                _DeclinedReason_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkDistHdrInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.WorkDistID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkDistHdrUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkDistHdrDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.WorkDistID)
            p.SourceColumn = ColumnNames.WorkDistID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.WorkDistID)
            p.SourceColumn = ColumnNames.WorkDistID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Date1)
            p.SourceColumn = ColumnNames.Date1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Orig_in1)
            p.SourceColumn = ColumnNames.Orig_in1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Orig_out1)
            p.SourceColumn = ColumnNames.Orig_out1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.New_in1)
            p.SourceColumn = ColumnNames.New_in1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.New_out1)
            p.SourceColumn = ColumnNames.New_out1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Reason)
            p.SourceColumn = ColumnNames.Reason
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Excuse_leave)
            p.SourceColumn = ColumnNames.Excuse_leave
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Excuse_date)
            p.SourceColumn = ColumnNames.Excuse_date
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LeaveCode)
            p.SourceColumn = ColumnNames.LeaveCode
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LeaveHours)
            p.SourceColumn = ColumnNames.LeaveHours
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Status)
            p.SourceColumn = ColumnNames.Status
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Stage)
            p.SourceColumn = ColumnNames.Stage
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.AuthBy)
            p.SourceColumn = ColumnNames.AuthBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Completed)
            p.SourceColumn = ColumnNames.Completed
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.DeclinedReason)
            p.SourceColumn = ColumnNames.DeclinedReason
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

