
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _LeaveTrans
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "LeaveTrans"
            Me.MappingName = "LeaveTrans"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveTransLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_LeaveTrans.Parameters.ID, ID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveTransLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property ID As SqlParameter
                Get
                    Return New SqlParameter("@ID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LeaveHdrID As SqlParameter
                Get
                    Return New SqlParameter("@LeaveHdrID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property LeaveDate As SqlParameter
                Get
                    Return New SqlParameter("@LeaveDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Days As SqlParameter
                Get
                    Return New SqlParameter("@Days", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Cnv_Hours As SqlParameter
                Get
                    Return New SqlParameter("@Cnv_Hours", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Cnv_Mins As SqlParameter
                Get
                    Return New SqlParameter("@Cnv_Mins", SqlDbType.Int, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const ID As String = "ID"
            Public Const LeaveHdrID As String = "LeaveHdrID"
            Public Const CompanyID As String = "CompanyID"
            Public Const LeaveDate As String = "LeaveDate"
            Public Const Days As String = "Days"
            Public Const Cnv_Hours As String = "Cnv_Hours"
            Public Const Cnv_Mins As String = "Cnv_Mins"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _LeaveTrans.PropertyNames.ID
                    ht(LeaveHdrID) = _LeaveTrans.PropertyNames.LeaveHdrID
                    ht(CompanyID) = _LeaveTrans.PropertyNames.CompanyID
                    ht(LeaveDate) = _LeaveTrans.PropertyNames.LeaveDate
                    ht(Days) = _LeaveTrans.PropertyNames.Days
                    ht(Cnv_Hours) = _LeaveTrans.PropertyNames.Cnv_Hours
                    ht(Cnv_Mins) = _LeaveTrans.PropertyNames.Cnv_Mins

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const ID As String = "ID"
            Public Const LeaveHdrID As String = "LeaveHdrID"
            Public Const CompanyID As String = "CompanyID"
            Public Const LeaveDate As String = "LeaveDate"
            Public Const Days As String = "Days"
            Public Const Cnv_Hours As String = "Cnv_Hours"
            Public Const Cnv_Mins As String = "Cnv_Mins"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _LeaveTrans.ColumnNames.ID
                    ht(LeaveHdrID) = _LeaveTrans.ColumnNames.LeaveHdrID
                    ht(CompanyID) = _LeaveTrans.ColumnNames.CompanyID
                    ht(LeaveDate) = _LeaveTrans.ColumnNames.LeaveDate
                    ht(Days) = _LeaveTrans.ColumnNames.Days
                    ht(Cnv_Hours) = _LeaveTrans.ColumnNames.Cnv_Hours
                    ht(Cnv_Mins) = _LeaveTrans.ColumnNames.Cnv_Mins

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const ID As String = "s_ID"
            Public Const LeaveHdrID As String = "s_LeaveHdrID"
            Public Const CompanyID As String = "s_CompanyID"
            Public Const LeaveDate As String = "s_LeaveDate"
            Public Const Days As String = "s_Days"
            Public Const Cnv_Hours As String = "s_Cnv_Hours"
            Public Const Cnv_Mins As String = "s_Cnv_Mins"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property ID As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.ID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.ID, Value)
            End Set
        End Property

        Public Overridable Property LeaveHdrID As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.LeaveHdrID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.LeaveHdrID, Value)
            End Set
        End Property

        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property LeaveDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LeaveDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LeaveDate, Value)
            End Set
        End Property

        Public Overridable Property Days As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Days)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Days, Value)
            End Set
        End Property

        Public Overridable Property Cnv_Hours As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Cnv_Hours)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Cnv_Hours, Value)
            End Set
        End Property

        Public Overridable Property Cnv_Mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Cnv_Mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Cnv_Mins, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_ID As String
            Get
                If Me.IsColumnNull(ColumnNames.ID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.ID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ID)
                Else
                    Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LeaveHdrID As String
            Get
                If Me.IsColumnNull(ColumnNames.LeaveHdrID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.LeaveHdrID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LeaveHdrID)
                Else
                    Me.LeaveHdrID = MyBase.SetIntegerAsString(ColumnNames.LeaveHdrID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LeaveDate As String
            Get
                If Me.IsColumnNull(ColumnNames.LeaveDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LeaveDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LeaveDate)
                Else
                    Me.LeaveDate = MyBase.SetDateTimeAsString(ColumnNames.LeaveDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Days As String
            Get
                If Me.IsColumnNull(ColumnNames.Days) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Days)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Days)
                Else
                    Me.Days = MyBase.SetDecimalAsString(ColumnNames.Days, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Cnv_Hours As String
            Get
                If Me.IsColumnNull(ColumnNames.Cnv_Hours) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Cnv_Hours)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Cnv_Hours)
                Else
                    Me.Cnv_Hours = MyBase.SetDecimalAsString(ColumnNames.Cnv_Hours, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Cnv_Mins As String
            Get
                If Me.IsColumnNull(ColumnNames.Cnv_Mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Cnv_Mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Cnv_Mins)
                Else
                    Me.Cnv_Mins = MyBase.SetIntegerAsString(ColumnNames.Cnv_Mins, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveHdrID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveHdrID, Parameters.LeaveHdrID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveDate, Parameters.LeaveDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Days() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Days, Parameters.Days)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Cnv_Hours() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Cnv_Hours, Parameters.Cnv_Hours)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Cnv_Mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Cnv_Mins, Parameters.Cnv_Mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property ID() As WhereParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property LeaveHdrID() As WhereParameter
                Get
                    If _LeaveHdrID_W Is Nothing Then
                        _LeaveHdrID_W = TearOff.LeaveHdrID
                    End If
                    Return _LeaveHdrID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property LeaveDate() As WhereParameter
                Get
                    If _LeaveDate_W Is Nothing Then
                        _LeaveDate_W = TearOff.LeaveDate
                    End If
                    Return _LeaveDate_W
                End Get
            End Property

            Public ReadOnly Property Days() As WhereParameter
                Get
                    If _Days_W Is Nothing Then
                        _Days_W = TearOff.Days
                    End If
                    Return _Days_W
                End Get
            End Property

            Public ReadOnly Property Cnv_Hours() As WhereParameter
                Get
                    If _Cnv_Hours_W Is Nothing Then
                        _Cnv_Hours_W = TearOff.Cnv_Hours
                    End If
                    Return _Cnv_Hours_W
                End Get
            End Property

            Public ReadOnly Property Cnv_Mins() As WhereParameter
                Get
                    If _Cnv_Mins_W Is Nothing Then
                        _Cnv_Mins_W = TearOff.Cnv_Mins
                    End If
                    Return _Cnv_Mins_W
                End Get
            End Property

            Private _ID_W As WhereParameter = Nothing
            Private _LeaveHdrID_W As WhereParameter = Nothing
            Private _CompanyID_W As WhereParameter = Nothing
            Private _LeaveDate_W As WhereParameter = Nothing
            Private _Days_W As WhereParameter = Nothing
            Private _Cnv_Hours_W As WhereParameter = Nothing
            Private _Cnv_Mins_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _ID_W = Nothing
                _LeaveHdrID_W = Nothing
                _CompanyID_W = Nothing
                _LeaveDate_W = Nothing
                _Days_W = Nothing
                _Cnv_Hours_W = Nothing
                _Cnv_Mins_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveHdrID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveHdrID, Parameters.LeaveHdrID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LeaveDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveDate, Parameters.LeaveDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Days() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Days, Parameters.Days)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Cnv_Hours() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Cnv_Hours, Parameters.Cnv_Hours)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Cnv_Mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Cnv_Mins, Parameters.Cnv_Mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property ID() As AggregateParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property LeaveHdrID() As AggregateParameter
                Get
                    If _LeaveHdrID_W Is Nothing Then
                        _LeaveHdrID_W = TearOff.LeaveHdrID
                    End If
                    Return _LeaveHdrID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property LeaveDate() As AggregateParameter
                Get
                    If _LeaveDate_W Is Nothing Then
                        _LeaveDate_W = TearOff.LeaveDate
                    End If
                    Return _LeaveDate_W
                End Get
            End Property

            Public ReadOnly Property Days() As AggregateParameter
                Get
                    If _Days_W Is Nothing Then
                        _Days_W = TearOff.Days
                    End If
                    Return _Days_W
                End Get
            End Property

            Public ReadOnly Property Cnv_Hours() As AggregateParameter
                Get
                    If _Cnv_Hours_W Is Nothing Then
                        _Cnv_Hours_W = TearOff.Cnv_Hours
                    End If
                    Return _Cnv_Hours_W
                End Get
            End Property

            Public ReadOnly Property Cnv_Mins() As AggregateParameter
                Get
                    If _Cnv_Mins_W Is Nothing Then
                        _Cnv_Mins_W = TearOff.Cnv_Mins
                    End If
                    Return _Cnv_Mins_W
                End Get
            End Property

            Private _ID_W As AggregateParameter = Nothing
            Private _LeaveHdrID_W As AggregateParameter = Nothing
            Private _CompanyID_W As AggregateParameter = Nothing
            Private _LeaveDate_W As AggregateParameter = Nothing
            Private _Days_W As AggregateParameter = Nothing
            Private _Cnv_Hours_W As AggregateParameter = Nothing
            Private _Cnv_Mins_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _ID_W = Nothing
                _LeaveHdrID_W = Nothing
                _CompanyID_W = Nothing
                _LeaveDate_W = Nothing
                _Days_W = Nothing
                _Cnv_Hours_W = Nothing
                _Cnv_Mins_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveTransInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.ID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveTransUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveTransDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LeaveHdrID)
            p.SourceColumn = ColumnNames.LeaveHdrID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LeaveDate)
            p.SourceColumn = ColumnNames.LeaveDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Days)
            p.SourceColumn = ColumnNames.Days
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Cnv_Hours)
            p.SourceColumn = ColumnNames.Cnv_Hours
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Cnv_Mins)
            p.SourceColumn = ColumnNames.Cnv_Mins
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

