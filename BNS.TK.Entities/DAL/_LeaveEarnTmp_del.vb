
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

Public MustInherit Class _LeaveEarnTmp_del
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "LeaveEarnTmp_del"
			Me.MappingName = "LeaveEarnTmp_del"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveEarnTmp_delLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal SessionID As Guid, ByVal Rowid As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_LeaveEarnTmp_del.Parameters.SessionID, SessionID)
		parameters.Add(_LeaveEarnTmp_del.Parameters.Rowid, Rowid)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveEarnTmp_delLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property SessionID As SqlParameter
			Get
				Return New SqlParameter("@SessionID", SqlDbType.UniqueIdentifier, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Rowid As SqlParameter
			Get
				Return New SqlParameter("@Rowid", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LeaveEarnID As SqlParameter
			Get
				Return New SqlParameter("@LeaveEarnID", SqlDbType.BigInt, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const SessionID As String = "SessionID"
        Public Const Rowid As String = "rowid"
        Public Const LeaveEarnID As String = "LeaveEarnID"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(SessionID) = _LeaveEarnTmp_del.PropertyNames.SessionID
				ht(Rowid) = _LeaveEarnTmp_del.PropertyNames.Rowid
				ht(LeaveEarnID) = _LeaveEarnTmp_del.PropertyNames.LeaveEarnID

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const SessionID As String = "SessionID"
        Public Const Rowid As String = "Rowid"
        Public Const LeaveEarnID As String = "LeaveEarnID"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(SessionID) = _LeaveEarnTmp_del.ColumnNames.SessionID
				ht(Rowid) = _LeaveEarnTmp_del.ColumnNames.Rowid
				ht(LeaveEarnID) = _LeaveEarnTmp_del.ColumnNames.LeaveEarnID

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const SessionID As String = "s_SessionID"
        Public Const Rowid As String = "s_Rowid"
        Public Const LeaveEarnID As String = "s_LeaveEarnID"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property SessionID As Guid
			Get
				Return MyBase.GetGuid(ColumnNames.SessionID)
			End Get
			Set(ByVal Value As Guid)
				MyBase.SetGuid(ColumnNames.SessionID, Value)
			End Set
		End Property

		Public Overridable Property Rowid As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.Rowid)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.Rowid, Value)
			End Set
		End Property

		Public Overridable Property LeaveEarnID As Long
			Get
				Return MyBase.GetLong(ColumnNames.LeaveEarnID)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.LeaveEarnID, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_SessionID As String
			Get
				If Me.IsColumnNull(ColumnNames.SessionID) Then
					Return String.Empty
				Else
					Return MyBase.GetGuidAsString(ColumnNames.SessionID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.SessionID)
				Else
					Me.SessionID = MyBase.SetGuidAsString(ColumnNames.SessionID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Rowid As String
			Get
				If Me.IsColumnNull(ColumnNames.Rowid) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.Rowid)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Rowid)
				Else
					Me.Rowid = MyBase.SetIntegerAsString(ColumnNames.Rowid, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LeaveEarnID As String
			Get
				If Me.IsColumnNull(ColumnNames.LeaveEarnID) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.LeaveEarnID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LeaveEarnID)
				Else
					Me.LeaveEarnID = MyBase.SetLongAsString(ColumnNames.LeaveEarnID, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property SessionID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.SessionID, Parameters.SessionID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Rowid() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Rowid, Parameters.Rowid)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LeaveEarnID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveEarnID, Parameters.LeaveEarnID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property SessionID() As WhereParameter 
			Get
				If _SessionID_W Is Nothing Then
					_SessionID_W = TearOff.SessionID
				End If
				Return _SessionID_W
			End Get
		End Property

		Public ReadOnly Property Rowid() As WhereParameter 
			Get
				If _Rowid_W Is Nothing Then
					_Rowid_W = TearOff.Rowid
				End If
				Return _Rowid_W
			End Get
		End Property

		Public ReadOnly Property LeaveEarnID() As WhereParameter 
			Get
				If _LeaveEarnID_W Is Nothing Then
					_LeaveEarnID_W = TearOff.LeaveEarnID
				End If
				Return _LeaveEarnID_W
			End Get
		End Property

		Private _SessionID_W As WhereParameter = Nothing
		Private _Rowid_W As WhereParameter = Nothing
		Private _LeaveEarnID_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_SessionID_W = Nothing
			_Rowid_W = Nothing
			_LeaveEarnID_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property SessionID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.SessionID, Parameters.SessionID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Rowid() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Rowid, Parameters.Rowid)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LeaveEarnID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveEarnID, Parameters.LeaveEarnID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property SessionID() As AggregateParameter 
			Get
				If _SessionID_W Is Nothing Then
					_SessionID_W = TearOff.SessionID
				End If
				Return _SessionID_W
			End Get
		End Property

		Public ReadOnly Property Rowid() As AggregateParameter 
			Get
				If _Rowid_W Is Nothing Then
					_Rowid_W = TearOff.Rowid
				End If
				Return _Rowid_W
			End Get
		End Property

		Public ReadOnly Property LeaveEarnID() As AggregateParameter 
			Get
				If _LeaveEarnID_W Is Nothing Then
					_LeaveEarnID_W = TearOff.LeaveEarnID
				End If
				Return _LeaveEarnID_W
			End Get
		End Property

		Private _SessionID_W As AggregateParameter = Nothing
		Private _Rowid_W As AggregateParameter = Nothing
		Private _LeaveEarnID_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_SessionID_W = Nothing
		_Rowid_W = Nothing
		_LeaveEarnID_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveEarnTmp_delInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.Rowid.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveEarnTmp_delUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveEarnTmp_delDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.SessionID)
		p.SourceColumn = ColumnNames.SessionID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Rowid)
		p.SourceColumn = ColumnNames.Rowid
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.SessionID)
		p.SourceColumn = ColumnNames.SessionID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Rowid)
		p.SourceColumn = ColumnNames.Rowid
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LeaveEarnID)
		p.SourceColumn = ColumnNames.LeaveEarnID
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

