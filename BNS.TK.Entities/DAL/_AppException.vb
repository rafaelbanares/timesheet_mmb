
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _AppException
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "AppException"
            Me.MappingName = "AppException"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_AppExceptionLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_AppException.Parameters.ID, ID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_AppExceptionLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property ID As SqlParameter
                Get
                    Return New SqlParameter("@ID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Exception As SqlParameter
                Get
                    Return New SqlParameter("@Exception", SqlDbType.VarChar, 1000)
                End Get
            End Property

            Public Shared ReadOnly Property CreateDate As SqlParameter
                Get
                    Return New SqlParameter("@CreateDate", SqlDbType.DateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const ID As String = "ID"
            Public Const Exception As String = "Exception"
            Public Const CreateDate As String = "CreateDate"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _AppException.PropertyNames.ID
                    ht(Exception) = _AppException.PropertyNames.Exception
                    ht(CreateDate) = _AppException.PropertyNames.CreateDate

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const ID As String = "ID"
            Public Const Exception As String = "Exception"
            Public Const CreateDate As String = "CreateDate"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _AppException.ColumnNames.ID
                    ht(Exception) = _AppException.ColumnNames.Exception
                    ht(CreateDate) = _AppException.ColumnNames.CreateDate

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const ID As String = "s_ID"
            Public Const Exception As String = "s_Exception"
            Public Const CreateDate As String = "s_CreateDate"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property ID As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.ID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.ID, Value)
            End Set
        End Property

        Public Overridable Property Exception As String
            Get
                Return MyBase.GetString(ColumnNames.Exception)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Exception, Value)
            End Set
        End Property

        Public Overridable Property CreateDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreateDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreateDate, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_ID As String
            Get
                If Me.IsColumnNull(ColumnNames.ID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.ID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ID)
                Else
                    Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Exception As String
            Get
                If Me.IsColumnNull(ColumnNames.Exception) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Exception)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Exception)
                Else
                    Me.Exception = MyBase.SetStringAsString(ColumnNames.Exception, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreateDate As String
            Get
                If Me.IsColumnNull(ColumnNames.CreateDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreateDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreateDate)
                Else
                    Me.CreateDate = MyBase.SetDateTimeAsString(ColumnNames.CreateDate, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Exception() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Exception, Parameters.Exception)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreateDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreateDate, Parameters.CreateDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property ID() As WhereParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property Exception() As WhereParameter
                Get
                    If _Exception_W Is Nothing Then
                        _Exception_W = TearOff.Exception
                    End If
                    Return _Exception_W
                End Get
            End Property

            Public ReadOnly Property CreateDate() As WhereParameter
                Get
                    If _CreateDate_W Is Nothing Then
                        _CreateDate_W = TearOff.CreateDate
                    End If
                    Return _CreateDate_W
                End Get
            End Property

            Private _ID_W As WhereParameter = Nothing
            Private _Exception_W As WhereParameter = Nothing
            Private _CreateDate_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _ID_W = Nothing
                _Exception_W = Nothing
                _CreateDate_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Exception() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Exception, Parameters.Exception)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreateDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreateDate, Parameters.CreateDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property ID() As AggregateParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property Exception() As AggregateParameter
                Get
                    If _Exception_W Is Nothing Then
                        _Exception_W = TearOff.Exception
                    End If
                    Return _Exception_W
                End Get
            End Property

            Public ReadOnly Property CreateDate() As AggregateParameter
                Get
                    If _CreateDate_W Is Nothing Then
                        _CreateDate_W = TearOff.CreateDate
                    End If
                    Return _CreateDate_W
                End Get
            End Property

            Private _ID_W As AggregateParameter = Nothing
            Private _Exception_W As AggregateParameter = Nothing
            Private _CreateDate_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _ID_W = Nothing
                _Exception_W = Nothing
                _CreateDate_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_AppExceptionInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.ID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_AppExceptionUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_AppExceptionDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Exception)
            p.SourceColumn = ColumnNames.Exception
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreateDate)
            p.SourceColumn = ColumnNames.CreateDate
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

