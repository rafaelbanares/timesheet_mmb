
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _Attendance
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "Attendance"
            Me.MappingName = "Attendance"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_AttendanceLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal EmployeeID As String, ByVal Date1 As DateTime) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_Attendance.Parameters.CompanyID, CompanyID)
            parameters.Add(_Attendance.Parameters.EmployeeID, EmployeeID)
            parameters.Add(_Attendance.Parameters.Date1, Date1)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_AttendanceLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property EmployeeID As SqlParameter
                Get
                    Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Date1 As SqlParameter
                Get
                    Return New SqlParameter("@Date", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Shiftcode As SqlParameter
                Get
                    Return New SqlParameter("@Shiftcode", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Restdaycode As SqlParameter
                Get
                    Return New SqlParameter("@Restdaycode", SqlDbType.VarChar, 7)
                End Get
            End Property

            Public Shared ReadOnly Property In1 As SqlParameter
                Get
                    Return New SqlParameter("@In1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Out1 As SqlParameter
                Get
                    Return New SqlParameter("@Out1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property In2 As SqlParameter
                Get
                    Return New SqlParameter("@In2", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Out2 As SqlParameter
                Get
                    Return New SqlParameter("@Out2", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Late As SqlParameter
                Get
                    Return New SqlParameter("@Late", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property UT As SqlParameter
                Get
                    Return New SqlParameter("@UT", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Absent As SqlParameter
                Get
                    Return New SqlParameter("@Absent", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Leave As SqlParameter
                Get
                    Return New SqlParameter("@Leave", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Excesshrs As SqlParameter
                Get
                    Return New SqlParameter("@Excesshrs", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Remarks As SqlParameter
                Get
                    Return New SqlParameter("@Remarks", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property ErrorDesc As SqlParameter
                Get
                    Return New SqlParameter("@ErrorDesc", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property Supervisor As SqlParameter
                Get
                    Return New SqlParameter("@Supervisor", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property OTcode As SqlParameter
                Get
                    Return New SqlParameter("@OTcode", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property Valid As SqlParameter
                Get
                    Return New SqlParameter("@Valid", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property ValidOTb4Shift As SqlParameter
                Get
                    Return New SqlParameter("@ValidOTb4Shift", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property First8 As SqlParameter
                Get
                    Return New SqlParameter("@First8", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Greater8 As SqlParameter
                Get
                    Return New SqlParameter("@Greater8", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property NDot1 As SqlParameter
                Get
                    Return New SqlParameter("@NDot1", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property NDot2 As SqlParameter
                Get
                    Return New SqlParameter("@NDot2", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Reghrs As SqlParameter
                Get
                    Return New SqlParameter("@Reghrs", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Regnd1 As SqlParameter
                Get
                    Return New SqlParameter("@Regnd1", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Regnd2 As SqlParameter
                Get
                    Return New SqlParameter("@Regnd2", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property ExcusedLate As SqlParameter
                Get
                    Return New SqlParameter("@ExcusedLate", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property ExcusedUT As SqlParameter
                Get
                    Return New SqlParameter("@ExcusedUT", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Leavecode As SqlParameter
                Get
                    Return New SqlParameter("@Leavecode", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property OTAuthStart As SqlParameter
                Get
                    Return New SqlParameter("@OTAuthStart", SqlDbType.DateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property OTAuthEnd As SqlParameter
                Get
                    Return New SqlParameter("@OTAuthEnd", SqlDbType.DateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Edited As SqlParameter
                Get
                    Return New SqlParameter("@Edited", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Posted As SqlParameter
                Get
                    Return New SqlParameter("@Posted", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Rendered As SqlParameter
                Get
                    Return New SqlParameter("@Rendered", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property OTNoPremium As SqlParameter
                Get
                    Return New SqlParameter("@OTNoPremium", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Processed As SqlParameter
                Get
                    Return New SqlParameter("@Processed", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property ReOpen As SqlParameter
                Get
                    Return New SqlParameter("@ReOpen", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Orig_in1 As SqlParameter
                Get
                    Return New SqlParameter("@Orig_in1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Orig_out1 As SqlParameter
                Get
                    Return New SqlParameter("@Orig_out1", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Latemins As SqlParameter
                Get
                    Return New SqlParameter("@Latemins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property UTmins As SqlParameter
                Get
                    Return New SqlParameter("@UTmins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Excessmins As SqlParameter
                Get
                    Return New SqlParameter("@Excessmins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property First8mins As SqlParameter
                Get
                    Return New SqlParameter("@First8mins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Greater8mins As SqlParameter
                Get
                    Return New SqlParameter("@Greater8mins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property NDot1mins As SqlParameter
                Get
                    Return New SqlParameter("@NDot1mins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property NDot2mins As SqlParameter
                Get
                    Return New SqlParameter("@NDot2mins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Regmins As SqlParameter
                Get
                    Return New SqlParameter("@Regmins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Regnd1mins As SqlParameter
                Get
                    Return New SqlParameter("@Regnd1mins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Regnd2mins As SqlParameter
                Get
                    Return New SqlParameter("@Regnd2mins", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property EarlyWork As SqlParameter
                Get
                    Return New SqlParameter("@EarlyWork", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Status As SqlParameter
                Get
                    Return New SqlParameter("@Status", SqlDbType.VarChar, 12)
                End Get
            End Property

            Public Shared ReadOnly Property Stage As SqlParameter
                Get
                    Return New SqlParameter("@Stage", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Reason As SqlParameter
                Get
                    Return New SqlParameter("@Reason", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property DeclinedReason As SqlParameter
                Get
                    Return New SqlParameter("@DeclinedReason", SqlDbType.VarChar, 100)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const Date1 As String = "Date"
            Public Const Shiftcode As String = "shiftcode"
            Public Const Restdaycode As String = "restdaycode"
            Public Const In1 As String = "in1"
            Public Const Out1 As String = "out1"
            Public Const In2 As String = "in2"
            Public Const Out2 As String = "out2"
            Public Const Late As String = "Late"
            Public Const UT As String = "UT"
            Public Const Absent As String = "Absent"
            Public Const Leave As String = "Leave"
            Public Const Excesshrs As String = "Excesshrs"
            Public Const Remarks As String = "Remarks"
            Public Const ErrorDesc As String = "ErrorDesc"
            Public Const Supervisor As String = "Supervisor"
            Public Const OTcode As String = "OTcode"
            Public Const Valid As String = "Valid"
            Public Const ValidOTb4Shift As String = "ValidOTb4Shift"
            Public Const First8 As String = "First8"
            Public Const Greater8 As String = "Greater8"
            Public Const NDot1 As String = "NDot1"
            Public Const NDot2 As String = "NDot2"
            Public Const Reghrs As String = "Reghrs"
            Public Const Regnd1 As String = "Regnd1"
            Public Const Regnd2 As String = "Regnd2"
            Public Const ExcusedLate As String = "ExcusedLate"
            Public Const ExcusedUT As String = "ExcusedUT"
            Public Const Leavecode As String = "Leavecode"
            Public Const OTAuthStart As String = "OTAuthStart"
            Public Const OTAuthEnd As String = "OTAuthEnd"
            Public Const Edited As String = "Edited"
            Public Const Posted As String = "Posted"
            Public Const Rendered As String = "Rendered"
            Public Const OTNoPremium As String = "OTNoPremium"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const Processed As String = "Processed"
            Public Const ReOpen As String = "ReOpen"
            Public Const Orig_in1 As String = "orig_in1"
            Public Const Orig_out1 As String = "orig_out1"
            Public Const Latemins As String = "Latemins"
            Public Const UTmins As String = "UTmins"
            Public Const Excessmins As String = "Excessmins"
            Public Const First8mins As String = "First8mins"
            Public Const Greater8mins As String = "Greater8mins"
            Public Const NDot1mins As String = "NDot1mins"
            Public Const NDot2mins As String = "NDot2mins"
            Public Const Regmins As String = "Regmins"
            Public Const Regnd1mins As String = "Regnd1mins"
            Public Const Regnd2mins As String = "Regnd2mins"
            Public Const EarlyWork As String = "EarlyWork"
            Public Const Status As String = "Status"
            Public Const Stage As String = "Stage"
            Public Const Reason As String = "Reason"
            Public Const DeclinedReason As String = "DeclinedReason"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Attendance.PropertyNames.CompanyID
                    ht(EmployeeID) = _Attendance.PropertyNames.EmployeeID
                    ht(Date1) = _Attendance.PropertyNames.Date1
                    ht(Shiftcode) = _Attendance.PropertyNames.Shiftcode
                    ht(Restdaycode) = _Attendance.PropertyNames.Restdaycode
                    ht(In1) = _Attendance.PropertyNames.In1
                    ht(Out1) = _Attendance.PropertyNames.Out1
                    ht(In2) = _Attendance.PropertyNames.In2
                    ht(Out2) = _Attendance.PropertyNames.Out2
                    ht(Late) = _Attendance.PropertyNames.Late
                    ht(UT) = _Attendance.PropertyNames.UT
                    ht(Absent) = _Attendance.PropertyNames.Absent
                    ht(Leave) = _Attendance.PropertyNames.Leave
                    ht(Excesshrs) = _Attendance.PropertyNames.Excesshrs
                    ht(Remarks) = _Attendance.PropertyNames.Remarks
                    ht(ErrorDesc) = _Attendance.PropertyNames.ErrorDesc
                    ht(Supervisor) = _Attendance.PropertyNames.Supervisor
                    ht(OTcode) = _Attendance.PropertyNames.OTcode
                    ht(Valid) = _Attendance.PropertyNames.Valid
                    ht(ValidOTb4Shift) = _Attendance.PropertyNames.ValidOTb4Shift
                    ht(First8) = _Attendance.PropertyNames.First8
                    ht(Greater8) = _Attendance.PropertyNames.Greater8
                    ht(NDot1) = _Attendance.PropertyNames.NDot1
                    ht(NDot2) = _Attendance.PropertyNames.NDot2
                    ht(Reghrs) = _Attendance.PropertyNames.Reghrs
                    ht(Regnd1) = _Attendance.PropertyNames.Regnd1
                    ht(Regnd2) = _Attendance.PropertyNames.Regnd2
                    ht(ExcusedLate) = _Attendance.PropertyNames.ExcusedLate
                    ht(ExcusedUT) = _Attendance.PropertyNames.ExcusedUT
                    ht(Leavecode) = _Attendance.PropertyNames.Leavecode
                    ht(OTAuthStart) = _Attendance.PropertyNames.OTAuthStart
                    ht(OTAuthEnd) = _Attendance.PropertyNames.OTAuthEnd
                    ht(Edited) = _Attendance.PropertyNames.Edited
                    ht(Posted) = _Attendance.PropertyNames.Posted
                    ht(Rendered) = _Attendance.PropertyNames.Rendered
                    ht(OTNoPremium) = _Attendance.PropertyNames.OTNoPremium
                    ht(LastUpdBy) = _Attendance.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _Attendance.PropertyNames.LastUpdDate
                    ht(Processed) = _Attendance.PropertyNames.Processed
                    ht(ReOpen) = _Attendance.PropertyNames.ReOpen
                    ht(Orig_in1) = _Attendance.PropertyNames.Orig_in1
                    ht(Orig_out1) = _Attendance.PropertyNames.Orig_out1
                    ht(Latemins) = _Attendance.PropertyNames.Latemins
                    ht(UTmins) = _Attendance.PropertyNames.UTmins
                    ht(Excessmins) = _Attendance.PropertyNames.Excessmins
                    ht(First8mins) = _Attendance.PropertyNames.First8mins
                    ht(Greater8mins) = _Attendance.PropertyNames.Greater8mins
                    ht(NDot1mins) = _Attendance.PropertyNames.NDot1mins
                    ht(NDot2mins) = _Attendance.PropertyNames.NDot2mins
                    ht(Regmins) = _Attendance.PropertyNames.Regmins
                    ht(Regnd1mins) = _Attendance.PropertyNames.Regnd1mins
                    ht(Regnd2mins) = _Attendance.PropertyNames.Regnd2mins
                    ht(EarlyWork) = _Attendance.PropertyNames.EarlyWork
                    ht(Status) = _Attendance.PropertyNames.Status
                    ht(Stage) = _Attendance.PropertyNames.Stage
                    ht(Reason) = _Attendance.PropertyNames.Reason
                    ht(DeclinedReason) = _Attendance.PropertyNames.DeclinedReason

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const Date1 As String = "Date1"
            Public Const Shiftcode As String = "Shiftcode"
            Public Const Restdaycode As String = "Restdaycode"
            Public Const In1 As String = "In1"
            Public Const Out1 As String = "Out1"
            Public Const In2 As String = "In2"
            Public Const Out2 As String = "Out2"
            Public Const Late As String = "Late"
            Public Const UT As String = "UT"
            Public Const Absent As String = "Absent"
            Public Const Leave As String = "Leave"
            Public Const Excesshrs As String = "Excesshrs"
            Public Const Remarks As String = "Remarks"
            Public Const ErrorDesc As String = "ErrorDesc"
            Public Const Supervisor As String = "Supervisor"
            Public Const OTcode As String = "OTcode"
            Public Const Valid As String = "Valid"
            Public Const ValidOTb4Shift As String = "ValidOTb4Shift"
            Public Const First8 As String = "First8"
            Public Const Greater8 As String = "Greater8"
            Public Const NDot1 As String = "NDot1"
            Public Const NDot2 As String = "NDot2"
            Public Const Reghrs As String = "Reghrs"
            Public Const Regnd1 As String = "Regnd1"
            Public Const Regnd2 As String = "Regnd2"
            Public Const ExcusedLate As String = "ExcusedLate"
            Public Const ExcusedUT As String = "ExcusedUT"
            Public Const Leavecode As String = "Leavecode"
            Public Const OTAuthStart As String = "OTAuthStart"
            Public Const OTAuthEnd As String = "OTAuthEnd"
            Public Const Edited As String = "Edited"
            Public Const Posted As String = "Posted"
            Public Const Rendered As String = "Rendered"
            Public Const OTNoPremium As String = "OTNoPremium"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const Processed As String = "Processed"
            Public Const ReOpen As String = "ReOpen"
            Public Const Orig_in1 As String = "Orig_in1"
            Public Const Orig_out1 As String = "Orig_out1"
            Public Const Latemins As String = "Latemins"
            Public Const UTmins As String = "UTmins"
            Public Const Excessmins As String = "Excessmins"
            Public Const First8mins As String = "First8mins"
            Public Const Greater8mins As String = "Greater8mins"
            Public Const NDot1mins As String = "NDot1mins"
            Public Const NDot2mins As String = "NDot2mins"
            Public Const Regmins As String = "Regmins"
            Public Const Regnd1mins As String = "Regnd1mins"
            Public Const Regnd2mins As String = "Regnd2mins"
            Public Const EarlyWork As String = "EarlyWork"
            Public Const Status As String = "Status"
            Public Const Stage As String = "Stage"
            Public Const Reason As String = "Reason"
            Public Const DeclinedReason As String = "DeclinedReason"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Attendance.ColumnNames.CompanyID
                    ht(EmployeeID) = _Attendance.ColumnNames.EmployeeID
                    ht(Date1) = _Attendance.ColumnNames.Date1
                    ht(Shiftcode) = _Attendance.ColumnNames.Shiftcode
                    ht(Restdaycode) = _Attendance.ColumnNames.Restdaycode
                    ht(In1) = _Attendance.ColumnNames.In1
                    ht(Out1) = _Attendance.ColumnNames.Out1
                    ht(In2) = _Attendance.ColumnNames.In2
                    ht(Out2) = _Attendance.ColumnNames.Out2
                    ht(Late) = _Attendance.ColumnNames.Late
                    ht(UT) = _Attendance.ColumnNames.UT
                    ht(Absent) = _Attendance.ColumnNames.Absent
                    ht(Leave) = _Attendance.ColumnNames.Leave
                    ht(Excesshrs) = _Attendance.ColumnNames.Excesshrs
                    ht(Remarks) = _Attendance.ColumnNames.Remarks
                    ht(ErrorDesc) = _Attendance.ColumnNames.ErrorDesc
                    ht(Supervisor) = _Attendance.ColumnNames.Supervisor
                    ht(OTcode) = _Attendance.ColumnNames.OTcode
                    ht(Valid) = _Attendance.ColumnNames.Valid
                    ht(ValidOTb4Shift) = _Attendance.ColumnNames.ValidOTb4Shift
                    ht(First8) = _Attendance.ColumnNames.First8
                    ht(Greater8) = _Attendance.ColumnNames.Greater8
                    ht(NDot1) = _Attendance.ColumnNames.NDot1
                    ht(NDot2) = _Attendance.ColumnNames.NDot2
                    ht(Reghrs) = _Attendance.ColumnNames.Reghrs
                    ht(Regnd1) = _Attendance.ColumnNames.Regnd1
                    ht(Regnd2) = _Attendance.ColumnNames.Regnd2
                    ht(ExcusedLate) = _Attendance.ColumnNames.ExcusedLate
                    ht(ExcusedUT) = _Attendance.ColumnNames.ExcusedUT
                    ht(Leavecode) = _Attendance.ColumnNames.Leavecode
                    ht(OTAuthStart) = _Attendance.ColumnNames.OTAuthStart
                    ht(OTAuthEnd) = _Attendance.ColumnNames.OTAuthEnd
                    ht(Edited) = _Attendance.ColumnNames.Edited
                    ht(Posted) = _Attendance.ColumnNames.Posted
                    ht(Rendered) = _Attendance.ColumnNames.Rendered
                    ht(OTNoPremium) = _Attendance.ColumnNames.OTNoPremium
                    ht(LastUpdBy) = _Attendance.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _Attendance.ColumnNames.LastUpdDate
                    ht(Processed) = _Attendance.ColumnNames.Processed
                    ht(ReOpen) = _Attendance.ColumnNames.ReOpen
                    ht(Orig_in1) = _Attendance.ColumnNames.Orig_in1
                    ht(Orig_out1) = _Attendance.ColumnNames.Orig_out1
                    ht(Latemins) = _Attendance.ColumnNames.Latemins
                    ht(UTmins) = _Attendance.ColumnNames.UTmins
                    ht(Excessmins) = _Attendance.ColumnNames.Excessmins
                    ht(First8mins) = _Attendance.ColumnNames.First8mins
                    ht(Greater8mins) = _Attendance.ColumnNames.Greater8mins
                    ht(NDot1mins) = _Attendance.ColumnNames.NDot1mins
                    ht(NDot2mins) = _Attendance.ColumnNames.NDot2mins
                    ht(Regmins) = _Attendance.ColumnNames.Regmins
                    ht(Regnd1mins) = _Attendance.ColumnNames.Regnd1mins
                    ht(Regnd2mins) = _Attendance.ColumnNames.Regnd2mins
                    ht(EarlyWork) = _Attendance.ColumnNames.EarlyWork
                    ht(Status) = _Attendance.ColumnNames.Status
                    ht(Stage) = _Attendance.ColumnNames.Stage
                    ht(Reason) = _Attendance.ColumnNames.Reason
                    ht(DeclinedReason) = _Attendance.ColumnNames.DeclinedReason

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const CompanyID As String = "s_CompanyID"
            Public Const EmployeeID As String = "s_EmployeeID"
            Public Const Date1 As String = "s_Date1"
            Public Const Shiftcode As String = "s_Shiftcode"
            Public Const Restdaycode As String = "s_Restdaycode"
            Public Const In1 As String = "s_In1"
            Public Const Out1 As String = "s_Out1"
            Public Const In2 As String = "s_In2"
            Public Const Out2 As String = "s_Out2"
            Public Const Late As String = "s_Late"
            Public Const UT As String = "s_UT"
            Public Const Absent As String = "s_Absent"
            Public Const Leave As String = "s_Leave"
            Public Const Excesshrs As String = "s_Excesshrs"
            Public Const Remarks As String = "s_Remarks"
            Public Const ErrorDesc As String = "s_ErrorDesc"
            Public Const Supervisor As String = "s_Supervisor"
            Public Const OTcode As String = "s_OTcode"
            Public Const Valid As String = "s_Valid"
            Public Const ValidOTb4Shift As String = "s_ValidOTb4Shift"
            Public Const First8 As String = "s_First8"
            Public Const Greater8 As String = "s_Greater8"
            Public Const NDot1 As String = "s_NDot1"
            Public Const NDot2 As String = "s_NDot2"
            Public Const Reghrs As String = "s_Reghrs"
            Public Const Regnd1 As String = "s_Regnd1"
            Public Const Regnd2 As String = "s_Regnd2"
            Public Const ExcusedLate As String = "s_ExcusedLate"
            Public Const ExcusedUT As String = "s_ExcusedUT"
            Public Const Leavecode As String = "s_Leavecode"
            Public Const OTAuthStart As String = "s_OTAuthStart"
            Public Const OTAuthEnd As String = "s_OTAuthEnd"
            Public Const Edited As String = "s_Edited"
            Public Const Posted As String = "s_Posted"
            Public Const Rendered As String = "s_Rendered"
            Public Const OTNoPremium As String = "s_OTNoPremium"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"
            Public Const Processed As String = "s_Processed"
            Public Const ReOpen As String = "s_ReOpen"
            Public Const Orig_in1 As String = "s_Orig_in1"
            Public Const Orig_out1 As String = "s_Orig_out1"
            Public Const Latemins As String = "s_Latemins"
            Public Const UTmins As String = "s_UTmins"
            Public Const Excessmins As String = "s_Excessmins"
            Public Const First8mins As String = "s_First8mins"
            Public Const Greater8mins As String = "s_Greater8mins"
            Public Const NDot1mins As String = "s_NDot1mins"
            Public Const NDot2mins As String = "s_NDot2mins"
            Public Const Regmins As String = "s_Regmins"
            Public Const Regnd1mins As String = "s_Regnd1mins"
            Public Const Regnd2mins As String = "s_Regnd2mins"
            Public Const EarlyWork As String = "s_EarlyWork"
            Public Const Status As String = "s_Status"
            Public Const Stage As String = "s_Stage"
            Public Const Reason As String = "s_Reason"
            Public Const DeclinedReason As String = "s_DeclinedReason"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property EmployeeID As String
            Get
                Return MyBase.GetString(ColumnNames.EmployeeID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.EmployeeID, Value)
            End Set
        End Property

        Public Overridable Property Date1 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Date1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Date1, Value)
            End Set
        End Property

        Public Overridable Property Shiftcode As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Shiftcode)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Shiftcode, Value)
            End Set
        End Property

        Public Overridable Property Restdaycode As String
            Get
                Return MyBase.GetString(ColumnNames.Restdaycode)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Restdaycode, Value)
            End Set
        End Property

        Public Overridable Property In1 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.In1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.In1, Value)
            End Set
        End Property

        Public Overridable Property Out1 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Out1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Out1, Value)
            End Set
        End Property

        Public Overridable Property In2 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.In2)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.In2, Value)
            End Set
        End Property

        Public Overridable Property Out2 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Out2)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Out2, Value)
            End Set
        End Property

        Public Overridable Property Late As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Late)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Late, Value)
            End Set
        End Property

        Public Overridable Property UT As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.UT)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.UT, Value)
            End Set
        End Property

        Public Overridable Property Absent As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Absent)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Absent, Value)
            End Set
        End Property

        Public Overridable Property Leave As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Leave)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Leave, Value)
            End Set
        End Property

        Public Overridable Property Excesshrs As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Excesshrs)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Excesshrs, Value)
            End Set
        End Property

        Public Overridable Property Remarks As String
            Get
                Return MyBase.GetString(ColumnNames.Remarks)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Remarks, Value)
            End Set
        End Property

        Public Overridable Property ErrorDesc As String
            Get
                Return MyBase.GetString(ColumnNames.ErrorDesc)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.ErrorDesc, Value)
            End Set
        End Property

        Public Overridable Property Supervisor As String
            Get
                Return MyBase.GetString(ColumnNames.Supervisor)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Supervisor, Value)
            End Set
        End Property

        Public Overridable Property OTcode As String
            Get
                Return MyBase.GetString(ColumnNames.OTcode)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.OTcode, Value)
            End Set
        End Property

        Public Overridable Property Valid As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Valid)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Valid, Value)
            End Set
        End Property

        Public Overridable Property ValidOTb4Shift As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.ValidOTb4Shift)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.ValidOTb4Shift, Value)
            End Set
        End Property

        Public Overridable Property First8 As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.First8)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.First8, Value)
            End Set
        End Property

        Public Overridable Property Greater8 As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Greater8)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Greater8, Value)
            End Set
        End Property

        Public Overridable Property NDot1 As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.NDot1)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.NDot1, Value)
            End Set
        End Property

        Public Overridable Property NDot2 As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.NDot2)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.NDot2, Value)
            End Set
        End Property

        Public Overridable Property Reghrs As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Reghrs)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Reghrs, Value)
            End Set
        End Property

        Public Overridable Property Regnd1 As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Regnd1)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Regnd1, Value)
            End Set
        End Property

        Public Overridable Property Regnd2 As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Regnd2)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Regnd2, Value)
            End Set
        End Property

        Public Overridable Property ExcusedLate As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.ExcusedLate)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.ExcusedLate, Value)
            End Set
        End Property

        Public Overridable Property ExcusedUT As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.ExcusedUT)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.ExcusedUT, Value)
            End Set
        End Property

        Public Overridable Property Leavecode As String
            Get
                Return MyBase.GetString(ColumnNames.Leavecode)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Leavecode, Value)
            End Set
        End Property

        Public Overridable Property OTAuthStart As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.OTAuthStart)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.OTAuthStart, Value)
            End Set
        End Property

        Public Overridable Property OTAuthEnd As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.OTAuthEnd)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.OTAuthEnd, Value)
            End Set
        End Property

        Public Overridable Property Edited As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Edited)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Edited, Value)
            End Set
        End Property

        Public Overridable Property Posted As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Posted)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Posted, Value)
            End Set
        End Property

        Public Overridable Property Rendered As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.Rendered)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.Rendered, Value)
            End Set
        End Property

        Public Overridable Property OTNoPremium As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.OTNoPremium)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.OTNoPremium, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property

        Public Overridable Property Processed As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Processed)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Processed, Value)
            End Set
        End Property

        Public Overridable Property ReOpen As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.ReOpen)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.ReOpen, Value)
            End Set
        End Property

        Public Overridable Property Orig_in1 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Orig_in1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Orig_in1, Value)
            End Set
        End Property

        Public Overridable Property Orig_out1 As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Orig_out1)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Orig_out1, Value)
            End Set
        End Property

        Public Overridable Property Latemins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Latemins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Latemins, Value)
            End Set
        End Property

        Public Overridable Property UTmins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.UTmins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.UTmins, Value)
            End Set
        End Property

        Public Overridable Property Excessmins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Excessmins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Excessmins, Value)
            End Set
        End Property

        Public Overridable Property First8mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.First8mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.First8mins, Value)
            End Set
        End Property

        Public Overridable Property Greater8mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Greater8mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Greater8mins, Value)
            End Set
        End Property

        Public Overridable Property NDot1mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.NDot1mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.NDot1mins, Value)
            End Set
        End Property

        Public Overridable Property NDot2mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.NDot2mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.NDot2mins, Value)
            End Set
        End Property

        Public Overridable Property Regmins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Regmins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Regmins, Value)
            End Set
        End Property

        Public Overridable Property Regnd1mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Regnd1mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Regnd1mins, Value)
            End Set
        End Property

        Public Overridable Property Regnd2mins As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Regnd2mins)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Regnd2mins, Value)
            End Set
        End Property

        Public Overridable Property EarlyWork As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.EarlyWork)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.EarlyWork, Value)
            End Set
        End Property

        Public Overridable Property Status As String
            Get
                Return MyBase.GetString(ColumnNames.Status)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Status, Value)
            End Set
        End Property

        Public Overridable Property Stage As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Stage)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Stage, Value)
            End Set
        End Property

        Public Overridable Property Reason As String
            Get
                Return MyBase.GetString(ColumnNames.Reason)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Reason, Value)
            End Set
        End Property

        Public Overridable Property DeclinedReason As String
            Get
                Return MyBase.GetString(ColumnNames.DeclinedReason)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.DeclinedReason, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EmployeeID As String
            Get
                If Me.IsColumnNull(ColumnNames.EmployeeID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EmployeeID)
                Else
                    Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Date1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Date1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Date1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Date1)
                Else
                    Me.Date1 = MyBase.SetDateTimeAsString(ColumnNames.Date1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Shiftcode As String
            Get
                If Me.IsColumnNull(ColumnNames.Shiftcode) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Shiftcode)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Shiftcode)
                Else
                    Me.Shiftcode = MyBase.SetIntegerAsString(ColumnNames.Shiftcode, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Restdaycode As String
            Get
                If Me.IsColumnNull(ColumnNames.Restdaycode) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Restdaycode)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Restdaycode)
                Else
                    Me.Restdaycode = MyBase.SetStringAsString(ColumnNames.Restdaycode, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_In1 As String
            Get
                If Me.IsColumnNull(ColumnNames.In1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.In1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.In1)
                Else
                    Me.In1 = MyBase.SetDateTimeAsString(ColumnNames.In1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Out1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Out1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Out1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Out1)
                Else
                    Me.Out1 = MyBase.SetDateTimeAsString(ColumnNames.Out1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_In2 As String
            Get
                If Me.IsColumnNull(ColumnNames.In2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.In2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.In2)
                Else
                    Me.In2 = MyBase.SetDateTimeAsString(ColumnNames.In2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Out2 As String
            Get
                If Me.IsColumnNull(ColumnNames.Out2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Out2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Out2)
                Else
                    Me.Out2 = MyBase.SetDateTimeAsString(ColumnNames.Out2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Late As String
            Get
                If Me.IsColumnNull(ColumnNames.Late) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Late)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Late)
                Else
                    Me.Late = MyBase.SetDecimalAsString(ColumnNames.Late, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_UT As String
            Get
                If Me.IsColumnNull(ColumnNames.UT) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.UT)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.UT)
                Else
                    Me.UT = MyBase.SetDecimalAsString(ColumnNames.UT, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Absent As String
            Get
                If Me.IsColumnNull(ColumnNames.Absent) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Absent)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Absent)
                Else
                    Me.Absent = MyBase.SetDecimalAsString(ColumnNames.Absent, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Leave As String
            Get
                If Me.IsColumnNull(ColumnNames.Leave) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Leave)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Leave)
                Else
                    Me.Leave = MyBase.SetDecimalAsString(ColumnNames.Leave, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Excesshrs As String
            Get
                If Me.IsColumnNull(ColumnNames.Excesshrs) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Excesshrs)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Excesshrs)
                Else
                    Me.Excesshrs = MyBase.SetDecimalAsString(ColumnNames.Excesshrs, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Remarks As String
            Get
                If Me.IsColumnNull(ColumnNames.Remarks) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Remarks)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Remarks)
                Else
                    Me.Remarks = MyBase.SetStringAsString(ColumnNames.Remarks, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ErrorDesc As String
            Get
                If Me.IsColumnNull(ColumnNames.ErrorDesc) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.ErrorDesc)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ErrorDesc)
                Else
                    Me.ErrorDesc = MyBase.SetStringAsString(ColumnNames.ErrorDesc, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Supervisor As String
            Get
                If Me.IsColumnNull(ColumnNames.Supervisor) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Supervisor)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Supervisor)
                Else
                    Me.Supervisor = MyBase.SetStringAsString(ColumnNames.Supervisor, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTcode As String
            Get
                If Me.IsColumnNull(ColumnNames.OTcode) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.OTcode)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTcode)
                Else
                    Me.OTcode = MyBase.SetStringAsString(ColumnNames.OTcode, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Valid As String
            Get
                If Me.IsColumnNull(ColumnNames.Valid) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Valid)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Valid)
                Else
                    Me.Valid = MyBase.SetBooleanAsString(ColumnNames.Valid, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ValidOTb4Shift As String
            Get
                If Me.IsColumnNull(ColumnNames.ValidOTb4Shift) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.ValidOTb4Shift)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ValidOTb4Shift)
                Else
                    Me.ValidOTb4Shift = MyBase.SetBooleanAsString(ColumnNames.ValidOTb4Shift, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_First8 As String
            Get
                If Me.IsColumnNull(ColumnNames.First8) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.First8)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.First8)
                Else
                    Me.First8 = MyBase.SetDecimalAsString(ColumnNames.First8, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Greater8 As String
            Get
                If Me.IsColumnNull(ColumnNames.Greater8) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Greater8)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Greater8)
                Else
                    Me.Greater8 = MyBase.SetDecimalAsString(ColumnNames.Greater8, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_NDot1 As String
            Get
                If Me.IsColumnNull(ColumnNames.NDot1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.NDot1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.NDot1)
                Else
                    Me.NDot1 = MyBase.SetDecimalAsString(ColumnNames.NDot1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_NDot2 As String
            Get
                If Me.IsColumnNull(ColumnNames.NDot2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.NDot2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.NDot2)
                Else
                    Me.NDot2 = MyBase.SetDecimalAsString(ColumnNames.NDot2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Reghrs As String
            Get
                If Me.IsColumnNull(ColumnNames.Reghrs) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Reghrs)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Reghrs)
                Else
                    Me.Reghrs = MyBase.SetDecimalAsString(ColumnNames.Reghrs, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Regnd1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Regnd1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Regnd1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Regnd1)
                Else
                    Me.Regnd1 = MyBase.SetDecimalAsString(ColumnNames.Regnd1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Regnd2 As String
            Get
                If Me.IsColumnNull(ColumnNames.Regnd2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Regnd2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Regnd2)
                Else
                    Me.Regnd2 = MyBase.SetDecimalAsString(ColumnNames.Regnd2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ExcusedLate As String
            Get
                If Me.IsColumnNull(ColumnNames.ExcusedLate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.ExcusedLate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ExcusedLate)
                Else
                    Me.ExcusedLate = MyBase.SetDecimalAsString(ColumnNames.ExcusedLate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ExcusedUT As String
            Get
                If Me.IsColumnNull(ColumnNames.ExcusedUT) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.ExcusedUT)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ExcusedUT)
                Else
                    Me.ExcusedUT = MyBase.SetDecimalAsString(ColumnNames.ExcusedUT, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Leavecode As String
            Get
                If Me.IsColumnNull(ColumnNames.Leavecode) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Leavecode)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Leavecode)
                Else
                    Me.Leavecode = MyBase.SetStringAsString(ColumnNames.Leavecode, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTAuthStart As String
            Get
                If Me.IsColumnNull(ColumnNames.OTAuthStart) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.OTAuthStart)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTAuthStart)
                Else
                    Me.OTAuthStart = MyBase.SetDateTimeAsString(ColumnNames.OTAuthStart, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTAuthEnd As String
            Get
                If Me.IsColumnNull(ColumnNames.OTAuthEnd) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.OTAuthEnd)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTAuthEnd)
                Else
                    Me.OTAuthEnd = MyBase.SetDateTimeAsString(ColumnNames.OTAuthEnd, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Edited As String
            Get
                If Me.IsColumnNull(ColumnNames.Edited) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Edited)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Edited)
                Else
                    Me.Edited = MyBase.SetBooleanAsString(ColumnNames.Edited, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Posted As String
            Get
                If Me.IsColumnNull(ColumnNames.Posted) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Posted)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Posted)
                Else
                    Me.Posted = MyBase.SetBooleanAsString(ColumnNames.Posted, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Rendered As String
            Get
                If Me.IsColumnNull(ColumnNames.Rendered) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.Rendered)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Rendered)
                Else
                    Me.Rendered = MyBase.SetDecimalAsString(ColumnNames.Rendered, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTNoPremium As String
            Get
                If Me.IsColumnNull(ColumnNames.OTNoPremium) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.OTNoPremium)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTNoPremium)
                Else
                    Me.OTNoPremium = MyBase.SetDecimalAsString(ColumnNames.OTNoPremium, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Processed As String
            Get
                If Me.IsColumnNull(ColumnNames.Processed) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Processed)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Processed)
                Else
                    Me.Processed = MyBase.SetBooleanAsString(ColumnNames.Processed, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ReOpen As String
            Get
                If Me.IsColumnNull(ColumnNames.ReOpen) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.ReOpen)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ReOpen)
                Else
                    Me.ReOpen = MyBase.SetBooleanAsString(ColumnNames.ReOpen, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Orig_in1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Orig_in1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Orig_in1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Orig_in1)
                Else
                    Me.Orig_in1 = MyBase.SetDateTimeAsString(ColumnNames.Orig_in1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Orig_out1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Orig_out1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Orig_out1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Orig_out1)
                Else
                    Me.Orig_out1 = MyBase.SetDateTimeAsString(ColumnNames.Orig_out1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Latemins As String
            Get
                If Me.IsColumnNull(ColumnNames.Latemins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Latemins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Latemins)
                Else
                    Me.Latemins = MyBase.SetIntegerAsString(ColumnNames.Latemins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_UTmins As String
            Get
                If Me.IsColumnNull(ColumnNames.UTmins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.UTmins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.UTmins)
                Else
                    Me.UTmins = MyBase.SetIntegerAsString(ColumnNames.UTmins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Excessmins As String
            Get
                If Me.IsColumnNull(ColumnNames.Excessmins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Excessmins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Excessmins)
                Else
                    Me.Excessmins = MyBase.SetIntegerAsString(ColumnNames.Excessmins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_First8mins As String
            Get
                If Me.IsColumnNull(ColumnNames.First8mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.First8mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.First8mins)
                Else
                    Me.First8mins = MyBase.SetIntegerAsString(ColumnNames.First8mins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Greater8mins As String
            Get
                If Me.IsColumnNull(ColumnNames.Greater8mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Greater8mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Greater8mins)
                Else
                    Me.Greater8mins = MyBase.SetIntegerAsString(ColumnNames.Greater8mins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_NDot1mins As String
            Get
                If Me.IsColumnNull(ColumnNames.NDot1mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.NDot1mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.NDot1mins)
                Else
                    Me.NDot1mins = MyBase.SetIntegerAsString(ColumnNames.NDot1mins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_NDot2mins As String
            Get
                If Me.IsColumnNull(ColumnNames.NDot2mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.NDot2mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.NDot2mins)
                Else
                    Me.NDot2mins = MyBase.SetIntegerAsString(ColumnNames.NDot2mins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Regmins As String
            Get
                If Me.IsColumnNull(ColumnNames.Regmins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Regmins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Regmins)
                Else
                    Me.Regmins = MyBase.SetIntegerAsString(ColumnNames.Regmins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Regnd1mins As String
            Get
                If Me.IsColumnNull(ColumnNames.Regnd1mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Regnd1mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Regnd1mins)
                Else
                    Me.Regnd1mins = MyBase.SetIntegerAsString(ColumnNames.Regnd1mins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Regnd2mins As String
            Get
                If Me.IsColumnNull(ColumnNames.Regnd2mins) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Regnd2mins)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Regnd2mins)
                Else
                    Me.Regnd2mins = MyBase.SetIntegerAsString(ColumnNames.Regnd2mins, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EarlyWork As String
            Get
                If Me.IsColumnNull(ColumnNames.EarlyWork) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.EarlyWork)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EarlyWork)
                Else
                    Me.EarlyWork = MyBase.SetBooleanAsString(ColumnNames.EarlyWork, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Status As String
            Get
                If Me.IsColumnNull(ColumnNames.Status) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Status)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Status)
                Else
                    Me.Status = MyBase.SetStringAsString(ColumnNames.Status, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Stage As String
            Get
                If Me.IsColumnNull(ColumnNames.Stage) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Stage)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Stage)
                Else
                    Me.Stage = MyBase.SetIntegerAsString(ColumnNames.Stage, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Reason As String
            Get
                If Me.IsColumnNull(ColumnNames.Reason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Reason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Reason)
                Else
                    Me.Reason = MyBase.SetStringAsString(ColumnNames.Reason, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_DeclinedReason As String
            Get
                If Me.IsColumnNull(ColumnNames.DeclinedReason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.DeclinedReason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.DeclinedReason)
                Else
                    Me.DeclinedReason = MyBase.SetStringAsString(ColumnNames.DeclinedReason, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Date1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Date1, Parameters.Date1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Shiftcode() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Shiftcode, Parameters.Shiftcode)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Restdaycode() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Restdaycode, Parameters.Restdaycode)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.In1, Parameters.In1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Out1, Parameters.Out1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.In2, Parameters.In2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Out2, Parameters.Out2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Late() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Late, Parameters.Late)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property UT() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.UT, Parameters.UT)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Absent() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Absent, Parameters.Absent)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Leave() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Leave, Parameters.Leave)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excesshrs() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Excesshrs, Parameters.Excesshrs)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Remarks() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Remarks, Parameters.Remarks)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ErrorDesc() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ErrorDesc, Parameters.ErrorDesc)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Supervisor() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Supervisor, Parameters.Supervisor)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTcode() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTcode, Parameters.OTcode)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Valid() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Valid, Parameters.Valid)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ValidOTb4Shift() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ValidOTb4Shift, Parameters.ValidOTb4Shift)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property First8() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.First8, Parameters.First8)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Greater8() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Greater8, Parameters.Greater8)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.NDot1, Parameters.NDot1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.NDot2, Parameters.NDot2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reghrs() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Reghrs, Parameters.Reghrs)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Regnd1, Parameters.Regnd1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Regnd2, Parameters.Regnd2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ExcusedLate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ExcusedLate, Parameters.ExcusedLate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ExcusedUT() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ExcusedUT, Parameters.ExcusedUT)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Leavecode() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Leavecode, Parameters.Leavecode)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTAuthStart() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTAuthStart, Parameters.OTAuthStart)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTAuthEnd() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTAuthEnd, Parameters.OTAuthEnd)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Edited() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Edited, Parameters.Edited)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Posted() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Posted, Parameters.Posted)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Rendered() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Rendered, Parameters.Rendered)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTNoPremium() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTNoPremium, Parameters.OTNoPremium)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Processed() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Processed, Parameters.Processed)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ReOpen() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ReOpen, Parameters.ReOpen)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_in1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Orig_in1, Parameters.Orig_in1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_out1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Orig_out1, Parameters.Orig_out1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Latemins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Latemins, Parameters.Latemins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property UTmins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.UTmins, Parameters.UTmins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excessmins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Excessmins, Parameters.Excessmins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property First8mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.First8mins, Parameters.First8mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Greater8mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Greater8mins, Parameters.Greater8mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot1mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.NDot1mins, Parameters.NDot1mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot2mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.NDot2mins, Parameters.NDot2mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regmins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Regmins, Parameters.Regmins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd1mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Regnd1mins, Parameters.Regnd1mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd2mins() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Regnd2mins, Parameters.Regnd2mins)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EarlyWork() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EarlyWork, Parameters.EarlyWork)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Reason, Parameters.Reason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As WhereParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property Date1() As WhereParameter
                Get
                    If _Date1_W Is Nothing Then
                        _Date1_W = TearOff.Date1
                    End If
                    Return _Date1_W
                End Get
            End Property

            Public ReadOnly Property Shiftcode() As WhereParameter
                Get
                    If _Shiftcode_W Is Nothing Then
                        _Shiftcode_W = TearOff.Shiftcode
                    End If
                    Return _Shiftcode_W
                End Get
            End Property

            Public ReadOnly Property Restdaycode() As WhereParameter
                Get
                    If _Restdaycode_W Is Nothing Then
                        _Restdaycode_W = TearOff.Restdaycode
                    End If
                    Return _Restdaycode_W
                End Get
            End Property

            Public ReadOnly Property In1() As WhereParameter
                Get
                    If _In1_W Is Nothing Then
                        _In1_W = TearOff.In1
                    End If
                    Return _In1_W
                End Get
            End Property

            Public ReadOnly Property Out1() As WhereParameter
                Get
                    If _Out1_W Is Nothing Then
                        _Out1_W = TearOff.Out1
                    End If
                    Return _Out1_W
                End Get
            End Property

            Public ReadOnly Property In2() As WhereParameter
                Get
                    If _In2_W Is Nothing Then
                        _In2_W = TearOff.In2
                    End If
                    Return _In2_W
                End Get
            End Property

            Public ReadOnly Property Out2() As WhereParameter
                Get
                    If _Out2_W Is Nothing Then
                        _Out2_W = TearOff.Out2
                    End If
                    Return _Out2_W
                End Get
            End Property

            Public ReadOnly Property Late() As WhereParameter
                Get
                    If _Late_W Is Nothing Then
                        _Late_W = TearOff.Late
                    End If
                    Return _Late_W
                End Get
            End Property

            Public ReadOnly Property UT() As WhereParameter
                Get
                    If _UT_W Is Nothing Then
                        _UT_W = TearOff.UT
                    End If
                    Return _UT_W
                End Get
            End Property

            Public ReadOnly Property Absent() As WhereParameter
                Get
                    If _Absent_W Is Nothing Then
                        _Absent_W = TearOff.Absent
                    End If
                    Return _Absent_W
                End Get
            End Property

            Public ReadOnly Property Leave() As WhereParameter
                Get
                    If _Leave_W Is Nothing Then
                        _Leave_W = TearOff.Leave
                    End If
                    Return _Leave_W
                End Get
            End Property

            Public ReadOnly Property Excesshrs() As WhereParameter
                Get
                    If _Excesshrs_W Is Nothing Then
                        _Excesshrs_W = TearOff.Excesshrs
                    End If
                    Return _Excesshrs_W
                End Get
            End Property

            Public ReadOnly Property Remarks() As WhereParameter
                Get
                    If _Remarks_W Is Nothing Then
                        _Remarks_W = TearOff.Remarks
                    End If
                    Return _Remarks_W
                End Get
            End Property

            Public ReadOnly Property ErrorDesc() As WhereParameter
                Get
                    If _ErrorDesc_W Is Nothing Then
                        _ErrorDesc_W = TearOff.ErrorDesc
                    End If
                    Return _ErrorDesc_W
                End Get
            End Property

            Public ReadOnly Property Supervisor() As WhereParameter
                Get
                    If _Supervisor_W Is Nothing Then
                        _Supervisor_W = TearOff.Supervisor
                    End If
                    Return _Supervisor_W
                End Get
            End Property

            Public ReadOnly Property OTcode() As WhereParameter
                Get
                    If _OTcode_W Is Nothing Then
                        _OTcode_W = TearOff.OTcode
                    End If
                    Return _OTcode_W
                End Get
            End Property

            Public ReadOnly Property Valid() As WhereParameter
                Get
                    If _Valid_W Is Nothing Then
                        _Valid_W = TearOff.Valid
                    End If
                    Return _Valid_W
                End Get
            End Property

            Public ReadOnly Property ValidOTb4Shift() As WhereParameter
                Get
                    If _ValidOTb4Shift_W Is Nothing Then
                        _ValidOTb4Shift_W = TearOff.ValidOTb4Shift
                    End If
                    Return _ValidOTb4Shift_W
                End Get
            End Property

            Public ReadOnly Property First8() As WhereParameter
                Get
                    If _First8_W Is Nothing Then
                        _First8_W = TearOff.First8
                    End If
                    Return _First8_W
                End Get
            End Property

            Public ReadOnly Property Greater8() As WhereParameter
                Get
                    If _Greater8_W Is Nothing Then
                        _Greater8_W = TearOff.Greater8
                    End If
                    Return _Greater8_W
                End Get
            End Property

            Public ReadOnly Property NDot1() As WhereParameter
                Get
                    If _NDot1_W Is Nothing Then
                        _NDot1_W = TearOff.NDot1
                    End If
                    Return _NDot1_W
                End Get
            End Property

            Public ReadOnly Property NDot2() As WhereParameter
                Get
                    If _NDot2_W Is Nothing Then
                        _NDot2_W = TearOff.NDot2
                    End If
                    Return _NDot2_W
                End Get
            End Property

            Public ReadOnly Property Reghrs() As WhereParameter
                Get
                    If _Reghrs_W Is Nothing Then
                        _Reghrs_W = TearOff.Reghrs
                    End If
                    Return _Reghrs_W
                End Get
            End Property

            Public ReadOnly Property Regnd1() As WhereParameter
                Get
                    If _Regnd1_W Is Nothing Then
                        _Regnd1_W = TearOff.Regnd1
                    End If
                    Return _Regnd1_W
                End Get
            End Property

            Public ReadOnly Property Regnd2() As WhereParameter
                Get
                    If _Regnd2_W Is Nothing Then
                        _Regnd2_W = TearOff.Regnd2
                    End If
                    Return _Regnd2_W
                End Get
            End Property

            Public ReadOnly Property ExcusedLate() As WhereParameter
                Get
                    If _ExcusedLate_W Is Nothing Then
                        _ExcusedLate_W = TearOff.ExcusedLate
                    End If
                    Return _ExcusedLate_W
                End Get
            End Property

            Public ReadOnly Property ExcusedUT() As WhereParameter
                Get
                    If _ExcusedUT_W Is Nothing Then
                        _ExcusedUT_W = TearOff.ExcusedUT
                    End If
                    Return _ExcusedUT_W
                End Get
            End Property

            Public ReadOnly Property Leavecode() As WhereParameter
                Get
                    If _Leavecode_W Is Nothing Then
                        _Leavecode_W = TearOff.Leavecode
                    End If
                    Return _Leavecode_W
                End Get
            End Property

            Public ReadOnly Property OTAuthStart() As WhereParameter
                Get
                    If _OTAuthStart_W Is Nothing Then
                        _OTAuthStart_W = TearOff.OTAuthStart
                    End If
                    Return _OTAuthStart_W
                End Get
            End Property

            Public ReadOnly Property OTAuthEnd() As WhereParameter
                Get
                    If _OTAuthEnd_W Is Nothing Then
                        _OTAuthEnd_W = TearOff.OTAuthEnd
                    End If
                    Return _OTAuthEnd_W
                End Get
            End Property

            Public ReadOnly Property Edited() As WhereParameter
                Get
                    If _Edited_W Is Nothing Then
                        _Edited_W = TearOff.Edited
                    End If
                    Return _Edited_W
                End Get
            End Property

            Public ReadOnly Property Posted() As WhereParameter
                Get
                    If _Posted_W Is Nothing Then
                        _Posted_W = TearOff.Posted
                    End If
                    Return _Posted_W
                End Get
            End Property

            Public ReadOnly Property Rendered() As WhereParameter
                Get
                    If _Rendered_W Is Nothing Then
                        _Rendered_W = TearOff.Rendered
                    End If
                    Return _Rendered_W
                End Get
            End Property

            Public ReadOnly Property OTNoPremium() As WhereParameter
                Get
                    If _OTNoPremium_W Is Nothing Then
                        _OTNoPremium_W = TearOff.OTNoPremium
                    End If
                    Return _OTNoPremium_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property Processed() As WhereParameter
                Get
                    If _Processed_W Is Nothing Then
                        _Processed_W = TearOff.Processed
                    End If
                    Return _Processed_W
                End Get
            End Property

            Public ReadOnly Property ReOpen() As WhereParameter
                Get
                    If _ReOpen_W Is Nothing Then
                        _ReOpen_W = TearOff.ReOpen
                    End If
                    Return _ReOpen_W
                End Get
            End Property

            Public ReadOnly Property Orig_in1() As WhereParameter
                Get
                    If _Orig_in1_W Is Nothing Then
                        _Orig_in1_W = TearOff.Orig_in1
                    End If
                    Return _Orig_in1_W
                End Get
            End Property

            Public ReadOnly Property Orig_out1() As WhereParameter
                Get
                    If _Orig_out1_W Is Nothing Then
                        _Orig_out1_W = TearOff.Orig_out1
                    End If
                    Return _Orig_out1_W
                End Get
            End Property

            Public ReadOnly Property Latemins() As WhereParameter
                Get
                    If _Latemins_W Is Nothing Then
                        _Latemins_W = TearOff.Latemins
                    End If
                    Return _Latemins_W
                End Get
            End Property

            Public ReadOnly Property UTmins() As WhereParameter
                Get
                    If _UTmins_W Is Nothing Then
                        _UTmins_W = TearOff.UTmins
                    End If
                    Return _UTmins_W
                End Get
            End Property

            Public ReadOnly Property Excessmins() As WhereParameter
                Get
                    If _Excessmins_W Is Nothing Then
                        _Excessmins_W = TearOff.Excessmins
                    End If
                    Return _Excessmins_W
                End Get
            End Property

            Public ReadOnly Property First8mins() As WhereParameter
                Get
                    If _First8mins_W Is Nothing Then
                        _First8mins_W = TearOff.First8mins
                    End If
                    Return _First8mins_W
                End Get
            End Property

            Public ReadOnly Property Greater8mins() As WhereParameter
                Get
                    If _Greater8mins_W Is Nothing Then
                        _Greater8mins_W = TearOff.Greater8mins
                    End If
                    Return _Greater8mins_W
                End Get
            End Property

            Public ReadOnly Property NDot1mins() As WhereParameter
                Get
                    If _NDot1mins_W Is Nothing Then
                        _NDot1mins_W = TearOff.NDot1mins
                    End If
                    Return _NDot1mins_W
                End Get
            End Property

            Public ReadOnly Property NDot2mins() As WhereParameter
                Get
                    If _NDot2mins_W Is Nothing Then
                        _NDot2mins_W = TearOff.NDot2mins
                    End If
                    Return _NDot2mins_W
                End Get
            End Property

            Public ReadOnly Property Regmins() As WhereParameter
                Get
                    If _Regmins_W Is Nothing Then
                        _Regmins_W = TearOff.Regmins
                    End If
                    Return _Regmins_W
                End Get
            End Property

            Public ReadOnly Property Regnd1mins() As WhereParameter
                Get
                    If _Regnd1mins_W Is Nothing Then
                        _Regnd1mins_W = TearOff.Regnd1mins
                    End If
                    Return _Regnd1mins_W
                End Get
            End Property

            Public ReadOnly Property Regnd2mins() As WhereParameter
                Get
                    If _Regnd2mins_W Is Nothing Then
                        _Regnd2mins_W = TearOff.Regnd2mins
                    End If
                    Return _Regnd2mins_W
                End Get
            End Property

            Public ReadOnly Property EarlyWork() As WhereParameter
                Get
                    If _EarlyWork_W Is Nothing Then
                        _EarlyWork_W = TearOff.EarlyWork
                    End If
                    Return _EarlyWork_W
                End Get
            End Property

            Public ReadOnly Property Status() As WhereParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Stage() As WhereParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property Reason() As WhereParameter
                Get
                    If _Reason_W Is Nothing Then
                        _Reason_W = TearOff.Reason
                    End If
                    Return _Reason_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As WhereParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Private _CompanyID_W As WhereParameter = Nothing
            Private _EmployeeID_W As WhereParameter = Nothing
            Private _Date1_W As WhereParameter = Nothing
            Private _Shiftcode_W As WhereParameter = Nothing
            Private _Restdaycode_W As WhereParameter = Nothing
            Private _In1_W As WhereParameter = Nothing
            Private _Out1_W As WhereParameter = Nothing
            Private _In2_W As WhereParameter = Nothing
            Private _Out2_W As WhereParameter = Nothing
            Private _Late_W As WhereParameter = Nothing
            Private _UT_W As WhereParameter = Nothing
            Private _Absent_W As WhereParameter = Nothing
            Private _Leave_W As WhereParameter = Nothing
            Private _Excesshrs_W As WhereParameter = Nothing
            Private _Remarks_W As WhereParameter = Nothing
            Private _ErrorDesc_W As WhereParameter = Nothing
            Private _Supervisor_W As WhereParameter = Nothing
            Private _OTcode_W As WhereParameter = Nothing
            Private _Valid_W As WhereParameter = Nothing
            Private _ValidOTb4Shift_W As WhereParameter = Nothing
            Private _First8_W As WhereParameter = Nothing
            Private _Greater8_W As WhereParameter = Nothing
            Private _NDot1_W As WhereParameter = Nothing
            Private _NDot2_W As WhereParameter = Nothing
            Private _Reghrs_W As WhereParameter = Nothing
            Private _Regnd1_W As WhereParameter = Nothing
            Private _Regnd2_W As WhereParameter = Nothing
            Private _ExcusedLate_W As WhereParameter = Nothing
            Private _ExcusedUT_W As WhereParameter = Nothing
            Private _Leavecode_W As WhereParameter = Nothing
            Private _OTAuthStart_W As WhereParameter = Nothing
            Private _OTAuthEnd_W As WhereParameter = Nothing
            Private _Edited_W As WhereParameter = Nothing
            Private _Posted_W As WhereParameter = Nothing
            Private _Rendered_W As WhereParameter = Nothing
            Private _OTNoPremium_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing
            Private _Processed_W As WhereParameter = Nothing
            Private _ReOpen_W As WhereParameter = Nothing
            Private _Orig_in1_W As WhereParameter = Nothing
            Private _Orig_out1_W As WhereParameter = Nothing
            Private _Latemins_W As WhereParameter = Nothing
            Private _UTmins_W As WhereParameter = Nothing
            Private _Excessmins_W As WhereParameter = Nothing
            Private _First8mins_W As WhereParameter = Nothing
            Private _Greater8mins_W As WhereParameter = Nothing
            Private _NDot1mins_W As WhereParameter = Nothing
            Private _NDot2mins_W As WhereParameter = Nothing
            Private _Regmins_W As WhereParameter = Nothing
            Private _Regnd1mins_W As WhereParameter = Nothing
            Private _Regnd2mins_W As WhereParameter = Nothing
            Private _EarlyWork_W As WhereParameter = Nothing
            Private _Status_W As WhereParameter = Nothing
            Private _Stage_W As WhereParameter = Nothing
            Private _Reason_W As WhereParameter = Nothing
            Private _DeclinedReason_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _Date1_W = Nothing
                _Shiftcode_W = Nothing
                _Restdaycode_W = Nothing
                _In1_W = Nothing
                _Out1_W = Nothing
                _In2_W = Nothing
                _Out2_W = Nothing
                _Late_W = Nothing
                _UT_W = Nothing
                _Absent_W = Nothing
                _Leave_W = Nothing
                _Excesshrs_W = Nothing
                _Remarks_W = Nothing
                _ErrorDesc_W = Nothing
                _Supervisor_W = Nothing
                _OTcode_W = Nothing
                _Valid_W = Nothing
                _ValidOTb4Shift_W = Nothing
                _First8_W = Nothing
                _Greater8_W = Nothing
                _NDot1_W = Nothing
                _NDot2_W = Nothing
                _Reghrs_W = Nothing
                _Regnd1_W = Nothing
                _Regnd2_W = Nothing
                _ExcusedLate_W = Nothing
                _ExcusedUT_W = Nothing
                _Leavecode_W = Nothing
                _OTAuthStart_W = Nothing
                _OTAuthEnd_W = Nothing
                _Edited_W = Nothing
                _Posted_W = Nothing
                _Rendered_W = Nothing
                _OTNoPremium_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _Processed_W = Nothing
                _ReOpen_W = Nothing
                _Orig_in1_W = Nothing
                _Orig_out1_W = Nothing
                _Latemins_W = Nothing
                _UTmins_W = Nothing
                _Excessmins_W = Nothing
                _First8mins_W = Nothing
                _Greater8mins_W = Nothing
                _NDot1mins_W = Nothing
                _NDot2mins_W = Nothing
                _Regmins_W = Nothing
                _Regnd1mins_W = Nothing
                _Regnd2mins_W = Nothing
                _EarlyWork_W = Nothing
                _Status_W = Nothing
                _Stage_W = Nothing
                _Reason_W = Nothing
                _DeclinedReason_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Date1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Date1, Parameters.Date1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Shiftcode() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Shiftcode, Parameters.Shiftcode)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Restdaycode() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Restdaycode, Parameters.Restdaycode)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.In1, Parameters.In1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Out1, Parameters.Out1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.In2, Parameters.In2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Out2, Parameters.Out2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Late() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Late, Parameters.Late)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property UT() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UT, Parameters.UT)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Absent() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Absent, Parameters.Absent)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Leave() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Leave, Parameters.Leave)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excesshrs() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Excesshrs, Parameters.Excesshrs)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Remarks() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Remarks, Parameters.Remarks)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ErrorDesc() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ErrorDesc, Parameters.ErrorDesc)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Supervisor() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Supervisor, Parameters.Supervisor)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTcode() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTcode, Parameters.OTcode)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Valid() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Valid, Parameters.Valid)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ValidOTb4Shift() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ValidOTb4Shift, Parameters.ValidOTb4Shift)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property First8() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.First8, Parameters.First8)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Greater8() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Greater8, Parameters.Greater8)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.NDot1, Parameters.NDot1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.NDot2, Parameters.NDot2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reghrs() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Reghrs, Parameters.Reghrs)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Regnd1, Parameters.Regnd1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Regnd2, Parameters.Regnd2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ExcusedLate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ExcusedLate, Parameters.ExcusedLate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ExcusedUT() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ExcusedUT, Parameters.ExcusedUT)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Leavecode() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Leavecode, Parameters.Leavecode)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTAuthStart() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTAuthStart, Parameters.OTAuthStart)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTAuthEnd() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTAuthEnd, Parameters.OTAuthEnd)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Edited() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Edited, Parameters.Edited)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Posted() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Posted, Parameters.Posted)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Rendered() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Rendered, Parameters.Rendered)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTNoPremium() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTNoPremium, Parameters.OTNoPremium)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Processed() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Processed, Parameters.Processed)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ReOpen() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ReOpen, Parameters.ReOpen)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_in1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Orig_in1, Parameters.Orig_in1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Orig_out1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Orig_out1, Parameters.Orig_out1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Latemins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Latemins, Parameters.Latemins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property UTmins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UTmins, Parameters.UTmins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Excessmins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Excessmins, Parameters.Excessmins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property First8mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.First8mins, Parameters.First8mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Greater8mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Greater8mins, Parameters.Greater8mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot1mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.NDot1mins, Parameters.NDot1mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property NDot2mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.NDot2mins, Parameters.NDot2mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regmins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Regmins, Parameters.Regmins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd1mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Regnd1mins, Parameters.Regnd1mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Regnd2mins() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Regnd2mins, Parameters.Regnd2mins)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EarlyWork() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EarlyWork, Parameters.EarlyWork)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Reason, Parameters.Reason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As AggregateParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property Date1() As AggregateParameter
                Get
                    If _Date1_W Is Nothing Then
                        _Date1_W = TearOff.Date1
                    End If
                    Return _Date1_W
                End Get
            End Property

            Public ReadOnly Property Shiftcode() As AggregateParameter
                Get
                    If _Shiftcode_W Is Nothing Then
                        _Shiftcode_W = TearOff.Shiftcode
                    End If
                    Return _Shiftcode_W
                End Get
            End Property

            Public ReadOnly Property Restdaycode() As AggregateParameter
                Get
                    If _Restdaycode_W Is Nothing Then
                        _Restdaycode_W = TearOff.Restdaycode
                    End If
                    Return _Restdaycode_W
                End Get
            End Property

            Public ReadOnly Property In1() As AggregateParameter
                Get
                    If _In1_W Is Nothing Then
                        _In1_W = TearOff.In1
                    End If
                    Return _In1_W
                End Get
            End Property

            Public ReadOnly Property Out1() As AggregateParameter
                Get
                    If _Out1_W Is Nothing Then
                        _Out1_W = TearOff.Out1
                    End If
                    Return _Out1_W
                End Get
            End Property

            Public ReadOnly Property In2() As AggregateParameter
                Get
                    If _In2_W Is Nothing Then
                        _In2_W = TearOff.In2
                    End If
                    Return _In2_W
                End Get
            End Property

            Public ReadOnly Property Out2() As AggregateParameter
                Get
                    If _Out2_W Is Nothing Then
                        _Out2_W = TearOff.Out2
                    End If
                    Return _Out2_W
                End Get
            End Property

            Public ReadOnly Property Late() As AggregateParameter
                Get
                    If _Late_W Is Nothing Then
                        _Late_W = TearOff.Late
                    End If
                    Return _Late_W
                End Get
            End Property

            Public ReadOnly Property UT() As AggregateParameter
                Get
                    If _UT_W Is Nothing Then
                        _UT_W = TearOff.UT
                    End If
                    Return _UT_W
                End Get
            End Property

            Public ReadOnly Property Absent() As AggregateParameter
                Get
                    If _Absent_W Is Nothing Then
                        _Absent_W = TearOff.Absent
                    End If
                    Return _Absent_W
                End Get
            End Property

            Public ReadOnly Property Leave() As AggregateParameter
                Get
                    If _Leave_W Is Nothing Then
                        _Leave_W = TearOff.Leave
                    End If
                    Return _Leave_W
                End Get
            End Property

            Public ReadOnly Property Excesshrs() As AggregateParameter
                Get
                    If _Excesshrs_W Is Nothing Then
                        _Excesshrs_W = TearOff.Excesshrs
                    End If
                    Return _Excesshrs_W
                End Get
            End Property

            Public ReadOnly Property Remarks() As AggregateParameter
                Get
                    If _Remarks_W Is Nothing Then
                        _Remarks_W = TearOff.Remarks
                    End If
                    Return _Remarks_W
                End Get
            End Property

            Public ReadOnly Property ErrorDesc() As AggregateParameter
                Get
                    If _ErrorDesc_W Is Nothing Then
                        _ErrorDesc_W = TearOff.ErrorDesc
                    End If
                    Return _ErrorDesc_W
                End Get
            End Property

            Public ReadOnly Property Supervisor() As AggregateParameter
                Get
                    If _Supervisor_W Is Nothing Then
                        _Supervisor_W = TearOff.Supervisor
                    End If
                    Return _Supervisor_W
                End Get
            End Property

            Public ReadOnly Property OTcode() As AggregateParameter
                Get
                    If _OTcode_W Is Nothing Then
                        _OTcode_W = TearOff.OTcode
                    End If
                    Return _OTcode_W
                End Get
            End Property

            Public ReadOnly Property Valid() As AggregateParameter
                Get
                    If _Valid_W Is Nothing Then
                        _Valid_W = TearOff.Valid
                    End If
                    Return _Valid_W
                End Get
            End Property

            Public ReadOnly Property ValidOTb4Shift() As AggregateParameter
                Get
                    If _ValidOTb4Shift_W Is Nothing Then
                        _ValidOTb4Shift_W = TearOff.ValidOTb4Shift
                    End If
                    Return _ValidOTb4Shift_W
                End Get
            End Property

            Public ReadOnly Property First8() As AggregateParameter
                Get
                    If _First8_W Is Nothing Then
                        _First8_W = TearOff.First8
                    End If
                    Return _First8_W
                End Get
            End Property

            Public ReadOnly Property Greater8() As AggregateParameter
                Get
                    If _Greater8_W Is Nothing Then
                        _Greater8_W = TearOff.Greater8
                    End If
                    Return _Greater8_W
                End Get
            End Property

            Public ReadOnly Property NDot1() As AggregateParameter
                Get
                    If _NDot1_W Is Nothing Then
                        _NDot1_W = TearOff.NDot1
                    End If
                    Return _NDot1_W
                End Get
            End Property

            Public ReadOnly Property NDot2() As AggregateParameter
                Get
                    If _NDot2_W Is Nothing Then
                        _NDot2_W = TearOff.NDot2
                    End If
                    Return _NDot2_W
                End Get
            End Property

            Public ReadOnly Property Reghrs() As AggregateParameter
                Get
                    If _Reghrs_W Is Nothing Then
                        _Reghrs_W = TearOff.Reghrs
                    End If
                    Return _Reghrs_W
                End Get
            End Property

            Public ReadOnly Property Regnd1() As AggregateParameter
                Get
                    If _Regnd1_W Is Nothing Then
                        _Regnd1_W = TearOff.Regnd1
                    End If
                    Return _Regnd1_W
                End Get
            End Property

            Public ReadOnly Property Regnd2() As AggregateParameter
                Get
                    If _Regnd2_W Is Nothing Then
                        _Regnd2_W = TearOff.Regnd2
                    End If
                    Return _Regnd2_W
                End Get
            End Property

            Public ReadOnly Property ExcusedLate() As AggregateParameter
                Get
                    If _ExcusedLate_W Is Nothing Then
                        _ExcusedLate_W = TearOff.ExcusedLate
                    End If
                    Return _ExcusedLate_W
                End Get
            End Property

            Public ReadOnly Property ExcusedUT() As AggregateParameter
                Get
                    If _ExcusedUT_W Is Nothing Then
                        _ExcusedUT_W = TearOff.ExcusedUT
                    End If
                    Return _ExcusedUT_W
                End Get
            End Property

            Public ReadOnly Property Leavecode() As AggregateParameter
                Get
                    If _Leavecode_W Is Nothing Then
                        _Leavecode_W = TearOff.Leavecode
                    End If
                    Return _Leavecode_W
                End Get
            End Property

            Public ReadOnly Property OTAuthStart() As AggregateParameter
                Get
                    If _OTAuthStart_W Is Nothing Then
                        _OTAuthStart_W = TearOff.OTAuthStart
                    End If
                    Return _OTAuthStart_W
                End Get
            End Property

            Public ReadOnly Property OTAuthEnd() As AggregateParameter
                Get
                    If _OTAuthEnd_W Is Nothing Then
                        _OTAuthEnd_W = TearOff.OTAuthEnd
                    End If
                    Return _OTAuthEnd_W
                End Get
            End Property

            Public ReadOnly Property Edited() As AggregateParameter
                Get
                    If _Edited_W Is Nothing Then
                        _Edited_W = TearOff.Edited
                    End If
                    Return _Edited_W
                End Get
            End Property

            Public ReadOnly Property Posted() As AggregateParameter
                Get
                    If _Posted_W Is Nothing Then
                        _Posted_W = TearOff.Posted
                    End If
                    Return _Posted_W
                End Get
            End Property

            Public ReadOnly Property Rendered() As AggregateParameter
                Get
                    If _Rendered_W Is Nothing Then
                        _Rendered_W = TearOff.Rendered
                    End If
                    Return _Rendered_W
                End Get
            End Property

            Public ReadOnly Property OTNoPremium() As AggregateParameter
                Get
                    If _OTNoPremium_W Is Nothing Then
                        _OTNoPremium_W = TearOff.OTNoPremium
                    End If
                    Return _OTNoPremium_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property Processed() As AggregateParameter
                Get
                    If _Processed_W Is Nothing Then
                        _Processed_W = TearOff.Processed
                    End If
                    Return _Processed_W
                End Get
            End Property

            Public ReadOnly Property ReOpen() As AggregateParameter
                Get
                    If _ReOpen_W Is Nothing Then
                        _ReOpen_W = TearOff.ReOpen
                    End If
                    Return _ReOpen_W
                End Get
            End Property

            Public ReadOnly Property Orig_in1() As AggregateParameter
                Get
                    If _Orig_in1_W Is Nothing Then
                        _Orig_in1_W = TearOff.Orig_in1
                    End If
                    Return _Orig_in1_W
                End Get
            End Property

            Public ReadOnly Property Orig_out1() As AggregateParameter
                Get
                    If _Orig_out1_W Is Nothing Then
                        _Orig_out1_W = TearOff.Orig_out1
                    End If
                    Return _Orig_out1_W
                End Get
            End Property

            Public ReadOnly Property Latemins() As AggregateParameter
                Get
                    If _Latemins_W Is Nothing Then
                        _Latemins_W = TearOff.Latemins
                    End If
                    Return _Latemins_W
                End Get
            End Property

            Public ReadOnly Property UTmins() As AggregateParameter
                Get
                    If _UTmins_W Is Nothing Then
                        _UTmins_W = TearOff.UTmins
                    End If
                    Return _UTmins_W
                End Get
            End Property

            Public ReadOnly Property Excessmins() As AggregateParameter
                Get
                    If _Excessmins_W Is Nothing Then
                        _Excessmins_W = TearOff.Excessmins
                    End If
                    Return _Excessmins_W
                End Get
            End Property

            Public ReadOnly Property First8mins() As AggregateParameter
                Get
                    If _First8mins_W Is Nothing Then
                        _First8mins_W = TearOff.First8mins
                    End If
                    Return _First8mins_W
                End Get
            End Property

            Public ReadOnly Property Greater8mins() As AggregateParameter
                Get
                    If _Greater8mins_W Is Nothing Then
                        _Greater8mins_W = TearOff.Greater8mins
                    End If
                    Return _Greater8mins_W
                End Get
            End Property

            Public ReadOnly Property NDot1mins() As AggregateParameter
                Get
                    If _NDot1mins_W Is Nothing Then
                        _NDot1mins_W = TearOff.NDot1mins
                    End If
                    Return _NDot1mins_W
                End Get
            End Property

            Public ReadOnly Property NDot2mins() As AggregateParameter
                Get
                    If _NDot2mins_W Is Nothing Then
                        _NDot2mins_W = TearOff.NDot2mins
                    End If
                    Return _NDot2mins_W
                End Get
            End Property

            Public ReadOnly Property Regmins() As AggregateParameter
                Get
                    If _Regmins_W Is Nothing Then
                        _Regmins_W = TearOff.Regmins
                    End If
                    Return _Regmins_W
                End Get
            End Property

            Public ReadOnly Property Regnd1mins() As AggregateParameter
                Get
                    If _Regnd1mins_W Is Nothing Then
                        _Regnd1mins_W = TearOff.Regnd1mins
                    End If
                    Return _Regnd1mins_W
                End Get
            End Property

            Public ReadOnly Property Regnd2mins() As AggregateParameter
                Get
                    If _Regnd2mins_W Is Nothing Then
                        _Regnd2mins_W = TearOff.Regnd2mins
                    End If
                    Return _Regnd2mins_W
                End Get
            End Property

            Public ReadOnly Property EarlyWork() As AggregateParameter
                Get
                    If _EarlyWork_W Is Nothing Then
                        _EarlyWork_W = TearOff.EarlyWork
                    End If
                    Return _EarlyWork_W
                End Get
            End Property

            Public ReadOnly Property Status() As AggregateParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Stage() As AggregateParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property Reason() As AggregateParameter
                Get
                    If _Reason_W Is Nothing Then
                        _Reason_W = TearOff.Reason
                    End If
                    Return _Reason_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As AggregateParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Private _CompanyID_W As AggregateParameter = Nothing
            Private _EmployeeID_W As AggregateParameter = Nothing
            Private _Date1_W As AggregateParameter = Nothing
            Private _Shiftcode_W As AggregateParameter = Nothing
            Private _Restdaycode_W As AggregateParameter = Nothing
            Private _In1_W As AggregateParameter = Nothing
            Private _Out1_W As AggregateParameter = Nothing
            Private _In2_W As AggregateParameter = Nothing
            Private _Out2_W As AggregateParameter = Nothing
            Private _Late_W As AggregateParameter = Nothing
            Private _UT_W As AggregateParameter = Nothing
            Private _Absent_W As AggregateParameter = Nothing
            Private _Leave_W As AggregateParameter = Nothing
            Private _Excesshrs_W As AggregateParameter = Nothing
            Private _Remarks_W As AggregateParameter = Nothing
            Private _ErrorDesc_W As AggregateParameter = Nothing
            Private _Supervisor_W As AggregateParameter = Nothing
            Private _OTcode_W As AggregateParameter = Nothing
            Private _Valid_W As AggregateParameter = Nothing
            Private _ValidOTb4Shift_W As AggregateParameter = Nothing
            Private _First8_W As AggregateParameter = Nothing
            Private _Greater8_W As AggregateParameter = Nothing
            Private _NDot1_W As AggregateParameter = Nothing
            Private _NDot2_W As AggregateParameter = Nothing
            Private _Reghrs_W As AggregateParameter = Nothing
            Private _Regnd1_W As AggregateParameter = Nothing
            Private _Regnd2_W As AggregateParameter = Nothing
            Private _ExcusedLate_W As AggregateParameter = Nothing
            Private _ExcusedUT_W As AggregateParameter = Nothing
            Private _Leavecode_W As AggregateParameter = Nothing
            Private _OTAuthStart_W As AggregateParameter = Nothing
            Private _OTAuthEnd_W As AggregateParameter = Nothing
            Private _Edited_W As AggregateParameter = Nothing
            Private _Posted_W As AggregateParameter = Nothing
            Private _Rendered_W As AggregateParameter = Nothing
            Private _OTNoPremium_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing
            Private _Processed_W As AggregateParameter = Nothing
            Private _ReOpen_W As AggregateParameter = Nothing
            Private _Orig_in1_W As AggregateParameter = Nothing
            Private _Orig_out1_W As AggregateParameter = Nothing
            Private _Latemins_W As AggregateParameter = Nothing
            Private _UTmins_W As AggregateParameter = Nothing
            Private _Excessmins_W As AggregateParameter = Nothing
            Private _First8mins_W As AggregateParameter = Nothing
            Private _Greater8mins_W As AggregateParameter = Nothing
            Private _NDot1mins_W As AggregateParameter = Nothing
            Private _NDot2mins_W As AggregateParameter = Nothing
            Private _Regmins_W As AggregateParameter = Nothing
            Private _Regnd1mins_W As AggregateParameter = Nothing
            Private _Regnd2mins_W As AggregateParameter = Nothing
            Private _EarlyWork_W As AggregateParameter = Nothing
            Private _Status_W As AggregateParameter = Nothing
            Private _Stage_W As AggregateParameter = Nothing
            Private _Reason_W As AggregateParameter = Nothing
            Private _DeclinedReason_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _Date1_W = Nothing
                _Shiftcode_W = Nothing
                _Restdaycode_W = Nothing
                _In1_W = Nothing
                _Out1_W = Nothing
                _In2_W = Nothing
                _Out2_W = Nothing
                _Late_W = Nothing
                _UT_W = Nothing
                _Absent_W = Nothing
                _Leave_W = Nothing
                _Excesshrs_W = Nothing
                _Remarks_W = Nothing
                _ErrorDesc_W = Nothing
                _Supervisor_W = Nothing
                _OTcode_W = Nothing
                _Valid_W = Nothing
                _ValidOTb4Shift_W = Nothing
                _First8_W = Nothing
                _Greater8_W = Nothing
                _NDot1_W = Nothing
                _NDot2_W = Nothing
                _Reghrs_W = Nothing
                _Regnd1_W = Nothing
                _Regnd2_W = Nothing
                _ExcusedLate_W = Nothing
                _ExcusedUT_W = Nothing
                _Leavecode_W = Nothing
                _OTAuthStart_W = Nothing
                _OTAuthEnd_W = Nothing
                _Edited_W = Nothing
                _Posted_W = Nothing
                _Rendered_W = Nothing
                _OTNoPremium_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _Processed_W = Nothing
                _ReOpen_W = Nothing
                _Orig_in1_W = Nothing
                _Orig_out1_W = Nothing
                _Latemins_W = Nothing
                _UTmins_W = Nothing
                _Excessmins_W = Nothing
                _First8mins_W = Nothing
                _Greater8mins_W = Nothing
                _NDot1mins_W = Nothing
                _NDot2mins_W = Nothing
                _Regmins_W = Nothing
                _Regnd1mins_W = Nothing
                _Regnd2mins_W = Nothing
                _EarlyWork_W = Nothing
                _Status_W = Nothing
                _Stage_W = Nothing
                _Reason_W = Nothing
                _DeclinedReason_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_AttendanceInsert]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_AttendanceUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_AttendanceDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Date1)
            p.SourceColumn = ColumnNames.Date1
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Date1)
            p.SourceColumn = ColumnNames.Date1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Shiftcode)
            p.SourceColumn = ColumnNames.Shiftcode
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Restdaycode)
            p.SourceColumn = ColumnNames.Restdaycode
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.In1)
            p.SourceColumn = ColumnNames.In1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Out1)
            p.SourceColumn = ColumnNames.Out1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.In2)
            p.SourceColumn = ColumnNames.In2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Out2)
            p.SourceColumn = ColumnNames.Out2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Late)
            p.SourceColumn = ColumnNames.Late
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.UT)
            p.SourceColumn = ColumnNames.UT
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Absent)
            p.SourceColumn = ColumnNames.Absent
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Leave)
            p.SourceColumn = ColumnNames.Leave
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Excesshrs)
            p.SourceColumn = ColumnNames.Excesshrs
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Remarks)
            p.SourceColumn = ColumnNames.Remarks
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ErrorDesc)
            p.SourceColumn = ColumnNames.ErrorDesc
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Supervisor)
            p.SourceColumn = ColumnNames.Supervisor
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTcode)
            p.SourceColumn = ColumnNames.OTcode
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Valid)
            p.SourceColumn = ColumnNames.Valid
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ValidOTb4Shift)
            p.SourceColumn = ColumnNames.ValidOTb4Shift
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.First8)
            p.SourceColumn = ColumnNames.First8
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Greater8)
            p.SourceColumn = ColumnNames.Greater8
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.NDot1)
            p.SourceColumn = ColumnNames.NDot1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.NDot2)
            p.SourceColumn = ColumnNames.NDot2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Reghrs)
            p.SourceColumn = ColumnNames.Reghrs
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Regnd1)
            p.SourceColumn = ColumnNames.Regnd1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Regnd2)
            p.SourceColumn = ColumnNames.Regnd2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ExcusedLate)
            p.SourceColumn = ColumnNames.ExcusedLate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ExcusedUT)
            p.SourceColumn = ColumnNames.ExcusedUT
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Leavecode)
            p.SourceColumn = ColumnNames.Leavecode
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTAuthStart)
            p.SourceColumn = ColumnNames.OTAuthStart
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTAuthEnd)
            p.SourceColumn = ColumnNames.OTAuthEnd
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Edited)
            p.SourceColumn = ColumnNames.Edited
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Posted)
            p.SourceColumn = ColumnNames.Posted
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Rendered)
            p.SourceColumn = ColumnNames.Rendered
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTNoPremium)
            p.SourceColumn = ColumnNames.OTNoPremium
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Processed)
            p.SourceColumn = ColumnNames.Processed
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ReOpen)
            p.SourceColumn = ColumnNames.ReOpen
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Orig_in1)
            p.SourceColumn = ColumnNames.Orig_in1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Orig_out1)
            p.SourceColumn = ColumnNames.Orig_out1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Latemins)
            p.SourceColumn = ColumnNames.Latemins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.UTmins)
            p.SourceColumn = ColumnNames.UTmins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Excessmins)
            p.SourceColumn = ColumnNames.Excessmins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.First8mins)
            p.SourceColumn = ColumnNames.First8mins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Greater8mins)
            p.SourceColumn = ColumnNames.Greater8mins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.NDot1mins)
            p.SourceColumn = ColumnNames.NDot1mins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.NDot2mins)
            p.SourceColumn = ColumnNames.NDot2mins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Regmins)
            p.SourceColumn = ColumnNames.Regmins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Regnd1mins)
            p.SourceColumn = ColumnNames.Regnd1mins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Regnd2mins)
            p.SourceColumn = ColumnNames.Regnd2mins
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EarlyWork)
            p.SourceColumn = ColumnNames.EarlyWork
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Status)
            p.SourceColumn = ColumnNames.Status
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Stage)
            p.SourceColumn = ColumnNames.Stage
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Reason)
            p.SourceColumn = ColumnNames.Reason
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.DeclinedReason)
            p.SourceColumn = ColumnNames.DeclinedReason
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

