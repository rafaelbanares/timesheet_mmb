
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject

Namespace BNS.TK.Entities

    Public MustInherit Class _OTAuthorization
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "OTAuthorization"
            Me.MappingName = "OTAuthorization"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OTAuthorizationLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_OTAuthorization.Parameters.ID, ID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OTAuthorizationLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property ID As SqlParameter
                Get
                    Return New SqlParameter("@ID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property EmployeeID As SqlParameter
                Get
                    Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property DateFiled As SqlParameter
                Get
                    Return New SqlParameter("@DateFiled", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property OTDate As SqlParameter
                Get
                    Return New SqlParameter("@OTDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property OTstart As SqlParameter
                Get
                    Return New SqlParameter("@OTstart", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property OTend As SqlParameter
                Get
                    Return New SqlParameter("@OTend", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property ApprovedOT As SqlParameter
                Get
                    Return New SqlParameter("@ApprovedOT", SqlDbType.Decimal, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Reason As SqlParameter
                Get
                    Return New SqlParameter("@Reason", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Remarks As SqlParameter
                Get
                    Return New SqlParameter("@Remarks", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property Status As SqlParameter
                Get
                    Return New SqlParameter("@Status", SqlDbType.VarChar, 12)
                End Get
            End Property

            Public Shared ReadOnly Property Stage As SqlParameter
                Get
                    Return New SqlParameter("@Stage", SqlDbType.SmallInt, 0)
                End Get
            End Property

            Public Shared ReadOnly Property AuthBy As SqlParameter
                Get
                    Return New SqlParameter("@AuthBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Posted As SqlParameter
                Get
                    Return New SqlParameter("@Posted", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedBy As SqlParameter
                Get
                    Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property DeclinedReason As SqlParameter
                Get
                    Return New SqlParameter("@DeclinedReason", SqlDbType.VarChar, 100)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const ID As String = "ID"
            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const DateFiled As String = "DateFiled"
            Public Const OTDate As String = "OTDate"
            Public Const OTstart As String = "OTstart"
            Public Const OTend As String = "OTend"
            Public Const ApprovedOT As String = "ApprovedOT"
            Public Const Reason As String = "Reason"
            Public Const Remarks As String = "Remarks"
            Public Const Status As String = "Status"
            Public Const Stage As String = "Stage"
            Public Const AuthBy As String = "AuthBy"
            Public Const Posted As String = "Posted"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const DeclinedReason As String = "DeclinedReason"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _OTAuthorization.PropertyNames.ID
                    ht(CompanyID) = _OTAuthorization.PropertyNames.CompanyID
                    ht(EmployeeID) = _OTAuthorization.PropertyNames.EmployeeID
                    ht(DateFiled) = _OTAuthorization.PropertyNames.DateFiled
                    ht(OTDate) = _OTAuthorization.PropertyNames.OTDate
                    ht(OTstart) = _OTAuthorization.PropertyNames.OTstart
                    ht(OTend) = _OTAuthorization.PropertyNames.OTend
                    ht(ApprovedOT) = _OTAuthorization.PropertyNames.ApprovedOT
                    ht(Reason) = _OTAuthorization.PropertyNames.Reason
                    ht(Remarks) = _OTAuthorization.PropertyNames.Remarks
                    ht(Status) = _OTAuthorization.PropertyNames.Status
                    ht(Stage) = _OTAuthorization.PropertyNames.Stage
                    ht(AuthBy) = _OTAuthorization.PropertyNames.AuthBy
                    ht(Posted) = _OTAuthorization.PropertyNames.Posted
                    ht(CreatedBy) = _OTAuthorization.PropertyNames.CreatedBy
                    ht(CreatedDate) = _OTAuthorization.PropertyNames.CreatedDate
                    ht(LastUpdBy) = _OTAuthorization.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _OTAuthorization.PropertyNames.LastUpdDate
                    ht(DeclinedReason) = _OTAuthorization.PropertyNames.DeclinedReason

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const ID As String = "ID"
            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const DateFiled As String = "DateFiled"
            Public Const OTDate As String = "OTDate"
            Public Const OTstart As String = "OTstart"
            Public Const OTend As String = "OTend"
            Public Const ApprovedOT As String = "ApprovedOT"
            Public Const Reason As String = "Reason"
            Public Const Remarks As String = "Remarks"
            Public Const Status As String = "Status"
            Public Const Stage As String = "Stage"
            Public Const AuthBy As String = "AuthBy"
            Public Const Posted As String = "Posted"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const DeclinedReason As String = "DeclinedReason"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _OTAuthorization.ColumnNames.ID
                    ht(CompanyID) = _OTAuthorization.ColumnNames.CompanyID
                    ht(EmployeeID) = _OTAuthorization.ColumnNames.EmployeeID
                    ht(DateFiled) = _OTAuthorization.ColumnNames.DateFiled
                    ht(OTDate) = _OTAuthorization.ColumnNames.OTDate
                    ht(OTstart) = _OTAuthorization.ColumnNames.OTstart
                    ht(OTend) = _OTAuthorization.ColumnNames.OTend
                    ht(ApprovedOT) = _OTAuthorization.ColumnNames.ApprovedOT
                    ht(Reason) = _OTAuthorization.ColumnNames.Reason
                    ht(Remarks) = _OTAuthorization.ColumnNames.Remarks
                    ht(Status) = _OTAuthorization.ColumnNames.Status
                    ht(Stage) = _OTAuthorization.ColumnNames.Stage
                    ht(AuthBy) = _OTAuthorization.ColumnNames.AuthBy
                    ht(Posted) = _OTAuthorization.ColumnNames.Posted
                    ht(CreatedBy) = _OTAuthorization.ColumnNames.CreatedBy
                    ht(CreatedDate) = _OTAuthorization.ColumnNames.CreatedDate
                    ht(LastUpdBy) = _OTAuthorization.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _OTAuthorization.ColumnNames.LastUpdDate
                    ht(DeclinedReason) = _OTAuthorization.ColumnNames.DeclinedReason

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const ID As String = "s_ID"
            Public Const CompanyID As String = "s_CompanyID"
            Public Const EmployeeID As String = "s_EmployeeID"
            Public Const DateFiled As String = "s_DateFiled"
            Public Const OTDate As String = "s_OTDate"
            Public Const OTstart As String = "s_OTstart"
            Public Const OTend As String = "s_OTend"
            Public Const ApprovedOT As String = "s_ApprovedOT"
            Public Const Reason As String = "s_Reason"
            Public Const Remarks As String = "s_Remarks"
            Public Const Status As String = "s_Status"
            Public Const Stage As String = "s_Stage"
            Public Const AuthBy As String = "s_AuthBy"
            Public Const Posted As String = "s_Posted"
            Public Const CreatedBy As String = "s_CreatedBy"
            Public Const CreatedDate As String = "s_CreatedDate"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"
            Public Const DeclinedReason As String = "s_DeclinedReason"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property ID As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.ID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.ID, Value)
            End Set
        End Property

        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property EmployeeID As String
            Get
                Return MyBase.GetString(ColumnNames.EmployeeID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.EmployeeID, Value)
            End Set
        End Property

        Public Overridable Property DateFiled As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.DateFiled)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.DateFiled, Value)
            End Set
        End Property

        Public Overridable Property OTDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.OTDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.OTDate, Value)
            End Set
        End Property

        Public Overridable Property OTstart As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.OTstart)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.OTstart, Value)
            End Set
        End Property

        Public Overridable Property OTend As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.OTend)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.OTend, Value)
            End Set
        End Property

        Public Overridable Property ApprovedOT As Decimal
            Get
                Return MyBase.GetDecimal(ColumnNames.ApprovedOT)
            End Get
            Set(ByVal Value As Decimal)
                MyBase.SetDecimal(ColumnNames.ApprovedOT, Value)
            End Set
        End Property

        Public Overridable Property Reason As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Reason)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Reason, Value)
            End Set
        End Property

        Public Overridable Property Remarks As String
            Get
                Return MyBase.GetString(ColumnNames.Remarks)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Remarks, Value)
            End Set
        End Property

        Public Overridable Property Status As String
            Get
                Return MyBase.GetString(ColumnNames.Status)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Status, Value)
            End Set
        End Property

        Public Overridable Property Stage As Short
            Get
                Return MyBase.GetShort(ColumnNames.Stage)
            End Get
            Set(ByVal Value As Short)
                MyBase.SetShort(ColumnNames.Stage, Value)
            End Set
        End Property

        Public Overridable Property AuthBy As String
            Get
                Return MyBase.GetString(ColumnNames.AuthBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.AuthBy, Value)
            End Set
        End Property

        Public Overridable Property Posted As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Posted)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Posted, Value)
            End Set
        End Property

        Public Overridable Property CreatedBy As String
            Get
                Return MyBase.GetString(ColumnNames.CreatedBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CreatedBy, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property

        Public Overridable Property DeclinedReason As String
            Get
                Return MyBase.GetString(ColumnNames.DeclinedReason)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.DeclinedReason, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_ID As String
            Get
                If Me.IsColumnNull(ColumnNames.ID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.ID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ID)
                Else
                    Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EmployeeID As String
            Get
                If Me.IsColumnNull(ColumnNames.EmployeeID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EmployeeID)
                Else
                    Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_DateFiled As String
            Get
                If Me.IsColumnNull(ColumnNames.DateFiled) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.DateFiled)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.DateFiled)
                Else
                    Me.DateFiled = MyBase.SetDateTimeAsString(ColumnNames.DateFiled, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTDate As String
            Get
                If Me.IsColumnNull(ColumnNames.OTDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.OTDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTDate)
                Else
                    Me.OTDate = MyBase.SetDateTimeAsString(ColumnNames.OTDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTstart As String
            Get
                If Me.IsColumnNull(ColumnNames.OTstart) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.OTstart)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTstart)
                Else
                    Me.OTstart = MyBase.SetDateTimeAsString(ColumnNames.OTstart, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OTend As String
            Get
                If Me.IsColumnNull(ColumnNames.OTend) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.OTend)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OTend)
                Else
                    Me.OTend = MyBase.SetDateTimeAsString(ColumnNames.OTend, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_ApprovedOT As String
            Get
                If Me.IsColumnNull(ColumnNames.ApprovedOT) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDecimalAsString(ColumnNames.ApprovedOT)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ApprovedOT)
                Else
                    Me.ApprovedOT = MyBase.SetDecimalAsString(ColumnNames.ApprovedOT, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Reason As String
            Get
                If Me.IsColumnNull(ColumnNames.Reason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Reason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Reason)
                Else
                    Me.Reason = MyBase.SetIntegerAsString(ColumnNames.Reason, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Remarks As String
            Get
                If Me.IsColumnNull(ColumnNames.Remarks) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Remarks)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Remarks)
                Else
                    Me.Remarks = MyBase.SetStringAsString(ColumnNames.Remarks, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Status As String
            Get
                If Me.IsColumnNull(ColumnNames.Status) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Status)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Status)
                Else
                    Me.Status = MyBase.SetStringAsString(ColumnNames.Status, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Stage As String
            Get
                If Me.IsColumnNull(ColumnNames.Stage) Then
                    Return String.Empty
                Else
                    Return MyBase.GetShortAsString(ColumnNames.Stage)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Stage)
                Else
                    Me.Stage = MyBase.SetShortAsString(ColumnNames.Stage, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_AuthBy As String
            Get
                If Me.IsColumnNull(ColumnNames.AuthBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.AuthBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.AuthBy)
                Else
                    Me.AuthBy = MyBase.SetStringAsString(ColumnNames.AuthBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Posted As String
            Get
                If Me.IsColumnNull(ColumnNames.Posted) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Posted)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Posted)
                Else
                    Me.Posted = MyBase.SetBooleanAsString(ColumnNames.Posted, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedBy As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedBy)
                Else
                    Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_DeclinedReason As String
            Get
                If Me.IsColumnNull(ColumnNames.DeclinedReason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.DeclinedReason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.DeclinedReason)
                Else
                    Me.DeclinedReason = MyBase.SetStringAsString(ColumnNames.DeclinedReason, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DateFiled() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.DateFiled, Parameters.DateFiled)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTDate, Parameters.OTDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTstart() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTstart, Parameters.OTstart)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTend() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OTend, Parameters.OTend)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ApprovedOT() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ApprovedOT, Parameters.ApprovedOT)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Reason, Parameters.Reason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Remarks() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Remarks, Parameters.Remarks)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property AuthBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.AuthBy, Parameters.AuthBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Posted() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Posted, Parameters.Posted)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property ID() As WhereParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As WhereParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property DateFiled() As WhereParameter
                Get
                    If _DateFiled_W Is Nothing Then
                        _DateFiled_W = TearOff.DateFiled
                    End If
                    Return _DateFiled_W
                End Get
            End Property

            Public ReadOnly Property OTDate() As WhereParameter
                Get
                    If _OTDate_W Is Nothing Then
                        _OTDate_W = TearOff.OTDate
                    End If
                    Return _OTDate_W
                End Get
            End Property

            Public ReadOnly Property OTstart() As WhereParameter
                Get
                    If _OTstart_W Is Nothing Then
                        _OTstart_W = TearOff.OTstart
                    End If
                    Return _OTstart_W
                End Get
            End Property

            Public ReadOnly Property OTend() As WhereParameter
                Get
                    If _OTend_W Is Nothing Then
                        _OTend_W = TearOff.OTend
                    End If
                    Return _OTend_W
                End Get
            End Property

            Public ReadOnly Property ApprovedOT() As WhereParameter
                Get
                    If _ApprovedOT_W Is Nothing Then
                        _ApprovedOT_W = TearOff.ApprovedOT
                    End If
                    Return _ApprovedOT_W
                End Get
            End Property

            Public ReadOnly Property Reason() As WhereParameter
                Get
                    If _Reason_W Is Nothing Then
                        _Reason_W = TearOff.Reason
                    End If
                    Return _Reason_W
                End Get
            End Property

            Public ReadOnly Property Remarks() As WhereParameter
                Get
                    If _Remarks_W Is Nothing Then
                        _Remarks_W = TearOff.Remarks
                    End If
                    Return _Remarks_W
                End Get
            End Property

            Public ReadOnly Property Status() As WhereParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Stage() As WhereParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property AuthBy() As WhereParameter
                Get
                    If _AuthBy_W Is Nothing Then
                        _AuthBy_W = TearOff.AuthBy
                    End If
                    Return _AuthBy_W
                End Get
            End Property

            Public ReadOnly Property Posted() As WhereParameter
                Get
                    If _Posted_W Is Nothing Then
                        _Posted_W = TearOff.Posted
                    End If
                    Return _Posted_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As WhereParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As WhereParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Private _ID_W As WhereParameter = Nothing
            Private _CompanyID_W As WhereParameter = Nothing
            Private _EmployeeID_W As WhereParameter = Nothing
            Private _DateFiled_W As WhereParameter = Nothing
            Private _OTDate_W As WhereParameter = Nothing
            Private _OTstart_W As WhereParameter = Nothing
            Private _OTend_W As WhereParameter = Nothing
            Private _ApprovedOT_W As WhereParameter = Nothing
            Private _Reason_W As WhereParameter = Nothing
            Private _Remarks_W As WhereParameter = Nothing
            Private _Status_W As WhereParameter = Nothing
            Private _Stage_W As WhereParameter = Nothing
            Private _AuthBy_W As WhereParameter = Nothing
            Private _Posted_W As WhereParameter = Nothing
            Private _CreatedBy_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing
            Private _DeclinedReason_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _ID_W = Nothing
                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _DateFiled_W = Nothing
                _OTDate_W = Nothing
                _OTstart_W = Nothing
                _OTend_W = Nothing
                _ApprovedOT_W = Nothing
                _Reason_W = Nothing
                _Remarks_W = Nothing
                _Status_W = Nothing
                _Stage_W = Nothing
                _AuthBy_W = Nothing
                _Posted_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _DeclinedReason_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DateFiled() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DateFiled, Parameters.DateFiled)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTDate, Parameters.OTDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTstart() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTstart, Parameters.OTstart)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OTend() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTend, Parameters.OTend)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property ApprovedOT() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApprovedOT, Parameters.ApprovedOT)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Reason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Reason, Parameters.Reason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Remarks() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Remarks, Parameters.Remarks)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property AuthBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AuthBy, Parameters.AuthBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Posted() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Posted, Parameters.Posted)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property ID() As AggregateParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As AggregateParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property DateFiled() As AggregateParameter
                Get
                    If _DateFiled_W Is Nothing Then
                        _DateFiled_W = TearOff.DateFiled
                    End If
                    Return _DateFiled_W
                End Get
            End Property

            Public ReadOnly Property OTDate() As AggregateParameter
                Get
                    If _OTDate_W Is Nothing Then
                        _OTDate_W = TearOff.OTDate
                    End If
                    Return _OTDate_W
                End Get
            End Property

            Public ReadOnly Property OTstart() As AggregateParameter
                Get
                    If _OTstart_W Is Nothing Then
                        _OTstart_W = TearOff.OTstart
                    End If
                    Return _OTstart_W
                End Get
            End Property

            Public ReadOnly Property OTend() As AggregateParameter
                Get
                    If _OTend_W Is Nothing Then
                        _OTend_W = TearOff.OTend
                    End If
                    Return _OTend_W
                End Get
            End Property

            Public ReadOnly Property ApprovedOT() As AggregateParameter
                Get
                    If _ApprovedOT_W Is Nothing Then
                        _ApprovedOT_W = TearOff.ApprovedOT
                    End If
                    Return _ApprovedOT_W
                End Get
            End Property

            Public ReadOnly Property Reason() As AggregateParameter
                Get
                    If _Reason_W Is Nothing Then
                        _Reason_W = TearOff.Reason
                    End If
                    Return _Reason_W
                End Get
            End Property

            Public ReadOnly Property Remarks() As AggregateParameter
                Get
                    If _Remarks_W Is Nothing Then
                        _Remarks_W = TearOff.Remarks
                    End If
                    Return _Remarks_W
                End Get
            End Property

            Public ReadOnly Property Status() As AggregateParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Stage() As AggregateParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property AuthBy() As AggregateParameter
                Get
                    If _AuthBy_W Is Nothing Then
                        _AuthBy_W = TearOff.AuthBy
                    End If
                    Return _AuthBy_W
                End Get
            End Property

            Public ReadOnly Property Posted() As AggregateParameter
                Get
                    If _Posted_W Is Nothing Then
                        _Posted_W = TearOff.Posted
                    End If
                    Return _Posted_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As AggregateParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As AggregateParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Private _ID_W As AggregateParameter = Nothing
            Private _CompanyID_W As AggregateParameter = Nothing
            Private _EmployeeID_W As AggregateParameter = Nothing
            Private _DateFiled_W As AggregateParameter = Nothing
            Private _OTDate_W As AggregateParameter = Nothing
            Private _OTstart_W As AggregateParameter = Nothing
            Private _OTend_W As AggregateParameter = Nothing
            Private _ApprovedOT_W As AggregateParameter = Nothing
            Private _Reason_W As AggregateParameter = Nothing
            Private _Remarks_W As AggregateParameter = Nothing
            Private _Status_W As AggregateParameter = Nothing
            Private _Stage_W As AggregateParameter = Nothing
            Private _AuthBy_W As AggregateParameter = Nothing
            Private _Posted_W As AggregateParameter = Nothing
            Private _CreatedBy_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing
            Private _DeclinedReason_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _ID_W = Nothing
                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _DateFiled_W = Nothing
                _OTDate_W = Nothing
                _OTstart_W = Nothing
                _OTend_W = Nothing
                _ApprovedOT_W = Nothing
                _Reason_W = Nothing
                _Remarks_W = Nothing
                _Status_W = Nothing
                _Stage_W = Nothing
                _AuthBy_W = Nothing
                _Posted_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _DeclinedReason_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OTAuthorizationInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.ID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OTAuthorizationUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OTAuthorizationDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.DateFiled)
            p.SourceColumn = ColumnNames.DateFiled
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTDate)
            p.SourceColumn = ColumnNames.OTDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTstart)
            p.SourceColumn = ColumnNames.OTstart
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OTend)
            p.SourceColumn = ColumnNames.OTend
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.ApprovedOT)
            p.SourceColumn = ColumnNames.ApprovedOT
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Reason)
            p.SourceColumn = ColumnNames.Reason
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Remarks)
            p.SourceColumn = ColumnNames.Remarks
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Status)
            p.SourceColumn = ColumnNames.Status
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Stage)
            p.SourceColumn = ColumnNames.Stage
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.AuthBy)
            p.SourceColumn = ColumnNames.AuthBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Posted)
            p.SourceColumn = ColumnNames.Posted
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedBy)
            p.SourceColumn = ColumnNames.CreatedBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.DeclinedReason)
            p.SourceColumn = ColumnNames.DeclinedReason
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

