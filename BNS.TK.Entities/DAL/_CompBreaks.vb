
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _CompBreaks
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "CompBreaks"
			Me.MappingName = "CompBreaks"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_CompBreaksLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_CompBreaks.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_CompBreaksLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property BreakIn As SqlParameter
			Get
				Return New SqlParameter("@BreakIn", SqlDbType.VarChar, 8)
			End Get
		End Property
		
		Public Shared ReadOnly Property BreakOut As SqlParameter
			Get
				Return New SqlParameter("@BreakOut", SqlDbType.VarChar, 8)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const BreakIn As String = "BreakIn"
        Public Const BreakOut As String = "BreakOut"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _CompBreaks.PropertyNames.ID
				ht(CompanyID) = _CompBreaks.PropertyNames.CompanyID
				ht(BreakIn) = _CompBreaks.PropertyNames.BreakIn
				ht(BreakOut) = _CompBreaks.PropertyNames.BreakOut

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const BreakIn As String = "BreakIn"
        Public Const BreakOut As String = "BreakOut"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _CompBreaks.ColumnNames.ID
				ht(CompanyID) = _CompBreaks.ColumnNames.CompanyID
				ht(BreakIn) = _CompBreaks.ColumnNames.BreakIn
				ht(BreakOut) = _CompBreaks.ColumnNames.BreakOut

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const BreakIn As String = "s_BreakIn"
        Public Const BreakOut As String = "s_BreakOut"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property BreakIn As String
			Get
				Return MyBase.GetString(ColumnNames.BreakIn)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.BreakIn, Value)
			End Set
		End Property

		Public Overridable Property BreakOut As String
			Get
				Return MyBase.GetString(ColumnNames.BreakOut)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.BreakOut, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_BreakIn As String
			Get
				If Me.IsColumnNull(ColumnNames.BreakIn) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.BreakIn)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.BreakIn)
				Else
					Me.BreakIn = MyBase.SetStringAsString(ColumnNames.BreakIn, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_BreakOut As String
			Get
				If Me.IsColumnNull(ColumnNames.BreakOut) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.BreakOut)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.BreakOut)
				Else
					Me.BreakOut = MyBase.SetStringAsString(ColumnNames.BreakOut, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property BreakIn() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.BreakIn, Parameters.BreakIn)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property BreakOut() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.BreakOut, Parameters.BreakOut)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property BreakIn() As WhereParameter 
			Get
				If _BreakIn_W Is Nothing Then
					_BreakIn_W = TearOff.BreakIn
				End If
				Return _BreakIn_W
			End Get
		End Property

		Public ReadOnly Property BreakOut() As WhereParameter 
			Get
				If _BreakOut_W Is Nothing Then
					_BreakOut_W = TearOff.BreakOut
				End If
				Return _BreakOut_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _BreakIn_W As WhereParameter = Nothing
		Private _BreakOut_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_BreakIn_W = Nothing
			_BreakOut_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property BreakIn() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.BreakIn, Parameters.BreakIn)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property BreakOut() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.BreakOut, Parameters.BreakOut)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property BreakIn() As AggregateParameter 
			Get
				If _BreakIn_W Is Nothing Then
					_BreakIn_W = TearOff.BreakIn
				End If
				Return _BreakIn_W
			End Get
		End Property

		Public ReadOnly Property BreakOut() As AggregateParameter 
			Get
				If _BreakOut_W Is Nothing Then
					_BreakOut_W = TearOff.BreakOut
				End If
				Return _BreakOut_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _BreakIn_W As AggregateParameter = Nothing
		Private _BreakOut_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_BreakIn_W = Nothing
		_BreakOut_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_CompBreaksInsert]" 
	    
		CreateParameters(cmd)
		    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_CompBreaksUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_CompBreaksDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.BreakIn)
		p.SourceColumn = ColumnNames.BreakIn
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.BreakOut)
		p.SourceColumn = ColumnNames.BreakOut
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

