
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _RunData
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "RunData"
			Me.MappingName = "RunData"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_RunDataLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_RunData.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_RunDataLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property LastInsert As SqlParameter
			Get
				Return New SqlParameter("@LastInsert", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property BulkInsertRunning As SqlParameter
			Get
				Return New SqlParameter("@BulkInsertRunning", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property KioskLocked As SqlParameter
			Get
				Return New SqlParameter("@KioskLocked", SqlDbType.Bit, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const LastInsert As String = "LastInsert"
        Public Const BulkInsertRunning As String = "BulkInsertRunning"
        Public Const ID As String = "ID"
        Public Const KioskLocked As String = "KioskLocked"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(LastInsert) = _RunData.PropertyNames.LastInsert
				ht(BulkInsertRunning) = _RunData.PropertyNames.BulkInsertRunning
				ht(ID) = _RunData.PropertyNames.ID
				ht(KioskLocked) = _RunData.PropertyNames.KioskLocked

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const LastInsert As String = "LastInsert"
        Public Const BulkInsertRunning As String = "BulkInsertRunning"
        Public Const ID As String = "ID"
        Public Const KioskLocked As String = "KioskLocked"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(LastInsert) = _RunData.ColumnNames.LastInsert
				ht(BulkInsertRunning) = _RunData.ColumnNames.BulkInsertRunning
				ht(ID) = _RunData.ColumnNames.ID
				ht(KioskLocked) = _RunData.ColumnNames.KioskLocked

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const LastInsert As String = "s_LastInsert"
        Public Const BulkInsertRunning As String = "s_BulkInsertRunning"
        Public Const ID As String = "s_ID"
        Public Const KioskLocked As String = "s_KioskLocked"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property LastInsert As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastInsert)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastInsert, Value)
			End Set
		End Property

		Public Overridable Property BulkInsertRunning As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.BulkInsertRunning)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.BulkInsertRunning, Value)
			End Set
		End Property

		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property KioskLocked As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.KioskLocked)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.KioskLocked, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_LastInsert As String
			Get
				If Me.IsColumnNull(ColumnNames.LastInsert) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastInsert)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastInsert)
				Else
					Me.LastInsert = MyBase.SetDateTimeAsString(ColumnNames.LastInsert, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_BulkInsertRunning As String
			Get
				If Me.IsColumnNull(ColumnNames.BulkInsertRunning) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.BulkInsertRunning)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.BulkInsertRunning)
				Else
					Me.BulkInsertRunning = MyBase.SetBooleanAsString(ColumnNames.BulkInsertRunning, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_KioskLocked As String
			Get
				If Me.IsColumnNull(ColumnNames.KioskLocked) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.KioskLocked)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.KioskLocked)
				Else
					Me.KioskLocked = MyBase.SetBooleanAsString(ColumnNames.KioskLocked, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property LastInsert() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastInsert, Parameters.LastInsert)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property BulkInsertRunning() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.BulkInsertRunning, Parameters.BulkInsertRunning)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property KioskLocked() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.KioskLocked, Parameters.KioskLocked)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property LastInsert() As WhereParameter 
			Get
				If _LastInsert_W Is Nothing Then
					_LastInsert_W = TearOff.LastInsert
				End If
				Return _LastInsert_W
			End Get
		End Property

		Public ReadOnly Property BulkInsertRunning() As WhereParameter 
			Get
				If _BulkInsertRunning_W Is Nothing Then
					_BulkInsertRunning_W = TearOff.BulkInsertRunning
				End If
				Return _BulkInsertRunning_W
			End Get
		End Property

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property KioskLocked() As WhereParameter 
			Get
				If _KioskLocked_W Is Nothing Then
					_KioskLocked_W = TearOff.KioskLocked
				End If
				Return _KioskLocked_W
			End Get
		End Property

		Private _LastInsert_W As WhereParameter = Nothing
		Private _BulkInsertRunning_W As WhereParameter = Nothing
		Private _ID_W As WhereParameter = Nothing
		Private _KioskLocked_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_LastInsert_W = Nothing
			_BulkInsertRunning_W = Nothing
			_ID_W = Nothing
			_KioskLocked_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property LastInsert() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastInsert, Parameters.LastInsert)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property BulkInsertRunning() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.BulkInsertRunning, Parameters.BulkInsertRunning)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property KioskLocked() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.KioskLocked, Parameters.KioskLocked)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property LastInsert() As AggregateParameter 
			Get
				If _LastInsert_W Is Nothing Then
					_LastInsert_W = TearOff.LastInsert
				End If
				Return _LastInsert_W
			End Get
		End Property

		Public ReadOnly Property BulkInsertRunning() As AggregateParameter 
			Get
				If _BulkInsertRunning_W Is Nothing Then
					_BulkInsertRunning_W = TearOff.BulkInsertRunning
				End If
				Return _BulkInsertRunning_W
			End Get
		End Property

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property KioskLocked() As AggregateParameter 
			Get
				If _KioskLocked_W Is Nothing Then
					_KioskLocked_W = TearOff.KioskLocked
				End If
				Return _KioskLocked_W
			End Get
		End Property

		Private _LastInsert_W As AggregateParameter = Nothing
		Private _BulkInsertRunning_W As AggregateParameter = Nothing
		Private _ID_W As AggregateParameter = Nothing
		Private _KioskLocked_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_LastInsert_W = Nothing
		_BulkInsertRunning_W = Nothing
		_ID_W = Nothing
		_KioskLocked_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_RunDataInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_RunDataUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_RunDataDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.LastInsert)
		p.SourceColumn = ColumnNames.LastInsert
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.BulkInsertRunning)
		p.SourceColumn = ColumnNames.BulkInsertRunning
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.KioskLocked)
		p.SourceColumn = ColumnNames.KioskLocked
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

