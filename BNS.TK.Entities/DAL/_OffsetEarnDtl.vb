
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

Public MustInherit Class _OffsetEarnDtl
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "OffsetEarnDtl"
			Me.MappingName = "OffsetEarnDtl"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetEarnDtlLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal Row_id As Long) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_OffsetEarnDtl.Parameters.Row_id, Row_id)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetEarnDtlLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property Row_id As SqlParameter
			Get
				Return New SqlParameter("@Row_id", SqlDbType.BigInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OffsetEarnID As SqlParameter
			Get
				Return New SqlParameter("@OffsetEarnID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Days As SqlParameter
			Get
				Return New SqlParameter("@Days", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Hours As SqlParameter
			Get
				Return New SqlParameter("@Hours", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Minutes As SqlParameter
			Get
				Return New SqlParameter("@Minutes", SqlDbType.Int, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const Row_id As String = "row_id"
        Public Const OffsetEarnID As String = "OffsetEarnID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const Days As String = "days"
        Public Const Hours As String = "hours"
        Public Const Minutes As String = "minutes"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(Row_id) = _OffsetEarnDtl.PropertyNames.Row_id
				ht(OffsetEarnID) = _OffsetEarnDtl.PropertyNames.OffsetEarnID
				ht(CompanyID) = _OffsetEarnDtl.PropertyNames.CompanyID
				ht(EmployeeID) = _OffsetEarnDtl.PropertyNames.EmployeeID
				ht(Days) = _OffsetEarnDtl.PropertyNames.Days
				ht(Hours) = _OffsetEarnDtl.PropertyNames.Hours
				ht(Minutes) = _OffsetEarnDtl.PropertyNames.Minutes

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const Row_id As String = "Row_id"
        Public Const OffsetEarnID As String = "OffsetEarnID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const Days As String = "Days"
        Public Const Hours As String = "Hours"
        Public Const Minutes As String = "Minutes"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(Row_id) = _OffsetEarnDtl.ColumnNames.Row_id
				ht(OffsetEarnID) = _OffsetEarnDtl.ColumnNames.OffsetEarnID
				ht(CompanyID) = _OffsetEarnDtl.ColumnNames.CompanyID
				ht(EmployeeID) = _OffsetEarnDtl.ColumnNames.EmployeeID
				ht(Days) = _OffsetEarnDtl.ColumnNames.Days
				ht(Hours) = _OffsetEarnDtl.ColumnNames.Hours
				ht(Minutes) = _OffsetEarnDtl.ColumnNames.Minutes

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const Row_id As String = "s_Row_id"
        Public Const OffsetEarnID As String = "s_OffsetEarnID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const Days As String = "s_Days"
        Public Const Hours As String = "s_Hours"
        Public Const Minutes As String = "s_Minutes"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property Row_id As Long
			Get
				Return MyBase.GetLong(ColumnNames.Row_id)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.Row_id, Value)
			End Set
		End Property

		Public Overridable Property OffsetEarnID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.OffsetEarnID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.OffsetEarnID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property Days As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.Days)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.Days, Value)
			End Set
		End Property

		Public Overridable Property Hours As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.Hours)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.Hours, Value)
			End Set
		End Property

		Public Overridable Property Minutes As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.Minutes)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.Minutes, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_Row_id As String
			Get
				If Me.IsColumnNull(ColumnNames.Row_id) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.Row_id)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Row_id)
				Else
					Me.Row_id = MyBase.SetLongAsString(ColumnNames.Row_id, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OffsetEarnID As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetEarnID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.OffsetEarnID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetEarnID)
				Else
					Me.OffsetEarnID = MyBase.SetIntegerAsString(ColumnNames.OffsetEarnID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Days As String
			Get
				If Me.IsColumnNull(ColumnNames.Days) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.Days)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Days)
				Else
					Me.Days = MyBase.SetIntegerAsString(ColumnNames.Days, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Hours As String
			Get
				If Me.IsColumnNull(ColumnNames.Hours) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.Hours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Hours)
				Else
					Me.Hours = MyBase.SetIntegerAsString(ColumnNames.Hours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Minutes As String
			Get
				If Me.IsColumnNull(ColumnNames.Minutes) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.Minutes)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Minutes)
				Else
					Me.Minutes = MyBase.SetIntegerAsString(ColumnNames.Minutes, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property Row_id() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Row_id, Parameters.Row_id)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OffsetEarnID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetEarnID, Parameters.OffsetEarnID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Days() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Days, Parameters.Days)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Hours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Hours, Parameters.Hours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Minutes() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Minutes, Parameters.Minutes)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property Row_id() As WhereParameter 
			Get
				If _Row_id_W Is Nothing Then
					_Row_id_W = TearOff.Row_id
				End If
				Return _Row_id_W
			End Get
		End Property

		Public ReadOnly Property OffsetEarnID() As WhereParameter 
			Get
				If _OffsetEarnID_W Is Nothing Then
					_OffsetEarnID_W = TearOff.OffsetEarnID
				End If
				Return _OffsetEarnID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property Days() As WhereParameter 
			Get
				If _Days_W Is Nothing Then
					_Days_W = TearOff.Days
				End If
				Return _Days_W
			End Get
		End Property

		Public ReadOnly Property Hours() As WhereParameter 
			Get
				If _Hours_W Is Nothing Then
					_Hours_W = TearOff.Hours
				End If
				Return _Hours_W
			End Get
		End Property

		Public ReadOnly Property Minutes() As WhereParameter 
			Get
				If _Minutes_W Is Nothing Then
					_Minutes_W = TearOff.Minutes
				End If
				Return _Minutes_W
			End Get
		End Property

		Private _Row_id_W As WhereParameter = Nothing
		Private _OffsetEarnID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _Days_W As WhereParameter = Nothing
		Private _Hours_W As WhereParameter = Nothing
		Private _Minutes_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_Row_id_W = Nothing
			_OffsetEarnID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_Days_W = Nothing
			_Hours_W = Nothing
			_Minutes_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property Row_id() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Row_id, Parameters.Row_id)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OffsetEarnID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetEarnID, Parameters.OffsetEarnID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Days() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Days, Parameters.Days)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Hours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Hours, Parameters.Hours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Minutes() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Minutes, Parameters.Minutes)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property Row_id() As AggregateParameter 
			Get
				If _Row_id_W Is Nothing Then
					_Row_id_W = TearOff.Row_id
				End If
				Return _Row_id_W
			End Get
		End Property

		Public ReadOnly Property OffsetEarnID() As AggregateParameter 
			Get
				If _OffsetEarnID_W Is Nothing Then
					_OffsetEarnID_W = TearOff.OffsetEarnID
				End If
				Return _OffsetEarnID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property Days() As AggregateParameter 
			Get
				If _Days_W Is Nothing Then
					_Days_W = TearOff.Days
				End If
				Return _Days_W
			End Get
		End Property

		Public ReadOnly Property Hours() As AggregateParameter 
			Get
				If _Hours_W Is Nothing Then
					_Hours_W = TearOff.Hours
				End If
				Return _Hours_W
			End Get
		End Property

		Public ReadOnly Property Minutes() As AggregateParameter 
			Get
				If _Minutes_W Is Nothing Then
					_Minutes_W = TearOff.Minutes
				End If
				Return _Minutes_W
			End Get
		End Property

		Private _Row_id_W As AggregateParameter = Nothing
		Private _OffsetEarnID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _Days_W As AggregateParameter = Nothing
		Private _Hours_W As AggregateParameter = Nothing
		Private _Minutes_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_Row_id_W = Nothing
		_OffsetEarnID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_Days_W = Nothing
		_Hours_W = Nothing
		_Minutes_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnDtlInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.Row_id.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnDtlUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnDtlDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.Row_id)
		p.SourceColumn = ColumnNames.Row_id
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.Row_id)
		p.SourceColumn = ColumnNames.Row_id
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OffsetEarnID)
		p.SourceColumn = ColumnNames.OffsetEarnID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Days)
		p.SourceColumn = ColumnNames.Days
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Hours)
		p.SourceColumn = ColumnNames.Hours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Minutes)
		p.SourceColumn = ColumnNames.Minutes
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

