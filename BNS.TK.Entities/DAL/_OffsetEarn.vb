
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

Public MustInherit Class _OffsetEarn
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "OffsetEarn"
			Me.MappingName = "OffsetEarn"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetEarnLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal OffsetEarnID As Long) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_OffsetEarn.Parameters.OffsetEarnID, OffsetEarnID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetEarnLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property OffsetEarnID As SqlParameter
			Get
				Return New SqlParameter("@OffsetEarnID", SqlDbType.BigInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property TranDate As SqlParameter
			Get
				Return New SqlParameter("@TranDate", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property AppYear As SqlParameter
			Get
				Return New SqlParameter("@AppYear", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Remarks As SqlParameter
			Get
				Return New SqlParameter("@Remarks", SqlDbType.VarChar, 150)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.DateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const OffsetEarnID As String = "OffsetEarnID"
        Public Const CompanyID As String = "CompanyID"
        Public Const TranDate As String = "TranDate"
        Public Const AppYear As String = "AppYear"
        Public Const Remarks As String = "Remarks"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(OffsetEarnID) = _OffsetEarn.PropertyNames.OffsetEarnID
				ht(CompanyID) = _OffsetEarn.PropertyNames.CompanyID
				ht(TranDate) = _OffsetEarn.PropertyNames.TranDate
				ht(AppYear) = _OffsetEarn.PropertyNames.AppYear
				ht(Remarks) = _OffsetEarn.PropertyNames.Remarks
				ht(CreatedBy) = _OffsetEarn.PropertyNames.CreatedBy
				ht(CreatedDate) = _OffsetEarn.PropertyNames.CreatedDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const OffsetEarnID As String = "OffsetEarnID"
        Public Const CompanyID As String = "CompanyID"
        Public Const TranDate As String = "TranDate"
        Public Const AppYear As String = "AppYear"
        Public Const Remarks As String = "Remarks"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(OffsetEarnID) = _OffsetEarn.ColumnNames.OffsetEarnID
				ht(CompanyID) = _OffsetEarn.ColumnNames.CompanyID
				ht(TranDate) = _OffsetEarn.ColumnNames.TranDate
				ht(AppYear) = _OffsetEarn.ColumnNames.AppYear
				ht(Remarks) = _OffsetEarn.ColumnNames.Remarks
				ht(CreatedBy) = _OffsetEarn.ColumnNames.CreatedBy
				ht(CreatedDate) = _OffsetEarn.ColumnNames.CreatedDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const OffsetEarnID As String = "s_OffsetEarnID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const TranDate As String = "s_TranDate"
        Public Const AppYear As String = "s_AppYear"
        Public Const Remarks As String = "s_Remarks"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property OffsetEarnID As Long
			Get
				Return MyBase.GetLong(ColumnNames.OffsetEarnID)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.OffsetEarnID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property TranDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.TranDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.TranDate, Value)
			End Set
		End Property

		Public Overridable Property AppYear As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.AppYear)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.AppYear, Value)
			End Set
		End Property

		Public Overridable Property Remarks As String
			Get
				Return MyBase.GetString(ColumnNames.Remarks)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Remarks, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_OffsetEarnID As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetEarnID) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.OffsetEarnID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetEarnID)
				Else
					Me.OffsetEarnID = MyBase.SetLongAsString(ColumnNames.OffsetEarnID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TranDate As String
			Get
				If Me.IsColumnNull(ColumnNames.TranDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.TranDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TranDate)
				Else
					Me.TranDate = MyBase.SetDateTimeAsString(ColumnNames.TranDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_AppYear As String
			Get
				If Me.IsColumnNull(ColumnNames.AppYear) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.AppYear)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.AppYear)
				Else
					Me.AppYear = MyBase.SetIntegerAsString(ColumnNames.AppYear, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Remarks As String
			Get
				If Me.IsColumnNull(ColumnNames.Remarks) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Remarks)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Remarks)
				Else
					Me.Remarks = MyBase.SetStringAsString(ColumnNames.Remarks, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property OffsetEarnID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetEarnID, Parameters.OffsetEarnID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TranDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TranDate, Parameters.TranDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property AppYear() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.AppYear, Parameters.AppYear)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Remarks() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Remarks, Parameters.Remarks)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property OffsetEarnID() As WhereParameter 
			Get
				If _OffsetEarnID_W Is Nothing Then
					_OffsetEarnID_W = TearOff.OffsetEarnID
				End If
				Return _OffsetEarnID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property TranDate() As WhereParameter 
			Get
				If _TranDate_W Is Nothing Then
					_TranDate_W = TearOff.TranDate
				End If
				Return _TranDate_W
			End Get
		End Property

		Public ReadOnly Property AppYear() As WhereParameter 
			Get
				If _AppYear_W Is Nothing Then
					_AppYear_W = TearOff.AppYear
				End If
				Return _AppYear_W
			End Get
		End Property

		Public ReadOnly Property Remarks() As WhereParameter 
			Get
				If _Remarks_W Is Nothing Then
					_Remarks_W = TearOff.Remarks
				End If
				Return _Remarks_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Private _OffsetEarnID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _TranDate_W As WhereParameter = Nothing
		Private _AppYear_W As WhereParameter = Nothing
		Private _Remarks_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_OffsetEarnID_W = Nothing
			_CompanyID_W = Nothing
			_TranDate_W = Nothing
			_AppYear_W = Nothing
			_Remarks_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property OffsetEarnID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetEarnID, Parameters.OffsetEarnID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TranDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TranDate, Parameters.TranDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property AppYear() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AppYear, Parameters.AppYear)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Remarks() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Remarks, Parameters.Remarks)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property OffsetEarnID() As AggregateParameter 
			Get
				If _OffsetEarnID_W Is Nothing Then
					_OffsetEarnID_W = TearOff.OffsetEarnID
				End If
				Return _OffsetEarnID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property TranDate() As AggregateParameter 
			Get
				If _TranDate_W Is Nothing Then
					_TranDate_W = TearOff.TranDate
				End If
				Return _TranDate_W
			End Get
		End Property

		Public ReadOnly Property AppYear() As AggregateParameter 
			Get
				If _AppYear_W Is Nothing Then
					_AppYear_W = TearOff.AppYear
				End If
				Return _AppYear_W
			End Get
		End Property

		Public ReadOnly Property Remarks() As AggregateParameter 
			Get
				If _Remarks_W Is Nothing Then
					_Remarks_W = TearOff.Remarks
				End If
				Return _Remarks_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Private _OffsetEarnID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _TranDate_W As AggregateParameter = Nothing
		Private _AppYear_W As AggregateParameter = Nothing
		Private _Remarks_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_OffsetEarnID_W = Nothing
		_CompanyID_W = Nothing
		_TranDate_W = Nothing
		_AppYear_W = Nothing
		_Remarks_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.OffsetEarnID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.OffsetEarnID)
		p.SourceColumn = ColumnNames.OffsetEarnID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.OffsetEarnID)
		p.SourceColumn = ColumnNames.OffsetEarnID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TranDate)
		p.SourceColumn = ColumnNames.TranDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.AppYear)
		p.SourceColumn = ColumnNames.AppYear
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Remarks)
		p.SourceColumn = ColumnNames.Remarks
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

