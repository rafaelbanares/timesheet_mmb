
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

Public MustInherit Class _OffsetApplication
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "OffsetApplication"
			Me.MappingName = "OffsetApplication"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetApplicationLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal OffsetAppID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_OffsetApplication.Parameters.OffsetAppID, OffsetAppID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetApplicationLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property OffsetAppID As SqlParameter
			Get
				Return New SqlParameter("@OffsetAppID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property OffsetDate As SqlParameter
			Get
				Return New SqlParameter("@OffsetDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Status As SqlParameter
			Get
				Return New SqlParameter("@Status", SqlDbType.VarChar, 12)
			End Get
		End Property
		
		Public Shared ReadOnly Property Remarks As SqlParameter
			Get
				Return New SqlParameter("@Remarks", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property TardyUT As SqlParameter
			Get
				Return New SqlParameter("@TardyUT", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OffsetUsed As SqlParameter
			Get
				Return New SqlParameter("@OffsetUsed", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const OffsetAppID As String = "OffsetAppID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const OffsetDate As String = "OffsetDate"
        Public Const Status As String = "Status"
        Public Const Remarks As String = "Remarks"
        Public Const TardyUT As String = "TardyUT"
        Public Const OffsetUsed As String = "OffsetUsed"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const LastUpdDate As String = "LastUpdDate"
        Public Const LastUpdBy As String = "LastUpdBy"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(OffsetAppID) = _OffsetApplication.PropertyNames.OffsetAppID
				ht(CompanyID) = _OffsetApplication.PropertyNames.CompanyID
				ht(EmployeeID) = _OffsetApplication.PropertyNames.EmployeeID
				ht(OffsetDate) = _OffsetApplication.PropertyNames.OffsetDate
				ht(Status) = _OffsetApplication.PropertyNames.Status
				ht(Remarks) = _OffsetApplication.PropertyNames.Remarks
				ht(TardyUT) = _OffsetApplication.PropertyNames.TardyUT
				ht(OffsetUsed) = _OffsetApplication.PropertyNames.OffsetUsed
				ht(CreatedDate) = _OffsetApplication.PropertyNames.CreatedDate
				ht(CreatedBy) = _OffsetApplication.PropertyNames.CreatedBy
				ht(LastUpdDate) = _OffsetApplication.PropertyNames.LastUpdDate
				ht(LastUpdBy) = _OffsetApplication.PropertyNames.LastUpdBy

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const OffsetAppID As String = "OffsetAppID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const OffsetDate As String = "OffsetDate"
        Public Const Status As String = "Status"
        Public Const Remarks As String = "Remarks"
        Public Const TardyUT As String = "TardyUT"
        Public Const OffsetUsed As String = "OffsetUsed"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const LastUpdDate As String = "LastUpdDate"
        Public Const LastUpdBy As String = "LastUpdBy"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(OffsetAppID) = _OffsetApplication.ColumnNames.OffsetAppID
				ht(CompanyID) = _OffsetApplication.ColumnNames.CompanyID
				ht(EmployeeID) = _OffsetApplication.ColumnNames.EmployeeID
				ht(OffsetDate) = _OffsetApplication.ColumnNames.OffsetDate
				ht(Status) = _OffsetApplication.ColumnNames.Status
				ht(Remarks) = _OffsetApplication.ColumnNames.Remarks
				ht(TardyUT) = _OffsetApplication.ColumnNames.TardyUT
				ht(OffsetUsed) = _OffsetApplication.ColumnNames.OffsetUsed
				ht(CreatedDate) = _OffsetApplication.ColumnNames.CreatedDate
				ht(CreatedBy) = _OffsetApplication.ColumnNames.CreatedBy
				ht(LastUpdDate) = _OffsetApplication.ColumnNames.LastUpdDate
				ht(LastUpdBy) = _OffsetApplication.ColumnNames.LastUpdBy

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const OffsetAppID As String = "s_OffsetAppID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const OffsetDate As String = "s_OffsetDate"
        Public Const Status As String = "s_Status"
        Public Const Remarks As String = "s_Remarks"
        Public Const TardyUT As String = "s_TardyUT"
        Public Const OffsetUsed As String = "s_OffsetUsed"
        Public Const CreatedDate As String = "s_CreatedDate"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"
        Public Const LastUpdBy As String = "s_LastUpdBy"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property OffsetAppID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.OffsetAppID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.OffsetAppID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property OffsetDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.OffsetDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.OffsetDate, Value)
			End Set
		End Property

		Public Overridable Property Status As String
			Get
				Return MyBase.GetString(ColumnNames.Status)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Status, Value)
			End Set
		End Property

		Public Overridable Property Remarks As String
			Get
				Return MyBase.GetString(ColumnNames.Remarks)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Remarks, Value)
			End Set
		End Property

		Public Overridable Property TardyUT As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.TardyUT)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.TardyUT, Value)
			End Set
		End Property

		Public Overridable Property OffsetUsed As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.OffsetUsed)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.OffsetUsed, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_OffsetAppID As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetAppID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.OffsetAppID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetAppID)
				Else
					Me.OffsetAppID = MyBase.SetIntegerAsString(ColumnNames.OffsetAppID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OffsetDate As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.OffsetDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetDate)
				Else
					Me.OffsetDate = MyBase.SetDateTimeAsString(ColumnNames.OffsetDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Status As String
			Get
				If Me.IsColumnNull(ColumnNames.Status) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Status)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Status)
				Else
					Me.Status = MyBase.SetStringAsString(ColumnNames.Status, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Remarks As String
			Get
				If Me.IsColumnNull(ColumnNames.Remarks) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Remarks)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Remarks)
				Else
					Me.Remarks = MyBase.SetStringAsString(ColumnNames.Remarks, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TardyUT As String
			Get
				If Me.IsColumnNull(ColumnNames.TardyUT) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.TardyUT)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TardyUT)
				Else
					Me.TardyUT = MyBase.SetIntegerAsString(ColumnNames.TardyUT, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OffsetUsed As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetUsed) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.OffsetUsed)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetUsed)
				Else
					Me.OffsetUsed = MyBase.SetIntegerAsString(ColumnNames.OffsetUsed, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property OffsetAppID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetAppID, Parameters.OffsetAppID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OffsetDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetDate, Parameters.OffsetDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Status() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Status, Parameters.Status)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Remarks() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Remarks, Parameters.Remarks)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TardyUT() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TardyUT, Parameters.TardyUT)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OffsetUsed() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetUsed, Parameters.OffsetUsed)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property OffsetAppID() As WhereParameter 
			Get
				If _OffsetAppID_W Is Nothing Then
					_OffsetAppID_W = TearOff.OffsetAppID
				End If
				Return _OffsetAppID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property OffsetDate() As WhereParameter 
			Get
				If _OffsetDate_W Is Nothing Then
					_OffsetDate_W = TearOff.OffsetDate
				End If
				Return _OffsetDate_W
			End Get
		End Property

		Public ReadOnly Property Status() As WhereParameter 
			Get
				If _Status_W Is Nothing Then
					_Status_W = TearOff.Status
				End If
				Return _Status_W
			End Get
		End Property

		Public ReadOnly Property Remarks() As WhereParameter 
			Get
				If _Remarks_W Is Nothing Then
					_Remarks_W = TearOff.Remarks
				End If
				Return _Remarks_W
			End Get
		End Property

		Public ReadOnly Property TardyUT() As WhereParameter 
			Get
				If _TardyUT_W Is Nothing Then
					_TardyUT_W = TearOff.TardyUT
				End If
				Return _TardyUT_W
			End Get
		End Property

		Public ReadOnly Property OffsetUsed() As WhereParameter 
			Get
				If _OffsetUsed_W Is Nothing Then
					_OffsetUsed_W = TearOff.OffsetUsed
				End If
				Return _OffsetUsed_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Private _OffsetAppID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _OffsetDate_W As WhereParameter = Nothing
		Private _Status_W As WhereParameter = Nothing
		Private _Remarks_W As WhereParameter = Nothing
		Private _TardyUT_W As WhereParameter = Nothing
		Private _OffsetUsed_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_OffsetAppID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_OffsetDate_W = Nothing
			_Status_W = Nothing
			_Remarks_W = Nothing
			_TardyUT_W = Nothing
			_OffsetUsed_W = Nothing
			_CreatedDate_W = Nothing
			_CreatedBy_W = Nothing
			_LastUpdDate_W = Nothing
			_LastUpdBy_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property OffsetAppID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetAppID, Parameters.OffsetAppID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OffsetDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetDate, Parameters.OffsetDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Status() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Status, Parameters.Status)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Remarks() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Remarks, Parameters.Remarks)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TardyUT() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TardyUT, Parameters.TardyUT)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OffsetUsed() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetUsed, Parameters.OffsetUsed)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property OffsetAppID() As AggregateParameter 
			Get
				If _OffsetAppID_W Is Nothing Then
					_OffsetAppID_W = TearOff.OffsetAppID
				End If
				Return _OffsetAppID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property OffsetDate() As AggregateParameter 
			Get
				If _OffsetDate_W Is Nothing Then
					_OffsetDate_W = TearOff.OffsetDate
				End If
				Return _OffsetDate_W
			End Get
		End Property

		Public ReadOnly Property Status() As AggregateParameter 
			Get
				If _Status_W Is Nothing Then
					_Status_W = TearOff.Status
				End If
				Return _Status_W
			End Get
		End Property

		Public ReadOnly Property Remarks() As AggregateParameter 
			Get
				If _Remarks_W Is Nothing Then
					_Remarks_W = TearOff.Remarks
				End If
				Return _Remarks_W
			End Get
		End Property

		Public ReadOnly Property TardyUT() As AggregateParameter 
			Get
				If _TardyUT_W Is Nothing Then
					_TardyUT_W = TearOff.TardyUT
				End If
				Return _TardyUT_W
			End Get
		End Property

		Public ReadOnly Property OffsetUsed() As AggregateParameter 
			Get
				If _OffsetUsed_W Is Nothing Then
					_OffsetUsed_W = TearOff.OffsetUsed
				End If
				Return _OffsetUsed_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Private _OffsetAppID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _OffsetDate_W As AggregateParameter = Nothing
		Private _Status_W As AggregateParameter = Nothing
		Private _Remarks_W As AggregateParameter = Nothing
		Private _TardyUT_W As AggregateParameter = Nothing
		Private _OffsetUsed_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_OffsetAppID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_OffsetDate_W = Nothing
		_Status_W = Nothing
		_Remarks_W = Nothing
		_TardyUT_W = Nothing
		_OffsetUsed_W = Nothing
		_CreatedDate_W = Nothing
		_CreatedBy_W = Nothing
		_LastUpdDate_W = Nothing
		_LastUpdBy_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetApplicationInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.OffsetAppID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetApplicationUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetApplicationDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.OffsetAppID)
		p.SourceColumn = ColumnNames.OffsetAppID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.OffsetAppID)
		p.SourceColumn = ColumnNames.OffsetAppID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OffsetDate)
		p.SourceColumn = ColumnNames.OffsetDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Status)
		p.SourceColumn = ColumnNames.Status
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Remarks)
		p.SourceColumn = ColumnNames.Remarks
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TardyUT)
		p.SourceColumn = ColumnNames.TardyUT
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OffsetUsed)
		p.SourceColumn = ColumnNames.OffsetUsed
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

