
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _TimeTransError
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "TimeTransError"
			Me.MappingName = "TimeTransError"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TimeTransErrorLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_TimeTransError.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TimeTransErrorLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property SwipeID As SqlParameter
			Get
				Return New SqlParameter("@SwipeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property TimeIO As SqlParameter
			Get
				Return New SqlParameter("@TimeIO", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property IO As SqlParameter
			Get
				Return New SqlParameter("@IO", SqlDbType.Char, 1)
			End Get
		End Property
		
		Public Shared ReadOnly Property Station As SqlParameter
			Get
				Return New SqlParameter("@Station", SqlDbType.Char, 3)
			End Get
		End Property
		
		Public Shared ReadOnly Property Posted As SqlParameter
			Get
				Return New SqlParameter("@Posted", SqlDbType.Bit, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const SwipeID As String = "SwipeID"
        Public Const TimeIO As String = "TimeIO"
        Public Const IO As String = "IO"
        Public Const Station As String = "Station"
        Public Const Posted As String = "Posted"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _TimeTransError.PropertyNames.ID
				ht(CompanyID) = _TimeTransError.PropertyNames.CompanyID
				ht(SwipeID) = _TimeTransError.PropertyNames.SwipeID
				ht(TimeIO) = _TimeTransError.PropertyNames.TimeIO
				ht(IO) = _TimeTransError.PropertyNames.IO
				ht(Station) = _TimeTransError.PropertyNames.Station
				ht(Posted) = _TimeTransError.PropertyNames.Posted

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const SwipeID As String = "SwipeID"
        Public Const TimeIO As String = "TimeIO"
        Public Const IO As String = "IO"
        Public Const Station As String = "Station"
        Public Const Posted As String = "Posted"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _TimeTransError.ColumnNames.ID
				ht(CompanyID) = _TimeTransError.ColumnNames.CompanyID
				ht(SwipeID) = _TimeTransError.ColumnNames.SwipeID
				ht(TimeIO) = _TimeTransError.ColumnNames.TimeIO
				ht(IO) = _TimeTransError.ColumnNames.IO
				ht(Station) = _TimeTransError.ColumnNames.Station
				ht(Posted) = _TimeTransError.ColumnNames.Posted

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const SwipeID As String = "s_SwipeID"
        Public Const TimeIO As String = "s_TimeIO"
        Public Const IO As String = "s_IO"
        Public Const Station As String = "s_Station"
        Public Const Posted As String = "s_Posted"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property SwipeID As String
			Get
				Return MyBase.GetString(ColumnNames.SwipeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.SwipeID, Value)
			End Set
		End Property

		Public Overridable Property TimeIO As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.TimeIO)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.TimeIO, Value)
			End Set
		End Property

		Public Overridable Property IO As String
			Get
				Return MyBase.GetString(ColumnNames.IO)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.IO, Value)
			End Set
		End Property

		Public Overridable Property Station As String
			Get
				Return MyBase.GetString(ColumnNames.Station)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Station, Value)
			End Set
		End Property

		Public Overridable Property Posted As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.Posted)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.Posted, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_SwipeID As String
			Get
				If Me.IsColumnNull(ColumnNames.SwipeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.SwipeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.SwipeID)
				Else
					Me.SwipeID = MyBase.SetStringAsString(ColumnNames.SwipeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TimeIO As String
			Get
				If Me.IsColumnNull(ColumnNames.TimeIO) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.TimeIO)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TimeIO)
				Else
					Me.TimeIO = MyBase.SetDateTimeAsString(ColumnNames.TimeIO, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_IO As String
			Get
				If Me.IsColumnNull(ColumnNames.IO) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.IO)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.IO)
				Else
					Me.IO = MyBase.SetStringAsString(ColumnNames.IO, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Station As String
			Get
				If Me.IsColumnNull(ColumnNames.Station) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Station)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Station)
				Else
					Me.Station = MyBase.SetStringAsString(ColumnNames.Station, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Posted As String
			Get
				If Me.IsColumnNull(ColumnNames.Posted) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.Posted)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Posted)
				Else
					Me.Posted = MyBase.SetBooleanAsString(ColumnNames.Posted, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property SwipeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.SwipeID, Parameters.SwipeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TimeIO() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TimeIO, Parameters.TimeIO)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property IO() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.IO, Parameters.IO)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Station() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Station, Parameters.Station)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Posted() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Posted, Parameters.Posted)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property SwipeID() As WhereParameter 
			Get
				If _SwipeID_W Is Nothing Then
					_SwipeID_W = TearOff.SwipeID
				End If
				Return _SwipeID_W
			End Get
		End Property

		Public ReadOnly Property TimeIO() As WhereParameter 
			Get
				If _TimeIO_W Is Nothing Then
					_TimeIO_W = TearOff.TimeIO
				End If
				Return _TimeIO_W
			End Get
		End Property

		Public ReadOnly Property IO() As WhereParameter 
			Get
				If _IO_W Is Nothing Then
					_IO_W = TearOff.IO
				End If
				Return _IO_W
			End Get
		End Property

		Public ReadOnly Property Station() As WhereParameter 
			Get
				If _Station_W Is Nothing Then
					_Station_W = TearOff.Station
				End If
				Return _Station_W
			End Get
		End Property

		Public ReadOnly Property Posted() As WhereParameter 
			Get
				If _Posted_W Is Nothing Then
					_Posted_W = TearOff.Posted
				End If
				Return _Posted_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _SwipeID_W As WhereParameter = Nothing
		Private _TimeIO_W As WhereParameter = Nothing
		Private _IO_W As WhereParameter = Nothing
		Private _Station_W As WhereParameter = Nothing
		Private _Posted_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_SwipeID_W = Nothing
			_TimeIO_W = Nothing
			_IO_W = Nothing
			_Station_W = Nothing
			_Posted_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property SwipeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.SwipeID, Parameters.SwipeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TimeIO() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TimeIO, Parameters.TimeIO)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property IO() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.IO, Parameters.IO)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Station() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Station, Parameters.Station)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Posted() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Posted, Parameters.Posted)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property SwipeID() As AggregateParameter 
			Get
				If _SwipeID_W Is Nothing Then
					_SwipeID_W = TearOff.SwipeID
				End If
				Return _SwipeID_W
			End Get
		End Property

		Public ReadOnly Property TimeIO() As AggregateParameter 
			Get
				If _TimeIO_W Is Nothing Then
					_TimeIO_W = TearOff.TimeIO
				End If
				Return _TimeIO_W
			End Get
		End Property

		Public ReadOnly Property IO() As AggregateParameter 
			Get
				If _IO_W Is Nothing Then
					_IO_W = TearOff.IO
				End If
				Return _IO_W
			End Get
		End Property

		Public ReadOnly Property Station() As AggregateParameter 
			Get
				If _Station_W Is Nothing Then
					_Station_W = TearOff.Station
				End If
				Return _Station_W
			End Get
		End Property

		Public ReadOnly Property Posted() As AggregateParameter 
			Get
				If _Posted_W Is Nothing Then
					_Posted_W = TearOff.Posted
				End If
				Return _Posted_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _SwipeID_W As AggregateParameter = Nothing
		Private _TimeIO_W As AggregateParameter = Nothing
		Private _IO_W As AggregateParameter = Nothing
		Private _Station_W As AggregateParameter = Nothing
		Private _Posted_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_SwipeID_W = Nothing
		_TimeIO_W = Nothing
		_IO_W = Nothing
		_Station_W = Nothing
		_Posted_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeTransErrorInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeTransErrorUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeTransErrorDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.SwipeID)
		p.SourceColumn = ColumnNames.SwipeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TimeIO)
		p.SourceColumn = ColumnNames.TimeIO
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.IO)
		p.SourceColumn = ColumnNames.IO
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Station)
		p.SourceColumn = ColumnNames.Station
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Posted)
		p.SourceColumn = ColumnNames.Posted
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

