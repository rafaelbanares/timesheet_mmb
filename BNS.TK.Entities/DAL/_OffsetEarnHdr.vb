
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

Public MustInherit Class _OffsetEarnHdr
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "OffsetEarnHdr"
			Me.MappingName = "OffsetEarnHdr"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetEarnHdrLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal OffsetEarnHdrID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_OffsetEarnHdr.Parameters.OffsetEarnHdrID, OffsetEarnHdrID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OffsetEarnHdrLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property OffsetEarnHdrID As SqlParameter
			Get
				Return New SqlParameter("@OffsetEarnHdrID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property Scope As SqlParameter
			Get
				Return New SqlParameter("@Scope", SqlDbType.VarChar, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property ScopeValue As SqlParameter
			Get
				Return New SqlParameter("@ScopeValue", SqlDbType.VarChar, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property OffsetMins As SqlParameter
			Get
				Return New SqlParameter("@OffsetMins", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Remarks As SqlParameter
			Get
				Return New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const OffsetEarnHdrID As String = "OffsetEarnHdrID"
        Public Const CompanyID As String = "CompanyID"
        Public Const Scope As String = "Scope"
        Public Const ScopeValue As String = "ScopeValue"
        Public Const OffsetMins As String = "OffsetMins"
        Public Const Remarks As String = "Remarks"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(OffsetEarnHdrID) = _OffsetEarnHdr.PropertyNames.OffsetEarnHdrID
				ht(CompanyID) = _OffsetEarnHdr.PropertyNames.CompanyID
				ht(Scope) = _OffsetEarnHdr.PropertyNames.Scope
				ht(ScopeValue) = _OffsetEarnHdr.PropertyNames.ScopeValue
				ht(OffsetMins) = _OffsetEarnHdr.PropertyNames.OffsetMins
				ht(Remarks) = _OffsetEarnHdr.PropertyNames.Remarks
				ht(CreatedBy) = _OffsetEarnHdr.PropertyNames.CreatedBy
				ht(CreatedDate) = _OffsetEarnHdr.PropertyNames.CreatedDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const OffsetEarnHdrID As String = "OffsetEarnHdrID"
        Public Const CompanyID As String = "CompanyID"
        Public Const Scope As String = "Scope"
        Public Const ScopeValue As String = "ScopeValue"
        Public Const OffsetMins As String = "OffsetMins"
        Public Const Remarks As String = "Remarks"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(OffsetEarnHdrID) = _OffsetEarnHdr.ColumnNames.OffsetEarnHdrID
				ht(CompanyID) = _OffsetEarnHdr.ColumnNames.CompanyID
				ht(Scope) = _OffsetEarnHdr.ColumnNames.Scope
				ht(ScopeValue) = _OffsetEarnHdr.ColumnNames.ScopeValue
				ht(OffsetMins) = _OffsetEarnHdr.ColumnNames.OffsetMins
				ht(Remarks) = _OffsetEarnHdr.ColumnNames.Remarks
				ht(CreatedBy) = _OffsetEarnHdr.ColumnNames.CreatedBy
				ht(CreatedDate) = _OffsetEarnHdr.ColumnNames.CreatedDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const OffsetEarnHdrID As String = "s_OffsetEarnHdrID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const Scope As String = "s_Scope"
        Public Const ScopeValue As String = "s_ScopeValue"
        Public Const OffsetMins As String = "s_OffsetMins"
        Public Const Remarks As String = "s_Remarks"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property OffsetEarnHdrID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.OffsetEarnHdrID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.OffsetEarnHdrID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property Scope As String
			Get
				Return MyBase.GetString(ColumnNames.Scope)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Scope, Value)
			End Set
		End Property

		Public Overridable Property ScopeValue As String
			Get
				Return MyBase.GetString(ColumnNames.ScopeValue)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ScopeValue, Value)
			End Set
		End Property

		Public Overridable Property OffsetMins As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.OffsetMins)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.OffsetMins, Value)
			End Set
		End Property

		Public Overridable Property Remarks As String
			Get
				Return MyBase.GetString(ColumnNames.Remarks)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Remarks, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_OffsetEarnHdrID As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetEarnHdrID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.OffsetEarnHdrID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetEarnHdrID)
				Else
					Me.OffsetEarnHdrID = MyBase.SetIntegerAsString(ColumnNames.OffsetEarnHdrID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Scope As String
			Get
				If Me.IsColumnNull(ColumnNames.Scope) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Scope)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Scope)
				Else
					Me.Scope = MyBase.SetStringAsString(ColumnNames.Scope, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ScopeValue As String
			Get
				If Me.IsColumnNull(ColumnNames.ScopeValue) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ScopeValue)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ScopeValue)
				Else
					Me.ScopeValue = MyBase.SetStringAsString(ColumnNames.ScopeValue, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OffsetMins As String
			Get
				If Me.IsColumnNull(ColumnNames.OffsetMins) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.OffsetMins)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OffsetMins)
				Else
					Me.OffsetMins = MyBase.SetIntegerAsString(ColumnNames.OffsetMins, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Remarks As String
			Get
				If Me.IsColumnNull(ColumnNames.Remarks) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Remarks)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Remarks)
				Else
					Me.Remarks = MyBase.SetStringAsString(ColumnNames.Remarks, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property OffsetEarnHdrID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetEarnHdrID, Parameters.OffsetEarnHdrID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Scope() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Scope, Parameters.Scope)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ScopeValue() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ScopeValue, Parameters.ScopeValue)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OffsetMins() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OffsetMins, Parameters.OffsetMins)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Remarks() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Remarks, Parameters.Remarks)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property OffsetEarnHdrID() As WhereParameter 
			Get
				If _OffsetEarnHdrID_W Is Nothing Then
					_OffsetEarnHdrID_W = TearOff.OffsetEarnHdrID
				End If
				Return _OffsetEarnHdrID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property Scope() As WhereParameter 
			Get
				If _Scope_W Is Nothing Then
					_Scope_W = TearOff.Scope
				End If
				Return _Scope_W
			End Get
		End Property

		Public ReadOnly Property ScopeValue() As WhereParameter 
			Get
				If _ScopeValue_W Is Nothing Then
					_ScopeValue_W = TearOff.ScopeValue
				End If
				Return _ScopeValue_W
			End Get
		End Property

		Public ReadOnly Property OffsetMins() As WhereParameter 
			Get
				If _OffsetMins_W Is Nothing Then
					_OffsetMins_W = TearOff.OffsetMins
				End If
				Return _OffsetMins_W
			End Get
		End Property

		Public ReadOnly Property Remarks() As WhereParameter 
			Get
				If _Remarks_W Is Nothing Then
					_Remarks_W = TearOff.Remarks
				End If
				Return _Remarks_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Private _OffsetEarnHdrID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _Scope_W As WhereParameter = Nothing
		Private _ScopeValue_W As WhereParameter = Nothing
		Private _OffsetMins_W As WhereParameter = Nothing
		Private _Remarks_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_OffsetEarnHdrID_W = Nothing
			_CompanyID_W = Nothing
			_Scope_W = Nothing
			_ScopeValue_W = Nothing
			_OffsetMins_W = Nothing
			_Remarks_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property OffsetEarnHdrID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetEarnHdrID, Parameters.OffsetEarnHdrID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Scope() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Scope, Parameters.Scope)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ScopeValue() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ScopeValue, Parameters.ScopeValue)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OffsetMins() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OffsetMins, Parameters.OffsetMins)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Remarks() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Remarks, Parameters.Remarks)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property OffsetEarnHdrID() As AggregateParameter 
			Get
				If _OffsetEarnHdrID_W Is Nothing Then
					_OffsetEarnHdrID_W = TearOff.OffsetEarnHdrID
				End If
				Return _OffsetEarnHdrID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property Scope() As AggregateParameter 
			Get
				If _Scope_W Is Nothing Then
					_Scope_W = TearOff.Scope
				End If
				Return _Scope_W
			End Get
		End Property

		Public ReadOnly Property ScopeValue() As AggregateParameter 
			Get
				If _ScopeValue_W Is Nothing Then
					_ScopeValue_W = TearOff.ScopeValue
				End If
				Return _ScopeValue_W
			End Get
		End Property

		Public ReadOnly Property OffsetMins() As AggregateParameter 
			Get
				If _OffsetMins_W Is Nothing Then
					_OffsetMins_W = TearOff.OffsetMins
				End If
				Return _OffsetMins_W
			End Get
		End Property

		Public ReadOnly Property Remarks() As AggregateParameter 
			Get
				If _Remarks_W Is Nothing Then
					_Remarks_W = TearOff.Remarks
				End If
				Return _Remarks_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Private _OffsetEarnHdrID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _Scope_W As AggregateParameter = Nothing
		Private _ScopeValue_W As AggregateParameter = Nothing
		Private _OffsetMins_W As AggregateParameter = Nothing
		Private _Remarks_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_OffsetEarnHdrID_W = Nothing
		_CompanyID_W = Nothing
		_Scope_W = Nothing
		_ScopeValue_W = Nothing
		_OffsetMins_W = Nothing
		_Remarks_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnHdrInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.OffsetEarnHdrID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnHdrUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OffsetEarnHdrDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.OffsetEarnHdrID)
		p.SourceColumn = ColumnNames.OffsetEarnHdrID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.OffsetEarnHdrID)
		p.SourceColumn = ColumnNames.OffsetEarnHdrID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Scope)
		p.SourceColumn = ColumnNames.Scope
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ScopeValue)
		p.SourceColumn = ColumnNames.ScopeValue
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OffsetMins)
		p.SourceColumn = ColumnNames.OffsetMins
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Remarks)
		p.SourceColumn = ColumnNames.Remarks
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

