
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _DepartmentShift
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "DepartmentShift"
			Me.MappingName = "DepartmentShift"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_DepartmentShiftLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal Deptcode As String) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_DepartmentShift.Parameters.CompanyID, CompanyID)
		parameters.Add(_DepartmentShift.Parameters.Deptcode, Deptcode)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_DepartmentShiftLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property Deptcode As SqlParameter
			Get
				Return New SqlParameter("@Deptcode", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Shift_in As SqlParameter
			Get
				Return New SqlParameter("@Shift_in", SqlDbType.VarChar, 8)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const CompanyID As String = "CompanyID"
        Public Const Deptcode As String = "deptcode"
        Public Const Shift_in As String = "shift_in"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(CompanyID) = _DepartmentShift.PropertyNames.CompanyID
				ht(Deptcode) = _DepartmentShift.PropertyNames.Deptcode
				ht(Shift_in) = _DepartmentShift.PropertyNames.Shift_in

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const CompanyID As String = "CompanyID"
        Public Const Deptcode As String = "Deptcode"
        Public Const Shift_in As String = "Shift_in"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(CompanyID) = _DepartmentShift.ColumnNames.CompanyID
				ht(Deptcode) = _DepartmentShift.ColumnNames.Deptcode
				ht(Shift_in) = _DepartmentShift.ColumnNames.Shift_in

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const CompanyID As String = "s_CompanyID"
        Public Const Deptcode As String = "s_Deptcode"
        Public Const Shift_in As String = "s_Shift_in"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property Deptcode As String
			Get
				Return MyBase.GetString(ColumnNames.Deptcode)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Deptcode, Value)
			End Set
		End Property

		Public Overridable Property Shift_in As String
			Get
				Return MyBase.GetString(ColumnNames.Shift_in)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Shift_in, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Deptcode As String
			Get
				If Me.IsColumnNull(ColumnNames.Deptcode) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Deptcode)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Deptcode)
				Else
					Me.Deptcode = MyBase.SetStringAsString(ColumnNames.Deptcode, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Shift_in As String
			Get
				If Me.IsColumnNull(ColumnNames.Shift_in) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Shift_in)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Shift_in)
				Else
					Me.Shift_in = MyBase.SetStringAsString(ColumnNames.Shift_in, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Deptcode() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Deptcode, Parameters.Deptcode)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Shift_in() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Shift_in, Parameters.Shift_in)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property Deptcode() As WhereParameter 
			Get
				If _Deptcode_W Is Nothing Then
					_Deptcode_W = TearOff.Deptcode
				End If
				Return _Deptcode_W
			End Get
		End Property

		Public ReadOnly Property Shift_in() As WhereParameter 
			Get
				If _Shift_in_W Is Nothing Then
					_Shift_in_W = TearOff.Shift_in
				End If
				Return _Shift_in_W
			End Get
		End Property

		Private _CompanyID_W As WhereParameter = Nothing
		Private _Deptcode_W As WhereParameter = Nothing
		Private _Shift_in_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_CompanyID_W = Nothing
			_Deptcode_W = Nothing
			_Shift_in_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Deptcode() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Deptcode, Parameters.Deptcode)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Shift_in() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Shift_in, Parameters.Shift_in)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property Deptcode() As AggregateParameter 
			Get
				If _Deptcode_W Is Nothing Then
					_Deptcode_W = TearOff.Deptcode
				End If
				Return _Deptcode_W
			End Get
		End Property

		Public ReadOnly Property Shift_in() As AggregateParameter 
			Get
				If _Shift_in_W Is Nothing Then
					_Shift_in_W = TearOff.Shift_in
				End If
				Return _Shift_in_W
			End Get
		End Property

		Private _CompanyID_W As AggregateParameter = Nothing
		Private _Deptcode_W As AggregateParameter = Nothing
		Private _Shift_in_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_CompanyID_W = Nothing
		_Deptcode_W = Nothing
		_Shift_in_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_DepartmentShiftInsert]" 
	    
		CreateParameters(cmd)
		    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_DepartmentShiftUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_DepartmentShiftDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Deptcode)
		p.SourceColumn = ColumnNames.Deptcode
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Deptcode)
		p.SourceColumn = ColumnNames.Deptcode
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Shift_in)
		p.SourceColumn = ColumnNames.Shift_in
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

