
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _SummOT
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "SummOT"
			Me.MappingName = "SummOT"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_SummOTLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_SummOT.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_SummOTLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayStart As SqlParameter
			Get
				Return New SqlParameter("@PayStart", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayEnd As SqlParameter
			Get
				Return New SqlParameter("@PayEnd", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TransStart As SqlParameter
			Get
				Return New SqlParameter("@TransStart", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TransEnd As SqlParameter
			Get
				Return New SqlParameter("@TransEnd", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OTCode As SqlParameter
			Get
				Return New SqlParameter("@OTCode", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property OTHours As SqlParameter
			Get
				Return New SqlParameter("@OTHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OT8Hours As SqlParameter
			Get
				Return New SqlParameter("@OT8Hours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OTNDHours As SqlParameter
			Get
				Return New SqlParameter("@OTNDHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OTND2Hours As SqlParameter
			Get
				Return New SqlParameter("@OTND2Hours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ReOpen As SqlParameter
			Get
				Return New SqlParameter("@ReOpen", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const PayStart As String = "PayStart"
        Public Const PayEnd As String = "PayEnd"
        Public Const TransStart As String = "TransStart"
        Public Const TransEnd As String = "TransEnd"
        Public Const OTCode As String = "OTCode"
        Public Const OTHours As String = "OTHours"
        Public Const OT8Hours As String = "OT8Hours"
        Public Const OTNDHours As String = "OTNDHours"
        Public Const OTND2Hours As String = "OTND2Hours"
        Public Const ReOpen As String = "ReOpen"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpDate As String = "LastUpDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _SummOT.PropertyNames.ID
				ht(CompanyID) = _SummOT.PropertyNames.CompanyID
				ht(EmployeeID) = _SummOT.PropertyNames.EmployeeID
				ht(PayStart) = _SummOT.PropertyNames.PayStart
				ht(PayEnd) = _SummOT.PropertyNames.PayEnd
				ht(TransStart) = _SummOT.PropertyNames.TransStart
				ht(TransEnd) = _SummOT.PropertyNames.TransEnd
				ht(OTCode) = _SummOT.PropertyNames.OTCode
				ht(OTHours) = _SummOT.PropertyNames.OTHours
				ht(OT8Hours) = _SummOT.PropertyNames.OT8Hours
				ht(OTNDHours) = _SummOT.PropertyNames.OTNDHours
				ht(OTND2Hours) = _SummOT.PropertyNames.OTND2Hours
				ht(ReOpen) = _SummOT.PropertyNames.ReOpen
				ht(CreatedBy) = _SummOT.PropertyNames.CreatedBy
				ht(CreatedDate) = _SummOT.PropertyNames.CreatedDate
				ht(LastUpdBy) = _SummOT.PropertyNames.LastUpdBy
				ht(LastUpDate) = _SummOT.PropertyNames.LastUpDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const PayStart As String = "PayStart"
        Public Const PayEnd As String = "PayEnd"
        Public Const TransStart As String = "TransStart"
        Public Const TransEnd As String = "TransEnd"
        Public Const OTCode As String = "OTCode"
        Public Const OTHours As String = "OTHours"
        Public Const OT8Hours As String = "OT8Hours"
        Public Const OTNDHours As String = "OTNDHours"
        Public Const OTND2Hours As String = "OTND2Hours"
        Public Const ReOpen As String = "ReOpen"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpDate As String = "LastUpDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _SummOT.ColumnNames.ID
				ht(CompanyID) = _SummOT.ColumnNames.CompanyID
				ht(EmployeeID) = _SummOT.ColumnNames.EmployeeID
				ht(PayStart) = _SummOT.ColumnNames.PayStart
				ht(PayEnd) = _SummOT.ColumnNames.PayEnd
				ht(TransStart) = _SummOT.ColumnNames.TransStart
				ht(TransEnd) = _SummOT.ColumnNames.TransEnd
				ht(OTCode) = _SummOT.ColumnNames.OTCode
				ht(OTHours) = _SummOT.ColumnNames.OTHours
				ht(OT8Hours) = _SummOT.ColumnNames.OT8Hours
				ht(OTNDHours) = _SummOT.ColumnNames.OTNDHours
				ht(OTND2Hours) = _SummOT.ColumnNames.OTND2Hours
				ht(ReOpen) = _SummOT.ColumnNames.ReOpen
				ht(CreatedBy) = _SummOT.ColumnNames.CreatedBy
				ht(CreatedDate) = _SummOT.ColumnNames.CreatedDate
				ht(LastUpdBy) = _SummOT.ColumnNames.LastUpdBy
				ht(LastUpDate) = _SummOT.ColumnNames.LastUpDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const PayStart As String = "s_PayStart"
        Public Const PayEnd As String = "s_PayEnd"
        Public Const TransStart As String = "s_TransStart"
        Public Const TransEnd As String = "s_TransEnd"
        Public Const OTCode As String = "s_OTCode"
        Public Const OTHours As String = "s_OTHours"
        Public Const OT8Hours As String = "s_OT8Hours"
        Public Const OTNDHours As String = "s_OTNDHours"
        Public Const OTND2Hours As String = "s_OTND2Hours"
        Public Const ReOpen As String = "s_ReOpen"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpDate As String = "s_LastUpDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property PayStart As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PayStart)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PayStart, Value)
			End Set
		End Property

		Public Overridable Property PayEnd As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PayEnd)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PayEnd, Value)
			End Set
		End Property

		Public Overridable Property TransStart As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.TransStart)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.TransStart, Value)
			End Set
		End Property

		Public Overridable Property TransEnd As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.TransEnd)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.TransEnd, Value)
			End Set
		End Property

		Public Overridable Property OTCode As String
			Get
				Return MyBase.GetString(ColumnNames.OTCode)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.OTCode, Value)
			End Set
		End Property

		Public Overridable Property OTHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.OTHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.OTHours, Value)
			End Set
		End Property

		Public Overridable Property OT8Hours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.OT8Hours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.OT8Hours, Value)
			End Set
		End Property

		Public Overridable Property OTNDHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.OTNDHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.OTNDHours, Value)
			End Set
		End Property

		Public Overridable Property OTND2Hours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.OTND2Hours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.OTND2Hours, Value)
			End Set
		End Property

		Public Overridable Property ReOpen As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.ReOpen)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.ReOpen, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayStart As String
			Get
				If Me.IsColumnNull(ColumnNames.PayStart) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PayStart)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayStart)
				Else
					Me.PayStart = MyBase.SetDateTimeAsString(ColumnNames.PayStart, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayEnd As String
			Get
				If Me.IsColumnNull(ColumnNames.PayEnd) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PayEnd)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayEnd)
				Else
					Me.PayEnd = MyBase.SetDateTimeAsString(ColumnNames.PayEnd, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TransStart As String
			Get
				If Me.IsColumnNull(ColumnNames.TransStart) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.TransStart)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TransStart)
				Else
					Me.TransStart = MyBase.SetDateTimeAsString(ColumnNames.TransStart, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TransEnd As String
			Get
				If Me.IsColumnNull(ColumnNames.TransEnd) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.TransEnd)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TransEnd)
				Else
					Me.TransEnd = MyBase.SetDateTimeAsString(ColumnNames.TransEnd, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OTCode As String
			Get
				If Me.IsColumnNull(ColumnNames.OTCode) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.OTCode)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OTCode)
				Else
					Me.OTCode = MyBase.SetStringAsString(ColumnNames.OTCode, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OTHours As String
			Get
				If Me.IsColumnNull(ColumnNames.OTHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.OTHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OTHours)
				Else
					Me.OTHours = MyBase.SetDecimalAsString(ColumnNames.OTHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OT8Hours As String
			Get
				If Me.IsColumnNull(ColumnNames.OT8Hours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.OT8Hours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OT8Hours)
				Else
					Me.OT8Hours = MyBase.SetDecimalAsString(ColumnNames.OT8Hours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OTNDHours As String
			Get
				If Me.IsColumnNull(ColumnNames.OTNDHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.OTNDHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OTNDHours)
				Else
					Me.OTNDHours = MyBase.SetDecimalAsString(ColumnNames.OTNDHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OTND2Hours As String
			Get
				If Me.IsColumnNull(ColumnNames.OTND2Hours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.OTND2Hours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OTND2Hours)
				Else
					Me.OTND2Hours = MyBase.SetDecimalAsString(ColumnNames.OTND2Hours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ReOpen As String
			Get
				If Me.IsColumnNull(ColumnNames.ReOpen) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.ReOpen)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ReOpen)
				Else
					Me.ReOpen = MyBase.SetBooleanAsString(ColumnNames.ReOpen, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpDate)
				Else
					Me.LastUpDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayStart() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayStart, Parameters.PayStart)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayEnd() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayEnd, Parameters.PayEnd)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TransStart() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TransStart, Parameters.TransStart)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TransEnd() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TransEnd, Parameters.TransEnd)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OTCode() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OTCode, Parameters.OTCode)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OTHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OTHours, Parameters.OTHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OT8Hours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OT8Hours, Parameters.OT8Hours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OTNDHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OTNDHours, Parameters.OTNDHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OTND2Hours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OTND2Hours, Parameters.OTND2Hours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ReOpen() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ReOpen, Parameters.ReOpen)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpDate, Parameters.LastUpDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property PayStart() As WhereParameter 
			Get
				If _PayStart_W Is Nothing Then
					_PayStart_W = TearOff.PayStart
				End If
				Return _PayStart_W
			End Get
		End Property

		Public ReadOnly Property PayEnd() As WhereParameter 
			Get
				If _PayEnd_W Is Nothing Then
					_PayEnd_W = TearOff.PayEnd
				End If
				Return _PayEnd_W
			End Get
		End Property

		Public ReadOnly Property TransStart() As WhereParameter 
			Get
				If _TransStart_W Is Nothing Then
					_TransStart_W = TearOff.TransStart
				End If
				Return _TransStart_W
			End Get
		End Property

		Public ReadOnly Property TransEnd() As WhereParameter 
			Get
				If _TransEnd_W Is Nothing Then
					_TransEnd_W = TearOff.TransEnd
				End If
				Return _TransEnd_W
			End Get
		End Property

		Public ReadOnly Property OTCode() As WhereParameter 
			Get
				If _OTCode_W Is Nothing Then
					_OTCode_W = TearOff.OTCode
				End If
				Return _OTCode_W
			End Get
		End Property

		Public ReadOnly Property OTHours() As WhereParameter 
			Get
				If _OTHours_W Is Nothing Then
					_OTHours_W = TearOff.OTHours
				End If
				Return _OTHours_W
			End Get
		End Property

		Public ReadOnly Property OT8Hours() As WhereParameter 
			Get
				If _OT8Hours_W Is Nothing Then
					_OT8Hours_W = TearOff.OT8Hours
				End If
				Return _OT8Hours_W
			End Get
		End Property

		Public ReadOnly Property OTNDHours() As WhereParameter 
			Get
				If _OTNDHours_W Is Nothing Then
					_OTNDHours_W = TearOff.OTNDHours
				End If
				Return _OTNDHours_W
			End Get
		End Property

		Public ReadOnly Property OTND2Hours() As WhereParameter 
			Get
				If _OTND2Hours_W Is Nothing Then
					_OTND2Hours_W = TearOff.OTND2Hours
				End If
				Return _OTND2Hours_W
			End Get
		End Property

		Public ReadOnly Property ReOpen() As WhereParameter 
			Get
				If _ReOpen_W Is Nothing Then
					_ReOpen_W = TearOff.ReOpen
				End If
				Return _ReOpen_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpDate() As WhereParameter 
			Get
				If _LastUpDate_W Is Nothing Then
					_LastUpDate_W = TearOff.LastUpDate
				End If
				Return _LastUpDate_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _PayStart_W As WhereParameter = Nothing
		Private _PayEnd_W As WhereParameter = Nothing
		Private _TransStart_W As WhereParameter = Nothing
		Private _TransEnd_W As WhereParameter = Nothing
		Private _OTCode_W As WhereParameter = Nothing
		Private _OTHours_W As WhereParameter = Nothing
		Private _OT8Hours_W As WhereParameter = Nothing
		Private _OTNDHours_W As WhereParameter = Nothing
		Private _OTND2Hours_W As WhereParameter = Nothing
		Private _ReOpen_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_PayStart_W = Nothing
			_PayEnd_W = Nothing
			_TransStart_W = Nothing
			_TransEnd_W = Nothing
			_OTCode_W = Nothing
			_OTHours_W = Nothing
			_OT8Hours_W = Nothing
			_OTNDHours_W = Nothing
			_OTND2Hours_W = Nothing
			_ReOpen_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayStart() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayStart, Parameters.PayStart)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayEnd() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayEnd, Parameters.PayEnd)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TransStart() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TransStart, Parameters.TransStart)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TransEnd() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TransEnd, Parameters.TransEnd)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OTCode() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTCode, Parameters.OTCode)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OTHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTHours, Parameters.OTHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OT8Hours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OT8Hours, Parameters.OT8Hours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OTNDHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTNDHours, Parameters.OTNDHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OTND2Hours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OTND2Hours, Parameters.OTND2Hours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ReOpen() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ReOpen, Parameters.ReOpen)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpDate, Parameters.LastUpDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property PayStart() As AggregateParameter 
			Get
				If _PayStart_W Is Nothing Then
					_PayStart_W = TearOff.PayStart
				End If
				Return _PayStart_W
			End Get
		End Property

		Public ReadOnly Property PayEnd() As AggregateParameter 
			Get
				If _PayEnd_W Is Nothing Then
					_PayEnd_W = TearOff.PayEnd
				End If
				Return _PayEnd_W
			End Get
		End Property

		Public ReadOnly Property TransStart() As AggregateParameter 
			Get
				If _TransStart_W Is Nothing Then
					_TransStart_W = TearOff.TransStart
				End If
				Return _TransStart_W
			End Get
		End Property

		Public ReadOnly Property TransEnd() As AggregateParameter 
			Get
				If _TransEnd_W Is Nothing Then
					_TransEnd_W = TearOff.TransEnd
				End If
				Return _TransEnd_W
			End Get
		End Property

		Public ReadOnly Property OTCode() As AggregateParameter 
			Get
				If _OTCode_W Is Nothing Then
					_OTCode_W = TearOff.OTCode
				End If
				Return _OTCode_W
			End Get
		End Property

		Public ReadOnly Property OTHours() As AggregateParameter 
			Get
				If _OTHours_W Is Nothing Then
					_OTHours_W = TearOff.OTHours
				End If
				Return _OTHours_W
			End Get
		End Property

		Public ReadOnly Property OT8Hours() As AggregateParameter 
			Get
				If _OT8Hours_W Is Nothing Then
					_OT8Hours_W = TearOff.OT8Hours
				End If
				Return _OT8Hours_W
			End Get
		End Property

		Public ReadOnly Property OTNDHours() As AggregateParameter 
			Get
				If _OTNDHours_W Is Nothing Then
					_OTNDHours_W = TearOff.OTNDHours
				End If
				Return _OTNDHours_W
			End Get
		End Property

		Public ReadOnly Property OTND2Hours() As AggregateParameter 
			Get
				If _OTND2Hours_W Is Nothing Then
					_OTND2Hours_W = TearOff.OTND2Hours
				End If
				Return _OTND2Hours_W
			End Get
		End Property

		Public ReadOnly Property ReOpen() As AggregateParameter 
			Get
				If _ReOpen_W Is Nothing Then
					_ReOpen_W = TearOff.ReOpen
				End If
				Return _ReOpen_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpDate() As AggregateParameter 
			Get
				If _LastUpDate_W Is Nothing Then
					_LastUpDate_W = TearOff.LastUpDate
				End If
				Return _LastUpDate_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _PayStart_W As AggregateParameter = Nothing
		Private _PayEnd_W As AggregateParameter = Nothing
		Private _TransStart_W As AggregateParameter = Nothing
		Private _TransEnd_W As AggregateParameter = Nothing
		Private _OTCode_W As AggregateParameter = Nothing
		Private _OTHours_W As AggregateParameter = Nothing
		Private _OT8Hours_W As AggregateParameter = Nothing
		Private _OTNDHours_W As AggregateParameter = Nothing
		Private _OTND2Hours_W As AggregateParameter = Nothing
		Private _ReOpen_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_PayStart_W = Nothing
		_PayEnd_W = Nothing
		_TransStart_W = Nothing
		_TransEnd_W = Nothing
		_OTCode_W = Nothing
		_OTHours_W = Nothing
		_OT8Hours_W = Nothing
		_OTNDHours_W = Nothing
		_OTND2Hours_W = Nothing
		_ReOpen_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummOTInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummOTUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummOTDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayStart)
		p.SourceColumn = ColumnNames.PayStart
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayEnd)
		p.SourceColumn = ColumnNames.PayEnd
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TransStart)
		p.SourceColumn = ColumnNames.TransStart
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TransEnd)
		p.SourceColumn = ColumnNames.TransEnd
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OTCode)
		p.SourceColumn = ColumnNames.OTCode
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OTHours)
		p.SourceColumn = ColumnNames.OTHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OT8Hours)
		p.SourceColumn = ColumnNames.OT8Hours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OTNDHours)
		p.SourceColumn = ColumnNames.OTNDHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OTND2Hours)
		p.SourceColumn = ColumnNames.OTND2Hours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ReOpen)
		p.SourceColumn = ColumnNames.ReOpen
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpDate)
		p.SourceColumn = ColumnNames.LastUpDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

