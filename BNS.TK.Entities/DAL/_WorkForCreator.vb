
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _WorkForCreator
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "WorkForCreator"
			Me.MappingName = "WorkForCreator"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkForCreatorLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal RowID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_WorkForCreator.Parameters.RowID, RowID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkForCreatorLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property RowID As SqlParameter
			Get
				Return New SqlParameter("@RowID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkForSubID As SqlParameter
			Get
				Return New SqlParameter("@WorkForSubID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const RowID As String = "rowID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const WorkForSubID As String = "WorkForSubID"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(RowID) = _WorkForCreator.PropertyNames.RowID
				ht(CompanyID) = _WorkForCreator.PropertyNames.CompanyID
				ht(EmployeeID) = _WorkForCreator.PropertyNames.EmployeeID
				ht(WorkForSubID) = _WorkForCreator.PropertyNames.WorkForSubID
				ht(CreatedBy) = _WorkForCreator.PropertyNames.CreatedBy
				ht(CreatedDate) = _WorkForCreator.PropertyNames.CreatedDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const RowID As String = "RowID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const WorkForSubID As String = "WorkForSubID"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(RowID) = _WorkForCreator.ColumnNames.RowID
				ht(CompanyID) = _WorkForCreator.ColumnNames.CompanyID
				ht(EmployeeID) = _WorkForCreator.ColumnNames.EmployeeID
				ht(WorkForSubID) = _WorkForCreator.ColumnNames.WorkForSubID
				ht(CreatedBy) = _WorkForCreator.ColumnNames.CreatedBy
				ht(CreatedDate) = _WorkForCreator.ColumnNames.CreatedDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const RowID As String = "s_RowID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const WorkForSubID As String = "s_WorkForSubID"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property RowID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.RowID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.RowID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property WorkForSubID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.WorkForSubID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.WorkForSubID, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_RowID As String
			Get
				If Me.IsColumnNull(ColumnNames.RowID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.RowID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.RowID)
				Else
					Me.RowID = MyBase.SetIntegerAsString(ColumnNames.RowID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkForSubID As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkForSubID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.WorkForSubID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkForSubID)
				Else
					Me.WorkForSubID = MyBase.SetIntegerAsString(ColumnNames.WorkForSubID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property RowID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.RowID, Parameters.RowID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkForSubID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkForSubID, Parameters.WorkForSubID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property RowID() As WhereParameter 
			Get
				If _RowID_W Is Nothing Then
					_RowID_W = TearOff.RowID
				End If
				Return _RowID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As WhereParameter 
			Get
				If _WorkForSubID_W Is Nothing Then
					_WorkForSubID_W = TearOff.WorkForSubID
				End If
				Return _WorkForSubID_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Private _RowID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _WorkForSubID_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_RowID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_WorkForSubID_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property RowID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RowID, Parameters.RowID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkForSubID, Parameters.WorkForSubID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property RowID() As AggregateParameter 
			Get
				If _RowID_W Is Nothing Then
					_RowID_W = TearOff.RowID
				End If
				Return _RowID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As AggregateParameter 
			Get
				If _WorkForSubID_W Is Nothing Then
					_WorkForSubID_W = TearOff.WorkForSubID
				End If
				Return _WorkForSubID_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Private _RowID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _WorkForSubID_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_RowID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_WorkForSubID_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkForCreatorInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.RowID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkForCreatorUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkForCreatorDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.RowID)
		p.SourceColumn = ColumnNames.RowID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.RowID)
		p.SourceColumn = ColumnNames.RowID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkForSubID)
		p.SourceColumn = ColumnNames.WorkForSubID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

