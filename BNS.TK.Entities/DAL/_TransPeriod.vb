
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _TransPeriod
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "TransPeriod"
            Me.MappingName = "TransPeriod"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TransPeriodLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal StartDate As DateTime, ByVal EndDate As DateTime) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_TransPeriod.Parameters.CompanyID, CompanyID)
            parameters.Add(_TransPeriod.Parameters.StartDate, StartDate)
            parameters.Add(_TransPeriod.Parameters.EndDate, EndDate)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TransPeriodLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property RowID As SqlParameter
                Get
                    Return New SqlParameter("@RowID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property StartDate As SqlParameter
                Get
                    Return New SqlParameter("@StartDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property EndDate As SqlParameter
                Get
                    Return New SqlParameter("@EndDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property PayStart As SqlParameter
                Get
                    Return New SqlParameter("@PayStart", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property PayEnd As SqlParameter
                Get
                    Return New SqlParameter("@PayEnd", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property AppYear As SqlParameter
                Get
                    Return New SqlParameter("@AppYear", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Current As SqlParameter
                Get
                    Return New SqlParameter("@Current", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Posted As SqlParameter
                Get
                    Return New SqlParameter("@Posted", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Finalized As SqlParameter
                Get
                    Return New SqlParameter("@Finalized", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Employee_lock_start As SqlParameter
                Get
                    Return New SqlParameter("@Employee_lock_start", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Employee_lock_end As SqlParameter
                Get
                    Return New SqlParameter("@Employee_lock_end", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Approver_lock_start As SqlParameter
                Get
                    Return New SqlParameter("@Approver_lock_start", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Approver_lock_end As SqlParameter
                Get
                    Return New SqlParameter("@Approver_lock_end", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const RowID As String = "rowID"
            Public Const CompanyID As String = "CompanyID"
            Public Const StartDate As String = "StartDate"
            Public Const EndDate As String = "EndDate"
            Public Const PayStart As String = "PayStart"
            Public Const PayEnd As String = "PayEnd"
            Public Const AppYear As String = "AppYear"
            Public Const Current As String = "Current"
            Public Const Posted As String = "Posted"
            Public Const Finalized As String = "Finalized"
            Public Const Employee_lock_start As String = "Employee_lock_start"
            Public Const Employee_lock_end As String = "Employee_lock_end"
            Public Const Approver_lock_start As String = "Approver_lock_start"
            Public Const Approver_lock_end As String = "Approver_lock_end"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(RowID) = _TransPeriod.PropertyNames.RowID
                    ht(CompanyID) = _TransPeriod.PropertyNames.CompanyID
                    ht(StartDate) = _TransPeriod.PropertyNames.StartDate
                    ht(EndDate) = _TransPeriod.PropertyNames.EndDate
                    ht(PayStart) = _TransPeriod.PropertyNames.PayStart
                    ht(PayEnd) = _TransPeriod.PropertyNames.PayEnd
                    ht(AppYear) = _TransPeriod.PropertyNames.AppYear
                    ht(Current) = _TransPeriod.PropertyNames.Current
                    ht(Posted) = _TransPeriod.PropertyNames.Posted
                    ht(Finalized) = _TransPeriod.PropertyNames.Finalized
                    ht(Employee_lock_start) = _TransPeriod.PropertyNames.Employee_lock_start
                    ht(Employee_lock_end) = _TransPeriod.PropertyNames.Employee_lock_end
                    ht(Approver_lock_start) = _TransPeriod.PropertyNames.Approver_lock_start
                    ht(Approver_lock_end) = _TransPeriod.PropertyNames.Approver_lock_end

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const RowID As String = "RowID"
            Public Const CompanyID As String = "CompanyID"
            Public Const StartDate As String = "StartDate"
            Public Const EndDate As String = "EndDate"
            Public Const PayStart As String = "PayStart"
            Public Const PayEnd As String = "PayEnd"
            Public Const AppYear As String = "AppYear"
            Public Const Current As String = "Current"
            Public Const Posted As String = "Posted"
            Public Const Finalized As String = "Finalized"
            Public Const Employee_lock_start As String = "Employee_lock_start"
            Public Const Employee_lock_end As String = "Employee_lock_end"
            Public Const Approver_lock_start As String = "Approver_lock_start"
            Public Const Approver_lock_end As String = "Approver_lock_end"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(RowID) = _TransPeriod.ColumnNames.RowID
                    ht(CompanyID) = _TransPeriod.ColumnNames.CompanyID
                    ht(StartDate) = _TransPeriod.ColumnNames.StartDate
                    ht(EndDate) = _TransPeriod.ColumnNames.EndDate
                    ht(PayStart) = _TransPeriod.ColumnNames.PayStart
                    ht(PayEnd) = _TransPeriod.ColumnNames.PayEnd
                    ht(AppYear) = _TransPeriod.ColumnNames.AppYear
                    ht(Current) = _TransPeriod.ColumnNames.Current
                    ht(Posted) = _TransPeriod.ColumnNames.Posted
                    ht(Finalized) = _TransPeriod.ColumnNames.Finalized
                    ht(Employee_lock_start) = _TransPeriod.ColumnNames.Employee_lock_start
                    ht(Employee_lock_end) = _TransPeriod.ColumnNames.Employee_lock_end
                    ht(Approver_lock_start) = _TransPeriod.ColumnNames.Approver_lock_start
                    ht(Approver_lock_end) = _TransPeriod.ColumnNames.Approver_lock_end

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const RowID As String = "s_RowID"
            Public Const CompanyID As String = "s_CompanyID"
            Public Const StartDate As String = "s_StartDate"
            Public Const EndDate As String = "s_EndDate"
            Public Const PayStart As String = "s_PayStart"
            Public Const PayEnd As String = "s_PayEnd"
            Public Const AppYear As String = "s_AppYear"
            Public Const Current As String = "s_Current"
            Public Const Posted As String = "s_Posted"
            Public Const Finalized As String = "s_Finalized"
            Public Const Employee_lock_start As String = "s_Employee_lock_start"
            Public Const Employee_lock_end As String = "s_Employee_lock_end"
            Public Const Approver_lock_start As String = "s_Approver_lock_start"
            Public Const Approver_lock_end As String = "s_Approver_lock_end"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property RowID As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.RowID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.RowID, Value)
            End Set
        End Property

        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property StartDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.StartDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.StartDate, Value)
            End Set
        End Property

        Public Overridable Property EndDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.EndDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.EndDate, Value)
            End Set
        End Property

        Public Overridable Property PayStart As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.PayStart)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.PayStart, Value)
            End Set
        End Property

        Public Overridable Property PayEnd As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.PayEnd)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.PayEnd, Value)
            End Set
        End Property

        Public Overridable Property AppYear As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.AppYear)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.AppYear, Value)
            End Set
        End Property

        Public Overridable Property Current As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Current)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Current, Value)
            End Set
        End Property

        Public Overridable Property Posted As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Posted)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Posted, Value)
            End Set
        End Property

        Public Overridable Property Finalized As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Finalized)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Finalized, Value)
            End Set
        End Property

        Public Overridable Property Employee_lock_start As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Employee_lock_start)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Employee_lock_start, Value)
            End Set
        End Property

        Public Overridable Property Employee_lock_end As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Employee_lock_end)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Employee_lock_end, Value)
            End Set
        End Property

        Public Overridable Property Approver_lock_start As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Approver_lock_start)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Approver_lock_start, Value)
            End Set
        End Property

        Public Overridable Property Approver_lock_end As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.Approver_lock_end)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.Approver_lock_end, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_RowID As String
            Get
                If Me.IsColumnNull(ColumnNames.RowID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.RowID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.RowID)
                Else
                    Me.RowID = MyBase.SetIntegerAsString(ColumnNames.RowID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_StartDate As String
            Get
                If Me.IsColumnNull(ColumnNames.StartDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.StartDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.StartDate)
                Else
                    Me.StartDate = MyBase.SetDateTimeAsString(ColumnNames.StartDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EndDate As String
            Get
                If Me.IsColumnNull(ColumnNames.EndDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.EndDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EndDate)
                Else
                    Me.EndDate = MyBase.SetDateTimeAsString(ColumnNames.EndDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_PayStart As String
            Get
                If Me.IsColumnNull(ColumnNames.PayStart) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.PayStart)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.PayStart)
                Else
                    Me.PayStart = MyBase.SetDateTimeAsString(ColumnNames.PayStart, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_PayEnd As String
            Get
                If Me.IsColumnNull(ColumnNames.PayEnd) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.PayEnd)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.PayEnd)
                Else
                    Me.PayEnd = MyBase.SetDateTimeAsString(ColumnNames.PayEnd, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_AppYear As String
            Get
                If Me.IsColumnNull(ColumnNames.AppYear) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.AppYear)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.AppYear)
                Else
                    Me.AppYear = MyBase.SetIntegerAsString(ColumnNames.AppYear, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Current As String
            Get
                If Me.IsColumnNull(ColumnNames.Current) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Current)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Current)
                Else
                    Me.Current = MyBase.SetBooleanAsString(ColumnNames.Current, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Posted As String
            Get
                If Me.IsColumnNull(ColumnNames.Posted) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Posted)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Posted)
                Else
                    Me.Posted = MyBase.SetBooleanAsString(ColumnNames.Posted, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Finalized As String
            Get
                If Me.IsColumnNull(ColumnNames.Finalized) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Finalized)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Finalized)
                Else
                    Me.Finalized = MyBase.SetBooleanAsString(ColumnNames.Finalized, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Employee_lock_start As String
            Get
                If Me.IsColumnNull(ColumnNames.Employee_lock_start) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Employee_lock_start)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Employee_lock_start)
                Else
                    Me.Employee_lock_start = MyBase.SetDateTimeAsString(ColumnNames.Employee_lock_start, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Employee_lock_end As String
            Get
                If Me.IsColumnNull(ColumnNames.Employee_lock_end) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Employee_lock_end)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Employee_lock_end)
                Else
                    Me.Employee_lock_end = MyBase.SetDateTimeAsString(ColumnNames.Employee_lock_end, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Approver_lock_start As String
            Get
                If Me.IsColumnNull(ColumnNames.Approver_lock_start) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Approver_lock_start)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Approver_lock_start)
                Else
                    Me.Approver_lock_start = MyBase.SetDateTimeAsString(ColumnNames.Approver_lock_start, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Approver_lock_end As String
            Get
                If Me.IsColumnNull(ColumnNames.Approver_lock_end) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.Approver_lock_end)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Approver_lock_end)
                Else
                    Me.Approver_lock_end = MyBase.SetDateTimeAsString(ColumnNames.Approver_lock_end, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property RowID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.RowID, Parameters.RowID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property StartDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.StartDate, Parameters.StartDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EndDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EndDate, Parameters.EndDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property PayStart() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.PayStart, Parameters.PayStart)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property PayEnd() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.PayEnd, Parameters.PayEnd)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property AppYear() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.AppYear, Parameters.AppYear)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Current() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Current, Parameters.Current)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Posted() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Posted, Parameters.Posted)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Finalized() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Finalized, Parameters.Finalized)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Employee_lock_start() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Employee_lock_start, Parameters.Employee_lock_start)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Employee_lock_end() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Employee_lock_end, Parameters.Employee_lock_end)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Approver_lock_start() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Approver_lock_start, Parameters.Approver_lock_start)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Approver_lock_end() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Approver_lock_end, Parameters.Approver_lock_end)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property RowID() As WhereParameter
                Get
                    If _RowID_W Is Nothing Then
                        _RowID_W = TearOff.RowID
                    End If
                    Return _RowID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property StartDate() As WhereParameter
                Get
                    If _StartDate_W Is Nothing Then
                        _StartDate_W = TearOff.StartDate
                    End If
                    Return _StartDate_W
                End Get
            End Property

            Public ReadOnly Property EndDate() As WhereParameter
                Get
                    If _EndDate_W Is Nothing Then
                        _EndDate_W = TearOff.EndDate
                    End If
                    Return _EndDate_W
                End Get
            End Property

            Public ReadOnly Property PayStart() As WhereParameter
                Get
                    If _PayStart_W Is Nothing Then
                        _PayStart_W = TearOff.PayStart
                    End If
                    Return _PayStart_W
                End Get
            End Property

            Public ReadOnly Property PayEnd() As WhereParameter
                Get
                    If _PayEnd_W Is Nothing Then
                        _PayEnd_W = TearOff.PayEnd
                    End If
                    Return _PayEnd_W
                End Get
            End Property

            Public ReadOnly Property AppYear() As WhereParameter
                Get
                    If _AppYear_W Is Nothing Then
                        _AppYear_W = TearOff.AppYear
                    End If
                    Return _AppYear_W
                End Get
            End Property

            Public ReadOnly Property Current() As WhereParameter
                Get
                    If _Current_W Is Nothing Then
                        _Current_W = TearOff.Current
                    End If
                    Return _Current_W
                End Get
            End Property

            Public ReadOnly Property Posted() As WhereParameter
                Get
                    If _Posted_W Is Nothing Then
                        _Posted_W = TearOff.Posted
                    End If
                    Return _Posted_W
                End Get
            End Property

            Public ReadOnly Property Finalized() As WhereParameter
                Get
                    If _Finalized_W Is Nothing Then
                        _Finalized_W = TearOff.Finalized
                    End If
                    Return _Finalized_W
                End Get
            End Property

            Public ReadOnly Property Employee_lock_start() As WhereParameter
                Get
                    If _Employee_lock_start_W Is Nothing Then
                        _Employee_lock_start_W = TearOff.Employee_lock_start
                    End If
                    Return _Employee_lock_start_W
                End Get
            End Property

            Public ReadOnly Property Employee_lock_end() As WhereParameter
                Get
                    If _Employee_lock_end_W Is Nothing Then
                        _Employee_lock_end_W = TearOff.Employee_lock_end
                    End If
                    Return _Employee_lock_end_W
                End Get
            End Property

            Public ReadOnly Property Approver_lock_start() As WhereParameter
                Get
                    If _Approver_lock_start_W Is Nothing Then
                        _Approver_lock_start_W = TearOff.Approver_lock_start
                    End If
                    Return _Approver_lock_start_W
                End Get
            End Property

            Public ReadOnly Property Approver_lock_end() As WhereParameter
                Get
                    If _Approver_lock_end_W Is Nothing Then
                        _Approver_lock_end_W = TearOff.Approver_lock_end
                    End If
                    Return _Approver_lock_end_W
                End Get
            End Property

            Private _RowID_W As WhereParameter = Nothing
            Private _CompanyID_W As WhereParameter = Nothing
            Private _StartDate_W As WhereParameter = Nothing
            Private _EndDate_W As WhereParameter = Nothing
            Private _PayStart_W As WhereParameter = Nothing
            Private _PayEnd_W As WhereParameter = Nothing
            Private _AppYear_W As WhereParameter = Nothing
            Private _Current_W As WhereParameter = Nothing
            Private _Posted_W As WhereParameter = Nothing
            Private _Finalized_W As WhereParameter = Nothing
            Private _Employee_lock_start_W As WhereParameter = Nothing
            Private _Employee_lock_end_W As WhereParameter = Nothing
            Private _Approver_lock_start_W As WhereParameter = Nothing
            Private _Approver_lock_end_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _RowID_W = Nothing
                _CompanyID_W = Nothing
                _StartDate_W = Nothing
                _EndDate_W = Nothing
                _PayStart_W = Nothing
                _PayEnd_W = Nothing
                _AppYear_W = Nothing
                _Current_W = Nothing
                _Posted_W = Nothing
                _Finalized_W = Nothing
                _Employee_lock_start_W = Nothing
                _Employee_lock_end_W = Nothing
                _Approver_lock_start_W = Nothing
                _Approver_lock_end_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property RowID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RowID, Parameters.RowID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property StartDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.StartDate, Parameters.StartDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EndDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EndDate, Parameters.EndDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property PayStart() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayStart, Parameters.PayStart)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property PayEnd() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayEnd, Parameters.PayEnd)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property AppYear() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AppYear, Parameters.AppYear)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Current() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Current, Parameters.Current)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Posted() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Posted, Parameters.Posted)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Finalized() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Finalized, Parameters.Finalized)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Employee_lock_start() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Employee_lock_start, Parameters.Employee_lock_start)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Employee_lock_end() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Employee_lock_end, Parameters.Employee_lock_end)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Approver_lock_start() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Approver_lock_start, Parameters.Approver_lock_start)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Approver_lock_end() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Approver_lock_end, Parameters.Approver_lock_end)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property RowID() As AggregateParameter
                Get
                    If _RowID_W Is Nothing Then
                        _RowID_W = TearOff.RowID
                    End If
                    Return _RowID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property StartDate() As AggregateParameter
                Get
                    If _StartDate_W Is Nothing Then
                        _StartDate_W = TearOff.StartDate
                    End If
                    Return _StartDate_W
                End Get
            End Property

            Public ReadOnly Property EndDate() As AggregateParameter
                Get
                    If _EndDate_W Is Nothing Then
                        _EndDate_W = TearOff.EndDate
                    End If
                    Return _EndDate_W
                End Get
            End Property

            Public ReadOnly Property PayStart() As AggregateParameter
                Get
                    If _PayStart_W Is Nothing Then
                        _PayStart_W = TearOff.PayStart
                    End If
                    Return _PayStart_W
                End Get
            End Property

            Public ReadOnly Property PayEnd() As AggregateParameter
                Get
                    If _PayEnd_W Is Nothing Then
                        _PayEnd_W = TearOff.PayEnd
                    End If
                    Return _PayEnd_W
                End Get
            End Property

            Public ReadOnly Property AppYear() As AggregateParameter
                Get
                    If _AppYear_W Is Nothing Then
                        _AppYear_W = TearOff.AppYear
                    End If
                    Return _AppYear_W
                End Get
            End Property

            Public ReadOnly Property Current() As AggregateParameter
                Get
                    If _Current_W Is Nothing Then
                        _Current_W = TearOff.Current
                    End If
                    Return _Current_W
                End Get
            End Property

            Public ReadOnly Property Posted() As AggregateParameter
                Get
                    If _Posted_W Is Nothing Then
                        _Posted_W = TearOff.Posted
                    End If
                    Return _Posted_W
                End Get
            End Property

            Public ReadOnly Property Finalized() As AggregateParameter
                Get
                    If _Finalized_W Is Nothing Then
                        _Finalized_W = TearOff.Finalized
                    End If
                    Return _Finalized_W
                End Get
            End Property

            Public ReadOnly Property Employee_lock_start() As AggregateParameter
                Get
                    If _Employee_lock_start_W Is Nothing Then
                        _Employee_lock_start_W = TearOff.Employee_lock_start
                    End If
                    Return _Employee_lock_start_W
                End Get
            End Property

            Public ReadOnly Property Employee_lock_end() As AggregateParameter
                Get
                    If _Employee_lock_end_W Is Nothing Then
                        _Employee_lock_end_W = TearOff.Employee_lock_end
                    End If
                    Return _Employee_lock_end_W
                End Get
            End Property

            Public ReadOnly Property Approver_lock_start() As AggregateParameter
                Get
                    If _Approver_lock_start_W Is Nothing Then
                        _Approver_lock_start_W = TearOff.Approver_lock_start
                    End If
                    Return _Approver_lock_start_W
                End Get
            End Property

            Public ReadOnly Property Approver_lock_end() As AggregateParameter
                Get
                    If _Approver_lock_end_W Is Nothing Then
                        _Approver_lock_end_W = TearOff.Approver_lock_end
                    End If
                    Return _Approver_lock_end_W
                End Get
            End Property

            Private _RowID_W As AggregateParameter = Nothing
            Private _CompanyID_W As AggregateParameter = Nothing
            Private _StartDate_W As AggregateParameter = Nothing
            Private _EndDate_W As AggregateParameter = Nothing
            Private _PayStart_W As AggregateParameter = Nothing
            Private _PayEnd_W As AggregateParameter = Nothing
            Private _AppYear_W As AggregateParameter = Nothing
            Private _Current_W As AggregateParameter = Nothing
            Private _Posted_W As AggregateParameter = Nothing
            Private _Finalized_W As AggregateParameter = Nothing
            Private _Employee_lock_start_W As AggregateParameter = Nothing
            Private _Employee_lock_end_W As AggregateParameter = Nothing
            Private _Approver_lock_start_W As AggregateParameter = Nothing
            Private _Approver_lock_end_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _RowID_W = Nothing
                _CompanyID_W = Nothing
                _StartDate_W = Nothing
                _EndDate_W = Nothing
                _PayStart_W = Nothing
                _PayEnd_W = Nothing
                _AppYear_W = Nothing
                _Current_W = Nothing
                _Posted_W = Nothing
                _Finalized_W = Nothing
                _Employee_lock_start_W = Nothing
                _Employee_lock_end_W = Nothing
                _Approver_lock_start_W = Nothing
                _Approver_lock_end_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TransPeriodInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.RowID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TransPeriodUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TransPeriodDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.StartDate)
            p.SourceColumn = ColumnNames.StartDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EndDate)
            p.SourceColumn = ColumnNames.EndDate
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.RowID)
            p.SourceColumn = ColumnNames.RowID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.StartDate)
            p.SourceColumn = ColumnNames.StartDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EndDate)
            p.SourceColumn = ColumnNames.EndDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.PayStart)
            p.SourceColumn = ColumnNames.PayStart
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.PayEnd)
            p.SourceColumn = ColumnNames.PayEnd
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.AppYear)
            p.SourceColumn = ColumnNames.AppYear
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Current)
            p.SourceColumn = ColumnNames.Current
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Posted)
            p.SourceColumn = ColumnNames.Posted
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Finalized)
            p.SourceColumn = ColumnNames.Finalized
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Employee_lock_start)
            p.SourceColumn = ColumnNames.Employee_lock_start
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Employee_lock_end)
            p.SourceColumn = ColumnNames.Employee_lock_end
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Approver_lock_start)
            p.SourceColumn = ColumnNames.Approver_lock_start
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Approver_lock_end)
            p.SourceColumn = ColumnNames.Approver_lock_end
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

