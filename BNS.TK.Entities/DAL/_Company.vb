
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _Company
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "Company"
			Me.MappingName = "Company"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_CompanyLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_Company.Parameters.CompanyID, CompanyID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_CompanyLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyName As SqlParameter
			Get
				Return New SqlParameter("@CompanyName", SqlDbType.VarChar, 50)
			End Get
		End Property
		
		Public Shared ReadOnly Property Address1 As SqlParameter
			Get
				Return New SqlParameter("@Address1", SqlDbType.VarChar, 50)
			End Get
		End Property
		
		Public Shared ReadOnly Property Address2 As SqlParameter
			Get
				Return New SqlParameter("@Address2", SqlDbType.VarChar, 50)
			End Get
		End Property
		
		Public Shared ReadOnly Property ContactPerson As SqlParameter
			Get
				Return New SqlParameter("@ContactPerson", SqlDbType.VarChar, 35)
			End Get
		End Property
		
		Public Shared ReadOnly Property ContactPosition As SqlParameter
			Get
				Return New SqlParameter("@ContactPosition", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property ApprovingOfficer As SqlParameter
			Get
				Return New SqlParameter("@ApprovingOfficer", SqlDbType.VarChar, 40)
			End Get
		End Property
		
		Public Shared ReadOnly Property OfficerPosition As SqlParameter
			Get
				Return New SqlParameter("@OfficerPosition", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property ContactNumber As SqlParameter
			Get
				Return New SqlParameter("@ContactNumber", SqlDbType.VarChar, 20)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmailAddress As SqlParameter
			Get
				Return New SqlParameter("@EmailAddress", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property FaxNumber As SqlParameter
			Get
				Return New SqlParameter("@FaxNumber", SqlDbType.VarChar, 30)
			End Get
		End Property
		
		Public Shared ReadOnly Property ND1Start As SqlParameter
			Get
				Return New SqlParameter("@ND1Start", SqlDbType.VarChar, 8)
			End Get
		End Property
		
		Public Shared ReadOnly Property ND1End As SqlParameter
			Get
				Return New SqlParameter("@ND1End", SqlDbType.VarChar, 8)
			End Get
		End Property
		
		Public Shared ReadOnly Property ND2Start As SqlParameter
			Get
				Return New SqlParameter("@ND2Start", SqlDbType.VarChar, 8)
			End Get
		End Property
		
		Public Shared ReadOnly Property ND2End As SqlParameter
			Get
				Return New SqlParameter("@ND2End", SqlDbType.VarChar, 8)
			End Get
		End Property
		
		Public Shared ReadOnly Property Variance As SqlParameter
			Get
				Return New SqlParameter("@Variance", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level1 As SqlParameter
			Get
				Return New SqlParameter("@Level1", SqlDbType.VarChar, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level2 As SqlParameter
			Get
				Return New SqlParameter("@Level2", SqlDbType.VarChar, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level3 As SqlParameter
			Get
				Return New SqlParameter("@Level3", SqlDbType.VarChar, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Level4 As SqlParameter
			Get
				Return New SqlParameter("@Level4", SqlDbType.VarChar, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property PasswordDays As SqlParameter
			Get
				Return New SqlParameter("@PasswordDays", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PasswordAttempt As SqlParameter
			Get
				Return New SqlParameter("@PasswordAttempt", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property YearCurrent As SqlParameter
			Get
				Return New SqlParameter("@YearCurrent", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property YearStart As SqlParameter
			Get
				Return New SqlParameter("@YearStart", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property YearEnd As SqlParameter
			Get
				Return New SqlParameter("@YearEnd", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkFor As SqlParameter
			Get
				Return New SqlParameter("@WorkFor", SqlDbType.VarChar, 25)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkForSub As SqlParameter
			Get
				Return New SqlParameter("@WorkForSub", SqlDbType.VarChar, 25)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkForEnabled As SqlParameter
			Get
				Return New SqlParameter("@WorkForEnabled", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property DefaultCC As SqlParameter
			Get
				Return New SqlParameter("@DefaultCC", SqlDbType.VarChar, 500)
			End Get
		End Property
		
		Public Shared ReadOnly Property DefaultBCC As SqlParameter
			Get
				Return New SqlParameter("@DefaultBCC", SqlDbType.VarChar, 500)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const CompanyID As String = "CompanyID"
        Public Const CompanyName As String = "CompanyName"
        Public Const Address1 As String = "Address1"
        Public Const Address2 As String = "Address2"
        Public Const ContactPerson As String = "ContactPerson"
        Public Const ContactPosition As String = "ContactPosition"
        Public Const ApprovingOfficer As String = "ApprovingOfficer"
        Public Const OfficerPosition As String = "OfficerPosition"
        Public Const ContactNumber As String = "ContactNumber"
        Public Const EmailAddress As String = "EmailAddress"
        Public Const FaxNumber As String = "FaxNumber"
        Public Const ND1Start As String = "ND1Start"
        Public Const ND1End As String = "ND1End"
        Public Const ND2Start As String = "ND2Start"
        Public Const ND2End As String = "ND2End"
        Public Const Variance As String = "Variance"
        Public Const Level1 As String = "Level1"
        Public Const Level2 As String = "Level2"
        Public Const Level3 As String = "Level3"
        Public Const Level4 As String = "Level4"
        Public Const PasswordDays As String = "PasswordDays"
        Public Const PasswordAttempt As String = "PasswordAttempt"
        Public Const YearCurrent As String = "YearCurrent"
        Public Const YearStart As String = "YearStart"
        Public Const YearEnd As String = "YearEnd"
        Public Const WorkFor As String = "WorkFor"
        Public Const WorkForSub As String = "WorkForSub"
        Public Const WorkForEnabled As String = "WorkForEnabled"
        Public Const DefaultCC As String = "DefaultCC"
        Public Const DefaultBCC As String = "DefaultBCC"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(CompanyID) = _Company.PropertyNames.CompanyID
				ht(CompanyName) = _Company.PropertyNames.CompanyName
				ht(Address1) = _Company.PropertyNames.Address1
				ht(Address2) = _Company.PropertyNames.Address2
				ht(ContactPerson) = _Company.PropertyNames.ContactPerson
				ht(ContactPosition) = _Company.PropertyNames.ContactPosition
				ht(ApprovingOfficer) = _Company.PropertyNames.ApprovingOfficer
				ht(OfficerPosition) = _Company.PropertyNames.OfficerPosition
				ht(ContactNumber) = _Company.PropertyNames.ContactNumber
				ht(EmailAddress) = _Company.PropertyNames.EmailAddress
				ht(FaxNumber) = _Company.PropertyNames.FaxNumber
				ht(ND1Start) = _Company.PropertyNames.ND1Start
				ht(ND1End) = _Company.PropertyNames.ND1End
				ht(ND2Start) = _Company.PropertyNames.ND2Start
				ht(ND2End) = _Company.PropertyNames.ND2End
				ht(Variance) = _Company.PropertyNames.Variance
				ht(Level1) = _Company.PropertyNames.Level1
				ht(Level2) = _Company.PropertyNames.Level2
				ht(Level3) = _Company.PropertyNames.Level3
				ht(Level4) = _Company.PropertyNames.Level4
				ht(PasswordDays) = _Company.PropertyNames.PasswordDays
				ht(PasswordAttempt) = _Company.PropertyNames.PasswordAttempt
				ht(YearCurrent) = _Company.PropertyNames.YearCurrent
				ht(YearStart) = _Company.PropertyNames.YearStart
				ht(YearEnd) = _Company.PropertyNames.YearEnd
				ht(WorkFor) = _Company.PropertyNames.WorkFor
				ht(WorkForSub) = _Company.PropertyNames.WorkForSub
				ht(WorkForEnabled) = _Company.PropertyNames.WorkForEnabled
				ht(DefaultCC) = _Company.PropertyNames.DefaultCC
				ht(DefaultBCC) = _Company.PropertyNames.DefaultBCC
				ht(CreatedBy) = _Company.PropertyNames.CreatedBy
				ht(CreatedDate) = _Company.PropertyNames.CreatedDate
				ht(LastUpdBy) = _Company.PropertyNames.LastUpdBy
				ht(LastUpdDate) = _Company.PropertyNames.LastUpdDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const CompanyID As String = "CompanyID"
        Public Const CompanyName As String = "CompanyName"
        Public Const Address1 As String = "Address1"
        Public Const Address2 As String = "Address2"
        Public Const ContactPerson As String = "ContactPerson"
        Public Const ContactPosition As String = "ContactPosition"
        Public Const ApprovingOfficer As String = "ApprovingOfficer"
        Public Const OfficerPosition As String = "OfficerPosition"
        Public Const ContactNumber As String = "ContactNumber"
        Public Const EmailAddress As String = "EmailAddress"
        Public Const FaxNumber As String = "FaxNumber"
        Public Const ND1Start As String = "ND1Start"
        Public Const ND1End As String = "ND1End"
        Public Const ND2Start As String = "ND2Start"
        Public Const ND2End As String = "ND2End"
        Public Const Variance As String = "Variance"
        Public Const Level1 As String = "Level1"
        Public Const Level2 As String = "Level2"
        Public Const Level3 As String = "Level3"
        Public Const Level4 As String = "Level4"
        Public Const PasswordDays As String = "PasswordDays"
        Public Const PasswordAttempt As String = "PasswordAttempt"
        Public Const YearCurrent As String = "YearCurrent"
        Public Const YearStart As String = "YearStart"
        Public Const YearEnd As String = "YearEnd"
        Public Const WorkFor As String = "WorkFor"
        Public Const WorkForSub As String = "WorkForSub"
        Public Const WorkForEnabled As String = "WorkForEnabled"
        Public Const DefaultCC As String = "DefaultCC"
        Public Const DefaultBCC As String = "DefaultBCC"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(CompanyID) = _Company.ColumnNames.CompanyID
				ht(CompanyName) = _Company.ColumnNames.CompanyName
				ht(Address1) = _Company.ColumnNames.Address1
				ht(Address2) = _Company.ColumnNames.Address2
				ht(ContactPerson) = _Company.ColumnNames.ContactPerson
				ht(ContactPosition) = _Company.ColumnNames.ContactPosition
				ht(ApprovingOfficer) = _Company.ColumnNames.ApprovingOfficer
				ht(OfficerPosition) = _Company.ColumnNames.OfficerPosition
				ht(ContactNumber) = _Company.ColumnNames.ContactNumber
				ht(EmailAddress) = _Company.ColumnNames.EmailAddress
				ht(FaxNumber) = _Company.ColumnNames.FaxNumber
				ht(ND1Start) = _Company.ColumnNames.ND1Start
				ht(ND1End) = _Company.ColumnNames.ND1End
				ht(ND2Start) = _Company.ColumnNames.ND2Start
				ht(ND2End) = _Company.ColumnNames.ND2End
				ht(Variance) = _Company.ColumnNames.Variance
				ht(Level1) = _Company.ColumnNames.Level1
				ht(Level2) = _Company.ColumnNames.Level2
				ht(Level3) = _Company.ColumnNames.Level3
				ht(Level4) = _Company.ColumnNames.Level4
				ht(PasswordDays) = _Company.ColumnNames.PasswordDays
				ht(PasswordAttempt) = _Company.ColumnNames.PasswordAttempt
				ht(YearCurrent) = _Company.ColumnNames.YearCurrent
				ht(YearStart) = _Company.ColumnNames.YearStart
				ht(YearEnd) = _Company.ColumnNames.YearEnd
				ht(WorkFor) = _Company.ColumnNames.WorkFor
				ht(WorkForSub) = _Company.ColumnNames.WorkForSub
				ht(WorkForEnabled) = _Company.ColumnNames.WorkForEnabled
				ht(DefaultCC) = _Company.ColumnNames.DefaultCC
				ht(DefaultBCC) = _Company.ColumnNames.DefaultBCC
				ht(CreatedBy) = _Company.ColumnNames.CreatedBy
				ht(CreatedDate) = _Company.ColumnNames.CreatedDate
				ht(LastUpdBy) = _Company.ColumnNames.LastUpdBy
				ht(LastUpdDate) = _Company.ColumnNames.LastUpdDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const CompanyID As String = "s_CompanyID"
        Public Const CompanyName As String = "s_CompanyName"
        Public Const Address1 As String = "s_Address1"
        Public Const Address2 As String = "s_Address2"
        Public Const ContactPerson As String = "s_ContactPerson"
        Public Const ContactPosition As String = "s_ContactPosition"
        Public Const ApprovingOfficer As String = "s_ApprovingOfficer"
        Public Const OfficerPosition As String = "s_OfficerPosition"
        Public Const ContactNumber As String = "s_ContactNumber"
        Public Const EmailAddress As String = "s_EmailAddress"
        Public Const FaxNumber As String = "s_FaxNumber"
        Public Const ND1Start As String = "s_ND1Start"
        Public Const ND1End As String = "s_ND1End"
        Public Const ND2Start As String = "s_ND2Start"
        Public Const ND2End As String = "s_ND2End"
        Public Const Variance As String = "s_Variance"
        Public Const Level1 As String = "s_Level1"
        Public Const Level2 As String = "s_Level2"
        Public Const Level3 As String = "s_Level3"
        Public Const Level4 As String = "s_Level4"
        Public Const PasswordDays As String = "s_PasswordDays"
        Public Const PasswordAttempt As String = "s_PasswordAttempt"
        Public Const YearCurrent As String = "s_YearCurrent"
        Public Const YearStart As String = "s_YearStart"
        Public Const YearEnd As String = "s_YearEnd"
        Public Const WorkFor As String = "s_WorkFor"
        Public Const WorkForSub As String = "s_WorkForSub"
        Public Const WorkForEnabled As String = "s_WorkForEnabled"
        Public Const DefaultCC As String = "s_DefaultCC"
        Public Const DefaultBCC As String = "s_DefaultBCC"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property CompanyName As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyName)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyName, Value)
			End Set
		End Property

		Public Overridable Property Address1 As String
			Get
				Return MyBase.GetString(ColumnNames.Address1)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Address1, Value)
			End Set
		End Property

		Public Overridable Property Address2 As String
			Get
				Return MyBase.GetString(ColumnNames.Address2)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Address2, Value)
			End Set
		End Property

		Public Overridable Property ContactPerson As String
			Get
				Return MyBase.GetString(ColumnNames.ContactPerson)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ContactPerson, Value)
			End Set
		End Property

		Public Overridable Property ContactPosition As String
			Get
				Return MyBase.GetString(ColumnNames.ContactPosition)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ContactPosition, Value)
			End Set
		End Property

		Public Overridable Property ApprovingOfficer As String
			Get
				Return MyBase.GetString(ColumnNames.ApprovingOfficer)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ApprovingOfficer, Value)
			End Set
		End Property

		Public Overridable Property OfficerPosition As String
			Get
				Return MyBase.GetString(ColumnNames.OfficerPosition)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.OfficerPosition, Value)
			End Set
		End Property

		Public Overridable Property ContactNumber As String
			Get
				Return MyBase.GetString(ColumnNames.ContactNumber)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ContactNumber, Value)
			End Set
		End Property

		Public Overridable Property EmailAddress As String
			Get
				Return MyBase.GetString(ColumnNames.EmailAddress)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmailAddress, Value)
			End Set
		End Property

		Public Overridable Property FaxNumber As String
			Get
				Return MyBase.GetString(ColumnNames.FaxNumber)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.FaxNumber, Value)
			End Set
		End Property

		Public Overridable Property ND1Start As String
			Get
				Return MyBase.GetString(ColumnNames.ND1Start)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ND1Start, Value)
			End Set
		End Property

		Public Overridable Property ND1End As String
			Get
				Return MyBase.GetString(ColumnNames.ND1End)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ND1End, Value)
			End Set
		End Property

		Public Overridable Property ND2Start As String
			Get
				Return MyBase.GetString(ColumnNames.ND2Start)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ND2Start, Value)
			End Set
		End Property

		Public Overridable Property ND2End As String
			Get
				Return MyBase.GetString(ColumnNames.ND2End)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ND2End, Value)
			End Set
		End Property

		Public Overridable Property Variance As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.Variance)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.Variance, Value)
			End Set
		End Property

		Public Overridable Property Level1 As String
			Get
				Return MyBase.GetString(ColumnNames.Level1)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level1, Value)
			End Set
		End Property

		Public Overridable Property Level2 As String
			Get
				Return MyBase.GetString(ColumnNames.Level2)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level2, Value)
			End Set
		End Property

		Public Overridable Property Level3 As String
			Get
				Return MyBase.GetString(ColumnNames.Level3)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level3, Value)
			End Set
		End Property

		Public Overridable Property Level4 As String
			Get
				Return MyBase.GetString(ColumnNames.Level4)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Level4, Value)
			End Set
		End Property

		Public Overridable Property PasswordDays As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.PasswordDays)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.PasswordDays, Value)
			End Set
		End Property

		Public Overridable Property PasswordAttempt As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.PasswordAttempt)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.PasswordAttempt, Value)
			End Set
		End Property

		Public Overridable Property YearCurrent As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.YearCurrent)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.YearCurrent, Value)
			End Set
		End Property

		Public Overridable Property YearStart As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.YearStart)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.YearStart, Value)
			End Set
		End Property

		Public Overridable Property YearEnd As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.YearEnd)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.YearEnd, Value)
			End Set
		End Property

		Public Overridable Property WorkFor As String
			Get
				Return MyBase.GetString(ColumnNames.WorkFor)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.WorkFor, Value)
			End Set
		End Property

		Public Overridable Property WorkForSub As String
			Get
				Return MyBase.GetString(ColumnNames.WorkForSub)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.WorkForSub, Value)
			End Set
		End Property

		Public Overridable Property WorkForEnabled As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.WorkForEnabled)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.WorkForEnabled, Value)
			End Set
		End Property

		Public Overridable Property DefaultCC As String
			Get
				Return MyBase.GetString(ColumnNames.DefaultCC)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.DefaultCC, Value)
			End Set
		End Property

		Public Overridable Property DefaultBCC As String
			Get
				Return MyBase.GetString(ColumnNames.DefaultBCC)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.DefaultBCC, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyName As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyName)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyName)
				Else
					Me.CompanyName = MyBase.SetStringAsString(ColumnNames.CompanyName, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Address1 As String
			Get
				If Me.IsColumnNull(ColumnNames.Address1) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Address1)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Address1)
				Else
					Me.Address1 = MyBase.SetStringAsString(ColumnNames.Address1, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Address2 As String
			Get
				If Me.IsColumnNull(ColumnNames.Address2) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Address2)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Address2)
				Else
					Me.Address2 = MyBase.SetStringAsString(ColumnNames.Address2, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ContactPerson As String
			Get
				If Me.IsColumnNull(ColumnNames.ContactPerson) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ContactPerson)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ContactPerson)
				Else
					Me.ContactPerson = MyBase.SetStringAsString(ColumnNames.ContactPerson, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ContactPosition As String
			Get
				If Me.IsColumnNull(ColumnNames.ContactPosition) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ContactPosition)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ContactPosition)
				Else
					Me.ContactPosition = MyBase.SetStringAsString(ColumnNames.ContactPosition, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ApprovingOfficer As String
			Get
				If Me.IsColumnNull(ColumnNames.ApprovingOfficer) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ApprovingOfficer)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ApprovingOfficer)
				Else
					Me.ApprovingOfficer = MyBase.SetStringAsString(ColumnNames.ApprovingOfficer, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OfficerPosition As String
			Get
				If Me.IsColumnNull(ColumnNames.OfficerPosition) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.OfficerPosition)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OfficerPosition)
				Else
					Me.OfficerPosition = MyBase.SetStringAsString(ColumnNames.OfficerPosition, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ContactNumber As String
			Get
				If Me.IsColumnNull(ColumnNames.ContactNumber) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ContactNumber)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ContactNumber)
				Else
					Me.ContactNumber = MyBase.SetStringAsString(ColumnNames.ContactNumber, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmailAddress As String
			Get
				If Me.IsColumnNull(ColumnNames.EmailAddress) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmailAddress)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmailAddress)
				Else
					Me.EmailAddress = MyBase.SetStringAsString(ColumnNames.EmailAddress, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_FaxNumber As String
			Get
				If Me.IsColumnNull(ColumnNames.FaxNumber) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.FaxNumber)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.FaxNumber)
				Else
					Me.FaxNumber = MyBase.SetStringAsString(ColumnNames.FaxNumber, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ND1Start As String
			Get
				If Me.IsColumnNull(ColumnNames.ND1Start) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ND1Start)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ND1Start)
				Else
					Me.ND1Start = MyBase.SetStringAsString(ColumnNames.ND1Start, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ND1End As String
			Get
				If Me.IsColumnNull(ColumnNames.ND1End) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ND1End)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ND1End)
				Else
					Me.ND1End = MyBase.SetStringAsString(ColumnNames.ND1End, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ND2Start As String
			Get
				If Me.IsColumnNull(ColumnNames.ND2Start) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ND2Start)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ND2Start)
				Else
					Me.ND2Start = MyBase.SetStringAsString(ColumnNames.ND2Start, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ND2End As String
			Get
				If Me.IsColumnNull(ColumnNames.ND2End) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ND2End)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ND2End)
				Else
					Me.ND2End = MyBase.SetStringAsString(ColumnNames.ND2End, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Variance As String
			Get
				If Me.IsColumnNull(ColumnNames.Variance) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.Variance)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Variance)
				Else
					Me.Variance = MyBase.SetIntegerAsString(ColumnNames.Variance, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level1 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level1) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level1)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level1)
				Else
					Me.Level1 = MyBase.SetStringAsString(ColumnNames.Level1, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level2 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level2) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level2)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level2)
				Else
					Me.Level2 = MyBase.SetStringAsString(ColumnNames.Level2, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level3 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level3) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level3)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level3)
				Else
					Me.Level3 = MyBase.SetStringAsString(ColumnNames.Level3, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Level4 As String
			Get
				If Me.IsColumnNull(ColumnNames.Level4) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Level4)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Level4)
				Else
					Me.Level4 = MyBase.SetStringAsString(ColumnNames.Level4, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PasswordDays As String
			Get
				If Me.IsColumnNull(ColumnNames.PasswordDays) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.PasswordDays)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PasswordDays)
				Else
					Me.PasswordDays = MyBase.SetIntegerAsString(ColumnNames.PasswordDays, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PasswordAttempt As String
			Get
				If Me.IsColumnNull(ColumnNames.PasswordAttempt) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.PasswordAttempt)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PasswordAttempt)
				Else
					Me.PasswordAttempt = MyBase.SetIntegerAsString(ColumnNames.PasswordAttempt, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_YearCurrent As String
			Get
				If Me.IsColumnNull(ColumnNames.YearCurrent) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.YearCurrent)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.YearCurrent)
				Else
					Me.YearCurrent = MyBase.SetIntegerAsString(ColumnNames.YearCurrent, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_YearStart As String
			Get
				If Me.IsColumnNull(ColumnNames.YearStart) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.YearStart)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.YearStart)
				Else
					Me.YearStart = MyBase.SetDateTimeAsString(ColumnNames.YearStart, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_YearEnd As String
			Get
				If Me.IsColumnNull(ColumnNames.YearEnd) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.YearEnd)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.YearEnd)
				Else
					Me.YearEnd = MyBase.SetDateTimeAsString(ColumnNames.YearEnd, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkFor As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkFor) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.WorkFor)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkFor)
				Else
					Me.WorkFor = MyBase.SetStringAsString(ColumnNames.WorkFor, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkForSub As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkForSub) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.WorkForSub)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkForSub)
				Else
					Me.WorkForSub = MyBase.SetStringAsString(ColumnNames.WorkForSub, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkForEnabled As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkForEnabled) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.WorkForEnabled)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkForEnabled)
				Else
					Me.WorkForEnabled = MyBase.SetBooleanAsString(ColumnNames.WorkForEnabled, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_DefaultCC As String
			Get
				If Me.IsColumnNull(ColumnNames.DefaultCC) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.DefaultCC)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.DefaultCC)
				Else
					Me.DefaultCC = MyBase.SetStringAsString(ColumnNames.DefaultCC, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_DefaultBCC As String
			Get
				If Me.IsColumnNull(ColumnNames.DefaultBCC) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.DefaultBCC)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.DefaultBCC)
				Else
					Me.DefaultBCC = MyBase.SetStringAsString(ColumnNames.DefaultBCC, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyName, Parameters.CompanyName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Address1() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Address1, Parameters.Address1)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Address2() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Address2, Parameters.Address2)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ContactPerson() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ContactPerson, Parameters.ContactPerson)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ContactPosition() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ContactPosition, Parameters.ContactPosition)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ApprovingOfficer() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ApprovingOfficer, Parameters.ApprovingOfficer)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OfficerPosition() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OfficerPosition, Parameters.OfficerPosition)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ContactNumber() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ContactNumber, Parameters.ContactNumber)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmailAddress() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmailAddress, Parameters.EmailAddress)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property FaxNumber() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.FaxNumber, Parameters.FaxNumber)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ND1Start() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ND1Start, Parameters.ND1Start)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ND1End() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ND1End, Parameters.ND1End)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ND2Start() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ND2Start, Parameters.ND2Start)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ND2End() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ND2End, Parameters.ND2End)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Variance() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Variance, Parameters.Variance)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level1() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level1, Parameters.Level1)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level2() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level2, Parameters.Level2)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level3() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level3, Parameters.Level3)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Level4() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Level4, Parameters.Level4)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PasswordDays() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PasswordDays, Parameters.PasswordDays)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PasswordAttempt() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PasswordAttempt, Parameters.PasswordAttempt)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property YearCurrent() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.YearCurrent, Parameters.YearCurrent)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property YearStart() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.YearStart, Parameters.YearStart)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property YearEnd() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.YearEnd, Parameters.YearEnd)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkFor() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkFor, Parameters.WorkFor)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkForSub() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkForSub, Parameters.WorkForSub)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkForEnabled() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkForEnabled, Parameters.WorkForEnabled)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property DefaultCC() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.DefaultCC, Parameters.DefaultCC)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property DefaultBCC() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.DefaultBCC, Parameters.DefaultBCC)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property CompanyName() As WhereParameter 
			Get
				If _CompanyName_W Is Nothing Then
					_CompanyName_W = TearOff.CompanyName
				End If
				Return _CompanyName_W
			End Get
		End Property

		Public ReadOnly Property Address1() As WhereParameter 
			Get
				If _Address1_W Is Nothing Then
					_Address1_W = TearOff.Address1
				End If
				Return _Address1_W
			End Get
		End Property

		Public ReadOnly Property Address2() As WhereParameter 
			Get
				If _Address2_W Is Nothing Then
					_Address2_W = TearOff.Address2
				End If
				Return _Address2_W
			End Get
		End Property

		Public ReadOnly Property ContactPerson() As WhereParameter 
			Get
				If _ContactPerson_W Is Nothing Then
					_ContactPerson_W = TearOff.ContactPerson
				End If
				Return _ContactPerson_W
			End Get
		End Property

		Public ReadOnly Property ContactPosition() As WhereParameter 
			Get
				If _ContactPosition_W Is Nothing Then
					_ContactPosition_W = TearOff.ContactPosition
				End If
				Return _ContactPosition_W
			End Get
		End Property

		Public ReadOnly Property ApprovingOfficer() As WhereParameter 
			Get
				If _ApprovingOfficer_W Is Nothing Then
					_ApprovingOfficer_W = TearOff.ApprovingOfficer
				End If
				Return _ApprovingOfficer_W
			End Get
		End Property

		Public ReadOnly Property OfficerPosition() As WhereParameter 
			Get
				If _OfficerPosition_W Is Nothing Then
					_OfficerPosition_W = TearOff.OfficerPosition
				End If
				Return _OfficerPosition_W
			End Get
		End Property

		Public ReadOnly Property ContactNumber() As WhereParameter 
			Get
				If _ContactNumber_W Is Nothing Then
					_ContactNumber_W = TearOff.ContactNumber
				End If
				Return _ContactNumber_W
			End Get
		End Property

		Public ReadOnly Property EmailAddress() As WhereParameter 
			Get
				If _EmailAddress_W Is Nothing Then
					_EmailAddress_W = TearOff.EmailAddress
				End If
				Return _EmailAddress_W
			End Get
		End Property

		Public ReadOnly Property FaxNumber() As WhereParameter 
			Get
				If _FaxNumber_W Is Nothing Then
					_FaxNumber_W = TearOff.FaxNumber
				End If
				Return _FaxNumber_W
			End Get
		End Property

		Public ReadOnly Property ND1Start() As WhereParameter 
			Get
				If _ND1Start_W Is Nothing Then
					_ND1Start_W = TearOff.ND1Start
				End If
				Return _ND1Start_W
			End Get
		End Property

		Public ReadOnly Property ND1End() As WhereParameter 
			Get
				If _ND1End_W Is Nothing Then
					_ND1End_W = TearOff.ND1End
				End If
				Return _ND1End_W
			End Get
		End Property

		Public ReadOnly Property ND2Start() As WhereParameter 
			Get
				If _ND2Start_W Is Nothing Then
					_ND2Start_W = TearOff.ND2Start
				End If
				Return _ND2Start_W
			End Get
		End Property

		Public ReadOnly Property ND2End() As WhereParameter 
			Get
				If _ND2End_W Is Nothing Then
					_ND2End_W = TearOff.ND2End
				End If
				Return _ND2End_W
			End Get
		End Property

		Public ReadOnly Property Variance() As WhereParameter 
			Get
				If _Variance_W Is Nothing Then
					_Variance_W = TearOff.Variance
				End If
				Return _Variance_W
			End Get
		End Property

		Public ReadOnly Property Level1() As WhereParameter 
			Get
				If _Level1_W Is Nothing Then
					_Level1_W = TearOff.Level1
				End If
				Return _Level1_W
			End Get
		End Property

		Public ReadOnly Property Level2() As WhereParameter 
			Get
				If _Level2_W Is Nothing Then
					_Level2_W = TearOff.Level2
				End If
				Return _Level2_W
			End Get
		End Property

		Public ReadOnly Property Level3() As WhereParameter 
			Get
				If _Level3_W Is Nothing Then
					_Level3_W = TearOff.Level3
				End If
				Return _Level3_W
			End Get
		End Property

		Public ReadOnly Property Level4() As WhereParameter 
			Get
				If _Level4_W Is Nothing Then
					_Level4_W = TearOff.Level4
				End If
				Return _Level4_W
			End Get
		End Property

		Public ReadOnly Property PasswordDays() As WhereParameter 
			Get
				If _PasswordDays_W Is Nothing Then
					_PasswordDays_W = TearOff.PasswordDays
				End If
				Return _PasswordDays_W
			End Get
		End Property

		Public ReadOnly Property PasswordAttempt() As WhereParameter 
			Get
				If _PasswordAttempt_W Is Nothing Then
					_PasswordAttempt_W = TearOff.PasswordAttempt
				End If
				Return _PasswordAttempt_W
			End Get
		End Property

		Public ReadOnly Property YearCurrent() As WhereParameter 
			Get
				If _YearCurrent_W Is Nothing Then
					_YearCurrent_W = TearOff.YearCurrent
				End If
				Return _YearCurrent_W
			End Get
		End Property

		Public ReadOnly Property YearStart() As WhereParameter 
			Get
				If _YearStart_W Is Nothing Then
					_YearStart_W = TearOff.YearStart
				End If
				Return _YearStart_W
			End Get
		End Property

		Public ReadOnly Property YearEnd() As WhereParameter 
			Get
				If _YearEnd_W Is Nothing Then
					_YearEnd_W = TearOff.YearEnd
				End If
				Return _YearEnd_W
			End Get
		End Property

		Public ReadOnly Property WorkFor() As WhereParameter 
			Get
				If _WorkFor_W Is Nothing Then
					_WorkFor_W = TearOff.WorkFor
				End If
				Return _WorkFor_W
			End Get
		End Property

		Public ReadOnly Property WorkForSub() As WhereParameter 
			Get
				If _WorkForSub_W Is Nothing Then
					_WorkForSub_W = TearOff.WorkForSub
				End If
				Return _WorkForSub_W
			End Get
		End Property

		Public ReadOnly Property WorkForEnabled() As WhereParameter 
			Get
				If _WorkForEnabled_W Is Nothing Then
					_WorkForEnabled_W = TearOff.WorkForEnabled
				End If
				Return _WorkForEnabled_W
			End Get
		End Property

		Public ReadOnly Property DefaultCC() As WhereParameter 
			Get
				If _DefaultCC_W Is Nothing Then
					_DefaultCC_W = TearOff.DefaultCC
				End If
				Return _DefaultCC_W
			End Get
		End Property

		Public ReadOnly Property DefaultBCC() As WhereParameter 
			Get
				If _DefaultBCC_W Is Nothing Then
					_DefaultBCC_W = TearOff.DefaultBCC
				End If
				Return _DefaultBCC_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _CompanyID_W As WhereParameter = Nothing
		Private _CompanyName_W As WhereParameter = Nothing
		Private _Address1_W As WhereParameter = Nothing
		Private _Address2_W As WhereParameter = Nothing
		Private _ContactPerson_W As WhereParameter = Nothing
		Private _ContactPosition_W As WhereParameter = Nothing
		Private _ApprovingOfficer_W As WhereParameter = Nothing
		Private _OfficerPosition_W As WhereParameter = Nothing
		Private _ContactNumber_W As WhereParameter = Nothing
		Private _EmailAddress_W As WhereParameter = Nothing
		Private _FaxNumber_W As WhereParameter = Nothing
		Private _ND1Start_W As WhereParameter = Nothing
		Private _ND1End_W As WhereParameter = Nothing
		Private _ND2Start_W As WhereParameter = Nothing
		Private _ND2End_W As WhereParameter = Nothing
		Private _Variance_W As WhereParameter = Nothing
		Private _Level1_W As WhereParameter = Nothing
		Private _Level2_W As WhereParameter = Nothing
		Private _Level3_W As WhereParameter = Nothing
		Private _Level4_W As WhereParameter = Nothing
		Private _PasswordDays_W As WhereParameter = Nothing
		Private _PasswordAttempt_W As WhereParameter = Nothing
		Private _YearCurrent_W As WhereParameter = Nothing
		Private _YearStart_W As WhereParameter = Nothing
		Private _YearEnd_W As WhereParameter = Nothing
		Private _WorkFor_W As WhereParameter = Nothing
		Private _WorkForSub_W As WhereParameter = Nothing
		Private _WorkForEnabled_W As WhereParameter = Nothing
		Private _DefaultCC_W As WhereParameter = Nothing
		Private _DefaultBCC_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_CompanyID_W = Nothing
			_CompanyName_W = Nothing
			_Address1_W = Nothing
			_Address2_W = Nothing
			_ContactPerson_W = Nothing
			_ContactPosition_W = Nothing
			_ApprovingOfficer_W = Nothing
			_OfficerPosition_W = Nothing
			_ContactNumber_W = Nothing
			_EmailAddress_W = Nothing
			_FaxNumber_W = Nothing
			_ND1Start_W = Nothing
			_ND1End_W = Nothing
			_ND2Start_W = Nothing
			_ND2End_W = Nothing
			_Variance_W = Nothing
			_Level1_W = Nothing
			_Level2_W = Nothing
			_Level3_W = Nothing
			_Level4_W = Nothing
			_PasswordDays_W = Nothing
			_PasswordAttempt_W = Nothing
			_YearCurrent_W = Nothing
			_YearStart_W = Nothing
			_YearEnd_W = Nothing
			_WorkFor_W = Nothing
			_WorkForSub_W = Nothing
			_WorkForEnabled_W = Nothing
			_DefaultCC_W = Nothing
			_DefaultBCC_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpdDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyName, Parameters.CompanyName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Address1() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Address1, Parameters.Address1)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Address2() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Address2, Parameters.Address2)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ContactPerson() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ContactPerson, Parameters.ContactPerson)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ContactPosition() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ContactPosition, Parameters.ContactPosition)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ApprovingOfficer() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ApprovingOfficer, Parameters.ApprovingOfficer)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OfficerPosition() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OfficerPosition, Parameters.OfficerPosition)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ContactNumber() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ContactNumber, Parameters.ContactNumber)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmailAddress() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmailAddress, Parameters.EmailAddress)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property FaxNumber() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.FaxNumber, Parameters.FaxNumber)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ND1Start() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ND1Start, Parameters.ND1Start)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ND1End() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ND1End, Parameters.ND1End)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ND2Start() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ND2Start, Parameters.ND2Start)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ND2End() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ND2End, Parameters.ND2End)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Variance() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Variance, Parameters.Variance)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level1() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level1, Parameters.Level1)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level2() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level2, Parameters.Level2)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level3() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level3, Parameters.Level3)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Level4() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Level4, Parameters.Level4)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PasswordDays() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PasswordDays, Parameters.PasswordDays)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PasswordAttempt() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PasswordAttempt, Parameters.PasswordAttempt)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property YearCurrent() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.YearCurrent, Parameters.YearCurrent)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property YearStart() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.YearStart, Parameters.YearStart)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property YearEnd() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.YearEnd, Parameters.YearEnd)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkFor() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkFor, Parameters.WorkFor)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkForSub() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkForSub, Parameters.WorkForSub)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkForEnabled() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkForEnabled, Parameters.WorkForEnabled)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property DefaultCC() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DefaultCC, Parameters.DefaultCC)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property DefaultBCC() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DefaultBCC, Parameters.DefaultBCC)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property CompanyName() As AggregateParameter 
			Get
				If _CompanyName_W Is Nothing Then
					_CompanyName_W = TearOff.CompanyName
				End If
				Return _CompanyName_W
			End Get
		End Property

		Public ReadOnly Property Address1() As AggregateParameter 
			Get
				If _Address1_W Is Nothing Then
					_Address1_W = TearOff.Address1
				End If
				Return _Address1_W
			End Get
		End Property

		Public ReadOnly Property Address2() As AggregateParameter 
			Get
				If _Address2_W Is Nothing Then
					_Address2_W = TearOff.Address2
				End If
				Return _Address2_W
			End Get
		End Property

		Public ReadOnly Property ContactPerson() As AggregateParameter 
			Get
				If _ContactPerson_W Is Nothing Then
					_ContactPerson_W = TearOff.ContactPerson
				End If
				Return _ContactPerson_W
			End Get
		End Property

		Public ReadOnly Property ContactPosition() As AggregateParameter 
			Get
				If _ContactPosition_W Is Nothing Then
					_ContactPosition_W = TearOff.ContactPosition
				End If
				Return _ContactPosition_W
			End Get
		End Property

		Public ReadOnly Property ApprovingOfficer() As AggregateParameter 
			Get
				If _ApprovingOfficer_W Is Nothing Then
					_ApprovingOfficer_W = TearOff.ApprovingOfficer
				End If
				Return _ApprovingOfficer_W
			End Get
		End Property

		Public ReadOnly Property OfficerPosition() As AggregateParameter 
			Get
				If _OfficerPosition_W Is Nothing Then
					_OfficerPosition_W = TearOff.OfficerPosition
				End If
				Return _OfficerPosition_W
			End Get
		End Property

		Public ReadOnly Property ContactNumber() As AggregateParameter 
			Get
				If _ContactNumber_W Is Nothing Then
					_ContactNumber_W = TearOff.ContactNumber
				End If
				Return _ContactNumber_W
			End Get
		End Property

		Public ReadOnly Property EmailAddress() As AggregateParameter 
			Get
				If _EmailAddress_W Is Nothing Then
					_EmailAddress_W = TearOff.EmailAddress
				End If
				Return _EmailAddress_W
			End Get
		End Property

		Public ReadOnly Property FaxNumber() As AggregateParameter 
			Get
				If _FaxNumber_W Is Nothing Then
					_FaxNumber_W = TearOff.FaxNumber
				End If
				Return _FaxNumber_W
			End Get
		End Property

		Public ReadOnly Property ND1Start() As AggregateParameter 
			Get
				If _ND1Start_W Is Nothing Then
					_ND1Start_W = TearOff.ND1Start
				End If
				Return _ND1Start_W
			End Get
		End Property

		Public ReadOnly Property ND1End() As AggregateParameter 
			Get
				If _ND1End_W Is Nothing Then
					_ND1End_W = TearOff.ND1End
				End If
				Return _ND1End_W
			End Get
		End Property

		Public ReadOnly Property ND2Start() As AggregateParameter 
			Get
				If _ND2Start_W Is Nothing Then
					_ND2Start_W = TearOff.ND2Start
				End If
				Return _ND2Start_W
			End Get
		End Property

		Public ReadOnly Property ND2End() As AggregateParameter 
			Get
				If _ND2End_W Is Nothing Then
					_ND2End_W = TearOff.ND2End
				End If
				Return _ND2End_W
			End Get
		End Property

		Public ReadOnly Property Variance() As AggregateParameter 
			Get
				If _Variance_W Is Nothing Then
					_Variance_W = TearOff.Variance
				End If
				Return _Variance_W
			End Get
		End Property

		Public ReadOnly Property Level1() As AggregateParameter 
			Get
				If _Level1_W Is Nothing Then
					_Level1_W = TearOff.Level1
				End If
				Return _Level1_W
			End Get
		End Property

		Public ReadOnly Property Level2() As AggregateParameter 
			Get
				If _Level2_W Is Nothing Then
					_Level2_W = TearOff.Level2
				End If
				Return _Level2_W
			End Get
		End Property

		Public ReadOnly Property Level3() As AggregateParameter 
			Get
				If _Level3_W Is Nothing Then
					_Level3_W = TearOff.Level3
				End If
				Return _Level3_W
			End Get
		End Property

		Public ReadOnly Property Level4() As AggregateParameter 
			Get
				If _Level4_W Is Nothing Then
					_Level4_W = TearOff.Level4
				End If
				Return _Level4_W
			End Get
		End Property

		Public ReadOnly Property PasswordDays() As AggregateParameter 
			Get
				If _PasswordDays_W Is Nothing Then
					_PasswordDays_W = TearOff.PasswordDays
				End If
				Return _PasswordDays_W
			End Get
		End Property

		Public ReadOnly Property PasswordAttempt() As AggregateParameter 
			Get
				If _PasswordAttempt_W Is Nothing Then
					_PasswordAttempt_W = TearOff.PasswordAttempt
				End If
				Return _PasswordAttempt_W
			End Get
		End Property

		Public ReadOnly Property YearCurrent() As AggregateParameter 
			Get
				If _YearCurrent_W Is Nothing Then
					_YearCurrent_W = TearOff.YearCurrent
				End If
				Return _YearCurrent_W
			End Get
		End Property

		Public ReadOnly Property YearStart() As AggregateParameter 
			Get
				If _YearStart_W Is Nothing Then
					_YearStart_W = TearOff.YearStart
				End If
				Return _YearStart_W
			End Get
		End Property

		Public ReadOnly Property YearEnd() As AggregateParameter 
			Get
				If _YearEnd_W Is Nothing Then
					_YearEnd_W = TearOff.YearEnd
				End If
				Return _YearEnd_W
			End Get
		End Property

		Public ReadOnly Property WorkFor() As AggregateParameter 
			Get
				If _WorkFor_W Is Nothing Then
					_WorkFor_W = TearOff.WorkFor
				End If
				Return _WorkFor_W
			End Get
		End Property

		Public ReadOnly Property WorkForSub() As AggregateParameter 
			Get
				If _WorkForSub_W Is Nothing Then
					_WorkForSub_W = TearOff.WorkForSub
				End If
				Return _WorkForSub_W
			End Get
		End Property

		Public ReadOnly Property WorkForEnabled() As AggregateParameter 
			Get
				If _WorkForEnabled_W Is Nothing Then
					_WorkForEnabled_W = TearOff.WorkForEnabled
				End If
				Return _WorkForEnabled_W
			End Get
		End Property

		Public ReadOnly Property DefaultCC() As AggregateParameter 
			Get
				If _DefaultCC_W Is Nothing Then
					_DefaultCC_W = TearOff.DefaultCC
				End If
				Return _DefaultCC_W
			End Get
		End Property

		Public ReadOnly Property DefaultBCC() As AggregateParameter 
			Get
				If _DefaultBCC_W Is Nothing Then
					_DefaultBCC_W = TearOff.DefaultBCC
				End If
				Return _DefaultBCC_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _CompanyID_W As AggregateParameter = Nothing
		Private _CompanyName_W As AggregateParameter = Nothing
		Private _Address1_W As AggregateParameter = Nothing
		Private _Address2_W As AggregateParameter = Nothing
		Private _ContactPerson_W As AggregateParameter = Nothing
		Private _ContactPosition_W As AggregateParameter = Nothing
		Private _ApprovingOfficer_W As AggregateParameter = Nothing
		Private _OfficerPosition_W As AggregateParameter = Nothing
		Private _ContactNumber_W As AggregateParameter = Nothing
		Private _EmailAddress_W As AggregateParameter = Nothing
		Private _FaxNumber_W As AggregateParameter = Nothing
		Private _ND1Start_W As AggregateParameter = Nothing
		Private _ND1End_W As AggregateParameter = Nothing
		Private _ND2Start_W As AggregateParameter = Nothing
		Private _ND2End_W As AggregateParameter = Nothing
		Private _Variance_W As AggregateParameter = Nothing
		Private _Level1_W As AggregateParameter = Nothing
		Private _Level2_W As AggregateParameter = Nothing
		Private _Level3_W As AggregateParameter = Nothing
		Private _Level4_W As AggregateParameter = Nothing
		Private _PasswordDays_W As AggregateParameter = Nothing
		Private _PasswordAttempt_W As AggregateParameter = Nothing
		Private _YearCurrent_W As AggregateParameter = Nothing
		Private _YearStart_W As AggregateParameter = Nothing
		Private _YearEnd_W As AggregateParameter = Nothing
		Private _WorkFor_W As AggregateParameter = Nothing
		Private _WorkForSub_W As AggregateParameter = Nothing
		Private _WorkForEnabled_W As AggregateParameter = Nothing
		Private _DefaultCC_W As AggregateParameter = Nothing
		Private _DefaultBCC_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_CompanyID_W = Nothing
		_CompanyName_W = Nothing
		_Address1_W = Nothing
		_Address2_W = Nothing
		_ContactPerson_W = Nothing
		_ContactPosition_W = Nothing
		_ApprovingOfficer_W = Nothing
		_OfficerPosition_W = Nothing
		_ContactNumber_W = Nothing
		_EmailAddress_W = Nothing
		_FaxNumber_W = Nothing
		_ND1Start_W = Nothing
		_ND1End_W = Nothing
		_ND2Start_W = Nothing
		_ND2End_W = Nothing
		_Variance_W = Nothing
		_Level1_W = Nothing
		_Level2_W = Nothing
		_Level3_W = Nothing
		_Level4_W = Nothing
		_PasswordDays_W = Nothing
		_PasswordAttempt_W = Nothing
		_YearCurrent_W = Nothing
		_YearStart_W = Nothing
		_YearEnd_W = Nothing
		_WorkFor_W = Nothing
		_WorkForSub_W = Nothing
		_WorkForEnabled_W = Nothing
		_DefaultCC_W = Nothing
		_DefaultBCC_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpdDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_CompanyInsert]" 
	    
		CreateParameters(cmd)
		    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_CompanyUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_CompanyDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyName)
		p.SourceColumn = ColumnNames.CompanyName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Address1)
		p.SourceColumn = ColumnNames.Address1
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Address2)
		p.SourceColumn = ColumnNames.Address2
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ContactPerson)
		p.SourceColumn = ColumnNames.ContactPerson
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ContactPosition)
		p.SourceColumn = ColumnNames.ContactPosition
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ApprovingOfficer)
		p.SourceColumn = ColumnNames.ApprovingOfficer
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OfficerPosition)
		p.SourceColumn = ColumnNames.OfficerPosition
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ContactNumber)
		p.SourceColumn = ColumnNames.ContactNumber
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmailAddress)
		p.SourceColumn = ColumnNames.EmailAddress
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.FaxNumber)
		p.SourceColumn = ColumnNames.FaxNumber
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ND1Start)
		p.SourceColumn = ColumnNames.ND1Start
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ND1End)
		p.SourceColumn = ColumnNames.ND1End
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ND2Start)
		p.SourceColumn = ColumnNames.ND2Start
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ND2End)
		p.SourceColumn = ColumnNames.ND2End
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Variance)
		p.SourceColumn = ColumnNames.Variance
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level1)
		p.SourceColumn = ColumnNames.Level1
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level2)
		p.SourceColumn = ColumnNames.Level2
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level3)
		p.SourceColumn = ColumnNames.Level3
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Level4)
		p.SourceColumn = ColumnNames.Level4
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PasswordDays)
		p.SourceColumn = ColumnNames.PasswordDays
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PasswordAttempt)
		p.SourceColumn = ColumnNames.PasswordAttempt
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.YearCurrent)
		p.SourceColumn = ColumnNames.YearCurrent
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.YearStart)
		p.SourceColumn = ColumnNames.YearStart
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.YearEnd)
		p.SourceColumn = ColumnNames.YearEnd
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkFor)
		p.SourceColumn = ColumnNames.WorkFor
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkForSub)
		p.SourceColumn = ColumnNames.WorkForSub
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkForEnabled)
		p.SourceColumn = ColumnNames.WorkForEnabled
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.DefaultCC)
		p.SourceColumn = ColumnNames.DefaultCC
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.DefaultBCC)
		p.SourceColumn = ColumnNames.DefaultBCC
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

