
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _TimeCreditDtl
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "TimeCreditDtl"
			Me.MappingName = "TimeCreditDtl"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TimeCreditDtlLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_TimeCreditDtl.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TimeCreditDtlLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TcID As SqlParameter
			Get
				Return New SqlParameter("@TcID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Date1 As SqlParameter
			Get
				Return New SqlParameter("@Date", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Used As SqlParameter
			Get
				Return New SqlParameter("@Used", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TardyUT As SqlParameter
			Get
				Return New SqlParameter("@TardyUT", SqlDbType.VarChar, 1)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const TcID As String = "tcID"
        Public Const Date1 As String = "Date"
        Public Const Used As String = "Used"
        Public Const TardyUT As String = "TardyUT"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _TimeCreditDtl.PropertyNames.ID
				ht(TcID) = _TimeCreditDtl.PropertyNames.TcID
				ht(Date1) = _TimeCreditDtl.PropertyNames.Date1
				ht(Used) = _TimeCreditDtl.PropertyNames.Used
				ht(TardyUT) = _TimeCreditDtl.PropertyNames.TardyUT

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const TcID As String = "TcID"
        Public Const Date1 As String = "Date1"
        Public Const Used As String = "Used"
        Public Const TardyUT As String = "TardyUT"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _TimeCreditDtl.ColumnNames.ID
				ht(TcID) = _TimeCreditDtl.ColumnNames.TcID
				ht(Date1) = _TimeCreditDtl.ColumnNames.Date1
				ht(Used) = _TimeCreditDtl.ColumnNames.Used
				ht(TardyUT) = _TimeCreditDtl.ColumnNames.TardyUT

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const TcID As String = "s_TcID"
        Public Const Date1 As String = "s_Date1"
        Public Const Used As String = "s_Used"
        Public Const TardyUT As String = "s_TardyUT"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property TcID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.TcID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.TcID, Value)
			End Set
		End Property

		Public Overridable Property Date1 As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.Date1)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.Date1, Value)
			End Set
		End Property

		Public Overridable Property Used As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.Used)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.Used, Value)
			End Set
		End Property

		Public Overridable Property TardyUT As String
			Get
				Return MyBase.GetString(ColumnNames.TardyUT)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.TardyUT, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TcID As String
			Get
				If Me.IsColumnNull(ColumnNames.TcID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.TcID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TcID)
				Else
					Me.TcID = MyBase.SetIntegerAsString(ColumnNames.TcID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Date1 As String
			Get
				If Me.IsColumnNull(ColumnNames.Date1) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.Date1)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Date1)
				Else
					Me.Date1 = MyBase.SetDateTimeAsString(ColumnNames.Date1, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Used As String
			Get
				If Me.IsColumnNull(ColumnNames.Used) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.Used)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Used)
				Else
					Me.Used = MyBase.SetDecimalAsString(ColumnNames.Used, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TardyUT As String
			Get
				If Me.IsColumnNull(ColumnNames.TardyUT) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.TardyUT)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TardyUT)
				Else
					Me.TardyUT = MyBase.SetStringAsString(ColumnNames.TardyUT, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TcID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TcID, Parameters.TcID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Date1() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Date1, Parameters.Date1)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Used() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Used, Parameters.Used)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TardyUT() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TardyUT, Parameters.TardyUT)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property TcID() As WhereParameter 
			Get
				If _TcID_W Is Nothing Then
					_TcID_W = TearOff.TcID
				End If
				Return _TcID_W
			End Get
		End Property

		Public ReadOnly Property Date1() As WhereParameter 
			Get
				If _Date1_W Is Nothing Then
					_Date1_W = TearOff.Date1
				End If
				Return _Date1_W
			End Get
		End Property

		Public ReadOnly Property Used() As WhereParameter 
			Get
				If _Used_W Is Nothing Then
					_Used_W = TearOff.Used
				End If
				Return _Used_W
			End Get
		End Property

		Public ReadOnly Property TardyUT() As WhereParameter 
			Get
				If _TardyUT_W Is Nothing Then
					_TardyUT_W = TearOff.TardyUT
				End If
				Return _TardyUT_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _TcID_W As WhereParameter = Nothing
		Private _Date1_W As WhereParameter = Nothing
		Private _Used_W As WhereParameter = Nothing
		Private _TardyUT_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_TcID_W = Nothing
			_Date1_W = Nothing
			_Used_W = Nothing
			_TardyUT_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TcID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TcID, Parameters.TcID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Date1() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Date1, Parameters.Date1)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Used() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Used, Parameters.Used)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TardyUT() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TardyUT, Parameters.TardyUT)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property TcID() As AggregateParameter 
			Get
				If _TcID_W Is Nothing Then
					_TcID_W = TearOff.TcID
				End If
				Return _TcID_W
			End Get
		End Property

		Public ReadOnly Property Date1() As AggregateParameter 
			Get
				If _Date1_W Is Nothing Then
					_Date1_W = TearOff.Date1
				End If
				Return _Date1_W
			End Get
		End Property

		Public ReadOnly Property Used() As AggregateParameter 
			Get
				If _Used_W Is Nothing Then
					_Used_W = TearOff.Used
				End If
				Return _Used_W
			End Get
		End Property

		Public ReadOnly Property TardyUT() As AggregateParameter 
			Get
				If _TardyUT_W Is Nothing Then
					_TardyUT_W = TearOff.TardyUT
				End If
				Return _TardyUT_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _TcID_W As AggregateParameter = Nothing
		Private _Date1_W As AggregateParameter = Nothing
		Private _Used_W As AggregateParameter = Nothing
		Private _TardyUT_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_TcID_W = Nothing
		_Date1_W = Nothing
		_Used_W = Nothing
		_TardyUT_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeCreditDtlInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeCreditDtlUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeCreditDtlDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TcID)
		p.SourceColumn = ColumnNames.TcID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Date1)
		p.SourceColumn = ColumnNames.Date1
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Used)
		p.SourceColumn = ColumnNames.Used
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TardyUT)
		p.SourceColumn = ColumnNames.TardyUT
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

