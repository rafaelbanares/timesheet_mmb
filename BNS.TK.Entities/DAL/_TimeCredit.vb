
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _TimeCredit
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "TimeCredit"
			Me.MappingName = "TimeCredit"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TimeCreditLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal TcId As Long) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_TimeCredit.Parameters.TcId, TcId)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_TimeCreditLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property TcId As SqlParameter
			Get
				Return New SqlParameter("@TcId", SqlDbType.BigInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Date1 As SqlParameter
			Get
				Return New SqlParameter("@Date", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TardyHrs As SqlParameter
			Get
				Return New SqlParameter("@TardyHrs", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property UTHrs As SqlParameter
			Get
				Return New SqlParameter("@UTHrs", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const TcId As String = "tcId"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const Date1 As String = "Date"
        Public Const TardyHrs As String = "TardyHrs"
        Public Const UTHrs As String = "UTHrs"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(TcId) = _TimeCredit.PropertyNames.TcId
				ht(CompanyID) = _TimeCredit.PropertyNames.CompanyID
				ht(EmployeeID) = _TimeCredit.PropertyNames.EmployeeID
				ht(Date1) = _TimeCredit.PropertyNames.Date1
				ht(TardyHrs) = _TimeCredit.PropertyNames.TardyHrs
				ht(UTHrs) = _TimeCredit.PropertyNames.UTHrs
				ht(LastUpdBy) = _TimeCredit.PropertyNames.LastUpdBy
				ht(LastUpdDate) = _TimeCredit.PropertyNames.LastUpdDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const TcId As String = "TcId"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const Date1 As String = "Date1"
        Public Const TardyHrs As String = "TardyHrs"
        Public Const UTHrs As String = "UTHrs"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(TcId) = _TimeCredit.ColumnNames.TcId
				ht(CompanyID) = _TimeCredit.ColumnNames.CompanyID
				ht(EmployeeID) = _TimeCredit.ColumnNames.EmployeeID
				ht(Date1) = _TimeCredit.ColumnNames.Date1
				ht(TardyHrs) = _TimeCredit.ColumnNames.TardyHrs
				ht(UTHrs) = _TimeCredit.ColumnNames.UTHrs
				ht(LastUpdBy) = _TimeCredit.ColumnNames.LastUpdBy
				ht(LastUpdDate) = _TimeCredit.ColumnNames.LastUpdDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const TcId As String = "s_TcId"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const Date1 As String = "s_Date1"
        Public Const TardyHrs As String = "s_TardyHrs"
        Public Const UTHrs As String = "s_UTHrs"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property TcId As Long
			Get
				Return MyBase.GetLong(ColumnNames.TcId)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.TcId, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property Date1 As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.Date1)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.Date1, Value)
			End Set
		End Property

		Public Overridable Property TardyHrs As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.TardyHrs)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.TardyHrs, Value)
			End Set
		End Property

		Public Overridable Property UTHrs As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.UTHrs)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.UTHrs, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_TcId As String
			Get
				If Me.IsColumnNull(ColumnNames.TcId) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.TcId)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TcId)
				Else
					Me.TcId = MyBase.SetLongAsString(ColumnNames.TcId, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Date1 As String
			Get
				If Me.IsColumnNull(ColumnNames.Date1) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.Date1)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Date1)
				Else
					Me.Date1 = MyBase.SetDateTimeAsString(ColumnNames.Date1, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TardyHrs As String
			Get
				If Me.IsColumnNull(ColumnNames.TardyHrs) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.TardyHrs)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TardyHrs)
				Else
					Me.TardyHrs = MyBase.SetDecimalAsString(ColumnNames.TardyHrs, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_UTHrs As String
			Get
				If Me.IsColumnNull(ColumnNames.UTHrs) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.UTHrs)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.UTHrs)
				Else
					Me.UTHrs = MyBase.SetDecimalAsString(ColumnNames.UTHrs, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property TcId() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TcId, Parameters.TcId)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Date1() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Date1, Parameters.Date1)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TardyHrs() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TardyHrs, Parameters.TardyHrs)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property UTHrs() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.UTHrs, Parameters.UTHrs)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property TcId() As WhereParameter 
			Get
				If _TcId_W Is Nothing Then
					_TcId_W = TearOff.TcId
				End If
				Return _TcId_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property Date1() As WhereParameter 
			Get
				If _Date1_W Is Nothing Then
					_Date1_W = TearOff.Date1
				End If
				Return _Date1_W
			End Get
		End Property

		Public ReadOnly Property TardyHrs() As WhereParameter 
			Get
				If _TardyHrs_W Is Nothing Then
					_TardyHrs_W = TearOff.TardyHrs
				End If
				Return _TardyHrs_W
			End Get
		End Property

		Public ReadOnly Property UTHrs() As WhereParameter 
			Get
				If _UTHrs_W Is Nothing Then
					_UTHrs_W = TearOff.UTHrs
				End If
				Return _UTHrs_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _TcId_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _Date1_W As WhereParameter = Nothing
		Private _TardyHrs_W As WhereParameter = Nothing
		Private _UTHrs_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_TcId_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_Date1_W = Nothing
			_TardyHrs_W = Nothing
			_UTHrs_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpdDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property TcId() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TcId, Parameters.TcId)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Date1() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Date1, Parameters.Date1)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TardyHrs() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TardyHrs, Parameters.TardyHrs)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property UTHrs() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UTHrs, Parameters.UTHrs)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property TcId() As AggregateParameter 
			Get
				If _TcId_W Is Nothing Then
					_TcId_W = TearOff.TcId
				End If
				Return _TcId_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property Date1() As AggregateParameter 
			Get
				If _Date1_W Is Nothing Then
					_Date1_W = TearOff.Date1
				End If
				Return _Date1_W
			End Get
		End Property

		Public ReadOnly Property TardyHrs() As AggregateParameter 
			Get
				If _TardyHrs_W Is Nothing Then
					_TardyHrs_W = TearOff.TardyHrs
				End If
				Return _TardyHrs_W
			End Get
		End Property

		Public ReadOnly Property UTHrs() As AggregateParameter 
			Get
				If _UTHrs_W Is Nothing Then
					_UTHrs_W = TearOff.UTHrs
				End If
				Return _UTHrs_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _TcId_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _Date1_W As AggregateParameter = Nothing
		Private _TardyHrs_W As AggregateParameter = Nothing
		Private _UTHrs_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_TcId_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_Date1_W = Nothing
		_TardyHrs_W = Nothing
		_UTHrs_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpdDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeCreditInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.TcId.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeCreditUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_TimeCreditDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.TcId)
		p.SourceColumn = ColumnNames.TcId
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.TcId)
		p.SourceColumn = ColumnNames.TcId
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Date1)
		p.SourceColumn = ColumnNames.Date1
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TardyHrs)
		p.SourceColumn = ColumnNames.TardyHrs
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.UTHrs)
		p.SourceColumn = ColumnNames.UTHrs
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

