
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

Public MustInherit Class _LeaveEarnDtl
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "LeaveEarnDtl"
			Me.MappingName = "LeaveEarnDtl"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveEarnDtlLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal Row_id As Long) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_LeaveEarnDtl.Parameters.Row_id, Row_id)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveEarnDtlLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property Row_id As SqlParameter
			Get
				Return New SqlParameter("@Row_id", SqlDbType.BigInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LeaveEarnID As SqlParameter
			Get
				Return New SqlParameter("@LeaveEarnID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Leavecode As SqlParameter
			Get
				Return New SqlParameter("@Leavecode", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property Earned As SqlParameter
			Get
				Return New SqlParameter("@Earned", SqlDbType.Decimal, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const Row_id As String = "row_id"
        Public Const LeaveEarnID As String = "LeaveEarnID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const Leavecode As String = "leavecode"
        Public Const Earned As String = "earned"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(Row_id) = _LeaveEarnDtl.PropertyNames.Row_id
				ht(LeaveEarnID) = _LeaveEarnDtl.PropertyNames.LeaveEarnID
				ht(CompanyID) = _LeaveEarnDtl.PropertyNames.CompanyID
				ht(EmployeeID) = _LeaveEarnDtl.PropertyNames.EmployeeID
				ht(Leavecode) = _LeaveEarnDtl.PropertyNames.Leavecode
				ht(Earned) = _LeaveEarnDtl.PropertyNames.Earned

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const Row_id As String = "Row_id"
        Public Const LeaveEarnID As String = "LeaveEarnID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const Leavecode As String = "Leavecode"
        Public Const Earned As String = "Earned"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(Row_id) = _LeaveEarnDtl.ColumnNames.Row_id
				ht(LeaveEarnID) = _LeaveEarnDtl.ColumnNames.LeaveEarnID
				ht(CompanyID) = _LeaveEarnDtl.ColumnNames.CompanyID
				ht(EmployeeID) = _LeaveEarnDtl.ColumnNames.EmployeeID
				ht(Leavecode) = _LeaveEarnDtl.ColumnNames.Leavecode
				ht(Earned) = _LeaveEarnDtl.ColumnNames.Earned

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const Row_id As String = "s_Row_id"
        Public Const LeaveEarnID As String = "s_LeaveEarnID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const Leavecode As String = "s_Leavecode"
        Public Const Earned As String = "s_Earned"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property Row_id As Long
			Get
				Return MyBase.GetLong(ColumnNames.Row_id)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.Row_id, Value)
			End Set
		End Property

		Public Overridable Property LeaveEarnID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.LeaveEarnID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.LeaveEarnID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property Leavecode As String
			Get
				Return MyBase.GetString(ColumnNames.Leavecode)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Leavecode, Value)
			End Set
		End Property

		Public Overridable Property Earned As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.Earned)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.Earned, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_Row_id As String
			Get
				If Me.IsColumnNull(ColumnNames.Row_id) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.Row_id)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Row_id)
				Else
					Me.Row_id = MyBase.SetLongAsString(ColumnNames.Row_id, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LeaveEarnID As String
			Get
				If Me.IsColumnNull(ColumnNames.LeaveEarnID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.LeaveEarnID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LeaveEarnID)
				Else
					Me.LeaveEarnID = MyBase.SetIntegerAsString(ColumnNames.LeaveEarnID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Leavecode As String
			Get
				If Me.IsColumnNull(ColumnNames.Leavecode) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Leavecode)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Leavecode)
				Else
					Me.Leavecode = MyBase.SetStringAsString(ColumnNames.Leavecode, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Earned As String
			Get
				If Me.IsColumnNull(ColumnNames.Earned) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.Earned)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Earned)
				Else
					Me.Earned = MyBase.SetDecimalAsString(ColumnNames.Earned, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property Row_id() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Row_id, Parameters.Row_id)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LeaveEarnID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveEarnID, Parameters.LeaveEarnID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Leavecode() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Leavecode, Parameters.Leavecode)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Earned() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Earned, Parameters.Earned)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property Row_id() As WhereParameter 
			Get
				If _Row_id_W Is Nothing Then
					_Row_id_W = TearOff.Row_id
				End If
				Return _Row_id_W
			End Get
		End Property

		Public ReadOnly Property LeaveEarnID() As WhereParameter 
			Get
				If _LeaveEarnID_W Is Nothing Then
					_LeaveEarnID_W = TearOff.LeaveEarnID
				End If
				Return _LeaveEarnID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property Leavecode() As WhereParameter 
			Get
				If _Leavecode_W Is Nothing Then
					_Leavecode_W = TearOff.Leavecode
				End If
				Return _Leavecode_W
			End Get
		End Property

		Public ReadOnly Property Earned() As WhereParameter 
			Get
				If _Earned_W Is Nothing Then
					_Earned_W = TearOff.Earned
				End If
				Return _Earned_W
			End Get
		End Property

		Private _Row_id_W As WhereParameter = Nothing
		Private _LeaveEarnID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _Leavecode_W As WhereParameter = Nothing
		Private _Earned_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_Row_id_W = Nothing
			_LeaveEarnID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_Leavecode_W = Nothing
			_Earned_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property Row_id() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Row_id, Parameters.Row_id)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LeaveEarnID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveEarnID, Parameters.LeaveEarnID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Leavecode() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Leavecode, Parameters.Leavecode)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Earned() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Earned, Parameters.Earned)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property Row_id() As AggregateParameter 
			Get
				If _Row_id_W Is Nothing Then
					_Row_id_W = TearOff.Row_id
				End If
				Return _Row_id_W
			End Get
		End Property

		Public ReadOnly Property LeaveEarnID() As AggregateParameter 
			Get
				If _LeaveEarnID_W Is Nothing Then
					_LeaveEarnID_W = TearOff.LeaveEarnID
				End If
				Return _LeaveEarnID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property Leavecode() As AggregateParameter 
			Get
				If _Leavecode_W Is Nothing Then
					_Leavecode_W = TearOff.Leavecode
				End If
				Return _Leavecode_W
			End Get
		End Property

		Public ReadOnly Property Earned() As AggregateParameter 
			Get
				If _Earned_W Is Nothing Then
					_Earned_W = TearOff.Earned
				End If
				Return _Earned_W
			End Get
		End Property

		Private _Row_id_W As AggregateParameter = Nothing
		Private _LeaveEarnID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _Leavecode_W As AggregateParameter = Nothing
		Private _Earned_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_Row_id_W = Nothing
		_LeaveEarnID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_Leavecode_W = Nothing
		_Earned_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveEarnDtlInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.Row_id.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveEarnDtlUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveEarnDtlDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.Row_id)
		p.SourceColumn = ColumnNames.Row_id
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.Row_id)
		p.SourceColumn = ColumnNames.Row_id
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LeaveEarnID)
		p.SourceColumn = ColumnNames.LeaveEarnID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Leavecode)
		p.SourceColumn = ColumnNames.Leavecode
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Earned)
		p.SourceColumn = ColumnNames.Earned
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

