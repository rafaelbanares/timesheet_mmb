
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _SummTimeSheet
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "SummTimeSheet"
			Me.MappingName = "SummTimeSheet"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_SummTimeSheetLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_SummTimeSheet.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_SummTimeSheetLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayStart As SqlParameter
			Get
				Return New SqlParameter("@PayStart", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayEnd As SqlParameter
			Get
				Return New SqlParameter("@PayEnd", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TransStart As SqlParameter
			Get
				Return New SqlParameter("@TransStart", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TransEnd As SqlParameter
			Get
				Return New SqlParameter("@TransEnd", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property RegHours As SqlParameter
			Get
				Return New SqlParameter("@RegHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ND1RegHours As SqlParameter
			Get
				Return New SqlParameter("@ND1RegHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ND2RegHours As SqlParameter
			Get
				Return New SqlParameter("@ND2RegHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Absences As SqlParameter
			Get
				Return New SqlParameter("@Absences", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property TardyHours As SqlParameter
			Get
				Return New SqlParameter("@TardyHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property UTHours As SqlParameter
			Get
				Return New SqlParameter("@UTHours", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property VLDays As SqlParameter
			Get
				Return New SqlParameter("@VLDays", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property SLDays As SqlParameter
			Get
				Return New SqlParameter("@SLDays", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayHoliDaily As SqlParameter
			Get
				Return New SqlParameter("@PayHoliDaily", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayHoliMonthly As SqlParameter
			Get
				Return New SqlParameter("@PayHoliMonthly", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ReOpen As SqlParameter
			Get
				Return New SqlParameter("@ReOpen", SqlDbType.Bit, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedBy As SqlParameter
			Get
				Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property CreatedDate As SqlParameter
			Get
				Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const PayStart As String = "PayStart"
        Public Const PayEnd As String = "PayEnd"
        Public Const TransStart As String = "TransStart"
        Public Const TransEnd As String = "TransEnd"
        Public Const RegHours As String = "RegHours"
        Public Const ND1RegHours As String = "ND1RegHours"
        Public Const ND2RegHours As String = "ND2RegHours"
        Public Const Absences As String = "Absences"
        Public Const TardyHours As String = "TardyHours"
        Public Const UTHours As String = "UTHours"
        Public Const VLDays As String = "VLDays"
        Public Const SLDays As String = "SLDays"
        Public Const PayHoliDaily As String = "PayHoliDaily"
        Public Const PayHoliMonthly As String = "PayHoliMonthly"
        Public Const ReOpen As String = "ReOpen"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpDate As String = "LastUpDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _SummTimeSheet.PropertyNames.ID
				ht(CompanyID) = _SummTimeSheet.PropertyNames.CompanyID
				ht(EmployeeID) = _SummTimeSheet.PropertyNames.EmployeeID
				ht(PayStart) = _SummTimeSheet.PropertyNames.PayStart
				ht(PayEnd) = _SummTimeSheet.PropertyNames.PayEnd
				ht(TransStart) = _SummTimeSheet.PropertyNames.TransStart
				ht(TransEnd) = _SummTimeSheet.PropertyNames.TransEnd
				ht(RegHours) = _SummTimeSheet.PropertyNames.RegHours
				ht(ND1RegHours) = _SummTimeSheet.PropertyNames.ND1RegHours
				ht(ND2RegHours) = _SummTimeSheet.PropertyNames.ND2RegHours
				ht(Absences) = _SummTimeSheet.PropertyNames.Absences
				ht(TardyHours) = _SummTimeSheet.PropertyNames.TardyHours
				ht(UTHours) = _SummTimeSheet.PropertyNames.UTHours
				ht(VLDays) = _SummTimeSheet.PropertyNames.VLDays
				ht(SLDays) = _SummTimeSheet.PropertyNames.SLDays
				ht(PayHoliDaily) = _SummTimeSheet.PropertyNames.PayHoliDaily
				ht(PayHoliMonthly) = _SummTimeSheet.PropertyNames.PayHoliMonthly
				ht(ReOpen) = _SummTimeSheet.PropertyNames.ReOpen
				ht(CreatedBy) = _SummTimeSheet.PropertyNames.CreatedBy
				ht(CreatedDate) = _SummTimeSheet.PropertyNames.CreatedDate
				ht(LastUpdBy) = _SummTimeSheet.PropertyNames.LastUpdBy
				ht(LastUpDate) = _SummTimeSheet.PropertyNames.LastUpDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const PayStart As String = "PayStart"
        Public Const PayEnd As String = "PayEnd"
        Public Const TransStart As String = "TransStart"
        Public Const TransEnd As String = "TransEnd"
        Public Const RegHours As String = "RegHours"
        Public Const ND1RegHours As String = "ND1RegHours"
        Public Const ND2RegHours As String = "ND2RegHours"
        Public Const Absences As String = "Absences"
        Public Const TardyHours As String = "TardyHours"
        Public Const UTHours As String = "UTHours"
        Public Const VLDays As String = "VLDays"
        Public Const SLDays As String = "SLDays"
        Public Const PayHoliDaily As String = "PayHoliDaily"
        Public Const PayHoliMonthly As String = "PayHoliMonthly"
        Public Const ReOpen As String = "ReOpen"
        Public Const CreatedBy As String = "CreatedBy"
        Public Const CreatedDate As String = "CreatedDate"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpDate As String = "LastUpDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _SummTimeSheet.ColumnNames.ID
				ht(CompanyID) = _SummTimeSheet.ColumnNames.CompanyID
				ht(EmployeeID) = _SummTimeSheet.ColumnNames.EmployeeID
				ht(PayStart) = _SummTimeSheet.ColumnNames.PayStart
				ht(PayEnd) = _SummTimeSheet.ColumnNames.PayEnd
				ht(TransStart) = _SummTimeSheet.ColumnNames.TransStart
				ht(TransEnd) = _SummTimeSheet.ColumnNames.TransEnd
				ht(RegHours) = _SummTimeSheet.ColumnNames.RegHours
				ht(ND1RegHours) = _SummTimeSheet.ColumnNames.ND1RegHours
				ht(ND2RegHours) = _SummTimeSheet.ColumnNames.ND2RegHours
				ht(Absences) = _SummTimeSheet.ColumnNames.Absences
				ht(TardyHours) = _SummTimeSheet.ColumnNames.TardyHours
				ht(UTHours) = _SummTimeSheet.ColumnNames.UTHours
				ht(VLDays) = _SummTimeSheet.ColumnNames.VLDays
				ht(SLDays) = _SummTimeSheet.ColumnNames.SLDays
				ht(PayHoliDaily) = _SummTimeSheet.ColumnNames.PayHoliDaily
				ht(PayHoliMonthly) = _SummTimeSheet.ColumnNames.PayHoliMonthly
				ht(ReOpen) = _SummTimeSheet.ColumnNames.ReOpen
				ht(CreatedBy) = _SummTimeSheet.ColumnNames.CreatedBy
				ht(CreatedDate) = _SummTimeSheet.ColumnNames.CreatedDate
				ht(LastUpdBy) = _SummTimeSheet.ColumnNames.LastUpdBy
				ht(LastUpDate) = _SummTimeSheet.ColumnNames.LastUpDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const PayStart As String = "s_PayStart"
        Public Const PayEnd As String = "s_PayEnd"
        Public Const TransStart As String = "s_TransStart"
        Public Const TransEnd As String = "s_TransEnd"
        Public Const RegHours As String = "s_RegHours"
        Public Const ND1RegHours As String = "s_ND1RegHours"
        Public Const ND2RegHours As String = "s_ND2RegHours"
        Public Const Absences As String = "s_Absences"
        Public Const TardyHours As String = "s_TardyHours"
        Public Const UTHours As String = "s_UTHours"
        Public Const VLDays As String = "s_VLDays"
        Public Const SLDays As String = "s_SLDays"
        Public Const PayHoliDaily As String = "s_PayHoliDaily"
        Public Const PayHoliMonthly As String = "s_PayHoliMonthly"
        Public Const ReOpen As String = "s_ReOpen"
        Public Const CreatedBy As String = "s_CreatedBy"
        Public Const CreatedDate As String = "s_CreatedDate"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpDate As String = "s_LastUpDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property PayStart As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PayStart)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PayStart, Value)
			End Set
		End Property

		Public Overridable Property PayEnd As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PayEnd)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PayEnd, Value)
			End Set
		End Property

		Public Overridable Property TransStart As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.TransStart)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.TransStart, Value)
			End Set
		End Property

		Public Overridable Property TransEnd As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.TransEnd)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.TransEnd, Value)
			End Set
		End Property

		Public Overridable Property RegHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.RegHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.RegHours, Value)
			End Set
		End Property

		Public Overridable Property ND1RegHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.ND1RegHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.ND1RegHours, Value)
			End Set
		End Property

		Public Overridable Property ND2RegHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.ND2RegHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.ND2RegHours, Value)
			End Set
		End Property

		Public Overridable Property Absences As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.Absences)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.Absences, Value)
			End Set
		End Property

		Public Overridable Property TardyHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.TardyHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.TardyHours, Value)
			End Set
		End Property

		Public Overridable Property UTHours As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.UTHours)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.UTHours, Value)
			End Set
		End Property

		Public Overridable Property VLDays As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.VLDays)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.VLDays, Value)
			End Set
		End Property

		Public Overridable Property SLDays As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.SLDays)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.SLDays, Value)
			End Set
		End Property

		Public Overridable Property PayHoliDaily As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.PayHoliDaily)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.PayHoliDaily, Value)
			End Set
		End Property

		Public Overridable Property PayHoliMonthly As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.PayHoliMonthly)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.PayHoliMonthly, Value)
			End Set
		End Property

		Public Overridable Property ReOpen As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.ReOpen)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.ReOpen, Value)
			End Set
		End Property

		Public Overridable Property CreatedBy As String
			Get
				Return MyBase.GetString(ColumnNames.CreatedBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CreatedBy, Value)
			End Set
		End Property

		Public Overridable Property CreatedDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.CreatedDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayStart As String
			Get
				If Me.IsColumnNull(ColumnNames.PayStart) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PayStart)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayStart)
				Else
					Me.PayStart = MyBase.SetDateTimeAsString(ColumnNames.PayStart, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayEnd As String
			Get
				If Me.IsColumnNull(ColumnNames.PayEnd) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PayEnd)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayEnd)
				Else
					Me.PayEnd = MyBase.SetDateTimeAsString(ColumnNames.PayEnd, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TransStart As String
			Get
				If Me.IsColumnNull(ColumnNames.TransStart) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.TransStart)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TransStart)
				Else
					Me.TransStart = MyBase.SetDateTimeAsString(ColumnNames.TransStart, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TransEnd As String
			Get
				If Me.IsColumnNull(ColumnNames.TransEnd) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.TransEnd)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TransEnd)
				Else
					Me.TransEnd = MyBase.SetDateTimeAsString(ColumnNames.TransEnd, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_RegHours As String
			Get
				If Me.IsColumnNull(ColumnNames.RegHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.RegHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.RegHours)
				Else
					Me.RegHours = MyBase.SetDecimalAsString(ColumnNames.RegHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ND1RegHours As String
			Get
				If Me.IsColumnNull(ColumnNames.ND1RegHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.ND1RegHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ND1RegHours)
				Else
					Me.ND1RegHours = MyBase.SetDecimalAsString(ColumnNames.ND1RegHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ND2RegHours As String
			Get
				If Me.IsColumnNull(ColumnNames.ND2RegHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.ND2RegHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ND2RegHours)
				Else
					Me.ND2RegHours = MyBase.SetDecimalAsString(ColumnNames.ND2RegHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Absences As String
			Get
				If Me.IsColumnNull(ColumnNames.Absences) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.Absences)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Absences)
				Else
					Me.Absences = MyBase.SetDecimalAsString(ColumnNames.Absences, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_TardyHours As String
			Get
				If Me.IsColumnNull(ColumnNames.TardyHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.TardyHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.TardyHours)
				Else
					Me.TardyHours = MyBase.SetDecimalAsString(ColumnNames.TardyHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_UTHours As String
			Get
				If Me.IsColumnNull(ColumnNames.UTHours) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.UTHours)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.UTHours)
				Else
					Me.UTHours = MyBase.SetDecimalAsString(ColumnNames.UTHours, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_VLDays As String
			Get
				If Me.IsColumnNull(ColumnNames.VLDays) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.VLDays)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.VLDays)
				Else
					Me.VLDays = MyBase.SetDecimalAsString(ColumnNames.VLDays, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_SLDays As String
			Get
				If Me.IsColumnNull(ColumnNames.SLDays) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.SLDays)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.SLDays)
				Else
					Me.SLDays = MyBase.SetDecimalAsString(ColumnNames.SLDays, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayHoliDaily As String
			Get
				If Me.IsColumnNull(ColumnNames.PayHoliDaily) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.PayHoliDaily)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayHoliDaily)
				Else
					Me.PayHoliDaily = MyBase.SetDecimalAsString(ColumnNames.PayHoliDaily, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayHoliMonthly As String
			Get
				If Me.IsColumnNull(ColumnNames.PayHoliMonthly) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.PayHoliMonthly)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayHoliMonthly)
				Else
					Me.PayHoliMonthly = MyBase.SetDecimalAsString(ColumnNames.PayHoliMonthly, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ReOpen As String
			Get
				If Me.IsColumnNull(ColumnNames.ReOpen) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.ReOpen)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ReOpen)
				Else
					Me.ReOpen = MyBase.SetBooleanAsString(ColumnNames.ReOpen, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedBy As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedBy)
				Else
					Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CreatedDate As String
			Get
				If Me.IsColumnNull(ColumnNames.CreatedDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CreatedDate)
				Else
					Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpDate)
				Else
					Me.LastUpDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayStart() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayStart, Parameters.PayStart)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayEnd() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayEnd, Parameters.PayEnd)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TransStart() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TransStart, Parameters.TransStart)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TransEnd() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TransEnd, Parameters.TransEnd)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property RegHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.RegHours, Parameters.RegHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ND1RegHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ND1RegHours, Parameters.ND1RegHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ND2RegHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ND2RegHours, Parameters.ND2RegHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Absences() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Absences, Parameters.Absences)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property TardyHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.TardyHours, Parameters.TardyHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property UTHours() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.UTHours, Parameters.UTHours)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property VLDays() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.VLDays, Parameters.VLDays)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property SLDays() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.SLDays, Parameters.SLDays)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayHoliDaily() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayHoliDaily, Parameters.PayHoliDaily)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayHoliMonthly() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayHoliMonthly, Parameters.PayHoliMonthly)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ReOpen() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ReOpen, Parameters.ReOpen)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CreatedDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpDate, Parameters.LastUpDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property PayStart() As WhereParameter 
			Get
				If _PayStart_W Is Nothing Then
					_PayStart_W = TearOff.PayStart
				End If
				Return _PayStart_W
			End Get
		End Property

		Public ReadOnly Property PayEnd() As WhereParameter 
			Get
				If _PayEnd_W Is Nothing Then
					_PayEnd_W = TearOff.PayEnd
				End If
				Return _PayEnd_W
			End Get
		End Property

		Public ReadOnly Property TransStart() As WhereParameter 
			Get
				If _TransStart_W Is Nothing Then
					_TransStart_W = TearOff.TransStart
				End If
				Return _TransStart_W
			End Get
		End Property

		Public ReadOnly Property TransEnd() As WhereParameter 
			Get
				If _TransEnd_W Is Nothing Then
					_TransEnd_W = TearOff.TransEnd
				End If
				Return _TransEnd_W
			End Get
		End Property

		Public ReadOnly Property RegHours() As WhereParameter 
			Get
				If _RegHours_W Is Nothing Then
					_RegHours_W = TearOff.RegHours
				End If
				Return _RegHours_W
			End Get
		End Property

		Public ReadOnly Property ND1RegHours() As WhereParameter 
			Get
				If _ND1RegHours_W Is Nothing Then
					_ND1RegHours_W = TearOff.ND1RegHours
				End If
				Return _ND1RegHours_W
			End Get
		End Property

		Public ReadOnly Property ND2RegHours() As WhereParameter 
			Get
				If _ND2RegHours_W Is Nothing Then
					_ND2RegHours_W = TearOff.ND2RegHours
				End If
				Return _ND2RegHours_W
			End Get
		End Property

		Public ReadOnly Property Absences() As WhereParameter 
			Get
				If _Absences_W Is Nothing Then
					_Absences_W = TearOff.Absences
				End If
				Return _Absences_W
			End Get
		End Property

		Public ReadOnly Property TardyHours() As WhereParameter 
			Get
				If _TardyHours_W Is Nothing Then
					_TardyHours_W = TearOff.TardyHours
				End If
				Return _TardyHours_W
			End Get
		End Property

		Public ReadOnly Property UTHours() As WhereParameter 
			Get
				If _UTHours_W Is Nothing Then
					_UTHours_W = TearOff.UTHours
				End If
				Return _UTHours_W
			End Get
		End Property

		Public ReadOnly Property VLDays() As WhereParameter 
			Get
				If _VLDays_W Is Nothing Then
					_VLDays_W = TearOff.VLDays
				End If
				Return _VLDays_W
			End Get
		End Property

		Public ReadOnly Property SLDays() As WhereParameter 
			Get
				If _SLDays_W Is Nothing Then
					_SLDays_W = TearOff.SLDays
				End If
				Return _SLDays_W
			End Get
		End Property

		Public ReadOnly Property PayHoliDaily() As WhereParameter 
			Get
				If _PayHoliDaily_W Is Nothing Then
					_PayHoliDaily_W = TearOff.PayHoliDaily
				End If
				Return _PayHoliDaily_W
			End Get
		End Property

		Public ReadOnly Property PayHoliMonthly() As WhereParameter 
			Get
				If _PayHoliMonthly_W Is Nothing Then
					_PayHoliMonthly_W = TearOff.PayHoliMonthly
				End If
				Return _PayHoliMonthly_W
			End Get
		End Property

		Public ReadOnly Property ReOpen() As WhereParameter 
			Get
				If _ReOpen_W Is Nothing Then
					_ReOpen_W = TearOff.ReOpen
				End If
				Return _ReOpen_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As WhereParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As WhereParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpDate() As WhereParameter 
			Get
				If _LastUpDate_W Is Nothing Then
					_LastUpDate_W = TearOff.LastUpDate
				End If
				Return _LastUpDate_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _PayStart_W As WhereParameter = Nothing
		Private _PayEnd_W As WhereParameter = Nothing
		Private _TransStart_W As WhereParameter = Nothing
		Private _TransEnd_W As WhereParameter = Nothing
		Private _RegHours_W As WhereParameter = Nothing
		Private _ND1RegHours_W As WhereParameter = Nothing
		Private _ND2RegHours_W As WhereParameter = Nothing
		Private _Absences_W As WhereParameter = Nothing
		Private _TardyHours_W As WhereParameter = Nothing
		Private _UTHours_W As WhereParameter = Nothing
		Private _VLDays_W As WhereParameter = Nothing
		Private _SLDays_W As WhereParameter = Nothing
		Private _PayHoliDaily_W As WhereParameter = Nothing
		Private _PayHoliMonthly_W As WhereParameter = Nothing
		Private _ReOpen_W As WhereParameter = Nothing
		Private _CreatedBy_W As WhereParameter = Nothing
		Private _CreatedDate_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_PayStart_W = Nothing
			_PayEnd_W = Nothing
			_TransStart_W = Nothing
			_TransEnd_W = Nothing
			_RegHours_W = Nothing
			_ND1RegHours_W = Nothing
			_ND2RegHours_W = Nothing
			_Absences_W = Nothing
			_TardyHours_W = Nothing
			_UTHours_W = Nothing
			_VLDays_W = Nothing
			_SLDays_W = Nothing
			_PayHoliDaily_W = Nothing
			_PayHoliMonthly_W = Nothing
			_ReOpen_W = Nothing
			_CreatedBy_W = Nothing
			_CreatedDate_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayStart() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayStart, Parameters.PayStart)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayEnd() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayEnd, Parameters.PayEnd)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TransStart() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TransStart, Parameters.TransStart)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TransEnd() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TransEnd, Parameters.TransEnd)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property RegHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RegHours, Parameters.RegHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ND1RegHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ND1RegHours, Parameters.ND1RegHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ND2RegHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ND2RegHours, Parameters.ND2RegHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Absences() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Absences, Parameters.Absences)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property TardyHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.TardyHours, Parameters.TardyHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property UTHours() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UTHours, Parameters.UTHours)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property VLDays() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.VLDays, Parameters.VLDays)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property SLDays() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.SLDays, Parameters.SLDays)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayHoliDaily() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayHoliDaily, Parameters.PayHoliDaily)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayHoliMonthly() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayHoliMonthly, Parameters.PayHoliMonthly)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ReOpen() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ReOpen, Parameters.ReOpen)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpDate, Parameters.LastUpDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property PayStart() As AggregateParameter 
			Get
				If _PayStart_W Is Nothing Then
					_PayStart_W = TearOff.PayStart
				End If
				Return _PayStart_W
			End Get
		End Property

		Public ReadOnly Property PayEnd() As AggregateParameter 
			Get
				If _PayEnd_W Is Nothing Then
					_PayEnd_W = TearOff.PayEnd
				End If
				Return _PayEnd_W
			End Get
		End Property

		Public ReadOnly Property TransStart() As AggregateParameter 
			Get
				If _TransStart_W Is Nothing Then
					_TransStart_W = TearOff.TransStart
				End If
				Return _TransStart_W
			End Get
		End Property

		Public ReadOnly Property TransEnd() As AggregateParameter 
			Get
				If _TransEnd_W Is Nothing Then
					_TransEnd_W = TearOff.TransEnd
				End If
				Return _TransEnd_W
			End Get
		End Property

		Public ReadOnly Property RegHours() As AggregateParameter 
			Get
				If _RegHours_W Is Nothing Then
					_RegHours_W = TearOff.RegHours
				End If
				Return _RegHours_W
			End Get
		End Property

		Public ReadOnly Property ND1RegHours() As AggregateParameter 
			Get
				If _ND1RegHours_W Is Nothing Then
					_ND1RegHours_W = TearOff.ND1RegHours
				End If
				Return _ND1RegHours_W
			End Get
		End Property

		Public ReadOnly Property ND2RegHours() As AggregateParameter 
			Get
				If _ND2RegHours_W Is Nothing Then
					_ND2RegHours_W = TearOff.ND2RegHours
				End If
				Return _ND2RegHours_W
			End Get
		End Property

		Public ReadOnly Property Absences() As AggregateParameter 
			Get
				If _Absences_W Is Nothing Then
					_Absences_W = TearOff.Absences
				End If
				Return _Absences_W
			End Get
		End Property

		Public ReadOnly Property TardyHours() As AggregateParameter 
			Get
				If _TardyHours_W Is Nothing Then
					_TardyHours_W = TearOff.TardyHours
				End If
				Return _TardyHours_W
			End Get
		End Property

		Public ReadOnly Property UTHours() As AggregateParameter 
			Get
				If _UTHours_W Is Nothing Then
					_UTHours_W = TearOff.UTHours
				End If
				Return _UTHours_W
			End Get
		End Property

		Public ReadOnly Property VLDays() As AggregateParameter 
			Get
				If _VLDays_W Is Nothing Then
					_VLDays_W = TearOff.VLDays
				End If
				Return _VLDays_W
			End Get
		End Property

		Public ReadOnly Property SLDays() As AggregateParameter 
			Get
				If _SLDays_W Is Nothing Then
					_SLDays_W = TearOff.SLDays
				End If
				Return _SLDays_W
			End Get
		End Property

		Public ReadOnly Property PayHoliDaily() As AggregateParameter 
			Get
				If _PayHoliDaily_W Is Nothing Then
					_PayHoliDaily_W = TearOff.PayHoliDaily
				End If
				Return _PayHoliDaily_W
			End Get
		End Property

		Public ReadOnly Property PayHoliMonthly() As AggregateParameter 
			Get
				If _PayHoliMonthly_W Is Nothing Then
					_PayHoliMonthly_W = TearOff.PayHoliMonthly
				End If
				Return _PayHoliMonthly_W
			End Get
		End Property

		Public ReadOnly Property ReOpen() As AggregateParameter 
			Get
				If _ReOpen_W Is Nothing Then
					_ReOpen_W = TearOff.ReOpen
				End If
				Return _ReOpen_W
			End Get
		End Property

		Public ReadOnly Property CreatedBy() As AggregateParameter 
			Get
				If _CreatedBy_W Is Nothing Then
					_CreatedBy_W = TearOff.CreatedBy
				End If
				Return _CreatedBy_W
			End Get
		End Property

		Public ReadOnly Property CreatedDate() As AggregateParameter 
			Get
				If _CreatedDate_W Is Nothing Then
					_CreatedDate_W = TearOff.CreatedDate
				End If
				Return _CreatedDate_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpDate() As AggregateParameter 
			Get
				If _LastUpDate_W Is Nothing Then
					_LastUpDate_W = TearOff.LastUpDate
				End If
				Return _LastUpDate_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _PayStart_W As AggregateParameter = Nothing
		Private _PayEnd_W As AggregateParameter = Nothing
		Private _TransStart_W As AggregateParameter = Nothing
		Private _TransEnd_W As AggregateParameter = Nothing
		Private _RegHours_W As AggregateParameter = Nothing
		Private _ND1RegHours_W As AggregateParameter = Nothing
		Private _ND2RegHours_W As AggregateParameter = Nothing
		Private _Absences_W As AggregateParameter = Nothing
		Private _TardyHours_W As AggregateParameter = Nothing
		Private _UTHours_W As AggregateParameter = Nothing
		Private _VLDays_W As AggregateParameter = Nothing
		Private _SLDays_W As AggregateParameter = Nothing
		Private _PayHoliDaily_W As AggregateParameter = Nothing
		Private _PayHoliMonthly_W As AggregateParameter = Nothing
		Private _ReOpen_W As AggregateParameter = Nothing
		Private _CreatedBy_W As AggregateParameter = Nothing
		Private _CreatedDate_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_PayStart_W = Nothing
		_PayEnd_W = Nothing
		_TransStart_W = Nothing
		_TransEnd_W = Nothing
		_RegHours_W = Nothing
		_ND1RegHours_W = Nothing
		_ND2RegHours_W = Nothing
		_Absences_W = Nothing
		_TardyHours_W = Nothing
		_UTHours_W = Nothing
		_VLDays_W = Nothing
		_SLDays_W = Nothing
		_PayHoliDaily_W = Nothing
		_PayHoliMonthly_W = Nothing
		_ReOpen_W = Nothing
		_CreatedBy_W = Nothing
		_CreatedDate_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummTimeSheetInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummTimeSheetUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummTimeSheetDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayStart)
		p.SourceColumn = ColumnNames.PayStart
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayEnd)
		p.SourceColumn = ColumnNames.PayEnd
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TransStart)
		p.SourceColumn = ColumnNames.TransStart
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TransEnd)
		p.SourceColumn = ColumnNames.TransEnd
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.RegHours)
		p.SourceColumn = ColumnNames.RegHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ND1RegHours)
		p.SourceColumn = ColumnNames.ND1RegHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ND2RegHours)
		p.SourceColumn = ColumnNames.ND2RegHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Absences)
		p.SourceColumn = ColumnNames.Absences
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.TardyHours)
		p.SourceColumn = ColumnNames.TardyHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.UTHours)
		p.SourceColumn = ColumnNames.UTHours
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.VLDays)
		p.SourceColumn = ColumnNames.VLDays
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.SLDays)
		p.SourceColumn = ColumnNames.SLDays
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayHoliDaily)
		p.SourceColumn = ColumnNames.PayHoliDaily
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayHoliMonthly)
		p.SourceColumn = ColumnNames.PayHoliMonthly
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ReOpen)
		p.SourceColumn = ColumnNames.ReOpen
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedBy)
		p.SourceColumn = ColumnNames.CreatedBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CreatedDate)
		p.SourceColumn = ColumnNames.CreatedDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpDate)
		p.SourceColumn = ColumnNames.LastUpDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

