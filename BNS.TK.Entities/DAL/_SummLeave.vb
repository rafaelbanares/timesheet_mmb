
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _SummLeave
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "SummLeave"
			Me.MappingName = "SummLeave"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_SummLeaveLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_SummLeave.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_SummLeaveLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayStart As SqlParameter
			Get
				Return New SqlParameter("@PayStart", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property PayEnd As SqlParameter
			Get
				Return New SqlParameter("@PayEnd", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LeaveCode As SqlParameter
			Get
				Return New SqlParameter("@LeaveCode", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property Leave As SqlParameter
			Get
				Return New SqlParameter("@Leave", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ReOpen As SqlParameter
			Get
				Return New SqlParameter("@ReOpen", SqlDbType.Bit, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const PayStart As String = "PayStart"
        Public Const PayEnd As String = "PayEnd"
        Public Const LeaveCode As String = "LeaveCode"
        Public Const Leave As String = "Leave"
        Public Const ReOpen As String = "ReOpen"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _SummLeave.PropertyNames.ID
				ht(CompanyID) = _SummLeave.PropertyNames.CompanyID
				ht(EmployeeID) = _SummLeave.PropertyNames.EmployeeID
				ht(PayStart) = _SummLeave.PropertyNames.PayStart
				ht(PayEnd) = _SummLeave.PropertyNames.PayEnd
				ht(LeaveCode) = _SummLeave.PropertyNames.LeaveCode
				ht(Leave) = _SummLeave.PropertyNames.Leave
				ht(ReOpen) = _SummLeave.PropertyNames.ReOpen

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const PayStart As String = "PayStart"
        Public Const PayEnd As String = "PayEnd"
        Public Const LeaveCode As String = "LeaveCode"
        Public Const Leave As String = "Leave"
        Public Const ReOpen As String = "ReOpen"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _SummLeave.ColumnNames.ID
				ht(CompanyID) = _SummLeave.ColumnNames.CompanyID
				ht(EmployeeID) = _SummLeave.ColumnNames.EmployeeID
				ht(PayStart) = _SummLeave.ColumnNames.PayStart
				ht(PayEnd) = _SummLeave.ColumnNames.PayEnd
				ht(LeaveCode) = _SummLeave.ColumnNames.LeaveCode
				ht(Leave) = _SummLeave.ColumnNames.Leave
				ht(ReOpen) = _SummLeave.ColumnNames.ReOpen

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const PayStart As String = "s_PayStart"
        Public Const PayEnd As String = "s_PayEnd"
        Public Const LeaveCode As String = "s_LeaveCode"
        Public Const Leave As String = "s_Leave"
        Public Const ReOpen As String = "s_ReOpen"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property PayStart As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PayStart)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PayStart, Value)
			End Set
		End Property

		Public Overridable Property PayEnd As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.PayEnd)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.PayEnd, Value)
			End Set
		End Property

		Public Overridable Property LeaveCode As String
			Get
				Return MyBase.GetString(ColumnNames.LeaveCode)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LeaveCode, Value)
			End Set
		End Property

		Public Overridable Property Leave As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.Leave)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.Leave, Value)
			End Set
		End Property

		Public Overridable Property ReOpen As Boolean
			Get
				Return MyBase.GetBoolean(ColumnNames.ReOpen)
			End Get
			Set(ByVal Value As Boolean)
				MyBase.SetBoolean(ColumnNames.ReOpen, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayStart As String
			Get
				If Me.IsColumnNull(ColumnNames.PayStart) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PayStart)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayStart)
				Else
					Me.PayStart = MyBase.SetDateTimeAsString(ColumnNames.PayStart, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_PayEnd As String
			Get
				If Me.IsColumnNull(ColumnNames.PayEnd) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.PayEnd)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.PayEnd)
				Else
					Me.PayEnd = MyBase.SetDateTimeAsString(ColumnNames.PayEnd, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LeaveCode As String
			Get
				If Me.IsColumnNull(ColumnNames.LeaveCode) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LeaveCode)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LeaveCode)
				Else
					Me.LeaveCode = MyBase.SetStringAsString(ColumnNames.LeaveCode, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Leave As String
			Get
				If Me.IsColumnNull(ColumnNames.Leave) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.Leave)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Leave)
				Else
					Me.Leave = MyBase.SetDecimalAsString(ColumnNames.Leave, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ReOpen As String
			Get
				If Me.IsColumnNull(ColumnNames.ReOpen) Then
					Return String.Empty
				Else
					Return MyBase.GetBooleanAsString(ColumnNames.ReOpen)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ReOpen)
				Else
					Me.ReOpen = MyBase.SetBooleanAsString(ColumnNames.ReOpen, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayStart() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayStart, Parameters.PayStart)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property PayEnd() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.PayEnd, Parameters.PayEnd)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LeaveCode() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LeaveCode, Parameters.LeaveCode)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Leave() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Leave, Parameters.Leave)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ReOpen() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ReOpen, Parameters.ReOpen)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property PayStart() As WhereParameter 
			Get
				If _PayStart_W Is Nothing Then
					_PayStart_W = TearOff.PayStart
				End If
				Return _PayStart_W
			End Get
		End Property

		Public ReadOnly Property PayEnd() As WhereParameter 
			Get
				If _PayEnd_W Is Nothing Then
					_PayEnd_W = TearOff.PayEnd
				End If
				Return _PayEnd_W
			End Get
		End Property

		Public ReadOnly Property LeaveCode() As WhereParameter 
			Get
				If _LeaveCode_W Is Nothing Then
					_LeaveCode_W = TearOff.LeaveCode
				End If
				Return _LeaveCode_W
			End Get
		End Property

		Public ReadOnly Property Leave() As WhereParameter 
			Get
				If _Leave_W Is Nothing Then
					_Leave_W = TearOff.Leave
				End If
				Return _Leave_W
			End Get
		End Property

		Public ReadOnly Property ReOpen() As WhereParameter 
			Get
				If _ReOpen_W Is Nothing Then
					_ReOpen_W = TearOff.ReOpen
				End If
				Return _ReOpen_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _PayStart_W As WhereParameter = Nothing
		Private _PayEnd_W As WhereParameter = Nothing
		Private _LeaveCode_W As WhereParameter = Nothing
		Private _Leave_W As WhereParameter = Nothing
		Private _ReOpen_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_PayStart_W = Nothing
			_PayEnd_W = Nothing
			_LeaveCode_W = Nothing
			_Leave_W = Nothing
			_ReOpen_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayStart() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayStart, Parameters.PayStart)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property PayEnd() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.PayEnd, Parameters.PayEnd)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LeaveCode() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LeaveCode, Parameters.LeaveCode)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Leave() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Leave, Parameters.Leave)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ReOpen() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ReOpen, Parameters.ReOpen)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property PayStart() As AggregateParameter 
			Get
				If _PayStart_W Is Nothing Then
					_PayStart_W = TearOff.PayStart
				End If
				Return _PayStart_W
			End Get
		End Property

		Public ReadOnly Property PayEnd() As AggregateParameter 
			Get
				If _PayEnd_W Is Nothing Then
					_PayEnd_W = TearOff.PayEnd
				End If
				Return _PayEnd_W
			End Get
		End Property

		Public ReadOnly Property LeaveCode() As AggregateParameter 
			Get
				If _LeaveCode_W Is Nothing Then
					_LeaveCode_W = TearOff.LeaveCode
				End If
				Return _LeaveCode_W
			End Get
		End Property

		Public ReadOnly Property Leave() As AggregateParameter 
			Get
				If _Leave_W Is Nothing Then
					_Leave_W = TearOff.Leave
				End If
				Return _Leave_W
			End Get
		End Property

		Public ReadOnly Property ReOpen() As AggregateParameter 
			Get
				If _ReOpen_W Is Nothing Then
					_ReOpen_W = TearOff.ReOpen
				End If
				Return _ReOpen_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _PayStart_W As AggregateParameter = Nothing
		Private _PayEnd_W As AggregateParameter = Nothing
		Private _LeaveCode_W As AggregateParameter = Nothing
		Private _Leave_W As AggregateParameter = Nothing
		Private _ReOpen_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_PayStart_W = Nothing
		_PayEnd_W = Nothing
		_LeaveCode_W = Nothing
		_Leave_W = Nothing
		_ReOpen_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummLeaveInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummLeaveUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_SummLeaveDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayStart)
		p.SourceColumn = ColumnNames.PayStart
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.PayEnd)
		p.SourceColumn = ColumnNames.PayEnd
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LeaveCode)
		p.SourceColumn = ColumnNames.LeaveCode
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Leave)
		p.SourceColumn = ColumnNames.Leave
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ReOpen)
		p.SourceColumn = ColumnNames.ReOpen
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

