
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _OutMail
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "OutMail"
			Me.MappingName = "OutMail"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OutMailLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_OutMail.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OutMailLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property FromEmail As SqlParameter
			Get
				Return New SqlParameter("@FromEmail", SqlDbType.VarChar, 200)
			End Get
		End Property
		
		Public Shared ReadOnly Property FromName As SqlParameter
			Get
				Return New SqlParameter("@FromName", SqlDbType.VarChar, 200)
			End Get
		End Property
		
		Public Shared ReadOnly Property ToEmail As SqlParameter
			Get
				Return New SqlParameter("@ToEmail", SqlDbType.VarChar, 1000)
			End Get
		End Property
		
		Public Shared ReadOnly Property ToName As SqlParameter
			Get
				Return New SqlParameter("@ToName", SqlDbType.VarChar, 200)
			End Get
		End Property
		
		Public Shared ReadOnly Property Cc As SqlParameter
			Get
				Return New SqlParameter("@Cc", SqlDbType.VarChar, 1000)
			End Get
		End Property
		
		Public Shared ReadOnly Property Bcc As SqlParameter
			Get
				Return New SqlParameter("@Bcc", SqlDbType.VarChar, 1000)
			End Get
		End Property
		
		Public Shared ReadOnly Property Subject As SqlParameter
			Get
				Return New SqlParameter("@Subject", SqlDbType.VarChar, 1000)
			End Get
		End Property
		
		Public Shared ReadOnly Property Body As SqlParameter
			Get
				Return New SqlParameter("@Body", SqlDbType.VarChar, 8000)
			End Get
		End Property
		
		Public Shared ReadOnly Property InsertDate As SqlParameter
			Get
				Return New SqlParameter("@InsertDate", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property SendDate As SqlParameter
			Get
				Return New SqlParameter("@SendDate", SqlDbType.DateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const FromEmail As String = "FromEmail"
        Public Const FromName As String = "FromName"
        Public Const ToEmail As String = "ToEmail"
        Public Const ToName As String = "ToName"
        Public Const Cc As String = "cc"
        Public Const Bcc As String = "bcc"
        Public Const Subject As String = "Subject"
        Public Const Body As String = "Body"
        Public Const InsertDate As String = "InsertDate"
        Public Const SendDate As String = "SendDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _OutMail.PropertyNames.ID
				ht(CompanyID) = _OutMail.PropertyNames.CompanyID
				ht(FromEmail) = _OutMail.PropertyNames.FromEmail
				ht(FromName) = _OutMail.PropertyNames.FromName
				ht(ToEmail) = _OutMail.PropertyNames.ToEmail
				ht(ToName) = _OutMail.PropertyNames.ToName
				ht(Cc) = _OutMail.PropertyNames.Cc
				ht(Bcc) = _OutMail.PropertyNames.Bcc
				ht(Subject) = _OutMail.PropertyNames.Subject
				ht(Body) = _OutMail.PropertyNames.Body
				ht(InsertDate) = _OutMail.PropertyNames.InsertDate
				ht(SendDate) = _OutMail.PropertyNames.SendDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const FromEmail As String = "FromEmail"
        Public Const FromName As String = "FromName"
        Public Const ToEmail As String = "ToEmail"
        Public Const ToName As String = "ToName"
        Public Const Cc As String = "Cc"
        Public Const Bcc As String = "Bcc"
        Public Const Subject As String = "Subject"
        Public Const Body As String = "Body"
        Public Const InsertDate As String = "InsertDate"
        Public Const SendDate As String = "SendDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _OutMail.ColumnNames.ID
				ht(CompanyID) = _OutMail.ColumnNames.CompanyID
				ht(FromEmail) = _OutMail.ColumnNames.FromEmail
				ht(FromName) = _OutMail.ColumnNames.FromName
				ht(ToEmail) = _OutMail.ColumnNames.ToEmail
				ht(ToName) = _OutMail.ColumnNames.ToName
				ht(Cc) = _OutMail.ColumnNames.Cc
				ht(Bcc) = _OutMail.ColumnNames.Bcc
				ht(Subject) = _OutMail.ColumnNames.Subject
				ht(Body) = _OutMail.ColumnNames.Body
				ht(InsertDate) = _OutMail.ColumnNames.InsertDate
				ht(SendDate) = _OutMail.ColumnNames.SendDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const FromEmail As String = "s_FromEmail"
        Public Const FromName As String = "s_FromName"
        Public Const ToEmail As String = "s_ToEmail"
        Public Const ToName As String = "s_ToName"
        Public Const Cc As String = "s_Cc"
        Public Const Bcc As String = "s_Bcc"
        Public Const Subject As String = "s_Subject"
        Public Const Body As String = "s_Body"
        Public Const InsertDate As String = "s_InsertDate"
        Public Const SendDate As String = "s_SendDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property FromEmail As String
			Get
				Return MyBase.GetString(ColumnNames.FromEmail)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.FromEmail, Value)
			End Set
		End Property

		Public Overridable Property FromName As String
			Get
				Return MyBase.GetString(ColumnNames.FromName)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.FromName, Value)
			End Set
		End Property

		Public Overridable Property ToEmail As String
			Get
				Return MyBase.GetString(ColumnNames.ToEmail)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ToEmail, Value)
			End Set
		End Property

		Public Overridable Property ToName As String
			Get
				Return MyBase.GetString(ColumnNames.ToName)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ToName, Value)
			End Set
		End Property

		Public Overridable Property Cc As String
			Get
				Return MyBase.GetString(ColumnNames.Cc)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Cc, Value)
			End Set
		End Property

		Public Overridable Property Bcc As String
			Get
				Return MyBase.GetString(ColumnNames.Bcc)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Bcc, Value)
			End Set
		End Property

		Public Overridable Property Subject As String
			Get
				Return MyBase.GetString(ColumnNames.Subject)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Subject, Value)
			End Set
		End Property

		Public Overridable Property Body As String
			Get
				Return MyBase.GetString(ColumnNames.Body)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Body, Value)
			End Set
		End Property

		Public Overridable Property InsertDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.InsertDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.InsertDate, Value)
			End Set
		End Property

		Public Overridable Property SendDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.SendDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.SendDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_FromEmail As String
			Get
				If Me.IsColumnNull(ColumnNames.FromEmail) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.FromEmail)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.FromEmail)
				Else
					Me.FromEmail = MyBase.SetStringAsString(ColumnNames.FromEmail, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_FromName As String
			Get
				If Me.IsColumnNull(ColumnNames.FromName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.FromName)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.FromName)
				Else
					Me.FromName = MyBase.SetStringAsString(ColumnNames.FromName, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ToEmail As String
			Get
				If Me.IsColumnNull(ColumnNames.ToEmail) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ToEmail)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ToEmail)
				Else
					Me.ToEmail = MyBase.SetStringAsString(ColumnNames.ToEmail, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ToName As String
			Get
				If Me.IsColumnNull(ColumnNames.ToName) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ToName)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ToName)
				Else
					Me.ToName = MyBase.SetStringAsString(ColumnNames.ToName, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Cc As String
			Get
				If Me.IsColumnNull(ColumnNames.Cc) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Cc)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Cc)
				Else
					Me.Cc = MyBase.SetStringAsString(ColumnNames.Cc, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Bcc As String
			Get
				If Me.IsColumnNull(ColumnNames.Bcc) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Bcc)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Bcc)
				Else
					Me.Bcc = MyBase.SetStringAsString(ColumnNames.Bcc, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Subject As String
			Get
				If Me.IsColumnNull(ColumnNames.Subject) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Subject)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Subject)
				Else
					Me.Subject = MyBase.SetStringAsString(ColumnNames.Subject, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Body As String
			Get
				If Me.IsColumnNull(ColumnNames.Body) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Body)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Body)
				Else
					Me.Body = MyBase.SetStringAsString(ColumnNames.Body, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_InsertDate As String
			Get
				If Me.IsColumnNull(ColumnNames.InsertDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.InsertDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.InsertDate)
				Else
					Me.InsertDate = MyBase.SetDateTimeAsString(ColumnNames.InsertDate, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_SendDate As String
			Get
				If Me.IsColumnNull(ColumnNames.SendDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.SendDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.SendDate)
				Else
					Me.SendDate = MyBase.SetDateTimeAsString(ColumnNames.SendDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property FromEmail() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.FromEmail, Parameters.FromEmail)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property FromName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.FromName, Parameters.FromName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ToEmail() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ToEmail, Parameters.ToEmail)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ToName() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ToName, Parameters.ToName)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Cc() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Cc, Parameters.Cc)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Bcc() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Bcc, Parameters.Bcc)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Subject() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Subject, Parameters.Subject)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Body() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Body, Parameters.Body)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property InsertDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.InsertDate, Parameters.InsertDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property SendDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.SendDate, Parameters.SendDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property FromEmail() As WhereParameter 
			Get
				If _FromEmail_W Is Nothing Then
					_FromEmail_W = TearOff.FromEmail
				End If
				Return _FromEmail_W
			End Get
		End Property

		Public ReadOnly Property FromName() As WhereParameter 
			Get
				If _FromName_W Is Nothing Then
					_FromName_W = TearOff.FromName
				End If
				Return _FromName_W
			End Get
		End Property

		Public ReadOnly Property ToEmail() As WhereParameter 
			Get
				If _ToEmail_W Is Nothing Then
					_ToEmail_W = TearOff.ToEmail
				End If
				Return _ToEmail_W
			End Get
		End Property

		Public ReadOnly Property ToName() As WhereParameter 
			Get
				If _ToName_W Is Nothing Then
					_ToName_W = TearOff.ToName
				End If
				Return _ToName_W
			End Get
		End Property

		Public ReadOnly Property Cc() As WhereParameter 
			Get
				If _Cc_W Is Nothing Then
					_Cc_W = TearOff.Cc
				End If
				Return _Cc_W
			End Get
		End Property

		Public ReadOnly Property Bcc() As WhereParameter 
			Get
				If _Bcc_W Is Nothing Then
					_Bcc_W = TearOff.Bcc
				End If
				Return _Bcc_W
			End Get
		End Property

		Public ReadOnly Property Subject() As WhereParameter 
			Get
				If _Subject_W Is Nothing Then
					_Subject_W = TearOff.Subject
				End If
				Return _Subject_W
			End Get
		End Property

		Public ReadOnly Property Body() As WhereParameter 
			Get
				If _Body_W Is Nothing Then
					_Body_W = TearOff.Body
				End If
				Return _Body_W
			End Get
		End Property

		Public ReadOnly Property InsertDate() As WhereParameter 
			Get
				If _InsertDate_W Is Nothing Then
					_InsertDate_W = TearOff.InsertDate
				End If
				Return _InsertDate_W
			End Get
		End Property

		Public ReadOnly Property SendDate() As WhereParameter 
			Get
				If _SendDate_W Is Nothing Then
					_SendDate_W = TearOff.SendDate
				End If
				Return _SendDate_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _FromEmail_W As WhereParameter = Nothing
		Private _FromName_W As WhereParameter = Nothing
		Private _ToEmail_W As WhereParameter = Nothing
		Private _ToName_W As WhereParameter = Nothing
		Private _Cc_W As WhereParameter = Nothing
		Private _Bcc_W As WhereParameter = Nothing
		Private _Subject_W As WhereParameter = Nothing
		Private _Body_W As WhereParameter = Nothing
		Private _InsertDate_W As WhereParameter = Nothing
		Private _SendDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_FromEmail_W = Nothing
			_FromName_W = Nothing
			_ToEmail_W = Nothing
			_ToName_W = Nothing
			_Cc_W = Nothing
			_Bcc_W = Nothing
			_Subject_W = Nothing
			_Body_W = Nothing
			_InsertDate_W = Nothing
			_SendDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property FromEmail() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.FromEmail, Parameters.FromEmail)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property FromName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.FromName, Parameters.FromName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ToEmail() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ToEmail, Parameters.ToEmail)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ToName() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ToName, Parameters.ToName)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Cc() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Cc, Parameters.Cc)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Bcc() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Bcc, Parameters.Bcc)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Subject() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Subject, Parameters.Subject)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Body() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Body, Parameters.Body)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property InsertDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.InsertDate, Parameters.InsertDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property SendDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.SendDate, Parameters.SendDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property FromEmail() As AggregateParameter 
			Get
				If _FromEmail_W Is Nothing Then
					_FromEmail_W = TearOff.FromEmail
				End If
				Return _FromEmail_W
			End Get
		End Property

		Public ReadOnly Property FromName() As AggregateParameter 
			Get
				If _FromName_W Is Nothing Then
					_FromName_W = TearOff.FromName
				End If
				Return _FromName_W
			End Get
		End Property

		Public ReadOnly Property ToEmail() As AggregateParameter 
			Get
				If _ToEmail_W Is Nothing Then
					_ToEmail_W = TearOff.ToEmail
				End If
				Return _ToEmail_W
			End Get
		End Property

		Public ReadOnly Property ToName() As AggregateParameter 
			Get
				If _ToName_W Is Nothing Then
					_ToName_W = TearOff.ToName
				End If
				Return _ToName_W
			End Get
		End Property

		Public ReadOnly Property Cc() As AggregateParameter 
			Get
				If _Cc_W Is Nothing Then
					_Cc_W = TearOff.Cc
				End If
				Return _Cc_W
			End Get
		End Property

		Public ReadOnly Property Bcc() As AggregateParameter 
			Get
				If _Bcc_W Is Nothing Then
					_Bcc_W = TearOff.Bcc
				End If
				Return _Bcc_W
			End Get
		End Property

		Public ReadOnly Property Subject() As AggregateParameter 
			Get
				If _Subject_W Is Nothing Then
					_Subject_W = TearOff.Subject
				End If
				Return _Subject_W
			End Get
		End Property

		Public ReadOnly Property Body() As AggregateParameter 
			Get
				If _Body_W Is Nothing Then
					_Body_W = TearOff.Body
				End If
				Return _Body_W
			End Get
		End Property

		Public ReadOnly Property InsertDate() As AggregateParameter 
			Get
				If _InsertDate_W Is Nothing Then
					_InsertDate_W = TearOff.InsertDate
				End If
				Return _InsertDate_W
			End Get
		End Property

		Public ReadOnly Property SendDate() As AggregateParameter 
			Get
				If _SendDate_W Is Nothing Then
					_SendDate_W = TearOff.SendDate
				End If
				Return _SendDate_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _FromEmail_W As AggregateParameter = Nothing
		Private _FromName_W As AggregateParameter = Nothing
		Private _ToEmail_W As AggregateParameter = Nothing
		Private _ToName_W As AggregateParameter = Nothing
		Private _Cc_W As AggregateParameter = Nothing
		Private _Bcc_W As AggregateParameter = Nothing
		Private _Subject_W As AggregateParameter = Nothing
		Private _Body_W As AggregateParameter = Nothing
		Private _InsertDate_W As AggregateParameter = Nothing
		Private _SendDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_FromEmail_W = Nothing
		_FromName_W = Nothing
		_ToEmail_W = Nothing
		_ToName_W = Nothing
		_Cc_W = Nothing
		_Bcc_W = Nothing
		_Subject_W = Nothing
		_Body_W = Nothing
		_InsertDate_W = Nothing
		_SendDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OutMailInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OutMailUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OutMailDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.FromEmail)
		p.SourceColumn = ColumnNames.FromEmail
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.FromName)
		p.SourceColumn = ColumnNames.FromName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ToEmail)
		p.SourceColumn = ColumnNames.ToEmail
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ToName)
		p.SourceColumn = ColumnNames.ToName
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Cc)
		p.SourceColumn = ColumnNames.Cc
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Bcc)
		p.SourceColumn = ColumnNames.Bcc
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Subject)
		p.SourceColumn = ColumnNames.Subject
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Body)
		p.SourceColumn = ColumnNames.Body
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.InsertDate)
		p.SourceColumn = ColumnNames.InsertDate
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.SendDate)
		p.SourceColumn = ColumnNames.SendDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

