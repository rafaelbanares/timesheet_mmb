
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _UserDefaults
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "UserDefaults"
			Me.MappingName = "UserDefaults"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_UserDefaultsLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal RowID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_UserDefaults.Parameters.RowID, RowID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_UserDefaultsLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property RowID As SqlParameter
			Get
				Return New SqlParameter("@RowID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.VarChar, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property AttFrom As SqlParameter
			Get
				Return New SqlParameter("@AttFrom", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property AttTo As SqlParameter
			Get
				Return New SqlParameter("@AttTo", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property AttCompanyID As SqlParameter
			Get
				Return New SqlParameter("@AttCompanyID", SqlDbType.VarChar, 50)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const RowID As String = "RowID"
        Public Const CompanyID As String = "CompanyID"
        Public Const ID As String = "ID"
        Public Const AttFrom As String = "AttFrom"
        Public Const AttTo As String = "AttTo"
        Public Const AttCompanyID As String = "AttCompanyID"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(RowID) = _UserDefaults.PropertyNames.RowID
				ht(CompanyID) = _UserDefaults.PropertyNames.CompanyID
				ht(ID) = _UserDefaults.PropertyNames.ID
				ht(AttFrom) = _UserDefaults.PropertyNames.AttFrom
				ht(AttTo) = _UserDefaults.PropertyNames.AttTo
				ht(AttCompanyID) = _UserDefaults.PropertyNames.AttCompanyID

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const RowID As String = "RowID"
        Public Const CompanyID As String = "CompanyID"
        Public Const ID As String = "ID"
        Public Const AttFrom As String = "AttFrom"
        Public Const AttTo As String = "AttTo"
        Public Const AttCompanyID As String = "AttCompanyID"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(RowID) = _UserDefaults.ColumnNames.RowID
				ht(CompanyID) = _UserDefaults.ColumnNames.CompanyID
				ht(ID) = _UserDefaults.ColumnNames.ID
				ht(AttFrom) = _UserDefaults.ColumnNames.AttFrom
				ht(AttTo) = _UserDefaults.ColumnNames.AttTo
				ht(AttCompanyID) = _UserDefaults.ColumnNames.AttCompanyID

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const RowID As String = "s_RowID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const ID As String = "s_ID"
        Public Const AttFrom As String = "s_AttFrom"
        Public Const AttTo As String = "s_AttTo"
        Public Const AttCompanyID As String = "s_AttCompanyID"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property RowID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.RowID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.RowID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property ID As String
			Get
				Return MyBase.GetString(ColumnNames.ID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property AttFrom As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.AttFrom)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.AttFrom, Value)
			End Set
		End Property

		Public Overridable Property AttTo As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.AttTo)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.AttTo, Value)
			End Set
		End Property

		Public Overridable Property AttCompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.AttCompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.AttCompanyID, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_RowID As String
			Get
				If Me.IsColumnNull(ColumnNames.RowID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.RowID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.RowID)
				Else
					Me.RowID = MyBase.SetIntegerAsString(ColumnNames.RowID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetStringAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_AttFrom As String
			Get
				If Me.IsColumnNull(ColumnNames.AttFrom) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.AttFrom)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.AttFrom)
				Else
					Me.AttFrom = MyBase.SetDateTimeAsString(ColumnNames.AttFrom, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_AttTo As String
			Get
				If Me.IsColumnNull(ColumnNames.AttTo) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.AttTo)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.AttTo)
				Else
					Me.AttTo = MyBase.SetDateTimeAsString(ColumnNames.AttTo, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_AttCompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.AttCompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.AttCompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.AttCompanyID)
				Else
					Me.AttCompanyID = MyBase.SetStringAsString(ColumnNames.AttCompanyID, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property RowID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.RowID, Parameters.RowID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property AttFrom() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.AttFrom, Parameters.AttFrom)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property AttTo() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.AttTo, Parameters.AttTo)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property AttCompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.AttCompanyID, Parameters.AttCompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property RowID() As WhereParameter 
			Get
				If _RowID_W Is Nothing Then
					_RowID_W = TearOff.RowID
				End If
				Return _RowID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property AttFrom() As WhereParameter 
			Get
				If _AttFrom_W Is Nothing Then
					_AttFrom_W = TearOff.AttFrom
				End If
				Return _AttFrom_W
			End Get
		End Property

		Public ReadOnly Property AttTo() As WhereParameter 
			Get
				If _AttTo_W Is Nothing Then
					_AttTo_W = TearOff.AttTo
				End If
				Return _AttTo_W
			End Get
		End Property

		Public ReadOnly Property AttCompanyID() As WhereParameter 
			Get
				If _AttCompanyID_W Is Nothing Then
					_AttCompanyID_W = TearOff.AttCompanyID
				End If
				Return _AttCompanyID_W
			End Get
		End Property

		Private _RowID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _ID_W As WhereParameter = Nothing
		Private _AttFrom_W As WhereParameter = Nothing
		Private _AttTo_W As WhereParameter = Nothing
		Private _AttCompanyID_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_RowID_W = Nothing
			_CompanyID_W = Nothing
			_ID_W = Nothing
			_AttFrom_W = Nothing
			_AttTo_W = Nothing
			_AttCompanyID_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property RowID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RowID, Parameters.RowID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property AttFrom() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AttFrom, Parameters.AttFrom)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property AttTo() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AttTo, Parameters.AttTo)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property AttCompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.AttCompanyID, Parameters.AttCompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property RowID() As AggregateParameter 
			Get
				If _RowID_W Is Nothing Then
					_RowID_W = TearOff.RowID
				End If
				Return _RowID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property AttFrom() As AggregateParameter 
			Get
				If _AttFrom_W Is Nothing Then
					_AttFrom_W = TearOff.AttFrom
				End If
				Return _AttFrom_W
			End Get
		End Property

		Public ReadOnly Property AttTo() As AggregateParameter 
			Get
				If _AttTo_W Is Nothing Then
					_AttTo_W = TearOff.AttTo
				End If
				Return _AttTo_W
			End Get
		End Property

		Public ReadOnly Property AttCompanyID() As AggregateParameter 
			Get
				If _AttCompanyID_W Is Nothing Then
					_AttCompanyID_W = TearOff.AttCompanyID
				End If
				Return _AttCompanyID_W
			End Get
		End Property

		Private _RowID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _ID_W As AggregateParameter = Nothing
		Private _AttFrom_W As AggregateParameter = Nothing
		Private _AttTo_W As AggregateParameter = Nothing
		Private _AttCompanyID_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_RowID_W = Nothing
		_CompanyID_W = Nothing
		_ID_W = Nothing
		_AttFrom_W = Nothing
		_AttTo_W = Nothing
		_AttCompanyID_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_UserDefaultsInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.RowID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_UserDefaultsUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_UserDefaultsDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.RowID)
		p.SourceColumn = ColumnNames.RowID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.RowID)
		p.SourceColumn = ColumnNames.RowID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.AttFrom)
		p.SourceColumn = ColumnNames.AttFrom
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.AttTo)
		p.SourceColumn = ColumnNames.AttTo
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.AttCompanyID)
		p.SourceColumn = ColumnNames.AttCompanyID
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

