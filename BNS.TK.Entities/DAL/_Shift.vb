
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject


Namespace BNS.TK.Entities

    Public MustInherit Class _Shift
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "Shift"
            Me.MappingName = "Shift"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_ShiftLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal Shiftcode As Integer) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_Shift.Parameters.CompanyID, CompanyID)
            parameters.Add(_Shift.Parameters.Shiftcode, Shiftcode)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_ShiftLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property Shiftcode As SqlParameter
                Get
                    Return New SqlParameter("@Shiftcode", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Description As SqlParameter
                Get
                    Return New SqlParameter("@Description", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property In1 As SqlParameter
                Get
                    Return New SqlParameter("@In1", SqlDbType.VarChar, 8)
                End Get
            End Property

            Public Shared ReadOnly Property Out1 As SqlParameter
                Get
                    Return New SqlParameter("@Out1", SqlDbType.VarChar, 8)
                End Get
            End Property

            Public Shared ReadOnly Property In2 As SqlParameter
                Get
                    Return New SqlParameter("@In2", SqlDbType.VarChar, 8)
                End Get
            End Property

            Public Shared ReadOnly Property Out2 As SqlParameter
                Get
                    Return New SqlParameter("@Out2", SqlDbType.VarChar, 8)
                End Get
            End Property

            Public Shared ReadOnly Property Flexi As SqlParameter
                Get
                    Return New SqlParameter("@Flexi", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Minimumot As SqlParameter
                Get
                    Return New SqlParameter("@Minimumot", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Crossover As SqlParameter
                Get
                    Return New SqlParameter("@Crossover", SqlDbType.Bit, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedBy As SqlParameter
                Get
                    Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Shortname As SqlParameter
                Get
                    Return New SqlParameter("@Shortname", SqlDbType.VarChar, 50)
                End Get
            End Property

            Public Shared ReadOnly Property Grace_in1 As SqlParameter
                Get
                    Return New SqlParameter("@Grace_in1", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Grace_in2 As SqlParameter
                Get
                    Return New SqlParameter("@Grace_in2", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Flexi_in1 As SqlParameter
                Get
                    Return New SqlParameter("@Flexi_in1", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Flexi_in2 As SqlParameter
                Get
                    Return New SqlParameter("@Flexi_in2", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Advanced_in As SqlParameter
                Get
                    Return New SqlParameter("@Advanced_in", SqlDbType.Bit, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const CompanyID As String = "CompanyID"
            Public Const Shiftcode As String = "shiftcode"
            Public Const Description As String = "description"
            Public Const In1 As String = "in1"
            Public Const Out1 As String = "out1"
            Public Const In2 As String = "in2"
            Public Const Out2 As String = "out2"
            Public Const Flexi As String = "flexi"
            Public Const Minimumot As String = "minimumot"
            Public Const Crossover As String = "crossover"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const Shortname As String = "shortname"
            Public Const Grace_in1 As String = "grace_in1"
            Public Const Grace_in2 As String = "grace_in2"
            Public Const Flexi_in1 As String = "flexi_in1"
            Public Const Flexi_in2 As String = "flexi_in2"
            Public Const Advanced_in As String = "advanced_in"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Shift.PropertyNames.CompanyID
                    ht(Shiftcode) = _Shift.PropertyNames.Shiftcode
                    ht(Description) = _Shift.PropertyNames.Description
                    ht(In1) = _Shift.PropertyNames.In1
                    ht(Out1) = _Shift.PropertyNames.Out1
                    ht(In2) = _Shift.PropertyNames.In2
                    ht(Out2) = _Shift.PropertyNames.Out2
                    ht(Flexi) = _Shift.PropertyNames.Flexi
                    ht(Minimumot) = _Shift.PropertyNames.Minimumot
                    ht(Crossover) = _Shift.PropertyNames.Crossover
                    ht(CreatedBy) = _Shift.PropertyNames.CreatedBy
                    ht(CreatedDate) = _Shift.PropertyNames.CreatedDate
                    ht(LastUpdBy) = _Shift.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _Shift.PropertyNames.LastUpdDate
                    ht(Shortname) = _Shift.PropertyNames.Shortname
                    ht(Grace_in1) = _Shift.PropertyNames.Grace_in1
                    ht(Grace_in2) = _Shift.PropertyNames.Grace_in2
                    ht(Flexi_in1) = _Shift.PropertyNames.Flexi_in1
                    ht(Flexi_in2) = _Shift.PropertyNames.Flexi_in2
                    ht(Advanced_in) = _Shift.PropertyNames.Advanced_in

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const CompanyID As String = "CompanyID"
            Public Const Shiftcode As String = "Shiftcode"
            Public Const Description As String = "Description"
            Public Const In1 As String = "In1"
            Public Const Out1 As String = "Out1"
            Public Const In2 As String = "In2"
            Public Const Out2 As String = "Out2"
            Public Const Flexi As String = "Flexi"
            Public Const Minimumot As String = "Minimumot"
            Public Const Crossover As String = "Crossover"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"
            Public Const Shortname As String = "Shortname"
            Public Const Grace_in1 As String = "Grace_in1"
            Public Const Grace_in2 As String = "Grace_in2"
            Public Const Flexi_in1 As String = "Flexi_in1"
            Public Const Flexi_in2 As String = "Flexi_in2"
            Public Const Advanced_in As String = "Advanced_in"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _Shift.ColumnNames.CompanyID
                    ht(Shiftcode) = _Shift.ColumnNames.Shiftcode
                    ht(Description) = _Shift.ColumnNames.Description
                    ht(In1) = _Shift.ColumnNames.In1
                    ht(Out1) = _Shift.ColumnNames.Out1
                    ht(In2) = _Shift.ColumnNames.In2
                    ht(Out2) = _Shift.ColumnNames.Out2
                    ht(Flexi) = _Shift.ColumnNames.Flexi
                    ht(Minimumot) = _Shift.ColumnNames.Minimumot
                    ht(Crossover) = _Shift.ColumnNames.Crossover
                    ht(CreatedBy) = _Shift.ColumnNames.CreatedBy
                    ht(CreatedDate) = _Shift.ColumnNames.CreatedDate
                    ht(LastUpdBy) = _Shift.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _Shift.ColumnNames.LastUpdDate
                    ht(Shortname) = _Shift.ColumnNames.Shortname
                    ht(Grace_in1) = _Shift.ColumnNames.Grace_in1
                    ht(Grace_in2) = _Shift.ColumnNames.Grace_in2
                    ht(Flexi_in1) = _Shift.ColumnNames.Flexi_in1
                    ht(Flexi_in2) = _Shift.ColumnNames.Flexi_in2
                    ht(Advanced_in) = _Shift.ColumnNames.Advanced_in

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const CompanyID As String = "s_CompanyID"
            Public Const Shiftcode As String = "s_Shiftcode"
            Public Const Description As String = "s_Description"
            Public Const In1 As String = "s_In1"
            Public Const Out1 As String = "s_Out1"
            Public Const In2 As String = "s_In2"
            Public Const Out2 As String = "s_Out2"
            Public Const Flexi As String = "s_Flexi"
            Public Const Minimumot As String = "s_Minimumot"
            Public Const Crossover As String = "s_Crossover"
            Public Const CreatedBy As String = "s_CreatedBy"
            Public Const CreatedDate As String = "s_CreatedDate"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"
            Public Const Shortname As String = "s_Shortname"
            Public Const Grace_in1 As String = "s_Grace_in1"
            Public Const Grace_in2 As String = "s_Grace_in2"
            Public Const Flexi_in1 As String = "s_Flexi_in1"
            Public Const Flexi_in2 As String = "s_Flexi_in2"
            Public Const Advanced_in As String = "s_Advanced_in"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property Shiftcode As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Shiftcode)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Shiftcode, Value)
            End Set
        End Property

        Public Overridable Property Description As String
            Get
                Return MyBase.GetString(ColumnNames.Description)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Description, Value)
            End Set
        End Property

        Public Overridable Property In1 As String
            Get
                Return MyBase.GetString(ColumnNames.In1)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.In1, Value)
            End Set
        End Property

        Public Overridable Property Out1 As String
            Get
                Return MyBase.GetString(ColumnNames.Out1)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Out1, Value)
            End Set
        End Property

        Public Overridable Property In2 As String
            Get
                Return MyBase.GetString(ColumnNames.In2)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.In2, Value)
            End Set
        End Property

        Public Overridable Property Out2 As String
            Get
                Return MyBase.GetString(ColumnNames.Out2)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Out2, Value)
            End Set
        End Property

        Public Overridable Property Flexi As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Flexi)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Flexi, Value)
            End Set
        End Property

        Public Overridable Property Minimumot As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Minimumot)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Minimumot, Value)
            End Set
        End Property

        Public Overridable Property Crossover As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Crossover)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Crossover, Value)
            End Set
        End Property

        Public Overridable Property CreatedBy As String
            Get
                Return MyBase.GetString(ColumnNames.CreatedBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CreatedBy, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property

        Public Overridable Property Shortname As String
            Get
                Return MyBase.GetString(ColumnNames.Shortname)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Shortname, Value)
            End Set
        End Property

        Public Overridable Property Grace_in1 As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Grace_in1)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Grace_in1, Value)
            End Set
        End Property

        Public Overridable Property Grace_in2 As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Grace_in2)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Grace_in2, Value)
            End Set
        End Property

        Public Overridable Property Flexi_in1 As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Flexi_in1)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Flexi_in1, Value)
            End Set
        End Property

        Public Overridable Property Flexi_in2 As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Flexi_in2)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Flexi_in2, Value)
            End Set
        End Property

        Public Overridable Property Advanced_in As Boolean
            Get
                Return MyBase.GetBoolean(ColumnNames.Advanced_in)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.SetBoolean(ColumnNames.Advanced_in, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Shiftcode As String
            Get
                If Me.IsColumnNull(ColumnNames.Shiftcode) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Shiftcode)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Shiftcode)
                Else
                    Me.Shiftcode = MyBase.SetIntegerAsString(ColumnNames.Shiftcode, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Description As String
            Get
                If Me.IsColumnNull(ColumnNames.Description) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Description)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Description)
                Else
                    Me.Description = MyBase.SetStringAsString(ColumnNames.Description, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_In1 As String
            Get
                If Me.IsColumnNull(ColumnNames.In1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.In1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.In1)
                Else
                    Me.In1 = MyBase.SetStringAsString(ColumnNames.In1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Out1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Out1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Out1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Out1)
                Else
                    Me.Out1 = MyBase.SetStringAsString(ColumnNames.Out1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_In2 As String
            Get
                If Me.IsColumnNull(ColumnNames.In2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.In2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.In2)
                Else
                    Me.In2 = MyBase.SetStringAsString(ColumnNames.In2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Out2 As String
            Get
                If Me.IsColumnNull(ColumnNames.Out2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Out2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Out2)
                Else
                    Me.Out2 = MyBase.SetStringAsString(ColumnNames.Out2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Flexi As String
            Get
                If Me.IsColumnNull(ColumnNames.Flexi) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Flexi)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Flexi)
                Else
                    Me.Flexi = MyBase.SetBooleanAsString(ColumnNames.Flexi, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Minimumot As String
            Get
                If Me.IsColumnNull(ColumnNames.Minimumot) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Minimumot)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Minimumot)
                Else
                    Me.Minimumot = MyBase.SetIntegerAsString(ColumnNames.Minimumot, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Crossover As String
            Get
                If Me.IsColumnNull(ColumnNames.Crossover) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Crossover)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Crossover)
                Else
                    Me.Crossover = MyBase.SetBooleanAsString(ColumnNames.Crossover, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedBy As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedBy)
                Else
                    Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Shortname As String
            Get
                If Me.IsColumnNull(ColumnNames.Shortname) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Shortname)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Shortname)
                Else
                    Me.Shortname = MyBase.SetStringAsString(ColumnNames.Shortname, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Grace_in1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Grace_in1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Grace_in1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Grace_in1)
                Else
                    Me.Grace_in1 = MyBase.SetIntegerAsString(ColumnNames.Grace_in1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Grace_in2 As String
            Get
                If Me.IsColumnNull(ColumnNames.Grace_in2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Grace_in2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Grace_in2)
                Else
                    Me.Grace_in2 = MyBase.SetIntegerAsString(ColumnNames.Grace_in2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Flexi_in1 As String
            Get
                If Me.IsColumnNull(ColumnNames.Flexi_in1) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Flexi_in1)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Flexi_in1)
                Else
                    Me.Flexi_in1 = MyBase.SetIntegerAsString(ColumnNames.Flexi_in1, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Flexi_in2 As String
            Get
                If Me.IsColumnNull(ColumnNames.Flexi_in2) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Flexi_in2)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Flexi_in2)
                Else
                    Me.Flexi_in2 = MyBase.SetIntegerAsString(ColumnNames.Flexi_in2, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Advanced_in As String
            Get
                If Me.IsColumnNull(ColumnNames.Advanced_in) Then
                    Return String.Empty
                Else
                    Return MyBase.GetBooleanAsString(ColumnNames.Advanced_in)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Advanced_in)
                Else
                    Me.Advanced_in = MyBase.SetBooleanAsString(ColumnNames.Advanced_in, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Shiftcode() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Shiftcode, Parameters.Shiftcode)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Description() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Description, Parameters.Description)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.In1, Parameters.In1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Out1, Parameters.Out1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.In2, Parameters.In2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Out2, Parameters.Out2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Flexi() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Flexi, Parameters.Flexi)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Minimumot() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Minimumot, Parameters.Minimumot)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Crossover() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Crossover, Parameters.Crossover)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Shortname() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Shortname, Parameters.Shortname)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Grace_in1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Grace_in1, Parameters.Grace_in1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Grace_in2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Grace_in2, Parameters.Grace_in2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Flexi_in1() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Flexi_in1, Parameters.Flexi_in1)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Flexi_in2() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Flexi_in2, Parameters.Flexi_in2)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Advanced_in() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Advanced_in, Parameters.Advanced_in)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property Shiftcode() As WhereParameter
                Get
                    If _Shiftcode_W Is Nothing Then
                        _Shiftcode_W = TearOff.Shiftcode
                    End If
                    Return _Shiftcode_W
                End Get
            End Property

            Public ReadOnly Property Description() As WhereParameter
                Get
                    If _Description_W Is Nothing Then
                        _Description_W = TearOff.Description
                    End If
                    Return _Description_W
                End Get
            End Property

            Public ReadOnly Property In1() As WhereParameter
                Get
                    If _In1_W Is Nothing Then
                        _In1_W = TearOff.In1
                    End If
                    Return _In1_W
                End Get
            End Property

            Public ReadOnly Property Out1() As WhereParameter
                Get
                    If _Out1_W Is Nothing Then
                        _Out1_W = TearOff.Out1
                    End If
                    Return _Out1_W
                End Get
            End Property

            Public ReadOnly Property In2() As WhereParameter
                Get
                    If _In2_W Is Nothing Then
                        _In2_W = TearOff.In2
                    End If
                    Return _In2_W
                End Get
            End Property

            Public ReadOnly Property Out2() As WhereParameter
                Get
                    If _Out2_W Is Nothing Then
                        _Out2_W = TearOff.Out2
                    End If
                    Return _Out2_W
                End Get
            End Property

            Public ReadOnly Property Flexi() As WhereParameter
                Get
                    If _Flexi_W Is Nothing Then
                        _Flexi_W = TearOff.Flexi
                    End If
                    Return _Flexi_W
                End Get
            End Property

            Public ReadOnly Property Minimumot() As WhereParameter
                Get
                    If _Minimumot_W Is Nothing Then
                        _Minimumot_W = TearOff.Minimumot
                    End If
                    Return _Minimumot_W
                End Get
            End Property

            Public ReadOnly Property Crossover() As WhereParameter
                Get
                    If _Crossover_W Is Nothing Then
                        _Crossover_W = TearOff.Crossover
                    End If
                    Return _Crossover_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As WhereParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property Shortname() As WhereParameter
                Get
                    If _Shortname_W Is Nothing Then
                        _Shortname_W = TearOff.Shortname
                    End If
                    Return _Shortname_W
                End Get
            End Property

            Public ReadOnly Property Grace_in1() As WhereParameter
                Get
                    If _Grace_in1_W Is Nothing Then
                        _Grace_in1_W = TearOff.Grace_in1
                    End If
                    Return _Grace_in1_W
                End Get
            End Property

            Public ReadOnly Property Grace_in2() As WhereParameter
                Get
                    If _Grace_in2_W Is Nothing Then
                        _Grace_in2_W = TearOff.Grace_in2
                    End If
                    Return _Grace_in2_W
                End Get
            End Property

            Public ReadOnly Property Flexi_in1() As WhereParameter
                Get
                    If _Flexi_in1_W Is Nothing Then
                        _Flexi_in1_W = TearOff.Flexi_in1
                    End If
                    Return _Flexi_in1_W
                End Get
            End Property

            Public ReadOnly Property Flexi_in2() As WhereParameter
                Get
                    If _Flexi_in2_W Is Nothing Then
                        _Flexi_in2_W = TearOff.Flexi_in2
                    End If
                    Return _Flexi_in2_W
                End Get
            End Property

            Public ReadOnly Property Advanced_in() As WhereParameter
                Get
                    If _Advanced_in_W Is Nothing Then
                        _Advanced_in_W = TearOff.Advanced_in
                    End If
                    Return _Advanced_in_W
                End Get
            End Property

            Private _CompanyID_W As WhereParameter = Nothing
            Private _Shiftcode_W As WhereParameter = Nothing
            Private _Description_W As WhereParameter = Nothing
            Private _In1_W As WhereParameter = Nothing
            Private _Out1_W As WhereParameter = Nothing
            Private _In2_W As WhereParameter = Nothing
            Private _Out2_W As WhereParameter = Nothing
            Private _Flexi_W As WhereParameter = Nothing
            Private _Minimumot_W As WhereParameter = Nothing
            Private _Crossover_W As WhereParameter = Nothing
            Private _CreatedBy_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing
            Private _Shortname_W As WhereParameter = Nothing
            Private _Grace_in1_W As WhereParameter = Nothing
            Private _Grace_in2_W As WhereParameter = Nothing
            Private _Flexi_in1_W As WhereParameter = Nothing
            Private _Flexi_in2_W As WhereParameter = Nothing
            Private _Advanced_in_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _CompanyID_W = Nothing
                _Shiftcode_W = Nothing
                _Description_W = Nothing
                _In1_W = Nothing
                _Out1_W = Nothing
                _In2_W = Nothing
                _Out2_W = Nothing
                _Flexi_W = Nothing
                _Minimumot_W = Nothing
                _Crossover_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _Shortname_W = Nothing
                _Grace_in1_W = Nothing
                _Grace_in2_W = Nothing
                _Flexi_in1_W = Nothing
                _Flexi_in2_W = Nothing
                _Advanced_in_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Shiftcode() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Shiftcode, Parameters.Shiftcode)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Description() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Description, Parameters.Description)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.In1, Parameters.In1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Out1, Parameters.Out1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property In2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.In2, Parameters.In2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Out2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Out2, Parameters.Out2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Flexi() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Flexi, Parameters.Flexi)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Minimumot() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Minimumot, Parameters.Minimumot)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Crossover() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Crossover, Parameters.Crossover)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Shortname() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Shortname, Parameters.Shortname)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Grace_in1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Grace_in1, Parameters.Grace_in1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Grace_in2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Grace_in2, Parameters.Grace_in2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Flexi_in1() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Flexi_in1, Parameters.Flexi_in1)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Flexi_in2() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Flexi_in2, Parameters.Flexi_in2)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Advanced_in() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Advanced_in, Parameters.Advanced_in)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property Shiftcode() As AggregateParameter
                Get
                    If _Shiftcode_W Is Nothing Then
                        _Shiftcode_W = TearOff.Shiftcode
                    End If
                    Return _Shiftcode_W
                End Get
            End Property

            Public ReadOnly Property Description() As AggregateParameter
                Get
                    If _Description_W Is Nothing Then
                        _Description_W = TearOff.Description
                    End If
                    Return _Description_W
                End Get
            End Property

            Public ReadOnly Property In1() As AggregateParameter
                Get
                    If _In1_W Is Nothing Then
                        _In1_W = TearOff.In1
                    End If
                    Return _In1_W
                End Get
            End Property

            Public ReadOnly Property Out1() As AggregateParameter
                Get
                    If _Out1_W Is Nothing Then
                        _Out1_W = TearOff.Out1
                    End If
                    Return _Out1_W
                End Get
            End Property

            Public ReadOnly Property In2() As AggregateParameter
                Get
                    If _In2_W Is Nothing Then
                        _In2_W = TearOff.In2
                    End If
                    Return _In2_W
                End Get
            End Property

            Public ReadOnly Property Out2() As AggregateParameter
                Get
                    If _Out2_W Is Nothing Then
                        _Out2_W = TearOff.Out2
                    End If
                    Return _Out2_W
                End Get
            End Property

            Public ReadOnly Property Flexi() As AggregateParameter
                Get
                    If _Flexi_W Is Nothing Then
                        _Flexi_W = TearOff.Flexi
                    End If
                    Return _Flexi_W
                End Get
            End Property

            Public ReadOnly Property Minimumot() As AggregateParameter
                Get
                    If _Minimumot_W Is Nothing Then
                        _Minimumot_W = TearOff.Minimumot
                    End If
                    Return _Minimumot_W
                End Get
            End Property

            Public ReadOnly Property Crossover() As AggregateParameter
                Get
                    If _Crossover_W Is Nothing Then
                        _Crossover_W = TearOff.Crossover
                    End If
                    Return _Crossover_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As AggregateParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Public ReadOnly Property Shortname() As AggregateParameter
                Get
                    If _Shortname_W Is Nothing Then
                        _Shortname_W = TearOff.Shortname
                    End If
                    Return _Shortname_W
                End Get
            End Property

            Public ReadOnly Property Grace_in1() As AggregateParameter
                Get
                    If _Grace_in1_W Is Nothing Then
                        _Grace_in1_W = TearOff.Grace_in1
                    End If
                    Return _Grace_in1_W
                End Get
            End Property

            Public ReadOnly Property Grace_in2() As AggregateParameter
                Get
                    If _Grace_in2_W Is Nothing Then
                        _Grace_in2_W = TearOff.Grace_in2
                    End If
                    Return _Grace_in2_W
                End Get
            End Property

            Public ReadOnly Property Flexi_in1() As AggregateParameter
                Get
                    If _Flexi_in1_W Is Nothing Then
                        _Flexi_in1_W = TearOff.Flexi_in1
                    End If
                    Return _Flexi_in1_W
                End Get
            End Property

            Public ReadOnly Property Flexi_in2() As AggregateParameter
                Get
                    If _Flexi_in2_W Is Nothing Then
                        _Flexi_in2_W = TearOff.Flexi_in2
                    End If
                    Return _Flexi_in2_W
                End Get
            End Property

            Public ReadOnly Property Advanced_in() As AggregateParameter
                Get
                    If _Advanced_in_W Is Nothing Then
                        _Advanced_in_W = TearOff.Advanced_in
                    End If
                    Return _Advanced_in_W
                End Get
            End Property

            Private _CompanyID_W As AggregateParameter = Nothing
            Private _Shiftcode_W As AggregateParameter = Nothing
            Private _Description_W As AggregateParameter = Nothing
            Private _In1_W As AggregateParameter = Nothing
            Private _Out1_W As AggregateParameter = Nothing
            Private _In2_W As AggregateParameter = Nothing
            Private _Out2_W As AggregateParameter = Nothing
            Private _Flexi_W As AggregateParameter = Nothing
            Private _Minimumot_W As AggregateParameter = Nothing
            Private _Crossover_W As AggregateParameter = Nothing
            Private _CreatedBy_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing
            Private _Shortname_W As AggregateParameter = Nothing
            Private _Grace_in1_W As AggregateParameter = Nothing
            Private _Grace_in2_W As AggregateParameter = Nothing
            Private _Flexi_in1_W As AggregateParameter = Nothing
            Private _Flexi_in2_W As AggregateParameter = Nothing
            Private _Advanced_in_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _CompanyID_W = Nothing
                _Shiftcode_W = Nothing
                _Description_W = Nothing
                _In1_W = Nothing
                _Out1_W = Nothing
                _In2_W = Nothing
                _Out2_W = Nothing
                _Flexi_W = Nothing
                _Minimumot_W = Nothing
                _Crossover_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                _Shortname_W = Nothing
                _Grace_in1_W = Nothing
                _Grace_in2_W = Nothing
                _Flexi_in1_W = Nothing
                _Flexi_in2_W = Nothing
                _Advanced_in_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_ShiftInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.Shiftcode.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_ShiftUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_ShiftDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Shiftcode)
            p.SourceColumn = ColumnNames.Shiftcode
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Shiftcode)
            p.SourceColumn = ColumnNames.Shiftcode
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Description)
            p.SourceColumn = ColumnNames.Description
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.In1)
            p.SourceColumn = ColumnNames.In1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Out1)
            p.SourceColumn = ColumnNames.Out1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.In2)
            p.SourceColumn = ColumnNames.In2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Out2)
            p.SourceColumn = ColumnNames.Out2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Flexi)
            p.SourceColumn = ColumnNames.Flexi
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Minimumot)
            p.SourceColumn = ColumnNames.Minimumot
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Crossover)
            p.SourceColumn = ColumnNames.Crossover
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedBy)
            p.SourceColumn = ColumnNames.CreatedBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Shortname)
            p.SourceColumn = ColumnNames.Shortname
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Grace_in1)
            p.SourceColumn = ColumnNames.Grace_in1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Grace_in2)
            p.SourceColumn = ColumnNames.Grace_in2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Flexi_in1)
            p.SourceColumn = ColumnNames.Flexi_in1
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Flexi_in2)
            p.SourceColumn = ColumnNames.Flexi_in2
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Advanced_in)
            p.SourceColumn = ColumnNames.Advanced_in
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

