
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _VerifiedTrans
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "VerifiedTrans"
            Me.MappingName = "VerifiedTrans"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_VerifiedTransLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String,
                                                     ByVal EmployeeID As String,
                                                     ByVal StartDate As DateTime,
                                                     ByVal EndDate As DateTime) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_VerifiedTrans.Parameters.CompanyID, CompanyID)
            parameters.Add(_VerifiedTrans.Parameters.EmployeeID, EmployeeID)
            parameters.Add(_VerifiedTrans.Parameters.StartDate, StartDate)
            parameters.Add(_VerifiedTrans.Parameters.EndDate, EndDate)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_VerifiedTransLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property ID As SqlParameter
                Get
                    Return New SqlParameter("@ID", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property EmployeeID As SqlParameter
                Get
                    Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property DateVerified As SqlParameter
                Get
                    Return New SqlParameter("@DateVerified", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property StartDate As SqlParameter
                Get
                    Return New SqlParameter("@StartDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property EndDate As SqlParameter
                Get
                    Return New SqlParameter("@EndDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const ID As String = "ID"
            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const DateVerified As String = "DateVerified"
            Public Const StartDate As String = "StartDate"
            Public Const EndDate As String = "EndDate"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _VerifiedTrans.PropertyNames.ID
                    ht(CompanyID) = _VerifiedTrans.PropertyNames.CompanyID
                    ht(EmployeeID) = _VerifiedTrans.PropertyNames.EmployeeID
                    ht(DateVerified) = _VerifiedTrans.PropertyNames.DateVerified
                    ht(StartDate) = _VerifiedTrans.PropertyNames.StartDate
                    ht(EndDate) = _VerifiedTrans.PropertyNames.EndDate

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const ID As String = "ID"
            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const DateVerified As String = "DateVerified"
            Public Const StartDate As String = "StartDate"
            Public Const EndDate As String = "EndDate"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(ID) = _VerifiedTrans.ColumnNames.ID
                    ht(CompanyID) = _VerifiedTrans.ColumnNames.CompanyID
                    ht(EmployeeID) = _VerifiedTrans.ColumnNames.EmployeeID
                    ht(DateVerified) = _VerifiedTrans.ColumnNames.DateVerified
                    ht(StartDate) = _VerifiedTrans.ColumnNames.StartDate
                    ht(EndDate) = _VerifiedTrans.ColumnNames.EndDate

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const ID As String = "s_ID"
            Public Const CompanyID As String = "s_CompanyID"
            Public Const EmployeeID As String = "s_EmployeeID"
            Public Const DateVerified As String = "s_DateVerified"
            Public Const StartDate As String = "s_StartDate"
            Public Const EndDate As String = "s_EndDate"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property ID As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.ID)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.ID, Value)
            End Set
        End Property

        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property EmployeeID As String
            Get
                Return MyBase.GetString(ColumnNames.EmployeeID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.EmployeeID, Value)
            End Set
        End Property

        Public Overridable Property DateVerified As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.DateVerified)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.DateVerified, Value)
            End Set
        End Property

        Public Overridable Property StartDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.StartDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.StartDate, Value)
            End Set
        End Property

        Public Overridable Property EndDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.EndDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.EndDate, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_ID As String
            Get
                If Me.IsColumnNull(ColumnNames.ID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.ID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.ID)
                Else
                    Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EmployeeID As String
            Get
                If Me.IsColumnNull(ColumnNames.EmployeeID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EmployeeID)
                Else
                    Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_DateVerified As String
            Get
                If Me.IsColumnNull(ColumnNames.DateVerified) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.DateVerified)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.DateVerified)
                Else
                    Me.DateVerified = MyBase.SetDateTimeAsString(ColumnNames.DateVerified, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_StartDate As String
            Get
                If Me.IsColumnNull(ColumnNames.StartDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.StartDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.StartDate)
                Else
                    Me.StartDate = MyBase.SetDateTimeAsString(ColumnNames.StartDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EndDate As String
            Get
                If Me.IsColumnNull(ColumnNames.EndDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.EndDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EndDate)
                Else
                    Me.EndDate = MyBase.SetDateTimeAsString(ColumnNames.EndDate, Value)
                End If
            End Set
        End Property

#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DateVerified() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.DateVerified, Parameters.DateVerified)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property StartDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.StartDate, Parameters.StartDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EndDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EndDate, Parameters.EndDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property ID() As WhereParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As WhereParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property DateVerified() As WhereParameter
                Get
                    If _DateVerified_W Is Nothing Then
                        _DateVerified_W = TearOff.DateVerified
                    End If
                    Return _DateVerified_W
                End Get
            End Property

            Public ReadOnly Property StartDate() As WhereParameter
                Get
                    If _StartDate_W Is Nothing Then
                        _StartDate_W = TearOff.StartDate
                    End If
                    Return _StartDate_W
                End Get
            End Property

            Public ReadOnly Property EndDate() As WhereParameter
                Get
                    If _EndDate_W Is Nothing Then
                        _EndDate_W = TearOff.EndDate
                    End If
                    Return _EndDate_W
                End Get
            End Property

            Private _ID_W As WhereParameter = Nothing
            Private _CompanyID_W As WhereParameter = Nothing
            Private _EmployeeID_W As WhereParameter = Nothing
            Private _DateVerified_W As WhereParameter = Nothing
            Private _StartDate_W As WhereParameter = Nothing
            Private _EndDate_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _ID_W = Nothing
                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _DateVerified_W = Nothing
                _StartDate_W = Nothing
                _EndDate_W = Nothing

                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property ID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DateVerified() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DateVerified, Parameters.DateVerified)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property StartDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.StartDate, Parameters.StartDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EndDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EndDate, Parameters.EndDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property ID() As AggregateParameter
                Get
                    If _ID_W Is Nothing Then
                        _ID_W = TearOff.ID
                    End If
                    Return _ID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As AggregateParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property DateVerified() As AggregateParameter
                Get
                    If _DateVerified_W Is Nothing Then
                        _DateVerified_W = TearOff.DateVerified
                    End If
                    Return _DateVerified_W
                End Get
            End Property

            Public ReadOnly Property StartDate() As AggregateParameter
                Get
                    If _StartDate_W Is Nothing Then
                        _StartDate_W = TearOff.StartDate
                    End If
                    Return _StartDate_W
                End Get
            End Property

            Public ReadOnly Property EndDate() As AggregateParameter
                Get
                    If _EndDate_W Is Nothing Then
                        _EndDate_W = TearOff.EndDate
                    End If
                    Return _EndDate_W
                End Get
            End Property

            Private _ID_W As AggregateParameter = Nothing
            Private _CompanyID_W As AggregateParameter = Nothing
            Private _EmployeeID_W As AggregateParameter = Nothing
            Private _DateVerified_W As AggregateParameter = Nothing
            Private _StartDate_W As AggregateParameter = Nothing
            Private _EndDate_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _ID_W = Nothing
                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _DateVerified_W = Nothing
                _StartDate_W = Nothing
                _EndDate_W = Nothing

                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_VerifiedTransInsert]"

            CreateParameters(cmd)

            Dim p As SqlParameter
            p = cmd.Parameters(Parameters.ID.ParameterName)
            p.Direction = ParameterDirection.Output

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_VerifiedTransUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_VerifiedTransDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.ID)
            p.SourceColumn = ColumnNames.ID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.DateVerified)
            p.SourceColumn = ColumnNames.DateVerified
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.StartDate)
            p.SourceColumn = ColumnNames.StartDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EndDate)
            p.SourceColumn = ColumnNames.EndDate
            p.SourceVersion = DataRowVersion.Current

        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

