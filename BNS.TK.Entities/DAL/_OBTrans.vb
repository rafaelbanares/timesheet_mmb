
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

Namespace BNS.TK.Entities

    Public MustInherit Class _OBTrans
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "OBTrans"
            Me.MappingName = "OBTrans"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OBTransLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal CompanyID As String, ByVal EmployeeID As String, ByVal OBDate As DateTime) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_OBTrans.Parameters.CompanyID, CompanyID)
            parameters.Add(_OBTrans.Parameters.EmployeeID, EmployeeID)
            parameters.Add(_OBTrans.Parameters.OBDate, OBDate)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_OBTransLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property EmployeeID As SqlParameter
                Get
                    Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property OBDate As SqlParameter
                Get
                    Return New SqlParameter("@OBDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property StartDate As SqlParameter
                Get
                    Return New SqlParameter("@StartDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property EndDate As SqlParameter
                Get
                    Return New SqlParameter("@EndDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property Status As SqlParameter
                Get
                    Return New SqlParameter("@Status", SqlDbType.VarChar, 15)
                End Get
            End Property

            Public Shared ReadOnly Property Remarks As SqlParameter
                Get
                    Return New SqlParameter("@Remarks", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property Stage As SqlParameter
                Get
                    Return New SqlParameter("@Stage", SqlDbType.Int, 0)
                End Get
            End Property

            Public Shared ReadOnly Property DeclinedReason As SqlParameter
                Get
                    Return New SqlParameter("@DeclinedReason", SqlDbType.VarChar, 100)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedBy As SqlParameter
                Get
                    Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdBy As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property LastUpdDate As SqlParameter
                Get
                    Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const OBDate As String = "OBDate"
            Public Const StartDate As String = "StartDate"
            Public Const EndDate As String = "EndDate"
            Public Const Status As String = "Status"
            Public Const Remarks As String = "Remarks"
            Public Const Stage As String = "Stage"
            Public Const DeclinedReason As String = "DeclinedReason"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _OBTrans.PropertyNames.CompanyID
                    ht(EmployeeID) = _OBTrans.PropertyNames.EmployeeID
                    ht(OBDate) = _OBTrans.PropertyNames.OBDate
                    ht(StartDate) = _OBTrans.PropertyNames.StartDate
                    ht(EndDate) = _OBTrans.PropertyNames.EndDate
                    ht(Status) = _OBTrans.PropertyNames.Status
                    ht(Remarks) = _OBTrans.PropertyNames.Remarks
                    ht(Stage) = _OBTrans.PropertyNames.Stage
                    ht(DeclinedReason) = _OBTrans.PropertyNames.DeclinedReason
                    ht(CreatedBy) = _OBTrans.PropertyNames.CreatedBy
                    ht(CreatedDate) = _OBTrans.PropertyNames.CreatedDate
                    ht(LastUpdBy) = _OBTrans.PropertyNames.LastUpdBy
                    ht(LastUpdDate) = _OBTrans.PropertyNames.LastUpdDate

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const CompanyID As String = "CompanyID"
            Public Const EmployeeID As String = "EmployeeID"
            Public Const OBDate As String = "OBDate"
            Public Const StartDate As String = "StartDate"
            Public Const EndDate As String = "EndDate"
            Public Const Status As String = "Status"
            Public Const Remarks As String = "Remarks"
            Public Const Stage As String = "Stage"
            Public Const DeclinedReason As String = "DeclinedReason"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"
            Public Const LastUpdBy As String = "LastUpdBy"
            Public Const LastUpdDate As String = "LastUpdDate"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(CompanyID) = _OBTrans.ColumnNames.CompanyID
                    ht(EmployeeID) = _OBTrans.ColumnNames.EmployeeID
                    ht(OBDate) = _OBTrans.ColumnNames.OBDate
                    ht(StartDate) = _OBTrans.ColumnNames.StartDate
                    ht(EndDate) = _OBTrans.ColumnNames.EndDate
                    ht(Status) = _OBTrans.ColumnNames.Status
                    ht(Remarks) = _OBTrans.ColumnNames.Remarks
                    ht(Stage) = _OBTrans.ColumnNames.Stage
                    ht(DeclinedReason) = _OBTrans.ColumnNames.DeclinedReason
                    ht(CreatedBy) = _OBTrans.ColumnNames.CreatedBy
                    ht(CreatedDate) = _OBTrans.ColumnNames.CreatedDate
                    ht(LastUpdBy) = _OBTrans.ColumnNames.LastUpdBy
                    ht(LastUpdDate) = _OBTrans.ColumnNames.LastUpdDate

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const CompanyID As String = "s_CompanyID"
            Public Const EmployeeID As String = "s_EmployeeID"
            Public Const OBDate As String = "s_OBDate"
            Public Const StartDate As String = "s_StartDate"
            Public Const EndDate As String = "s_EndDate"
            Public Const Status As String = "s_Status"
            Public Const Remarks As String = "s_Remarks"
            Public Const Stage As String = "s_Stage"
            Public Const DeclinedReason As String = "s_DeclinedReason"
            Public Const CreatedBy As String = "s_CreatedBy"
            Public Const CreatedDate As String = "s_CreatedDate"
            Public Const LastUpdBy As String = "s_LastUpdBy"
            Public Const LastUpdDate As String = "s_LastUpdDate"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property EmployeeID As String
            Get
                Return MyBase.GetString(ColumnNames.EmployeeID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.EmployeeID, Value)
            End Set
        End Property

        Public Overridable Property OBDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.OBDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.OBDate, Value)
            End Set
        End Property

        Public Overridable Property StartDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.StartDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.StartDate, Value)
            End Set
        End Property

        Public Overridable Property EndDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.EndDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.EndDate, Value)
            End Set
        End Property

        Public Overridable Property Status As String
            Get
                Return MyBase.GetString(ColumnNames.Status)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Status, Value)
            End Set
        End Property

        Public Overridable Property Remarks As String
            Get
                Return MyBase.GetString(ColumnNames.Remarks)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.Remarks, Value)
            End Set
        End Property

        Public Overridable Property Stage As Integer
            Get
                Return MyBase.GetInteger(ColumnNames.Stage)
            End Get
            Set(ByVal Value As Integer)
                MyBase.SetInteger(ColumnNames.Stage, Value)
            End Set
        End Property

        Public Overridable Property DeclinedReason As String
            Get
                Return MyBase.GetString(ColumnNames.DeclinedReason)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.DeclinedReason, Value)
            End Set
        End Property

        Public Overridable Property CreatedBy As String
            Get
                Return MyBase.GetString(ColumnNames.CreatedBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CreatedBy, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property

        Public Overridable Property LastUpdBy As String
            Get
                Return MyBase.GetString(ColumnNames.LastUpdBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.LastUpdBy, Value)
            End Set
        End Property

        Public Overridable Property LastUpdDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EmployeeID As String
            Get
                If Me.IsColumnNull(ColumnNames.EmployeeID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EmployeeID)
                Else
                    Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_OBDate As String
            Get
                If Me.IsColumnNull(ColumnNames.OBDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.OBDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.OBDate)
                Else
                    Me.OBDate = MyBase.SetDateTimeAsString(ColumnNames.OBDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_StartDate As String
            Get
                If Me.IsColumnNull(ColumnNames.StartDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.StartDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.StartDate)
                Else
                    Me.StartDate = MyBase.SetDateTimeAsString(ColumnNames.StartDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_EndDate As String
            Get
                If Me.IsColumnNull(ColumnNames.EndDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.EndDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.EndDate)
                Else
                    Me.EndDate = MyBase.SetDateTimeAsString(ColumnNames.EndDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Status As String
            Get
                If Me.IsColumnNull(ColumnNames.Status) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Status)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Status)
                Else
                    Me.Status = MyBase.SetStringAsString(ColumnNames.Status, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Remarks As String
            Get
                If Me.IsColumnNull(ColumnNames.Remarks) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.Remarks)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Remarks)
                Else
                    Me.Remarks = MyBase.SetStringAsString(ColumnNames.Remarks, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_Stage As String
            Get
                If Me.IsColumnNull(ColumnNames.Stage) Then
                    Return String.Empty
                Else
                    Return MyBase.GetIntegerAsString(ColumnNames.Stage)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.Stage)
                Else
                    Me.Stage = MyBase.SetIntegerAsString(ColumnNames.Stage, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_DeclinedReason As String
            Get
                If Me.IsColumnNull(ColumnNames.DeclinedReason) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.DeclinedReason)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.DeclinedReason)
                Else
                    Me.DeclinedReason = MyBase.SetStringAsString(ColumnNames.DeclinedReason, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedBy As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedBy)
                Else
                    Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdBy As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdBy)
                Else
                    Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_LastUpdDate As String
            Get
                If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.LastUpdDate)
                Else
                    Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OBDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.OBDate, Parameters.OBDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property StartDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.StartDate, Parameters.StartDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EndDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.EndDate, Parameters.EndDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Remarks() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Remarks, Parameters.Remarks)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As WhereParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property OBDate() As WhereParameter
                Get
                    If _OBDate_W Is Nothing Then
                        _OBDate_W = TearOff.OBDate
                    End If
                    Return _OBDate_W
                End Get
            End Property

            Public ReadOnly Property StartDate() As WhereParameter
                Get
                    If _StartDate_W Is Nothing Then
                        _StartDate_W = TearOff.StartDate
                    End If
                    Return _StartDate_W
                End Get
            End Property

            Public ReadOnly Property EndDate() As WhereParameter
                Get
                    If _EndDate_W Is Nothing Then
                        _EndDate_W = TearOff.EndDate
                    End If
                    Return _EndDate_W
                End Get
            End Property

            Public ReadOnly Property Status() As WhereParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Remarks() As WhereParameter
                Get
                    If _Remarks_W Is Nothing Then
                        _Remarks_W = TearOff.Remarks
                    End If
                    Return _Remarks_W
                End Get
            End Property

            Public ReadOnly Property Stage() As WhereParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As WhereParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As WhereParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As WhereParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As WhereParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Private _CompanyID_W As WhereParameter = Nothing
            Private _EmployeeID_W As WhereParameter = Nothing
            Private _OBDate_W As WhereParameter = Nothing
            Private _StartDate_W As WhereParameter = Nothing
            Private _EndDate_W As WhereParameter = Nothing
            Private _Status_W As WhereParameter = Nothing
            Private _Remarks_W As WhereParameter = Nothing
            Private _Stage_W As WhereParameter = Nothing
            Private _DeclinedReason_W As WhereParameter = Nothing
            Private _CreatedBy_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing
            Private _LastUpdBy_W As WhereParameter = Nothing
            Private _LastUpdDate_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _OBDate_W = Nothing
                _StartDate_W = Nothing
                _EndDate_W = Nothing
                _Status_W = Nothing
                _Remarks_W = Nothing
                _Stage_W = Nothing
                _DeclinedReason_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EmployeeID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property OBDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OBDate, Parameters.OBDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property StartDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.StartDate, Parameters.StartDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property EndDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EndDate, Parameters.EndDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Status() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Status, Parameters.Status)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Remarks() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Remarks, Parameters.Remarks)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property Stage() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Stage, Parameters.Stage)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property DeclinedReason() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DeclinedReason, Parameters.DeclinedReason)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property LastUpdDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property EmployeeID() As AggregateParameter
                Get
                    If _EmployeeID_W Is Nothing Then
                        _EmployeeID_W = TearOff.EmployeeID
                    End If
                    Return _EmployeeID_W
                End Get
            End Property

            Public ReadOnly Property OBDate() As AggregateParameter
                Get
                    If _OBDate_W Is Nothing Then
                        _OBDate_W = TearOff.OBDate
                    End If
                    Return _OBDate_W
                End Get
            End Property

            Public ReadOnly Property StartDate() As AggregateParameter
                Get
                    If _StartDate_W Is Nothing Then
                        _StartDate_W = TearOff.StartDate
                    End If
                    Return _StartDate_W
                End Get
            End Property

            Public ReadOnly Property EndDate() As AggregateParameter
                Get
                    If _EndDate_W Is Nothing Then
                        _EndDate_W = TearOff.EndDate
                    End If
                    Return _EndDate_W
                End Get
            End Property

            Public ReadOnly Property Status() As AggregateParameter
                Get
                    If _Status_W Is Nothing Then
                        _Status_W = TearOff.Status
                    End If
                    Return _Status_W
                End Get
            End Property

            Public ReadOnly Property Remarks() As AggregateParameter
                Get
                    If _Remarks_W Is Nothing Then
                        _Remarks_W = TearOff.Remarks
                    End If
                    Return _Remarks_W
                End Get
            End Property

            Public ReadOnly Property Stage() As AggregateParameter
                Get
                    If _Stage_W Is Nothing Then
                        _Stage_W = TearOff.Stage
                    End If
                    Return _Stage_W
                End Get
            End Property

            Public ReadOnly Property DeclinedReason() As AggregateParameter
                Get
                    If _DeclinedReason_W Is Nothing Then
                        _DeclinedReason_W = TearOff.DeclinedReason
                    End If
                    Return _DeclinedReason_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As AggregateParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Public ReadOnly Property LastUpdBy() As AggregateParameter
                Get
                    If _LastUpdBy_W Is Nothing Then
                        _LastUpdBy_W = TearOff.LastUpdBy
                    End If
                    Return _LastUpdBy_W
                End Get
            End Property

            Public ReadOnly Property LastUpdDate() As AggregateParameter
                Get
                    If _LastUpdDate_W Is Nothing Then
                        _LastUpdDate_W = TearOff.LastUpdDate
                    End If
                    Return _LastUpdDate_W
                End Get
            End Property

            Private _CompanyID_W As AggregateParameter = Nothing
            Private _EmployeeID_W As AggregateParameter = Nothing
            Private _OBDate_W As AggregateParameter = Nothing
            Private _StartDate_W As AggregateParameter = Nothing
            Private _EndDate_W As AggregateParameter = Nothing
            Private _Status_W As AggregateParameter = Nothing
            Private _Remarks_W As AggregateParameter = Nothing
            Private _Stage_W As AggregateParameter = Nothing
            Private _DeclinedReason_W As AggregateParameter = Nothing
            Private _CreatedBy_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing
            Private _LastUpdBy_W As AggregateParameter = Nothing
            Private _LastUpdDate_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _CompanyID_W = Nothing
                _EmployeeID_W = Nothing
                _OBDate_W = Nothing
                _StartDate_W = Nothing
                _EndDate_W = Nothing
                _Status_W = Nothing
                _Remarks_W = Nothing
                _Stage_W = Nothing
                _DeclinedReason_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                _LastUpdBy_W = Nothing
                _LastUpdDate_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OBTransInsert]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OBTransUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_OBTransDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OBDate)
            p.SourceColumn = ColumnNames.OBDate
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EmployeeID)
            p.SourceColumn = ColumnNames.EmployeeID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.OBDate)
            p.SourceColumn = ColumnNames.OBDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.StartDate)
            p.SourceColumn = ColumnNames.StartDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.EndDate)
            p.SourceColumn = ColumnNames.EndDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Status)
            p.SourceColumn = ColumnNames.Status
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Remarks)
            p.SourceColumn = ColumnNames.Remarks
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.Stage)
            p.SourceColumn = ColumnNames.Stage
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.DeclinedReason)
            p.SourceColumn = ColumnNames.DeclinedReason
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedBy)
            p.SourceColumn = ColumnNames.CreatedBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdBy)
            p.SourceColumn = ColumnNames.LastUpdBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.LastUpdDate)
            p.SourceColumn = ColumnNames.LastUpdDate
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End Namespace

