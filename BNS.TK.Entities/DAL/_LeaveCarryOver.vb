
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _LeaveCarryOver
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "LeaveCarryOver"
			Me.MappingName = "LeaveCarryOver"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveCarryOverLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal ID As Integer) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_LeaveCarryOver.Parameters.ID, ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_LeaveCarryOverLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property ID As SqlParameter
			Get
				Return New SqlParameter("@ID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CompanyID As SqlParameter
			Get
				Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property EmployeeID As SqlParameter
			Get
				Return New SqlParameter("@EmployeeID", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property DateFiled As SqlParameter
			Get
				Return New SqlParameter("@DateFiled", SqlDbType.DateTime, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Year As SqlParameter
			Get
				Return New SqlParameter("@Year", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property CarryOver As SqlParameter
			Get
				Return New SqlParameter("@CarryOver", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Days As SqlParameter
			Get
				Return New SqlParameter("@Days", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Approver As SqlParameter
			Get
				Return New SqlParameter("@Approver", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property Code As SqlParameter
			Get
				Return New SqlParameter("@Code", SqlDbType.Char, 10)
			End Get
		End Property
		
		Public Shared ReadOnly Property Reason As SqlParameter
			Get
				Return New SqlParameter("@Reason", SqlDbType.VarChar, 100)
			End Get
		End Property
		
		Public Shared ReadOnly Property Status As SqlParameter
			Get
				Return New SqlParameter("@Status", SqlDbType.VarChar, 12)
			End Get
		End Property
		
		Public Shared ReadOnly Property Stage As SqlParameter
			Get
				Return New SqlParameter("@Stage", SqlDbType.SmallInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const DateFiled As String = "DateFiled"
        Public Const Year As String = "Year"
        Public Const CarryOver As String = "CarryOver"
        Public Const Days As String = "Days"
        Public Const Approver As String = "Approver"
        Public Const Code As String = "Code"
        Public Const Reason As String = "Reason"
        Public Const Status As String = "Status"
        Public Const Stage As String = "Stage"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _LeaveCarryOver.PropertyNames.ID
				ht(CompanyID) = _LeaveCarryOver.PropertyNames.CompanyID
				ht(EmployeeID) = _LeaveCarryOver.PropertyNames.EmployeeID
				ht(DateFiled) = _LeaveCarryOver.PropertyNames.DateFiled
				ht(Year) = _LeaveCarryOver.PropertyNames.Year
				ht(CarryOver) = _LeaveCarryOver.PropertyNames.CarryOver
				ht(Days) = _LeaveCarryOver.PropertyNames.Days
				ht(Approver) = _LeaveCarryOver.PropertyNames.Approver
				ht(Code) = _LeaveCarryOver.PropertyNames.Code
				ht(Reason) = _LeaveCarryOver.PropertyNames.Reason
				ht(Status) = _LeaveCarryOver.PropertyNames.Status
				ht(Stage) = _LeaveCarryOver.PropertyNames.Stage
				ht(LastUpdBy) = _LeaveCarryOver.PropertyNames.LastUpdBy
				ht(LastUpdDate) = _LeaveCarryOver.PropertyNames.LastUpdDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const ID As String = "ID"
        Public Const CompanyID As String = "CompanyID"
        Public Const EmployeeID As String = "EmployeeID"
        Public Const DateFiled As String = "DateFiled"
        Public Const Year As String = "Year"
        Public Const CarryOver As String = "CarryOver"
        Public Const Days As String = "Days"
        Public Const Approver As String = "Approver"
        Public Const Code As String = "Code"
        Public Const Reason As String = "Reason"
        Public Const Status As String = "Status"
        Public Const Stage As String = "Stage"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(ID) = _LeaveCarryOver.ColumnNames.ID
				ht(CompanyID) = _LeaveCarryOver.ColumnNames.CompanyID
				ht(EmployeeID) = _LeaveCarryOver.ColumnNames.EmployeeID
				ht(DateFiled) = _LeaveCarryOver.ColumnNames.DateFiled
				ht(Year) = _LeaveCarryOver.ColumnNames.Year
				ht(CarryOver) = _LeaveCarryOver.ColumnNames.CarryOver
				ht(Days) = _LeaveCarryOver.ColumnNames.Days
				ht(Approver) = _LeaveCarryOver.ColumnNames.Approver
				ht(Code) = _LeaveCarryOver.ColumnNames.Code
				ht(Reason) = _LeaveCarryOver.ColumnNames.Reason
				ht(Status) = _LeaveCarryOver.ColumnNames.Status
				ht(Stage) = _LeaveCarryOver.ColumnNames.Stage
				ht(LastUpdBy) = _LeaveCarryOver.ColumnNames.LastUpdBy
				ht(LastUpdDate) = _LeaveCarryOver.ColumnNames.LastUpdDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const ID As String = "s_ID"
        Public Const CompanyID As String = "s_CompanyID"
        Public Const EmployeeID As String = "s_EmployeeID"
        Public Const DateFiled As String = "s_DateFiled"
        Public Const Year As String = "s_Year"
        Public Const CarryOver As String = "s_CarryOver"
        Public Const Days As String = "s_Days"
        Public Const Approver As String = "s_Approver"
        Public Const Code As String = "s_Code"
        Public Const Reason As String = "s_Reason"
        Public Const Status As String = "s_Status"
        Public Const Stage As String = "s_Stage"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property ID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ID, Value)
			End Set
		End Property

		Public Overridable Property CompanyID As String
			Get
				Return MyBase.GetString(ColumnNames.CompanyID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.CompanyID, Value)
			End Set
		End Property

		Public Overridable Property EmployeeID As String
			Get
				Return MyBase.GetString(ColumnNames.EmployeeID)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.EmployeeID, Value)
			End Set
		End Property

		Public Overridable Property DateFiled As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.DateFiled)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.DateFiled, Value)
			End Set
		End Property

		Public Overridable Property Year As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.Year)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.Year, Value)
			End Set
		End Property

		Public Overridable Property CarryOver As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.CarryOver)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.CarryOver, Value)
			End Set
		End Property

		Public Overridable Property Days As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.Days)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.Days, Value)
			End Set
		End Property

		Public Overridable Property Approver As String
			Get
				Return MyBase.GetString(ColumnNames.Approver)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Approver, Value)
			End Set
		End Property

		Public Overridable Property Code As String
			Get
				Return MyBase.GetString(ColumnNames.Code)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Code, Value)
			End Set
		End Property

		Public Overridable Property Reason As String
			Get
				Return MyBase.GetString(ColumnNames.Reason)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Reason, Value)
			End Set
		End Property

		Public Overridable Property Status As String
			Get
				Return MyBase.GetString(ColumnNames.Status)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Status, Value)
			End Set
		End Property

		Public Overridable Property Stage As Short
			Get
				Return MyBase.GetShort(ColumnNames.Stage)
			End Get
			Set(ByVal Value As Short)
				MyBase.SetShort(ColumnNames.Stage, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.ID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ID)
				Else
					Me.ID = MyBase.SetIntegerAsString(ColumnNames.ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CompanyID As String
			Get
				If Me.IsColumnNull(ColumnNames.CompanyID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.CompanyID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CompanyID)
				Else
					Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_EmployeeID As String
			Get
				If Me.IsColumnNull(ColumnNames.EmployeeID) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.EmployeeID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.EmployeeID)
				Else
					Me.EmployeeID = MyBase.SetStringAsString(ColumnNames.EmployeeID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_DateFiled As String
			Get
				If Me.IsColumnNull(ColumnNames.DateFiled) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.DateFiled)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.DateFiled)
				Else
					Me.DateFiled = MyBase.SetDateTimeAsString(ColumnNames.DateFiled, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Year As String
			Get
				If Me.IsColumnNull(ColumnNames.Year) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.Year)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Year)
				Else
					Me.Year = MyBase.SetIntegerAsString(ColumnNames.Year, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_CarryOver As String
			Get
				If Me.IsColumnNull(ColumnNames.CarryOver) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.CarryOver)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.CarryOver)
				Else
					Me.CarryOver = MyBase.SetDecimalAsString(ColumnNames.CarryOver, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Days As String
			Get
				If Me.IsColumnNull(ColumnNames.Days) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.Days)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Days)
				Else
					Me.Days = MyBase.SetDecimalAsString(ColumnNames.Days, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Approver As String
			Get
				If Me.IsColumnNull(ColumnNames.Approver) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Approver)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Approver)
				Else
					Me.Approver = MyBase.SetStringAsString(ColumnNames.Approver, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Code As String
			Get
				If Me.IsColumnNull(ColumnNames.Code) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Code)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Code)
				Else
					Me.Code = MyBase.SetStringAsString(ColumnNames.Code, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Reason As String
			Get
				If Me.IsColumnNull(ColumnNames.Reason) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Reason)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Reason)
				Else
					Me.Reason = MyBase.SetStringAsString(ColumnNames.Reason, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Status As String
			Get
				If Me.IsColumnNull(ColumnNames.Status) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Status)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Status)
				Else
					Me.Status = MyBase.SetStringAsString(ColumnNames.Status, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Stage As String
			Get
				If Me.IsColumnNull(ColumnNames.Stage) Then
					Return String.Empty
				Else
					Return MyBase.GetShortAsString(ColumnNames.Stage)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Stage)
				Else
					Me.Stage = MyBase.SetShortAsString(ColumnNames.Stage, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ID, Parameters.ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CompanyID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property EmployeeID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property DateFiled() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.DateFiled, Parameters.DateFiled)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Year() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Year, Parameters.Year)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property CarryOver() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.CarryOver, Parameters.CarryOver)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Days() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Days, Parameters.Days)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Approver() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Approver, Parameters.Approver)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Code() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Code, Parameters.Code)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Reason() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Reason, Parameters.Reason)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Status() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Status, Parameters.Status)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Stage() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Stage, Parameters.Stage)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As WhereParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As WhereParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As WhereParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property DateFiled() As WhereParameter 
			Get
				If _DateFiled_W Is Nothing Then
					_DateFiled_W = TearOff.DateFiled
				End If
				Return _DateFiled_W
			End Get
		End Property

		Public ReadOnly Property Year() As WhereParameter 
			Get
				If _Year_W Is Nothing Then
					_Year_W = TearOff.Year
				End If
				Return _Year_W
			End Get
		End Property

		Public ReadOnly Property CarryOver() As WhereParameter 
			Get
				If _CarryOver_W Is Nothing Then
					_CarryOver_W = TearOff.CarryOver
				End If
				Return _CarryOver_W
			End Get
		End Property

		Public ReadOnly Property Days() As WhereParameter 
			Get
				If _Days_W Is Nothing Then
					_Days_W = TearOff.Days
				End If
				Return _Days_W
			End Get
		End Property

		Public ReadOnly Property Approver() As WhereParameter 
			Get
				If _Approver_W Is Nothing Then
					_Approver_W = TearOff.Approver
				End If
				Return _Approver_W
			End Get
		End Property

		Public ReadOnly Property Code() As WhereParameter 
			Get
				If _Code_W Is Nothing Then
					_Code_W = TearOff.Code
				End If
				Return _Code_W
			End Get
		End Property

		Public ReadOnly Property Reason() As WhereParameter 
			Get
				If _Reason_W Is Nothing Then
					_Reason_W = TearOff.Reason
				End If
				Return _Reason_W
			End Get
		End Property

		Public ReadOnly Property Status() As WhereParameter 
			Get
				If _Status_W Is Nothing Then
					_Status_W = TearOff.Status
				End If
				Return _Status_W
			End Get
		End Property

		Public ReadOnly Property Stage() As WhereParameter 
			Get
				If _Stage_W Is Nothing Then
					_Stage_W = TearOff.Stage
				End If
				Return _Stage_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _ID_W As WhereParameter = Nothing
		Private _CompanyID_W As WhereParameter = Nothing
		Private _EmployeeID_W As WhereParameter = Nothing
		Private _DateFiled_W As WhereParameter = Nothing
		Private _Year_W As WhereParameter = Nothing
		Private _CarryOver_W As WhereParameter = Nothing
		Private _Days_W As WhereParameter = Nothing
		Private _Approver_W As WhereParameter = Nothing
		Private _Code_W As WhereParameter = Nothing
		Private _Reason_W As WhereParameter = Nothing
		Private _Status_W As WhereParameter = Nothing
		Private _Stage_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_ID_W = Nothing
			_CompanyID_W = Nothing
			_EmployeeID_W = Nothing
			_DateFiled_W = Nothing
			_Year_W = Nothing
			_CarryOver_W = Nothing
			_Days_W = Nothing
			_Approver_W = Nothing
			_Code_W = Nothing
			_Reason_W = Nothing
			_Status_W = Nothing
			_Stage_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpdDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ID, Parameters.ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.EmployeeID, Parameters.EmployeeID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property DateFiled() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.DateFiled, Parameters.DateFiled)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Year() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Year, Parameters.Year)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property CarryOver() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CarryOver, Parameters.CarryOver)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Days() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Days, Parameters.Days)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Approver() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Approver, Parameters.Approver)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Code() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Code, Parameters.Code)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Reason() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Reason, Parameters.Reason)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Status() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Status, Parameters.Status)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Stage() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Stage, Parameters.Stage)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property ID() As AggregateParameter 
			Get
				If _ID_W Is Nothing Then
					_ID_W = TearOff.ID
				End If
				Return _ID_W
			End Get
		End Property

		Public ReadOnly Property CompanyID() As AggregateParameter 
			Get
				If _CompanyID_W Is Nothing Then
					_CompanyID_W = TearOff.CompanyID
				End If
				Return _CompanyID_W
			End Get
		End Property

		Public ReadOnly Property EmployeeID() As AggregateParameter 
			Get
				If _EmployeeID_W Is Nothing Then
					_EmployeeID_W = TearOff.EmployeeID
				End If
				Return _EmployeeID_W
			End Get
		End Property

		Public ReadOnly Property DateFiled() As AggregateParameter 
			Get
				If _DateFiled_W Is Nothing Then
					_DateFiled_W = TearOff.DateFiled
				End If
				Return _DateFiled_W
			End Get
		End Property

		Public ReadOnly Property Year() As AggregateParameter 
			Get
				If _Year_W Is Nothing Then
					_Year_W = TearOff.Year
				End If
				Return _Year_W
			End Get
		End Property

		Public ReadOnly Property CarryOver() As AggregateParameter 
			Get
				If _CarryOver_W Is Nothing Then
					_CarryOver_W = TearOff.CarryOver
				End If
				Return _CarryOver_W
			End Get
		End Property

		Public ReadOnly Property Days() As AggregateParameter 
			Get
				If _Days_W Is Nothing Then
					_Days_W = TearOff.Days
				End If
				Return _Days_W
			End Get
		End Property

		Public ReadOnly Property Approver() As AggregateParameter 
			Get
				If _Approver_W Is Nothing Then
					_Approver_W = TearOff.Approver
				End If
				Return _Approver_W
			End Get
		End Property

		Public ReadOnly Property Code() As AggregateParameter 
			Get
				If _Code_W Is Nothing Then
					_Code_W = TearOff.Code
				End If
				Return _Code_W
			End Get
		End Property

		Public ReadOnly Property Reason() As AggregateParameter 
			Get
				If _Reason_W Is Nothing Then
					_Reason_W = TearOff.Reason
				End If
				Return _Reason_W
			End Get
		End Property

		Public ReadOnly Property Status() As AggregateParameter 
			Get
				If _Status_W Is Nothing Then
					_Status_W = TearOff.Status
				End If
				Return _Status_W
			End Get
		End Property

		Public ReadOnly Property Stage() As AggregateParameter 
			Get
				If _Stage_W Is Nothing Then
					_Stage_W = TearOff.Stage
				End If
				Return _Stage_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _ID_W As AggregateParameter = Nothing
		Private _CompanyID_W As AggregateParameter = Nothing
		Private _EmployeeID_W As AggregateParameter = Nothing
		Private _DateFiled_W As AggregateParameter = Nothing
		Private _Year_W As AggregateParameter = Nothing
		Private _CarryOver_W As AggregateParameter = Nothing
		Private _Days_W As AggregateParameter = Nothing
		Private _Approver_W As AggregateParameter = Nothing
		Private _Code_W As AggregateParameter = Nothing
		Private _Reason_W As AggregateParameter = Nothing
		Private _Status_W As AggregateParameter = Nothing
		Private _Stage_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_ID_W = Nothing
		_CompanyID_W = Nothing
		_EmployeeID_W = Nothing
		_DateFiled_W = Nothing
		_Year_W = Nothing
		_CarryOver_W = Nothing
		_Days_W = Nothing
		_Approver_W = Nothing
		_Code_W = Nothing
		_Reason_W = Nothing
		_Status_W = Nothing
		_Stage_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpdDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveCarryOverInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveCarryOverUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_LeaveCarryOverDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.ID)
		p.SourceColumn = ColumnNames.ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CompanyID)
		p.SourceColumn = ColumnNames.CompanyID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.EmployeeID)
		p.SourceColumn = ColumnNames.EmployeeID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.DateFiled)
		p.SourceColumn = ColumnNames.DateFiled
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Year)
		p.SourceColumn = ColumnNames.Year
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.CarryOver)
		p.SourceColumn = ColumnNames.CarryOver
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Days)
		p.SourceColumn = ColumnNames.Days
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Approver)
		p.SourceColumn = ColumnNames.Approver
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Code)
		p.SourceColumn = ColumnNames.Code
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Reason)
		p.SourceColumn = ColumnNames.Reason
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Status)
		p.SourceColumn = ColumnNames.Status
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Stage)
		p.SourceColumn = ColumnNames.Stage
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

