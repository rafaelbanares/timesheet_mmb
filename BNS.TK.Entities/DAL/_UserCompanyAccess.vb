
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized
Imports MMB.DataObject 

NameSpace BNS.TK.Entities

    Public MustInherit Class _UserCompanyAccess
        Inherits SqlClientEntity

        Public Sub New()
            Me.QuerySource = "UserCompanyAccess"
            Me.MappingName = "UserCompanyAccess"
        End Sub

        '=================================================================
        '  Public Overrides Sub AddNew()
        '=================================================================
        '
        '=================================================================
        Public Overrides Sub AddNew()
            MyBase.AddNew()

        End Sub

        Public Overrides Sub FlushData()
            Me._whereClause = Nothing
            Me._aggregateClause = Nothing
            MyBase.FlushData()
        End Sub


        '=================================================================
        '  	Public Function LoadAll() As Boolean
        '=================================================================
        '  Loads all of the records in the database, and sets the currentRow to the first row
        '=================================================================
        Public Function LoadAll() As Boolean

            Dim parameters As ListDictionary = Nothing


            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_UserCompanyAccessLoadAll]", parameters)

        End Function

        '=================================================================
        ' Public Overridable Function LoadByPrimaryKey()  As Boolean
        '=================================================================
        '  Loads a single row of via the primary key
        '=================================================================
        Public Overridable Function LoadByPrimaryKey(ByVal UserID As String, ByVal CompanyID As String) As Boolean

            Dim parameters As ListDictionary = New ListDictionary()
            parameters.Add(_UserCompanyAccess.Parameters.UserID, UserID)
            parameters.Add(_UserCompanyAccess.Parameters.CompanyID, CompanyID)

            Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_UserCompanyAccessLoadByPrimaryKey]", parameters)

        End Function

#Region "Parameters"
        Protected Class Parameters

            Public Shared ReadOnly Property UserID As SqlParameter
                Get
                    Return New SqlParameter("@UserID", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CompanyID As SqlParameter
                Get
                    Return New SqlParameter("@CompanyID", SqlDbType.Char, 10)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedBy As SqlParameter
                Get
                    Return New SqlParameter("@CreatedBy", SqlDbType.Char, 15)
                End Get
            End Property

            Public Shared ReadOnly Property CreatedDate As SqlParameter
                Get
                    Return New SqlParameter("@CreatedDate", SqlDbType.DateTime, 0)
                End Get
            End Property

        End Class
#End Region

#Region "ColumnNames"
        Public Class ColumnNames

            Public Const UserID As String = "UserID"
            Public Const CompanyID As String = "CompanyID"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"

            Public Shared Function ToPropertyName(ByVal columnName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(UserID) = _UserCompanyAccess.PropertyNames.UserID
                    ht(CompanyID) = _UserCompanyAccess.PropertyNames.CompanyID
                    ht(CreatedBy) = _UserCompanyAccess.PropertyNames.CreatedBy
                    ht(CreatedDate) = _UserCompanyAccess.PropertyNames.CreatedDate

                End If

                Return CType(ht(columnName), String)

            End Function

            Private Shared ht As Hashtable = Nothing
        End Class
#End Region

#Region "PropertyNames"
        Public Class PropertyNames

            Public Const UserID As String = "UserID"
            Public Const CompanyID As String = "CompanyID"
            Public Const CreatedBy As String = "CreatedBy"
            Public Const CreatedDate As String = "CreatedDate"

            Public Shared Function ToColumnName(ByVal propertyName As String) As String

                If ht Is Nothing Then

                    ht = New Hashtable

                    ht(UserID) = _UserCompanyAccess.ColumnNames.UserID
                    ht(CompanyID) = _UserCompanyAccess.ColumnNames.CompanyID
                    ht(CreatedBy) = _UserCompanyAccess.ColumnNames.CreatedBy
                    ht(CreatedDate) = _UserCompanyAccess.ColumnNames.CreatedDate

                End If

                Return CType(ht(propertyName), String)

            End Function

            Private Shared ht As Hashtable = Nothing

        End Class
#End Region

#Region "StringPropertyNames"
        Public Class StringPropertyNames

            Public Const UserID As String = "s_UserID"
            Public Const CompanyID As String = "s_CompanyID"
            Public Const CreatedBy As String = "s_CreatedBy"
            Public Const CreatedDate As String = "s_CreatedDate"

        End Class
#End Region

#Region "Properties"
        Public Overridable Property UserID As String
            Get
                Return MyBase.GetString(ColumnNames.UserID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.UserID, Value)
            End Set
        End Property

        Public Overridable Property CompanyID As String
            Get
                Return MyBase.GetString(ColumnNames.CompanyID)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CompanyID, Value)
            End Set
        End Property

        Public Overridable Property CreatedBy As String
            Get
                Return MyBase.GetString(ColumnNames.CreatedBy)
            End Get
            Set(ByVal Value As String)
                MyBase.SetString(ColumnNames.CreatedBy, Value)
            End Set
        End Property

        Public Overridable Property CreatedDate As DateTime
            Get
                Return MyBase.GetDateTime(ColumnNames.CreatedDate)
            End Get
            Set(ByVal Value As DateTime)
                MyBase.SetDateTime(ColumnNames.CreatedDate, Value)
            End Set
        End Property


#End Region

#Region "String Properties"

        Public Overridable Property s_UserID As String
            Get
                If Me.IsColumnNull(ColumnNames.UserID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.UserID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.UserID)
                Else
                    Me.UserID = MyBase.SetStringAsString(ColumnNames.UserID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CompanyID As String
            Get
                If Me.IsColumnNull(ColumnNames.CompanyID) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CompanyID)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CompanyID)
                Else
                    Me.CompanyID = MyBase.SetStringAsString(ColumnNames.CompanyID, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedBy As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedBy) Then
                    Return String.Empty
                Else
                    Return MyBase.GetStringAsString(ColumnNames.CreatedBy)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedBy)
                Else
                    Me.CreatedBy = MyBase.SetStringAsString(ColumnNames.CreatedBy, Value)
                End If
            End Set
        End Property

        Public Overridable Property s_CreatedDate As String
            Get
                If Me.IsColumnNull(ColumnNames.CreatedDate) Then
                    Return String.Empty
                Else
                    Return MyBase.GetDateTimeAsString(ColumnNames.CreatedDate)
                End If
            End Get
            Set(ByVal Value As String)
                If String.Empty = Value Then
                    Me.SetColumnNull(ColumnNames.CreatedDate)
                Else
                    Me.CreatedDate = MyBase.SetDateTimeAsString(ColumnNames.CreatedDate, Value)
                End If
            End Set
        End Property


#End Region

#Region "Where Clause"
        Public Class WhereClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffWhereParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffWhereParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "TearOff's"
            Public Class TearOffWhereParameter

                Public Sub New(ByVal clause As WhereClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property UserID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.UserID, Parameters.UserID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As WhereParameter
                    Get
                        Dim where As WhereParameter = New WhereParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddWhereParemeter(where)
                        Return where
                    End Get
                End Property


                Private _clause As WhereClause
            End Class
#End Region

            Public ReadOnly Property UserID() As WhereParameter
                Get
                    If _UserID_W Is Nothing Then
                        _UserID_W = TearOff.UserID
                    End If
                    Return _UserID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As WhereParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As WhereParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As WhereParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Private _UserID_W As WhereParameter = Nothing
            Private _CompanyID_W As WhereParameter = Nothing
            Private _CreatedBy_W As WhereParameter = Nothing
            Private _CreatedDate_W As WhereParameter = Nothing

            Public Sub WhereClauseReset()

                _UserID_W = Nothing
                _CompanyID_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                Me._entity.Query.FlushWhereParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffWhereParameter
        End Class

        Public ReadOnly Property Where() As WhereClause
            Get
                If _whereClause Is Nothing Then
                    _whereClause = New WhereClause(Me)
                End If

                Return _whereClause
            End Get
        End Property

        Private _whereClause As WhereClause = Nothing
#End Region

#Region "Aggregate Clause"
        Public Class AggregateClause

            Public Sub New(ByVal entity As BusinessEntity)
                Me._entity = entity
            End Sub

            Public ReadOnly Property TearOff As TearOffAggregateParameter
                Get
                    If _tearOff Is Nothing Then
                        _tearOff = New TearOffAggregateParameter(Me)
                    End If

                    Return _tearOff
                End Get
            End Property

#Region "AggregateParameter TearOff's"
            Public Class TearOffAggregateParameter

                Public Sub New(ByVal clause As AggregateClause)
                    Me._clause = clause
                End Sub


                Public ReadOnly Property UserID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.UserID, Parameters.UserID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CompanyID() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CompanyID, Parameters.CompanyID)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedBy() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedBy, Parameters.CreatedBy)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property

                Public ReadOnly Property CreatedDate() As AggregateParameter
                    Get
                        Dim where As AggregateParameter = New AggregateParameter(ColumnNames.CreatedDate, Parameters.CreatedDate)
                        Me._clause._entity.Query.AddAggregateParameter(where)
                        Return where
                    End Get
                End Property


                Private _clause As AggregateClause
            End Class
#End Region

            Public ReadOnly Property UserID() As AggregateParameter
                Get
                    If _UserID_W Is Nothing Then
                        _UserID_W = TearOff.UserID
                    End If
                    Return _UserID_W
                End Get
            End Property

            Public ReadOnly Property CompanyID() As AggregateParameter
                Get
                    If _CompanyID_W Is Nothing Then
                        _CompanyID_W = TearOff.CompanyID
                    End If
                    Return _CompanyID_W
                End Get
            End Property

            Public ReadOnly Property CreatedBy() As AggregateParameter
                Get
                    If _CreatedBy_W Is Nothing Then
                        _CreatedBy_W = TearOff.CreatedBy
                    End If
                    Return _CreatedBy_W
                End Get
            End Property

            Public ReadOnly Property CreatedDate() As AggregateParameter
                Get
                    If _CreatedDate_W Is Nothing Then
                        _CreatedDate_W = TearOff.CreatedDate
                    End If
                    Return _CreatedDate_W
                End Get
            End Property

            Private _UserID_W As AggregateParameter = Nothing
            Private _CompanyID_W As AggregateParameter = Nothing
            Private _CreatedBy_W As AggregateParameter = Nothing
            Private _CreatedDate_W As AggregateParameter = Nothing

            Public Sub AggregateClauseReset()

                _UserID_W = Nothing
                _CompanyID_W = Nothing
                _CreatedBy_W = Nothing
                _CreatedDate_W = Nothing
                Me._entity.Query.FlushAggregateParameters()

            End Sub

            Private _entity As BusinessEntity
            Private _tearOff As TearOffAggregateParameter
        End Class

        Public ReadOnly Property Aggregate() As AggregateClause
            Get
                If _aggregateClause Is Nothing Then
                    _aggregateClause = New AggregateClause(Me)
                End If

                Return _aggregateClause
            End Get
        End Property

        Private _aggregateClause As AggregateClause = Nothing
#End Region

        Protected Overrides Function GetInsertCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_UserCompanyAccessInsert]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetUpdateCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_UserCompanyAccessUpdate]"

            CreateParameters(cmd)

            Return cmd

        End Function

        Protected Overrides Function GetDeleteCommand() As IDbCommand

            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_UserCompanyAccessDelete]"

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.UserID)
            p.SourceColumn = ColumnNames.UserID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current


            Return cmd

        End Function

        Private Sub CreateParameters(ByVal cmd As SqlCommand)

            Dim p As SqlParameter
            p = cmd.Parameters.Add(Parameters.UserID)
            p.SourceColumn = ColumnNames.UserID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CompanyID)
            p.SourceColumn = ColumnNames.CompanyID
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedBy)
            p.SourceColumn = ColumnNames.CreatedBy
            p.SourceVersion = DataRowVersion.Current

            p = cmd.Parameters.Add(Parameters.CreatedDate)
            p.SourceColumn = ColumnNames.CreatedDate
            p.SourceVersion = DataRowVersion.Current


        End Sub

        Public Sub FromDataSet(ByVal ds As DataSet)
            Me.DataTable = ds.Tables(0)
        End Sub

        Public Function ToDataSet() As DataSet
            Dim dataSet As DataSet = New DataSet
            If Me.DataTable.DataSet Is Nothing Then
                dataSet.Tables.Add(Me.DataTable)
            Else
                dataSet.Tables.Add(Me.DataTable.Copy)
            End If
            Return dataSet
        End Function

        Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
            Me.LoadFromRawSql(sql, parameters)
        End Sub

    End Class

End NameSpace

