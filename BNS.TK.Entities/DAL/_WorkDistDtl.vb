
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Imports MMB.DataObject

NameSpace BNS.TK.Entities

Public MustInherit Class _WorkDistDtl
	Inherits SqlClientEntity

		Public Sub New() 
			Me.QuerySource = "WorkDistDtl"
			Me.MappingName = "WorkDistDtl"
		End Sub

	'=================================================================
	'  Public Overrides Sub AddNew()
	'=================================================================
	'
	'=================================================================
	Public Overrides Sub AddNew()
		MyBase.AddNew()
				
	End Sub
	
	Public Overrides Sub FlushData()
		Me._whereClause = nothing
		Me._aggregateClause = nothing		
		MyBase.FlushData()
	End Sub
	
		
	'=================================================================
	'  	Public Function LoadAll() As Boolean
	'=================================================================
	'  Loads all of the records in the database, and sets the currentRow to the first row
	'=================================================================
	Public Function LoadAll() As Boolean
	
		Dim parameters As ListDictionary = Nothing
		
		
    	Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkDistDtlLoadAll]", parameters)
		
	End Function

	'=================================================================
	' Public Overridable Function LoadByPrimaryKey()  As Boolean
	'=================================================================
	'  Loads a single row of via the primary key
	'=================================================================
	Public Overridable Function LoadByPrimaryKey(ByVal Row_ID As Long) As Boolean

		Dim parameters As ListDictionary = New ListDictionary()
		parameters.Add(_WorkDistDtl.Parameters.Row_ID, Row_ID)
		
		Return MyBase.LoadFromSql("[" + Me.SchemaStoredProcedure + "proc_WorkDistDtlLoadByPrimaryKey]", parameters)

	End Function

	#Region "Parameters"
	Protected class Parameters 
		
		Public Shared ReadOnly Property Row_ID As SqlParameter
			Get
				Return New SqlParameter("@Row_ID", SqlDbType.BigInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkDistID As SqlParameter
			Get
				Return New SqlParameter("@WorkDistID", SqlDbType.BigInt, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkForID As SqlParameter
			Get
				Return New SqlParameter("@WorkForID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property WorkForSubID As SqlParameter
			Get
				Return New SqlParameter("@WorkForSubID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property ProjID As SqlParameter
			Get
				Return New SqlParameter("@ProjID", SqlDbType.Int, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property Task As SqlParameter
			Get
				Return New SqlParameter("@Task", SqlDbType.VarChar, 500)
			End Get
		End Property
		
		Public Shared ReadOnly Property RegHrs As SqlParameter
			Get
				Return New SqlParameter("@RegHrs", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property OT As SqlParameter
			Get
				Return New SqlParameter("@OT", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property RstOT As SqlParameter
			Get
				Return New SqlParameter("@RstOT", SqlDbType.Decimal, 0)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdBy As SqlParameter
			Get
				Return New SqlParameter("@LastUpdBy", SqlDbType.Char, 15)
			End Get
		End Property
		
		Public Shared ReadOnly Property LastUpdDate As SqlParameter
			Get
				Return New SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime, 0)
			End Get
		End Property
		
	End Class
	#End Region	

	#Region "ColumnNames"
	Public class ColumnNames
		
        Public Const Row_ID As String = "row_ID"
        Public Const WorkDistID As String = "WorkDistID"
        Public Const WorkForID As String = "WorkForID"
        Public Const WorkForSubID As String = "WorkForSubID"
        Public Const ProjID As String = "ProjID"
        Public Const Task As String = "Task"
        Public Const RegHrs As String = "RegHrs"
        Public Const OT As String = "OT"
        Public Const RstOT As String = "RstOT"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToPropertyName(ByVal columnName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(Row_ID) = _WorkDistDtl.PropertyNames.Row_ID
				ht(WorkDistID) = _WorkDistDtl.PropertyNames.WorkDistID
				ht(WorkForID) = _WorkDistDtl.PropertyNames.WorkForID
				ht(WorkForSubID) = _WorkDistDtl.PropertyNames.WorkForSubID
				ht(ProjID) = _WorkDistDtl.PropertyNames.ProjID
				ht(Task) = _WorkDistDtl.PropertyNames.Task
				ht(RegHrs) = _WorkDistDtl.PropertyNames.RegHrs
				ht(OT) = _WorkDistDtl.PropertyNames.OT
				ht(RstOT) = _WorkDistDtl.PropertyNames.RstOT
				ht(LastUpdBy) = _WorkDistDtl.PropertyNames.LastUpdBy
				ht(LastUpdDate) = _WorkDistDtl.PropertyNames.LastUpdDate

			End If
			
			Return CType(ht(columnName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing		 
	End Class
	#End Region	
	
	#Region "PropertyNames"
	Public class PropertyNames
		
        Public Const Row_ID As String = "Row_ID"
        Public Const WorkDistID As String = "WorkDistID"
        Public Const WorkForID As String = "WorkForID"
        Public Const WorkForSubID As String = "WorkForSubID"
        Public Const ProjID As String = "ProjID"
        Public Const Task As String = "Task"
        Public Const RegHrs As String = "RegHrs"
        Public Const OT As String = "OT"
        Public Const RstOT As String = "RstOT"
        Public Const LastUpdBy As String = "LastUpdBy"
        Public Const LastUpdDate As String = "LastUpdDate"

		Shared Public Function ToColumnName(ByVal propertyName As String) As String

			If ht Is Nothing Then
			
				ht = new Hashtable
				
				ht(Row_ID) = _WorkDistDtl.ColumnNames.Row_ID
				ht(WorkDistID) = _WorkDistDtl.ColumnNames.WorkDistID
				ht(WorkForID) = _WorkDistDtl.ColumnNames.WorkForID
				ht(WorkForSubID) = _WorkDistDtl.ColumnNames.WorkForSubID
				ht(ProjID) = _WorkDistDtl.ColumnNames.ProjID
				ht(Task) = _WorkDistDtl.ColumnNames.Task
				ht(RegHrs) = _WorkDistDtl.ColumnNames.RegHrs
				ht(OT) = _WorkDistDtl.ColumnNames.OT
				ht(RstOT) = _WorkDistDtl.ColumnNames.RstOT
				ht(LastUpdBy) = _WorkDistDtl.ColumnNames.LastUpdBy
				ht(LastUpdDate) = _WorkDistDtl.ColumnNames.LastUpdDate

			End If
			
			Return CType(ht(propertyName), String)
			
		End Function
		
		Shared Private ht  As Hashtable = Nothing
		
	End Class
	#End Region	
	
	#Region "StringPropertyNames"
	Public class StringPropertyNames
		
        Public Const Row_ID As String = "s_Row_ID"
        Public Const WorkDistID As String = "s_WorkDistID"
        Public Const WorkForID As String = "s_WorkForID"
        Public Const WorkForSubID As String = "s_WorkForSubID"
        Public Const ProjID As String = "s_ProjID"
        Public Const Task As String = "s_Task"
        Public Const RegHrs As String = "s_RegHrs"
        Public Const OT As String = "s_OT"
        Public Const RstOT As String = "s_RstOT"
        Public Const LastUpdBy As String = "s_LastUpdBy"
        Public Const LastUpdDate As String = "s_LastUpdDate"

	End Class
	#End Region		
	
	#Region "Properties" 
		Public Overridable Property Row_ID As Long
			Get
				Return MyBase.GetLong(ColumnNames.Row_ID)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.Row_ID, Value)
			End Set
		End Property

		Public Overridable Property WorkDistID As Long
			Get
				Return MyBase.GetLong(ColumnNames.WorkDistID)
			End Get
			Set(ByVal Value As Long)
				MyBase.SetLong(ColumnNames.WorkDistID, Value)
			End Set
		End Property

		Public Overridable Property WorkForID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.WorkForID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.WorkForID, Value)
			End Set
		End Property

		Public Overridable Property WorkForSubID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.WorkForSubID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.WorkForSubID, Value)
			End Set
		End Property

		Public Overridable Property ProjID As Integer
			Get
				Return MyBase.GetInteger(ColumnNames.ProjID)
			End Get
			Set(ByVal Value As Integer)
				MyBase.SetInteger(ColumnNames.ProjID, Value)
			End Set
		End Property

		Public Overridable Property Task As String
			Get
				Return MyBase.GetString(ColumnNames.Task)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.Task, Value)
			End Set
		End Property

		Public Overridable Property RegHrs As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.RegHrs)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.RegHrs, Value)
			End Set
		End Property

		Public Overridable Property OT As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.OT)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.OT, Value)
			End Set
		End Property

		Public Overridable Property RstOT As Decimal
			Get
				Return MyBase.GetDecimal(ColumnNames.RstOT)
			End Get
			Set(ByVal Value As Decimal)
				MyBase.SetDecimal(ColumnNames.RstOT, Value)
			End Set
		End Property

		Public Overridable Property LastUpdBy As String
			Get
				Return MyBase.GetString(ColumnNames.LastUpdBy)
			End Get
			Set(ByVal Value As String)
				MyBase.SetString(ColumnNames.LastUpdBy, Value)
			End Set
		End Property

		Public Overridable Property LastUpdDate As DateTime
			Get
				Return MyBase.GetDateTime(ColumnNames.LastUpdDate)
			End Get
			Set(ByVal Value As DateTime)
				MyBase.SetDateTime(ColumnNames.LastUpdDate, Value)
			End Set
		End Property


	#End Region  
	
	#Region "String Properties" 

		Public Overridable Property s_Row_ID As String
			Get
				If Me.IsColumnNull(ColumnNames.Row_ID) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.Row_ID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Row_ID)
				Else
					Me.Row_ID = MyBase.SetLongAsString(ColumnNames.Row_ID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkDistID As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkDistID) Then
					Return String.Empty
				Else
					Return MyBase.GetLongAsString(ColumnNames.WorkDistID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkDistID)
				Else
					Me.WorkDistID = MyBase.SetLongAsString(ColumnNames.WorkDistID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkForID As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkForID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.WorkForID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkForID)
				Else
					Me.WorkForID = MyBase.SetIntegerAsString(ColumnNames.WorkForID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_WorkForSubID As String
			Get
				If Me.IsColumnNull(ColumnNames.WorkForSubID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.WorkForSubID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.WorkForSubID)
				Else
					Me.WorkForSubID = MyBase.SetIntegerAsString(ColumnNames.WorkForSubID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_ProjID As String
			Get
				If Me.IsColumnNull(ColumnNames.ProjID) Then
					Return String.Empty
				Else
					Return MyBase.GetIntegerAsString(ColumnNames.ProjID)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.ProjID)
				Else
					Me.ProjID = MyBase.SetIntegerAsString(ColumnNames.ProjID, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_Task As String
			Get
				If Me.IsColumnNull(ColumnNames.Task) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.Task)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.Task)
				Else
					Me.Task = MyBase.SetStringAsString(ColumnNames.Task, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_RegHrs As String
			Get
				If Me.IsColumnNull(ColumnNames.RegHrs) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.RegHrs)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.RegHrs)
				Else
					Me.RegHrs = MyBase.SetDecimalAsString(ColumnNames.RegHrs, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_OT As String
			Get
				If Me.IsColumnNull(ColumnNames.OT) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.OT)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.OT)
				Else
					Me.OT = MyBase.SetDecimalAsString(ColumnNames.OT, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_RstOT As String
			Get
				If Me.IsColumnNull(ColumnNames.RstOT) Then
					Return String.Empty
				Else
					Return MyBase.GetDecimalAsString(ColumnNames.RstOT)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.RstOT)
				Else
					Me.RstOT = MyBase.SetDecimalAsString(ColumnNames.RstOT, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdBy As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdBy) Then
					Return String.Empty
				Else
					Return MyBase.GetStringAsString(ColumnNames.LastUpdBy)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdBy)
				Else
					Me.LastUpdBy = MyBase.SetStringAsString(ColumnNames.LastUpdBy, Value)
				End If
			End Set
		End Property

		Public Overridable Property s_LastUpdDate As String
			Get
				If Me.IsColumnNull(ColumnNames.LastUpdDate) Then
					Return String.Empty
				Else
					Return MyBase.GetDateTimeAsString(ColumnNames.LastUpdDate)
				End If
			End Get
			Set(ByVal Value As String)
				If String.Empty = value Then
					Me.SetColumnNull(ColumnNames.LastUpdDate)
				Else
					Me.LastUpdDate = MyBase.SetDateTimeAsString(ColumnNames.LastUpdDate, Value)
				End If
			End Set
		End Property


	#End Region  	

	#Region "Where Clause"
    Public Class WhereClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffWhereParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffWhereParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "TearOff's"
		Public class TearOffWhereParameter

			Public Sub New(ByVal clause As WhereClause)
				Me._clause = clause
			End Sub
		
	
			Public ReadOnly Property Row_ID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Row_ID, Parameters.Row_ID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkDistID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkDistID, Parameters.WorkDistID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkForID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkForID, Parameters.WorkForID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property WorkForSubID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.WorkForSubID, Parameters.WorkForSubID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property ProjID() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.ProjID, Parameters.ProjID)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property Task() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.Task, Parameters.Task)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property RegHrs() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.RegHrs, Parameters.RegHrs)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property OT() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.OT, Parameters.OT)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property RstOT() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.RstOT, Parameters.RstOT)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdBy() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property

			Public ReadOnly Property LastUpdDate() As WhereParameter
				Get
					Dim where As WhereParameter = New WhereParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
					Me._clause._entity.Query.AddWhereParemeter(where)
					Return where
				End Get
			End Property


			Private _clause as WhereClause
		End Class
		#End Region	

		Public ReadOnly Property Row_ID() As WhereParameter 
			Get
				If _Row_ID_W Is Nothing Then
					_Row_ID_W = TearOff.Row_ID
				End If
				Return _Row_ID_W
			End Get
		End Property

		Public ReadOnly Property WorkDistID() As WhereParameter 
			Get
				If _WorkDistID_W Is Nothing Then
					_WorkDistID_W = TearOff.WorkDistID
				End If
				Return _WorkDistID_W
			End Get
		End Property

		Public ReadOnly Property WorkForID() As WhereParameter 
			Get
				If _WorkForID_W Is Nothing Then
					_WorkForID_W = TearOff.WorkForID
				End If
				Return _WorkForID_W
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As WhereParameter 
			Get
				If _WorkForSubID_W Is Nothing Then
					_WorkForSubID_W = TearOff.WorkForSubID
				End If
				Return _WorkForSubID_W
			End Get
		End Property

		Public ReadOnly Property ProjID() As WhereParameter 
			Get
				If _ProjID_W Is Nothing Then
					_ProjID_W = TearOff.ProjID
				End If
				Return _ProjID_W
			End Get
		End Property

		Public ReadOnly Property Task() As WhereParameter 
			Get
				If _Task_W Is Nothing Then
					_Task_W = TearOff.Task
				End If
				Return _Task_W
			End Get
		End Property

		Public ReadOnly Property RegHrs() As WhereParameter 
			Get
				If _RegHrs_W Is Nothing Then
					_RegHrs_W = TearOff.RegHrs
				End If
				Return _RegHrs_W
			End Get
		End Property

		Public ReadOnly Property OT() As WhereParameter 
			Get
				If _OT_W Is Nothing Then
					_OT_W = TearOff.OT
				End If
				Return _OT_W
			End Get
		End Property

		Public ReadOnly Property RstOT() As WhereParameter 
			Get
				If _RstOT_W Is Nothing Then
					_RstOT_W = TearOff.RstOT
				End If
				Return _RstOT_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As WhereParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As WhereParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _Row_ID_W As WhereParameter = Nothing
		Private _WorkDistID_W As WhereParameter = Nothing
		Private _WorkForID_W As WhereParameter = Nothing
		Private _WorkForSubID_W As WhereParameter = Nothing
		Private _ProjID_W As WhereParameter = Nothing
		Private _Task_W As WhereParameter = Nothing
		Private _RegHrs_W As WhereParameter = Nothing
		Private _OT_W As WhereParameter = Nothing
		Private _RstOT_W As WhereParameter = Nothing
		Private _LastUpdBy_W As WhereParameter = Nothing
		Private _LastUpdDate_W As WhereParameter = Nothing

			Public Sub WhereClauseReset()

			_Row_ID_W = Nothing
			_WorkDistID_W = Nothing
			_WorkForID_W = Nothing
			_WorkForSubID_W = Nothing
			_ProjID_W = Nothing
			_Task_W = Nothing
			_RegHrs_W = Nothing
			_OT_W = Nothing
			_RstOT_W = Nothing
			_LastUpdBy_W = Nothing
			_LastUpdDate_W = Nothing
				Me._entity.Query.FlushWhereParameters()

			End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffWhereParameter
    End Class	

	Public ReadOnly Property Where() As WhereClause
		Get
			If _whereClause Is Nothing Then
				_whereClause = New WhereClause(Me)
			End If
	
			Return _whereClause
		End Get
	End Property
	
	Private _whereClause As WhereClause = Nothing	
#End Region	

#Region "Aggregate Clause"
    Public Class AggregateClause

        Public Sub New(ByVal entity As BusinessEntity)
            Me._entity = entity
        End Sub
		
		Public ReadOnly Property TearOff As TearOffAggregateParameter
			Get
				If _tearOff Is Nothing Then
					_tearOff = new TearOffAggregateParameter(Me)
				End If

				Return _tearOff
			End Get
		End Property

		#Region "AggregateParameter TearOff's"
		Public class TearOffAggregateParameter

			Public Sub New(ByVal clause As AggregateClause)
				Me._clause = clause
			End Sub
		
	
		Public ReadOnly Property Row_ID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Row_ID, Parameters.Row_ID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkDistID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkDistID, Parameters.WorkDistID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkForID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkForID, Parameters.WorkForID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.WorkForSubID, Parameters.WorkForSubID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property ProjID() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.ProjID, Parameters.ProjID)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property Task() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.Task, Parameters.Task)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property RegHrs() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RegHrs, Parameters.RegHrs)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property OT() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.OT, Parameters.OT)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property RstOT() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.RstOT, Parameters.RstOT)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdBy, Parameters.LastUpdBy)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter
			Get
				Dim where As AggregateParameter = New AggregateParameter(ColumnNames.LastUpdDate, Parameters.LastUpdDate)
				Me._clause._entity.Query.AddAggregateParameter(where)
				Return where
			End Get
		End Property


			Private _clause as AggregateClause
		End Class
		#End Region	

		Public ReadOnly Property Row_ID() As AggregateParameter 
			Get
				If _Row_ID_W Is Nothing Then
					_Row_ID_W = TearOff.Row_ID
				End If
				Return _Row_ID_W
			End Get
		End Property

		Public ReadOnly Property WorkDistID() As AggregateParameter 
			Get
				If _WorkDistID_W Is Nothing Then
					_WorkDistID_W = TearOff.WorkDistID
				End If
				Return _WorkDistID_W
			End Get
		End Property

		Public ReadOnly Property WorkForID() As AggregateParameter 
			Get
				If _WorkForID_W Is Nothing Then
					_WorkForID_W = TearOff.WorkForID
				End If
				Return _WorkForID_W
			End Get
		End Property

		Public ReadOnly Property WorkForSubID() As AggregateParameter 
			Get
				If _WorkForSubID_W Is Nothing Then
					_WorkForSubID_W = TearOff.WorkForSubID
				End If
				Return _WorkForSubID_W
			End Get
		End Property

		Public ReadOnly Property ProjID() As AggregateParameter 
			Get
				If _ProjID_W Is Nothing Then
					_ProjID_W = TearOff.ProjID
				End If
				Return _ProjID_W
			End Get
		End Property

		Public ReadOnly Property Task() As AggregateParameter 
			Get
				If _Task_W Is Nothing Then
					_Task_W = TearOff.Task
				End If
				Return _Task_W
			End Get
		End Property

		Public ReadOnly Property RegHrs() As AggregateParameter 
			Get
				If _RegHrs_W Is Nothing Then
					_RegHrs_W = TearOff.RegHrs
				End If
				Return _RegHrs_W
			End Get
		End Property

		Public ReadOnly Property OT() As AggregateParameter 
			Get
				If _OT_W Is Nothing Then
					_OT_W = TearOff.OT
				End If
				Return _OT_W
			End Get
		End Property

		Public ReadOnly Property RstOT() As AggregateParameter 
			Get
				If _RstOT_W Is Nothing Then
					_RstOT_W = TearOff.RstOT
				End If
				Return _RstOT_W
			End Get
		End Property

		Public ReadOnly Property LastUpdBy() As AggregateParameter 
			Get
				If _LastUpdBy_W Is Nothing Then
					_LastUpdBy_W = TearOff.LastUpdBy
				End If
				Return _LastUpdBy_W
			End Get
		End Property

		Public ReadOnly Property LastUpdDate() As AggregateParameter 
			Get
				If _LastUpdDate_W Is Nothing Then
					_LastUpdDate_W = TearOff.LastUpdDate
				End If
				Return _LastUpdDate_W
			End Get
		End Property

		Private _Row_ID_W As AggregateParameter = Nothing
		Private _WorkDistID_W As AggregateParameter = Nothing
		Private _WorkForID_W As AggregateParameter = Nothing
		Private _WorkForSubID_W As AggregateParameter = Nothing
		Private _ProjID_W As AggregateParameter = Nothing
		Private _Task_W As AggregateParameter = Nothing
		Private _RegHrs_W As AggregateParameter = Nothing
		Private _OT_W As AggregateParameter = Nothing
		Private _RstOT_W As AggregateParameter = Nothing
		Private _LastUpdBy_W As AggregateParameter = Nothing
		Private _LastUpdDate_W As AggregateParameter = Nothing

		Public Sub AggregateClauseReset()

		_Row_ID_W = Nothing
		_WorkDistID_W = Nothing
		_WorkForID_W = Nothing
		_WorkForSubID_W = Nothing
		_ProjID_W = Nothing
		_Task_W = Nothing
		_RegHrs_W = Nothing
		_OT_W = Nothing
		_RstOT_W = Nothing
		_LastUpdBy_W = Nothing
		_LastUpdDate_W = Nothing
			Me._entity.Query.FlushAggregateParameters()

		End Sub
	
		Private _entity As BusinessEntity
		Private _tearOff As TearOffAggregateParameter
    End Class	

	Public ReadOnly Property Aggregate() As AggregateClause
		Get
			If _aggregateClause Is Nothing Then
				_aggregateClause = New AggregateClause(Me)
			End If
	
			Return _aggregateClause
		End Get
	End Property
	
	Private _aggregateClause As AggregateClause = Nothing	
#End Region	

	Protected Overrides Function GetInsertCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkDistDtlInsert]" 
	    
		CreateParameters(cmd)
		
		Dim p As SqlParameter
		p = cmd.Parameters(Parameters.Row_ID.ParameterName)
		p.Direction = ParameterDirection.Output
    
		Return cmd 

  	End Function
	
	Protected Overrides Function GetUpdateCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkDistDtlUpdate]" 
		
		CreateParameters(cmd) 
		    
		Return cmd
	
	End Function	
	
	Protected Overrides Function GetDeleteCommand() As IDbCommand
	
		Dim cmd As SqlCommand = New SqlCommand
		cmd.CommandType = CommandType.StoredProcedure    
		cmd.CommandText = "[" + Me.SchemaStoredProcedure + "proc_WorkDistDtlDelete]" 
		
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.Row_ID)
		p.SourceColumn = ColumnNames.Row_ID
		p.SourceVersion = DataRowVersion.Current

  
		Return cmd
	
	End Function	
	
	Private Sub CreateParameters(ByVal cmd As SqlCommand)
	
		Dim p As SqlParameter
		p = cmd.Parameters.Add(Parameters.Row_ID)
		p.SourceColumn = ColumnNames.Row_ID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkDistID)
		p.SourceColumn = ColumnNames.WorkDistID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkForID)
		p.SourceColumn = ColumnNames.WorkForID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.WorkForSubID)
		p.SourceColumn = ColumnNames.WorkForSubID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.ProjID)
		p.SourceColumn = ColumnNames.ProjID
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.Task)
		p.SourceColumn = ColumnNames.Task
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.RegHrs)
		p.SourceColumn = ColumnNames.RegHrs
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.OT)
		p.SourceColumn = ColumnNames.OT
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.RstOT)
		p.SourceColumn = ColumnNames.RstOT
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdBy)
		p.SourceColumn = ColumnNames.LastUpdBy
		p.SourceVersion = DataRowVersion.Current

		p = cmd.Parameters.Add(Parameters.LastUpdDate)
		p.SourceColumn = ColumnNames.LastUpdDate
		p.SourceVersion = DataRowVersion.Current


	End Sub	
	
	Public Sub FromDataSet(ByVal ds As DataSet)
		Me.DataTable = ds.Tables(0)
	End Sub

	Public Function ToDataSet() As DataSet
		Dim dataSet As DataSet = New DataSet
		If Me.DataTable.DataSet Is Nothing Then
			dataSet.Tables.Add(Me.DataTable)
		Else
			dataSet.Tables.Add(Me.DataTable.Copy)
		End If
		Return dataSet
	End Function

	Public Sub DynamicQuery(ByVal sql As String, ByVal parameters As Object())
		Me.LoadFromRawSql(sql, parameters)
	End Sub

End Class

End NameSpace

