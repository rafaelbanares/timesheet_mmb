
Namespace BNS.TK.Entities

    Public Class LeaveEarn
        Inherits _LeaveEarn



        Public Shared Function ExcusedLeaveApplySession(ByVal sessionid As Guid) As Boolean
            Dim o As New LeaveEarn
            Dim parameters As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary
            parameters.Add(New System.Data.SqlClient.SqlParameter("@sessionid", SqlDbType.UniqueIdentifier, 0), sessionid)

            o.LoadFromSql("usa_ExcusedLeaveApplySession", parameters)
            o = Nothing

            Return True
        End Function

        Public Shared Function LeaveBalance(ByVal companyid As String, ByVal employeeID As String, ByVal attDate As DateTime, ByVal leavecode As String) As Integer
            Dim o As New LeaveEarn
            Dim parameters As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary
            parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", SqlDbType.Char, 10), companyid)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@EmployeeID", SqlDbType.Char, 15), employeeID)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@date", SqlDbType.SmallDateTime), attDate)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@leavecode", SqlDbType.VarChar, 10), leavecode)

            o.LoadFromSql("usa_LeaveBalance", parameters)
            If o.RowCount = 0 Then Return 0

            Return Convert.ToInt32(o.GetColumn("Earned")) - Convert.ToInt32(o.GetColumn("Taken"))
        End Function

    End Class

End Namespace
