﻿

Namespace BNS.TK.Entities
    Public Class Attendance
        Inherits _Attendance



        Public Shared Function CreateAttendanceFromTimeTrans(ByVal companyid As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal employeeID As String) As Boolean
            Dim o As New Attendance
            Dim parameters As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary
            parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", SqlDbType.Char, 10), companyid)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@sdate", SqlDbType.SmallDateTime), startDate)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@edate", SqlDbType.SmallDateTime), endDate)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@employeeid", SqlDbType.Char, 15), employeeID)
            o.LoadFromSql("usa_GenerateAttendanceFromTimeTrans", parameters)
            o = Nothing

            Return True
        End Function

    End Class
End Namespace