 

NameSpace BNS.TK.Entities

    Public Class OffsetEarn
        Inherits _OffsetEarn


        Public Shared Function OffsetBalance(ByVal companyid As String, ByVal employeeID As String, ByVal attDate As DateTime) As Integer
            Dim o As New OffsetEarn
            Dim parameters As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary
            parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", SqlDbType.Char, 10), companyid)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@EmployeeID", SqlDbType.Char, 15), employeeID)
            parameters.Add(New System.Data.SqlClient.SqlParameter("@date", SqlDbType.SmallDateTime), attDate)

            o.LoadFromSql("usa_OffsetBalance", parameters)
            If o.RowCount = 0 Then Return 0

            Return Convert.ToInt32(o.GetColumn("Earned")) - Convert.ToInt32(o.GetColumn("Taken"))
        End Function
    End Class

End NameSpace
