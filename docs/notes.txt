

Max logout for Halfday work in the morning is 2PM
08:30 - 2:00	not HD
09:00 - 2:00	not HD
09:00 - 1:00	HD
10:00 - 2:00	HD
10:01 - 2:01	not HD
10:30 - 2:30	not HD


//determine projected shift
if PossiblyLate
   Within flexi range?
      create projected shift from base shift
   else
      Set projected shift equals max flexi
else
   if (earlyLogin)
      create projected shift from base shift
   else
      Set projected shift equals base shift



half-day work in the afternoon may come between 1:30PM-2:30PM, with a sliding log out time to complete the 4 hours
1:30 - xx:xx		HD
2:00 - xx:xx		HD