using System;
using System.Collections.Generic;
using System.Text;

using System.Configuration;

namespace AttendanceProcess
{
    class PrgoramStart
    {
        static void Main(string[] args)
        {
            /* 
             * Usage:
             * AttendanceProcess.exe                        - Runs attendance update, will process all unprocessed attendance and attendance with errors
             * AttendanceProcess.exe 0                      - Runs attendance insert, will insert attendance from 
             *                                                  BNSAttendance_EW.dbo.RunData to current date -1 (lastinsertdate to yesterday)
             * AttendanceProcess.exe 1                      - Runs attendance insert, will insert attendance from 
             *                                                  BNSAttendance_EW.dbo.RunData to current date    (lastinsertdate to today)
             * AttendanceProcess.exe 01.01.2009 01.15.2009  - Runs attendance insert, will insert attendance from datefrom to dateto
            */
            DateTime startprocess = DateTime.Now;

            MainProcessBulk p = new MainProcessBulk();

            if (args.Length == 0)
                p.ProcessAttendanceBulk();
            else if (args.Length > 0)
                p.AttendanceInsertBulk(args);

            TimeSpan ts = DateTime.Now - startprocess;
            Console.WriteLine(string.Format("Total time executed {0} minute(s) {1} second(s) ", ts.Minutes, ts.Seconds));
            Console.Read();
        }
    }

    class MainProcessBulk
    {
        string conn = ConfigurationManager.AppSettings.Get("Connection");
        string maindb = ConfigurationManager.AppSettings.Get("MasterDB");

        string userID = "system";
        string companyID = "EW01";
        string db = "EWB_TimeKeep";

        public void ProcessAttendanceBulk()
        {
            DateTime startprocess = DateTime.Now;
            Bns.Attendance.DBCredential dbc = new Bns.Attendance.DBCredential(conn, maindb, userID, companyID, db);
            Bns.Attendance.AttendanceProcessBulk process = new Bns.Attendance.AttendanceProcessBulk(dbc);

            process.ExecuteBulk();
        }

        public void AttendanceInsertBulk(string[] args)
        {
            DateTime startprocess = DateTime.Now;

            //... step 1 - first insert rows to attendance
            Bns.Attendance.DBCredential dbc = new Bns.Attendance.DBCredential(conn, maindb, userID, companyID, db);

            Bns.Attendance.AttendanceInsertBulk insertbulk = new Bns.Attendance.AttendanceInsertBulk(dbc);

            insertbulk.Args = args;
            insertbulk.Execute();

            //... step 2 - process inserted rows
            Bns.Attendance.AttendanceProcessBulk process = new Bns.Attendance.AttendanceProcessBulk(dbc);

            process.ExecuteBulk();
        }

    }
}
