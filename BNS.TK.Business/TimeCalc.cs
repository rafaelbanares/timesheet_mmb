﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class TimeCalc
    {
        #region CTOR
        public TimeCalc() { }
        #endregion



        private int ComputeTardy(DateTime payable_in, DateTime shift_in)
        {
            return (payable_in <= shift_in) ? 0 : (int)(payable_in - shift_in).TotalMinutes;
        }

        
        
        /// <summary>
        /// scenario: 11:30AM - 4:30PM  (shift: 8:30-5:30 flexi)
        /// halfday will be 10:00-2:00PM.
        /// returns minutes
        /// </summary>
        /// <param name="payable"></param>
        /// <param name="projected"></param>
        /// <returns></returns>
        public int ComputeTardy(TimeEntry payable, ShiftEntry projected)
        {
            if (payable.NoEntry()) return 0;

            int trd = 0;
            if (!payable.NoEntryInOut1())   //AM entry exists
                trd = ComputeTardy(payable.In1, projected.In1);
           
            if (!payable.NoEntryInOut2())   //PM entry exists
                trd += ComputeTardy(payable.In2, projected.In2);

            return trd;
        }

        //Undertime
        private int ComputeUT(DateTime payable_out, DateTime shift_out)
        {
            return (payable_out >= shift_out) ? 0 : (int)(shift_out - payable_out).TotalMinutes;
        }

        public int ComputeUT(TimeEntry payable, ShiftEntry projected)
        {
            if (payable.NoEntry()) return 0;

            int ut = 0;
            if (!payable.NoEntryInOut2())    //entry exist in PM 
                ut = ComputeUT(payable.Out2, projected.Out2);

            if (!payable.NoEntryInOut1())    //entry exist in AM 
                ut += ComputeUT(payable.Out1, projected.Out1);

            return ut;
        }


        public int ComputeRegMinutesWork(TimeEntry payable)
        {
            int reg_mins = 0;

            if (!payable.NoEntryInOut1())
                reg_mins = (int)(payable.Out1 - payable.In1).TotalMinutes;

            if (!payable.NoEntryInOut2())
                reg_mins += (int)(payable.Out2 - payable.In2).TotalMinutes;

            return reg_mins;
        }


        


        public int ExcessMins(DateTime start, DateTime end)
        {
            int excess = (int)(end - start).TotalMinutes;
            return (excess < 0) ? 0 : excess;
        }

        public int ExcessMins(TimeEntry payable, DateTime raw_end)
        {
            int excess = 0;
            if (payable.NoEntry())
                return 0;
            else if (payable.NoEntryInOut2())
                excess = (int)(raw_end - payable.Out1).TotalMinutes;
            else
                excess = (int)(raw_end - payable.Out2).TotalMinutes;

            return (excess < 0) ? 0 : excess;
        }

        public decimal ComputeAbsent(TimeEntry payable, bool wholedayLeave, bool halfdayLeave, bool holiday, bool halfdayHoliday)
        {
            if (wholedayLeave || holiday) return 0;

            if (payable.NoEntry())
            {
                if (halfdayLeave || halfdayHoliday) 
                    return .5M;
                else
                    return 1;
            }
            if (payable.NoEntryInOut1() || payable.NoEntryInOut2())
            {
                if (halfdayLeave || halfdayHoliday)
                    return 0;
                else
                    return .5M;
            }

            return 0;
        }
    }
}
