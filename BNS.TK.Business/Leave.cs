﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class Leave
    {
         
        public bool halfdayLeave { get; private set; }
        //public bool halfdayAmLeave { get; private set; }
        public bool wholedayLeave { get; private set; }
        public string leaveCode { get; private set; }        
        public decimal days { get; private set; }
        public decimal hours { get; private set; }

        public string Description(string leavecode)
        {
            if (_leavecode.Find(leavecode))
            {
                return _leavecode.Description;  
            }
            else
                return leavecode;
        }

        #region CTOR
        public Leave(string CompanyID, DateTime startDate, DateTime endDate, string employeeID)
        {
            _companyID = CompanyID;

            LoadLeaveTrans(startDate, endDate, employeeID);

            _leavecode = new LeaveCode();
            _leavecode.Where.CompanyID.Value = CompanyID;
            _leavecode.Query.Load("");
            _leavecode.Sort = LeaveCode.ColumnNames.Code;
  
        }
        #endregion

 

        public bool FindLeave(string employeeID, DateTime date)
        {
            wholedayLeave = false;
            halfdayLeave = false;
            //halfdayAmLeave = false;
            leaveCode = "";
            days = 0;
            hours = 0;

            //find leave
            object[] key = { employeeID, date };
            if (!_leaveTransHdr.Find(key)) return false;

            wholedayLeave = (_leaveTransHdr.Days >= 1) ? true : false;
            halfdayLeave = (_leaveTransHdr.Days == Convert.ToDecimal(0.5)) ? true : false;
            //halfdayAmLeave = (halfdayLeave && _leaveTransHdr.HalfdayAm);
            leaveCode = _leaveTransHdr.s_Code;
            days = _leaveTransHdr.Days;
            hours = _leaveTransHdr.Cnv_Hours;

            return true;
        }

      

        #region LoadLeaveTrans
        private void LoadLeaveTrans(DateTime startDate, DateTime endDate, string employeeID)
        {
            _leaveTransHdr = new LeaveTransHdr();
            object[] parameters = { _companyID, startDate, endDate, employeeID };
            string sql = "select " +
                        "	EmployeeID, " +
                        "	a.LeaveDate, " +
                        "	a.[Days], " +
                        "	a.Cnv_Hours, " +
                        "	Code " +
                        "From LeaveTrans a Inner Join LeaveTransHdr b " +
                        "On a.LeaveHdrID = b.ID " +
                        "Where " +
                        "	a.CompanyID = {0} and " +
                        "	b.[Status] = 'Approved' and " +
                        "	(LeaveDate between {1} and {2}) and " +
                        "   (coalesce({3},'') = '' or EmployeeID = {3}) ";
            //"   Posted = 0 and " +

            _leaveTransHdr.DynamicQuery(sql, parameters);
            _leaveTransHdr.Sort = "EmployeeID, LeaveDate";
        }
        #endregion


        //private vars
        string _companyID;
        LeaveTransHdr _leaveTransHdr;
        LeaveCode _leavecode;
    }
}
