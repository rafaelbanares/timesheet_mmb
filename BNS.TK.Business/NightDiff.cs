﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class NightDiff
    {
        #region CTOR
        public NightDiff(string nd1_start, string nd1_end, string nd2_start, string nd2_end)
        {
            _nd1start = nd1_start;
            _nd1end = nd1_end;
            _nd2start = nd2_start;
            _nd2end = nd2_end;
        }
        #endregion


        public DateTime nd1start { get; private set; }
        public DateTime nd1end { get; private set; }
        public DateTime nd2start { get; private set; }
        public DateTime nd2end { get; private set; }


        public SinglePair GetND1range(DateTime date)
        {
            SinglePair nd;
            nd.In = (_nd1start.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(date.ToShortDateString() + " " + _nd1start);
            nd.Out = (_nd1end.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(date.ToShortDateString() + " " + _nd1end);

            if (!IsNull(nd.Out) && nd.Out < nd.In) nd.Out += new TimeSpan(24, 0, 0);
            return nd;
        }
        public SinglePair GetND2range(DateTime date)
        {
            SinglePair nd;
            date = date.AddDays(1);
            nd.In = (_nd2start.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(date.ToShortDateString() + " " + _nd2start);
            nd.Out = (_nd2end.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(date.ToShortDateString() + " " + _nd2end);

            if (!IsNull(nd.Out) && nd.Out < nd.In) nd.Out += new TimeSpan(24, 0, 0);
            return nd;
        }

        string _nd1start;
        string _nd1end;
        string _nd2start;
        string _nd2end;


        private bool IsNull(DateTime inout)
        {
            return (inout == DateTime.MinValue);
        }
    }
}
