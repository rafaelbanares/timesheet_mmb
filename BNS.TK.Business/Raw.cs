﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class Raw
    {

        #region CTOR
        public Raw(){}

        public Raw(DateTime date, DateTime in1, DateTime out1)
        {
            Set(date, in1, out1);
        }

        public Raw(string date, string in1, string out1)
        {
            Set(date, in1, out1);
        }
        #endregion


        public void Set(DateTime date, DateTime xin1, DateTime xout1)
        {
            AttDate = date;
            In = xin1;
            Out = xout1;
        }

        public void Set(string date, string xin1, string xout1)
        {
            In = (xin1.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(date + " " + xin1);
            Out = (xout1.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(date + " " + xout1);
            
            if (!dnull(Out) && Out < In) Out += new TimeSpan(24, 0, 0);
        }

        #region Tools
        public bool dnull(DateTime inout)
        {
            return (inout == DateTime.MinValue);
        }

        public bool ValidEntry()
        {
            return ((dnull(In) == dnull(Out)) && In <= Out);
        }

        public bool NoEntry()
        {
            return (dnull(In) && dnull(Out));
        }

        #endregion

        public DateTime AttDate { get; set; }
        public DateTime In { get; set; }
        public DateTime Out { get; set; }        
    }
}
