﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class OBusiness
    {
      

        #region GetUnfiledOB
        private static DateRange GetUnfiledOB(DateRange ob, Raw raw)
        {
            DateRange gap = new DateRange();

            if (raw.NoEntry()) return gap;
            
            if (ob.Start < raw.In)
            {
                if (ob.End < raw.In)
                {
                    gap.Start = ob.End;
                    gap.End = raw.In;
                }
            }

            if (ob.End > raw.Out)
            {
                if (ob.Start > raw.Out)
                {
                    gap.Start = raw.Out;
                    gap.End = ob.Start;
                }
            }

            return gap;
        }
        #endregion

        

        //raw must be the original raw entries
        public static int ComputeDeductibleUnfiledOB(DateRange ob, Raw raw, TimeEntry payable)
        {
            if (ob.NoEntry()) return 0;
            if (payable.NoEntry()) return 0;

            DateRange gap = GetUnfiledOB(ob, raw);
            if (gap.NoEntry()) return 0;

            int gap1_mins = 0;
            if (!payable.NoEntryInOut1())
            {
                //check if gap is within the payable first half
                if ((gap.Start < payable.Out1) && (gap.End > payable.In1))
                {
                    if (gap.Start < payable.In1)
                    {
                        gap.Start = payable.In1;
                    }

                    if (gap.End > payable.Out1)
                    {
                        gap.End = payable.Out1;
                    }

                    gap1_mins = (int)(gap.End - gap.Start).TotalMinutes;
                }                
            }

            int gap2_mins = 0;
            if (!payable.NoEntryInOut2())
            {
                //check if gap is within the payable second half
                if ((gap.Start < payable.Out2) && (gap.End > payable.In2))
                {
                    if (gap.Start < payable.In2)
                    {
                        gap.Start = payable.In2;
                    }

                    if (gap.End > payable.Out2)
                    {
                        gap.End = payable.Out2;
                    }

                    gap2_mins = (int)(gap.End - gap.Start).TotalMinutes;
                }                
            }

            return gap1_mins + gap2_mins;
        }


        
    }
}
