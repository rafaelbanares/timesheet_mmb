﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class CompensatoryLeaveProcess
    {

        #region CTOR
        public CompensatoryLeaveProcess(string companyid, DateTime startDate, DateTime endDate, string employeeID)
        {
            _companyID = companyid;
            _sessionid = System.Guid.NewGuid();
            LoadRef(startDate, endDate, employeeID);
            _lvdel = new LeaveEarnTmp_del();
            _lvnew = new LeaveEarnTmp_new();
        }
        #endregion

        private const string CL_CODE = "CL";
        private const int ThreeShiftWork = 1380;    // 23hrs (24hrs less break)
        private const int TwoShiftWork = 960;       // 2 shift continuous work

        public void Process(string employeeid, DateTime attDate, int totalWorkMins)
        {
            if (totalWorkMins >= TwoShiftWork && totalWorkMins < ThreeShiftWork)  // 2 shift continuous work
                Create(employeeid, attDate, 1, attDate.AddDays(1), attDate.AddDays(61));
            else if (totalWorkMins >= ThreeShiftWork)  //>= 24hrs including 1hr break)
            {
                Create(employeeid, attDate, 1, attDate.AddDays(1), attDate.AddDays(61));
                Create(employeeid, attDate, 1, attDate.AddDays(2), attDate.AddDays(62));
            }
            else
                Remove(employeeid, attDate);
        }

        public void ProcessOT(string employeeid, DateTime attDate, int totalWorkMins)
        {
            if (totalWorkMins >= 480)  //>=8hours 
                Create(employeeid, attDate, 1, attDate.AddDays(1), attDate.AddDays(61));
            else
                Remove(employeeid, attDate);
        }

        public void Create(string employeeid, DateTime attDate, int days, DateTime validFrom, DateTime validTo)
        {            
            object[] find = { employeeid, attDate };
            if (!_lvref.Find(find))
            {
                _lvnew.AddNew();
                _lvnew.SessionID = _sessionid;
                _lvnew.LeaveEarnID = 0;
                _lvnew.CompanyID = _companyID;
                _lvnew.TranDate = attDate;
                _lvnew.LeaveCode = CL_CODE;
                _lvnew.AppYear = DateTime.Today.Year;
                _lvnew.AppMonth = 0;
                _lvnew.ValidFrom = validFrom;
                _lvnew.ValidTo = validTo;
                _lvnew.Remarks = "Earned leave for working on a restday/holiday or for working 16 hours or more on a weekday";
                _lvnew.LeaveEarnType = "Earning";
                _lvnew.EmployeeID = employeeid;
                _lvnew.Earned = days;
                _lvnew.CreatedBy = "admin";
                _lvnew.CreatedDate = DateTime.Now;   
            }
        }

        public void Remove(string employeeid, DateTime attDate)
        {
            object[] find = { employeeid, attDate };
            if (_lvref.Find(find))
            {
                _lvdel.AddNew();
                _lvdel.SessionID = _sessionid;
                _lvdel.LeaveEarnID = _lvref.LeaveEarnID;
            }
        }

        public void Save()
        {
            _lvdel.Save();
            _lvnew.Save();

            LeaveEarn.ExcusedLeaveApplySession(_sessionid);            
        }


        #region LoadRef
        private void LoadRef(DateTime startDate, DateTime endDate, string employeeID)
        {
            _lvref = new LeaveEarn();
            object[] parameters = { _companyID, CL_CODE, employeeID, startDate, endDate };
            string sql = "select " +
                        "	a.LeaveEarnID, " +
                        "	b.EmployeeID, " +
                        "	a.TranDate " +
                        "from LeaveEarn a inner join LeaveEarnDtl b " +
                        "on a.leaveearnid=b.leaveearnid " +
                        "where " +
                        "	a.companyid = {0} and " +
                        "	a.leavecode = {1} and " +
                        "	(coalesce({2},'') = '' or b.EmployeeID = {2}) and " +
                        "	(a.TranDate between {3} and {4} ) and " +
                        "	a.SystemCreated = 1 ";
            _lvref.DynamicQuery(sql, parameters);
            _lvref.Sort = "EmployeeID,TranDate";
        }
        #endregion

        string _companyID;
        LeaveEarnTmp_del _lvdel;
        LeaveEarnTmp_new _lvnew;
        LeaveEarn _lvref;
        Guid _sessionid;
    }
}
