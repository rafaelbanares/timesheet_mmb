﻿using BNS.TK.Entities;
using System;

namespace BNS.TK.Business
{

    //transEntry - contains raw values but in two-pairs (in1-out1, in2-out2)
    public class ProcessAttend
    {

        #region CTOR
        public ProcessAttend(string CompanyID, string userid)
        {
            _companyID = CompanyID;
            _userid = userid;
        }
        #endregion


        public void Process(DateTime startDate, DateTime endDate, string employeeID = "", bool isSummary = false)
        {
            //load transactions
            CreateAttendanceFromTimeTrans(startDate, endDate, employeeID);
            LoadAttendance(startDate, endDate, employeeID);
            if (_attendance.RowCount == 0) return;
            LoadReferences(startDate, endDate, employeeID);
            _nightDiff = new NightDiff(_company.ND1Start, _company.ND1End, _company.ND2Start, _company.ND2End);
            _leave = new Leave(_companyID, startDate, endDate, employeeID);

            IOvertimeRepository overtimeRepo = new OvertimeRepository(_companyID, startDate, endDate, employeeID);
            _overtimeProcess = new OvertimeProcess(_nightDiff, overtimeRepo);

            _timecalc = new TimeCalc();
            _attendanceDto = new AttendanceDto();
            TimeEntry trans = new TimeEntry();
            ShiftEntry shift = new ShiftEntry();
            Raw raw = new Raw();
            Raw origRaw = new Raw();
            CompensatoryLeaveProcess xleave = new CompensatoryLeaveProcess(_companyID, startDate, endDate, employeeID);

            ProcessRaw processRaw = new ProcessRaw();
            ShiftEntry projected = new ShiftEntry();
            TimeEntry payable = new TimeEntry();
            Excused excusedApplication = new Excused();
            bool isrestday = false;
            
            do
            {
                _overtimeProcess.Clear();

                if (!_shift.Find(_attendance.Shiftcode)) throw new Exception("Shift code: " + _attendance.s_Shiftcode.TrimEnd() + " not found.");
                
                isrestday = IsRestday(_attendance.Date1);
                raw.Set(_attendance.Date1, _attendance.In1, _attendance.Out1);
                origRaw.Set(_attendance.Date1, _attendance.In1, _attendance.Out1);
                _attendanceDto.Initialize();
                var hasLeave = _leave.FindLeave(_attendance.EmployeeID, _attendance.Date1);
                _holiday.FindHoliday(_attendance.Date1);
                _employee.Find(_attendance.EmployeeID);

                
                shift.SetTimeEntry(_attendance.s_Date1, _shift.s_In1, _shift.s_Out1, _shift.s_In2, _shift.s_Out2, _shift.Advanced_in);
                //shift.SetTimeEntry(_attendance.s_Date1, _shift.s_In1, _shift.s_Out1, _shift.s_In2, _shift.s_Out2, false);
                shift.Flexi = _shift.Flexi;
                shift.Flexi_in1 = _shift.Flexi_in1;
                shift.Flexi_in2 = _shift.Flexi_in2;
                shift.Grace_in1 = _shift.Grace_in1;
                shift.Grace_in2 = _shift.Grace_in2;
                

                //start of process
                DateRange OBtime = GetOB(_attendance.EmployeeID, _attendance.Date1);
                AdjustRawFromOB(raw, OBtime);

                var emp = new Employee();
                emp.LoadByPrimaryKey(_companyID, _attendance.EmployeeID);


                if (raw.NoEntry())
                    ProcessNoEntry();
                else if (!raw.ValidEntry() && !hasLeave && !isSummary && !emp.NonSwiper)
                {
                    _attendanceDto.remarks = "Invalid";
                    _attendanceDto.valid = false;
                }
                else if (raw.ValidEntry())
                {                    
                    if (isrestday || _holiday.Wholeday)
                    {
                        //overtime
                        _overtimeProcess.MinimumOTmins = _shift.Minimumot;
                        _overtimeProcess.ComputeOvertime(_attendance.EmployeeID, _attendance.Date1, raw.In, raw.Out, shift.In1, raw.In, _employee.OTExempt);
                        _attendanceDto.excess_mins = _timecalc.ExcessMins(raw.In, raw.Out);

                        if (_overtimeProcess.TotalOTmins > 0)
                            _attendanceDto.remarks = "Overtime";
                        else
                        {
                            _attendanceDto.remarks = (isrestday) ? "Restday" : _holiday.Description;
                        }

                        //Compensatory leave
                        if (_attendance.s_Status.IsEqual(TKWorkFlow.Status.Approved))
                        {
                            xleave.ProcessOT(_attendance.EmployeeID, _attendance.Date1, _attendanceDto.excess_mins);
                        }
                    }
                    else
                    {
                        //regular day
                        //                                                
                        projected = processRaw.ComputeProjectedShift(raw, shift, _attendance.EarlyWork, _leave.halfdayLeave, _holiday.Halfday);
                        trans = processRaw.ComputeTransEntry(raw, projected);
                        payable = processRaw.ComputePayableEntry(trans, projected, _leave.halfdayLeave, _holiday.Halfday, _holiday.HalfdayAM);
                        //
                        int unfiledOB = OBusiness.ComputeDeductibleUnfiledOB(OBtime, origRaw, payable);

                        if (_timecalc.ComputeTardy(payable, projected) > 0 || _timecalc.ComputeUT(payable, projected) > 0)
                        {
                            _offsetProcess.ApplyOffset(_attendance.EmployeeID, _attendance.Date1, projected, payable);
                        }

                        //adjust payable based on filed excused tardy/ut
                        //--------------------------------------------------------------------------------
                        DateRange excusedRange = GetExcusedTrans(_attendance.EmployeeID, _attendance.Date1);
                        if (!_employee.TardyExempt)
                        {
                            excusedApplication.ComputeExcusedTardy(excusedRange, payable, projected);
                            _attendance.ExcusedLate = (decimal)excusedApplication.ExcusedTardy / 60M;
                        }
                        else
                        {
                            //Tardy Exempt: remove late by adjusting payable in1
                            if (!payable.NoEntryInOut1() && payable.In1 > projected.In1)
                            {
                                payable.In1 = projected.In1;
                            }
                        }
                        if (!_employee.UTExempt)
                        {
                            excusedApplication.ComputeExcusedUT(excusedRange, payable, projected);
                            _attendance.ExcusedUT = (decimal)excusedApplication.ExcusedUT / 60;
                        }
                        else
                        {
                            //UT Exempt: remove UT by adjusting payable out2
                            if (!payable.NoEntryInOut2() && payable.Out2 < projected.Out2)
                            {
                                payable.Out2 = projected.Out2;
                            }
                        }
                        //--------------------------------------------------------------------------------

                        //tardy/ut/reg
                        _attendanceDto.late_mins = _timecalc.ComputeTardy(payable, projected) + unfiledOB;
                        _attendanceDto.UT_mins = _timecalc.ComputeUT(payable, projected);
                        
                        _attendanceDto.reg_mins = _timecalc.ComputeRegMinutesWork(payable);
                        _attendanceDto.absent_days = _timecalc.ComputeAbsent(payable, _leave.wholedayLeave, _leave.halfdayLeave, _holiday.Wholeday, _holiday.Halfday);

                        _overtimeProcess.MinimumOTmins = _shift.Minimumot;
                        _overtimeProcess.ComputeOvertime(_attendance.EmployeeID, _attendance.Date1, payable.Out2, raw.Out, shift.In1,raw.In, _employee.OTExempt);
                        _attendanceDto.excess_mins = _timecalc.ExcessMins(payable, raw.Out);

                        if (_employee.TardyExempt)
                        {
                            _attendance.Late = 0;
                            _attendance.Latemins = 0;
                            _attendance.ExcusedLate = _attendanceDto.late_mins / 60M;
                        }
                                                 
                        _attendanceDto.remarks = GetRegularDayRemarks();

                        if (!OBtime.NoEntry())
                        {
                            _attendanceDto.remarks = _attendanceDto.remarks + (_attendanceDto.remarks.TrimEnd() == "" ? "" : "/") + "OB";
                        }

                        //Compensatory leave
                        if (!Convert.IsDBNull(_attendance.GetColumn("status")) && _attendance.GetColumn("status").ToString().TrimEnd().ToLower() == "approved")
                        {
                            xleave.Process(_attendance.EmployeeID, _attendance.Date1, _attendanceDto.reg_mins + _attendanceDto.excess_mins);
                        }
                    }

                }

                _attendance.Absent =  _attendanceDto.absent_days;
                _attendance.Leavecode = _leave.leaveCode;
                _attendance.Leave = _leave.days;
                _attendance.Excesshrs = _attendanceDto.excess_mins / 60M;
                _attendance.Excessmins = _attendanceDto.excess_mins;
                _attendance.Remarks = (_attendanceDto.remarks == "") ? " " : _attendanceDto.remarks;
                _attendance.OTcode = OvertimeProcess.GetOTcode(isrestday, _holiday.Wholeday, _holiday.Legal);  //what if halfday holiday?
                _attendance.First8 = _overtimeProcess.First8 / 60M;
                _attendance.First8mins = _overtimeProcess.First8;
                _attendance.Greater8 = _overtimeProcess.Greater8 / 60M;
                _attendance.Greater8mins = _overtimeProcess.Greater8;
                _attendance.NDot1 = _overtimeProcess.ND1 / 60M;
                _attendance.NDot1mins = _overtimeProcess.ND1;
                _attendance.NDot2 = _overtimeProcess.ND2 / 60M;
                _attendance.NDot2mins = _overtimeProcess.ND2;
                
                _attendance.Reghrs = _attendanceDto.reg_mins / 60M;
                _attendance.Regmins = _attendanceDto.reg_mins;

                _attendance.Latemins = _attendanceDto.late_mins;
                _attendance.Late = decimal.Round(_attendanceDto.late_mins / 60M, 2);
                _attendance.UT = decimal.Round(_attendanceDto.UT_mins / 60M, 2);
                _attendance.UTmins = _attendanceDto.UT_mins;


                _attendance.Regnd1 = CalculateND1(_attendance.Date1, payable.In1, payable.Out1) / 60M;
                _attendance.Regnd1mins = CalculateND1(_attendance.Date1, payable.In1, payable.Out1);
                _attendance.Regnd2 = CalculateND2(_attendance.Date1, payable.In1, payable.Out1) / 60M;
                _attendance.Regnd2mins = CalculateND2(_attendance.Date1, payable.In1, payable.Out1);
                _attendance.Rendered = _attendance.Reghrs + _attendance.Excesshrs;
                _attendance.LastUpdBy = _userid;
                _attendance.LastUpdDate = DateTime.Now;
                _attendance.Processed = true;
                _attendance.Valid = _attendanceDto.valid;
                if (!_attendanceDto.valid)
                    _attendance.ErrorDesc = "Missing In/Out";

                
            } while (_attendance.MoveNext());

            // commit
            _attendance.Save();
            xleave.Save();

        }

        private DateRange GetOB(string employeeid, DateTime date)
        {
            DateRange ob = new DateRange();
            object[] objKey = { employeeid, date };
            if (_obtrans.Find(objKey))
            {
                ob.Start = _obtrans.StartDate;
                ob.End = _obtrans.EndDate;
            }
            return ob;
        }

        private DateRange GetExcusedTrans(string employeeid, DateTime date)
        {
            DateRange excused = new DateRange();
            object[] objKey = { employeeid, date };
            if (_excusedTardyUT.Find(objKey))
            {
                excused.Start = _excusedTardyUT.StartDate;
                excused.End = _excusedTardyUT.EndDate;
            }
            return excused;
        }

        
        //Adjust raw entry if filed an OB
        private void AdjustRawFromOB(Raw raw, DateRange ob)
        {
            if (ob.NoEntry()) return;
            if (!raw.ValidEntry()) return;


            if (raw.NoEntry())
            {
                raw.In = ob.Start;
                raw.Out = ob.End;
            }
            else
            {
                if (_obtrans.StartDate < raw.In)
                {
                    raw.In = _obtrans.StartDate;
                }

                if (_obtrans.EndDate > raw.Out)
                {
                    raw.Out = _obtrans.EndDate;
                }
            }
        }

        


        #region IsRestday
        private bool IsRestday(DateTime date)
        {
            int dayofweek = (int)date.DayOfWeek;
            if (dayofweek == 0) dayofweek = 7; // change to 7 if sunday

            return _attendance.s_Restdaycode.Contains(dayofweek.ToString());
        }
        #endregion

        #region GetRegularDayRemarks
        private string GetRegularDayRemarks()
        {
            string rsbMarker = "[***]";
            System.Text.StringBuilder sb = new System.Text.StringBuilder(rsbMarker);

            //absent remarks
            if (_attendanceDto.absent_days == 1)
                sb.Append("/Absent");
            else if (_attendanceDto.absent_days == .5M)
                sb.Append("/halfday Absent");

            if (_attendance.EarlyWork)
                sb.Append("/Early Login");

            if (_holiday.Halfday)
                sb.Append("/Halfday Holiday");

            //leave remarks
            if (_leave.days == .5M)
                sb.Append("/halfday " + _leave.leaveCode.TrimEnd());
            else if (_leave.days == 1)
                sb.Append("/" + _leave.leaveCode);
            else if (_leave.days > 0)
                sb.Append("/" + _leave.hours.ToString("#0.00") + "hrs " +_leave.leaveCode);

            if (_attendanceDto.late_mins > 0)
                sb.Append("/Tardy");
            
            if (_attendanceDto.UT_mins > 0)
                sb.Append("/UT");

            if (_overtimeProcess.First8 > 0)
                sb.Append("/OT");

            //if (_offsetProcess.LastOffsetUsed > 0)
            //    sb.Append("/Offset");

            string rem = sb.ToString().Replace(rsbMarker + "/", "").Replace(rsbMarker, "");
            if (_attendanceDto.reg_mins >= 480 && rem.TrimEnd() == "")
            {                
                if (_attendance.ExcusedLate > 0)
                    rem = "On-time(Excused Tardy)";
                else if (_attendance.ExcusedUT > 0)
                    rem = "On-time(Excused UT)";
                else
                    rem = "On-time";
            }

            return rem;
        }
        #endregion

        private bool xPossiblyLatex(Raw raw, ShiftEntry shift)
        {
            return (raw.In > shift.In1);
        }

       
        
        #region FlexiOnOvertime
        // NOTE: not implemented yet - wait for lowe to ask for this
        //allow flexi - if there is an approved ot authorization  (30minutes minimum ot excluding offset time))
        //example: reported to work at 9:35am = 5mins late (35mins extension should be met to qualify for flexi and OT)

        //private bool FlexiOnOvertime(ShiftEntry shiftEntry, ShiftEntry projectedShift, TimeEntry transEntry, TimeEntry payable, processRaw mt)
        //{
        //    if (_overtime.IsApproved(_attendance.EmployeeID, _attendance.Date1))
        //    {
        //        ShiftEntry tmpProjectedShift = new ShiftEntry(projectedShift.In1, projectedShift.Out1, projectedShift.In2, projectedShift.Out2, shiftEntry.Flexi);
        //        TimeEntry tmpPayable = new TimeEntry(payable.In1, payable.Out1, payable.In2, payable.Out2);
        //        if (mt.ComputeFlexiPayableFromRaw(shiftEntry, transEntry, tmpProjectedShift, tmpPayable, _leave.halfdayLeave, _shift.Flexi_in1, _shift.Flexi_in2))
        //        {
        //            _overtime.ComputeOvertime(_attendance.EmployeeID, _attendance.Date1, tmpPayable.Out2, transEntry.Out2, _shift.Minimumot, _employee.OTExempt);
        //            if (_overtime.First8 > 0) return true;
        //        }
        //    }

        //    return false;
        //}
        #endregion


        #region ProcessNoEntry
        private void ProcessNoEntry()
        {
            _attendanceDto.remarks = "";
            if (IsRestday(_attendance.Date1))
                _attendanceDto.remarks = "Restday";

            else if (_holiday.Wholeday)
                _attendanceDto.remarks = _holiday.Description;

            else
            {
                //regularday noentry: check if approved leave
                if (_leave.days == 0)   //no filed leave
                {
                    if (_holiday.Halfday)
                    {
                        _attendanceDto.absent_days = .5M;
                        _attendanceDto.remarks = _holiday.Description.TrimEnd() + " (Halfday)";
                    }
                    else
                    {
                        _attendanceDto.absent_days = 1;
                        _attendanceDto.remarks = "Absent";
                    }
                }
                else
                {
                    _attendanceDto.leave = _leave.days;
                    _attendanceDto.remarks = _leave.Description(_leave.leaveCode);
                }
            }            
        }
        #endregion

        


        //private bool IsWholedayHoliday(DateTime date)
        //{
        //    if (_holiday.Find(date))
        //        return (!_holiday.Halfday);
        //    else
        //        return false;
        //}
        //private bool IsHalfdayHoliday(DateTime date)
        //{
        //    if (_holiday.Find(date))
        //        return (_holiday.Halfday);
        //    else
        //        return false;
        //}

        //private bool IsLegal(DateTime date)
        //{
        //    if (!_holiday.Find(date)) return false;
        //    return (_holiday.Legal);
        //}



        private int CalculateND1(DateTime date, DateTime xin, DateTime xout)
        {
            SinglePair nd1range = _nightDiff.GetND1range(date);
            return ComputeRegularND(nd1range.In, nd1range.Out, xin, xout);
        }

        private int CalculateND2(DateTime date, DateTime xin, DateTime xout)
        {
            SinglePair nd2range = _nightDiff.GetND2range(date);
            return ComputeRegularND(nd2range.In, nd2range.Out, xin, xout);
        }

         private int ComputeRegularND(DateTime ndstart, DateTime ndend, DateTime xin, DateTime xout)
        {
            DateTime actualNdStart = (xin > ndstart) ? xin : ndstart;
            DateTime actualNdEnd = (xout > ndend) ? ndend : xout;
            if (actualNdStart > actualNdEnd) return 0;

            return (int)(actualNdEnd - actualNdStart).TotalMinutes;
        }
        
 

        #region LoadReferences
        private void LoadReferences(DateTime startDate, DateTime endDate, string employeeid)
        {
            _company = new Company();
            _company.Where.CompanyID.Value = _companyID;
            _company.Query.Load("");

            _employee = new Employee();
            _employee.Query.AddResultColumn(Employee.ColumnNames.EmployeeID);
            _employee.Query.AddResultColumn(Employee.ColumnNames.TardyExempt);
            _employee.Query.AddResultColumn(Employee.ColumnNames.UTExempt);
            _employee.Query.AddResultColumn(Employee.ColumnNames.OTExempt);
            _employee.Where.CompanyID.Value = _companyID;
            if (employeeid != null && employeeid.TrimEnd() != "")
            {
                _employee.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
                _employee.Where.EmployeeID.Value = employeeid;
            }
            _employee.Query.Load("");
            _employee.Sort = Employee.ColumnNames.EmployeeID;

            _offsetProcess = new OffsetProcess(_companyID, startDate, endDate, employeeid, _attendance);
            
            // shift
            _shift = new Shift();
            _shift.Where.CompanyID.Value = _companyID;
            _shift.Query.Load("");
            _shift.Sort = Shift.ColumnNames.Shiftcode;

            ////Holiday
            _holiday = new HolidayEntity(_companyID, startDate.Year);

            
            //UTRoundoff
            _utRoundoff = new UTroundoff();
            _utRoundoff.Where.CompanyID.Value = _companyID;
            _utRoundoff.Query.Load("");

            _obtrans = new OBTrans();
            _obtrans.Where.CompanyID.Value = _companyID;
            _obtrans.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            _obtrans.Where.Status.Value = TKWorkFlow.Status.Approved;  
            _obtrans.Query.Load("");     //TODO: load only what is needed 
            _obtrans.Sort = OBTrans.ColumnNames.EmployeeID + ", " + OBTrans.ColumnNames.OBDate;

            _excusedTardyUT = new ExcusedTardyUT();
            _excusedTardyUT.Where.CompanyID.Value = _companyID;
            _excusedTardyUT.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            _excusedTardyUT.Where.Status.Value = TKWorkFlow.Status.Approved;
            _excusedTardyUT.Query.Load("");     //TODO: load only what is needed 
            _excusedTardyUT.Sort = ExcusedTardyUT.ColumnNames.EmployeeID + ", " + ExcusedTardyUT.ColumnNames.ExcuseDate;

        }
        #endregion        


        #region LoadAttendance
        private void LoadAttendance(DateTime startDate, DateTime endDate, string employeeID)
        {
            _attendance = new Attendance();
            object[] parameters = { _companyID, 0, startDate, endDate, employeeID };
            string sql = "SELECT a.*, b.[Status] " +
                        "FROM Attendance a Left Join WorkDistHdr b " +
                        "On a.CompanyID = b.CompanyID AND " +
                        "   a.EmployeeID = b.EmployeeID AND " +
                        "   a.[Date] = b.[Date] " +
                        "WHERE a.CompanyID = {0} AND a.Posted = {1} AND (a.Date between {2} and {3}) AND " + 
                        "   (coalesce({4},'') = '' or a.EmployeeID = {4}) " +
                        "Order By a.EmployeeID, a.Date ";
            _attendance.DynamicQuery(sql, parameters);
        }
        #endregion

        #region CreateAttendanceFromRaw
        private void CreateAttendanceFromTimeTrans(DateTime startDate, DateTime endDate, string employeeID)
        {            
            Attendance.CreateAttendanceFromTimeTrans(_companyID, startDate, endDate, employeeID);
        }
        #endregion

        //entities
        string _companyID;
        string _userid = "";
        Company _company;
        NightDiff _nightDiff;
        Attendance _attendance;
        Employee _employee;
        Shift _shift;
        Leave _leave;
        OvertimeProcess _overtimeProcess;
        TimeCalc _timecalc;
        OffsetProcess _offsetProcess;
        AttendanceDto _attendanceDto;        
        //Holiday _holiday;
        HolidayEntity _holiday;
        UTroundoff _utRoundoff;
        OBTrans _obtrans;
        ExcusedTardyUT _excusedTardyUT;
        //OBusiness _obusiness;
        
    }



   
}
