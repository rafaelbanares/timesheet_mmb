﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace BNS.TK.Business
{
    public class TKWorkFlow
    {
        public bool HasApproveAction { get; private set; }
        public bool HasDeclineAction { get; private set; }
        public bool HasCancelAction { get; private set; }
        public bool HasRecallAction { get; private set; }
        public bool HasEditAction { get; private set; }

        #region CTOR
        public TKWorkFlow(){}
        public TKWorkFlow(string status, bool isPosted) 
        {
            SetApproverActions(status, isPosted);
        }
        #endregion

        #region Initialize
        private void Initialize()
        {
            HasApproveAction = false;
            HasDeclineAction = false;
            HasCancelAction = false;
            HasRecallAction = false;
            HasEditAction = false;
        }
        #endregion

        public void SetApproverActions(string status, bool isPosted)
        {
            Initialize();

            if (isPosted || IsCancelled(status)) return;            

            //for approval
            if (IsForApproval(status))
            {
                HasApproveAction = true;
                HasDeclineAction = true;
                return;
            }
            //declined/approved
            if (IsDeclined(status) || IsApproved(status))
            {
                HasRecallAction = true;
                return;
            }
            
            return;
        }

        public void SetCreatorActions(string status, bool isPosted)
        {
            Initialize();

            if (isPosted) return;

            //for approval
            if (IsForApproval(status))
            {
                HasCancelAction = true;
                HasEditAction = true;
                return;
            }
            
            return;
        }

        

        #region Status
        public static class Status
        {
            public const string ForApproval = "For Approval";
            public const string Approved = "Approved";
            public const string Declined = "Declined";
            public const string Cancelled = "Cancelled";
        }
        #endregion


        #region Action
        public static class Action
        {
            public const string Approve = "Approve";
            public const string Decline = "Decline";
            public const string Cancel = "Cancel";
            public const string Recall = "Recall";
            public const string Edit = "Edit";
            public const string NoAction = "";
        }
        #endregion

        #region Check Status
        public static bool IsForApproval(string status)
        {
            return status.IsEqual(Status.ForApproval);
        }

        public static bool IsApproved(string status)
        {
            return status.IsEqual(Status.Approved);
        }

        public static bool IsDeclined(string status)
        {
            return status.IsEqual(Status.Declined);
        }

        public static bool IsCancelled(string status)
        {
            return status.IsEqual(Status.Cancelled);
        }        
        #endregion
         

    }
    
}
