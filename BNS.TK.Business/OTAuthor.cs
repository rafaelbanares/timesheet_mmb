﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class OTAuthor
    {
        #region CTOR
        public OTAuthor() { }
        public OTAuthor(DateTime otstart, DateTime otend, bool isapproved)
        {
            OTstart = otstart;
            OTend = otend;
            IsApproved = isapproved;
        }
        #endregion

        public DateTime OTstart { get; set; }
        public DateTime OTend { get; set; }
        public bool IsApproved { get; set; }

    }
}
