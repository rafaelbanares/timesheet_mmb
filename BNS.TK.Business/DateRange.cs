﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class DateRange
    {
        public DateTime Start;
        public DateTime End;

        #region CTOR
        public DateRange()
        {
            Start = DateTime.MinValue;
            End = DateTime.MinValue;
        }
        public DateRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }
        #endregion

        public bool NoEntry()
        {
            return (Start == DateTime.MinValue) && (End == DateTime.MinValue);
        }
    }

}
