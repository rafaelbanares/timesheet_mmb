﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public interface IOvertimeRepository
    {
        OTAuthor GetOTAuthorization(string employeeid, DateTime date);
    }
}
