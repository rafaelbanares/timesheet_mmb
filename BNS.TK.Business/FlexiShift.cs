﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class XFlexiShiftX
    {
        public XFlexiShiftX()
        {
        }



        #region OBSOLETE
        //public bool ApplyFlexiSched(ShiftEntry shift, TimeEntry trans, ShiftEntry projectedShift, TimeEntry payable, bool approvedHalfdayLeave, int flexi_in1, int flexi_in2)
        //{
        //    if (trans.NoEntry()) return false;
            
        //    //initialize projected to shift
        //    projectedShift.In1 = shift.In1;
        //    projectedShift.Out1 = shift.Out1;
        //    projectedShift.In2 = shift.In2;
        //    projectedShift.Out2 = shift.Out2;

        //    //default payable
        //    payable.In1 = trans.In1 > shift.In1 ? trans.In1 : shift.In1;
        //    payable.Out1 = shift.Out1;
        //    payable.In2 = shift.In2;
        //    payable.Out2 = trans.Out2 < shift.Out2 ? trans.Out2 : shift.Out2;

        //    if (trans.NoEntryInOut1())
        //    {
        //        projectedShift.In1 = DateTime.MinValue;
        //        projectedShift.Out1 = DateTime.MinValue;
        //        payable.In1 = DateTime.MinValue;
        //        payable.Out1 = DateTime.MinValue;
        //        payable.In2 = trans.In2 > shift.In2 ? trans.In2 : shift.In2;

        //        int trd = (int)(trans.In2 - shift.In2).TotalMinutes;
        //        if (WithinFlexiRange(flexi_in2, trd))
        //        {                    
        //            if ((approvedHalfdayLeave && _applyFlexiForApprovedHalfDay) || (!approvedHalfdayLeave && _applyFlexiForUnapprovedHalfDay))
        //            {
        //                projectedShift.In2 = shift.In2.AddMinutes(trd);
        //                projectedShift.Out2 = shift.Out2.AddMinutes(trd);

        //                payable.In2 = projectedShift.In2;
        //                payable.Out2 = trans.Out2 < projectedShift.Out2 ? trans.Out2 : projectedShift.Out2;

        //                return true;
        //            }
        //        }
        //    }
        //    else if (trans.NoEntryInOut2())
        //    {
        //        projectedShift.In2 = DateTime.MinValue;
        //        projectedShift.Out2 = DateTime.MinValue;
        //        payable.In2 = DateTime.MinValue;
        //        payable.Out2 = DateTime.MinValue;
        //        payable.Out1 = trans.Out1 < shift.Out1 ? trans.Out1 : shift.Out1;

        //        int trd = (int)(trans.In1 - shift.In1).TotalMinutes;
        //        if (WithinFlexiRange(flexi_in1, trd))
        //        {  
        //            if ((approvedHalfdayLeave && _applyFlexiForApprovedHalfDay) || (!approvedHalfdayLeave && _applyFlexiForUnapprovedHalfDay))
        //            {
        //                projectedShift.In1 = shift.In1.AddMinutes(trd);
        //                projectedShift.Out1 = shift.Out1.AddMinutes(trd);

        //                payable.In1 = projectedShift.In1;
        //                payable.Out1 = trans.Out1 < projectedShift.Out1 ? trans.Out1 : projectedShift.Out1;

        //                return true;
        //            }
        //        }
        //    }
        //    else //complete trans
        //    {
        //        int trd = (int)(trans.In1 - shift.In1).TotalMinutes;
        //        if (WithinFlexiRange(flexi_in1, trd))
        //        {
        //            projectedShift.In1 = trans.In1;
        //            projectedShift.Out1 = shift.Out1.AddMinutes(trd);
        //            projectedShift.In2 = shift.In2.AddMinutes(trd);
        //            projectedShift.Out2 = shift.Out2.AddMinutes(trd);

        //            //adjust payable break
        //            payable.In1 = trans.In1;
        //            payable.Out1 = projectedShift.Out1;
        //            payable.In2 = projectedShift.In2;
        //            payable.Out2 = trans.Out2 < projectedShift.Out2 ? trans.Out2 : projectedShift.Out2;

        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //private bool WithinFlexiRange(int flexi_in, int trd)
        //{
        //    return (((flexi_in < 0 && trd < 0) || (flexi_in > 0 && trd > 0)) && Math.Abs(trd) <= Math.Abs(flexi_in));
        //}

        //public bool ApplyFlexiForApprovedHalfDay 
        //{ 
        //    get { return _applyFlexiForApprovedHalfDay; } 
        //    set { _applyFlexiForApprovedHalfDay = value; } 
        //}
        //public bool ApplyFlexiForUnapprovedHalfDay
        //{
        //    get { return _applyFlexiForUnapprovedHalfDay; }
        //    set { _applyFlexiForUnapprovedHalfDay = value; } 
        //}

        //private bool _applyFlexiForApprovedHalfDay = true;
        //private bool _applyFlexiForUnapprovedHalfDay = false; //default
        #endregion
    }
}
