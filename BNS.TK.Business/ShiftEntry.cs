﻿using System;

namespace BNS.TK.Business
{
    public class ShiftEntry
    {
        public DateTime In1 { get; set; }
        public DateTime Out1 { get; set; }
        public DateTime In2 { get; set; }
        public DateTime Out2 { get; set; }
        public bool Flexi { get; set; }
        public int Flexi_in1 { get; set; }
        public int Flexi_in2 { get; set; }
        public int Grace_in1 { get; set; }
        public int Grace_in2 { get; set; }

        
        #region CTOR
        public ShiftEntry() { }
        public ShiftEntry(DateTime in1, DateTime out1, DateTime in2, DateTime out2)
        {
            In1 = in1;
            Out1 = out1;
            In2 = in2;
            Out2 = out2;
        }
        public ShiftEntry(string processdate, string in1, string out1, string in2, string out2, bool advanced_in)
        {        
            SetTimeEntry(processdate, in1, out1, in2, out2, advanced_in);
        }
        
        #endregion


        public void SetTimeEntry(string processdate, string in1, string out1, string in2, string out2, bool advanced_in)
        {            
            
            if (in1.TrimEnd() == "")
                In1 = DateTime.MinValue;
            else
            {
                In1 = (advanced_in) ? DateTime.Parse(processdate + " " + in1).AddDays(-1) : DateTime.Parse(processdate + " " + in1);
            }
            Out1 = (out1.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + out1);
            In2 = (in2.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + in2);
            Out2 = (out2.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + out2);
                        
            if (Out1 < In1) Out1 += new TimeSpan(24, 0, 0);
            if (In2 < Out1) In2 += new TimeSpan(24, 0, 0);
            if (Out2 < In2) Out2 += new TimeSpan(24, 0, 0);
        }

        public void SetTimeEntry(DateTime xin1, DateTime xout1, DateTime xin2, DateTime xout2)
        {
            In1 = xin1;
            Out1 = xout1;
            In2 = xin2;
            Out2 = xout2;
        }

        public int GetAM_mins()
        {
            return (int)(Out1 - In1).TotalMinutes;
        }

        public int GetPM_mins()
        {
            return (int)(Out2 - In2).TotalMinutes;
        }

        public int GetBreak_mins()
        {
            return (int)(In2 - Out1).TotalMinutes;
        }


        public void CreateFlexShift(DateTime xin1, ShiftEntry projected)
        {
            projected.In1 = xin1;
            projected.Out1 = projected.In1.AddMinutes(GetAM_mins());
            projected.In2 = projected.Out1.AddMinutes(GetBreak_mins());
            projected.Out2 = projected.In2.AddMinutes(GetPM_mins());
        }

        public void CreateFlexShiftFromIN2(DateTime xin2, ShiftEntry projected)
        {
            projected.In2 = xin2;
            projected.Out1 = projected.In2.AddMinutes(GetBreak_mins() * -1);
            projected.In1 = projected.Out1.AddMinutes(GetAM_mins() * -1);
            projected.Out2 = projected.In2.AddMinutes(GetPM_mins());
        }

        public static void CreateMaxFlexShift(ShiftEntry shift, int flex_in1, ShiftEntry projected)
        {
            projected.In1 = shift.In1.AddMinutes(flex_in1);
            projected.Out1 = shift.Out1.AddMinutes(flex_in1);
            projected.In2 = shift.In2.AddMinutes(flex_in1);
            projected.Out2 = shift.Out2.AddMinutes(flex_in1);
        }

    }

}
