﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class HolidayEntity
    {

        public string Description { get; private set; }
        public bool Halfday { get; private set; }
        public bool Wholeday { get; private set; }
        public bool HalfdayAM { get; private set; }
        public bool Legal { get; private set; }
        
        

        #region CTOR
        public HolidayEntity(string CompanyID, int year)
        {
            _companyID = CompanyID;

            LoadHolidays(year);
        }
        #endregion

       
        public bool FindHoliday(DateTime date)
        {
            Wholeday = false;
            Halfday = false;
            HalfdayAM = false;
            Legal = false;
            Description = "";
            
            //holiday
            if (!_holiday.Find(date)) return false;

            Halfday = _holiday.Halfday;
            Wholeday = (!_holiday.Halfday);
            HalfdayAM = _holiday.HalfdayAM;
            Legal = _holiday.Legal;
            Description = _holiday.Description;
            
            return true;
        }

        #region LoadHolidays
        private void LoadHolidays(int year)
        {
            _holiday = new Holiday();
            object[] parameters = {_companyID, year};
            _holiday.DynamicQuery("select * From Holiday Where CompanyID = {0} and (year([Date]) = {1} or year([Date]) = ({1} + 1))", parameters);
            _holiday.Sort = Holiday.ColumnNames.Date;
        }
        #endregion


        string _companyID;
        Holiday _holiday;
    }
}
