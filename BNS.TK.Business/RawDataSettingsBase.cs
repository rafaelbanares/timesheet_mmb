﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public abstract class RawDataSettingsBase
    {
        #region CTOR
        public RawDataSettingsBase()
        {
            SwipeID = new RawAttribute();
            EmployeeID = new RawAttribute();
            LogDate = new RawAttribute();
            LogTime = new RawAttribute();
            IO = new RawIOAttribute();
        }
        #endregion

        public RawAttribute SwipeID { get; private set; }
        public RawAttribute LogDate { get; private set; }
        public RawAttribute LogTime { get; private set; }
        public RawIOAttribute IO { get; private set; }
        public RawAttribute EmployeeID { get; private set; }
        //public string InMarker { get; set; }
        //public string OutMarker { get; set; }


        public virtual bool IsValidIOCode(string io)
        {
            return (io == IO.InMarker || io == IO.OutMarker);
        }

    }

    public class RawAttribute
    {
        /// <summary>
        /// Position starting from one (1)
        /// </summary>
        public int Position;


        /// <summary>
        /// 
        /// Ordinal Position starting from one (1)
        /// Use this only if file is delimited
        /// </summary>
        public int DelimitedSequence;

        /// <summary>
        /// character count
        /// </summary>
        public int Length;

        /// <summary>
        /// Format string for datetime
        /// </summary>
        public string Format;
    }

    public class RawIOAttribute : RawAttribute
    {
        public string InMarker { get; set; }
        public string OutMarker { get; set; }
    }
}
