﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class OvertimeRepository : IOvertimeRepository
    {
        #region CTOR
        public OvertimeRepository(string companyID, DateTime startDate, DateTime endDate, string employeeID)
        {
            LoadOTAuthorization(companyID, startDate, endDate, employeeID);
        }
        #endregion


        public OTAuthor GetOTAuthorization(string employeeid, DateTime date)
        {
            OTAuthor auth = new OTAuthor();
            auth.IsApproved = false;

            object[] key = { employeeid, date };
            if (!_otAuthorization.Find(key)) return auth;

            auth.IsApproved = true;
            auth.OTstart = _otAuthorization.OTstart;
            auth.OTend = _otAuthorization.OTend;

            return auth;
        }

        #region LoadOTAuthorization
        private void LoadOTAuthorization(string companyID, DateTime startDate, DateTime endDate, string employeeID)
        {
            _otAuthorization = new OTAuthorization();
            object[] parameters = { companyID, startDate, endDate, employeeID };
            string sql = "Select " +
                        "	EmployeeID, " +
                        "	OTDate, " +
                        "	OTstart, " +
                        "	OTend " +
                        "From OTAuthorization " +
                        "Where " +
                        "	CompanyID = {0} and " +
                        "   Posted = 0 and " +
                        "	Status = 'Approved' and " +
                        "	(OTDate between {1} and {2}) and " +
                        "   (coalesce({3},'') = '' or EmployeeID = {3}) ";

            _otAuthorization.DynamicQuery(sql, parameters);
            _otAuthorization.Sort = "EmployeeID, OTDate";
        }
        #endregion

        private OTAuthorization _otAuthorization;
    }
}
