﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class ProcessRaw
    {
        public ProcessRaw() { }

         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="shift"></param>
        /// <param name="flexi"></param>
        /// <param name="flexi_in1"></param>
        /// <param name="flexi_in2"></param>
        /// <param name="halfdayAmLeave"></param>
        /// <param name="halfdayAmHoliday"></param>
        /// <returns></returns>
        public ShiftEntry ComputeProjectedShift(Raw raw, ShiftEntry shift, bool approvedEarlyLogin, bool halfdayLeave, bool halfdayHoliday)
        {
            ShiftEntry projectedShift = new ShiftEntry(DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);

            if (raw.NoEntry()) return projectedShift;
            if (!raw.ValidEntry()) return projectedShift;
            
            if (raw.In > shift.In1) //possibly late 
            {                                
                if (shift.Flexi)
                {
                    if (WithinFlexiRange(shift.Flexi_in1, raw.In, shift.In1))
                        shift.CreateFlexShift(raw.In, projectedShift);
                    else if (raw.In >= shift.In2 && raw.In <= shift.In2.AddMinutes(shift.Flexi_in2))
                        //create projected shift based on Shift IN2 if log-in is already within Halfday work in afternoon
                        // sample login 2:00PM => 10 - 2:00 - 3:00 - 7:00
                        shift.CreateFlexShiftFromIN2(raw.In, projectedShift);
                    else if (halfdayHoliday || halfdayLeave)
                        projectedShift.SetTimeEntry(shift.In1, shift.Out1, shift.In2, shift.Out2);                            
                    else
                        ShiftEntry.CreateMaxFlexShift(shift, shift.Flexi_in1, projectedShift);
                }
                else
                {
                    if (WithinFlexiRange(shift.Grace_in1, raw.In, shift.In1))
                        shift.CreateFlexShift(raw.In, projectedShift);
                    else if (raw.In >= shift.In2 && raw.In <= shift.In2.AddMinutes(shift.Grace_in2))
                        //create projected shift based on Shift IN2 if log-in is already within Halfday work in afternoon
                        // sample login 2:00PM => 10 - 2:00 - 3:00 - 7:00
                        shift.CreateFlexShiftFromIN2(raw.In, projectedShift);
                    else
                        projectedShift.SetTimeEntry(shift.In1, shift.Out1, shift.In2, shift.Out2);
                }
            }
            else //on-time or early login
            {
                if (approvedEarlyLogin)
                    shift.CreateFlexShift(raw.In, projectedShift);
                else
                    projectedShift.SetTimeEntry(shift.In1, shift.Out1, shift.In2, shift.Out2);
            }
       
            return projectedShift;
        }

        #region ComputeTransEntry
        /// <summary>
        /// Compute trans entry from raw and projected shift
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="projectedShift"></param>
        /// <returns></returns>
        public TimeEntry ComputeTransEntry(Raw raw, ShiftEntry projectedShift)
        {
            TimeEntry trans = new TimeEntry(DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);

            if (raw.NoEntry()) return trans;
            if (!raw.ValidEntry())
            {
                trans.In1 = raw.In;
                trans.Out1 = raw.Out;
                return trans;
            }
            
            //determine if Halfday
            if (raw.In >= projectedShift.Out1)
            {
                trans.In2 = raw.In;       //noentry in am
                trans.Out2 = raw.Out;
            }
            else
            {
                if (raw.Out <= projectedShift.In2)
                {
                    trans.In1 = raw.In;
                    trans.Out1 = raw.Out;  //noentry in pm
                }
                else
                {
                    //both AM PM
                    trans.In1 = raw.In;
                    trans.Out1 = projectedShift.Out1;
                    trans.In2 = projectedShift.In2;
                    trans.Out2 = raw.Out;
                }
            }
            return trans;
        }
        #endregion


       /// <summary>
       /// returns payable entry
       /// </summary>
       /// <param name="trans"></param>
       /// <param name="projectedShift"></param>
       /// <returns></returns>
        public TimeEntry ComputePayableEntry(TimeEntry trans, ShiftEntry projectedShift, bool halfdayLeave, bool halfdayHoliday, bool halfdayHolidayAM)
        {            
            TimeEntry payable = new TimeEntry(DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
            
            if (!trans.ValidEntry()) return payable;

            if (trans.NoEntryInOut1())
            {
                //no entry in AM
                payable.In1 = DateTime.MinValue;
                payable.Out1 = DateTime.MinValue;
                payable.In2 = trans.In2 > projectedShift.In2 ? trans.In2 : projectedShift.In2;
                payable.Out2 = trans.Out2 < projectedShift.Out2 ? trans.Out2 : projectedShift.Out2;
            }
            else if (trans.NoEntryInOut2())
            {
                //no entry in PM
                payable.In1 = trans.In1 > projectedShift.In1 ? trans.In1 : projectedShift.In1;
                payable.Out1 = trans.Out1 < projectedShift.Out1 ? trans.Out1 : projectedShift.Out1;
                payable.In2 = DateTime.MinValue;
                payable.Out2 = DateTime.MinValue;
            }
            else //complete trans
            {                
                payable.In1 = trans.In1 > projectedShift.In1 ? trans.In1 : projectedShift.In1;
                payable.Out1 = projectedShift.Out1;
                payable.In2 = projectedShift.In2;
                payable.Out2 = trans.Out2 < projectedShift.Out2 ? trans.Out2 : projectedShift.Out2;
            }

            //check if halfday holiday
            if (halfdayHoliday)
            {
                if (halfdayHolidayAM)
                {
                    payable.In1 = DateTime.MinValue;
                    payable.Out1 = DateTime.MinValue;
                }
                else
                {
                    payable.In2 = DateTime.MinValue;
                    payable.Out2 = DateTime.MinValue;
                }
            }
            //check if halfday leave
            if (halfdayLeave)
            {
                if (HalfdayAM_leave(trans, projectedShift))
                {
                    payable.In1 = DateTime.MinValue;
                    payable.Out1 = DateTime.MinValue;
                }
                else
                {
                    payable.In2 = DateTime.MinValue;
                    payable.Out2 = DateTime.MinValue;
                }
            }

            return payable;
        }

        private bool HalfdayAM_leave(TimeEntry trans, ShiftEntry projectedShift)
        {
            bool leaveAM = true;
            if (trans.NoEntryInOut1())
                return leaveAM;

            if (trans.NoEntryInOut2())
               return !leaveAM;

            if (trans.In1 <= projectedShift.In1)
               return !leaveAM;
            else
            {
                int tardy = (int)(trans.In1 - projectedShift.In1).TotalMinutes;
                int ut = (int)(projectedShift.Out2 - trans.Out2).TotalMinutes;
               return (tardy <= ut) ? !leaveAM : leaveAM;
            }
        }

        private bool WithinFlexiRange(int flexi_in, DateTime trans_in1, DateTime shift_in1)
        {
            int trd = (int)(trans_in1 - shift_in1).TotalMinutes;            
            return ((flexi_in > 0 && trd > 0) && Math.Abs(trd) <= Math.Abs(flexi_in));
        }

        private bool dnull(DateTime date)
        {
            return (date == DateTime.MinValue) ? true : false;
        }
    }
}
