﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class RawTimeParser
    {
        #region CTOR
        public RawTimeParser(RawDataSettingsBase rawSettings)
        {
            _rawSettings = rawSettings;
        }
        #endregion

        public string SwipeID { get; set; }
        public string EmployeeID { get; set; }
        public DateTime LogDateTime { get; set; }
        public string io { get; set; }

        public bool IsValidIOCode()
        {
            return (io == "I" || io == "O");
        }

        /// <summary>
        /// raw data have fixed positions
        /// </summary>
        /// <param name="rawData"></param>
        public void Parse(string rawData)
        {
            //card number
            SwipeID = "";
            if (_rawSettings.SwipeID.Position >= 0)
            {
                SwipeID = rawData.Substring(_rawSettings.SwipeID.Position.Less(1), _rawSettings.SwipeID.Length).TrimEnd();
            }

            //Employee number
            EmployeeID = null;
            if (_rawSettings.EmployeeID.Position >= 0)
            {
                EmployeeID = rawData.Substring(_rawSettings.EmployeeID.Position.Less(1), _rawSettings.EmployeeID.Length).TrimEnd();
            }

            //log date and time
            string dateio = rawData.Substring(_rawSettings.LogDate.Position.Less(1),_rawSettings.LogDate.Length);
            string timeio = rawData.Substring(_rawSettings.LogTime.Position.Less(1), _rawSettings.LogTime.Length);            
            LogDateTime = DateTime.ParseExact(dateio + " " + timeio, _rawSettings.LogDate.Format + " " + _rawSettings.LogTime.Format, CultureInfo.InvariantCulture);

            //In/Out
            string xio = rawData.Substring(_rawSettings.IO.Position.Less(1), _rawSettings.IO.Length);
            if (xio == _rawSettings.IO.InMarker)
                io = "I";
            else
                io = (xio == _rawSettings.IO.OutMarker) ? "O" : "X";
        }

        /// <summary>
        /// raw data separated by delimiter
        /// </summary>
        /// <param name="rawData"></param>
        /// <param name="delimiter"></param>
        public void Parse(string rawData, char delimiter)
        {
            if (rawData.IsNullOrEmpty()) return;    //throw new Exception("First row is empty. Please check.");

            string[] data = rawData.Split(delimiter);

            //card number
            SwipeID = "";
            if (_rawSettings.SwipeID.Position >= 0)
            {
                SwipeID = data[_rawSettings.SwipeID.DelimitedSequence.Less(1)].Trim();
            }

            //Employee number
            EmployeeID = null;
            if (_rawSettings.EmployeeID.Position >= 0)
            {
                //EmployeeID = data[_rawSettings.EmployeeID.DelimitedSequence.Less(1)].Substring(_rawSettings.EmployeeID.Position.Less(1)).TrimEnd();
                EmployeeID = data[_rawSettings.EmployeeID.DelimitedSequence.Less(1)].Trim();
            }

            //log date and time
            string dateio = data[_rawSettings.LogDate.DelimitedSequence.Less(1)].Substring(_rawSettings.LogDate.Position.Less(1), _rawSettings.LogDate.Length).TrimEnd();
            string timeio = data[_rawSettings.LogTime.DelimitedSequence.Less(1)].Substring(_rawSettings.LogTime.Position.Less(1), _rawSettings.LogTime.Length).TrimEnd();
            LogDateTime = DateTime.ParseExact(dateio + " " + timeio, _rawSettings.LogDate.Format + " " + _rawSettings.LogTime.Format, CultureInfo.InvariantCulture);

            //In/Out
            string xio = data[_rawSettings.IO.DelimitedSequence.Less(1)].TrimEnd();
            if (xio == _rawSettings.IO.InMarker)
                io = "I";
            else
                io = (xio == _rawSettings.IO.OutMarker) ? "O" : "X";
        }


        private RawDataSettingsBase _rawSettings;
    }

    
}
