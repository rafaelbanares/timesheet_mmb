﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class TimeEntry
    {     
        public DateTime In1 { get; set; }
        public DateTime Out1 { get; set; }
        public DateTime In2 { get; set; }
        public DateTime Out2 { get; set; }

        #region String Properties
        //public string s_In1 { get { return In1.ToString(datetime_format); } }
        //public string s_Out1 { get { return Out1.ToString(datetime_format); } }
        //public string s_In2 { get { return In2.ToString(datetime_format); } }
        //public string s_Out2 { get { return Out2.ToString(datetime_format); } }
        #endregion



        #region CTOR
        public TimeEntry() { }

        public TimeEntry(DateTime xin1, DateTime xout1, DateTime xin2, DateTime xout2)
        {            
            In1 = xin1;
            Out1 = xout1;
            In2 = xin2;
            Out2 = xout2;
        }
        public TimeEntry(string processdate, string xin1, string xout1, string xin2, string xout2)
        {
            SetTimeEntry(processdate, xin1, xout1, xin2, xout2);
        }
        #endregion
        

        public void SetTimeEntry(string processdate, string xin1, string xout1, string xin2, string xout2)
        {
            In1 = (xin1.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + xin1);
            Out1 = (xout1.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + xout1);
            In2 = (xin2.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + xin2);
            Out2 = (xout2.TrimEnd() == "") ? DateTime.MinValue : DateTime.Parse(processdate + " " + xout2);

            if (!dnull(Out1) && Out1 < In1) Out1 += new TimeSpan(24, 0, 0);
            if (!dnull(In2) && In2 < Out1) In2 += new TimeSpan(24, 0, 0);
            if (!dnull(Out2) && Out2 < In2) Out2 += new TimeSpan(24, 0, 0);
        }
        public void SetTimeEntry(DateTime xin1, DateTime xout1, DateTime xin2, DateTime xout2)
        {
            In1 = xin1;
            Out1 = xout1;
            In2 = xin2;
            Out2 = xout2;
        }

        #region Utilities
        public bool dnull(DateTime inout)
        {
            return (inout == DateTime.MinValue);
        }

        public bool ValidEntry()
        {
            return ((dnull(In1) == dnull(Out1)) && (dnull(In2) == dnull(Out2)));
        }

        public bool NoEntry()
        {
            return (In1 == DateTime.MinValue && Out1 == DateTime.MinValue && In2 == DateTime.MinValue && Out2 == DateTime.MinValue);
        }
        public bool NoEntryInOut1()
        {
            //return (!NoEntry() && In1 == DateTime.MinValue && Out1 == DateTime.MinValue);
            return (In1 == DateTime.MinValue && Out1 == DateTime.MinValue);
        }
        public bool NoEntryInOut2()
        {
            //return (!NoEntry() && In2 == DateTime.MinValue && Out2 == DateTime.MinValue);
            return (In2 == DateTime.MinValue && Out2 == DateTime.MinValue);
        }
        #endregion


        private const string datetime_format = "MM/dd/yyyy hh:mm tt";
    }

}
