﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class RawDataSettings : RawDataSettingsBase
    {
        #region CTOR
        public RawDataSettings(string companyid):base()
        {
            LoadSettings(companyid);
        }
        #endregion

    

        //TODO: refactor this to put settings in DB
        private void LoadSettings(string companyid)
        {
            LoadStepanSettings();

            //if (companyid.IsEqual("HYUNDAI"))
            //{
            //    LoadHyundaiSettings();
            //}
            //else if (companyid.IsEqual("JASON"))
            //{
            //    LoadJasonSettings();
            //}
            //else if (companyid.IsEqual("LOWE"))
            //{
            //    LoadLoweSettings();
            //}
            //else if (companyid.IsEqual("STEPAN"))
            //{
            //    LoadStepanSettings();
            //}

        }

        //HYUNDAI
        public void LoadHyundaiSettings()
        {
            SwipeID.Position = 13;
            SwipeID.Length = 6;

            LogDate.Position = 3;
            LogDate.Length = 6;
            LogDate.Format = "ddMMyy";

            LogTime.Position = 9;
            LogTime.Length = 4;
            LogTime.Format = "HHmm";

            IO.Position = 19;
            IO.Length = 1;
            IO.InMarker = "0";
            IO.OutMarker = "9";            
        }

        //Stepan - tab delimited
        public void LoadStepanSettings()
        {

            EmployeeID.DelimitedSequence = 1;
            EmployeeID.Position = 2;

            LogDate.DelimitedSequence = 2;
            LogDate.Position = 1;
            LogDate.Length = 10;
            LogDate.Format = "yyyy-MM-dd";

            LogTime.DelimitedSequence = 2;
            LogTime.Position = 12;
            LogTime.Length = 5;
            LogTime.Format = "HH:mm";

            IO.DelimitedSequence = 4;
            IO.Position = 1;
            IO.Length = 1;
            IO.InMarker = "0";
            IO.OutMarker = "1";

            SwipeID.Position = -1;  //not used
        }

        //LOWE
        public void LoadLoweSettings()
        {
            SwipeID.Position = 18;
            SwipeID.Length = 4;

            LogDate.Position = 4;
            LogDate.Length = 8;
            LogDate.Format = "yyyyMMdd";

            LogTime.Position = 12;
            LogTime.Length = 4;
            LogTime.Format = "HHmm";

            IO.Position = 22;
            IO.Length = 1;
            IO.InMarker = "0";
            IO.OutMarker = "9";
        }

        //JASON
        public void LoadJasonSettings()
        {
            SwipeID.Position = 1;
            SwipeID.Length = 12;

            LogDate.Position = 22;
            LogDate.Length = 10;
            LogDate.Format = "MM/dd/yyyy";

            LogTime.Position = 33;
            LogTime.Length = 5;
            LogTime.Format = "HH:mm";

            IO.Position = 42;
            IO.Length = 1;
            IO.InMarker = "1";
            IO.OutMarker = "?";
        }

        //MMB
        public void LoadMMBISettings()
        {
            SwipeID.Position = 3;            
            LogDate.Length = 6;
            LogDate.Format = "MMd/dd/yyyy";

            LogTime.Position = 12;
            LogTime.Length = 4;
            LogTime.Format = "HHmm";         
        }

    }
}
