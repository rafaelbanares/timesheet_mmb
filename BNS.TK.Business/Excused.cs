﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class Excused
    {
        /// <summary>
        /// Gets (sets) the computed excused tardy in minutes
        /// </summary>
        public int ExcusedTardy { get; private set; }

        /// <summary>
        /// Gets (sets) the computed excused undertime in minutes
        /// </summary>
        public int ExcusedUT { get; private set; }




        public void ComputeExcusedTardy(DateRange excused, TimeEntry payable, ShiftEntry projected)
        {
            ExcusedTardy = 0;

            if (excused.NoEntry()) return;
            if (payable.NoEntry()) return;

            // excused tardy
            DateRange excusedTardy = new DateRange(DateTime.MinValue, DateTime.MinValue);
            if (!payable.NoEntryInOut1())
            {
                // check if he is late and the filed excused start and end is within the allowed range
                if (payable.In1 > projected.In1 && excused.Start < payable.In1 && excused.End >= payable.In1)
                {
                    excusedTardy.Start = excused.Start;
                    excusedTardy.End = (excused.End > payable.In1) ? payable.In1 : excused.End;
                    ExcusedTardy = (int)(excusedTardy.End - excusedTardy.Start).TotalMinutes;

                    //adjust payable
                    if (ExcusedTardy > 0)
                    {
                        payable.In1 = payable.In1.AddMinutes(ExcusedTardy * -1);
                        payable.In1 = (payable.In1 < projected.In1) ? projected.In1 : payable.In1;                   
                    }
                }
            }
            
            return;
        }


        public void ComputeExcusedUT(DateRange excused, TimeEntry payable, ShiftEntry projected)
        {
            ExcusedUT = 0;

            if (excused.NoEntry()) return;
            if (payable.NoEntry()) return;

            DateRange excusedUT = new DateRange(DateTime.MinValue, DateTime.MinValue);
            if (!payable.NoEntryInOut2())
            {
                // check if he is undertime and the filed excused start and end is within the allowed range
                if (payable.Out2 < projected.Out2 && (excused.Start >= payable.In2 && excused.Start <= payable.Out2) && excused.End > payable.Out2)
                {
                    excusedUT.Start = excused.Start;
                    excusedUT.End = excused.End;
                    ExcusedUT = (int)(excusedUT.End - excusedUT.Start).TotalMinutes;

                    //adjust payable
                    if (ExcusedUT > 0)
                    {
                        payable.Out2 = payable.Out2.AddMinutes(ExcusedUT);
                        payable.Out2 = (payable.Out2 > projected.Out2) ? projected.Out2 : payable.Out2;
                    }
                }
            }

            return;
        }
    }
}
