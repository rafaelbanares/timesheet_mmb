﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public struct SinglePair
    {
        #region CTOR
        public SinglePair(DateTime xin, DateTime xout)
        {
            In = xin;
            Out = xout;
        }
        #endregion

        public DateTime In;
        public DateTime Out;
    
        public bool NoEntry()
        {
            return (In == DateTime.MinValue) && (Out == DateTime.MinValue);
        }
    }
}
