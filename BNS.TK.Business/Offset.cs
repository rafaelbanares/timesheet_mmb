﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class Offset
    {
        #region CTOR
        public Offset(){}
        public Offset(int offsetMinimum)
        {
            _offsetMinimum = offsetMinimum;
        }
        #endregion

        public int OffsetMinimum
        {
            get { return _offsetMinimum; }
            set { _offsetMinimum = value; }
        }
        public int ActualTardyUT
        {
            get { return _actualTardyUT; }
        }



        /// <summary>
        /// this must be the last step to be applied 
        ///    i.e. grace, flexi, earlywork, etc. must already be applied
        /// offsetBal and offset are in minutes
        /// payable time and offsetBal will be adjusted if there is an offset
        /// returns the offset in minutes used.
        /// </summary>
        /// <param name="projectedShift"></param>
        /// <param name="payable"></param>
        /// <param name="offsetBal"></param>
        /// <returns></returns>
        public int ComputeOffset(ShiftEntry projectedShift, TimeEntry payable, int offsetUsed_mins)
        {
            if (offsetUsed_mins <= 0) return 0;

            
            int offsetTardy2 = 0;
            int offsetUt2 = 0;
            if (!payable.NoEntryInOut2()) //PM entry exists
            {                
                int preTardy2 = (int)(payable.In2 - projectedShift.In2).TotalMinutes;
                int preUt2 = (int)(projectedShift.Out2 - payable.Out2).TotalMinutes;
                
                //tardy PM offset
                if (offsetUsed_mins >= preTardy2 && preTardy2 > 0)
                {
                    offsetTardy2 = preTardy2;
                    offsetUsed_mins -= offsetTardy2;
                }
                else if (preTardy2 > 0)
                {
                    offsetTardy2 = offsetUsed_mins;
                    offsetUsed_mins = 0;
                }

                //ut PM offset                
                if (offsetUsed_mins >= preUt2 && preUt2 > 0)
                {
                    offsetUt2 = preUt2; 
                    offsetUsed_mins -= offsetUt2;
                }
                else if (preUt2 > 0)
                {
                    offsetUt2 = offsetUsed_mins;
                    offsetUsed_mins = 0;
                }
            }
            

            int offsetTardy1 = 0;
            int offsetUt1 = 0;
            if (!payable.NoEntryInOut1())       //AM entry exists
            {
                int preTardy1 = (int)(payable.In1 - projectedShift.In1).TotalMinutes;
                int preUt1 = (int)(projectedShift.Out1 - payable.Out1).TotalMinutes;

                //tardy AM offset                
                if (offsetUsed_mins >= preTardy1 && preTardy1 > 0)
                {
                    offsetTardy1 = preTardy1;
                    offsetUsed_mins -= offsetTardy1;
                }
                else if (preTardy1 > 0)
                {
                    offsetTardy1 = offsetUsed_mins;
                    offsetUsed_mins = 0;
                }

                //ut AM offset 
                if (offsetUsed_mins >= preUt1 && preUt1 > 0)
                {
                    offsetUt1 = preUt1;
                    offsetUsed_mins -= offsetUt1;
                }
                else if (preUt1 > 0)
                {
                    offsetUt1 = offsetUsed_mins;
                    offsetUsed_mins = 0;
                }
            }
            //else if (!payable.NoEntry() && !approvedHalfdayLeave)
            //{
            //    //noentry in AM but not absent && not halfday leave                
            //    int preTardy1 = (int)(projectedShift.Out1 - projectedShift.In1).TotalMinutes;
            //    if (preTardy1 > 0)
            //    {
            //        payable.In1 = projectedShift.Out1;   //intentional to be adjusted later
            //        payable.Out1 = projectedShift.Out1;
            //        if (offsetUsed_mins >= preTardy1)
            //        {
            //            offsetTardy1 = preTardy1;
            //            offsetUsed_mins -= offsetTardy1;
            //        }
            //        else
            //        {
            //            offsetTardy1 = offsetUsed_mins;
            //            offsetUsed_mins = 0;
            //        }
            //    }

            //}

            //adjust the payable time based on offset
            if (offsetTardy1 > 0) payable.In1 = payable.In1.AddMinutes(offsetTardy1 * -1);
            if (offsetTardy2 > 0) payable.In2 = payable.In2.AddMinutes(offsetTardy2 * -1); 
            if (offsetUt1 > 0) payable.Out1 = payable.Out1.AddMinutes(offsetUt1);
            if (offsetUt2 > 0) payable.Out2 = payable.Out2.AddMinutes(offsetUt2);
            
            //total offset
            int offsetUsed = offsetTardy1 + offsetTardy2 + offsetUt1 + offsetUt2;
            _actualTardyUT = offsetUsed;

            if (offsetUsed > 0 && offsetUsed < _offsetMinimum)
                offsetUsed = _offsetMinimum;

            return offsetUsed;
        }



        private int _offsetMinimum = 0;
        private int _actualTardyUT = 0;
    }
}
