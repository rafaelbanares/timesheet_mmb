﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{

    public class OvertimeProcess
    {
        #region CTOR
        public OvertimeProcess(NightDiff ndiff, IOvertimeRepository overtimeRepo)
        {            
            //LessBreakOTmins = 240;  //default 4hours
            LessBreakOTmins = 0;  //default 4hours
            MinimumOTmins = 30;     //default to 30 mins minimum ot

            _nightDiff = ndiff;
            _overtimeRepo = overtimeRepo;
        }
        #endregion

        /// <summary>
        /// (less 1hr break if > this value)
        /// </summary>
        public int LessBreakOTmins { get; set; }

        /// <summary>
        /// Minimum Overtime (in minutes)
        /// </summary>
        public int MinimumOTmins { get; set; }

        /// <summary>
        /// First 8 hours overtime
        /// </summary>
        public int First8 { get; private set; }

        /// <summary>
        /// Beyond 8 hours overtime
        /// </summary>
        public int Greater8 { get; private set; }

        /// <summary>
        /// Night Diff 1
        /// </summary>
        public int ND1 { get; private set; }

        /// <summary>
        /// Night Diff 2
        /// </summary>
        public int ND2 { get; private set; }

        /// <summary>
        /// Total Overtime hours
        /// </summary>
        public int TotalOTmins { get; private set; }


        public void Clear()
        {
            First8 = 0;
            Greater8 = 0;
            ND1 = 0;
            ND2 = 0;
            TotalOTmins = 0;
        }

        /// <summary>
        /// Compute the Overtime of an employee
        /// </summary>
        /// <param name="employeeid"></param>
        /// <param name="otdate"></param>
        /// <param name="otStart"></param>
        /// <param name="actualEnd"></param>
        /// <param name="otexempt"></param>
        public void ComputeOvertime(string employeeid, DateTime otdate, DateTime otStart, DateTime actualEnd, DateTime shiftStart, DateTime rawStart, bool otexempt)
        {
            First8 = 0; Greater8 = 0; ND1 = 0; ND2 = 0; TotalOTmins = 0;

            if (otexempt) return;

            OTAuthor otauth = _overtimeRepo.GetOTAuthorization(employeeid, otdate);
            if (!otauth.IsApproved) return;
            
            SinglePair payableOT = new SinglePair();

            if (otauth.OTstart < shiftStart)
            {
                //ot before shift
                payableOT = GetPayableOTentry(rawStart, shiftStart, otauth);
            }
            else
            {
                payableOT = GetPayableOTentry(otStart, actualEnd, otauth);
            }


            //Total OT
            TotalOTmins = CalculateTotalOT(payableOT);
            
            //first 8 hours overtime
            First8 = (TotalOTmins > 480) ? 480 : TotalOTmins;

            //after 8 hours overtime
            Greater8 = (TotalOTmins > 480) ? TotalOTmins - 480 : 0;

            //night diff
            ND1 = CalculateND1(otdate, payableOT);
            ND2 = CalculateND2(otdate, payableOT);
        }


        private SinglePair GetPayableOTentry(DateTime otStart, DateTime actualOTend, OTAuthor otauth)
        {
            SinglePair payableOT;
            payableOT.In = (otStart < otauth.OTstart) ? otauth.OTstart : otStart;
            payableOT.Out = (actualOTend < otauth.OTend) ? actualOTend : otauth.OTend;
            return payableOT;
        }

    


        private int CalculateND1(DateTime date, SinglePair ot)
        {
            SinglePair nd1range = _nightDiff.GetND1range(date);
            return ComputeNDOT(nd1range.In, nd1range.Out, ot.In, ot.Out);
        }

        private int CalculateND2(DateTime date, SinglePair ot)
        {
            SinglePair nd2range = _nightDiff.GetND2range(date);
            return ComputeNDOT(nd2range.In, nd2range.Out, ot.In, ot.Out);
        }

        #region GetOTcode
        //rst  hol   lgl
        //0     0     0     REG
        //0     0     1     REG
        //0     1     0     SPL
        //0     1     1     LGL
        //1     0     0     RST
        //1     0     1     RST
        //1     1     0     SPLRST
        //1     1     1     LGLRST
        public static string GetOTcode(bool isRestday, bool isHoliday, bool isLegal)
        {
            string otcode;
            
            if (!isRestday && isHoliday)
                otcode = isLegal ? "LGL" : "SPL";

            else if (isRestday && !isHoliday)
                otcode = "RST";

            else if (isRestday && isHoliday)
                otcode = isLegal ? "LGLRST" : "SPLRST";
            else
                otcode = "REG";

            return otcode;
        }
        #endregion


        #region ComputeNDOT
        /// <summary>
        /// return ND in minutes
        /// </summary>
        /// <param name="ndstart"></param>
        /// <param name="ndend"></param>
        /// <param name="otstart"></param>
        /// <param name="otend"></param>
        /// <returns></returns>
        private int ComputeNDOT(DateTime ndstart, DateTime ndend, DateTime otstart, DateTime otend)
        {
            DateTime actualNdStart = (otstart > ndstart) ? otstart : ndstart;
            DateTime actualNdEnd = (otend > ndend) ? ndend : otend;
            if (actualNdStart > actualNdEnd) return 0;

            return (int)(actualNdEnd - actualNdStart).TotalMinutes;
        }
        #endregion

        #region CalculateTotalOT
        private int CalculateTotalOT(SinglePair ot)
        {
            int totalOTmins = (int)(ot.Out - ot.In).TotalMinutes;
            if (totalOTmins < MinimumOTmins) return 0;

            //less 1hr break
            if (totalOTmins > LessBreakOTmins && LessBreakOTmins > 0)
            {
                totalOTmins -= 60;
                if (totalOTmins < LessBreakOTmins)
                    totalOTmins = LessBreakOTmins;
            }
            return totalOTmins;
        }
        #endregion


        
        private NightDiff _nightDiff;
        private IOvertimeRepository _overtimeRepo;
    }

}
