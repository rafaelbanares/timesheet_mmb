﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class OffsetProcess
    {
        
        #region CTOR
        public OffsetProcess(string CompanyID, DateTime startDate, DateTime endDate, string employeeid, Attendance attendance)
        {
            _companyID = CompanyID;
            _attendance = attendance;

            _offset = new Offset(30); //30 minutes default minimum offset

            LoadCurrentOffsetApplications(startDate, endDate, employeeid);
        }
        #endregion

      

        /// <summary>
        /// we dont need to validate offset balance here, it is the job of the leave (offset) during application
        /// returns the offset used in minutes.
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="date"></param>
        /// <param name="projectedShift"></param>
        /// <param name="payable"></param>
        /// <param name="approvedHalfdayLeave"></param>
        /// <returns></returns>
        public int ApplyOffset(string employeeID, DateTime date, ShiftEntry projectedShift, TimeEntry payable)
        {
            if (employeeID == null) return 0;
            
            if (ApprovedOffset(employeeID, date))
                return _offset.ComputeOffset(projectedShift, payable, _offsetTrans.OffsetMins);                
            
            return 0;
        }

        private bool ApprovedOffset(string employeeid, DateTime date)
        {
            object[] find = {employeeid, date};
            return (_offsetTrans.Find(find));
        }


        #region LoadCurrentOffsetApplications
        /// <summary>
        /// approved only
        /// </summary>
        /// <param name="processStartDate"></param>
        /// <param name="processEndDate"></param>
        /// <param name="employeeID"></param>
        private void LoadCurrentOffsetApplications(DateTime processStartDate, DateTime processEndDate, string employeeID)
        {
            _offsetTrans = new OffsetTrans();
            object[] parameters = { _companyID, employeeID, processStartDate, processEndDate };
            string sql = "select * " +
                        "from OffsetTrans " +
                        "Where CompanyID = {0} and " +
                        "	(coalesce({1},'') = '' or EmployeeID = {1}) and " +
                        "	Status = 'Approved' and " +
                        "	(DateUsed between {2} and {3} ) " +
                        "Order By EmployeeID, DateUsed ";

            _offsetTrans.DynamicQuery(sql, parameters);
            _offsetTrans.Sort = "EmployeeID, DateUsed";
        }
        #endregion



        //private 
        Attendance _attendance;
        OffsetTrans _offsetTrans;
        Offset _offset;
        string _companyID;


    }
}
