﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public struct AttendanceDto
    {
        public int late_mins { get; set; }
        public int UT_mins { get; set; }
        public decimal absent_days { get; set; }
        public decimal leave { get; set; }
        public int excess_mins { get; set; }
        public string remarks { get; set; }
        public string errorDesc { get; set; }
        public string supervisor { get; set; }
        public int reg_mins { get; set; }
        public int regnd1_mins { get; set; }
        public int regnd2_mins { get; set; }
        public bool valid { get; set; }

        public void Initialize()
        {
            late_mins = 0;
            UT_mins = 0;
            absent_days = 0;
            leave = 0;
            excess_mins = 0;
            remarks = "";
            errorDesc = "";
            supervisor = "";
            //otcode = "";
            //first8 = 0;
            //greater8 = 0;
            //ndot1 = 0;
            //ndot2 = 0;
            reg_mins = 0;
            regnd1_mins = 0;
            regnd2_mins = 0;
            valid = true;
        }
    }
}
