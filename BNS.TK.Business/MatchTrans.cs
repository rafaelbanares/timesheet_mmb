﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNS.TK.Entities;

namespace BNS.TK.Business
{
    public class MatchTrans
    {
        public MatchTrans() { }


        /// <summary>
        /// Shoot the raw time into two-pair entries (AM/PM)
        /// Applicable for 1 pair raw data (in-0ut)
        /// trans will contain 4 entries:
        ///     scenario (flexi)
        ///     raw:            01:30AM  10:30AM
        ///     output trans:	01:30AM  05:30AM 06:30AM  10:30AM
        /// </summary>
        /// <param name="raw">input: raw data</param>
        /// <param name="shift">input: employee shift</param>
        /// <param name="projectedShift">output: adjusted shift if flexi</param>
        /// <param name="trans">output: 2-pair entries from raw</param>
        /// <param name="flexi">input: Not necessarily mean flexi shift but it can also mean "early login"</param>
        /// <returns>True if valid raw data, False if invalid raw data</returns>
        public bool ShootOnePair(Raw raw, ShiftEntry shift, ShiftEntry projectedShift, TimeEntry trans, bool flexi, int flexi_in1, int flexi_in2)
        {
           
            trans.SetTimeEntry(DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
            
            if (raw.NoEntry()) return false;
            if (!raw.ValidEntry())
            {
                trans.In1 = raw.In;
                trans.Out1 = raw.Out;
                return false;
            }

            //determine projected shift
            if (flexi)
            {
                if (raw.In > shift.In1) //possibly late
                {
                    if (WithinFlexiRange(flexi_in1, raw.In, shift.In1))
                        shift.CreateFlexShift(raw.In, projectedShift);
                    else if (raw.In >= shift.In2 && raw.In <= shift.In2.AddMinutes(flexi_in2))
                        //set projected shift to base shift if log-in is already within HD work in afternoon
                        shift.CreateFlexShiftFromIN2(raw.In, projectedShift);
                    else
                        ShiftEntry.CreateMaxFlexShift(shift, flexi_in1, projectedShift);
                }
                else
                {
                    shift.CreateFlexShift(raw.In, projectedShift);
                }                
            }
            else
            {
                projectedShift.SetTimeEntry(shift.In1, shift.Out1, shift.In2, shift.Out2);
            }


            //determine if Halfday
            if (raw.In >= projectedShift.Out1)
            {
                trans.In2 = raw.In;       //noentry in am
                trans.Out2 = raw.Out;
            }
            else
            {
                if (raw.Out <= projectedShift.In2)  //must use the projected shift to check halfday PM due to possiblity of early login
                {
                    trans.In1 = raw.In;
                    trans.Out1 = raw.Out;  //noentry in pm
                }
                else
                {
                    //both AM PM
                    trans.In1 = raw.In;
                    trans.Out1 = projectedShift.Out1;
                    trans.In2 = projectedShift.In2;
                    trans.Out2 = raw.Out;
                }
            }

            return true;
        }
        


        /// <summary>
        /// Must recalc trans entry if not qualified for flexi due to this scenario
        /// raw           10:01AM - 02:01PM
        /// trans-flexi   10:01AM - 02:01PM                       (after applying flexi, trans is a halfday)
        /// trans-nflex   10:01AM - 12:30PM - 13:30PM - 02:01PM   (but due to fexi rule it is not qualified. must recalc trans for nonflexi shift)
        /// 
        /// </summary>
        /// <param name="raw">input: raw data</param>
        /// <param name="trans">output: 2-pair entries from raw</param>
        /// <param name="shift">input: employee shift</param>
        /// <param name="projectedShift">output: adjusted shift if flexi</param>
        /// <param name="payable">computed output</param>
        /// <param name="flexi">input: Not necessarily mean flexi shift but it can also mean "early login"</param>
        /// <param name="flexi_in1">input: fexi (AM) in minutes</param>
        /// <param name="flexi_in2">input: fexi (PM) in minutes</param>
        /// <returns>Modifies payable, trans and projectedShift</returns>
        public bool ComputePayableFromRaw(Raw raw, ShiftEntry shift, TimeEntry trans, ShiftEntry projectedShift, TimeEntry payable, bool flexi, int flexi_in1, int flexi_in2)
        {
            if (!ShootOnePair(raw, shift, projectedShift, trans, flexi, flexi_in1, flexi_in2)) 
                return false;
            
            if (trans.NoEntryInOut1())
            {
                //no entry in AM
                payable.In1 = DateTime.MinValue;
                payable.Out1 = DateTime.MinValue;
                payable.In2 = trans.In2 > projectedShift.In2 ? projectedShift.In2 : projectedShift.In2;
                payable.Out2 = trans.Out2 < projectedShift.Out2 ? trans.Out2 : projectedShift.Out2;
            }
            else if (trans.NoEntryInOut2())
            {
                //no entry in PM
                payable.In1 = trans.In1 > projectedShift.In1 ? trans.In1 : projectedShift.In1;
                payable.Out1 = trans.Out1 < projectedShift.Out1 ? trans.Out1 : projectedShift.Out1;
                payable.In2 = DateTime.MinValue;
                payable.Out2 = DateTime.MinValue;
            }
            else //complete trans
            {                
                payable.In1 = trans.In1 > projectedShift.In1 ? trans.In1 : projectedShift.In1;
                payable.Out1 = projectedShift.Out1;
                payable.In2 = projectedShift.In2;
                payable.Out2 = trans.Out2 < projectedShift.Out2 ? trans.Out2 : projectedShift.Out2;
            }
            return true;
        }

 
         

        private bool WithinFlexiRange(int flexi_in, int trd)
        {
            return (((flexi_in < 0 && trd < 0) || (flexi_in > 0 && trd > 0)) && Math.Abs(trd) <= Math.Abs(flexi_in));
        }

        private bool WithinFlexiRange(int flexi_in, DateTime trans_in1, DateTime shift_in1)
        {
            int trd = (int)(trans_in1 - shift_in1).TotalMinutes;
            return (((flexi_in < 0 && trd < 0) || (flexi_in > 0 && trd > 0)) && Math.Abs(trd) <= Math.Abs(flexi_in));
        }

        private bool dnull(DateTime date)
        {
            return (date == DateTime.MinValue) ? true : false;
        }
    }
}
