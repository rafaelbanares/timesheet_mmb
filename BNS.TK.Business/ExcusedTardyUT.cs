﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNS.TK.Business
{
    public class Excused
    {

        //raw must be the original raw entries
        public static int ComputeExcusedMins(DateRange excused, TimeEntry payable)
        {
            if (excused.NoEntry()) return 0;
            if (payable.NoEntry()) return 0;

            DateRange excusedAM = new DateRange(DateTime.MinValue, DateTime.MinValue);
            int excusedAM_mins = 0;
            if (!payable.NoEntryInOut1())
            {
                excusedAM.Start = (excused.Start < payable.In1) ? payable.In1 : excused.Start;
                excusedAM.End = (excused.End > payable.Out1) ? payable.Out1 : excused.End;
                excusedAM_mins = (int)(excusedAM.End - excusedAM.Start).TotalMinutes;
            }

            
            DateRange excusedPM = new DateRange(DateTime.MinValue, DateTime.MinValue);
            int excusedPM_mins = 0;
            if (!payable.NoEntryInOut2())
            {
                excusedPM.Start = (excused.Start < payable.In2) ? payable.In2 : excused.Start;
                excusedPM.End = (excused.End > payable.Out2) ? payable.Out2 : excused.End;
                excusedPM_mins = (int)(excusedPM.End - excusedPM.Start).TotalMinutes;
            }

            return excusedAM_mins + excusedPM_mins;
        }
    }
}
