using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for TransError
    /// </summary>

    public class TransError
    {
        string _errormsg = "";

        public string ErrMsg { get { return _errormsg; } set { _errormsg = value; } }
        public bool IsError { get { return _errormsg != ""; } }

        public TransError()
        {
        }
        private bool Empty(DateTime d)
        {
            return d == DateTime.MinValue;
        }
        public bool CheckExcessTrans(string excess)
        {
            if (excess.Length > 0)
                _errormsg = "excess transaction";

            return IsError;
        }
        public bool CheckIncomplete(DateTime inx, DateTime outx)
        {
            if (Empty(inx) && !Empty(outx))
                _errormsg = "incomplete - no in";
            else if (!Empty(inx) && Empty(outx))
                _errormsg = "incomplete - no out";

            return IsError;
        }
        public bool CheckVariance(DateTime inx, DateTime outx, TimeSpan variance)
        {
            if (outx.Subtract(inx) <= variance)
                _errormsg = "variance on transaction";

            return IsError;
        }

        public bool CheckShiftMismatch(DateTime in1, DateTime shiftin1)
        {
            TimeSpan range = new TimeSpan(6, 0, 0);

            DateTime timestart = shiftin1.Subtract(range);
            DateTime timeend = shiftin1.Add(range);

            // in1 must be within 6 hours before or after the shift
            if (in1 >= timestart && in1 <= timeend)
                return true; // not a shift error            

            if (in1 <= shiftin1)
            {
                //... check if cross over and still within the shift
                in1 += new TimeSpan(24, 0, 0);

                // in1 must be within 6 hours before or after the shift, same condition above just added 24 hours to in1 for cross over
                if (in1 >= timestart && in1 <= timeend)
                    return true; // not a shift error
            }

            _errormsg = "shift error";

            return IsError;
        }
    }
}

