using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for AttendanceSettings
    /// </summary>
    public class Settings
    {
        bool _flexbreak = false;
        bool _isdaily = false;
        bool _flexi = false;
        bool _isotb4shift = false;

        bool _tardyExempt = false;
        bool _utExempt = false;
        bool _otExempt = false;
        bool _nonSwiper = false;

        string _leavecode;
        decimal _leavedays;
        bool _leavewithpay;

        string _authOTcode;
        DateTime _authOTstart;
        DateTime _authOTend;
        double _approvedOT;

        DateTime _gracein1;
        DateTime _gracein2;
        DateTime _flextimeinlimit;
        TransFour _nd;
        TimeSpan _variance;
        TransTwo[] _companybreaks;

        bool _isrestday;
        bool _isholiday;

        int _minOT;
        double _maxTardy;
        double _maxUT;

        public TransTwo[] CompanyBreaks { get { return _companybreaks; } set { _companybreaks = value; } }
        public TransFour ND { get { return _nd; } set { _nd = value; } }
        public DateTime GraceIn1 { get { return _gracein1; } set { _gracein1 = value; } }
        public DateTime GraceIn2 { get { return _gracein2; } set { _gracein2 = value; } }

        public DateTime FlexTimeInLimit { get { return _flextimeinlimit; } set { _flextimeinlimit = value; } }
        public TimeSpan Variance { get { return _variance; } set { _variance = value; } }

        public int MinimumOT { get { return _minOT; } set { _minOT = value; } }
        public double MaxTardy { get { return _maxTardy; } set { _maxTardy = value; } }
        public double MaxUT { get { return _maxUT; } set { _maxUT = value; } }

        public string LeaveCode { get { return _leavecode; } set { _leavecode = value; } }
        public decimal LeaveDays { get { return _leavedays; } set { _leavedays = value; } }
        public bool LeaveWithPay { get { return _leavewithpay; } set { _leavewithpay = value; } }

        public string AuthOTcode { get { return _authOTcode; } set { _authOTcode = value; } }
        public DateTime AuthOTstart { get { return _authOTstart; } set { _authOTstart = value; } }
        public DateTime AuthOTend { get { return _authOTend; } set { _authOTend = value; } }
        public double ApprovedOT { get { return _approvedOT; } set { _approvedOT = value; } }

        public bool IsRestday { get { return _isrestday; } set { _isrestday = value; } }
        public bool IsHoliday { get { return _isholiday; } set { _isholiday = value; } }

        public bool FlexBreak { get { return _flexbreak; } set { _flexbreak = value; } }
        public bool Flexi { get { return _flexi; } set { _flexi = value; } }
        public bool IsDaily { get { return _isdaily; } set { _isdaily = value; } }
        public bool IsOTb4shift { get { return _isotb4shift; } set { _isotb4shift = value; } }

        public bool TardyExempt { get { return _tardyExempt; } set { _tardyExempt = value; } }
        public bool UTExempt { get { return _utExempt; } set { _utExempt = value; } }
        public bool OTExempt { get { return _otExempt; } set { _otExempt = value; } }
        public bool NonSwiper { get { return _nonSwiper; } set { _nonSwiper = value; } }

        //TimeSpan _allowableLatehrs = new TimeSpan(3, 0, 0);
        //public TimeSpan AllowableLatehrs { get { return _allowableLatehrs; } set { _allowableLatehrs = value; } }

        public TimeSpan ND1Count(DateTime _start, DateTime _end)
        {
            return Utils.GetTimeSpent(_nd.In1, _nd.Out1, _start, _end);
        }

        public TimeSpan ND2Count(DateTime _start, DateTime _end)
        {
            return Utils.GetTimeSpent(_nd.In2, _nd.Out2, _start, _end);
        }
    }
}