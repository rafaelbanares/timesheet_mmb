﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMB.Core
{
    public static class Constants
    {
        public static class UserMode
        {
            public const int Employee = 0;
            public const int Approver = 1;
            public const int Admin = 9; 
        }
    }
}
