using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for Attendance
    /// </summary>
    public class AttendanceBase
    {
        protected WorkSpan raw;
        protected Settings settings;

        protected TimeSpan _late = new TimeSpan();
        protected TimeSpan _ut = new TimeSpan();
        protected TimeSpan _latebreak = new TimeSpan();

        protected TimeSpan _exceshrs = new TimeSpan();
        protected TimeSpan _otb4shift = new TimeSpan();
        protected TimeSpan _nd1ot = new TimeSpan();
        protected TimeSpan _nd2ot = new TimeSpan();

        protected TimeSpan _reghrs = new TimeSpan();
        protected TimeSpan _nd1reg = new TimeSpan();
        protected TimeSpan _nd2reg = new TimeSpan();

        protected double _absent;
        protected string _remarks;
        protected string _errordesc;


        public double Late { get { return Utils.ConvertToDouble(_late + _latebreak); } }
        public double UT { get { return Utils.ConvertToDouble(_ut); } }
        public double Exceshrs  { get { return Utils.ConvertToDouble(_exceshrs + _otb4shift); } }

        public double ND1ot { get { return Utils.ConvertToDouble(_nd1ot); } }
        public double ND2ot { get { return Utils.ConvertToDouble(_nd2ot); } }

        public double Reghrs { get { return Utils.ConvertToDouble(_reghrs); } }
        public double ND1reg { get { return Utils.ConvertToDouble(_nd1reg); } }
        public double ND2reg { get { return Utils.ConvertToDouble(_nd2reg); } }

        public double Absent { get { return _absent; } }
        public string Remarks { get { return _remarks; } }
        public string ErrorDesc { get { return _errordesc; } }
    }

}