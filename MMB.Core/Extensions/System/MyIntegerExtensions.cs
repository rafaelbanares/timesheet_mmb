﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class MyIntegerExtensions
    {
        /// <summary>
        /// Perform subtraction
        /// </summary>
        /// <param name="value"></param>
        /// <param name="less"></param>
        /// <returns></returns>
        public static int Less(this int value, int less)
        {
            return (value - less);
        }
    }
}
