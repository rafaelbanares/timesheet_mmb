﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class MyStringExtensions
    {
        public static bool IsNumeric(this string s)
        {
            int n;
            return int.TryParse(s, out n);
        }

        public static bool IsDecimal(this string s)
        {
            double n;
            return double.TryParse(s, out n);
        }

        public static bool IsDate(this string s)
        {
            DateTime d;
            return DateTime.TryParse(s, out d);
        }


        /// <summary>
        /// Converts s to integer. Return zero if not numeric
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int ToInteger(this string s)
        {
            if (s.IsNumeric())
                return int.Parse(s);
            else
                return 0;
        }

        /// <summary>
        /// Returns true if s is > zero
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool GreaterZero(this string s)
        {
            return (s.ToInteger() > 0);
        }

        
        /// <summary>
        /// Returns true if equal ignoring Case and trailing spaces
        /// </summary>
        /// <param name="value"></param>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public static bool IsEqual(this string value, string compareTo)
        {
            return (value.TrimEnd().ToUpper() == compareTo.TrimEnd().ToUpper());
        }

        /// <summary>
        /// Formats the current <c>System.String</c> object applying the Camel Case format.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string ToPascalCase(this string item)
        {
            return string.IsNullOrEmpty(item) ? item : string.Concat(item.Substring(0, 1).ToUpperInvariant(), item.Substring(1, item.Length-1).ToLowerInvariant());
        }

        //public static string RemoveStart(this string item, string stringToRemove)
        //{
        //    return
        //        !string.IsNullOrEmpty(stringToRemove)
        //        && !string.IsNullOrEmpty(item)
        //        && item.StartsWith(stringToRemove) ? item.Substring(stringToRemove.Length) : item;
        //}
    }

}
