using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for Attendance_Flexi
    /// </summary>
    sealed public class Attendance_Flexi : AttendanceProcess
    {
        override public void Process(WorkSpan r, Settings s)
        {
            raw = r;
            settings = s;

            SetRemarks();
            SetErrorDesc();
            if (!r.AttErr.IsError)
            {
                //... this process should be in sequence...
                CheckAbsent();
                if (_absent == 0)
                {
                    raw.AssignFlexiBasedWorkHours(settings.FlexBreak, settings.FlexTimeInLimit);

                    ComputeLateFirstIn();
                    ComputeUTLastOut();
                    ComputeAbsentHalf();
                    ComputeLateBreak();
                    ComputeAbsentHalf();

                    ComputeExcessHours();   // this also computes ND OT
                    ComupteOTBeforeShift(); // this also computes ND OT

                    ComputeRegHrs();
                    ComputeNDRegHrs();

                    AdjustTardyUT();

                    //... recompute undertime because of tardiness adjustment
                    RecomputeUT();                  
                }
            }
        }

        private void RecomputeUT()
        {
            // if halfday abs, 
            if (raw.IsHalfDay1 || raw.IsHalfDay2)
                _ut = Utils.Subtract(raw.ShiftTotalhrs, raw.TotalWorkHrs + _late);
        }

        override protected void ComputeLateFirstIn()
        {
            _late += Utils.Subtract(raw.Work.In1, settings.FlexTimeInLimit);
        }

        override protected void ComputeUTLastOut()
        {
            _ut += Utils.Subtract(raw.ShiftTotalhrs, raw.TotalWorkHrs + _late);
        }

    }
}