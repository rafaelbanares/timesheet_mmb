using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for TransFour
    /// </summary>
    public struct TransTwo
    {
        DateTime _in;
        DateTime _out;

        public DateTime In { get { return _in; } set { _in = value; } }
        public DateTime Out { get { return _out; } set { _out = value; } }

        public TransTwo(DateTime timein, DateTime timeout)
        {
            _in = timein;
            _out = timeout;
        }

        public static TransTwo[] GetInOuts(string processdate, DataRowCollection datarows)
        {
            List<TransTwo> trans = new List<TransTwo>();
            foreach (DataRow dr in datarows)
            {
                DateTime timein = DateTime.Parse(processdate + " " + dr[0].ToString()); // timein from datarows[]
                DateTime timeout = DateTime.Parse(processdate + " " + dr[1].ToString()); // timeout from datarows[]

                trans.Add(new TransTwo(timein, timeout));
            }

            return trans.ToArray();
        }
    }

    /// <summary>
    /// Summary description for TransFour
    /// </summary>
    public struct TransFour
    {
        DateTime _in1;
        DateTime _out1;
        DateTime _in2;
        DateTime _out2;
        String _excess;

        public DateTime In1 { get { return _in1; } set { _in1 = value; } }
        public DateTime Out1 { get { return _out1; } set { _out1 = value; } }
        public DateTime In2 { get { return _in2; } set { _in2 = value; } }
        public DateTime Out2 { get { return _out2; } set { _out2 = value; } }
        public String ExcessTrans { get { return _excess; } set { _excess = value; } }


        public TransFour(DateTime in1, DateTime out1, DateTime in2, DateTime out2)
        {
            _in1 = in1;
            _out1 = out1;
            _in2 = in2;
            _out2 = out2;
            _excess = "";
        }

        public DateTime Parse(string date, string time)
        {
            DateTime retval = new DateTime();
            if (time.Length > 0)
                retval = DateTime.Parse(date + " " + time);

            return retval ;
        }

        public TransFour(string processdate, string in1, string out1, string in2, string out2)
        {
            _in1 = DateTime.Parse(processdate + " " + in1);
            _out1 = DateTime.Parse(processdate + " " + out1);

            if (_out1 < _in1)
                _out1 += new TimeSpan(24, 0, 0);

            _in2 = DateTime.Parse(processdate + " " + in2);
            if (_in2 < _out1)
                _in2 += new TimeSpan(24, 0, 0);

            _out2 = DateTime.Parse(processdate + " " + out2);
            if (_out2 < _in2)
                _out2 += new TimeSpan(24, 0, 0);

            _excess = "";
        }

        //public DateTimeParse()

        /*
        public bool IsEmpty_In1 { get { return _in1 == DateTime.MinValue; } }
        public bool IsEmpty_Out1 { get { return _out1 == DateTime.MinValue; } }
        public bool IsEmpty_In2 { get { return _in2 == DateTime.MinValue; } }
        public bool IsEmpty_Out2 { get { return _out2 == DateTime.MinValue; } }

        public TransFour(string date, string in1, string out1, string in2, string out2)
        {
            if (in1.Length > 0)
                _in1 = DateTime.Parse(date + " " + in1);

            if (out1.Length > 0)
                _out1 = DateTime.Parse(date + " " + out1);

            if (in2.Length > 0)
                _in2 = DateTime.Parse(date + " " + in2);

            if (out2.Length > 0)
                _out2 = DateTime.Parse(date + " " + out2);
        }
        */
    }
}