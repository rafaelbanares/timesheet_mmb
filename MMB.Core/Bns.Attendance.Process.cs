using System;
using System.Data;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for AttendanceProcess
    /// </summary>
    abstract public class AttendanceProcess : AttendanceBase
    {
        abstract public void Process(WorkSpan r, Settings s);
        abstract protected void ComputeLateFirstIn();
        abstract protected void ComputeUTLastOut();
        
        public TimeSpan tsExcessHrs { get { return _exceshrs + _otb4shift; } }
        public TimeSpan tsLate { get { return _late + _latebreak; } }
        public TimeSpan tsUT { get { return _ut; } }

        virtual protected void CheckAbsent()
        {
            if (raw.Transactions == 0)
            {                
                _absent = 1;
            }
        }

        virtual protected void ComputeAbsentHalf()
        {
            if (raw.IsHalfDay1)
            {
                _late = new TimeSpan();
                _absent = 0.5;
            }
            if (raw.IsHalfDay2)
            {
                _absent = 0.5;
                _ut = new TimeSpan();
            }
        }

        virtual protected void ComupteOTBeforeShift()
        {
            if (raw.Excess.In1 < raw.Excess.Out1)
            {
                TimeSpan excess = raw.Excess.Out1 - raw.Excess.In1;

                //... check if overtime is greater than the "shift minimum OT"
                if (excess.TotalMinutes >= settings.MinimumOT)
                {
                    _otb4shift += excess;
                    _nd1ot += (settings.ND1Count(raw.Excess.In1, raw.Excess.Out1));
                    _nd2ot += (settings.ND2Count(raw.Excess.In1, raw.Excess.Out1));
                }
            }        
        }

        virtual protected void ComputeExcessHours()
        {
            if (raw.Excess.In2 < raw.Excess.Out2)
            {
                TimeSpan excess = new TimeSpan();
                TimeSpan nd1excess = new TimeSpan();
                TimeSpan nd2excess = new TimeSpan();

                //... check if there is field overtime
                if (settings.AuthOTend > DateTime.MinValue)
                {
                    //... test if the filed overtime is crossover, OT before shift must be reset to false
                    //... scenario shift 10-6am  , trans 9pm-11am, field overtime 6am-8am
                    DateTime crossOTStart = settings.AuthOTstart.AddHours(24);
                    DateTime crossOTEnd = settings.AuthOTend.AddHours(24);
                    if (raw.Excess.In2 <= crossOTEnd && raw.Excess.Out2 >= crossOTStart)
                    {
                        settings.AuthOTstart = crossOTStart;
                        settings.AuthOTend = crossOTEnd;

                        //raw.Excess.In1 = DateTime.MinValue;
                        //raw.Excess.Out1 = DateTime.MinValue;
                        //settings.IsOTb4shift = false;
                    }
                    //.... end of adjustment

                    //... compute based on the filed overtime
                    DateTime inOT = raw.Excess.In2 > settings.AuthOTstart ? raw.Excess.In2 : settings.AuthOTstart;
                    DateTime outOT = raw.Excess.Out2 > settings.AuthOTend ? settings.AuthOTend : raw.Excess.Out2;

                    excess = (outOT - inOT);
                    nd1excess = (settings.ND1Count(inOT, outOT));
                    nd2excess = (settings.ND2Count(inOT, outOT));
                }
                else
                {
                    excess = (raw.Excess.Out2 - raw.Excess.In2);
                    nd1excess = (settings.ND1Count(raw.Excess.In2, raw.Excess.Out2));
                    nd2excess = (settings.ND2Count(raw.Excess.In2, raw.Excess.Out2));
                }

                //... check if overtime is greater than the "shift minimum OT"
                if (excess.TotalMinutes >= settings.MinimumOT)
                {
                    _exceshrs += excess;
                    _nd1ot += nd1excess;
                    _nd2ot += nd2excess;
                }
            }
        }

        virtual protected void CheckExemptions()
        {
            //// Line commented - excesshours are not overtime hours
            //if (settings.OTExempt)
            //{
            //    _exceshrs = Utils.ConvertToTime(0);
            //    _otb4shift = Utils.ConvertToTime(0);
            //}
            if (settings.TardyExempt)
            {
                _late = Utils.ConvertToTime(0);

                if (_absent == 0.5 && settings.LeaveDays <= Convert.ToDecimal(0))
                    _absent = 0;
                if (_absent == 0.5 && settings.LeaveDays > Convert.ToDecimal(0))
                    _absent =  Convert.ToDouble(settings.LeaveDays); // setting _absent = 0 will also make leave = 0, will erase leaves from timetrans
            }
            if (settings.UTExempt)
            {
                _ut = Utils.ConvertToTime(0);
            }
        }

        virtual protected void AdjustExcessHours()
        {
            //... follow the filed overtime
            if (Exceshrs > settings.ApprovedOT)
            {
                _exceshrs = Utils.ConvertToTime(settings.ApprovedOT);
                _otb4shift = Utils.ConvertToTime(0);
            }

        }

        virtual protected void AdjustTardyUT()
        {
            if (Late > settings.MaxTardy)
            {
                _absent = 0.5;
                _late = new TimeSpan();
            }

            if (UT > settings.MaxUT)
            {
                _absent = 0.5;
                _ut = new TimeSpan();
            }
        }

        virtual protected void ComupteHolidayRestdayOT()
        {
            TimeSpan excess = new TimeSpan();
            TimeSpan nd1excess = new TimeSpan();
            TimeSpan nd2excess = new TimeSpan();
         
            if (raw.Work.Out1 > DateTime.MinValue)
            {
                excess += (raw.Work.Out1 - raw.Work.In1);
                nd1excess += (settings.ND1Count(raw.Work.In1, raw.Work.Out1));
                nd2excess += (settings.ND2Count(raw.Work.In1, raw.Work.Out1));
            }
            if (raw.Work.Out2 > DateTime.MinValue)
            {
                excess += (raw.Work.Out2 - raw.Work.In2);
                nd1excess += (settings.ND1Count(raw.Work.In2, raw.Work.Out2));
                nd2excess += (settings.ND2Count(raw.Work.In2, raw.Work.Out2));
            }
            //... check if overtime is greater than the "shift minimum OT"
            if (excess.TotalMinutes >= settings.MinimumOT)
            {
                _exceshrs += excess;
                _nd1ot += nd1excess;
                _nd2ot += nd2excess;
            }

            //... for companies who have breaks during restday or holiday, breaks will be deducted from overtime
            if (settings.CompanyBreaks.Length > 0)
                LessCompanyBreaksDuringRestday();
        }

        virtual protected void LessCompanyBreaksDuringRestday()
        {
            foreach (TransTwo compbreak in settings.CompanyBreaks)
            {
                TimeSpan breakhours = new TimeSpan();

                if (raw.Work.Out1 > DateTime.MinValue && raw.Work.In1 <= compbreak.Out && raw.Work.Out1 >= compbreak.In)
                {
                    DateTime breakin = compbreak.In > raw.Work.In1 ? compbreak.In : raw.Work.In1;
                    DateTime breakout = compbreak.Out < raw.Work.Out1 ? compbreak.Out : raw.Work.Out1;
                    breakhours += (breakout - breakin);
                }
                if (raw.Work.Out2 > DateTime.MinValue && raw.Work.In2 <= compbreak.Out && raw.Work.Out2 >= compbreak.In)
                {
                    DateTime breakin = compbreak.In > raw.Work.In2 ? compbreak.In : raw.Work.In2;
                    DateTime breakout = compbreak.Out < raw.Work.Out2 ? compbreak.Out : raw.Work.Out2;
                    breakhours += (breakout - breakin);
                }

                //... deduct break from overtime
                if (_exceshrs > breakhours)
                    _exceshrs -= breakhours;
                else
                    _exceshrs = new TimeSpan();  // reset to zero

                //... adjust night differenctial if they exceed overtime hours
                if (_nd1ot > _exceshrs)
                    _nd1ot = _exceshrs;

                if (_nd2ot > _exceshrs)
                    _nd2ot = _exceshrs;
            }
        }

           
        /* old codes
        virtual protected void ComupteHolidayRestdayOT()
        {
            TimeSpan excess = new TimeSpan();
            TimeSpan nd1excess = new TimeSpan();
            TimeSpan nd2excess = new TimeSpan();

            //... check if there is field overtime
            if (settings.AuthOTend > DateTime.MinValue)
            {
                if (raw.Trans.Out1 > DateTime.MinValue)
                {
                    //... compute based on the filed overtime and 1st transaction is complete
                    DateTime inOT = raw.Trans.In1 > settings.AuthOTstart ? raw.Trans.In1 : settings.AuthOTstart;
                    DateTime outOT = raw.Trans.Out1 > settings.AuthOTend ? settings.AuthOTend : raw.Trans.Out1;

                    excess += (outOT - inOT);
                    nd1excess += (settings.ND1Count(inOT, outOT));
                    nd2excess += (settings.ND2Count(inOT, outOT));
                }

                //... check if there is field overtime and 2nd transaction is complete
                if (raw.Trans.Out2 > DateTime.MinValue)
                {
                    //... compute based on the filed overtime
                    DateTime inOT = raw.Trans.In2 > settings.AuthOTstart ? raw.Trans.In2 : settings.AuthOTstart;
                    DateTime outOT = raw.Trans.Out2 > settings.AuthOTend ? settings.AuthOTend : raw.Trans.Out2;

                    excess += (outOT - inOT);
                    nd1excess += (settings.ND1Count(inOT, outOT));
                    nd2excess += (settings.ND2Count(inOT, outOT));
                }
            }
            else
            {
                if (raw.Trans.Out1 > DateTime.MinValue)
                {
                    excess += (raw.Trans.Out1 - raw.Trans.In1);
                    nd1excess += (settings.ND1Count(raw.Trans.In1, raw.Trans.Out1 ));
                    nd2excess += (settings.ND2Count(raw.Trans.In1, raw.Trans.Out1 ));
                }
                if (raw.Trans.Out2 > DateTime.MinValue)
                {
                    excess += (raw.Trans.Out2 - raw.Trans.In2);
                    nd1excess += (settings.ND1Count(raw.Trans.In2, raw.Trans.Out2));
                    nd2excess += (settings.ND2Count(raw.Trans.In2, raw.Trans.Out2));
                }
            }

            //... check if overtime is greater than the "shift minimum OT"
            if (excess.TotalMinutes >= settings.MinimumOT)
            {
                _exceshrs += excess;
                _nd1ot += nd1excess;
                _nd2ot += nd2excess;
            }
        }
        */

        virtual protected void ComputeLateBreak()
        {
            if (settings.FlexBreak)
                ComputeLateFlexBreak();
            else
                _latebreak += (Utils.Subtract(raw.Work.In2, raw.Shift.In2));
        }

        virtual protected void ComputeLateFlexBreak()
        {
            _latebreak = Utils.Subtract(raw.Work.In2 - raw.Work.Out1, raw.ShiftBreakhrs);
        }

        virtual protected void ComputeRegHrs()
        {
            
            if (settings.IsDaily)
            {
                //reghrs = shifthrs - late - ut
                _reghrs += (raw.ShiftTotalhrs - _ut - _late);
            }
            else
            {
                _reghrs += (raw.ShiftTotalhrs);
            }
        }

        virtual protected void AdjustRegHrs()
        {

            //_reghrs = (raw.ShiftTotalhrs - _ut - _late); the sp - deducts the rounded late and ut
            //adjust _reghrs only if absent
            if (_absent == 1)
                _reghrs = new TimeSpan(0);
            else if (_absent == 0.5)
            {
                //... temporary hardcode 4 hours for halfday
                _reghrs = Utils.Subtract(_reghrs, new TimeSpan(4, 0, 0) );
            }
        }

        virtual protected void ComputeNDRegHrs()
        {
            _nd1reg += (settings.ND1Count(raw.Work.In1, raw.Work.Out1));
            _nd1reg += (settings.ND1Count(raw.Work.In2, raw.Work.Out2));

            _nd2reg += (settings.ND2Count(raw.Work.In1, raw.Work.Out1));
            _nd2reg += (settings.ND2Count(raw.Work.In2, raw.Work.Out2));
        }

        virtual protected void SetRemarks()
        {
            _remarks = raw.AttErr.ErrMsg;
        }

        virtual protected void SetErrorDesc()
        {
            _errordesc = raw.AttErr.ErrMsg;
            if (_errordesc != "" && !(settings.IsRestday || settings.IsHoliday) )
                _absent = 1;
        }


    }
}