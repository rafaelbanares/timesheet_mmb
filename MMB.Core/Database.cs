﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace MMB.Core
{
    public static class Database
    {
        public static DbProviderFactory ProviderFactory { get { return DbProviderFactories.GetFactory("System.Data.SqlClient"); } }
        public static string ConnectionString { get { return System.Configuration.ConfigurationManager.AppSettings.Get("Connection2"); } }
    }
}
