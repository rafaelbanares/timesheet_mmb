using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for WorkSpan
    /// </summary>

    public class WorkSpan
    {
        TransError _atterr = new TransError();

        TransFour _shift = new TransFour();
        TransFour _trans = new TransFour();
        TransFour _work = new TransFour();
        TransFour _excess = new TransFour();

        public TransFour Shift { get { return _shift; } set { _shift = value; } }
        public TransFour Trans { get { return _trans; } set { _trans = value; } }
        public TransFour Work { get { return _work; } set { _work = value; } }
        public TransFour Excess { get { return _excess; } set { _excess = value; } }
        public TimeSpan ShiftBreakhrs { get { return _shiftbreakhrs; } }
        public TimeSpan ShiftTotalhrs { get { return _shifttotalhrs; } }

        TimeSpan _shiftbreakhrs;
        TimeSpan _shifttotalhrs;
        //TimeSpan _totalflexhrs;

        bool _isrestholiday;
        int _transactions = 0;

        public int Transactions { get { return _transactions; } }
        public TransError AttErr { get { return _atterr; } }
        public TimeSpan TotalWorkHrs { get { return (_work.Out1 - _work.In1) + (_work.Out2 - _work.In2); } }
        public bool IsAbsent { get { return _transactions == 0; } }
        public bool IsHalfDay1 { get { return _work.In1 == _work.Out1; } }
        public bool IsHalfDay2 { get { return _work.In2 == _work.Out2; } }
        public bool IsRestHoliday { get { return _isrestholiday; } set { _isrestholiday = value; } }

        public WorkSpan()
        {
        }

        //... if two transaction and the second transaction is all OT
        //... _transaction must be set to 1 and update excesshrs
        public void AssignFlexiBasedWorkHours(bool flexbreak, DateTime flextimeinlimit)
        {
            //DateTime flextimeoutlimit = flextimeinlimit + _shifttotalhrs + _shiftbreakhrs;
            _work.In1 = _shift.In1 > _trans.In1 ? _shift.In1 : _trans.In1;
            DateTime projectedtimeout = _work.In1 + _shifttotalhrs + _shiftbreakhrs;

            if (_transactions == 1)
            {
                _work.Out2 = _trans.Out1 < projectedtimeout ? _trans.Out1 : projectedtimeout;
                if (flexbreak)
                {
                    //... because only one transaction - get shift defaults
                    _work.Out1 = _shift.Out1;
                    _work.In2 = _shift.In2;
                }
                else
                {
                    _work.Out1 = _shift.Out1 < _trans.Out1 ? _shift.Out1 : _trans.Out1;
                    _work.In2 = _shift.In2 > _trans.In1 ? _shift.In2 : _trans.In1;
                }
            }
            else if (_transactions == 2)
            {
                _work.Out2 = _trans.Out2 < projectedtimeout ? _trans.Out2 : projectedtimeout;
                if (flexbreak)
                {
                    //... base on actual
                    _work.Out1 = _trans.Out1;
                    _work.In2 = _trans.In2;
                }
                else
                {
                    _work.Out1 = _shift.Out1 < _trans.Out1 ? _shift.Out1 : _trans.Out1;
                    _work.In2 = _shift.In2 > _trans.In2 ? _shift.In2 : _trans.In2;
                }
            }

            //... determine excess hours before and after shift
            if (_trans.In1 < _shift.In1)
            {
                _excess.In1 = _trans.In1;
                _excess.Out1 = _shift.In1;
            }
            if (_work.Out2 > _shift.Out2)
            {
                _excess.In2 = _work.Out2;
                _excess.Out2 = _trans.Out2;
            }
        }

        public double AssignRestHolidayWorkHours(System.Data.DataRow[] authorizedOT)
        {
            double approvedOT = 0;
            //... Overtime authorization table
            if (authorizedOT.Length > 0)
            {
                
                for (int i = 0; i < authorizedOT.Length; i++)
                {
                    DateTime otstart = (DateTime)authorizedOT[i]["OTstart"];
                    DateTime otend = (DateTime)authorizedOT[i]["OTend"];
                    approvedOT += Convert.ToDouble(authorizedOT[i]["ApprovedOT"]);

                    if (i == 0)
                    {
                        _work.In1 = otstart > _trans.In1 ? otstart : _trans.In1;
                        _work.Out1 = otend < _trans.Out1 ? otend : _trans.Out1;
                    }
                    else
                    {                        
                        // this should consider if transaction is 1 or 2; for the meantime assume that transacation is only 1
                        _work.In2 = otstart > _trans.In1 ? otstart : _trans.In1;
                        _work.Out2 = otend < _trans.Out1 ? otend : _trans.Out1;
                    }
                }
            }
            else
            {
                _work.In1 = _trans.In1;
                _work.Out1 = _trans.Out1;

                _work.In2 = _trans.In2;
                _work.Out2 = _trans.Out2;
            }
            return approvedOT;
        }

        public void AssignShiftBasedWorkHours(bool flexbreak, bool otb4shift)
        {
            //... work1 actual work on 1st shift
            //... work2 actual work on 2nd shift
            _work.In1 = _shift.In1 > _trans.In1 ? _shift.In1 : _trans.In1;
            _work.Out1 = _shift.Out1 < _trans.Out1 ? _shift.Out1 : _trans.Out1;

            //... if transaction 2 is overtime, set transaction to 1. (4 transactions)
            if (_transactions == 2 && _trans.In2 > _shift.Out2 )
            {
                _transactions = 1;
            }

            //... get break base on shift
            if (_transactions == 1)
            {
                _work.In2 = _shift.In2 > _trans.In1 ? _shift.In2 : _trans.In1;
                _work.Out2 = _shift.Out2 < _trans.Out1 ? _shift.Out2 : _trans.Out1;
            }
            else if (_transactions == 2)
            {
                _work.In2 = _shift.In2 > _trans.In2 ? _shift.In2 : _trans.In2;
                _work.Out2 = _shift.Out2 < _trans.Out2 ? _shift.Out2 : _trans.Out2;
            }

            //... adjust breaks if flexbreak
            if (flexbreak)
            {
                if (_transactions == 1)
                {
                    //... base on shift
                    _work.Out1 = _shift.Out1;
                    _work.In2 = _shift.In2;
                }
                else if (_transactions == 2)
                {
                    //... base on actual
                    _work.Out1 = _trans.Out1;
                    _work.In2 = _trans.In2;
                }
            }

            //... half day shift was encountered
            //... if halfday the shift in and out will be equal
            //... so that if you subtract the two the work hours it will be zero;
            if (_work.In1 > _work.Out1)
                _work.Out1 = _work.In1;

            if (_work.In1 > _work.In2)
                _work.In2 = _work.In1;

            if (_work.In2 > _work.Out2)
            {
                //... all three will be equal to last in (work out 20
                _work.In2 = _work.Out2;
                _work.Out1 = _work.Out2;
            }

            //... determine excess hours before and after shift
            if (otb4shift && _trans.In1 < _shift.In1)
            {
                _excess.In1 = _trans.In1;
                _excess.Out1 = _shift.In1;
            }

            if (_trans.Out1 > _shift.Out2)
            {
                _excess.In2 = _work.Out2;
                _excess.Out2 = _trans.Out1;
            }

            if (_trans.Out2 > _shift.Out2)
            {
                //_excess.In2 = _work.Out2;
                _excess.In2 = _work.Out2 > _trans.In2 ? _work.Out2 : _trans.In2;
                _excess.Out2 = _trans.Out2;
            }
        }

        private void CheckShift()
        {
            _shiftbreakhrs = _shift.In2 - _shift.Out1;
            _shifttotalhrs = (_shift.Out1 - _shift.In1) + (_shift.Out2 - _shift.In2);

            //_shiftbreakhrs = new TimeSpan(0, 0, 0);
            //_shifttotalhrs = new TimeSpan(8, 0, 0);

            //... do not check shift error
            //if (!_isrestholiday)
            //    AttErr.CheckShiftMismatch(_trans.In1, _shift.In1 );
        }

        public void CheckError(TimeSpan variance, string excesstrans)
        {
            if (!AttErr.CheckExcessTrans(excesstrans) && !AttErr.CheckIncomplete(_trans.In1, _trans.Out1))
            {
                //... complete transaction
                _transactions = 1;
                //... do not check variance error because of OT b4 shift
                //AttErr.CheckVariance(_trans.In1, _trans.Out1, variance);
                if (!AttErr.IsError)
                {
                    AttErr.CheckIncomplete(_trans.In2, _trans.Out2);
                    if (!AttErr.IsError)
                    {
                        if (Trans.In2 != DateTime.MinValue)
                        {
                            _transactions = 2;
                            //... do not check variance error because of OT b4 shift
                            //AttErr.CheckVariance(_trans.In2, _trans.Out2, variance);
                        }
                    }
                }
            }

            if (!AttErr.IsError )
                CheckShift();
        }
    }
}