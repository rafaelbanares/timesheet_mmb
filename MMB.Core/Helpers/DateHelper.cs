﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMB.Core
{
    public class DateHelper
    {
        private static string[] DayOfWeeks { get { return new string[] { "Monday", "Tuesday", "Wednessday", "Thursday", "Friday", "Saturday", "Sunday" }; } }

        public static string DecodeDOW(string days)
        {
            string[] dows = DayOfWeeks;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < dows.Length; i++)
            {
                if (days.Contains((i + 1).ToString()))
                {
                    sb.Append("," + dows[i]);
                }
            }
            return sb.ToString().RemoveStart(",");
        }

    }
}
