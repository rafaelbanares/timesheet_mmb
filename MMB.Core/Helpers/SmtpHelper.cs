﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace MMB.Core
{
    [Serializable]
    public struct SmtpInfos
    {
        public string SmtpHost { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public bool IsAnonymous()
        {
            return (UserName == null);
        }

        public bool TestConnect()
        {
            return SmtpHelper.TestSmtp(this);
        }

        public override string ToString()
        {
            var result = "Smtp Server : " + SmtpHost + "; ";
            result += "Port : " + Port + "; ";
            result += "Security : " + (IsAnonymous() ? "Anonymous" : string.Format( "User {0}, Password {1}", UserName, Password))+ "; ";

            return base.ToString();
        }
    }

   

    public static class SmtpHelper
    {
        public static void Send(SmtpInfos connectionInfos, MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Host = connectionInfos.SmtpHost;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Port = connectionInfos.Port;
            client.Credentials = new NetworkCredential(connectionInfos.UserName, connectionInfos.Password);

            client.Send(message);
        }
        

        public static void Send(SmtpInfos connectionInfos, string Sender, string To, string CC, string Subject, string Body, string[] attachments)
        {
            var message = new MailMessage();

            message.From = new MailAddress(Sender);

            string[] recipients = To.Split(';');
            foreach (string recipient in recipients)
            {
                message.To.Add(new MailAddress(recipient));
            }
            if (CC != null)
            {

                string[] CCrecipients = CC.Split(';');
                foreach (string CCrecipient in CCrecipients)
                {
                    message.CC.Add(new MailAddress(CCrecipient));
                }
            }
            if (attachments != null)
            {
                foreach (string attachment in attachments)
                {
                    message.Attachments.Add(new Attachment(attachment));
                }
            }

            message.Subject = Subject;
            if (Body != null)
            {
                message.Body = Body.Replace(Environment.NewLine, "<br>");
            }
            message.IsBodyHtml = true;

            Send(connectionInfos, message);            
        }

        public static bool TestSmtp(SmtpInfos connectionInfos)
        {
            bool isConnectionValid = false;
            TcpClient client = new TcpClient();

            try
            {
                client.Connect(connectionInfos.SmtpHost, connectionInfos.Port);
                isConnectionValid = client.Connected;
                client.Close();
            }
            catch
            {
                // invalid connection
            }
            finally
            {
                client.Close();
            }
            return isConnectionValid;
        }
    }
}
