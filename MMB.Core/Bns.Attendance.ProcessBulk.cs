using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Bns.Attendance
{
    public class AttendanceProcessBulk
    {
        DataSet dsDefault;
        DataSet dsData;
        DBCredential dbCredential;

        public DataSet ProcessData { get { return dsData; } }

        protected AttendanceProcessBulk()
        {
        }

        public AttendanceProcessBulk(DBCredential dbc)
        {
            dbCredential = dbc;
        }

        public void ExecuteBulk()
        {
            dsData = SqlHelper.ExecuteDataset(dbCredential.Connstring, CommandType.StoredProcedure,
                dbCredential.MainDB + ".dbo.usa_AttendanceProcessBulk", new SqlParameter[] {
                                    new SqlParameter("@DBName", dbCredential.DB)
                                    ,new SqlParameter("@CompanyID", dbCredential.CompanyID)
                                });

            if (dsData.Tables.Count > 0)
                Execute();
        }

        public void ExecuteOneTrans(string employeeid, DateTime attendancedate, string shiftcode, string restdaycode, string[] trans)
        {
            dsData = SqlHelper.ExecuteDataset(dbCredential.Connstring, CommandType.StoredProcedure,
                dbCredential.MainDB + ".dbo.usa_AttendanceProcessOneTrans", new SqlParameter[] {
                                    new SqlParameter("@DBName", dbCredential.DB)
                                    ,new SqlParameter("@CompanyID", dbCredential.CompanyID)
                                    ,new SqlParameter("@EmployeeID", employeeid)
                                    ,new SqlParameter("@Date", attendancedate)
                                    ,new SqlParameter("@ShiftCode", shiftcode)
                                    ,new SqlParameter("@RestdayCode", restdaycode)
                                    ,new SqlParameter("@in1",   Utils.DefaultNull( trans[0] ))
                                    ,new SqlParameter("@out1",  Utils.DefaultNull( trans[1] ))
                                    ,new SqlParameter("@in2",   Utils.DefaultNull( trans[2] ))
                                    ,new SqlParameter("@out2",  Utils.DefaultNull( trans[3] ))
                                });
            Execute();
        }


        private void Execute()
        {
            dsDefault = SqlHelper.ExecuteDataset(dbCredential.Connstring, CommandType.StoredProcedure,
                            dbCredential.MainDB + ".dbo.usa_AttendanceDefaults", new SqlParameter[] {
                                    new SqlParameter("@DBName", dbCredential.DB),
                                    new SqlParameter("@CompanyID", dbCredential.CompanyID)
                                  });


            //... dsData structure if table count is greater than zero
            /*
                Table[0] - Attendance
                Table[1] - Leave
                Table[2] - OTAuthorization
                Table[3] - Holiday
                Table[4] - Shift
            */

            DataRelation Att_Leave = new DataRelation("Att_Leave", dsData.Tables[0].Columns["EmployeeID"], dsData.Tables[1].Columns["EmployeeID"], false);

            //... relation on [Parent.EmployeeID Parent.Date] child [Child.EmployeeID Child.OTDate]
            DataRelation Att_OTAuth = new DataRelation("Att_OTAuth",
                new DataColumn[] { dsData.Tables[0].Columns["EmployeeID"], dsData.Tables[0].Columns["Date"] },
                new DataColumn[] { dsData.Tables[2].Columns["EmployeeID"], dsData.Tables[2].Columns["OTDate"] }, false);

            DataRelation Att_Holiday = new DataRelation("Att_Holiday", dsData.Tables[0].Columns["Date"], dsData.Tables[3].Columns["Date"], false);

            DataRelation Att_Shift = new DataRelation("Att_Shift", dsData.Tables[0].Columns["ShiftCode"], dsData.Tables[4].Columns["ShiftCode"], false);

            dsData.Relations.Add(Att_Leave);
            dsData.Relations.Add(Att_OTAuth);
            dsData.Relations.Add(Att_Holiday);
            dsData.Relations.Add(Att_Shift);

            //... needed for batch update
            SqlConnection connection = new SqlConnection(dbCredential.Connstring);
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM " + dbCredential.DB + ".dbo.Attendance WHERE 1=0", connection);
                DataSet dsAttendance = new DataSet();
                adapter.Fill(dsAttendance);

                foreach (DataRow drAttend in dsData.Tables[0].Rows)
                {
                    #region attendance process per record
                    string empid = drAttend["EmployeeID"].ToString();

                    DataRow[] drHoliday = drAttend.GetChildRows("Att_Holiday");
                    DataRow[] drShift = drAttend.GetChildRows("Att_Shift");

                    DateTime currProcessDate = DateTime.Parse(drAttend["Date"].ToString());
                    string processdate = currProcessDate.ToString("MM/dd/yyyy");

                    int dayofweek = (int)currProcessDate.DayOfWeek;
                    if (dayofweek == 0)
                        dayofweek = 7; // change to 7 if sunday

                    string restdaycode = drAttend["RestDayCode"].ToString();

                    bool isrestday = restdaycode.Contains(dayofweek.ToString());
                    bool isholiday = drHoliday.Length > 0;

                    TransFour shift = new TransFour(processdate,
                                                    drShift[0]["in1"].ToString(),
                                                    drShift[0]["out1"].ToString(),
                                                    drShift[0]["in2"].ToString(),
                                                    drShift[0]["out2"].ToString());

                    TransFour trans = new TransFour(
                            drAttend["in1"] == DBNull.Value ? DateTime.MinValue : (DateTime)drAttend["in1"],
                            drAttend["out1"] == DBNull.Value ? DateTime.MinValue : (DateTime)drAttend["out1"],
                            drAttend["in2"] == DBNull.Value ? DateTime.MinValue : (DateTime)drAttend["in2"],
                            drAttend["out2"] == DBNull.Value ? DateTime.MinValue : (DateTime)drAttend["out2"]);

                    //... re-assess absent if no transaction
                    bool isAbsent = (trans.In1 == DateTime.MinValue &&
                                    trans.Out1 == DateTime.MinValue &&
                                    trans.In2 == DateTime.MinValue &&
                                    trans.Out2 == DateTime.MinValue);

                    //... get attendance settings
                    Settings settings = GetAttendanceSettings(drAttend, processdate, isrestday, drShift, drHoliday);
                    settings.FlexTimeInLimit = shift.In1.AddHours(Convert.ToDouble(drShift[0]["maxflexi"]));

                    WorkSpan raw = new WorkSpan();
                    raw.Shift = shift;
                    raw.Trans = trans;
                    raw.IsRestHoliday = (isrestday || isholiday);

                    if (!isAbsent)
                        raw.CheckError(settings.Variance, trans.ExcessTrans);

                    if (!raw.AttErr.IsError)
                        raw.AssignRestHolidayWorkHours(drAttend.GetChildRows("Att_OTAuth"));

                    AttendanceProcess attendance;
                    if (settings.Flexi)
                    {
                        // assign flexi limit here
                        //settings.FlexTimeInLimit = Utils.ConvertToTime(shift[5]); ;
                        attendance = new Attendance_Flexi();
                        attendance.Process(raw, settings);
                    }
                    else
                    {
                        attendance = new Attendance_ShiftBased();
                        attendance.Process(raw, settings);
                    }
                    #endregion

                    #region roundoff ot,ut,tardy
                    string otMins, tardyMins, utMins;

                    //... if restday and no transaction
                    if (isrestday && isAbsent)
                    {
                        otMins = "0.00";
                        tardyMins = "0.00";
                        utMins = "0.00";
                    }
                    else
                    {
                        otMins = Utils.TimeToDecimalRounded(attendance.Exceshrs, Roundoff(RoundTable.Overtime, attendance.tsExcessHrs.Minutes));
                        tardyMins = Utils.TimeToDecimalRounded(attendance.Late, Roundoff(RoundTable.Tardy, attendance.tsLate.Minutes));
                        utMins = Utils.TimeToDecimalRounded(attendance.UT, Roundoff(RoundTable.UT, attendance.tsUT.Minutes));
                    }
                    #endregion

                    #region save attendance
                    DataRow drAtt = dsAttendance.Tables[0].NewRow();

                    drAtt["CompanyID"] = dbCredential.CompanyID;
                    drAtt["EmployeeID"] = empid;
                    drAtt["Date"] = processdate;
                    drAtt["shiftcode"] = drShift[0]["shiftcode"].ToString();
                    drAtt["restdaycode"] = restdaycode;

                    drAtt["in1"] = Utils.DefaultDBNull(raw.Trans.In1);
                    drAtt["out1"] = Utils.DefaultDBNull(raw.Trans.Out1);
                    drAtt["in2"] = Utils.DefaultDBNull(raw.Trans.In2);
                    drAtt["out2"] = Utils.DefaultDBNull(raw.Trans.Out2);

                    drAtt["Late"] = tardyMins;
                    drAtt["UT"] = utMins;
                    drAtt["Absent"] = attendance.Absent;

                    drAtt["Leave"] = settings.LeaveDays;
                    drAtt["Excesshrs"] = otMins;
                    drAtt["ErrorDesc"] = attendance.ErrorDesc;
                    drAtt["Remarks"] = attendance.Remarks;

                    drAtt["Supervisor"] = "";
                    drAtt["OTcode"] = settings.AuthOTcode;                    
                    drAtt["Valid"] = (settings.IsOTb4shift || settings.AuthOTend > DateTime.MinValue);

                    drAtt["ValidOTb4Shift"] = settings.IsOTb4shift;
                    drAtt["First8"] = attendance.Exceshrs > 8 ? 8.00 : attendance.Exceshrs;
                    drAtt["Greater8"] = attendance.Exceshrs > 8 ? attendance.Exceshrs - 8 : 0.00;
                    drAtt["NDot1"] = attendance.ND1ot;
                    drAtt["NDot2"] = attendance.ND2ot;
                    drAtt["Reghrs"] = attendance.Reghrs;

                    drAtt["Regnd1"] = attendance.ND1reg;
                    drAtt["Regnd2"] = attendance.ND2reg;

                    drAtt["ExcusedLate"] = (double)0.00;
                    drAtt["ExcusedUT"] = (double)0.00;
                    drAtt["Leavecode"] = settings.LeaveCode;

                    drAtt["OTAuthStart"] = Utils.DefaultDBNull(settings.AuthOTstart);
                    drAtt["OTAuthEnd"] = Utils.DefaultDBNull(settings.AuthOTend);
                    drAtt["Edited"] = false;

                    drAtt["Processed"] = true;
                    drAtt["LastUpdBy"] = dbCredential.UID;
                    drAtt["LastUpdDate"] = DateTime.Now;

                    dsAttendance.Tables[0].Rows.Add(drAtt);
                    #endregion
                }

                #region batch update - "no insert to attendance being done here", just pure update
                SqlCommand command = new SqlCommand(dbCredential.MainDB + ".dbo.usa_AttendanceUpdateOnly", connection);
                //SqlCommand command = new SqlCommand();
                //command.CommandText = dbCredential.MainDB + ".dbo.usa_AttendanceInsert";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@CompanyID", SqlDbType.Char, 10)).SourceColumn = "CompanyID";
                command.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Char, 15)).SourceColumn = "EmployeeID";
                command.Parameters.Add(new SqlParameter("@Date", SqlDbType.SmallDateTime)).SourceColumn = "Date";
                command.Parameters.Add(new SqlParameter("@shiftcode", SqlDbType.VarChar, 10)).SourceColumn = "shiftcode";
                command.Parameters.Add(new SqlParameter("@restdaycode", SqlDbType.VarChar, 7)).SourceColumn = "restdaycode";
                command.Parameters.Add(new SqlParameter("@in1", SqlDbType.SmallDateTime)).SourceColumn = "in1";
                command.Parameters.Add(new SqlParameter("@out1", SqlDbType.SmallDateTime)).SourceColumn = "out1";
                command.Parameters.Add(new SqlParameter("@in2", SqlDbType.SmallDateTime)).SourceColumn = "in2";
                command.Parameters.Add(new SqlParameter("@out2", SqlDbType.SmallDateTime)).SourceColumn = "out2";
                command.Parameters.Add(new SqlParameter("@Late", SqlDbType.Decimal, 11)).SourceColumn = "Late";
                command.Parameters.Add(new SqlParameter("@UT", SqlDbType.Decimal, 11)).SourceColumn = "UT";
                command.Parameters.Add(new SqlParameter("@Absent", SqlDbType.Decimal, 11)).SourceColumn = "Absent";
                command.Parameters.Add(new SqlParameter("@Leave", SqlDbType.Decimal, 11)).SourceColumn = "Leave";
                command.Parameters.Add(new SqlParameter("@Excesshrs", SqlDbType.Decimal, 11)).SourceColumn = "Excesshrs";
                command.Parameters.Add(new SqlParameter("@ErrorDesc", SqlDbType.VarChar, 100)).SourceColumn = "ErrorDesc";
                command.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.VarChar, 100)).SourceColumn = "Remarks";
                command.Parameters.Add(new SqlParameter("@Supervisor", SqlDbType.Char, 15)).SourceColumn = "Supervisor";
                command.Parameters.Add(new SqlParameter("@OTcode", SqlDbType.Char, 10)).SourceColumn = "OTcode";
                command.Parameters.Add(new SqlParameter("@Valid", SqlDbType.Bit)).SourceColumn = "Valid";
                command.Parameters.Add(new SqlParameter("@ValidOTb4Shift", SqlDbType.Bit)).SourceColumn = "ValidOTb4Shift";
                command.Parameters.Add(new SqlParameter("@First8", SqlDbType.Decimal, 11)).SourceColumn = "First8";
                command.Parameters.Add(new SqlParameter("@Greater8", SqlDbType.Decimal, 11)).SourceColumn = "Greater8";
                command.Parameters.Add(new SqlParameter("@NDot1", SqlDbType.Decimal, 11)).SourceColumn = "NDot1";
                command.Parameters.Add(new SqlParameter("@NDot2", SqlDbType.Decimal, 11)).SourceColumn = "NDot2";
                command.Parameters.Add(new SqlParameter("@Reghrs", SqlDbType.Decimal, 11)).SourceColumn = "Reghrs";
                command.Parameters.Add(new SqlParameter("@Regnd1", SqlDbType.Decimal, 11)).SourceColumn = "Regnd1";
                command.Parameters.Add(new SqlParameter("@Regnd2", SqlDbType.Decimal, 11)).SourceColumn = "Regnd2";
                command.Parameters.Add(new SqlParameter("@ExcusedLate", SqlDbType.Decimal, 11)).SourceColumn = "ExcusedLate";
                command.Parameters.Add(new SqlParameter("@ExcusedUT", SqlDbType.Decimal, 11)).SourceColumn = "ExcusedUT";
                command.Parameters.Add(new SqlParameter("@Leavecode", SqlDbType.Char, 2)).SourceColumn = "Leavecode";
                command.Parameters.Add(new SqlParameter("@OTAuthStart", SqlDbType.SmallDateTime)).SourceColumn = "OTAuthStart";
                command.Parameters.Add(new SqlParameter("@OTAuthEnd", SqlDbType.SmallDateTime)).SourceColumn = "OTAuthEnd";
                command.Parameters.Add(new SqlParameter("@Edited", SqlDbType.Bit)).SourceColumn = "Edited";
                command.Parameters.Add(new SqlParameter("@Processed", SqlDbType.Bit)).SourceColumn = "Processed";
                command.Parameters.Add(new SqlParameter("@LastUpdBy", SqlDbType.Char, 15)).SourceColumn = "LastUpdBy";
                command.Parameters.Add(new SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime)).SourceColumn = "LastUpdDate";
                command.Parameters.Add(new SqlParameter("@DBname", dbCredential.DB));

                adapter.InsertCommand = command;

                //... if single employee disable batch transaction.
                //adapter.UpdateBatchSize = singleemp ? 1 : 0;
                adapter.UpdateBatchSize = dsData.Tables[0].Rows.Count == 1 ? 1 : 0;
                //adapter.UpdateBatchSize = 0;
                command.UpdatedRowSource = UpdateRowSource.None;
                command.CommandTimeout = 0;

                adapter.Update(dsAttendance);
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.ToString()));
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
                #endregion
        }

        private Settings GetAttendanceSettings(DataRow employee, string processdate, bool isrestday, DataRow[] empshift, DataRow[] holiday)
        {
            DataRow company = dsDefault.Tables[0].Rows[0];
            DataRow[] leavetrans = employee.GetChildRows("Att_Leave");
            DataRow[] authorizedOT = employee.GetChildRows("Att_OTAuth");

            Settings settings = new Settings();

            //... this will be based on attendance
            settings.Variance = new TimeSpan(0, int.Parse(company["Variance"].ToString()), 0);
            settings.IsDaily = employee["PayrollType"].ToString() == "D";

            settings.TardyExempt = (bool)employee["TardyExempt"];
            settings.UTExempt = (bool)employee["UTExempt"];
            settings.OTExempt = (bool)employee["OTExempt"];
            settings.NonSwiper = (bool)employee["NonSwiper"];
            settings.IsRestday = isrestday;
            settings.IsHoliday = holiday.Length > 0;

            settings.FlexBreak = (bool)empshift[0]["flexbreak"];
            //settings.IsOTb4shift = (bool)empshift[0]["OtB4Shift"];
            settings.IsOTb4shift = false; // OT b4 shift must be based on filed overtime not on shift
            settings.Flexi = (bool)empshift[0]["Flexi"];
            //settings.FlexTimeInLimit = shift.In1.AddHours(Convert.ToDouble(empshift[0]["maxflexi"]));

            settings.GraceIn1 = DateTime.Parse(processdate + " " + empshift[0]["GraceIn1"].ToString());
            settings.GraceIn2 = DateTime.Parse(processdate + " " + empshift[0]["GraceIn2"].ToString());
            settings.MinimumOT = (int)empshift[0]["MinimumOT"];
            settings.MaxTardy = Convert.ToDouble(empshift[0]["MaxTardy"]);
            settings.MaxUT = Convert.ToDouble(empshift[0]["MaxUT"]);


            settings.ND = new TransFour(processdate,
                                        company["ND1Start"].ToString(),
                                        company["ND1End"].ToString(),
                                        company["ND2Start"].ToString(),
                                        company["ND2End"].ToString());

            settings.CompanyBreaks = TransTwo.GetInOuts(processdate, dsDefault.Tables[4].Rows); //dsDefault.Tables[4] is companybreaks

            DateTime currProcessDate = DateTime.Parse(processdate);

            //... Leave Code Table
            foreach (DataRow drleave in leavetrans)
            {
                if (currProcessDate >= DateTime.Parse(drleave["StartDate"].ToString()) && currProcessDate <= DateTime.Parse(drleave["EndDate"].ToString()))
                {
                    decimal ld = (decimal)leavetrans[0]["Days"];
                    settings.LeaveCode = drleave["LeaveCode"].ToString();
                    settings.LeaveDays = ld >= 1 ? 1 : ld;
                    //settings.LeaveWithPay = (bool)leavetrans["WithPay"];
                    break;
                }
            }

            //... Overtime authorization table
            if (authorizedOT.Length > 0)
            {
                double approvedOT = 0;

                for (int i = 0; i < authorizedOT.Length; i++)
                {
                    //... night shift conflict on OT before shift
                    DateTime shiftin1 = DateTime.Parse(processdate + " " + empshift[0]["in1"].ToString());
                    DateTime otstart = (DateTime)authorizedOT[i]["OTstart"];
                    DateTime otend = (DateTime)authorizedOT[i]["OTend"];

                    //... if filed overtime is less than shift in1, filed ot is otb4shift
                    if (otstart < shiftin1 && shiftin1.Subtract(otstart).Hours < 12)
                    {
                        settings.IsOTb4shift = true;
                    }

                    settings.AuthOTstart = otstart;
                    settings.AuthOTend = otend;
                    approvedOT += Convert.ToDouble(authorizedOT[i]["ApprovedOT"]);

                }
                settings.ApprovedOT = approvedOT;
            }

            //... Assign Holiday R Holiday
            if (holiday.Length > 0)
            {
                //... holiday overtime
                bool islegal = (bool)(holiday[0]["Legal"]);

                if (isrestday && islegal)
                    settings.AuthOTcode = "LGLRST";
                else if (isrestday && !islegal)
                    settings.AuthOTcode = "SPLRST";
                else if (islegal)
                    settings.AuthOTcode = "LGL";
                else if (!islegal)
                    settings.AuthOTcode = "SPL";
            }
            else
            {
                //... non-holiday overtime
                if (isrestday)
                    settings.AuthOTcode = "RST";
                else
                    settings.AuthOTcode = "REG";
            }

            return settings;
        }

        private int Roundoff(RoundTable rt, int origvalue)
        {
            DataTable refTable = dsDefault.Tables[(int)rt];
            DataRow[] dr = refTable.Select(origvalue.ToString() + " >= LO AND " + origvalue.ToString() + " <= UP");
            return dr.Length > 0 ? (int)dr[0]["Rounded"] : origvalue;
        }

        public enum RoundTable
        {
            Tardy = 1,
            UT = 2,
            Overtime = 3
        }

    }
}
