using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for Attendance_ShiftBased
    /// </summary>
    sealed public class Attendance_ShiftBased : AttendanceProcess
    {
        override public void Process(WorkSpan r, Settings s)
        {
            raw = r;
            settings = s;

            SetRemarks();
            SetErrorDesc();
            if (!r.AttErr.IsError)
            {
                if (raw.IsAbsent && settings.IsRestday)
                {
                    //... do nothing for no transaction and restday
                    //... but it is important to save the date on Attendance table
                }
                else if (settings.IsRestday || settings.IsHoliday)
                {
                    ComupteHolidayRestdayOT();
                }
                else
                {
                    //... this process should be in sequence...
                    CheckAbsent();
                    if (!raw.IsAbsent)
                    {
                        //... ot b4 shift is also handled in this method.
                        raw.AssignShiftBasedWorkHours(settings.FlexBreak, settings.IsOTb4shift);

                        if (raw.Work.In1 > settings.GraceIn1)
                            ComputeLateFirstIn();

                        ComputeUTLastOut();
                        ComputeAbsentHalf();
                        ComputeLateBreak();
                        ComputeAbsentHalf();

                        ComputeExcessHours();   // this also computes ND OT
                        ComupteOTBeforeShift(); // this also computes ND OT

                        ComputeRegHrs();
                        ComputeNDRegHrs();

                        ComputeHalfdayLateUT();
                        AdjustTardyUT();
                        AdjustRegHrs();
                        //AdjustExcessHours();
                        CheckExemptions();
                    }
                }
            }
        }

        private void ComputeHalfdayLateUT()
        {
            // if halfday abs, compute late on shiftin2
            if (raw.IsHalfDay1)
                if (raw.Work.In2 > settings.GraceIn2)
                    _late += Utils.Subtract(raw.Work.In2, raw.Shift.In2);

            if (raw.IsHalfDay2)
                _ut += Utils.Subtract(raw.Shift.Out1, raw.Work.Out1);
        }

        override protected void ComputeLateFirstIn()
        {
            //... accumulate late hours
            _late += Utils.Subtract(raw.Work.In1, raw.Shift.In1);
        }

        override protected void ComputeUTLastOut()
        {
            //... accumnulate tardy hours
            _ut += Utils.Subtract(raw.Shift.Out2, raw.Work.Out2);
        }

    }
}