using System;

namespace MMB.Core
{
    /// <summary>
    /// Attendance Processing Utilities
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// gets the time of the passed string
        /// </summary>
        static public string GetTime(string sdt)
        {
            string retval = "";
            if (sdt.Length > 8)
            {
                int pos1 = sdt.IndexOf(" ");
                int pos2 = sdt.IndexOf(":");

                string hour = sdt.Substring(pos1 + 1, pos2 - pos1 - 1);
                string mins = sdt.Substring(pos2 + 1, 2);
                string ampm = sdt.Substring(sdt.Length - 2);

                if (hour.Length < 2)
                    hour = "0" + hour;

                retval = hour + ":" + mins + " " + ampm;
            }
            return retval;
        }

        /// <summary>
        /// Convert time to Datetime, if oldref is greater add 1 to day.
        /// </summary>
        static public string TimeToDateTimeString(string date, string time, string oldref)
        {
            //... i chosed to return string data type because you cant return a nullable datetime
            string retval = "";
            if (time.Length > 4)
            {
                //DateTime dt = DateTime.Parse(date + " " + time);

                //if (oldref.Length > 0)
                //{
                //    DateTime old = DateTime.Parse(oldref);
                //    if (old > dt)
                //        dt = dt.AddHours(24);
                //}
                //else
                //{
                //    if (dt.Hour <= 2)
                //        dt = dt.AddHours(24);
                //}
                //commented by rsb: don't know what this is for

                DateTime dt = DateTime.Parse(date + " " + time);

                retval = dt.ToString();
            }
            return retval;
        }

        /// <summary>
        /// Convert time into a rounded decimal number
        /// </summary>
        static public string TimeToDecimalRounded(double decimaltime)
        {
            int hours = (int)decimaltime;
            int mins = (int)((decimaltime - hours) * 100);

            string retval = hours + "." + ((mins > 9) ? mins.ToString() : "0" + mins);

            return retval;
        }

        /// <summary>
        /// Convert time into a rounded decimal number
        /// </summary>
        static public string TimeToDecimalRounded(double hours, int minutes)
        {
            /*
            //... the .00 is required in order to represent 60.00 as double and not as an integer.
            string mins = ((int)(minutes / 60.00 * 100)).ToString();
            string retval = (int)hours + "." + ((minutes > 9) ? mins : "0" + mins);
            return retval;
            */
            //return ((int)hours + (minutes / 60.00)).ToString("###.##"); 
            return ((int)hours + (minutes / 60.00)).ToString("F2"); 
        }

        /// <summary>
        /// Covert a string into a timespan
        /// </summary>
        static public double ConvertToDouble(TimeSpan t)
        {
            //... the .00 is required in order to represent 60.00 as double and not as an integer.
            return t.Hours + t.Minutes / 60.00;
        }

        /// <summary>
        /// Covert a double into a timespan
        /// </summary>
        static public TimeSpan ConvertToTime(double d)
        {
            return new TimeSpan(0, (int)(d * 60), 0);
        }


        /// <summary>
        /// Covert a string into a timespan
        /// </summary>
        static public TimeSpan ConvertToTime(string s)
        {
            int hour = int.Parse(s.Substring(0, 2));
            int mins = int.Parse(s.Substring(3, 2));
            bool ispm = s.Contains("PM");
            if (ispm && hour < 12)
                hour += 12;
            return new TimeSpan(hour, mins, 0);
        }

        /// <summary>
        /// Returns a timespan within a given base range
        /// </summary>
        static public TimeSpan GetTimeSpent(TimeSpan baseFrom, TimeSpan baseTo, TimeSpan actualFrom, TimeSpan actualTo)
        {
            if (actualTo > baseFrom && actualFrom < baseTo)
            {
                actualFrom = baseFrom > actualFrom ? baseFrom : actualFrom;
                actualTo = baseTo < actualTo ? baseTo : actualTo;
                return actualTo - actualFrom;
            }
            return new TimeSpan();
        }

        /// <summary>
        /// Returns a timespan within a given base range
        /// </summary>
        static public TimeSpan GetTimeSpent(DateTime baseFrom, DateTime baseTo, DateTime actualFrom, DateTime actualTo)
        {
            if (actualTo > baseFrom && actualFrom < baseTo)
            {
                actualFrom = baseFrom > actualFrom ? baseFrom : actualFrom;
                actualTo = baseTo < actualTo ? baseTo : actualTo;
                return actualTo - actualFrom;
            }
            return new TimeSpan();
        }


        /// <summary>
        /// Returns a non negative timespan substraction
        /// </summary>
        static public TimeSpan Subtract(TimeSpan highvalue, TimeSpan lowvalue)
        {
            if (highvalue > lowvalue)
                return highvalue - lowvalue;
            else
                return new TimeSpan();
        }

        /// <summary>
        /// Returns a non negative timespan substraction
        /// </summary>
        static public TimeSpan Subtract(DateTime highvalue, DateTime lowvalue)
        {
            if (highvalue > lowvalue)
                return highvalue - lowvalue;
            else
                return new TimeSpan();
        }

       
        public static object DefaultDBNull(DateTime dt)
        {
            if (dt == DateTime.MinValue)
                return DBNull.Value;
            else
                return dt;
        }

        public static bool DBNullOrEmpty(object s)
        {
            return (Convert.IsDBNull(s) || s.ToString().TrimEnd() == "");
        }

        /// <summary>
        /// Defaults a string to zero to avoid error in conversion
        /// </summary>
        public static decimal DefaultZero(string s)
        {
            if (s.Trim().Length == 0)
                return 0;
            else
                return Convert.ToDecimal(s);
        }
        /// <summary>
        /// Defaults a string to NULL to save date as NULL and not mm/dd/1900
        /// </summary>
        public static string DefaultNull(string s)
        {
            if (s.Length < 4)
                return null;
            else
                return s;
        }

    }
}