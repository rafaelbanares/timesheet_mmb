﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMB.Core
{
    public class DBCredential
    {
        public string Connstring { get; set; }
        public string MainDB { get; set; }
        public string UID { get; set; }
        public string CompanyID { get; set; }
        public string DB { get; set; }

        public DBCredential(string connstring, string maindb, string uid, string companyid, string db)
        {
            Connstring = connstring;
            MainDB = maindb;
            UID = uid;
            CompanyID = companyid;
            DB = db;
        }

    }
}
