using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class ucOTAuth : UserControlUI
{
    private string _OTAuthID;

    public string OTAuthID
    {
        get { return _OTAuthID; }
        set
        {            
            _OTAuthID = value;
            hfOTAuthID.Value = _OTAuthID;
            populateScreenDefaults();
        }
    }
    

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
    }


    private void populateScreenDefaults()
    {
        string id = hfOTAuthID.Value;

        DataRow drOTAuth = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthByID",
                        new SqlParameter[] { 
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@ID", id )                        
                        }).Tables[0].Rows[0];

        lblFullname.Text = drOTAuth["FullName"].ToString();
        txtDateFiled.Text = Tools.ShortDate(drOTAuth["DateFiled"].ToString(), _KioskSession.DateFormat);
        txtOTDate.Text = drOTAuth["OTDate"].ToString();

        txtin1.Text = Tools.GetTime(drOTAuth["OTstart"].ToString());
        txtout1.Text = Tools.GetTime(drOTAuth["OTend"].ToString());

        txtOTHours.Text = drOTAuth["ApprovedOT"].ToString();
        txtReason.Text = drOTAuth["Reason"].ToString();
    }

    private void OTAuthUpdate(string status)
    {
        DateTime in1;
        DateTime out1;
        string othours = "0.00";

        in1 = DateTime.Parse(txtOTDate.Text + " " + txtin1.Text);
        out1 = DateTime.Parse(txtOTDate.Text + " " + txtout1.Text);

        if (in1 > out1)
            out1 = out1.AddHours(24);

        if (txtOTDate.Text.Length > 0 && txtin1.Text.Length > 0 && txtout1.Text.Length > 0)
        {
            TimeSpan ts = out1.Subtract(in1);
            othours = MMB.Core.Utils.TimeToDecimalRounded(ts.Hours, ts.Minutes);
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", hfEmployeeID.Value));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@ID", hfOTAuthID.Value));
        sqlparam.Add(new SqlParameter("@OTDate", txtOTDate.Text));
        sqlparam.Add(new SqlParameter("@OTstart", in1 ));
        sqlparam.Add(new SqlParameter("@OTend ", out1));
        sqlparam.Add(new SqlParameter("@ApprovedOT", othours));
        sqlparam.Add(new SqlParameter("@Reason", txtReason.Text));
        sqlparam.Add(new SqlParameter("@Status", status));

        sqlparam.Add(new SqlParameter("@Stage", "2"));
        sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));

        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdate", sqlparam.ToArray());
    }

    protected void btnApproved_Click(object sender, EventArgs e)
    {
        OTAuthUpdate("Approved");
        ClickUpdate(this, e);
    }
    protected void btnDeclined_Click(object sender, EventArgs e)
    {
        OTAuthUpdate("Declined");
        ClickUpdate(this, e);
    }
    public void Update()
    {
        upAuth.Update();
    }
    public UpdatePanelUpdateMode UpdateMode
    {
        get { return upAuth.UpdateMode; }
        set { upAuth.UpdateMode = value; }
    }

    public event EventHandler ClickUpdate;

    protected void OnClickUpdate(EventArgs e)
    {
        ClickUpdate(this, e);
    }
}
