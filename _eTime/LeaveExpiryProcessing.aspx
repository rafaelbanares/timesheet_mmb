﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true"CodeFile="LeaveExpiryProcessing.aspx.cs" Inherits="LeaveExpiryProcessing" Title="Bisneeds - Timekeeping - Leave Expiry Processing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" runat="Server">

<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Leave Expiry Processing"></asp:Label>
<br /><br /><br />

<table width="300px" border="1">
<tr>
    <td align="right">
        Transaction Date:
    </td>
    <td>
        <asp:TextBox ID="txtTranDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
        <asp:ImageButton ID="ibtxtTranDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
            ToolTip="Click to choose date" OnClientClick="return false;" />

        <ajaxToolkit:MaskedEditValidator id="mevTranDate" runat="server" InvalidValueBlurredMessage="*"
            InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtTranDate" 
            ControlExtender="meeTranDate" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
        </ajaxToolkit:MaskedEditValidator>
    </td>
</tr>
<tr>
    <td align="right">
        Is Year End Process?
    </td>
    <td>
        <asp:RadioButton ID="rbYes" runat="server" CssClass="ControlDefaults" GroupName="yearend" Text="Yes" />&nbsp;
        <asp:RadioButton ID="rbNo" runat="server" CssClass="ControlDefaults" 
            GroupName="yearend" Text="No" Checked="True" />
    </td>
</tr>
<tr>
    <td colspan="2" align="center">
        <br />
        <asp:Button ID="btnSubmit" runat="server" CssClass="ControlDefaults" Text="Process" OnClick="btnSubmit_Click" /><br />
        <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
    </td>
</tr>
</table>

<ajaxToolkit:MaskedEditExtender ID="meeTranDate" runat="server" ErrorTooltipEnabled="True"
    Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
    OnInvalidCssClass="MaskedEditError" TargetControlID="txtTranDate">
</ajaxToolkit:MaskedEditExtender>

<ajaxToolkit:CalendarExtender ID="caleTranDate" runat="server" PopupButtonID="ibtxtTranDate"
    TargetControlID="txtTranDate">
</ajaxToolkit:CalendarExtender>

</asp:Content>
