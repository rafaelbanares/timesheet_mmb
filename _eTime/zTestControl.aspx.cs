using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

public partial class zTestControl : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            populateGrid();
    }

    private void populateGrid()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("f1"));
        dt.Columns.Add(new DataColumn("f2"));
        dt.Columns.Add(new DataColumn("f3"));
        dt.Columns.Add(new DataColumn("f4"));

        for (int i = 0; i < 4; i++)
        {
            DataRow dr = dt.NewRow();
            dr["f1"] = "column1_row" + i.ToString();
            dr["f2"] = "column2_row" + i.ToString();
            dr["f3"] = "column3_row" + i.ToString();
            dr["f4"] = "column4_row" + i.ToString();
            dt.Rows.Add(dr);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
}
