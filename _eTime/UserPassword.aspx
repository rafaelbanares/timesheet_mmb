﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="UserPassword.aspx.cs" Inherits="UserPassword" Title="User and Password Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript">
    function ToggleCheckbox()
    {
        $get('<% =pnlExpireDate.ClientID %>').style.display = $get('<% =chkNeverExpire.ClientID %>').checked ? 'none' : '';
    }
</script>

    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="User and Password Settings"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname, MI"></asp:Label><br />
    <br />
    
    <table>
        <tr>
            <td>
                <asp:CheckBox ID="chkAdmin" runat="server" Text="User is member of Admin or Timekeeper" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkReset" runat="server" Text="Reset password on next logon" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkAccountLocked" runat="server" Text="Account is locked" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkNeverExpire" runat="server" Text="Password never expires"/>
                <br />
                <asp:Panel ID="pnlExpireDate" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblExpiry" runat="server" Text="Password will expire on:"></asp:Label>
                    <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtExpiryDate" runat="server" ImageUrl="~/Graphics/calendar.gif" ToolTip="Click to choose date" OnClientClick="return false;" Width="16px" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
            </td>
        </tr>
    </table>

    <ajaxToolkit:MaskedEditExtender ID="meeExpiryDate" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtExpiryDate">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleExpiryDate" runat="server" PopupButtonID="ibtxtExpiryDate" TargetControlID="txtExpiryDate">
    </ajaxToolkit:CalendarExtender>

</asp:Content>

