﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
public partial class popupDateFilter : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            txtDateStart.Text = _Url["DateStart"];
            txtDateEnd.Text = _Url["DateEnd"];
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (DateTime.Parse(txtDateStart.Text) > DateTime.Parse(txtDateEnd.Text))
            ClientScript.RegisterStartupScript(typeof(Page), "okclicked",
                        "<script language=JavaScript>alert('Date start cannot be greater than date end');</script>");
        else
            ClientScript.RegisterStartupScript(typeof(Page), "okclicked",
                string.Format("<script language=JavaScript>updateparent('{0}','{1}');</script>", txtDateStart.ClientID, txtDateEnd.ClientID));
   
    }
}
