﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

using Microsoft.ApplicationBlocks.Data;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class WorkForSubProj : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            gvMain.Columns[0].HeaderText = _KioskSession.WorkFor;
            gvMain.Columns[1].HeaderText = _KioskSession.WorkForSub;

            BindGrid(false);
        }
        Form.DefaultButton = "";
        //Page.Title = "Bisneeds - Timekeeping - " + lblModule.Text;
    }

    private void BindGrid(bool isAdd)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForSubProjLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });

        if (isAdd)
        {
            DataRow dr = ds.Tables[0].NewRow();
            ds.Tables[0].Rows.InsertAt(dr, 0);

            //ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

            gvMain.DataSource = ds.Tables[0];
            gvMain.EditIndex = 0; // ds.Tables[0].Rows.Count - 1;
            gvMain.DataBind();

            TextBox txtShortDesc = gvMain.Rows[gvMain.EditIndex].FindControl("txtShortDesc") as TextBox;
            txtShortDesc.Attributes["onfocus"] = "javascript:this.select();";
            txtShortDesc.Focus();
        }
        else
        {
            gvMain.DataSource = ds.Tables[0];
            gvMain.DataBind();
        }
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid(false);
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool isUsed = DataBinder.Eval(e.Row.DataItem, "isUsed").ToString() == "1";

            if (e.Row.RowIndex != gvMain.EditIndex)
            {
                (e.Row.FindControl("ibDelete") as ImageButton).Visible = (!isUsed && gvMain.EditIndex == -1);
                (e.Row.FindControl("ibEdit") as ImageButton).Visible = gvMain.EditIndex == -1;

                //rsb
                bool active = bool.Parse(DataBinder.Eval(e.Row.DataItem, "Active").ToString());
                Image imgActive = e.Row.FindControl("imgActive") as Image;
                imgActive.Visible = active;
            }
            else
            {
                string workForID = DataBinder.Eval(e.Row.DataItem, "WorkForID").ToString();
                string workForSubID = DataBinder.Eval(e.Row.DataItem, "WorkForSubID").ToString();

                DropDownList ddlWorkFor = e.Row.FindControl("ddlWorkFor") as DropDownList;
                DataSet dsWorkFor = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                    "SELECT WorkForID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkFor a WHERE CompanyID = @CompanyID " +
                    "   AND EXISTS (SELECT TOP 1 1 FROM " + _KioskSession.DB + ".dbo.WorkForSub b WHERE a.WorkForID = b.WorkForID ) ORDER BY ShortDesc ",
                                new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

                ddlWorkFor.DataSource = dsWorkFor;
                ddlWorkFor.DataBind();

                if (workForID == "")
                    workForID = ddlWorkFor.SelectedItem.Value;

                ddlWorkFor.SelectedValue = ddlWorkFor.Items.FindByValue(workForID).Value;

                DropDownList ddlWorkForSub = e.Row.FindControl("ddlWorkForSub") as DropDownList;
                populateWorkForSub(workForID, workForSubID, ddlWorkForSub);

                if (isUsed)
                {
                    ddlWorkFor.Enabled = false;
                    ddlWorkForSub.Enabled = false;
                    ddlWorkFor.ToolTip = "already used in work distribution";
                    ddlWorkForSub.ToolTip = "already used in work distribution";
                }

                bool active = false;
                bool.TryParse(DataBinder.Eval(e.Row.DataItem, "Active").ToString(), out active);

                CheckBox chkActive = e.Row.FindControl("chkActive") as CheckBox;
                chkActive.Checked = active;
            }
        }
    }
    protected void ddlWorkFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlWorkFor = (DropDownList)sender;
        DropDownList ddlWorkForSub = ddlWorkFor.Parent.FindControl("ddlWorkForSub") as DropDownList;

        populateWorkForSub(ddlWorkFor.SelectedValue, "0", ddlWorkForSub);
    }

    private void populateWorkForSub(string workForID, string workForSubID, DropDownList ddlWorkForSub)
    {
        DataSet dsWorkForSub = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT WorkForSubID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkForSub WHERE CompanyID = @CompanyID AND WorkForID = @WorkForID ORDER BY ShortDesc ",
                        new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@WorkForID", workForID) });

        ddlWorkForSub.DataSource = dsWorkForSub;
        ddlWorkForSub.DataBind();

        if ( ddlWorkForSub.Items.FindByValue(workForSubID) != null) 
            ddlWorkForSub.SelectedValue = ddlWorkForSub.Items.FindByValue(workForSubID).Value;
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["ProjID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForSubProjDelete",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@ProjID", key), 
                new SqlParameter("@DBname", _KioskSession.DB) 
            });

        gvMain.EditIndex = -1;
        BindGrid(false);

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid(false);
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ProjID"].ToString();

        TextBox txtShortDesc = gvMain.Rows[e.RowIndex].FindControl("txtShortDesc") as TextBox;
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;
        DropDownList ddlWorkForSub = gvMain.Rows[e.RowIndex].FindControl("ddlWorkForSub") as DropDownList;
        CheckBox chkActive = gvMain.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@ProjID", key));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@ShortDesc", txtShortDesc.Text));
        sqlparam.Add(new SqlParameter("@Description", txtDescription.Text));
        sqlparam.Add(new SqlParameter("@Active", chkActive.Checked));
        sqlparam.Add(new SqlParameter("@WorkForSubID", ddlWorkForSub.SelectedValue));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

        string errmsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForSubProjInsertUpdate",
            sqlparam.ToArray()).Tables[0].Rows[0]["ErrMsg"].ToString();

        if (errmsg != "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "insertfail", "alert('" + errmsg + "');", true);
        }
        else
        {
            gvMain.EditIndex = -1;
            BindGrid(false);
        }
    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
}
