<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AutoLock.aspx.cs" Inherits="AutoLock" Title="Bisneeds - Timekeeping - AutoLock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="AutoLock Definitions"></asp:Label><br />
    <br />    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" 
                OnRowUpdating="gvMain_RowUpdating" Width="800px" 
                DataKeyNames="CompanyID">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>
                    <asp:TemplateField HeaderText="Attendance<br>Start Date">
                        <ItemStyle Width="100px" />
                        <ItemTemplate>
                            <asp:Literal id="litStartDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "StartDate"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                
                    <asp:TemplateField HeaderText="Attendance<br>End Date">
                        <ItemStyle Width="100px" />
                        <ItemTemplate>
                            <asp:Literal id="litEndDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "EndDate"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Employee<BR>Lock Start">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtEmployee_lock_start" runat="server" Width="72px" Text='<%# Bind("Employee_lock_start") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtEmployee_lock_start" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeEmployee_lock_start" runat="server" TargetControlID="txtEmployee_lock_start"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleEmployee_lock_start" runat="server" TargetControlID="txtEmployee_lock_start" PopupButtonID="ibtxtEmployee_lock_start"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litEmployee_lock_start" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Employee_lock_start"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Employee<BR>Lock End">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtEmployee_lock_end" runat="server" Width="72px" Text='<%# Bind("Employee_lock_end") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtEmployee_lock_end" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeEmployee_lock_end" runat="server" TargetControlID="txtEmployee_lock_end"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleEmployee_lock_end" runat="server" TargetControlID="txtEmployee_lock_end" PopupButtonID="ibtxtEmployee_lock_end"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litEmployee_lock_end" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Employee_lock_end"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Approver<BR>Lock Start">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtApprover_lock_start" runat="server" Width="72px" Text='<%# Bind("Approver_lock_start") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtApprover_lock_start" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeApprover_lock_start" runat="server" TargetControlID="txtApprover_lock_start"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleApprover_lock_start" runat="server" TargetControlID="txtApprover_lock_start" PopupButtonID="ibtxtApprover_lock_start"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litApprover_lock_start" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Approver_lock_start"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Approver<BR>Lock End">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtApprover_lock_end" runat="server" Width="72px" Text='<%# Bind("Approver_lock_end") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtApprover_lock_end" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeApprover_lock_end" runat="server" TargetControlID="txtApprover_lock_end"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleApprover_lock_end" runat="server" TargetControlID="txtApprover_lock_end" PopupButtonID="ibtxtApprover_lock_end"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litApprover_lock_end" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Approver_lock_end"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>            
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>                        
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

