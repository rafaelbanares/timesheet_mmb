<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AdminSummarizeAttendance.aspx.cs" Inherits="AdminSummarizeAttendance" Title="Bisneeds - Timekeeping - Summarize Attendance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial"
        Font-Size="Large" Font-Underline="False" Text="Summarize Attendance"></asp:Label>
    
    <br />
    <br />
    
    <table cellpadding="0" class="TableBorder1" style="width: 250px">
        <tr>
            <td class="DataGridHeaderStyle" colspan="2">
                Transaction Period to Process
            </td>
        </tr>
        <tr>
            <td align="right">
                Transaction From:
            </td>
            <td align="left">
                <asp:TextBox ID="txtTranStart" runat="server" CssClass="TextBoxDate" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                Transaction To:</td>
            <td align="left">
                <asp:TextBox ID="txtTranEnd" runat="server" CssClass="TextBoxDate" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" class="TableBorder1" style="width: 250px">
        <tr>
            <td class="DataGridHeaderStyle" colspan="2">
                Payroll Period
            </td>
        </tr>

        <tr>
            <td align="right">
                Pay Period Start:
            </td>
            <td align="left">
                <asp:TextBox ID="txtPayStart" runat="server" CssClass="TextBoxDate" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                Pay Period End:</td>
            <td align="left">
                <asp:TextBox ID="txtPayEnd" runat="server" CssClass="TextBoxDate" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnSummarize" runat="server" Text="Summarize" OnClick="btnSummarize_Click" /><br />
    <br />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>


    <br />
    <asp:Button ID="btnForceSummarize" runat="server" Text="Force Summarize" 
        OnClick="btnForceSummarize_Click" Visible="False" />
    <asp:HiddenField ID="hfRowID" runat="server" />



</asp:Content>

