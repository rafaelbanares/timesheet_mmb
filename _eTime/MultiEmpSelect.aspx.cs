using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;

public partial class MultiEmpSelect : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");
            body.Attributes.Add("onload", "initialload('" + _Url["ParentEmpsID"] + "');");
            
            //txtEmps.Text = _Url["EmpIDs"];
            BindGrid();

            txtEmps.Style.Add("display", "none");
            chkAll.Attributes.Add("onclick", "progchanged('0');");
            btnOK.Attributes.Add("onclick","updateparent();return false;");
        }
    }

    private DataSet GetData()
    {
        //TextBox txtSearch = Master.FindControl("txtSearch") as TextBox;
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@DisplaySelected", false)
            ,new SqlParameter("@EmpList", "")
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeDeptList", spParams);
        return ds;
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    protected override DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        gvEmp.SelectedIndex = -1;
        return base.SortDataTable(dataTable, isPageIndexChanging);
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkEmp = e.Row.FindControl("chkEmp") as CheckBox;
            Label lblID = e.Row.FindControl("lblID") as Label;

            chkEmp.Checked = (txtEmps.Text).IndexOf(lblID.Text.Trim() + "~") >= 0;
            chkEmp.Attributes.Add("onclick", string.Format("chkEmp('{0}','{1}');", chkEmp.ClientID, lblID.ClientID));

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    //protected void ibGo_Click(object sender, ImageClickEventArgs e)
    //{
    //    BindGrid();
    //}
    //protected void txtSearch_TextChanged(object sender, EventArgs e)
    //{
    //    BindGrid();
    //}

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        DataTable dt = GetData().Tables[0];
        if (chkAll.Checked)
        {            
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sb.Append(dt.Rows[i]["EmployeeID"].ToString().Trim() + "~");
            }

            txtEmps.Text = sb.ToString();
        }
        else
        {
            //... this will uncheck all checkboxes on asynchronus postback. the switch hfProgChanged is in place to avoid this
            if (hfProgChanged.Value == "0")
                txtEmps.Text = "";            
        }

        gvEmp.DataSource = SortDataTable(dt, false);
        gvEmp.DataBind();    
    }


    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

}
