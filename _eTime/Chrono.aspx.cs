﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bns.AttendanceUI;
using Bns.AttUtils;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using BNS.Framework.Encryption;

public partial class Chrono : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        //... temporarily hardcoded values
        string companyid = "EW01";
        string clientdb = "BNSAttendance_EW";
        string masterdb = System.Configuration.ConfigurationManager.AppSettings.Get("MasterDB");

        DataTable  dt =
            SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, masterdb + ".dbo.usa_AttendanceProcessOneTrans",
                new SqlParameter[] {
                    new SqlParameter("@CompanyID", companyid),
                    new SqlParameter("@Date", DateTime.Today),
                    new SqlParameter("@EmployeeID", txtUsername.Text),
                    new SqlParameter("@Password", Crypto.ActionEncrypt(txtPassword.Text) ),
                    new SqlParameter("@Time", DateTime.Now),
                    new SqlParameter("@IO", rbOut.Checked?"O":"I"),
                    new SqlParameter("@DBname",clientdb)
                }).Tables[0];

        string errmsg = dt.Rows[0]["ErrMsg"].ToString();
        if (errmsg.Length > 0)
        {
            lblError.Text = errmsg;
        }
        else
        {
            lblError.Text = "Time " + (rbOut.Checked ? "out" : "in") + " successfully completed";
        }

    }
}
