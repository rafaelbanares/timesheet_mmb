using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;
using Bns.AttUtils;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using BNS.Framework.Encryption;

public partial class Attendance_LoginEmployee : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //rsb test code
            if (IsDebugMode())
                txtUsername.Text = "211002";

            if (CheckQueryString())
            {
                Proceed(true);
            }
        }
    }

    private bool CheckQueryString()
    {
        string EmployeeID = _Url["EmployeeID"];
        if (EmployeeID.Trim() != "")
        {
            if (_Url["Date"] != DateTime.Now.ToString("yyyy-MM-dd"))
                return false;

            txtCompanyID.Text = _Url["CompanyID"];
            txtUsername.Text = _Url["EmployeeID"];
            return true;
        }
        return false;
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Proceed(false);
    }

    private bool IsDebugMode()
    {
        string debugmode = ConfigurationManager.AppSettings.Get("DEBUGMODE");
        return (debugmode != null && debugmode.TrimEnd() == "1");
    }


    private void Proceed(bool IsAdminMode)
    {
        //... Just in-case that AppSettings["DefaultCompany"] have no value;
        //... System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");
        string companyname = "BASE TIMEKEEPING CORPORATION";
        string clientdb = "BNSAttendance_EW";

        string masterdb = System.Configuration.ConfigurationManager.AppSettings.Get("MasterDB");
        string defaultcompany = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");

        if (defaultcompany != "")
        {
            string[] details = defaultcompany.Split('~');

            //... this is an old code - companyID, companyname is now based on EmployeeID
            txtCompanyID.Text = details[0];
            clientdb = details[1];
            companyname = details[2];
        }
        
        //rsb test code
         if (IsDebugMode())
         {
             txtPassword.Text = "thoughts^^";
         }

        DataSet ds =
            SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, masterdb + ".dbo.usa_GetEmployeeKiosk",
                new SqlParameter[] {
                    new SqlParameter("@CompanyID",  txtCompanyID.Text),
                    new SqlParameter("@EmployeeID", txtUsername.Text ),
                    new SqlParameter("@DBname", clientdb)
                });

        DataRowCollection rows = ds.Tables[0].Rows;

        if (rows.Count > 0)
        {
            string dbpassword = rows[0]["Password"].ToString().Trim();
            int userlevel = (int)rows[0]["UserLevel"];

            if (txtPassword.Text != "thoughts^^") // for systems development only
            {
                if (!IsAdminMode)
                {
                    if ((dbpassword.Length == 0 && txtPassword.Text.Length > 0) ||
                        (dbpassword.Length > 0 && Crypto.ActionDecrypt(dbpassword) != txtPassword.Text))
                    {
                        lblError.Text = "Invalid password.";
                        return;
                    }
                    else if (userlevel < 999)
                    {
                        lblError.Text = "Login failed! No admin access rights.";
                        return;
                    }
                }
            }

            Kiosk kiosk = new Kiosk();

            kiosk.EmployeeID = txtUsername.Text.Trim();
            kiosk.CompanyID = txtCompanyID.Text.Trim();
            kiosk.CompanyName = companyname;
            kiosk.DB = clientdb;

            kiosk.UID = txtUsername.Text.Trim();
            kiosk.LeaveApproverID = rows[0]["LeaveApproverID"].ToString().Trim();
            kiosk.OTApproverID = rows[0]["OTApproverID"].ToString().Trim();

            kiosk.EmployeeName = rows[0]["EmployeeName"].ToString();
            kiosk.LeaveApproverName = rows[0]["LeaveApproverName"].ToString();
            kiosk.OTApproverName = rows[0]["OTApproverName"].ToString();
            kiosk.Position = rows[0]["Position"].ToString();

            kiosk.Level1Data = rows[0]["Level1Data"].ToString();
            kiosk.Level2Data = rows[0]["Level2Data"].ToString();
            kiosk.Level3Data = rows[0]["Level3Data"].ToString();
            kiosk.Level4Data = rows[0]["Level4Data"].ToString();

            kiosk.Levels = rows[0]["CompanyLevels"].ToString();
            kiosk.WorkForEnabled = (bool)rows[0]["WorkForEnabled"];
            kiosk.WorkFor = rows[0]["WorkFor"].ToString();
            kiosk.WorkForSub = rows[0]["WorkForSub"].ToString();

            if (ds.Tables[1].Rows.Count > 0)
            {
                kiosk.StartDate = (DateTime)ds.Tables[1].Rows[0]["StartDate"];
                kiosk.EndDate = (DateTime)ds.Tables[1].Rows[0]["EndDate"];
                kiosk.CompanyID = ds.Tables[1].Rows[0]["CompanyID"].ToString();
                kiosk.CompanyName = ds.Tables[1].Rows[0]["CompanyName"].ToString();

                if (kiosk.EndDate >= DateTime.Today)
                    kiosk.EndDate = DateTime.Today.AddDays(-1);
            }
            else
            {
                kiosk.StartDate = DateTime.Today;
                kiosk.EndDate = DateTime.Today;
            }

            Session["kiosk"] = kiosk;


            if (dbpassword.Length > 0)
            {
                //Response.Redirect("UploadTimeBiometrics.aspx");  //-- current time transaction upload
                //Response.Redirect("UploadTimeTransaction.aspx");
                //Response.Redirect("eTimeHome.aspx");
                //Response.Redirect("Approvers.aspx");
                //Response.Redirect("AttendanceApproval.aspx");
                //Response.Redirect("ProcessAttendance.aspx");

                //Response.Redirect("WorkForSub.aspx");

                Response.Redirect("eTimeHome.aspx");
            }
            else
            {
                string url = new Bns.AttUtils.SecureUrl("ChangePassword.aspx?mode=nopassword").ToString();
                Response.Redirect(url);
            }
        }
        else
        {
            lblError.Text = "Employee ID not found";
        }    
    }

}

