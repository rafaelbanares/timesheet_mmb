using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;

public partial class AttendanceApproval : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();

            BindGrid();
        }
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }
    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            ,new SqlParameter("@StartDate", txtDateStart.Text )
            ,new SqlParameter("@EndDate", txtDateEnd.Text )
            ,new SqlParameter("@Filter", ddlShow.SelectedItem.Value)
        };
        
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceApproval", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblorestday = e.Row.FindControl("lblorestday") as Label;
            lblorestday.ToolTip = Tools.ShowRestdays(lblorestday.Text);

            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                Label lblreason = e.Row.FindControl("lblreason") as Label;
                string reason = lblreason.Text;
                if (reason.Length > 15)
                {
                    lblreason.ToolTip = reason;
                    lblreason.Text = reason.Substring(0, 15) + "...";
                }

                Literal litIn1 = e.Row.FindControl("litIn1") as Literal;
                Literal litOut1 = e.Row.FindControl("litOut1") as Literal;
                Literal litNewIn1 = e.Row.FindControl("litNewIn1") as Literal;
                Literal litNewOut1 = e.Row.FindControl("litNewOut1") as Literal;

                string redfont = @"<font color='red'>{0}</font>";
                string grayfont = @"<font color='gray'>{0}</font>";

                litNewIn1.Text = string.Format(litIn1.Text != litNewIn1.Text? redfont : grayfont, litNewIn1.Text);
                litNewOut1.Text = string.Format(litOut1.Text != litNewOut1.Text? redfont: grayfont, litNewOut1.Text);
            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Literal litEmployeeID = gvEmp.Rows[e.RowIndex].FindControl("litEmployeeID") as Literal;
        Literal litDate = gvEmp.Rows[e.RowIndex].FindControl("litDate") as Literal;
        Label lbloshift = gvEmp.Rows[e.RowIndex].FindControl("lbloshift") as Label;
        Label lblorestday = gvEmp.Rows[e.RowIndex].FindControl("lblorestday") as Label;
        Literal litIn1 = gvEmp.Rows[e.RowIndex].FindControl("litIn1") as Literal;
        Literal litOut1 = gvEmp.Rows[e.RowIndex].FindControl("litOut1") as Literal;
        TextBox tbin1 = gvEmp.Rows[e.RowIndex].FindControl("tbin1") as TextBox;
        TextBox tbout1 = gvEmp.Rows[e.RowIndex].FindControl("tbout1") as TextBox;
        TextBox tbreason = gvEmp.Rows[e.RowIndex].FindControl("tbreason") as TextBox;

        string in1 = Utils.TimeToDateTimeString(litDate.Text, litIn1.Text, "");
        string out1 = Utils.TimeToDateTimeString(litDate.Text, litOut1.Text, in1);
        string newin1 = Utils.TimeToDateTimeString(litDate.Text, tbin1.Text, "");
        string newout1 = Utils.TimeToDateTimeString(litDate.Text, tbout1.Text, newin1);

        if (in1 != newin1 || out1 != newout1)
        {
            List<SqlParameter> sqlparam = new List<SqlParameter>();

            sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
            sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
            sqlparam.Add(new SqlParameter("@Date", litDate.Text));
            sqlparam.Add(new SqlParameter("@orig_shiftcode", lbloshift.Text));
            sqlparam.Add(new SqlParameter("@orig_restcode", lblorestday.Text));
            sqlparam.Add(new SqlParameter("@orig_in1", Tools.DefaultNull(in1)));
            sqlparam.Add(new SqlParameter("@orig_out1", Tools.DefaultNull(out1)));
            sqlparam.Add(new SqlParameter("@orig_in2", null));     //...temporarily not used
            sqlparam.Add(new SqlParameter("@orig_out2", null));     //...temporarily not used
            sqlparam.Add(new SqlParameter("@new_in1", Tools.DefaultNull(newin1)));
            sqlparam.Add(new SqlParameter("@new_out1", Tools.DefaultNull(newout1)));
            sqlparam.Add(new SqlParameter("@new_in2", null));       //...temporarily not used
            sqlparam.Add(new SqlParameter("@new_out2", null));      //...temporarily not used
            sqlparam.Add(new SqlParameter("@reason", tbreason.Text));
            sqlparam.Add(new SqlParameter("@status", "Approved"));
            sqlparam.Add(new SqlParameter("@stage", "2"));
            sqlparam.Add(new SqlParameter("@approvedby", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@lastupdby", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@lastupddate", DateTime.Now));
            sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceApprovalUpdateInsert", sqlparam.ToArray());
        }

        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void ddlShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
}
