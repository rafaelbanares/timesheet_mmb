﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class EmployeeHeader : UserControlUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!Page.IsPostBack)
            PopulateScreenDefaults();
    }


    public void PopulateScreenDefaults()
    {
        SecureUrl _Url = new SecureUrl(Request.Url.PathAndQuery);
        string empid = _Url["EmpID"].Trim();
        string empname = _Url["EmpName"].Trim();


        lblFullname.Text = empname + " (" + empid + ")";

        SecureUrl secureUrl = new SecureUrl(string.Format("EmployeeInfo.aspx?CompID={0}&EmpID={1}", _KioskSession.CompanyID, empid));
        lbEmpInfo.Attributes.Add("onclick", "return openEmpInfo('" + secureUrl.ToString() + "');");
    }
}
