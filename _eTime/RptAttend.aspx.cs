//.... check work distribution must not allow idle time 

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using Microsoft.ApplicationBlocks.Data;
using BNS.Attendance.DataObjects;

public partial class RptAttend : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            LoadReports();
            updateurl();
        }
    }

    private void LoadReports()
    {
        lblClient.Text = _KioskSession.WorkFor +":";
        lblBrand.Text = _KioskSession.WorkForSub + ":";
        populateWorkFor();

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                                MainDB + ".dbo.usa_GetReportsPerType", new SqlParameter[] { new SqlParameter("@reportType", "R") });

        grvQuarterly.DataSource = ds.Tables[0];
        grvQuarterly.DataBind();

        DataSet dsEmployee = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT EmployeeID, FullName FROM " + _KioskSession.DB + ".dbo.Employee WHERE CompanyID = @CompanyID ORDER BY FullName ",
                new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlEmployee.DataSource = dsEmployee.Tables[0];
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem("All", ""));
    }
    
    protected void grvQuarterly_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    #region SaveReportParametersToSession
    private void SaveReportParametersToSession(string reportID)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                                       "Select CRName, CRTitle From " + MainDB + ".dbo.Reports Where ReportID = @reportID ",
                                       new SqlParameter[] { new SqlParameter("@reportID", reportID) });

        if (ds.Tables[0].Rows.Count == 0) throw new Exception("Invalid Report");

        ReportParameters oRptParam = new ReportParameters();

        oRptParam.DB = _KioskSession.DB;

        //oRptParam.Paycode = ddlPaycode.SelectedValue;
        //oRptParam.ApplicableMonth = Convert.ToInt32(ddlMonth.SelectedValue);
        //oRptParam.ApplicableYear = Convert.ToInt32(ddlYear.SelectedValue);

        string[] level = hfLevels.Value.Split('~');
        oRptParam.Level1 = level[0];
        oRptParam.Level2 = level[1];
        oRptParam.Level3 = level[2];
        oRptParam.Level4 = level[3];
        oRptParam.WorkFor = ddlWorkFor.SelectedItem.Value;
        oRptParam.WorkForSub = ddlWorkForSub.SelectedItem.Value;
        oRptParam.ProjID = ddlProject.SelectedItem.Value;

        oRptParam.Export = chkExcel.Checked ? "excel" : "";
        oRptParam.AllCompanies = chkIncludeAll.Checked;

        oRptParam.TranStart = txtDateStart.Text;
        oRptParam.TranEnd = txtDateEnd.Text;

        oRptParam.EmployeeID = ddlEmployee.SelectedItem.Value;
        oRptParam.ReportName = ds.Tables[0].Rows[0]["CRName"].ToString();
        oRptParam.CompanyName = _KioskSession.CompanyName;
        oRptParam.ReportTitle = ds.Tables[0].Rows[0]["CRTitle"].ToString();
        oRptParam.ReportSubTitle1 = "Transaction period from " + txtDateStart.Text + " to " + txtDateEnd.Text;
        oRptParam.GroupBy1 = ddlGroupBy.SelectedValue;
        oRptParam.IsExcel = chkExcel.Checked;
        
        
        oRptParam.SortBy = ddlSortBy.SelectedValue;
        Session["ReportParameters"] = oRptParam;
    }
    private bool ValidateParameters()
    {
        return true;
    }
    #endregion

    protected void lnkReport_Click(object sender, EventArgs e)
    {
        if (!ValidateParameters()) return;

        Control pressed = (Control)sender;
        HtmlInputHidden txtRptID = (HtmlInputHidden)pressed.Parent.Parent.FindControl("hdnID");
        if (txtRptID != null)
        {
            SaveReportParametersToSession(txtRptID.Value);
            SecureUrl secureUrl = new SecureUrl(string.Format("{0}?rpt={1}", "./Viewer/Viewer.aspx", txtRptID.Value));

            ClientScript.RegisterStartupScript(typeof(Page), "open", Bns.AttUtils.Tools.WindowPopup(secureUrl.ToString(), ""), false);
        }
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        lnkLevel.Attributes.Remove("onclick");
        lnkLevel.Attributes.Add("onclick", "openwindow(); return false;");
        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    protected void ddlWorkFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        populateWorkForSub();
    }

    protected void ddlWorkForSub_SelectedIndexChanged(object sender, EventArgs e)
    {
        populateProject();
    }


    private void populateProject()
    {
        string workForSubID = ddlWorkForSub.SelectedValue;

        if (workForSubID == "0")   //idle time
        {
            ddlProject.Items.Clear();
            ddlProject.Items.Add(new ListItem("Idle Time", "0"));
        }
        else if (workForSubID == "-1") // All
        {
            ddlProject.Items.Clear();
            ddlProject.Items.Insert(0, new ListItem("All", "-1"));
        }
        else
        {
            ddlProject.Items.Clear();

            DataSet dsProj = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                "SELECT projID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkForSubProj WHERE CompanyID = @CompanyID AND WorkForSubID = @WorkForSubID  ORDER BY ShortDesc ",
                            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@WorkForSubID", workForSubID) });

            ddlProject.DataSource = dsProj;
            ddlProject.DataBind();

            //... No idle time if client is already specified (work distrib functionality)
            //ddlProject.Items.Insert(0, new ListItem("Idle Time", "0"));
            ddlProject.Items.Insert(0, new ListItem("All", "-1"));

        }  
        ddlProject.SelectedIndex = 0;
    }

    private void populateWorkForSub()
    {
        string workForID = ddlWorkFor.SelectedValue;

        if (workForID == "0")   //idle time
        {
            ddlWorkForSub.Items.Clear();
            ddlWorkForSub.Items.Add(new ListItem("Idle Time", "0"));
        }
        else if (workForID == "-1") // All
        {
            ddlWorkForSub.Items.Clear();
            ddlWorkForSub.Items.Insert(0, new ListItem("All", "-1"));
        }
        else
        {
            ddlWorkForSub.Items.Clear();
            DataSet dsWorkForSub = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                "SELECT WorkForSubID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkForSub WHERE CompanyID = @CompanyID AND WorkForID = @WorkForID ORDER BY ShortDesc ",
                            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@WorkForID", workForID) });

            ddlWorkForSub.DataSource = dsWorkForSub;
            ddlWorkForSub.DataBind();

            //... No idle time if client is already specified (work distrib functionality)
            //ddlWorkForSub.Items.Insert(0, new ListItem("Idle Time", "0"));
            ddlWorkForSub.Items.Insert(0, new ListItem("All", "-1"));

        }

        ddlWorkForSub.SelectedIndex = 0;
        populateProject();
    }

    private void populateWorkFor()
    {
        DataSet dsWorkFor = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT WorkForID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkFor WHERE CompanyID = @CompanyID ORDER BY ShortDesc ",
                        new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlWorkFor.DataSource = dsWorkFor;
        ddlWorkFor.DataBind();
        ddlWorkFor.Items.Insert(0, new ListItem("Idle Time", "0"));
        ddlWorkFor.Items.Insert(0, new ListItem("All", "-1"));

        populateWorkForSub();
    }

}
