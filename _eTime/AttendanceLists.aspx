<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AttendanceLists.aspx.cs" Inherits="AttendanceLists" Title="Bisneeds - Timekeeping - Attendance Lists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function EndRequestHandler(sender, args) {
   if (args.get_error() == undefined)
    {
        var action = $get("<%=hfUpdate.ClientID%>").value;
        if (action)
        {            
            popupWindow=wopen(action,"popupWindow",800,550);
        }
    }
   else
       alert('There was an error' + args.get_error().message);
}
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;
    
    closechild();
}
function closechild()
{
    $get("<%=ibGo.ClientID%>").click();
    closewindow(popupWindow);
}

</script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Attendance Admin"></asp:Label><br />
            <br />
            <table width="100%">
              <tr>
                <td align="right" style="width: 80px">
                    <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click" ValidationGroup="emplist" /></td>
              </tr>
            </table>               
        </td>
        <td align="right">
            <table>
                <tr>
                    <td>
                        Attendance
                        From:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                            ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td>
                        To:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                            ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                        </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Show:&nbsp;<asp:DropDownList id="ddlShow" runat="server" 
                            CssClass="ControlDefaults" 
                            OnSelectedIndexChanged="ddlShow_SelectedIndexChanged">
                            <asp:ListItem Value="all">All Transactions</asp:ListItem>
                            <asp:ListItem Value="error" Selected="True">Erroneous Transactions</asp:ListItem>
                            <asp:ListItem Value="notrans">Absent/No Transaction</asp:ListItem>
                        </asp:DropDownList></td>
                    <td align="left">
                        <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                            ToolTip="Go" /></td>
                </tr>
            </table>
        </td>
        <td align="center">
            <table>
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lnkClear" runat="server" ToolTip="Clear Selection" 
                            onclick="lnkClear_Click">clear</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                            Width="279px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20"
        Width="100%" AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging">
        <Columns>

            <asp:TemplateField HeaderText="Emp.ID" SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal id="litEmpID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName" >
                <ItemTemplate>
                    <asp:Literal id="litName" runat="server" Text='<%# Bind("Fullname") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Attendance&lt;br&gt;Date" SortExpression="Date">
                <ItemTemplate>
                    <asp:Literal id="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Date"), "MM/dd/yyyy") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="ShiftIn" HeaderText="Shift&lt;br&gt;in" DataFormatString="{0:HH:mm tt}" HtmlEncode="False" />
            <asp:BoundField DataField="ShiftOut" HeaderText="Shift&lt;br&gt;out" DataFormatString="{0:HH:mm tt}" HtmlEncode="False" />
            <asp:BoundField DataField="in1" HeaderText="Time&lt;br&gt;in" DataFormatString="{0:hh:mm tt}" HtmlEncode="False" />
            <asp:BoundField DataField="out1" HeaderText="Time&lt;br&gt;out" DataFormatString="{0:hh:mm tt}" HtmlEncode="False" />
            
            <asp:BoundField DataField="Absent" HeaderText="Absent&lt;br&gt;days" HtmlEncode="False" />
            <asp:BoundField DataField="Leave" HeaderText="Leave&lt;br&gt;days" HtmlEncode="False" />
            <asp:BoundField DataField="Late" HeaderText="Late&lt;br&gt;hours" HtmlEncode="False" />
            <asp:BoundField DataField="UT" HeaderText="UT&lt;br&gt;hours" HtmlEncode="False" />
            
            <asp:BoundField DataField="RegHrs" HeaderText="Regular&lt;br&gt;hours" HtmlEncode="False" />
            <asp:BoundField DataField="ExcessHrs" HeaderText="Excess&lt;br&gt;hours" HtmlEncode="False" />
            <asp:BoundField DataField="ErrorDesc" HeaderText="Error&lt;br&gt;Remarks" HtmlEncode="False" />
            <asp:TemplateField HeaderText="More">
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit.gif" OnClick="ibEdit_Click" ToolTip="Edit" />                        
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <asp:HiddenField ID="hfUpdate" runat="server" />
        <asp:HiddenField ID="hfLevels" runat="server" Value="~~~" />
        <asp:HiddenField ID="hfURL" runat="server" />
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="ddlShow"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="btnRefresh"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibSearch"></asp:AsyncPostBackTrigger>
    </Triggers>
    
</asp:UpdatePanel>

    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    
    <table>
        <tr align='left'>
            <td><strong>Legend:</strong></td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_Restday%>"></td><td style="width:120px">- Restday</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_Holiday%>"></td><td style="width:120px">- Holiday</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_RstHoliday%>"></td><td style="width:120px">- Restday/Holiday</td>
        </tr>
        <tr>
            <td colspan='7'>
            </td>
        </tr>
        <tr align='left'>
            <td></td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_LeaveWhole%>"></td><td>- Wholeday Leave</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_LeaveHalf%>"></td><td>- Partial Leave</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_Absent%>"></td><td>- Absent</td>
        </tr>
    </table>

    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" style="display:none" />
    
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart" PopupPosition="BottomRight">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd" PopupPosition="BottomRight">
    </ajaxToolkit:CalendarExtender>

</asp:Content>
