using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class Shift : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ShiftLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ShiftLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@IsBlankData", true), new SqlParameter("@DBname", _KioskSession.DB) });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvMain.DataBind();
        hfIsAdd.Value = "1";
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;

            bool viewmode = (ibDelete != null);

            bool flexi = bool.Parse(DataBinder.Eval(e.Row.DataItem, "flexi").ToString());
            bool flexbreak = bool.Parse(DataBinder.Eval(e.Row.DataItem, "flexbreak").ToString());
            bool otb4shift = bool.Parse(DataBinder.Eval(e.Row.DataItem, "otb4shift").ToString());

            if (viewmode)
            {
                Image imgFlexi = e.Row.FindControl("imgFlexi") as Image;
                Image imgFlexBreak = e.Row.FindControl("imgFlexBreak") as Image;
                Image imgOtB4Shift = e.Row.FindControl("imgOtB4Shift") as Image;

                imgFlexi.Visible = flexi;
                imgFlexBreak.Visible = flexbreak;
                imgOtB4Shift.Visible = otb4shift;

                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
            }
            else
            {
                //... edit mode
                CheckBox chkFlexi = e.Row.FindControl("chkFlexi") as CheckBox;
                CheckBox chkFlexBreak = e.Row.FindControl("chkFlexBreak") as CheckBox;
                CheckBox chkOtB4Shift = e.Row.FindControl("chkOtB4Shift") as CheckBox;

                chkFlexi.Checked = flexi;
                chkFlexBreak.Checked = flexbreak;
                chkOtB4Shift.Checked = otb4shift;
            }

        }
    }

    
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["ShiftCode"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ShiftDelete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@ShiftCode", key), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ShiftCode"].ToString();
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;

        TextBox txtin1 = gvMain.Rows[e.RowIndex].FindControl("txtin1") as TextBox;
        TextBox txtout1 = gvMain.Rows[e.RowIndex].FindControl("txtout1") as TextBox;
        TextBox txtin2 = gvMain.Rows[e.RowIndex].FindControl("txtin2") as TextBox;
        TextBox txtout2 = gvMain.Rows[e.RowIndex].FindControl("txtout2") as TextBox;

        TextBox txtgracein1 = gvMain.Rows[e.RowIndex].FindControl("txtgracein1") as TextBox;
        TextBox txtgracein2 = gvMain.Rows[e.RowIndex].FindControl("txtgracein2") as TextBox;

        TextBox txtminot = gvMain.Rows[e.RowIndex].FindControl("txtminot") as TextBox;
        TextBox txtmaxtardy = gvMain.Rows[e.RowIndex].FindControl("txtmaxtardy") as TextBox;
        TextBox txtmaxut = gvMain.Rows[e.RowIndex].FindControl("txtmaxut") as TextBox;
        TextBox txtmaxflexi = gvMain.Rows[e.RowIndex].FindControl("txtmaxflexi") as TextBox;

        CheckBox chkFlexi = gvMain.Rows[e.RowIndex].FindControl("chkFlexi") as CheckBox;
        CheckBox chkFlexBreak = gvMain.Rows[e.RowIndex].FindControl("chkFlexBreak") as CheckBox;
        CheckBox chkOtB4Shift = gvMain.Rows[e.RowIndex].FindControl("chkOtB4Shift") as CheckBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));        
        sqlparam.Add(new SqlParameter("@Description", txtDescription.Text));

        sqlparam.Add(new SqlParameter("@in1", txtin1.Text));
        sqlparam.Add(new SqlParameter("@out1", txtout1.Text));
        sqlparam.Add(new SqlParameter("@in2", txtin2.Text));
        sqlparam.Add(new SqlParameter("@out2", txtout2.Text));

        sqlparam.Add(new SqlParameter("@gracein1", txtgracein1.Text));
        sqlparam.Add(new SqlParameter("@gracein2", txtgracein2.Text));
        sqlparam.Add(new SqlParameter("@minimumot", txtminot.Text));
        sqlparam.Add(new SqlParameter("@maxtardy", txtmaxtardy.Text));
        sqlparam.Add(new SqlParameter("@maxut", txtmaxut.Text));
        sqlparam.Add(new SqlParameter("@maxflexi ", txtmaxflexi.Text));

        sqlparam.Add(new SqlParameter("@flexi", chkFlexi.Checked ));
        sqlparam.Add(new SqlParameter("@flexbreak", chkFlexBreak.Checked ));
        sqlparam.Add(new SqlParameter("@otb4shift", chkOtB4Shift.Checked ));

        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (hfIsAdd.Value == "0")
        {
            sqlparam.Add(new SqlParameter("@ShiftCode", key));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ShiftUpdate", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ShiftInsert", sqlparam.ToArray());
        }

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
}