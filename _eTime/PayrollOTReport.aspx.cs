﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using Bns.AttendanceUI;
using Microsoft.ApplicationBlocks.Data;

public partial class PayrollOTReport : KioskPageUI
{
    private void Page_Load(object sender, System.EventArgs e)
    {
        base.GetUserSession();
        Response.ContentType = "application/ms-excel";
        Response.AddHeader("Content-Disposition", "inline;filename=rawdata.xls");
        Response.Buffer = true;
        this.EnableViewState = false;

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, "rat_AttendanceSummaryLoweOT");
        //new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        DataGrid dg = new DataGrid();
        dg.DataSource = ds.Tables[0];
        dg.AllowPaging = false;
        dg.DataBind();
        dg.RenderControl(htw);

        Response.Write(sw.ToString());
        Response.End();
    }
}