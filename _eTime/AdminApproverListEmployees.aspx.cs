﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class AdminApproverListEmployees : KioskPageUI
{
    string _ApprpoverID;

    protected void Page_Load(object sender, EventArgs e)
    {
        _ApprpoverID = _Url["ApproverID"];

        base.GetUserSession();

        if (!IsPostBack)
        {
            PopulateApprover();
            BindGrid();
        }
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@ApproverID", _ApprpoverID)
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproversMembers", spParams);
        return ds;
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;

            //... Temporarily disabled for multi companies, because approvers and members may come from different companies
            ibEdit.Visible = false;

            /*
            string empid = DataBinder.Eval(e.Row.DataItem, "EmployeeID").ToString();
            SecureUrl urlAppr = new SecureUrl(string.Format(@"popupApprover.aspx?hfID={0}&lbID={1}", hfApprID.ClientID, hfEmpID.ClientID));

            ibEdit.Attributes.Add("onclick", string.Format("openSelectEmp('{0}','{1}');return false;", empid, urlAppr));

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            */


        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    private void PopulateApprover()
    {
        SqlParameter[] spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@ApproverID", _ApprpoverID)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproversGetByApprover1", spParams);
        DataRowCollection dr = ds.Tables[0].Rows;

        if (dr.Count > 0)
        {
            lblApprover1.Text = dr[0]["Approver1Name"].ToString();
            lblApprover2.Text = dr[0]["Approver2Name"].ToString();
            lblApprover3.Text = dr[0]["Approver3Name"].ToString();
        }
        else
        {
            lblApprover1.Text = "None";
            lblApprover2.Text = "None";
            lblApprover3.Text = "None";
        }
    }
    protected void btnRefreshGrid_Click(object sender, EventArgs e)
    {
        if (hfEmpID.Value != "")
        {
            SqlParameter[] spParams = new SqlParameter[] {
                new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", hfEmpID.Value)
                ,new SqlParameter("@ApproverID", hfApprID.Value)
                ,new SqlParameter("@DBname", _KioskSession.DB)
                ,new SqlParameter("@LastUpdBy", _KioskSession.UID)
                ,new SqlParameter("@LastUpdDate", DateTime.Now)
            };

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_EmployeeApproverUpdate", spParams);
            BindGrid();

            hfEmpID.Value = ""; //reset selection
        }
    }
}
