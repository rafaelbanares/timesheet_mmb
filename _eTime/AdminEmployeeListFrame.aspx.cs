using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class AdminEmployeeListFrame : KioskPageUI
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        hfUpdate.Value = "";
        base.GetUserSession();
        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            hfModule.Value = _Url["module"];
            body.Attributes.Add("onload", "load();");

            //body.Attributes.Add("onfocus", "if (window.popupWindow) {window.popupWindow.close()}");
            //body.Attributes.Add("onfocus", "closewindow();");

            BindGrid();

            SecureUrl secureUrl = new SecureUrl("BlankPageIframe.aspx?moduledesc=" + _Url["moduledesc"]);
            hfUpdate.Value = secureUrl.ToString();

            hlAddEmployee.Visible = hfModule.Value.ToLower().Contains("empmasattendance");
            updateurl();
        }
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        lnkLevel.Attributes.Remove("onclick");
        lnkLevel.Attributes.Add("onclick", "openwindow(); return false;");
        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    private bool OTExemptFilter()
    {
        return _Url["otexemptfilter"].TrimEnd() == "1";
    }

    private bool ShowActive()
    {
        return (ddlFilterStat.SelectedItem.Value.ToLower().TrimEnd() == "active");
    }

    private DataSet GetData()
    {
        //TextBox txtSearch = Master.FindControl("txtSearch") as TextBox;
        string[] levels = hfLevels.Value.Split('~');

        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Level1", levels[0])
            ,new SqlParameter("@Level2", levels[1])
            ,new SqlParameter("@Level3", levels[2])
            ,new SqlParameter("@Level4", levels[3])
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            ,new SqlParameter("@otexemptfilter", OTExemptFilter())
            ,new SqlParameter("@showactive", ShowActive())
            
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeList", spParams);
        return ds;
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    protected override DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        gvEmp.SelectedIndex = -1;
        return base.SortDataTable(dataTable, isPageIndexChanging);
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void ibSelect_Click(object sender, ImageClickEventArgs e)
    {
        openDataEntry(sender, hfModule.Value);

        //openDataEntry(sender, "AdminFileLeave.aspx");
        //openDataEntry(sender, "AdminFileOT.aspx");
        
        //openDataEntry(sender, "AdminFileChangeShift.aspx");
        //openDataEntry(sender, "AdminFileChangeRestDay.aspx");

        //openDataEntry(sender, "AdminEditAttendance.aspx");
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    private void openDataEntry(object sender, string pagename)
    {        
        Control ibButton = (Control)sender;
        Literal litID = (Literal)ibButton.Parent.Parent.FindControl("litID");
        Literal litName = (Literal)ibButton.Parent.Parent.FindControl("litName");

        SecureUrl secureUrl = new SecureUrl(
            string.Format(pagename + "?EmpID={0}&EmpName={1}", litID.Text, litName.Text));

        //... needs postback in order to create a secured url.
        hfUpdate.Value = secureUrl.ToString();

        ViewState["EmpID"] = litID.Text;
    }


    protected void gvEmp_DataBound(object sender, EventArgs e)
    {
        string empid =
            ViewState["EmpID"] == null ? string.Empty : ViewState["EmpID"].ToString();

        

        for (int i = 0; i < gvEmp.Rows.Count; i++)        
        {

            Literal litID = (Literal)gvEmp.Rows[i].FindControl("litID");
            if (litID.Text == empid)
                gvEmp.SelectedIndex = i;
        }
    }

    protected void ddlFilterStat_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}
