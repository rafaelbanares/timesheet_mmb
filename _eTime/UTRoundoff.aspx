<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="UTRoundoff.aspx.cs" Inherits="UTRoundoff" Title="Bisneeds - Timekeeping - Undertime Roudingoff Definitions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Undertime Roudingoff Definitions"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Undertime Roundoff Definition" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Undertime Roundoff Definition</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" Width="300px" DataKeyNames="ID">
                <HeaderStyle CssClass="DataGridHeaderStyle" />                
                <Columns>
                
                    <asp:TemplateField HeaderText="Range&lt;br&gt;from">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLO" runat="server" CssClass="TextBoxDecimal" Text='<%# Bind("LO") %>' MaxLength="13" Width="20px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeLO" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="99" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtLO">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="lblLO" runat="server" Text='<%# Bind("LO") %>'></asp:Literal>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" BorderColor="DodgerBlue" />     
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Range&lt;br&gt;to">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUP" runat="server" CssClass="TextBoxDecimal" Text='<%# Bind("UP") %>' MaxLength="13" Width="20px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeUP" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="99" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtUP">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="lblUP" runat="server" Text='<%# Bind("UP") %>'></asp:Literal>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" BorderColor="DodgerBlue" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Rounded<br>(minutes)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRounded" runat="server" CssClass="TextBoxDecimal" Text='<%# Bind("Rounded") %>' MaxLength="13" Width="20px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeRounded" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="99" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtRounded">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="lblRounded" runat="server" Text='<%# Bind("Rounded") %>'></asp:Literal>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" BorderColor="DodgerBlue" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

