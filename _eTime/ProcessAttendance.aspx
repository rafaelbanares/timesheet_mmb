﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ProcessAttendance.aspx.cs" Inherits="ProcessAttendance" Title="Attendance Processing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial"
        Font-Size="Large" Font-Underline="False" Text="Process Transactions"></asp:Label>
<br />
            <table>
                <tr>
                    <td colspan="2">
                        <asp:RadioButton ID="rbAllEmp" Text="All Employees" runat="server" 
                            GroupName="grpSelect" oncheckedchanged="rbAllEmp_CheckedChanged"  Checked="true"
                            AutoPostBack="True" />
                    </td>                    
                </tr>
                <tr style="display:none">
                    <td width="150">
                        <asp:RadioButton ID="rbLevel1" runat="server" AutoPostBack="True" 
                            oncheckedchanged="rbLevel1_CheckedChanged" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLevel1" runat="server" Enabled="False" DataValueField="Code" DataTextField="Description" Width="350px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton ID="rbLevel2" runat="server" GroupName="grpSelect" 
                            AutoPostBack="True" oncheckedchanged="rbLevel2_CheckedChanged"/>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLevel2" runat="server" DataValueField="Code" DataTextField="Description"
                            Width="350px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton ID="rbLevel3" runat="server" GroupName="grpSelect" 
                            AutoPostBack="True" oncheckedchanged="rbLevel3_CheckedChanged"/>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLevel3" runat="server" DataValueField="Code" DataTextField="Description"
                            Width="350px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton ID="rbLevel4" runat="server" GroupName="grpSelect" 
                            AutoPostBack="True" oncheckedchanged="rbLevel4_CheckedChanged"/>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLevel4" runat="server" DataValueField="Code" DataTextField="Description"
                            OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" Width="350px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton ID="rbOneEmp" runat="server" Text="One Employee" 
                            GroupName="grpSelect" AutoPostBack="True" 
                            oncheckedchanged="rbOneEmp_CheckedChanged"/>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOneEmployee" runat="server" DataValueField="Code" DataTextField="Description" Width="350px"></asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Attendance From: &nbsp; 
                        <asp:TextBox ID="txtTranStart" runat="server" CssClass="TextBoxDate"></asp:TextBox> 

                    <asp:ImageButton ID="ibtxtTranStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                        OnClientClick="return false;" ToolTip="Click to choose date" />

                    <ajaxToolkit:MaskedEditValidator id="mevTranStart" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtTranStart" 
                        ControlExtender="meeTranStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                        IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                    </ajaxToolkit:MaskedEditValidator>
                    
                    &nbsp; To &nbsp;
                    
                    <asp:TextBox ID="txtTranEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtTranEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                        OnClientClick="return false;" ToolTip="Click to choose date" />

                    <ajaxToolkit:MaskedEditValidator id="mevTranEnd" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtTranEnd" 
                        ControlExtender="meeTranEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                        IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                    </ajaxToolkit:MaskedEditValidator>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnOK" runat="server" Text="OK" Width="60" 
                            onclick="btnOK_Click"></asp:Button>
                    </td>
                </tr>
            </table>

    <ajaxToolkit:MaskedEditExtender ID="meeTranStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtTranStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleTranStart" runat="server" PopupButtonID="ibtxtTranStart"
        TargetControlID="txtTranStart">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:MaskedEditExtender ID="meeTranEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtTranEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleTranEnd" runat="server" PopupButtonID="ibtxtTranEnd"
        TargetControlID="txtTranEnd">
    </ajaxToolkit:CalendarExtender>


</asp:Content>

