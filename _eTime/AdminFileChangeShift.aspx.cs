using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class AdminFileChangeShift : KioskPageUI
{
    string _EmployeeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        _EmployeeID = _Url["EmpID"];

        base.GetUserSession();
        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            
            lblFullname.Text = _Url["EmpName"];
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeShiftLoadByDateRange",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _EmployeeID), 
                new SqlParameter("@StartDate", txtDateStart.Text), 
                new SqlParameter("@EndDate", txtDateEnd.Text), 
                new SqlParameter("@DBname", _KioskSession.DB)             
            });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeShiftLoadByDateRange",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _EmployeeID), 
                new SqlParameter("@StartDate", txtDateStart.Text), 
                new SqlParameter("@EndDate", txtDateEnd.Text), 
                new SqlParameter("@IsBlankData", true),
                new SqlParameter("@DBname", _KioskSession.DB)             
            });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvMain.DataBind();
        hfIsAdd.Value = "1";
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
            if (ibDelete != null)
            {
                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
            }
            else
            {
                string shiftcode = DataBinder.Eval(e.Row.DataItem, "ShiftCode").ToString();

                DropDownList ddlShift = e.Row.FindControl("ddlShift") as DropDownList;
                //... populate Shift dropdown
                DataSet dsShift = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                    "SELECT ShiftCode, Description FROM " + _KioskSession.DB + ".dbo.Shift WHERE CompanyID = @CompanyID",
                                new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

                ddlShift.DataSource = dsShift.Tables[0];
                ddlShift.DataBind();
                ddlShift.Items.Insert(0, "");

                ddlShift.SelectedValue = shiftcode;

            }
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeShiftDelete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@ID", key), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        TextBox txtStart = gvMain.Rows[e.RowIndex].FindControl("txtStart") as TextBox;
        TextBox txtEnd = gvMain.Rows[e.RowIndex].FindControl("txtEnd") as TextBox;
        DropDownList ddlShift = gvMain.Rows[e.RowIndex].FindControl("ddlShift") as DropDownList;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _EmployeeID ));
        sqlparam.Add(new SqlParameter("@StartDate", txtStart.Text));
        sqlparam.Add(new SqlParameter("@EndDate", txtEnd.Text));
        sqlparam.Add(new SqlParameter("@ShiftCode", ddlShift.SelectedItem.Value ));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (hfIsAdd.Value == "0")
        {
            sqlparam.Add(new SqlParameter("@ID", key));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeShiftUpdate", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeShiftInsert", sqlparam.ToArray());
        }

        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
}