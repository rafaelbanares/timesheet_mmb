﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class UserPassword : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            lblFullname.Text = _Url["EmpName"];
            BindSettings();
            chkNeverExpire.Attributes.Add("onclick", "ToggleCheckbox()");
        }
    }

    private void BindSettings()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetUserPasswordSettings",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _Url["EmpID"]), 
                new SqlParameter("@DBname", _KioskSession.DB)             
            });

        if (ds.Tables[0].Rows.Count > 0)
        {
            DataRow drEmp = ds.Tables[0].Rows[0];

            chkAdmin.Checked = ((int)drEmp["UserLevel"]) > 99;
            chkReset.Checked = (drEmp["Password"].ToString() == "");
            chkNeverExpire.Checked = (bool) drEmp["PwdNeverExpire"] ;
            txtExpiryDate.Text = drEmp["PwdExpireDate"].ToString();
            chkAccountLocked.Checked = (bool)drEmp["AccountIsLocked"];
            togglePannel();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_SavePasswordSettings",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _Url["EmpID"]),
                new SqlParameter("@UserLevel", chkAdmin.Checked?999:0 ),
                new SqlParameter("@PwdNeverExpire", chkNeverExpire.Checked ),
                new SqlParameter("@PwdExpireDate", txtExpiryDate.Text  ),
                new SqlParameter("@AccountLocked", chkAccountLocked.Checked  ),
                new SqlParameter("@Reset", chkReset.Checked  ),
                new SqlParameter("@DBname", _KioskSession.DB)
            });
        togglePannel();
    }

    private void togglePannel()
    {
        pnlExpireDate.Style.Remove("display");
        pnlExpireDate.Style.Add("display", chkNeverExpire.Checked ? "none" : "");
    }
}



