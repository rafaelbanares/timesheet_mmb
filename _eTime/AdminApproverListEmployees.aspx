﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="AdminApproverListEmployees.aspx.cs" Inherits="AdminApproverListEmployees" Title="Approver Member Lists" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>

<script type="text/javascript">
    function openSelectEmp(sEmpID, sUrl)
    {
      try
      {
        $get("<% =hfEmpID.ClientID %>").value = sEmpID;
        popupWindow=wopen(sUrl,"popupWindow",1000,600);
      }
      catch (err)
      {
      }
    }
    
    //... popup approver passes these parameters.
    function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue)
    {
      try
       {
        //$get(openerEmpClientID).value = openerEmpValue;
        //$get(openerNameClientID).value = openerNameValue;

        $get("<% =hfApprID.ClientID %>").value = openerEmpValue; //-- selected approver
        //$get("<% =hfEmpID.ClientID %>").value; //-- selected employee
        $get("<% =btnRefreshGrid.ClientID %>").click();

        closewindow(popupWindow);
       }
      catch (err)
      {
      }      
    }
</script>

<table>
    <tr>
        <td>
            Approver #1:
        </td>
        <td>
            <asp:Label ID="lblApprover1" runat="server"></asp:Label>
        </td>
    </tr>

    <tr>
        <td>
            Approver #2:
        </td>
        <td>
            <asp:Label ID="lblApprover2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Approver #3:
        </td>
        <td>
            <asp:Label ID="lblApprover3" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<br />
<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Approver Member Lists"></asp:Label><br />
    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
        AllowPaging="True" AllowSorting="True" OnSorting="gvEmp_Sorting" PageSize="16"
        OnPageIndexChanging="gvEmp_PageIndexChanging" OnRowDataBound="gvEmp_RowDataBound">
        <Columns>
            <asp:BoundField DataField="EmployeeID" HeaderText="Employee ID" SortExpression="EmployeeID"
                ItemStyle-Width="120px">
                <ItemStyle Width="120px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="FullName"
                ItemStyle-Width="500px">
                <ItemStyle Width="500px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField>
                <ItemStyle VerticalAlign="Top" Width="10px" />
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit.gif" ToolTip="Update Approver" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>

    <br />
    <asp:Label ID="lblHeadCount" runat="server" Font-Names="Arial" Font-Size="Large"></asp:Label>
    
    <asp:HiddenField ID="hfEmpID" runat="server" />
    <asp:HiddenField ID="hfApprID" runat="server" />
    <asp:Button ID="btnRefreshGrid" runat="server" Text="RefreshGrid" onclick="btnRefreshGrid_Click" style="display:none" />

</asp:Content>

