using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;
using Bns.AttUtils;
using Microsoft.ApplicationBlocks.Data;

public partial class AttendanceMaster : MasterPage 
{
    protected string _ConnectionString = ConfigurationManager.AppSettings.Get("Connection");
    protected static readonly String MainDB = ConfigurationManager.AppSettings.Get("MasterDB");
    private Kiosk _KioskSession;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserSession();
        if (!IsPostBack)
        {
            PopulateMenu();
        }
    }

    protected void PopulateMenu()
    {
        /*
        DataSet ds = new DataSet();
        // Query the database for site map nodes
        using (SqlConnection connection = new SqlConnection(_ConnectionString))
        {
            //_KioskSession.CompanyID

            connection.Open();
            SqlCommand command = new SqlCommand(MainDB + ".dbo.usa_eTimeSiteMap", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(ds);
        }
        */

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_eTimeSiteMap",
            new SqlParameter[] { new SqlParameter("@DBName", _KioskSession.DB) });

        DataRelation ChildrenLevel1 = new DataRelation("ChildrenLevel1", ds.Tables[0].Columns["ID"], ds.Tables[1].Columns["Parent"], false);
        DataRelation ChildrenLevel2 = new DataRelation("ChildrenLevel2", ds.Tables[1].Columns["ID"], ds.Tables[2].Columns["Parent"], false);
        ds.Relations.Add(ChildrenLevel1);
        ds.Relations.Add(ChildrenLevel2);

        menuMain.Items.Clear();
        

        foreach (DataRow parentItem in ds.Tables[0].Rows)
        {
            MenuItem parentMenu = BuildMenu(parentItem);
            menuMain.Items.Add(parentMenu);

            foreach (DataRow childItem in parentItem.GetChildRows("ChildrenLevel1"))
            {
                MenuItem childMenu = BuildMenu(childItem);
                if (childMenu != null)
                {
                    parentMenu.ChildItems.Add(childMenu);

                    foreach (DataRow grandchildItem in childItem.GetChildRows("ChildrenLevel2"))
                    {
                        MenuItem grandchildMenu = BuildMenu(grandchildItem);
                        if (grandchildMenu != null)
                            childMenu.ChildItems.Add(grandchildMenu);

                        //.... its possible to have additional grandgrandChild here

                    }
                }
            }
        }

        menuMain.DataBind();
    }

    private MenuItem BuildMenu(DataRow node)
    {
        string url = node["Url"].ToString();
        string secureurl = node["SecureUrl"].ToString();
        string title = node["Title"].ToString();
        string description = node["Description"].ToString();

        if (url != "")
        {
            if (secureurl != "")
                url = new SecureUrl(secureurl).ToString();
        }

        //... detect special node
        if (title[0] == '*')
        {
            if (title == "*Division")   // Level2
                title = _KioskSession.Level[1];
            if (title == "*Department") // Level3
                title = _KioskSession.Level[2];
            if (title == "*Section")   // Level4
                title = _KioskSession.Level[3];

            if (title == "*WorkFor")   // Client
                if (_KioskSession.WorkForEnabled)
                    title = _KioskSession.WorkFor + " Definition";
                else
                    return null;

            if (title == "*WorkForSub")// Agent
                if (_KioskSession.WorkForEnabled)
                    title = _KioskSession.WorkForSub + " Definition";
                else
                    return null;

            description = title;
        }

        MenuItem item = new MenuItem(title, node["ID"].ToString(), "", url);
        item.ToolTip = description;

        if (url == "")
            item.Selectable = false;

        return item;
    }

    protected void GetUserSession()
    {
        object session = (object)Session["kiosk"];

        if (session == null)
            Response.Redirect("LoginEmployee.aspx");

        _KioskSession = (Kiosk)session;
    }


}

