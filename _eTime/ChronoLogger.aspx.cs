using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;
using Bns.AttUtils;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using BNS.Framework.Encryption;


public partial class ChronoLogger : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //... never use session on this page, because this page is open 24/7
        litScript.Text = "";
        if (!IsPostBack)
        {

        }
    }

    protected void bOK_Click(object sender, EventArgs e)
    {
        //... temporarily hardcoded values
        string companyid = "EW01";
        string clientdb = "BNSAttendance_EW";

        string masterdb = System.Configuration.ConfigurationManager.AppSettings.Get("MasterDB");
        string defaultcompany = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");

        if (defaultcompany != "")
        {
            string[] details = defaultcompany.Split('~');

            companyid = details[0];
            clientdb = details[1];
            //companyname = details[2];
        }

        string io = hdIO.Value;
        string password = tbPWD.Text == "" ? "" : Crypto.ActionEncrypt(tbPWD.Text);
        DateTime timeio = DateTime.Parse(hdTime.Value);
        //DateTime timeio = DateTime.Parse(lbDate.Text + " " + lbTime.Text);

        DataTable dt =
            SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, masterdb + ".dbo.usa_AttendanceTimeIO",
                new SqlParameter[] {
                    new SqlParameter("@CompanyID", companyid),
                    new SqlParameter("@EmployeeID", tbUID.Text),
                    new SqlParameter("@Password", password),
                    new SqlParameter("@Date", DateTime.Today),
                    //new SqlParameter("@TimeIO", DateTime.Now),
                    new SqlParameter("@TimeIO", timeio),
                    new SqlParameter("@IO", io),
                    new SqlParameter("@DBname",clientdb)
                }).Tables[0];

        string errmsg = dt.Rows[0]["ErrMsg"].ToString();
        if (errmsg.Length > 0)
        {
            lblError.Text = errmsg;
            litScript.Text = @"<script type='text/javascript'>assignIO();</script>";
        }
        else
        {
            string msg = "Last log-on successful... ";
            lblMessage.Text = msg;
            //litScript.Text = @"<script type='text/javascript'>alert('"+msg+"');</script>";
            tbUID.Text = "";
            tbPWD.Text = "";
        }

    }
}
