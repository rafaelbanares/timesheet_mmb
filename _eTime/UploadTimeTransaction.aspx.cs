using System;
using System.IO;
using Bns.AttendanceUI;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using MMB.Core;
using BNS.TK.Entities;
using BNS.TK.Business;

public partial class UploadTimeTransaction : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (fuWorkfile.HasFile)
        {
            try
            {
                string strFile = Server.MapPath("~/UploadedFiles/" + fuWorkfile.FileName);

                lblStatus.Text = "Uploading....";
                fuWorkfile.SaveAs(strFile);

                //BulkInsert(strFile);      //rsb: uncomment this after testing
                //
                UploadRaw2(strFile);

                lblSummary.ForeColor = System.Drawing.Color.Black;
                lblSummary.Text = "Uploading completed...";
            }
            catch //(Exception ex)
            {
                lblSummary.ForeColor = System.Drawing.Color.Red;
                lblSummary.Text = "The file uploaded was not recognized by the system";
            }
        }
        else
        {
            lblStatus.Text = "You have not specified a file.";
        }
    }

    //private void BulkInsert(string strFile)
    //{
    //    DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeTransBiometrics3", new SqlParameter[] {
    //                        new SqlParameter("@DBName", _KioskSession.DB)
    //                        ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
    //                        ,new SqlParameter("@FilePath", strFile) }
    //                    );

    //    lblSummary.Text = string.Format("Successfuly uploaded ({0}) new transactions, ({1}) found invalid machine ID",
    //                                        ds.Tables[0].Rows[0][0].ToString(), ds.Tables[1].Rows.Count );

    //    GridView1.DataSource = ds.Tables[1];
    //    GridView1.DataBind();
    //}

    //Sample format:1120        08:59 PM20080612TO F
    //private void processFile(string file)
    //{
    //    //TimeMachine tm = new TmStandard();
    //    Bns.Attendance.TimeMachine tm = new Bns.Attendance.TmBiometrics();
    //    string line;
    //    int valid = 0, error = 0;

    //    List<string> list = new List<string>();

    //    using (StreamReader sr = new StreamReader(file))
    //    {
    //        // the succeding line will be the data
    //        while ((line = sr.ReadLine()) != null)
    //        {
    //            if (tm.SetInfo(line))
    //            {
    //                SqlParameter output = new SqlParameter("@ValidOut", 0);
    //                output.Direction = ParameterDirection.Output;

    //                SqlParameter[] spParams = new SqlParameter[]
    //                    {
    //                        new SqlParameter("@DBName", _KioskSession.DB)
    //                        ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
    //                        ,new SqlParameter("@SwipeID", tm.SwipeID)
    //                        ,new SqlParameter("@TimeIO", tm.Timetrans)
    //                        ,new SqlParameter("@IO", tm.IO)
    //                        ,new SqlParameter("@Station", tm.Station)
    //                        ,output
    //                    };

    //                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeTransInsert", spParams);

    //                //if (bool.Parse(output.SqlValue.ToString()))
    //                if (output.Value.ToString() == "1")
    //                {
    //                    valid++;
    //                }
    //                else
    //                {
    //                    error++;

    //                    if (error < 20)
    //                        list.Add(tm.SwipeID);
    //                    else if (error == 20)
    //                        list.Add("more to list..");
    //                }
    //            }
    //        }
    //    }
    //    lblSummary.Text = string.Format("There are ({0}) valid machine ID and ({1}) invalid machine ID", valid, error);
    //    GridView1.DataSource = list;
    //    GridView1.DataBind();        
    //}


    // Future enhancement:
    //        position	length	format	mapping
    //swipeid		13		6	
    //logdate		3		6	ddMMyy
    //logtime		9		4	HHmm
    //io		    19		1		    I:0;O:9
    private void UploadRaw(string file)
    {
        System.Guid guid = System.Guid.NewGuid();
        DateTime now = DateTime.Now;        
        RawTime raw = new RawTime();

        try
        {
            using (StreamReader sr = new StreamReader(file))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string dateio = line.Substring(2, 6);
                    string timeio = line.Substring(8, 4);

                    raw.AddNew();
                    raw.SessionID = guid;
                    raw.CompanyID = _KioskSession.CompanyID;
                    raw.SwipeID = line.Substring(12, 6);
                    raw.TimeIO = DateTime.ParseExact(dateio + " " + timeio, "ddMMyy HHmm", CultureInfo.InvariantCulture);
                    raw.IO = (line.Substring(18, 1) == "0") ? "I" : "O";
                    raw.Station = "";
                    //raw.CreatedBy = _KioskSession.UID;
                    //raw.CreatedDate = now;
                }
            }
            raw.Save();
            lblSummary.Text = "Done.";
        }
        catch (Exception ex)
        {
            lblSummary.Text = ex.Message;
        }
    }

    private void UploadRaw2(string file)
    {
        System.Guid guid = System.Guid.NewGuid();
        DateTime now = DateTime.Now;
        RawTime raw = new RawTime();

        RawDataSettingsBase rs = new RawDataSettings();
        RawTimeParser rawParser = new RawTimeParser(rs);

        try
        {
            using (StreamReader sr = new StreamReader(file))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (rawParser.IsValidIOCode(line))
                    {
                        rawParser.Parse(line);
     
                        raw.AddNew();
                        raw.SessionID = guid;
                        raw.CompanyID = _KioskSession.CompanyID;
                        raw.SwipeID = rawParser.SwipeID;
                        raw.TimeIO = rawParser.LogDateTime;
                        raw.IO = rawParser.IO;
                        raw.Station = "";
                        //raw.CreatedBy = _KioskSession.UID;
                        //raw.CreatedDate = now;
                    }
                    
                }
            }
            raw.Save();
            lblSummary.Text = "Done.";
        }
        catch (Exception ex)
        {
            lblSummary.Text = ex.Message;
        }
    }

    //Sample format:1120        08:59 PM20080612TO F
    private void TempMethodConvertBiometrics(string file)
    {        
        //TimeMachine tm = new TmStandard();
        Bns.Attendance.TimeMachine tm = new Bns.Attendance.TmBiometrics();
        string line;
        string swipeid = "";
        int empcount = 0;

        //...Get the list of employees with no swipe id, to be replaced using the text file
        string qry1 = string.Format(
            "select employeeid from {0}.dbo.employee where companyid = '{1}' and swipeid = '' order by employeeid",
                _KioskSession.DB, _KioskSession.CompanyID);
        DataSet dsEmployee = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, qry1);

        List<string> list = new List<string>();
        using (StreamReader sr = new StreamReader(file))
        {
            // the succeding line will be the data
            while ((line = sr.ReadLine()) != null)
            {
                if (tm.SetInfo(line))
                {
                    if (swipeid != tm.SwipeID)
                    {
                        swipeid = tm.SwipeID;

                        //...check for the existance of swipeid
                        string qry2 = string.Format(
                            "select 1 from {0}.dbo.employee where companyid = '{1}' and swipeid = '{2}'",
                                _KioskSession.DB, _KioskSession.CompanyID, swipeid);

                        int count = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, qry2).Tables[0].Rows.Count;

                        if (count == 0)
                        {
                            string qry3 = string.Format(
                                "update {0}.dbo.employee set swipeid = '{1}',dateterminated = null " +
                                    "where companyid = '{2}' and employeeid = '{3}' ",
                                        _KioskSession.DB, 
                                        swipeid, 
                                        _KioskSession.CompanyID, 
                                        dsEmployee.Tables[0].Rows[empcount++][0].ToString() );

                            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.Text, qry3);
                        }
                    }
                }
            }
        }

    }    

    /* generic time format
    //Sample format:0002  ,10/28/2007,08:12,I
    private void processFile(string file)
    {
        TimeMachine tm = new TmStandard();
        string line;
        int valid = 0, error = 0;

        List<string> list = new List<string>();

        using (StreamReader sr = new StreamReader(file))
        {            
            // the succeding line will be the data
            while ((line = sr.ReadLine()) != null)
            {
                if (tm.SetInfo(line))
                {
                    SqlParameter output = new SqlParameter("@ValidOut",0);
                    output.Direction = ParameterDirection.Output;
                    
                    SqlParameter[] spParams = new SqlParameter[]
                        {
                            new SqlParameter("@DBName", _KioskSession.DB)
                            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                            ,new SqlParameter("@SwipeID", tm.SwipeID)
                            ,new SqlParameter("@TimeIO", tm.Timetrans)
                            ,new SqlParameter("@IO", tm.IO)
                            ,new SqlParameter("@Station", tm.Station)
                            ,output
                        };

                    SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeTransInsert", spParams);

                    //if (bool.Parse(output.SqlValue.ToString()))
                    if (output.Value.ToString() == "1")
                    {
                        valid++;
                    }
                    else
                    {                        
                        error++;

                        if (error < 20)
                            list.Add(tm.SwipeID);
                        else if (error == 20)
                            list.Add("more to list..");
                    }
                }
            }
        }
        lblSummary.Text = string.Format("There are ({0}) valid machine ID and ({1}) invalid machine ID", valid, error);
        GridView1.DataSource = list;        
        GridView1.DataBind();
        //GridView1.Columns[0].HeaderText = "Invlid ID";
    }
    */

}

