﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="popupEmployeeCompany.aspx.cs" Inherits="popupEmployeeCompany" Title="Bisneeds - Timekeeping - Select Employee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript">
function load() {    
   Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);   
}

function EndRequestHandler(sender, args) {
   if (args.get_error() == undefined)
    {
        var openerEmpClientID  = "<%=_Url["hfID"]%>";
        var openerNameClientID = "<%=_Url["lbID"]%>";
        var openerCompanyClientID = "<%=_Url["compID"]%>";
        
        var openerEmpValue = $get("<%=hfEmpID.ClientID%>").value;
        var openerNameValue = $get("<%=hfName.ClientID%>").value;
        var openerCompanyValue = $get("<%=ddlCompany.ClientID%>").value;
        
        if (openerEmpValue != '')
        {
            window.opener.updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue, openerCompanyClientID, openerCompanyValue); 
        }
    }
   else
       alert('There was an error' + args.get_error().message);
}
</script>

<div style="width:496px">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Label ID="lblSearch" runat="server" CssClass="ControlDefaults" Text="Search:"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" CssClass="ControlDefaults"
                    OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                        ValidationGroup="emplist" />
            </td>
            <td align="right">
                <asp:Label ID="lblCompany" runat="server" CssClass="ControlDefaults" Text="Select Company:"></asp:Label></td>
            <td>
                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="ControlDefaults" 
                        DataTextField="CompanyName" DataValueField="CompanyID" AutoPostBack="True" 
                        onselectedindexchanged="ddlCompany_SelectedIndexChanged">
                    </asp:DropDownList>
            </td>
        </tr>
    </table>
<asp:UpdatePanel ID="updEmp" UpdateMode="Conditional" runat="server">
<ContentTemplate>
<asp:GridView id="gvEmp" runat="server" CssClass="DataGridStyle" Width="100%" OnSelectedIndexChanged="gvEmp_SelectedIndexChanged" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanging="gvEmp_PageIndexChanging" OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" PageSize="20"><Columns>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="ibSelect" runat="server" ValidationGroup="emplist" ImageUrl="~/Graphics/select.gif" CommandName="select" OnClick="ibSelect_Click" ToolTip="Select"></asp:ImageButton> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="EmployeeID" SortExpression="EmployeeID"><ItemTemplate>
<asp:Label id="lblID" runat="server" Text='<%# Bind("EmployeeID") %>' ></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="FullName" SortExpression="FullName"><ItemTemplate>
<asp:Literal id="litName" runat="server" Text='<%# Bind("Fullname") %>' ></asp:Literal> 
</ItemTemplate>
</asp:TemplateField>
</Columns>
<PagerStyle HorizontalAlign="Right"></PagerStyle>
<EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
</asp:GridView>&nbsp;&nbsp; <asp:HiddenField id="hfEmpID" runat="server"></asp:HiddenField> <asp:HiddenField id="hfName" runat="server"></asp:HiddenField> <asp:HiddenField id="hfFunc" runat="server"></asp:HiddenField>
</ContentTemplate>
</asp:UpdatePanel>
</div>


</asp:Content>
