<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AdminLeave.aspx.cs" Inherits="AdminLeave" Title="Bisneeds - Timekeeping - Leaves Approval" %>
<%@ Register Src="ucLeaveApproval.ascx" TagName="ucLeaveApproval" TagPrefix="uc1" %>

<asp:Content ID="c1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;
    
    closechild();
}
function closechild()
{
    $get("<%=ibGo.ClientID%>").click();
    closewindow(popupWindow);
}
</script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Leave Admin"></asp:Label><br />
            <br />
            <table width="100%">
                <tr>
                    <td align="right" style="width: 80px">
                        <asp:Label ID="lblSearch" runat="server" CssClass="ControlDefaults" Text="Search:"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" CssClass="ControlDefaults"
                            OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                                ValidationGroup="emplist" /></td>
                </tr>
            </table>
        </td>
        <td align="right" valign="top">
            <table>
                <tr>
                    <td>
                                <table>
                                    <tr>
                                        <td>
                                            Attendance From:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate" ValidationGroup="vdate"></asp:TextBox>
                                            <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                                OnClientClick="return false;" ToolTip="Click to choose date" />
                                            <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                                                ControlExtender="meeDateStart" ErrorMessage="Error on date" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" ValidationGroup="vdate"  >*</ajaxToolkit:MaskedEditValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            To:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate" ValidationGroup="vdate"></asp:TextBox>
                                            <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                                OnClientClick="return false;" ToolTip="Click to choose date" />
                                            <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                                                ControlExtender="meeDateEnd" ErrorMessage="Error on date" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" ValidationGroup="vdate"  >*</ajaxToolkit:MaskedEditValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Select only leaves :</td>
                                        <td align="left" colspan="1">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ControlDefaults" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True" ValidationGroup="vdate">
                                                <asp:ListItem Value="">No Filter</asp:ListItem>
                                                <asp:ListItem Selected="True">For Approval</asp:ListItem>
                                                <asp:ListItem>Approved</asp:ListItem>
                                                <asp:ListItem>Declined</asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                                <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
                                    Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
                                    TargetControlID="txtDateStart">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
                                    Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
                                    TargetControlID="txtDateEnd">
                                </ajaxToolkit:CalendarExtender>
                    </td>
                    <td>
                        <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click" ToolTip="Go" />
                    </td>
                </tr>
            </table>
        </td>
        <td align="center" valign="top">            
            <table>
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lnkClear" runat="server" ToolTip="Clear Selection" 
                            onclick="lnkClear_Click">clear</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                            Width="279px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<asp:UpdatePanel id="up1" runat="server" UpdateMode="Conditional">
<ContentTemplate>            
    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle" Width="100%" AllowPaging="True" AllowSorting="True" OnSorting="gvEmp_Sorting">
        <Columns>
            <asp:BoundField DataField="EmployeeID" HeaderText="Emp. ID" SortExpression="EmployeeID" />
            <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="Fullname" />
            <asp:BoundField DataField="DateFiled" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Date filed"
                HtmlEncode="False" SortExpression="DateFiled" />
            <asp:BoundField DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Start date"
                HtmlEncode="False" SortExpression="StartDate" />
            <asp:BoundField DataField="EndDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="End date"
                HtmlEncode="False" SortExpression="EndDate" />
            <asp:BoundField DataField="Days" HeaderText="No. of days" SortExpression="Days" />
            <asp:BoundField DataField="Code" HeaderText="Leave" SortExpression="Code" />
            <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:TemplateField>
                <ItemStyle VerticalAlign="Top" />
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit.gif" OnClick="ibEdit_Click" /><asp:HiddenField ID="hfID" runat="server" Value='<%# Bind("ID") %>' />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>        
</ContentTemplate>
</asp:UpdatePanel>

<asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="vdate" />
<br />
<asp:Button ID="btnShowPopup" runat="server" Text="Show Popup" style="display:none" /><asp:HiddenField
    ID="hfLevels" runat="server" Value="~~~" />
<asp:HiddenField ID="hfURL" runat="server" />

<ajaxToolkit:ModalPopupExtender ID="mpe1" runat="server"         
    TargetControlID="btnShowPopup"
    PopupControlID="pnl1" 
    DropShadow="true"
    CancelControlID="btnCancel"
    BackgroundCssClass="modalBackground"
    PopupDragHandleControlID="pnl3" />


<asp:Panel ID="pnl1" runat="server" style="display:none; background-color:dodgerblue;border:solid 1px green" CssClass="modalPopup">    
    <asp:Panel ID="pnl3" runat="server" Style="cursor: move;background-color:dodgerblue;border:solid 1px Gray;">
        <table width="630">
            <tr>
                <td width="100%" align="center">
                    <asp:Label id="lblPopup" Text="Leaves Approval" Font-Underline="False" Font-Size="Large" Font-Names="Arial" Font-Bold="True" runat="server" ForeColor="White"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Close" />
                </td>
            </tr>            
        </table>                       
    </asp:Panel>        
    <uc1:ucLeaveApproval ID="ucLeaveApp" runat="server" UpdateMode="Conditional" OnClickUpdate="ucLeaveApp_OnClickUpdate"/>
</asp:Panel>
</asp:Content>
