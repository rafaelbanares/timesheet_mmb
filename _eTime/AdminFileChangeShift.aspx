<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="AdminFileChangeShift.aspx.cs" Inherits="AdminFileChangeShift" Title="Bisneeds - Timekeeping - Change of Shift" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <table width="100%">
        <tr>
            <td>

    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Change of Shift Form"></asp:Label><br />
                <br />
                <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname S."></asp:Label></td>
            <td>
                <table>
                    <tr>
                        <td>
                            Filter From:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                OnClientClick="return false;" ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                                ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                OnClientClick="return false;" ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                                ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                        <td align="left">
                            <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                                ToolTip="Go" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="Add change of shift" /><asp:LinkButton ID="lbNew" runat="server"
            CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">Add change of shift</asp:LinkButton><br />

    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" CssClass="DataGridStyle" Width="500px" OnRowUpdating="gvMain_RowUpdating"
                OnRowEditing="gvMain_RowEditing" OnRowDeleting="gvMain_RowDeleting" OnRowDataBound="gvMain_RowDataBound"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" DataKeyNames="ID" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField HeaderText="Start date" SortExpression="StartDate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtStart" runat="server" Text='<%# Bind("StartDate", "{0:MM/dd/yyyy}") %>'
                                CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtStart" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                            </asp:ImageButton>&nbsp;<asp:RequiredFieldValidator ID="rfvStart" runat="server"
                                ErrorMessage="Date start is required" ControlToValidate="txtStart" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                                    ID="meeStart" runat="server" TargetControlID="txtStart" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleStart" runat="server" TargetControlID="txtStart"
                                PopupButtonID="ibtxtStart">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litStart" runat="server" Text='<%# Bind("StartDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End date" SortExpression="EndDate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEnd" runat="server" Text='<%# Bind("EndDate", "{0:MM/dd/yyyy}") %>'
                                CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtEnd" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                            </asp:ImageButton>
                            <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ErrorMessage="Date end is required"
                                ControlToValidate="txtEnd" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                                    ID="meeEnd" runat="server" TargetControlID="txtEnd" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleEnd" runat="server" TargetControlID="txtEnd"
                                PopupButtonID="ibtxtEnd">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litEnd" runat="server" Text='<%# Bind("EndDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Shift" SortExpression="Code">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlShift" runat="server" DataValueField="ShiftCode" DataTextField="Description">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvShiftCode" runat="server" ErrorMessage="Shift code is required"
                                ControlToValidate="ddlShift" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action">
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans"
                                CommandName="update" ToolTip="Update"></asp:ImageButton>
                            <asp:ImageButton ID="ibCancel" runat="server" ImageUrl="~/Graphics/cancel.GIF" CommandName="cancel" ToolTip="Cancel">
                            </asp:ImageButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit.gif" CommandName="edit" ToolTip="Edit">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ibDelete" runat="server" ImageUrl="~/Graphics/delete.gif" CommandName="delete" ToolTip="Delete">
                            </asp:ImageButton>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top" Width="42px"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server"></asp:HiddenField>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="lbNew"></asp:AsyncPostBackTrigger>
        </Triggers>

    </asp:UpdatePanel>
    <br />
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

</asp:Content>

