<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" Title="Bisneeds - Timekeeping - Change Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<table>
    
    <tr>
        <td align="right">
            <asp:Label ID="lblOldPwd" runat="server" Text="Old Password"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtOldPwd" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
   <tr>
        <td align="right">
            <asp:Label ID="lblNewPwd" runat="server" Text="New Password"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" MaxLength="8"></asp:TextBox>
            <asp:RegularExpressionValidator
                ID="rvNew" runat="server" 
                ErrorMessage="Password should be 6-8 characters" 
                ValidationExpression="^.{6,8}$" ControlToValidate="txtNewPwd" 
                ValidationGroup="trans">*
            </asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator
                ID="rfv" runat="server"
                ErrorMessage="Password must have a value"
                ControlToValidate="txtNewPwd" 
                ValidationGroup="trans"
                >*
            </asp:RequiredFieldValidator>

        </td>   
   </tr>
   <tr>
        <td align="right">
            <asp:Label ID="lblRetype" runat="server" Text="Re-type New Password"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtRetype" runat="server" TextMode="Password" MaxLength="8"></asp:TextBox>
        </td>   
   </tr>
   <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnOK" runat="server" OnClick="btnOK_Click" Text="Ok" Width="65px" ValidationGroup="trans" />
            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel"
                Width="65px" /></td>
   </tr>
</table>
    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
    <asp:ValidationSummary ID="vsum1" runat="server" ShowMessageBox="True" 
        ShowSummary="False" ValidationGroup="trans" />
</asp:Content>

