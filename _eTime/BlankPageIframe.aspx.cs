using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class BlankPageIframe : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblModule.Text = _Url["moduledesc"];
            lblMesseage.Text = _Url["message"];
        }
    }
}
