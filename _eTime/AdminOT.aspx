<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AdminOT.aspx.cs" Inherits="AdminOT" Title="Bisneeds - Timekeeping - Overtime Approvals" %>
<%@ Register Src="ucOTAuth.ascx" TagName="ucOTAuth" TagPrefix="uc1" %>
<asp:Content ID="c1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;
    
    closechild();
}
function closechild()
{
    $get("<%=ibGo.ClientID%>").click();
    closewindow(popupWindow);
}

</script>

    <table width="100%">
        <tr>
            <td valign="top">
                <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                    Font-Underline="False" Text="Overtime Lists"></asp:Label><br />
                <br />
                <table width="100%">
                    <tr>
                        <td align="right" style="width: 80px">
                            <asp:Label ID="lblSearch" runat="server" CssClass="ControlDefaults" Text="Search:"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" CssClass="ControlDefaults"
                                OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                                    ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                                    ValidationGroup="emplist" /></td>
                    </tr>
                </table>
            </td>
            
            <td valign="top" align="right">
                <table>
                    <tr>
                        <td>
                            Attendance
                            From:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                OnClientClick="return false;" ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                                ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                        <td align="left" style="color: #000000">
                        </td>
                    </tr>
                    <tr style="color: #000000">
                        <td>
                            To:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                OnClientClick="return false;" ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                                ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                        <td align="left">
                            <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                                ToolTip="Go" /></td>
                    </tr>
                    <tr>
                        <td>
                            Select only leaves :</td>
                        <td align="left" colspan="1">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ControlDefaults" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Value="">No Filter</asp:ListItem>
                                <asp:ListItem Selected="True">For Approval</asp:ListItem>
                                <asp:ListItem>Approved</asp:ListItem>
                                <asp:ListItem>Declined</asp:ListItem>
                            </asp:DropDownList></td>
                        <td colspan="1">
                        </td>
                    </tr>
                </table>
            </td>            
            <td align="center" valign="top">
            <table>
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lnkClear" runat="server" ToolTip="Clear Selection" 
                            onclick="lnkClear_Click">clear</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                            Width="279px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
    
<asp:UpdatePanel id="up1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>            
        <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle" Width="100%" AllowPaging="True" AllowSorting="True" OnSorting="gvEmp_Sorting" PageSize="16" OnPageIndexChanging="gvEmp_PageIndexChanging" OnRowDataBound="gvEmp_RowDataBound">
            <Columns>
                <asp:BoundField DataField="EmployeeID" HeaderText="Employee ID" SortExpression="EmployeeID"  />
                <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="FullName" />
                                
                <asp:BoundField DataField="OTDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="OT date"
                    HtmlEncode="False" SortExpression="OTDate" />
                <asp:BoundField DataField="OTStart" DataFormatString="{0:hh:mm tt}" HeaderText="OT start"
                    HtmlEncode="False" />
                <asp:BoundField DataField="OTEnd" DataFormatString="{0:hh:mm tt}" HeaderText="OT end"
                    HtmlEncode="False" />
                <asp:BoundField DataField="ApprovedOT" HeaderText="Approved hours" SortExpression="ApprovedOT">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Reason" HeaderText="Reason" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:TemplateField>
                    <ItemStyle VerticalAlign="Top" />
                    <ItemTemplate>
                        <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit.gif" OnClick="ibEdit_Click" ToolTip="Edit" />
                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Bind("ID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="DataGridHeaderStyle" />
            <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
        </asp:GridView>
        
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibSearch"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>

    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    <asp:Button ID="btnShowPopup" runat="server" Text="Show Popup" style="display:none" /><asp:HiddenField
        ID="hfLevels" runat="server" Value="~~~" />
    <asp:HiddenField ID="hfURL" runat="server" />
    
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:ModalPopupExtender ID="mpe1" runat="server"         
        TargetControlID="btnShowPopup"
        PopupControlID="pnl1" 
        DropShadow="true"
        CancelControlID="btnCancel"
        BackgroundCssClass="modalBackground"
        PopupDragHandleControlID="pnl3" />


    <asp:Panel ID="pnl1" runat="server" style="display:none; background-color:dodgerblue;border:solid 1px green" CssClass="modalPopup">    
        <asp:Panel ID="pnl3" runat="server" Style="cursor: move;background-color:dodgerblue;border:solid 1px Gray;">
            <table width="580">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label id="lblPopup" Text="Overtime Approval" Font-Underline="False" Font-Size="Large" Font-Names="Arial" Font-Bold="True" runat="server" ForeColor="White"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Close" />
                    </td>
                </tr>            
            </table>            
        </asp:Panel>
        <uc1:ucOTAuth ID="ucOTAuth" runat="server" UpdateMode="Conditional" OnClickUpdate="ucLeaveApp_OnClickUpdate" />
    </asp:Panel>

</asp:Content>
