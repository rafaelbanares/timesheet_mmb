﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class FileMaint : KioskPageUI
{
    protected static int colspan = 4;
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        PopulateScreen();
    }


    private void PopulateScreen()
    {
        DataSet dsDatata = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetMenuListFileMaint");

        DataTable dtHeader = dsDatata.Tables[0];
        hdr1.Text = dtHeader.Rows[0]["Description"].ToString();
        hdr2.Text = dtHeader.Rows[1]["Description"].ToString();
        hdr3.Text = dtHeader.Rows[2]["Description"].ToString();

        DataRow[] drDetails1 = dsDatata.Tables[1].Select("ParentNode = '" + dtHeader.Rows[0]["Node"].ToString() + "'");
        DataRow[] drDetails2 = dsDatata.Tables[1].Select("ParentNode = '" + dtHeader.Rows[1]["Node"].ToString() + "'");
        DataRow[] drDetails3 = dsDatata.Tables[1].Select("ParentNode = '" + dtHeader.Rows[2]["Node"].ToString() + "'");

        //string tbl = @"<table border=1 style='width:100%;border-collapse:collapse;'><tr>{0}</tr></table>";
        string tbl = @"<tr>{0}</tr>";

        string table1 = string.Format(tbl, tablerows(drDetails1));
        string table2 = string.Format(tbl, tablerows(drDetails2));
        string table3 = string.Format(tbl, tablerows(drDetails3));

        det1.Text = table1;
        det2.Text = table2;
        det3.Text = table3;
        
    }

    private string tablerows(DataRow[] rows)
    {
        string cell = @"<td style='width:25%;height:33px'><a href={1}>{0}</td>";

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= rows.Length; i++)
        {
            string url = rows[i - 1]["secureurl"].ToString();
            if (url == "")
                url = rows[i - 1]["url"].ToString();
            else
                url = new SecureUrl(url).ToString();

            string node = rows[i - 1]["Node"].ToString();
            string description = rows[i - 1]["description"].ToString();

            //... special nodes...
            if (node == "LEVEL2")
                description = _KioskSession.Level[1];
            else if (node == "LEVEL3")
                description = _KioskSession.Level[2];
            else if (node == "LEVEL4")
                description = _KioskSession.Level[3];

            sb.Append(string.Format(cell, description, url));

            if (i % colspan == 0 && i < rows.Length )
            {
                sb.Append(@"</tr> <tr>");
            }
        }
        return sb.ToString();
    }


}
