﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="popupHistory.aspx.cs" Inherits="popupHistory" Title="Employee Audittrail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:GridView ID="gvEmp" runat="server" CssClass="DataGridStyle" Width="100%"
        AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvEmp_PageIndexChanging"
        OnRowDataBound="gvEmp_RowDataBound" PageSize="5">

        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ibSelect" runat="server" ValidationGroup="emplist" ImageUrl="~/Graphics/select.gif"
                        CommandName="select" OnClick="ibSelect_Click" ToolTip="select"></asp:ImageButton>
                </ItemTemplate>
                <HeaderStyle Width="22px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Update Date">
                <ItemTemplate>
                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("LastUpdDate") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Updated By">
                <ItemTemplate>
                    <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UpdatedBy") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Width="220px" />
            </asp:TemplateField>            

            <asp:TemplateField HeaderText="Changes">
                <ItemTemplate>
                    <asp:Label ID="lblChanges" runat="server" Text='<%# Bind("Changes") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
        </Columns>
        <PagerStyle HorizontalAlign="Right"></PagerStyle>
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy"></SelectedRowStyle>
        <HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
    </asp:GridView>
</asp:Content>
