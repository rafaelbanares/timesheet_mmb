﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class LeaveYearEnd : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        lblError.Text = "";
        if (!IsPostBack)
        {
            txtCurrentYear.Text = DateTime.Today.Year.ToString();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@Year", txtCurrentYear.Text));
        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usp_YearEndProcess_Lowe", sqlparam.ToArray());

        lblError.Text = "Process Complete";
        btnSubmit.Enabled = false;

    }
    protected void chkAgree_CheckedChanged(object sender, EventArgs e)
    {
        btnSubmit.Enabled = chkAgree.Checked;
    }
}