using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;

public partial class MultipleRestday : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            //updateurl();

            SecureUrl secureUrl = new SecureUrl(string.Format("MultiEmpSelect.aspx?ParentEmpsID={0}", hfSelectedEmp.ClientID));
            lbSelectEmp.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
        }
    }

    //private void updateurl()
    //{
    //    SecureUrl secureUrl = new SecureUrl(
    //    string.Format("MultiEmpSelect.aspx?EmpIDs={0}", hfSelectedEmp.Value));

    //    lbSelectEmp.Attributes.Remove("onclick");
    //    lbSelectEmp.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    //}

    private DataSet GetData()
    {
        //TextBox txtSearch = Master.FindControl("txtSearch") as TextBox;
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmpList", hfSelectedEmp.Value)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeDeptList", spParams);
        return ds;
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    protected override DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        gvEmp.SelectedIndex = -1;
        return base.SortDataTable(dataTable, isPageIndexChanging);
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGrid();
        //updateurl();
    }

    protected void btnChangeRestday_Click(object sender, EventArgs e)
    {
        string empids = hfSelectedEmp.Value.Trim();
        lblMessage.Text = "";

        if (empids.Length == 0)
        {
            lblMessage.Text = "Please select employees (step 1)";
            return;
        }

        //... multiple insert here.
        string[] empidlist = empids.Split('~');

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cblRestDay.Items.Count; i++)
        {
            if (cblRestDay.Items[i].Selected)
                sb.Append((i == 0 ? 7 : i).ToString());
        }

        SqlConnection conn = new SqlConnection(_ConnectionString);
        try
        {
            conn.Open();

            for (int i = 0; i < empidlist.Length - 1; i++)
            {
                string empid = empidlist[i];
                List<SqlParameter> sqlparam = new List<SqlParameter>();
                sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
                sqlparam.Add(new SqlParameter("@EmployeeID", empid));
                sqlparam.Add(new SqlParameter("@StartDate", txtDateStart.Text));
                sqlparam.Add(new SqlParameter("@EndDate", txtDateEnd.Text));
                sqlparam.Add(new SqlParameter("@RestdayCode", sb.ToString() ));
                sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
                sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
                sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
                sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
                sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeRestdayInsert", sqlparam.ToArray());
            }

            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
                conn = null;
            }

            SecureUrl url = new SecureUrl("BlankPage.aspx?Message=Change restday successfully applied.");
            Response.Redirect(url.ToString());       
        }
        catch
        {
            lblMessage.Text = "Change Restday failed";
            //... finally statement will close connection
        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
                conn = null;
            }
        }

    }
}
