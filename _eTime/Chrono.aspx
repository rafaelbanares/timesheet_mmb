﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Chrono.aspx.cs" Inherits="Chrono" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No title</title>
    <style type="text/css">
body
{
    font: 11px/1.5em  Verdana,Arial, Helvetica, sans-serif;
}

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border="1" style="background-color:ghostwhite; border-width:1; border-color:dodgerblue">
     <tr>
      <td>
          <strong>
          <asp:Label ID="lblTitle" runat="server"></asp:Label>
          </strong>
        &nbsp;<hr />
        <table border="0" cellpadding="0" cellspacing="3">
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="rbIn" runat="server" Checked="True" GroupName="group1" 
                        Text="Time In" />
&nbsp;
                    <asp:RadioButton ID="rbOut" runat="server" GroupName="group1" Text="Time Out" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    Employee ID:</td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" Width="150px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right">
                    Password:</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="150px">a</asp:TextBox></td>
            </tr>
            <tr style="display:none">
                <td align="right">
                    Company:</td>
                <td>
                    <asp:TextBox ID="txtCompanyID" runat="server" Enabled="False" Width="150px">EW01</asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td align="right">
                    <asp:Button ID="btnLogin" runat="server" Text="Go" onclick="btnLogin_Click" 
                        Width="120px" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtUsername"
                        ErrorMessage="- Employee ID is required"></asp:RequiredFieldValidator><br />
                    </td>
            </tr>
        </table>
      </td>
     </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
