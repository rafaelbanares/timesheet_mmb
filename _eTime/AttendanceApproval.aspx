<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AttendanceApproval.aspx.cs" Inherits="AttendanceApproval" Title="Attendance Approval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Approver Lists Admin"></asp:Label><br />
            <br />
            <table width="100%">
              <tr>
                <td align="right" style="width: 80px">
                    <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click" ValidationGroup="emplist" /></td>
              </tr>
            </table>
        </td>
        <td align="right">
            <table>
                <tr>
                    <td>
                        Attendance From:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator ID="mevDateStart" runat="server" ControlExtender="meeDateStart"
                            ControlToValidate="txtDateStart" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td>
                        To:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator ID="mevDateEnd" runat="server" ControlExtender="meeDateEnd"
                            ControlToValidate="txtDateEnd" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Show:&nbsp;<asp:DropDownList ID="ddlShow" runat="server" AutoPostBack="True" CssClass="ControlDefaults" OnSelectedIndexChanged="ddlShow_SelectedIndexChanged">
                            <asp:ListItem Value="all">All Transactions</asp:ListItem>
                            <asp:ListItem Selected="True" Value="error">Erroneous Transactions</asp:ListItem>
                        </asp:DropDownList></td>
                    <td align="left">
                        <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                            ToolTip="Go" /></td>
                </tr>
            </table>
        </td>
        <td align="center">
        </td>
    </tr>
</table>
    <br />

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20"
        Width="100%" AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" OnRowUpdating="gvEmp_RowUpdating">
        <Columns>
        
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litFullname" runat="server" Text='<%# Bind("FullName") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Date"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Day">
                <ItemTemplate>
                    <asp:Literal ID="litDay" runat="server" Text='<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "Date").ToString()).DayOfWeek.ToString().Substring(0,3).ToLower() %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Shift">
                <ItemTemplate>
                    <asp:Label ID="lbloshift" runat="server" Text='<%# Bind("orig_ShiftCode") %>' ToolTip='<%# Bind("orig_ShiftDesc") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Restday">
                <ItemTemplate>
                    <asp:Label ID="lblorestday" runat="server" Text='<%# Bind("orig_RestCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="In1">
                <ItemTemplate>
                    <asp:Literal ID="litIn1" runat="server" Text='<%# Bind("orig_In1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Out1">
                <ItemTemplate>
                    <asp:Literal ID="litOut1" runat="server" Text='<%# Bind("orig_Out1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="New In1">
                <EditItemTemplate>
                    <asp:TextBox ID="tbin1" runat="server" Text='<%# Bind("new_In1", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1" ControlToValidate="tbin1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbin1">
                    </ajaxToolkit:MaskedEditExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litNewIn1" runat="server" Text='<%# Bind("new_In1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="New Out1">
                <EditItemTemplate>
                    <asp:TextBox ID="tbout1" runat="server" Text='<%# Bind("new_Out1", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="tbout1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbout1">
                    </ajaxToolkit:MaskedEditExtender>                    
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litNewOut1" runat="server" Text='<%# Bind("new_Out1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason">
                <EditItemTemplate>
                    <asp:TextBox ID="tbreason" runat="server" Text='<%# Bind("reason") %>' Width="125px" MaxLength="200"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblreason" runat="server" Text='<%# Bind("reason") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <ItemStyle Width="42px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
        <asp:HiddenField ID="hfErrorMsg" runat="server" />
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ddlShow"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibSearch"></asp:AsyncPostBackTrigger>
    </Triggers>
    
</asp:UpdatePanel>

    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        PopupPosition="BottomRight" TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        PopupPosition="BottomRight" TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

</asp:Content>

