﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveYearEnd.aspx.cs" Inherits="LeaveYearEnd" Title="Leave Year-End Processing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Leave Year-End Processing"></asp:Label>
<br /><br /><br />

<table width="300px" border="1">
<tr>
    <td align="right">
        Current Year:
    </td>
    <td>
        <asp:TextBox ID="txtCurrentYear" runat="server" CssClass="TextBoxDate" 
            Width="50px" MaxLength="4"></asp:TextBox>
    </td>
</tr>
<tr>
    <td align="center" colspan="2">
        <asp:CheckBox ID="chkAgree" runat="server" AutoPostBack="True" 
            oncheckedchanged="chkAgree_CheckedChanged" 
            Text="I am sure to year-end process" />
    </td>
</tr>
<tr>
    <td colspan="2" align="center">
        <br />
        <asp:Button ID="btnSubmit" runat="server" CssClass="ControlDefaults" 
            Text="Process" OnClick="btnSubmit_Click" Enabled="False" /><br />
        <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
    </td>
</tr>
</table>


</asp:Content>

