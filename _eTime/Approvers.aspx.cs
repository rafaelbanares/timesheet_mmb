using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities; 

public partial class Approvers : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        hfErrorMsg.Value = "";

        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            body.Attributes.Add("onload", "load();");
            BindGrid();
        }
    }
    private void BindGrid()
    {
        hfIsAdd.Value = "0";
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }
    private void BindGridAdd()
    {
        gvEmp.PageIndex = 0;
        hfIsAdd.Value = "1";
        DataSet ds = GetData();
        DataView dv = SortDataTable(GetData().Tables[0], false);

        dv.Table.Rows.InsertAt(dv.Table.NewRow(), 0);
        gvEmp.DataSource = dv;
        gvEmp.EditIndex = 0;
        gvEmp.DataBind();
    }
 
    
    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@IsBlankData", hfIsAdd.Value)
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproverLoadByCompanyID", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex == gvEmp.EditIndex)
            {
                Button btnAppr = e.Row.FindControl("btnAppr") as Button;
                Button btnAlt1 = e.Row.FindControl("btnAlt1") as Button;
                Button btnAlt2 = e.Row.FindControl("btnAlt2") as Button;
                Button btnAlt3 = e.Row.FindControl("btnAlt3") as Button;

                TextBox txtAppr = e.Row.FindControl("txtAppr") as TextBox;
                TextBox txtAlt1 = e.Row.FindControl("txtAlt1") as TextBox;
                TextBox txtAlt2 = e.Row.FindControl("txtAlt2") as TextBox;
                TextBox txtAlt3 = e.Row.FindControl("txtAlt3") as TextBox;

                HiddenField hfAppr = e.Row.FindControl("hfAppr") as HiddenField;
                HiddenField hfAlt1 = e.Row.FindControl("hfAlt1") as HiddenField;
                HiddenField hfAlt2 = e.Row.FindControl("hfAlt2") as HiddenField;
                HiddenField hfAlt3 = e.Row.FindControl("hfAlt3") as HiddenField;

                HiddenField hfApprCompID = e.Row.FindControl("hfApprCompID") as HiddenField;
                HiddenField hfAlt1CompID = e.Row.FindControl("hfAlt1CompID") as HiddenField;
                HiddenField hfAlt2CompID = e.Row.FindControl("hfAlt2CompID") as HiddenField;
                HiddenField hfAlt3CompID = e.Row.FindControl("hfAlt3CompID") as HiddenField;

                SecureUrl urlAppr = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAppr.ClientID, txtAppr.ClientID, hfApprCompID.ClientID));
                SecureUrl urlAlt1 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAlt1.ClientID, txtAlt1.ClientID, hfAlt1CompID.ClientID));
                SecureUrl urlAlt2 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAlt2.ClientID, txtAlt2.ClientID, hfAlt2CompID.ClientID));
                SecureUrl urlAlt3 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAlt3.ClientID, txtAlt3.ClientID, hfAlt3CompID.ClientID));

                btnAppr.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAppr));
                btnAlt1.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAlt1));
                btnAlt2.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAlt2));
                btnAlt3.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAlt3));

                if (hfIsAdd.Value != "1")
                {
                    btnAppr.Enabled = false;
                    txtAppr.Enabled = false;
                }

                txtAppr.Enabled = false;
                txtAlt1.Enabled = false;
                txtAlt2.Enabled = false;
                txtAlt3.Enabled = false;

            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string approverID = gvEmp.DataKeys[e.RowIndex].Values["ApproverID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproverDelete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@ApproverID", approverID), new SqlParameter("@DBname", _KioskSession.DB) });

        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        HiddenField hfAppr = gvEmp.Rows[e.RowIndex].FindControl("hfAppr") as HiddenField;
        HiddenField hfAlt1 = gvEmp.Rows[e.RowIndex].FindControl("hfAlt1") as HiddenField;
        HiddenField hfAlt2 = gvEmp.Rows[e.RowIndex].FindControl("hfAlt2") as HiddenField;
        HiddenField hfAlt3 = gvEmp.Rows[e.RowIndex].FindControl("hfAlt3") as HiddenField;

        HiddenField hfApprCompID = gvEmp.Rows[e.RowIndex].FindControl("hfApprCompID") as HiddenField;
        HiddenField hfAlt1CompID = gvEmp.Rows[e.RowIndex].FindControl("hfAlt1CompID") as HiddenField;
        HiddenField hfAlt2CompID = gvEmp.Rows[e.RowIndex].FindControl("hfAlt2CompID") as HiddenField;
        HiddenField hfAlt3CompID = gvEmp.Rows[e.RowIndex].FindControl("hfAlt3CompID") as HiddenField;

        SqlParameter spIsAdd = new SqlParameter("@IsAddMode", hfIsAdd.Value);

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", hfApprCompID.Value));
        sqlparam.Add(new SqlParameter("@ApproverID", hfAppr.Value));
        sqlparam.Add(new SqlParameter("@Alternative1", hfAlt1.Value));
        sqlparam.Add(new SqlParameter("@Alternative2", hfAlt2.Value));
        sqlparam.Add(new SqlParameter("@Alternative3", hfAlt3.Value));

        sqlparam.Add(new SqlParameter("@Alt1CompID", hfAlt1CompID.Value));
        sqlparam.Add(new SqlParameter("@Alt2CompID", hfAlt2CompID.Value));
        sqlparam.Add(new SqlParameter("@Alt3CompID", hfAlt3CompID.Value));

        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));


        sqlparam.Add(spIsAdd);
        string errmsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproverValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();
        if (errmsg.Length > 0)
        {
            hfErrorMsg.Value = errmsg; 
            return;
        }
        sqlparam.Remove(spIsAdd);

        if (hfIsAdd.Value == "0")
        {
            sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproverUpdate", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproverInsert", sqlparam.ToArray());
        }

        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }

    private void RemoveApprover(string mainApprId, int approverLevelToRemove)
    {
        Approver appr = new Approver();
        if (appr.LoadByPrimaryKey(_KioskSession.CompanyID, mainApprId))
        {
            if (approverLevelToRemove == 1)
            {
                appr.s_Alternative1 = "";
                appr.s_Alt1CompID = "";
                appr.Save(); 
            }
            else if (approverLevelToRemove == 2)
            {
                appr.s_Alternative2 = "";
                appr.s_Alt1CompID = "";
                appr.Save();
            }
            else if (approverLevelToRemove == 3)
            {
                appr.s_Alternative3 = "";
                appr.s_Alt1CompID = "";
                appr.Save();
            }
        }

    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.IndexOf("clear") < 0) return;

        GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
        if (row == null) return;
        HiddenField hfAppr = gvEmp.Rows[row.RowIndex].FindControl("hfAppr") as HiddenField;
        if (hfAppr == null || hfAppr.Value.Trim() == "")
            return;

        if (e.CommandName == "clear1")
        {
            //level 1 backup approver
            RemoveApprover(hfAppr.Value, 1);
            gvEmp.EditIndex = -1;
            BindGrid();
        }
        else if (e.CommandName == "clear2")
        {
            //level 2 main approver
            RemoveApprover(hfAppr.Value, 2);
            gvEmp.EditIndex = -1;
            BindGrid();
        }
        else if (e.CommandName == "clear3")
        {
            //level 2 backup approver
            RemoveApprover(hfAppr.Value, 3);
            gvEmp.EditIndex = -1;
            BindGrid();
        }
    }
}
