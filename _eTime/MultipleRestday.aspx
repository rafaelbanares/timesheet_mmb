<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="MultipleRestday.aspx.cs" Inherits="MultipleRestday" Title="Bisneeds - Timekeeping - Multiple Change of Restday" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url) {
    popupWindow=wopen(url,"popupWindow",900,580);
}

function assignEmpID(s)
{
    $get("<%=hfSelectedEmp.ClientID%>").value = s;
    $get("<%=btnRefresh.ClientID%>").click();
    closewindow(popupWindow);
}
</script>   

    <br />
    <strong>Change of Restday (multiple employees).</strong><br />
            <br />
            
            
    <table cellpadding=5 cellspacing=5>
        <tr>
            <td valign="top">
            <strong>Step 1.</strong> Select employees to change restday.&nbsp;
                <br /><br />
            
                <table>
                    <tr>
                        <td valign="top">
                            <strong>Step 2.</strong> Select restday
                        </td>
                        <td>
                            <asp:CheckBoxList ID="cblRestDay" runat="server" Height="58px">
                                <asp:ListItem Selected="True">Sunday</asp:ListItem>
                                <asp:ListItem>Monday</asp:ListItem>
                                <asp:ListItem>Tuesday</asp:ListItem>
                                <asp:ListItem>Wednessday</asp:ListItem>
                                <asp:ListItem>Thursday</asp:ListItem>
                                <asp:ListItem>Friday</asp:ListItem>
                                <asp:ListItem Selected="True">Saturday</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
                <br />
            <strong>Step 3.</strong> Select date range.<br />
            <table>
                <tr>
                    <td>
                        Date From:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                            ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        To:</td>
                    <td align="left">
                        <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                            ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                </tr>
            </table><br />
            <asp:Button ID="btnChangeRestday" runat="server" Text="Change Restday" OnClick="btnChangeRestday_Click" /><br />
                <br />
                <asp:Label ID="lblMessage" runat="server" Font-Bold="True"></asp:Label><ajaxToolkit:MaskedEditExtender
                    ID="meeDateStart" runat="server" ErrorTooltipEnabled="True" Mask="99/99/9999"
                    MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
                </ajaxToolkit:MaskedEditExtender>
                <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
                    TargetControlID="txtDateStart">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
                    Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
                </ajaxToolkit:MaskedEditExtender>
                <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
                    TargetControlID="txtDateEnd">
                </ajaxToolkit:CalendarExtender>
            </td>
        <td  valign="top">            
            <asp:UpdatePanel ID="updEmp" UpdateMode="Conditional" runat="server">
            <ContentTemplate>            
                To select employees:<asp:LinkButton ID="lbSelectEmp" runat="server">click here</asp:LinkButton>
                <asp:HiddenField ID="hfSelectedEmp" runat="server" Value=" " />
                <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    CssClass="DataGridStyle" OnPageIndexChanging="gvEmp_PageIndexChanging" OnSorting="gvEmp_Sorting" PageSize="20" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="EmployeeID" SortExpression="EmployeeID">
                            <ItemTemplate>
                                <asp:Literal ID="litID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="FullName" SortExpression="FullName">
                            <ItemTemplate>
                                <asp:Literal ID="litName" runat="server" Text='<%# Bind("Fullname") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Department" SortExpression="Description">
                            <ItemTemplate>
                                <asp:Literal ID="litDesc" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <EmptyDataTemplate>
                        <span style="color:Red">No record found</span>
                    </EmptyDataTemplate>
                    <PagerStyle HorizontalAlign="Right" />
                    <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRefresh"></asp:AsyncPostBackTrigger>                           
            </Triggers>
            </asp:UpdatePanel>

                <asp:Button ID="btnRefresh" runat="server" OnClick="btnRefresh_Click" Style="display: none"
                    Text="Refresh" ValidationGroup="refresh" /></td>
        </tr>
    </table>
    <br />
</asp:Content>

