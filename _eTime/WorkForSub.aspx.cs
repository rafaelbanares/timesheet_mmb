﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class WorkForSub : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            UpdateLabels();
            BindGrid(false);
        }
        //Page.Title = "Bisneeds - Timekeeping - " + lblModule.Text;
    }

    private void UpdateLabels()
    {
        lblModule.Text = _KioskSession.WorkForSub + " File Maintenance";
        lbNew.Text = "New " + _KioskSession.WorkForSub + " Definition";
        ibNew.ToolTip = "New " + _KioskSession.WorkForSub + " Definition";
    }

    private void BindGrid(bool isAdd)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForSubLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });

        if (isAdd)
        {
            DataRow dr = ds.Tables[0].NewRow();
            ds.Tables[0].Rows.InsertAt(dr, 0);

            //ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

            gvMain.DataSource = ds.Tables[0];
            gvMain.EditIndex = 0; // ds.Tables[0].Rows.Count - 1;
            gvMain.DataBind();

            DropDownList ddlParent = gvMain.Rows[gvMain.EditIndex].FindControl("ddlParent") as DropDownList;
            //ddlParent.Attributes["onfocus"] = "javascript:this.select();";
            ddlParent.Focus();
        }
        else
        {
            gvMain.DataSource = ds.Tables[0];
            gvMain.DataBind();
        }
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid(false);
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool isUsed = DataBinder.Eval(e.Row.DataItem, "isUsed").ToString() == "1";

            if (e.Row.RowIndex != gvMain.EditIndex)
            {
                (e.Row.FindControl("ibDelete") as ImageButton).Visible = (!isUsed && gvMain.EditIndex == -1);
                (e.Row.FindControl("ibEdit") as ImageButton).Visible = gvMain.EditIndex == -1;

                //rsb
                bool active = bool.Parse(DataBinder.Eval(e.Row.DataItem, "Active").ToString());
                Image imgActive = e.Row.FindControl("imgActive") as Image;
                imgActive.Visible = active;
            }
            else
            {
                string memberof = DataBinder.Eval(e.Row.DataItem, "MemberOf").ToString();

                DropDownList ddlParent = e.Row.FindControl("ddlParent") as DropDownList;
                DataSet dsParent = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                    "SELECT WorkForID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkFor WHERE CompanyID = @CompanyID ORDER BY ShortDesc",
                                new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

                ddlParent.DataSource = dsParent.Tables[0];
                ddlParent.DataBind();
                ddlParent.Items.Insert(0, "");

                ddlParent.SelectedValue = ddlParent.Items.FindByText(memberof).Value;

                if (isUsed)
                {
                    ddlParent.Enabled = false;
                    ddlParent.ToolTip = "already used in work distribution";
                }

                bool active = true;
                bool.TryParse(DataBinder.Eval(e.Row.DataItem, "Active").ToString(), out active);
                CheckBox chkActive = e.Row.FindControl("chkActive") as CheckBox;
                chkActive.Checked = active;
            }
        }
    }

    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["WorkForSubID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForSubDelete",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@WorkForSubID", key), 
                new SqlParameter("@DBname", _KioskSession.DB) 
            });

        gvMain.EditIndex = -1;
        BindGrid(false);

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid(false);
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["WorkForSubID"].ToString();

        TextBox txtShortDesc = gvMain.Rows[e.RowIndex].FindControl("txtShortDesc") as TextBox;
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;
        DropDownList ddlParent = gvMain.Rows[e.RowIndex].FindControl("ddlParent") as DropDownList;
        CheckBox chkActive = gvMain.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@WorkForSubID", key));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@ShortDesc", txtShortDesc.Text));
        sqlparam.Add(new SqlParameter("@Description", txtDescription.Text));
        sqlparam.Add(new SqlParameter("@Active", chkActive.Checked));
        sqlparam.Add(new SqlParameter("@WorkForID", ddlParent.SelectedValue));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

        string errmsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForSubInsertUpdate",
            sqlparam.ToArray()).Tables[0].Rows[0]["ErrMsg"].ToString();

        if (errmsg != "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "insertfail", "alert('" + errmsg + "');", true);
        }
        else
        {
            gvMain.EditIndex = -1;
            BindGrid(false);
        }
    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
}
