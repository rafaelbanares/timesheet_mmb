<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="WorkDistrib.aspx.cs" Inherits="WorkDistrib" Title="Work Distribution" %>
<%@ Register src="EmployeeHeader.ascx" tagname="EmpHeader" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>

<script type="text/javascript">
    function ResetLeave()
    {
        var ddlLeave = document.getElementById('<% =ddlLeave.ClientID %>');
        var txtLvHours = document.getElementById('<% =txtLvHours.ClientID %>');        
        ddlLeave.value = '';
        txtLvHours.value = '';
    }
    
    function validatenumber(event, obj) {
        var code = (event.which) ? event.which : event.keyCode;
        var character = String.fromCharCode(code);

        if ((code >= 48 && code <= 57)) {
            if (obj.value == "0")
                obj.value = ""; //return false;
            return true;
        }
        else if (code == 46) { // Check dot
            if (obj.value.indexOf(".") < 0) {
                if (obj.value.length == 0)
                    obj.value = "0";
                return true;
            }
        }
        else if (code == 8 || code == 116) { // Allow backspace, F5
            return true;
        }
        else if (code >= 37 && code <= 40) { // Allow directional arrows
            return true;
        }

        return false;
    }
    function OnEditOk() {
        if (confirm('Are you sure?')) {
            return true;
        }
        return false;
    }
    function validatefield(obj) { // Remove dot if last character
        if (obj.value.indexOf(".") == obj.value.length - 1) {
            obj.value = obj.value.substring(0, obj.value.length - 1)
        } // Clear text box if not a number, incase user drags/drop letter into box
        else if (isNaN(obj.value)) {
            obj.value = "0.0";
        }
    }

    function statusupdate() {
        alert('Save completed');
        window.opener.refreshList();
    }

    function Validate() {
        // If no group name provided the whole page gets validated
        Page_ClientValidate('Trans');
    }

    function ValidatePop() {
        Page_ClientValidate('modaltrans');
    }

    function ValidateGrp(grp) {
        Page_ClientValidate(grp);
    }

</script> 

    <uc1:EmpHeader ID="EmpHeader1" runat="server" />
    <br />

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <table cellpadding="0" cellspacing="0" border="1">
        <tr align="center" class="DataGridHeaderStyle">
            <td>Date</td>
            <td style="width:40px">&nbsp</td>
            <td style="width:140px">Actual</td>
            <td style="width:160px">Correction In/Out</td>
            <td>Remarks</td>
            <td colspan="2" style="width:200px"><b>Hours Work Total</b></td>
            <td>Variance</td>
        </tr>
        <tr>
            <td align="center"><asp:Label ID="lblDate" runat="server" style="text-decoration: underline; font-weight: 700"></asp:Label></td>
            <td align="right"><asp:Label ID="lblIn" runat="server" Text="In:"></asp:Label></td>
            <td align="center"><asp:Label ID="lblOrigIn" runat="server" ></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDateIn" runat="server" Width="70px" AutoPostBack="True" 
                    ValidationGroup="Trans" OnChange="Validate();" ReadOnly="True"></asp:TextBox>
                <ajaxToolkit:MaskedEditExtender
                    ID="meeDateIn" runat="server" TargetControlID="txtDateIn" OnInvalidCssClass="MaskedEditError"
                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                </ajaxToolkit:MaskedEditExtender>
                <asp:RequiredFieldValidator ID="rfvDateIn" runat="server" ErrorMessage="Date is required" ToolTip="Date is required"
                    ControlToValidate="txtDateIn" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                
                <asp:TextBox ID="txtIn1" runat="server" Width="55px" AutoPostBack="True" ValidationGroup="Trans" ReadOnly="True"></asp:TextBox>
                 <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                    Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtIn1">
                </ajaxToolkit:MaskedEditExtender>
                <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1" ControlToValidate="txtIn1"
                    Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                    ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>

            </td>
            <td rowspan="2">
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="ControlDefaults" 
                    Width="250px" TextMode="MultiLine" Height="45px" MaxLength="100" 
                    ToolTip="remarks can only have 100 characters" ReadOnly="True"></asp:TextBox>
            </td>
            <td align="right">Time In-Out:</td>
            <td align="center">
                <asp:Label ID="lblTime" runat="server" Text="" Font-Bold="true"></asp:Label>
            </td>
            <td rowspan="2" align="center">
                <asp:Label ID="lblDiff" runat="server" Text="" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right"><asp:Label ID="lblOut" runat="server" Text="Out:"></asp:Label></td>
            <td align="center"><asp:Label ID="lblOrigOut" runat="server"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDateOut" runat="server" Width="70px" AutoPostBack="True" 
                    ValidationGroup="Trans" OnChange="Validate();" ReadOnly="True"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDateOut" runat="server" ErrorMessage="Date is required" ToolTip="Date is required"
                    ControlToValidate="txtDateOut" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                <ajaxToolkit:MaskedEditExtender
                    ID="meeDateOut" runat="server" TargetControlID="txtDateOut" OnInvalidCssClass="MaskedEditError"
                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                </ajaxToolkit:MaskedEditExtender>
                                
                <asp:TextBox ID="txtOut1" runat="server" Width="55px" AutoPostBack="True" 
                    ValidationGroup="Trans" ReadOnly="True"></asp:TextBox>
                <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                    Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtOut1">
                </ajaxToolkit:MaskedEditExtender>
                <ajaxToolkit:MaskedEditValidator ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="txtOut1"
                    Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                    ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                <asp:LinkButton ID="lbEditInOut" runat="server" CssClass="ControlDefaults" Font-Bold="True" >Edit</asp:LinkButton>
            </td>
            <td align="right">Work Distrib:</td>
            <td align="center">
                <asp:Label ID="lblRunning" runat="server" Text="" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnRoute" runat="server" CssClass="ControlDefaults" 
                    Text="Save/Route for Approval" onclick="btnRoute_Click" 
                    ValidationGroup="Trans" />
                <asp:Button ID="btnRevise" runat="server" CssClass="ControlDefaults" 
                    OnClientClick="return window.confirm( 'Are you sure you want to revise attendance details? \n If already approved, you will need to re-route your attendance for approval. ' );" 
                    Text="Modify" onclick="btnRevise_Click" ValidationGroup="Trans" />            
            </td>
            <td align="right" valign="middle">
                &nbsp; Leave Code:<br />
                Leave Hours:</td>
            <td>
                <asp:DropDownList ID="ddlLeave" runat="server" CssClass="ControlDefaults" 
                    DataTextField="Description" DataValueField="Code" AutoPostBack="True" onselectedindexchanged="ddlLeave_SelectedIndexChanged">
                </asp:DropDownList>
                <br />
                <asp:TextBox ID="txtLvHours" runat="server" 
                    CssClass="ControlDefaults" Width="40px" ValidationGroup="Trans"></asp:TextBox>
                <ajaxToolkit:MaskedEditExtender ID="meetxtLvHours" runat="server" 
                    ErrorTooltipEnabled="True" InputDirection="RightToLeft" Mask="9.99" 
                    MaskType="Number" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtLvHours">
                </ajaxToolkit:MaskedEditExtender>
                &nbsp;in decimal</td>
            <td align="center" colspan="1" rowspan="1" valign="middle">
                <asp:TextBox ID="txtRemarks2" runat="server" CssClass="ControlDefaults" 
                    Height="45px" MaxLength="100" TextMode="MultiLine" 
                    ToolTip="remarks can only have 100 characters" Width="250px"></asp:TextBox>
            </td>
            <td align="right">
                Regular Hours:<br />
                Excess Hours:</td>
            <td align="center">
                <asp:Label ID="lblRegHrs" runat="server" Font-Bold="True"></asp:Label>
                <br />
                <asp:Label ID="lblExcessHrs" runat="server" Font-Bold="True"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td align="right" valign="middle">
                <asp:Label ID="lblOTlabel" runat="server" Text="OT"></asp:Label>&nbsp;
                <asp:Label ID="lblOT" runat="server"></asp:Label>
            </td>
            <td>
                <table style="width:100%;">
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbFileOffset" runat="server" CssClass="ControlDefaults" 
                                Font-Bold="True" Visible="false" onclick="lbFileOffset_Click" >File Offset</asp:LinkButton>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLatelabel" runat="server" Text="Late&nbsp;"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblLate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblUTlabel" runat="server" Text="UT"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblUT" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblOffsetlabel" runat="server" Visible="false" Text="Offset"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblOffset" runat="server" Visible="false"></asp:Label>&nbsp;
                            <asp:LinkButton ID="lbCancelOfs" runat="server" CssClass="ControlDefaults" 
                                Font-Bold="True" OnClientClick="return window.confirm( 'Are you sure want to cancel offset?' );" onclick="lbCancelOfs_Click">cancel</asp:LinkButton>                            
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" colspan="1" valign="middle">
                <asp:Label ID="lblAction" runat="server" Text="Do you want to? "></asp:Label>
                &nbsp;
                <asp:Button ID="btnApprove" runat="server" CssClass="ControlDefaults" 
                    onclick="btnApprove_Click" 
                    OnClientClick="return window.confirm( 'Approve attendance details?' );" 
                    Text="Approve" />
                &nbsp;&nbsp;
                <asp:Button ID="btnDecline" runat="server" CssClass="ControlDefaults" 
                    Text="Decline" />            
                </td>
            <td align="right">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblNote" runat="server" Font-Bold="True" Font-Italic="True" 
                    ForeColor="Red" 
                    Text="*Note: incomplete attendance data cannot be routed for approval"></asp:Label>
                <br />
                <asp:Label ID="lblNote0" runat="server" Font-Bold="True" Font-Italic="True" 
                    ForeColor="Red" 
                    Text="*Please click refresh in the time transaction list to update values"></asp:Label>
            </td>
            <td align="center" colspan="1">
                <asp:Button ID="btnRecall" runat="server" CssClass="ControlDefaults" 
                    onclick="btnRecall_Click" 
                    OnClientClick="return window.confirm( 'Recall approval?' );" 
                    Text="Recall Work Distribution" ToolTip="Send Back For-Approval" />
            </td>
            <td colspan="3" align="center">
                Status:&nbsp;<asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                <br />
                <asp:Panel ID="pnlDclReason" runat="server" Visible="false">
                   Reason:&nbsp;<asp:Label ID="lblDeclinedReason" runat="server" Font-Bold="true"></asp:Label>
                </asp:Panel>
            </td>
        </tr>
    </table>
        <br />
    <asp:Label ID="lblAgree" runat="server" 
            Text="I affirm that the hours I have posted and will post in the future in the timesheet system is the correct and true amount of time worked 
            against the job numbers/projects under which the same time is being recorded. I understand that it is my responsibility to take all reasonable
            steps to record my time worked on behalf of the company in an accurate and timely manner. Failing to do so may result in disciplinary action,
            which may include termination."  Width ="950px"
             />
        <br />
    <hr />
    
    <asp:HiddenField ID="hfWorkDistID" runat="server" />
    
    <br />
    
    <table style="width:900px">
    <tr>
        <td><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">New Work Distribution</asp:LinkButton></td>
        <td align="right">
            &nbsp;</td>
        </tr>
    </table>
    
    <asp:GridView ID="gvMain" runat="server" DataKeyNames="row_ID"
        AutoGenerateColumns="False" CssClass="DataGridStyle" 
        onrowdatabound="gvMain_RowDataBound" OnRowCancelingEdit="gvMain_RowCancelingEdit"
        OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" 
        OnRowUpdating="gvMain_RowUpdating" Width="900px" >
        <Columns>

            <asp:TemplateField HeaderText="FlexHeader-WorkFor">
                <ItemStyle Width="110px" />
                <ItemTemplate>
                    <asp:Label ID="lblWorkFor" runat="server" Text='<%# Bind("WorkForShortDesc") %>' ></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlWorkFor" runat="server" CssClass="ControlDefaults" Width="105px" 
                        DataTextField="ShortDesc" DataValueField="WorkForID" 
                        ValidationGroup="Trans" AutoPostBack="true" 
                        onselectedindexchanged="ddlWorkFor_SelectedIndexChanged" ></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="FlexHeader-WorkForSub">
                <ItemStyle Width="110px" />
                <ItemTemplate>
                    <asp:Label ID="lblWorkForSub" runat="server" Text='<%# Bind("WorkForSubShortDesc") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlWorkForSub" runat="server" CssClass="ControlDefaults" Width="105px" 
                        DataTextField="ShortDesc" DataValueField="WorkForSubID" ValidationGroup="Trans"
                        AutoPostBack="true" onselectedindexchanged="ddlWorkForSub_SelectedIndexChanged"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Project">
                <ItemStyle Width="250px" />
                <ItemTemplate>
                    <asp:Label ID="lblProject" runat="server" Text='<%# Bind("ProjShortDesc") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlWorkForSubProj" runat="server" CssClass="ControlDefaults" Width="175px" 
                        DataTextField="ShortDesc" DataValueField="ProjIDDesc" ValidationGroup="Trans"
                        AutoPostBack="true" onselectedindexchanged="ddlWorkForSubProj_SelectedIndexChanged"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Task Description">
                <ItemStyle Width="40%" />
                <ItemTemplate>
                    <asp:Label ID="lblTask" runat="server" Text='<%# Bind("Task") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtTask" runat="server" Text='<%# Bind("Task") %>' ValidationGroup="Trans" Width="90%" MaxLength="500"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTask" runat="server" ControlToValidate="txtTask" 
                                ErrorMessage="Project/Task Cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Regular Hours">
                <ItemStyle Width="40px" HorizontalAlign="Right" />
                <ItemTemplate>
                    <asp:Label ID="lblRegHrs" runat="server" Text='<%# Bind("RegHrs") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtRegHrs" MaxLength="5" onkeypress="return validatenumber(event, this);" onblur="validatefield(this);" onpaste="return false;" runat="server" Text='<%# Bind("RegHrs") %>' ValidationGroup="Trans" Width="40px"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="WeekDay OT">
                <ItemStyle Width="40px" HorizontalAlign="Right" />
                <ItemTemplate>
                    <asp:Label ID="lblOT" runat="server" Text='<%# Bind("OT") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtOT" MaxLength="5" onkeypress="return validatenumber(event, this);" onblur="validatefield(this);" onpaste="return false;" runat="server" Text='<%# Bind("OT") %>' ValidationGroup="Trans" Width="40px"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Sat/Sun OT">
                <ItemStyle Width="40px" HorizontalAlign="Right" />
                <ItemTemplate>
                    <asp:Label ID="lblRstOT" runat="server" Text='<%# Bind("RstOT") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtRstOT" MaxLength="5" onkeypress="return validatenumber(event, this);" onblur="validatefield(this);" onpaste="return false;" runat="server" Text='<%# Bind("RstOT") %>' ValidationGroup="Trans" Width="40px"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <ItemStyle Width="42px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                    <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
    </asp:GridView>
    
    <asp:Panel ID="pnlPopup" runat="server" Width="600px" Style="display: none" CssClass="modalPopup">
        <div><br />
            <table style="width:100%;" cellpadding="1" cellspacing="1">
                <tr>
                    <td>Actual&nbsp;In</td>
                    <td>
                        <asp:TextBox ID="txtOrig_In" runat="server" Enabled="false" Width="120px"></asp:TextBox>
                    </td>
                    <td>Requested&nbsp;In</td>
                    <td>
                        <asp:TextBox ID="txtNewDateIn" runat="server" Width="70px" AutoPostBack="False" ValidationGroup="modaltrans" OnChange="ValidatePop();"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender
                            ID="meNewDateIn" runat="server" TargetControlID="txtNewDateIn" OnInvalidCssClass="MaskedEditError"
                            OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                            Mask="99/99/9999" ErrorTooltipEnabled="True">
                        </ajaxToolkit:MaskedEditExtender>
                        <asp:TextBox ID="txtNewTimeIn" runat="server" Width="55px" AutoPostBack="False" ValidationGroup="modaltrans"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="meNewIn" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                            Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                            OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewTimeIn">
                        </ajaxToolkit:MaskedEditExtender>
                        <ajaxToolkit:MaskedEditValidator ID="mveNewIn" runat="server" ControlExtender="meNewIn" ControlToValidate="txtNewTimeIn"
                                Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                                ValidationGroup="modaltrans">
                        </ajaxToolkit:MaskedEditValidator>
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="chkEarlyLogin" runat="server" CssClass="ControlDefaults" Text="Early Login?" />
                    </td>                    
                </tr>
                <tr>
                    <td>Actual&nbsp;Out&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtOrig_Out" runat="server" Enabled="false" Width="120px"></asp:TextBox>                        
                    </td>
                    <td>Requested&nbsp;Out</td>
                    <td>
                        <asp:TextBox ID="txtNewDateOut" runat="server" Width="70px" AutoPostBack="False" ValidationGroup="modaltrans" OnChange="ValidatePop();"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender
                            ID="MaskedEditExtender2" runat="server" TargetControlID="txtNewDateOut" OnInvalidCssClass="MaskedEditError"
                            OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                            Mask="99/99/9999" ErrorTooltipEnabled="True">
                        </ajaxToolkit:MaskedEditExtender>
                        
                        <asp:TextBox ID="txtNewTimeOut" runat="server" Width="55px" AutoPostBack="False" ValidationGroup="modaltrans"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="meNewOut" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                            Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                            OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewTimeOut">
                        </ajaxToolkit:MaskedEditExtender>
                        <ajaxToolkit:MaskedEditValidator ID="mveNewOut" runat="server" ControlExtender="meNewOut" ControlToValidate="txtNewTimeOut"
                                Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                                ValidationGroup="modaltrans">
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                </tr>                
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">Remarks</td>
                    <td>
                        <asp:TextBox ID="txtNewRemarks" runat="server" CssClass="ControlDefaults" Height="100px" MaxLength="100" TextMode="MultiLine" ToolTip="remarks can only have 100 characters" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNewRem" runat="server" ErrorMessage="*" ControlToValidate="txtNewRemarks" ValidationGroup="modaltrans"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width:100%; text-align:center">
                        <br />
                        <asp:Button ID="btnPopSave" runat="server" Text="Save" OnClientClick="ValidatePop()" onclick="btnPopSave_Click"/>
                        &nbsp;
                        <asp:Button ID="btnPopCancel" runat="server" Text="Cancel" />
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server"
      TargetControlId="lbEditInOut" 
      PopupControlID="pnlPopup"       
      CancelControlID="btnPopCancel"
      BackgroundCssClass="modalBackground"  />

    <asp:Panel ID="pnlOffset" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
        <div><br />
            <table style="width:100%;" cellpadding="1" cellspacing="1">
                <tr>
                    <td>Type</td>
                    <td>
                        Offset</td>
                </tr>
                <tr>
                    <td>Balance</td>
                    <td>
                        <asp:Label ID="lblOffsetbal" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Offset Hours</td>
                    <td>
                        <asp:TextBox ID="txtEditOfstHrs" runat="server" CssClass="ControlDefaults" 
                            ValidationGroup="Trans" Width="50px" ReadOnly="True" ></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="meeEditOfstHrs" 
                            runat="server" ErrorTooltipEnabled="True" InputDirection="RightToLeft" 
                            Mask="9.99" MaskType="Number" MessageValidatorTip="true" 
                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" 
                            TargetControlID="txtEditOfstHrs">
                        </ajaxToolkit:MaskedEditExtender>
                        <asp:Label ID="lblEditOfsMins" runat="server"></asp:Label>
                        <asp:HiddenField ID="hfOfsMins" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>Reason</td>
                    <td>
                        <asp:TextBox ID="txtNewRemarks2" runat="server" CssClass="ControlDefaults" 
                            Height="45px" MaxLength="100" TextMode="MultiLine" 
                            ToolTip="" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNewRmk2" runat="server" ErrorMessage="*" ControlToValidate="txtNewRemarks2" ValidationGroup="leavepop"></asp:RequiredFieldValidator>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2" style="width:100%; text-align:center">
                    <br />
                        <asp:Button ID="btnOffsetSave" runat="server" onclick="btnOffsetSave_Click" 
                            OnClientClick="ValidateGrp('leavepop')" Text="Save" />
                        &nbsp;
                        <asp:Button ID="btnOffsetCancel" runat="server" Text="Cancel" />                  
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpe2" runat="server"
      TargetControlId="lbFileOffset" 
      PopupControlID="pnlOffset"       
      CancelControlID="btnOffsetCancel"
      BackgroundCssClass="modalBackground"  />

    <asp:Panel ID="pnlDecline" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
        <div><br />
            <table style="width:100%;" cellpadding="1" cellspacing="1">
                <tr>
                    <td>Reason</td>
                    <td>
                        <asp:TextBox ID="txtDecReason" runat="server" CssClass="ControlDefaults" 
                            Height="65px" MaxLength="100" TextMode="MultiLine" 
                            ToolTip="" Width="340px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDecReason" runat="server" ErrorMessage="required" ControlToValidate="txtDecReason" ValidationGroup="declinepop"></asp:RequiredFieldValidator>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2" style="width:100%; text-align:center">
                    <br />
                        <asp:Button ID="btnDeclineWD" runat="server" onclick="btnDeclineWD_Click" 
                            OnClientClick="ValidateGrp('declinepop')" Text="Decline" />
                        &nbsp;
                        <asp:Button ID="btnDecReasonCancel" runat="server" Text="Cancel" />                  
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpe3" runat="server"
      TargetControlId="btnDecline" 
      PopupControlID="pnlDecline"
      CancelControlID="btnDecReasonCancel"
      BackgroundCssClass="modalBackground"  />
      
    <asp:HiddenField ID="hfIsAdd" runat="server" />
    <asp:HiddenField ID="hfOTCode" runat="server" />
    <asp:HiddenField ID="hfCompleted" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>        
    </asp:UpdatePanel>

</asp:Content>

