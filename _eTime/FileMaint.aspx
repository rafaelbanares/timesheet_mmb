﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="FileMaint.aspx.cs" Inherits="FileMaint" Title="File Maintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<table border="1" style="width:90%;border-collapse:collapse;">
    <tr class="DataGridHeaderStyle">
        <td colspan="<%=colspan%>"><asp:Label ID="hdr1" runat="server" ></asp:Label></td>
    </tr>
    <asp:Literal ID="det1" runat="server"></asp:Literal>
</table>

<br />
<br />
<br />


<table border="1" style="width:90%;border-collapse:collapse;">
    <tr class="DataGridHeaderStyle">
        <td colspan="<%=colspan%>"><asp:Label ID="hdr2" runat="server" ></asp:Label></td>
    </tr>
    <asp:Literal ID="det2" runat="server"></asp:Literal>
</table>


<br />
<br />
<br />


<table border="1" style="width:90%;border-collapse:collapse;">
    <tr class="DataGridHeaderStyle">
        <td colspan="<%=colspan%>"><asp:Label ID="hdr3" runat="server" ></asp:Label></td>
    </tr>
    <asp:Literal ID="det3" runat="server"></asp:Literal>
</table>

</asp:Content>
