﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="zAdminSQL.aspx.cs" Inherits="zAdminSQL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Connection String:
            <br />
            <asp:TextBox ID="tbConn" runat="server" Width="597px" Text=""></asp:TextBox>
        </div>
        <div>
            <br />
            Query String:<br />
            <asp:TextBox ID="tbText" runat="server" Width="597px" TextMode="MultiLine" 
                Height="166px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Execute" 
                onclick="Button1_Click" />
        </div>
        
        <div>
            <asp:GridView ID="gvResult" runat="server">
            </asp:GridView>
        </div>
        
        <div>
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        </div>
        
        
        
    </form>
</body>
</html>
