﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="EmpmasAttendanceAudit.aspx.cs" Inherits="EmpmasAttendanceAudit" Title="Employee Audit Trail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Audit Trail -"></asp:Label>&nbsp;
    <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname S."></asp:Label>
    <br />
    <br />
    <asp:GridView ID="gvEmp" runat="server" CssClass="DataGridStyle" Width="720"
        AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvEmp_PageIndexChanging"
        OnRowDataBound="gvEmp_RowDataBound" PageSize="5" 
        DataKeyNames="LastUpdBy, LastUpdDate,Changes" 
        onselectedindexchanged="gvEmp_SelectedIndexChanged">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ibSelect" runat="server" ImageUrl="~/Graphics/select.gif" CommandName="select" ToolTip="select"></asp:ImageButton>
                </ItemTemplate>
                <HeaderStyle Width="22px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Update Date">
                <ItemTemplate>
                    <asp:Literal ID="lblDate" runat="server" Text='<%# Bind("LastUpdDate") %>'></asp:Literal>
                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Updated By">
                <ItemTemplate>
                    <asp:Literal ID="lblUserName" runat="server" Text='<%# Bind("UpdatedByName") %>'></asp:Literal>
                </ItemTemplate>
                <HeaderStyle Width="220px" />
            </asp:TemplateField>            

            <asp:TemplateField HeaderText="Changes">
                <ItemTemplate>
                    <asp:Literal ID="lblChanges" runat="server" Text='<%# Bind("Changes") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>            
        </Columns>
        <PagerStyle HorizontalAlign="Right"></PagerStyle>
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy"></SelectedRowStyle>
        <HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
    </asp:GridView>
    <br />
    
    <asp:Panel ID="pnlMain" runat="server" Enabled="false">
    <table cellpadding="0" class="TableBorder1" width="720">
        <tr>
            <td class="DataGridHeaderStyle" colspan="6">
                Basic Information</td>
        </tr>
        <tr>
            <td>
                Employee ID.</td>
            <td>
                <asp:TextBox ID="txtEmployeeID" runat="server" CssClass="ControlDefaults" MaxLength="10"
                    Width="90px"></asp:TextBox>
            </td>
            <td style="color: #000000">
                Gender:</td>
            <td>
                <asp:DropDownList ID="ddlGender" runat="server" CssClass="ControlDefaults">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="M">Male</asp:ListItem>
                    <asp:ListItem Value="F">Female</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="color: #000000">
                Birth Date:</td>
            <td>
                <asp:TextBox ID="txtBirthDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Last Name:</td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server" CssClass="ControlDefaults" MaxLength="30"
                    Width="200px"></asp:TextBox>
            </td>
            <td>
                Address:</td>
            <td colspan="3">
                <asp:TextBox ID="txtAddress1" runat="server" CssClass="ControlDefaults" MaxLength="100"
                    Width="300px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                First Name:</td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server" CssClass="ControlDefaults" MaxLength="30"
                    Width="200px"></asp:TextBox>
            </td>
            <td>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtAddress2" runat="server" CssClass="ControlDefaults" MaxLength="100"
                    Width="300px"></asp:TextBox></td>
        </tr>
        <tr>
            <td valign="top">
                Middle Name:</td>
            <td valign="top">
                <asp:TextBox ID="txtMiddleName" runat="server" CssClass="ControlDefaults" MaxLength="30"
                    Width="200px"></asp:TextBox></td>
            <td>
                Department:</td>
            <td colspan="3" valign="top">
                <asp:TextBox ID="txtDepartment" runat="server" CssClass="ControlDefaults"
                    Width="278px" TextMode="MultiLine" Height="62px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" class="TableBorder1" width="720">
        <tr>
            <td class="DataGridHeaderStyle" colspan="4">
                Employment Information</td>
        </tr>
        <tr>
            <td>
                Employment Type:</td>
            <td>
                <asp:DropDownList ID="ddlEmploymentType" runat="server" CssClass="ControlDefaults">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="C">Contractual</asp:ListItem>
                    <asp:ListItem Value="P">Probationary</asp:ListItem>
                    <asp:ListItem Value="R">Regular</asp:ListItem>
                </asp:DropDownList></td>
            <td>
                Date Employed:</td>
            <td>
                <asp:TextBox ID="txtDateEmployed" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Rank:</td>
            <td>
                <asp:DropDownList ID="ddlRank" runat="server" CssClass="ControlDefaults">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="M">Manager</asp:ListItem>
                    <asp:ListItem Value="R">Rank &amp; File</asp:ListItem>
                    <asp:ListItem Value="S">Supervisor</asp:ListItem>
                </asp:DropDownList></td>
            <td>
                Date Regularized:</td>
            <td>
                <asp:TextBox ID="txtDateRegularized" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="height: 20px">
                Position:</td>
            <td style="height: 20px">
                <asp:TextBox ID="txtPosition" runat="server" CssClass="ControlDefaults" MaxLength="50"
                    Width="200px"></asp:TextBox></td>
            <td style="height: 20px">
                Date Teminated:</td>
            <td style="height: 20px">
                <asp:TextBox ID="txtDateTerminated" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" class="TableBorder1" width="720">
        <tr>
            <td class="DataGridHeaderStyle" colspan="4">
                Attendance Information</td>
        </tr>
        <tr>
            <td valign="top">
    
    <table>
        <tr>
            <td>
                Machine Swipe ID:                
            </td>
            <td>
                <asp:TextBox ID="txtSwipeID" runat="server" CssClass="ControlDefaults" MaxLength="15" Width="90px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Overtime Approver:</td>
            <td>
                <asp:TextBox ID="txtApproverOT" runat="server" CssClass="ControlDefaults" MaxLength="10"
                    Width="200px" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Base Shift:</td>
            <td>
                <asp:DropDownList ID="ddlShift" runat="server" DataTextField="Description" DataValueField="ShiftCode"></asp:DropDownList>
            </td>
        </tr>
    </table>
            </td>
            <td valign="top">
    
    <table>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlRD1" runat="server">
                <span style="text-decoration: underline">Base Restday:<br />
                </span>
                <br />
                <asp:CheckBoxList ID="cblRestDay1" runat="server">
                    <asp:ListItem Selected="True" Value="6">Saturday</asp:ListItem>
                    <asp:ListItem Selected="True" Value="7">Sunday</asp:ListItem>
                </asp:CheckBoxList>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:Panel ID="pnlRD2" runat="server">
                <asp:CheckBoxList ID="cblRestDay2" runat="server">
                    <asp:ListItem Value="1">Monday</asp:ListItem>
                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                    <asp:ListItem Value="3">Wednessday</asp:ListItem>
                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                    <asp:ListItem Value="5">Friday</asp:ListItem>
                </asp:CheckBoxList>
                </asp:Panel>
            </td>
        </tr>    
    </table>
            </td>
            <td valign="top">
    <table>
        <tr>
            <td valign="top">
                <span style="text-decoration: underline">
                Exemptions:</span><br />
                <br />
                <asp:CheckBox ID="chkTardyExempt" runat="server" Text="Tardy Exempt" /><br />
                <asp:CheckBox ID="chkUTExempt" runat="server" Text="Undertime Exempt" /><br />
                <asp:CheckBox ID="chkOTExempt" runat="server" Text="Overtime Exempt" /><br />
                <asp:CheckBox ID="chkNonswiper" runat="server" Text="Non-swiper" /></td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
    </asp:Panel>
    
    




</asp:Content>

