using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class AdminFileChangeRestDay : KioskPageUI
{
    string _EmployeeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        _EmployeeID = _Url["EmpID"];

        base.GetUserSession();
        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();

            lblFullname.Text = _Url["EmpName"];
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeRestDayLoadByDateRange",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _EmployeeID), 
                new SqlParameter("@StartDate", txtDateStart.Text), 
                new SqlParameter("@EndDate", txtDateEnd.Text), 
                new SqlParameter("@DBname", _KioskSession.DB)             
            });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeRestDayLoadByDateRange",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _EmployeeID), 
                new SqlParameter("@StartDate", txtDateStart.Text), 
                new SqlParameter("@EndDate", txtDateEnd.Text), 
                new SqlParameter("@IsBlankData", true),
                new SqlParameter("@DBname", _KioskSession.DB)             
            });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvMain.DataBind();
        hfIsAdd.Value = "1";
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string RestDaycode = DataBinder.Eval(e.Row.DataItem, "RestDayCode").ToString();
            string[] day =  { "Monday", "Tuesday", "Wednessday", "Thursday", "Friday", "Saturday", "Sunday" };

            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
            if (ibDelete != null)
            {                
                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
                Literal litDescription = e.Row.FindControl("litDescription") as Literal;

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < day.Length; i++)
                {
                    if (RestDaycode.Contains((i + 1).ToString()))
                    {
                        sb.Append("," + day[i]);
                    }
                }
                if (sb.Length > 0)
                    litDescription.Text = sb.ToString().Substring(1);
            }
            else
            {                
                CheckBoxList cblRestDay = e.Row.FindControl("cblRestDay") as CheckBoxList;
                //RestDaycode  1,7

                for (int i = 0; i < cblRestDay.Items.Count; i++)
                {
                    cblRestDay.Items[i].Selected = i == 0 ? RestDaycode.Contains("7") : RestDaycode.Contains(i.ToString());
                }
            }
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeRestDayDelete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@ID", key), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        TextBox txtStart = gvMain.Rows[e.RowIndex].FindControl("txtStart") as TextBox;
        TextBox txtEnd = gvMain.Rows[e.RowIndex].FindControl("txtEnd") as TextBox;

        StringBuilder sb = new StringBuilder();
        CheckBoxList cblRestDay = gvMain.Rows[e.RowIndex].FindControl("cblRestDay") as CheckBoxList;
        for (int i = 0; i < cblRestDay.Items.Count; i++)
        {
            if (cblRestDay.Items[i].Selected)
                sb.Append( (i==0 ? 7:i).ToString() );
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _EmployeeID));
        sqlparam.Add(new SqlParameter("@StartDate", txtStart.Text));
        sqlparam.Add(new SqlParameter("@EndDate", txtEnd.Text));
        sqlparam.Add(new SqlParameter("@RestDayCode", sb.ToString()));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (hfIsAdd.Value == "0")
        {
            sqlparam.Add(new SqlParameter("@ID", key));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeRestDayUpdate", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeRestDayInsert", sqlparam.ToArray());
        }

        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
}