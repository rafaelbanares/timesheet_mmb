﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;

public partial class EmpmasAttendanceAudit : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            lblFullname.Text = _Url["EmployeeName"];
            populateShift();

            BindAuditGrid();
            if (gvEmp.Rows.Count > 0)
            {
                gvEmp.SelectedIndex = 0;
                bindEmpData();
            }
        }
    }

    private void bindData(string updatedBy, string updateDate, string changes)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                MainDB + ".dbo.usa_AudTrailEmployeeLoadByPrimaryKey",
                new SqlParameter[] { 
                        new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                        new SqlParameter("@EmployeeID", _Url["EmployeeID"]), 
                        new SqlParameter("@LastUpdBy", updatedBy ),
                        new SqlParameter("@LastUpdDate", updateDate ),
                        new SqlParameter("@DBName", _KioskSession.DB) 
                });

        if (ds.Tables[0].Rows.Count == 1)
        {
            DataRow dr = ds.Tables[0].Rows[0];
            AuditCheck ac = new AuditCheck(dr, changes);

            txtEmployeeID.Text = dr["EmployeeID"].ToString();

            ac.Assign(txtLastName, "LastName");
            ac.Assign(txtFirstName, "FirstName");
            ac.Assign(txtMiddleName, "MiddleName");
            ac.Assign(txtAddress1, "Address1");
            ac.Assign(txtAddress2, "Address2");
            ac.Assign(txtBirthDate, "BirthDate", Tools.ShortDate(dr["BirthDate"], _KioskSession.DateFormat));

            ac.Assign(ddlGender, "Gender");
            ac.Assign(ddlEmploymentType, "EmploymentType");
            ac.Assign(ddlRank, "Rank");

            txtDepartment.Text = " " + 
                dr["Level1Disp"].ToString() + "\r" +
                dr["Level2Disp"].ToString() + "\r" +
                dr["Level3Disp"].ToString() + "\r" +
                dr["Level4Disp"].ToString();

            ac.Compare(txtDepartment, "Level");

            ac.Assign(txtPosition, "Position");
            ac.Assign(txtDateEmployed, "DateEmployed", Tools.ShortDate(dr["DateEmployed"], _KioskSession.DateFormat));
            ac.Assign(txtDateRegularized, "DateRegularized", Tools.ShortDate(dr["DateRegularized"], _KioskSession.DateFormat));
            ac.Assign(txtDateTerminated, "DateTerminated", Tools.ShortDate(dr["DateTerminated"], _KioskSession.DateFormat));

            ac.Assign(txtSwipeID, "SwipeID");
            //ac.Assign(txtApproverLeave, "ApproverLeave", dr["ApproverLeaveName"].ToString() );
            ac.Assign(txtApproverOT, "ApproverOT", dr["ApproverOTName"].ToString() );

            ac.Assign(ddlShift, "ShiftCode");

            string restday = dr["RestDay"].ToString();
            cblRestDay1.Items[0].Selected = restday.Contains("6"); // Saturday
            cblRestDay1.Items[1].Selected = restday.Contains("7"); // Sunday
            cblRestDay2.Items[0].Selected = restday.Contains("1"); // Monday
            cblRestDay2.Items[1].Selected = restday.Contains("2"); // Tuesday
            cblRestDay2.Items[2].Selected = restday.Contains("3"); // Wednessday
            cblRestDay2.Items[3].Selected = restday.Contains("4"); // Thursday
            cblRestDay2.Items[4].Selected = restday.Contains("5"); // Friday

            ac.Compare(pnlRD1, "Restday");
            ac.Compare(pnlRD2, "Restday");

            chkTardyExempt.Checked = (bool)dr["TardyExempt"];
            chkUTExempt.Checked = (bool)dr["UTExempt"];
            chkOTExempt.Checked = (bool)dr["OTExempt"];
            chkNonswiper.Checked = (bool)dr["Nonswiper"];

        }
    }

    private void populateShift()
    {
        //... populate Shift dropdown
        DataSet dsShift = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT ShiftCode, Description FROM " + _KioskSession.DB + ".dbo.Shift WHERE CompanyID = @CompanyID",
                        new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlShift.DataSource = dsShift.Tables[0];
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, "");
    }

    #region grid aread
    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmployeeID", _Url["EmployeeID"] )
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AudTrailByEmployeeID", spParams);
        return ds;
    }

    private void BindAuditGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);
        gvEmp.DataBind();
    }

    protected override DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        gvEmp.SelectedIndex = -1;
        return base.SortDataTable(dataTable, isPageIndexChanging);
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindEmpData();
    }
    #endregion

    private void bindEmpData()
    {
        string updatedBy = gvEmp.DataKeys[gvEmp.SelectedIndex].Values["LastUpdBy"].ToString();
        string updateDate = gvEmp.DataKeys[gvEmp.SelectedIndex].Values["LastUpdDate"].ToString();
        string changes = gvEmp.DataKeys[gvEmp.SelectedIndex].Values["Changes"].ToString();

        bindData(updatedBy, updateDate, changes);
    }

    public class AuditCheck
    {
        DataRow _RowEmployee;
        string _Changes;

        public AuditCheck(DataRow dr, string changes)
        {
            _RowEmployee = dr;
            _Changes = changes.ToLower();
        }

        //public DataRow RowEmployee { set {_RowEmployee = value; } }
        //public string Changes { set {Changes = value; } }

        public void Assign(DropDownList ddlObject, string datacolumn)
        {
            ddlObject.Text = _RowEmployee[datacolumn].ToString();
            Compare(ddlObject, datacolumn);
        }

        public void Assign(CheckBox chkObject, string datacolumn)
        {
            chkObject.Checked = (bool)_RowEmployee[datacolumn];
            Compare(chkObject, datacolumn);
        }

        public void Assign(TextBox txtObject, string datacolumn)
        {
            txtObject.Text = _RowEmployee[datacolumn].ToString();
            Compare(txtObject, datacolumn);
        }

        public void Assign(TextBox txtObject, string datacolumn, string value)
        {
            txtObject.Text = value;
            Compare(txtObject, datacolumn);
        }

        public void Compare(WebControl oWebControl, string datacolumn)
        {
            if (_Changes.Contains(datacolumn.ToLower()))
                oWebControl.BackColor = System.Drawing.Color.Yellow;
            else
                oWebControl.BackColor = System.Drawing.Color.White;
        }

        //private void Compare(TextBox txtObject, string datacolumn)
        //{
        //    if (_Changes.Contains(datacolumn))
        //        txtObject.BackColor = System.Drawing.Color.Yellow;
        //}

    }

}

