<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ztestservertime.aspx.cs" Inherits="ztestservertime" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Test Server Time</title>
    <script type="text/javascript">
        var serverDate;
        function getServerDate(){
           serverDate=new Date("<%=DateTime.Now.ToString() %>"); //server date and time, change server-side code accordingly
        }

        function tick(){
           serverDate.setSeconds(serverDate.getSeconds()+1);  
           var min = serverDate.getMinutes();
           if (min<10) min="0"+min;
           var sec = serverDate.getSeconds();
           if (sec<10) sec="0"+sec;
           window.status = serverDate.getHours() + ":" + min + ":" + sec;
        }

        window.onload=function(){
           getServerDate();
           setInterval("tick()", 1000);
        }    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
