<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChronoLogger.aspx.cs" Inherits="ChronoLogger" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>eTime Login/Logout System</title>

<script type="text/javascript">
var serverDate;
function getServerDate(){
   serverDate=new Date("<%=DateTime.Now.ToString() %>"); //server date and time, change server-side code accordingly
}

function startTime()
{
serverDate.setSeconds(serverDate.getSeconds()+1);  
//var d=new Date();
var d=serverDate;
$get('hdTime').value = d.toLocaleDateString() + " " + d.toLocaleTimeString() ;
// add a zero in front of numbers<10
var mm=addZero(d.getMonth()+1);
var dd=addZero(d.getDate());
var yy=addZero(d.getFullYear());
var h= addZero(d.getHours());
var m= addZero(d.getMinutes());
var s= addZero(d.getSeconds());

$get('lbDate').innerHTML = mm+"/"+dd+"/"+yy;
$get('lbTime').innerHTML = h+":"+m+":"+s;

//alert($get('lbTime').innerHTML);
t=setTimeout('startTime()',1000);
}

function addZero(i)
{
if (i<10)
  {
  i="0" + i;
  }
return i;
}

document.onkeypress = keyIO;
function keyIO(e)
{
    e=e||event;
    var keynum;
    var keychar;

    if(window.event) // IE
        keynum = e.keyCode;
    else if(e.which) // Netscape/Firefox/Opera
        keynum = e.which;

    try
    {
        $get("tbKeyCode").value = keynum;
        if (keynum == 73 || keynum == 105 || keynum == 79 || keynum == 111)
        {        
            assignIO();
        }
    }
    catch(e)
    {
    }
    return false;    
}

function assignIO()
{
    var keynum  = $get("tbKeyCode").value;    
    document.onkeypress = "";
    $get('lblMessage').innerHTML = "";
    $get('dio').style.display = "none";
    $get('dlogin').style.display = "";
    
    //$get('tbUID').value = "";
    $get('tbPWD').value = "";
    
    if (keynum == 73 || keynum == 105)
    {
        $get('bOK').value = "Time IN";
        $get('hdIO').value = "I";
    }
    if (keynum == 79 || keynum == 111)
    {
        $get('bOK').value = "Time OUT";
        $get('hdIO').value = "O";
    }
    
    $get('tbUID').focus();
}

function keyPressHandler(e) {
  var kC  = (window.event) ?    // MSIE or Firefox?
             event.keyCode : e.keyCode;
  var Esc = (window.event) ?   
            27 : e.DOM_VK_ESCAPE // MSIE : Firefox
  if(kC==Esc)
    form1.submit();
     //alert("Esc pressed");
}

</script>

</head>
<body id="bmain" onload="getServerDate();startTime();" onkeypress="keyPressHandler(event)">
    <form id="form1" runat="server">
    <asp:ScriptManager id="scm1" runat="server"></asp:ScriptManager>
    <div>
    <table width="100%">
        <tr>
            <td align="center">
              <table width="500" border=1>
                <tr>
                  <td>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lbDate" runat="server" Font-Size="XX-Large"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbTime" runat="server" Font-Size="XX-Large"></asp:Label></td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hdIO" runat="server" />
                    <br />
                    <br />
                    <asp:Label ID="lblMessage" runat="server" Font-Size="Large"></asp:Label><br />
                    <asp:HiddenField ID="hdTime" runat="server" />
                    <br />
                    
                    <div id="dio">
                        <asp:Label ID="lblIO" runat="server" Text="Press (I) for Time In or (O) for Time Out"></asp:Label>
                        <asp:TextBox ID="tbKeyCode" runat="server" style="display:none"></asp:TextBox>
                        <br /><br /><br /><br /><br /><br /><br /><br />
                    </div>                
                    <div id="dlogin" style="display:none" >
                        <table>
                        <tr>
                            <td>
                                Employee ID:
                            </td>
                            <td>
                                <asp:TextBox ID="tbUID" runat="server" MaxLength="15" Width="108px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password:
                            </td>
                            <td>
                                <asp:TextBox ID="tbPWD" runat="server" MaxLength="60" Width="108px" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="bOK" runat="server" Text="TimeIO" OnClick="bOK_Click" />&nbsp;
                                <asp:Button ID="bCancel" runat="server" Text="Cancel" ValidationGroup="None" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br />
                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="tbUID">- Employee ID is required</asp:RequiredFieldValidator></td>
                        </tr>
                    </table>
                    </div>
                    <asp:Literal ID="litScript" runat="server"></asp:Literal>
                  
                  </td>
                </tr>
              </table>
            </td>            
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
