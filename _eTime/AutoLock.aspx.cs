using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;

public partial class AutoLock : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        TransPeriod tp = new TransPeriod();
        object[] parameters = { _KioskSession.CompanyID, DateTime.Today.Year };
        string sql = "select * from TransPeriod where companyid = {0} and year(StartDate) = {1} order by StartDate ";
        tp.DynamicQuery(sql, parameters);

        gvMain.DataSource = tp.ToDataSet().Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }
 

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {        
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {   
    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {        
        Literal litStartDate = gvMain.Rows[e.RowIndex].FindControl("litStartDate") as Literal;
        Literal litEndDate = gvMain.Rows[e.RowIndex].FindControl("litEndDate") as Literal;
        TextBox txtEmployee_lock_start = gvMain.Rows[e.RowIndex].FindControl("txtEmployee_lock_start") as TextBox;
        TextBox txtEmployee_lock_end = gvMain.Rows[e.RowIndex].FindControl("txtEmployee_lock_end") as TextBox;
        TextBox txtApprover_lock_start = gvMain.Rows[e.RowIndex].FindControl("txtApprover_lock_start") as TextBox;
        TextBox txtApprover_lock_end = gvMain.Rows[e.RowIndex].FindControl("txtApprover_lock_end") as TextBox;

        TransPeriod tp = new TransPeriod();
        if (tp.LoadByPrimaryKey(_KioskSession.CompanyID, Convert.ToDateTime(litStartDate.Text), Convert.ToDateTime(litEndDate.Text)))
        {
            tp.s_Employee_lock_start = txtEmployee_lock_start.Text;
            tp.s_Employee_lock_end = txtEmployee_lock_end.Text;
            tp.s_Approver_lock_start = txtApprover_lock_start.Text;
            tp.s_Approver_lock_end = txtApprover_lock_end.Text;
            tp.Save();
        }
 
        gvMain.EditIndex = -1;
        BindGrid();

    }
    
    
}