﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

using Microsoft.ApplicationBlocks.Data;
using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class WorkDistribApprList : KioskPageUI
{
    protected readonly string sBackColor_HoliRestday = "#c0c0c0";
    protected readonly string sBackColor_Erroneous = "#ffd700";
    protected readonly string sBackColor_LeaveWhole = "#90ee90";
    protected readonly string sBackColor_LeavePartial = "#CCFF66";
    protected readonly string sBackColor_ForApproval = "#f4a460";
    protected readonly string sBackColor_Incomplete = "#CC99FF";

    string approverno;
    string stage;

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        //... must check if right query string is passed
        if (_Url["approverno"] == "")
            Response.Redirect("LoginEmployee.aspx");
        else
        {
            approverno = _Url["approverno"];
            stage = (approverno == "1" ? "1" : "2");
        }

        if (!IsPostBack)
        {
            DropDownList dl = (DropDownList)EmpHeader1.FindControl("ddlDisplay");
            dl.Items.Add(new ListItem("Incomplete", "incomplete"));
            dl.Items.Add(new ListItem("No in/out", "notrans"));
            BindGrid();
        }
    }

    private void BindGrid()
    {
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.DataBind();
    }

    private DataSet GetData()
    {
        HiddenField hdDateStart = (HiddenField)EmpHeader1.FindControl("hdDateStart");
        HiddenField hdDateEnd = (HiddenField) EmpHeader1.FindControl("hdDateEnd");
        HiddenField hdDateSelection = (HiddenField)EmpHeader1.FindControl("hdDateSelection");

        if (hdDateSelection.Value == "")
        {
            hdDateStart.Value = _KioskSession.StartDate.ToShortDateString();
            hdDateEnd.Value = _KioskSession.EndDate.ToShortDateString();
        }

        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
            ,new SqlParameter("@ApproverNo", approverno)
            ,new SqlParameter("@StartDate", hdDateStart.Value )
            ,new SqlParameter("@EndDate", hdDateEnd.Value )
            ,new SqlParameter("@Filter", EmpHeader1.Display )
            ,new SqlParameter("@Search", EmpHeader1.Search )
        };
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistApprover", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }

    protected void gvEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.PageIndex = e.NewPageIndex;
        gvEmployees.DataBind();
    }

    protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmployees.EditIndex)
            {
                ImageButton ibWorkDist = e.Row.FindControl("ibWorkDist") as ImageButton;
                Label lblStatus = e.Row.FindControl("lblStatus") as Label;
                
                string date = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "Date").ToString(), _KioskSession.DateFormat);
                string employeeid = DataBinder.Eval(e.Row.DataItem, "EmployeeID").ToString();
                string url = string.Format("WorkDistrib.aspx?Date={0}&EmpID={1}&approverno={2}", date, employeeid, approverno);

                SecureUrl su = new SecureUrl(url);
                ibWorkDist.Attributes["onclick"] = "javascript:openWD('" + su.ToString() + "');return false;";

                string otcode = DataBinder.Eval(e.Row.DataItem, "OTCode").ToString().Trim();
                decimal leavedays = (decimal)DataBinder.Eval(e.Row.DataItem, "Leave");
                decimal absent = (decimal)DataBinder.Eval(e.Row.DataItem, "Absent");
                string leavecode = DataBinder.Eval(e.Row.DataItem, "LeaveCode").ToString().Trim();

                int countdata = (int)DataBinder.Eval(e.Row.DataItem, "CountData");
                bool completed = (bool)DataBinder.Eval(e.Row.DataItem, "Completed");
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().Trim();

                //ibWorkDist.Visible = completed;

                if (!completed && otcode != "REG")
                    lblStatus.Text = "Restday/Holiday";
                else if (leavedays >= 1 || absent == 1)
                    lblStatus.Text = leavedays >= 1 ? "On leave" : "Absent";
                else if (!completed && countdata == 0)
                    lblStatus.Text = "No Work Details";
                else if (!completed && countdata > 0)
                    lblStatus.Text = "Incomplete/Error";
                else
                    lblStatus.Text = status;

                #region assign background color
                if (completed && status.ToLower() == "for approval")
                    e.Row.BackColor = ColorTranslator.FromHtml(sBackColor_ForApproval);
                else if (!completed && countdata > 0)
                    e.Row.BackColor = ColorTranslator.FromHtml(sBackColor_Incomplete);
                else if (otcode != "REG")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_HoliRestday);
                else if (leavedays >= 1 || absent == 1)
                    e.Row.BackColor = ColorTranslator.FromHtml(sBackColor_LeaveWhole);
                else if (!completed && countdata == 0)
                    e.Row.BackColor = ColorTranslator.FromHtml(sBackColor_Erroneous);
                else if (leavecode != "" && leavedays < 1)
                    e.Row.BackColor = ColorTranslator.FromHtml(sBackColor_LeavePartial);
                #endregion
            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        EmpHeader1.UpdateURL();
        BindGrid();
    }
}
