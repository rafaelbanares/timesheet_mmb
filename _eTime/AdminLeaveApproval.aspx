﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AdminLeaveApproval.aspx.cs" Inherits="AdminLeaveApproval" Title="Leave Approval List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;
    
    closechild();
}
function closechild()
{
    $get("<%=ibGo.ClientID%>").click();
    closewindow(popupWindow);
}
</script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Leave Approval List"></asp:Label><br />
            <br />
            <table width="100%">
              <tr>
                <td align="right" style="width: 80px">
                    <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" 
                        OnClick="ibGo_Click" ValidationGroup="emplist" Enabled="False" /></td>
              </tr>
            </table>
        </td>
        <td align="right">
            <table>
                <tr>
                    <td>
                        Attendance From:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator ID="mevDateStart" runat="server" ControlExtender="meeDateStart"
                            ControlToValidate="txtDateStart" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td>
                        To:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator ID="mevDateEnd" runat="server" ControlExtender="meeDateEnd"
                            ControlToValidate="txtDateEnd" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Show:&nbsp;<asp:DropDownList ID="ddlShow" runat="server" AutoPostBack="True" CssClass="ControlDefaults" OnSelectedIndexChanged="ddlShow_SelectedIndexChanged">
                            <asp:ListItem Value="all">All Transactions</asp:ListItem>
                            <asp:ListItem Value="For Approval">For Approval</asp:ListItem>
                            <asp:ListItem>For Final Approval</asp:ListItem>
                        </asp:DropDownList></td>
                    <td align="left">
                        <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                            ToolTip="Go" /></td>
                </tr>
            </table>
        </td>
        <td align="center">
            <table>
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lnkClear" runat="server" ToolTip="Clear Selection" 
                            onclick="lnkClear_Click">clear</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                            Width="279px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<hr />
<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
<asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" 
            OnRowUpdating="gvEmp_RowUpdating" Width="100%" 
            DataKeyNames="id,status" onrowdeleting="gvEmp_RowDeleting">
        <Columns>
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litFullname" runat="server" Text='<%# Bind("FullName") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Date Filed" SortExpression="DateFiled">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "DateFiled"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Start Date" SortExpression="StartDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txtStart" runat="server" Text='<%# Bind("StartDate", "{0:MM/dd/yyyy}") %>'
                        CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtStart" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                    </asp:ImageButton>&nbsp;<asp:RequiredFieldValidator ID="rfvStart" runat="server"
                        ErrorMessage="Date start is required" ControlToValidate="txtStart" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                            ID="meeStart" runat="server" TargetControlID="txtStart" OnInvalidCssClass="MaskedEditError"
                            OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                            Mask="99/99/9999" ErrorTooltipEnabled="True">
                        </ajaxToolkit:MaskedEditExtender>
                    <ajaxToolkit:CalendarExtender ID="caleStart" runat="server" TargetControlID="txtStart"
                        PopupButtonID="ibtxtStart">
                    </ajaxToolkit:CalendarExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litStartDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "StartDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="End Date" SortExpression="EndDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEnd" runat="server" Text='<%# Bind("EndDate", "{0:MM/dd/yyyy}") %>'
                        CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtEnd" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                    </asp:ImageButton>
                    <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ErrorMessage="Date end is required"
                        ControlToValidate="txtEnd" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                            ID="meeEnd" runat="server" TargetControlID="txtEnd" OnInvalidCssClass="MaskedEditError"
                            OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                            Mask="99/99/9999" ErrorTooltipEnabled="True">
                        </ajaxToolkit:MaskedEditExtender>
                    <ajaxToolkit:CalendarExtender ID="caleEnd" runat="server" TargetControlID="txtEnd"
                        PopupButtonID="ibtxtEnd">
                    </ajaxToolkit:CalendarExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litEndDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "EndDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="No. of days">
                <EditItemTemplate>
                    <asp:TextBox ID="txtLeaveDays" runat="server" CssClass="ControlDefaults" Width="55px" ValidationGroup="Trans" Text='<%# Bind("Days") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litDays" runat="server" Text='<%# Bind("Days") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Leave">
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlLeave" runat="server" CssClass="ControlDefaults" Width="138px" DataTextField="Description" DataValueField="Code" ValidationGroup="Trans">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvLeave" runat="server" ErrorMessage="Leave code is required" ControlToValidate="ddlLeave">*</asp:RequiredFieldValidator></td>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litLeave" runat="server" Text='<%# Bind("Code") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason">
                <EditItemTemplate>
                    <asp:TextBox ID="tbreason" runat="server" Text='<%# Bind("reason") %>' 
                        Width="100px" MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReason" runat="server" 
                        ControlToValidate="tbreason" 
                        ErrorMessage="Please indicate a reason for corrrections" 
                        ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status" SortExpression="Stage">
                <ItemTemplate>
                    <asp:Literal ID="litStage" runat="server" Text='<%# Bind("Stage") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle Width="40px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" Visible="false" ImageUrl="~/Graphics/edit.gif" />
                    <asp:ImageButton ID="ibDelete" runat="server" ImageUrl="~/Graphics/delete.gif" Visible="false" CommandName="delete" ToolTip="Delete" 
                        OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );"></asp:ImageButton>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <table width="100%">
        <tr>
            <td style="width:40%"></td>
            <td align="right"><asp:Button ID="btnForApproval" runat="server" Text="Approve all - For Approval" Width="220px" onclick="btnForApproval_Click" /></td>
            <td align="right"><asp:Button ID="btnForFinalApproval" runat="server" Text="Approve all - For Final Approval" Width="220px" onclick="btnForFinalApproval_Click" /></td>
        </tr>
    </table>

    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ddlShow"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
    </Triggers>    
</asp:UpdatePanel>


    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        PopupPosition="BottomRight" TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        PopupPosition="BottomRight" TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

    <asp:HiddenField
    ID="hfLevels" runat="server" Value="~~~" />
<asp:HiddenField ID="hfURL" runat="server" />

</asp:Content>

