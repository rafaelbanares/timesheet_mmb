﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttUtils;


public partial class EmployeeInfo : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!Page.IsPostBack)
        {
            string compid = _Url["CompID"].Trim();
            string empid = _Url["EmpID"].Trim();
            PopulateScreenDefaults(compid, empid);
        }
    }

    public void PopulateScreenDefaults(string compID, string empID)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeInfo",
            new SqlParameter[] { 
                new SqlParameter("@DBname", _KioskSession.DB) 
                , new SqlParameter("@CompanyID", compID)
                , new SqlParameter("@EmployeeID", empID) 
            });

        if ( ds.Tables[0].Rows.Count > 0 )
        {
            DataRow emp = ds.Tables[0].Rows[0];

            lblCompanyID.Text = emp["CompanyID"].ToString(); 
            lblFullname.Text = emp["FullName"].ToString();
            
            lblEmployeeID.Text = emp["EmployeeID"].ToString();
            lblMachineID.Text = emp["SwipeID"].ToString();

            lblDateEmployed.Text = Tools.ShortDate(emp["DateEmployed"], _KioskSession.DateFormat);
            lblDateRegularized.Text = Tools.ShortDate(emp["DateRegularized"], _KioskSession.DateFormat);

            lblTardyExempt.Text = emp["TardyExempt"].ToString();
            lblUTExempt.Text = emp["UTExempt"].ToString();
            lblOTExempt.Text = emp["OTExempt"].ToString();

            lblNonSwiper.Text = emp["NonSwiper"].ToString();
            lblPosition.Text = emp["Position"].ToString();

            lblShift.Text = emp["Shift"].ToString();
            lblRestDay.Text = Tools.ShowRestdays(emp["RestDay"].ToString());

            lblApprover1.Text = emp["Approver1Name"].ToString() + " - " + emp["Approver1ID"].ToString();
            lblApprover2.Text = emp["Approver2Name"].ToString() + " - " + emp["Approver2ID"].ToString();
            lblApprover3.Text = emp["Approver3Name"].ToString() + " - " + emp["Approver3ID"].ToString();

            lblLevel2.Text = emp["Level2"].ToString();
            lblLevel3.Text = emp["Level3"].ToString();
            lblLevel4.Text = emp["Level4"].ToString();

            lblLevel2Disp.Text = _KioskSession.Level[1] + ":";
            lblLevel3Disp.Text = _KioskSession.Level[2] + ":";
            lblLevel4Disp.Text = _KioskSession.Level[3] + ":";
        }

    }
}

