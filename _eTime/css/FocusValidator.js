/* noi 12-16-2007 */
function datevalid(oID)
{
    var inputstring = oID.value;
    if (inputstring.length == 0)
        return;
    
    var match, year, month, date, result, isvalid;

    if ((match = /^(\d{4})([.\/-])(\d{1,2})\2(\d{1,2})$/.exec(inputstring))) 
    {
        year = Number(match[1]);
        month = Number(match[3]);
        date = Number(match[4]);
    }         
    else if ((match = /^(\d{1,2})([.\/-])(\d{1,2})\2(\d{4})$/.exec(inputstring)))
    {
        year = Number(match[4]);
        month = Number(match[1]);
        date = Number(match[3]);
    }
    else if ((match = /^(\d{1,2})([.\/-])(\d{1,2})([.\/-]?)$/.exec(inputstring)))
    {
        month = Number(match[1]);
        date = Number(match[3]);
        year = new Date().getYear();
    }
    else if ((match = /^(\d{1,2})([.\/-]?)$/.exec(inputstring)))
    {
        var today = new Date();
        year = today.getYear();
        month = today.getMonth() +1;
        date = Number(match[1]);
    }
    
    if (month > 12) 
    {
        var temp = month;
        month = date;
        date = temp;
    }
     
    //.. some of the code below the "abrupt return" are intentional, for future reference
    with ((result = new Date(year, --month, date)))
        if ( !((month == getMonth()) && (date == getDate())) ) 
        {
            oID.focus();
            return false;            
            //...get default date ;
            var today = new Date();
            year = today.getYear();
            month = today.getMonth();            
            date = today.getDate();
        }
        
    return true;
        
    //var output =
    oID.value = 
        (++month<10?'0'+month:month) + '/' +
        (date<10?'0'+date:date) + '/' + year;        
    //return output;
}

