/* noi 03-16-2007 */
function timevalid(oID)
{
    var inputstring = oID.value;
    if (inputstring.length == 0)
        return;

    var match, hour, mins, isvalid;

    if  ((match = /^(\d{1,2})(:)(\d{1,2})(...)/.exec(inputstring)))
    {
        hour = Number(match[1]);
        mins = Number(match[3]);
    }

    if (hour > 24) 
    {
        oID.focus();
        return false;
    }

    return true;
}

