using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class AdminLeaveBalance : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            PopulateDdl();
            BindGrid();
            updateurl();
        }
    }

    protected void ddlCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        lnkLevel.Attributes.Remove("onclick");
        lnkLevel.Attributes.Add("onclick", "openwindow(); return false;");
        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    private void BindGrid()
    {
        gvEmp.SelectedIndex = -1;
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        //... clear details
        gvLTrans.DataBind();
        up2.Update();
    }

    private DataSet GetData()
    {
        string[] levels = hfLevels.Value.Split('~');
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Level1", levels[0])
            ,new SqlParameter("@Level2", levels[1])
            ,new SqlParameter("@Level3", levels[2])
            ,new SqlParameter("@Level4", levels[3])
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            ,new SqlParameter("@Code", ddlCode.SelectedItem.Value.ToString())
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveBalanceAdminByEmployeeID", spParams);
        return ds;
    }

    private void PopulateDdl()
    {
        string qry2 =
            "select '' as Code, '---ALL---' as Description  union all " +
            "select Code, Description  from " + _KioskSession.DB + ".dbo.LeaveCode where companyid='" + _KioskSession.CompanyID + "'";

        DataSet dsCode = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, qry2);
        ddlCode.DataSource = dsCode.Tables[0];
        ddlCode.DataTextField = "Description";
        ddlCode.DataValueField = "Code";
        ddlCode.DataBind();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {        
        BindLeaveTrans();
        up2.Update();
    }

    #region BindLeaveTrans
    private void BindLeaveTrans()
    {
        //gvLTrans.DataSource = SortDataTable(GetLeaveTrans().Tables[0], false);  //-- this will cause error because SortDataTable uses GridViewSortExpression which is from gvEmp grid
        gvLTrans.DataSource = GetLeaveTrans().Tables[0];
        gvLTrans.DataBind();
    }
    #endregion
    #region GetLeaveTrans
    private DataSet GetLeaveTrans()
    {
        string key = gvEmp.DataKeys[gvEmp.SelectedIndex].Values["EmployeeID"].ToString();

        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmployeeID", key)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveEarnedTakenByEmployeeID", spParams);
        return ds;
    }
    #endregion

    protected void ibSearch_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        hfLevels.Value = "~~~";
        txtLevels.Text = "";
        BindGrid();
        updateurl();
    }
    protected void gvLTrans_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvLTrans.EditIndex)
            {
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().Trim().ToLower();

                if (!(status == "for approval" || status == "declined"))
                {
                    ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
                    ibDelete.Visible = false;
                }
            }
        }
    }
    protected void gvLTrans_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvLTrans.DataKeys[e.RowIndex].Values["ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveDelete",
            new SqlParameter[] { new SqlParameter("@ID", key), 
                                 new SqlParameter("@LastUpdBy", _KioskSession.UID),
                                 new SqlParameter("@LastUpdDate", DateTime.Now),
                                 new SqlParameter("@DBname", _KioskSession.DB) });
        gvLTrans.EditIndex = -1;
        BindLeaveTrans();
    }
    protected void gvLTrans_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvLTrans.PageIndex;
        BindLeaveTrans();

        gvLTrans.PageIndex = pageIndex;
    }

}
