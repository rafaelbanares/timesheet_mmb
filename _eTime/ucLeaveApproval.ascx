<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeaveApproval.ascx.cs" Inherits="ucLeaveApproval" %>
    
<asp:UpdatePanel id="upLVR" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    <table bgcolor="white">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname, MI"></asp:Label>
                <hr />
                <br />
            </td>
        </tr>
        <tr>
            <td align="right">
                Date filed:</td>
            <td>
                <asp:TextBox ID="txtDateFiled" runat="server" CssClass="TextBoxDate" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Date start:</td>
            <td>
                <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                    ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" ValidationGroup="leaveapp"  >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Date end:</td>
            <td>
                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                    ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" ValidationGroup="leaveapp"  >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Number of days:</td>
            <td>
                <asp:TextBox ID="txtLeaveDays" runat="server" CssClass="ControlDefaults" Width="40px">1.00</asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Leave:</td>
            <td>
                <asp:TextBox ID="txtLeaveCode" runat="server" CssClass="ControlDefaults" Width="64px" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Reason for leave:</td>
            <td>
                <asp:TextBox ID="txtReason" runat="server" CssClass="ControlDefaults" Height="74px"
                    MaxLength="100" TextMode="MultiLine" Width="504px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReason"
                    ErrorMessage="Reason for leave cannot be blank" ValidationGroup="leaveapp">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right" style="height: 26px">
                
            </td>
            <td style="height: 26px">
                &nbsp;<asp:Button ID="btnApproved" runat="server" CssClass="ControlDefaults" OnClick="btnApproved_Click"
                    Text="Approve" ValidationGroup="leaveapp" />
                <asp:Button ID="btnDeclined" runat="server" CssClass="ControlDefaults" OnClick="btnDeclined_Click"
                    OnClientClick="return confirm('Are you sure to decline this leave?')" Text="Decline" ValidationGroup="leaveapp" /></td>
        </tr>
    </table>
    <asp:HiddenField ID="hfLeaveID" runat="server" />
    
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeLeaveDays" runat="server" ErrorTooltipEnabled="True"
        InputDirection="RightToLeft" Mask="99.99" MaskType="Number" MessageValidatorTip="true"
        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtLeaveDays">
    </ajaxToolkit:MaskedEditExtender>

    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="leaveapp" />
        <asp:HiddenField ID="hfEmployeeID" runat="server" />
    <br />
    
    </ContentTemplate>
</asp:UpdatePanel>
