﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="WorkDistribApprList.aspx.cs" Inherits="WorkDistribApprList" %>
<%@ Register src="EmpHeaderFilter.ascx" tagname="EmpHeaderFilter" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>
    <script type="text/javascript">
        function openWD(sUrl) {
            popupWorkDist = wopen(sUrl, "popupWorkDist", 1000, 600);
        }
    </script>
    <uc1:EmpHeaderFilter ID="EmpHeader1" runat="server" />
    <hr />
    <asp:GridView ID="gvEmployees" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20"
        Width="100%" onrowdatabound="gvEmployees_RowDataBound" 
        onpageindexchanging="gvEmployees_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="EmployeeID" HeaderText="Emp.ID." />
            <asp:BoundField DataField="Fullname" HeaderText="Employee Name" />
            <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False"  />
            <asp:BoundField DataField="in1" HeaderText="Time&lt;br&gt;in" DataFormatString="{0:hh:mm tt}" HtmlEncode="False" />
            <asp:BoundField DataField="out1" HeaderText="Time&lt;br&gt;out" DataFormatString="{0:hh:mm tt}" HtmlEncode="False" />
            
            <asp:BoundField DataField="Absent" HeaderText="Absent<br>(Days)" HtmlEncode="false"/>
            <asp:BoundField DataField="Leave" HeaderText="Leave<br>(Days)" HtmlEncode="false" />
            <asp:BoundField DataField="Late" HeaderText="Actual<br>Late<br>(Hours)" HtmlEncode="false"/>
            <asp:BoundField DataField="ExcusedLate" HeaderText="Excused<br>Late<br>(Hours)" HtmlEncode="false"/>
            <asp:BoundField DataField="UT" HeaderText="Actual<br>UT<br>(Hours)" HtmlEncode="false"/>
            <asp:BoundField DataField="ExcusedUT" HeaderText="Excused<br>UT<br>(Hours)" HtmlEncode="false"/>
            <asp:BoundField DataField="Rendered" HeaderText="Rendered<br>Hours" HtmlEncode="false" />

            <asp:TemplateField HeaderText="Status">
                <ItemTemplate >
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="More">
                <ItemTemplate >
                    <asp:ImageButton ID="ibWorkDist" runat="server" ImageUrl="~/Graphics/workdist.gif" ToolTip="work distribution" />
                    <asp:HiddenField ID="hfDate" runat="server" Value='<%# Bind("Date") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
    </asp:GridView>
    
    <asp:Button ID="btnFU" runat="server" Text="" onclick="btnFU_Click" style="display:none" />
    
    <br />
    <hr />

    <table>
        <tr align='left'>
            <td><strong>Legend:</strong></td>
            <td style="width:20px; height:22px; background-color:#90ee90"></td><td style="width:120px">- Leave / Absent</td>
            <td style="width:20px; height:22px; background-color:#f4a460"></td><td style="width:120px">- For Approval</td>
            <td style="width:20px; height:22px; background-color:#ffd700"></td><td style="width:180px">- Erroneous / No Work Details</td>
        </tr>
        <tr>
            <td colspan='7'>
            </td>
        </tr>
        <tr align='left'>
            <td></td>
            <td style="width:20px; height:22px; background-color:#CCFF66"></td><td>- Partial Leave</td>
            <td style="width:20px; height:22px; background-color:#c0c0c0"></td><td>- Holiday / Restday</td>
            <td style="width:20px; height:22px; background-color:#CC99FF"></td><td>- Incomplete Work Details</td>
        </tr>
    </table>
    
    
    <br />    
</asp:Content>

