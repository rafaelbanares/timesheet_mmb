<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="Shift.aspx.cs" Inherits="Shift" Title="Bisneeds - Timekeeping - Shift Definitions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Shift Definitions"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Shift Code" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Shift Code</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" Width="100%" DataKeyNames="ShiftCode">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>
                
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Description") %>' MaxLength="30" Width="190px"></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                        </ItemTemplate>                       
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="In1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtin1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("in1") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevin1" runat="server" Display="Dynamic" 
                                ValidationGroup="Trans" ControlToValidate="txtin1" ControlExtender="mein1" EmptyValueMessage="*" IsValidEmpty="False" InvalidValueMessage="*" /><ajaxToolkit:MaskedEditExtender ID="mein1" runat="server"  ErrorTooltipEnabled="True" 
                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                TargetControlID="txtin1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litin1" runat="server" Text='<%# Bind("in1") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Out1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtout1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("out1") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevout1" runat="server" EmptyValueBlurredText="*"
                                InvalidValueMessage="*" Display="Dynamic" 
                                ValidationGroup="Trans" ControlToValidate="txtout1" ControlExtender="meout1" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="meout1" runat="server"  ErrorTooltipEnabled="True" 
                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                TargetControlID="txtout1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litout1" runat="server" Text='<%# Bind("out1") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="In2">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtin2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("in2") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevin2" runat="server"
                                InvalidValueMessage="*" Display="Dynamic" 
                                ValidationGroup="Trans" ControlToValidate="txtin2" ControlExtender="mein2" EmptyValueMessage="*" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="mein2" runat="server"  ErrorTooltipEnabled="True" 
                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                TargetControlID="txtin2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litin2" runat="server" Text='<%# Bind("in2") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Out2">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtout2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("out2") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevout2" runat="server" InvalidValueBlurredMessage="*" Display="Dynamic" 
                                ValidationGroup="Trans" ControlToValidate="txtout2" ControlExtender="meout2" EmptyValueMessage="*" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="meout2" runat="server"  ErrorTooltipEnabled="True" 
                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                TargetControlID="txtout2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litout2" runat="server" Text='<%# Bind("out2") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Grace&lt;br&gt;period 1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtgracein1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("gracein1") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevgracein1" runat="server" InvalidValueBlurredMessage="*" Display="Dynamic" 
                                ValidationGroup="Trans" ControlToValidate="txtgracein1" ControlExtender="megracein1" EmptyValueMessage="*" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="megracein1" runat="server"  ErrorTooltipEnabled="True" 
                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                TargetControlID="txtgracein1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litgracein1" runat="server" Text='<%# Bind("gracein1") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Grace&lt;br&gt;period 2">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtgracein2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("gracein2") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevgracein2" runat="server" InvalidValueBlurredMessage="*" Display="Dynamic" 
                                ValidationGroup="Trans" ControlToValidate="txtgracein2" ControlExtender="megracein2" EmptyValueMessage="*" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="megracein2" runat="server"  ErrorTooltipEnabled="True" 
                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                TargetControlID="txtgracein2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litgracein2" runat="server" Text='<%# Bind("gracein2") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Min OT<br>in minutes">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtminot" runat="server" CssClass="ControlDefaults" Width="24px" Text='<%# Bind("minimumot") %>'></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litminot" runat="server" Text='<%# Bind("minimumot") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Max tardy&lt;br&gt;in minutes">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtmaxtardy" runat="server" CssClass="ControlDefaults" Width="24px" Text='<%# Bind("maxtardy") %>'></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litmaxtardy" runat="server" Text='<%# Bind("maxtardy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Max UT&lt;br&gt;in minutes">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtmaxut" runat="server" CssClass="ControlDefaults" Width="24px" Text='<%# Bind("maxut") %>'></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litmaxut" runat="server" Text='<%# Bind("maxut") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Flexi<br>Shift">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkFlexi" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgFlexi" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center" />                
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Flexi<br>Break">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkFlexBreak" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgFlexBreak" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center" />                
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="OT Before<br>Shift">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkOtB4Shift" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgOtB4Shift" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />                
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Max Flexi&lt;br&gt;in hours">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtmaxflexi" runat="server" CssClass="ControlDefaults" Width="24px" Text='<%# Bind("maxflexi") %>'></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litmaxflexi" runat="server" Text='<%# Bind("maxflexi") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>