using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;

public partial class AttendanceProcess : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            //DateTime startprocess = DateTime.Now;

            //Bns.Attendance.DBCredential dbc = new Bns.Attendance.DBCredential(_ConnectionString, MainDB, _KioskSession.UID, _KioskSession.CompanyID, _KioskSession.DB);
            //Bns.SpecificProcess.ClientSpecificProcess process = new Bns.SpecificProcess.ClientSpecificProcess(dbc);

            //process.Specific = ConfigurationManager.AppSettings.Get("AttendanceProcess");
            //process.ExecuteBulk();

       
            //TimeSpan ts = DateTime.Now - startprocess;
            //litMessage.Text = string.Format("Processing completed - Total time executed on server {0} minute(s) {1} second(s) ", ts.Minutes, ts.Seconds);

            litMessage.Text = string.Format("Processing completed!");
        }
    }
}
