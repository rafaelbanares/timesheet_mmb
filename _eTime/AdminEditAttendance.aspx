<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="AdminEditAttendance.aspx.cs" Inherits="AdminEditAttendance" Title="Bisneeds - Timekeeping - Edit Attendance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function EndRequestHandler(sender, args) {
   if (args.get_error() == undefined)
    {        
        var action = $get("<%=hfUpdate.ClientID%>").value;
        if (action)
        {            
            popupWindow=wopen(action,"popupWindow",800,550);
        }
    }
   else
       alert('There was an error' + args.get_error().message);
}
function closechild()
{
    $get("<%=ibGo.ClientID%>").click();
    closewindow(popupWindow);
}

</script>   

<table width="100%">
   <tr>
      <td>
          <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
              Font-Underline="False" Text="Attendance Correction Form"></asp:Label><br />
          <br />
            <asp:Label id="lblFullname" runat="server" Text="Lastname, Firstname S." Font-Size="Large" Font-Names="Arial"></asp:Label></td>

      <td>
        <table>
            <tr>
                <td>
                    Attendance From:
                </td>
                <td align="left">
                    <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                        OnClientClick="return false;" ToolTip="Click to choose date" />

                    <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Cannot accept invalid value" ValidationGroup="FiltGrp" ControlToValidate="txtDateStart" 
                        ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                        IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                    </ajaxToolkit:MaskedEditValidator>

                </td>
                <td align="left">
                </td>
            </tr>
            <tr>
                <td>
                    To:
                </td>
                <td align="left">
                    <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                        OnClientClick="return false;" ToolTip="Click to choose date" />
                        
                    <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Cannot accept invalid value" ValidationGroup="FiltGrp" ControlToValidate="txtDateEnd" 
                        ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                        IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                    </ajaxToolkit:MaskedEditValidator>
                </td>
                <td align="left">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Show:&nbsp;<asp:DropDownList ID="ddlShow" runat="server" CssClass="ControlDefaults"
                        OnSelectedIndexChanged="ddlShow_SelectedIndexChanged" AutoPostBack="True" ValidationGroup="FiltGrp">
                        <asp:ListItem>All Transactions</asp:ListItem>
                        <asp:ListItem Value="error">Erroneous Transactions</asp:ListItem>
                        <asp:ListItem Value="notrans">Absent/No Transaction</asp:ListItem>
                    </asp:DropDownList></td>
                <td align="left">
                    <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                        ToolTip="Go" ValidationGroup="FiltGrp" /></td>
            </tr>
        </table>
      </td>
   </tr>
</table>

<br />

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
        CssClass="DataGridStyle" OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging"
        PageSize="20">
        
        
        <Columns>
            <asp:TemplateField HeaderText="Attendance&lt;br&gt;Date" SortExpression="Date">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Date"), "MM/dd/yyyy") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ShiftIn" DataFormatString="{0:HH:mm tt}" HeaderText="Shift&lt;br&gt;in"
                HtmlEncode="False" />
            <asp:BoundField DataField="ShiftOut" DataFormatString="{0:HH:mm tt}" HeaderText="Shift&lt;br&gt;out"
                HtmlEncode="False" />
            <asp:BoundField DataField="in1" DataFormatString="{0:hh:mm tt}" HeaderText="Time&lt;br&gt;in"
                HtmlEncode="False" />
            <asp:BoundField DataField="out1" DataFormatString="{0:hh:mm tt}" HeaderText="Time&lt;br&gt;out"
                HtmlEncode="False" />
            <asp:BoundField DataField="Absent" HeaderText="Absent&lt;br&gt;days" HtmlEncode="False" />
            <asp:BoundField DataField="Leave" HeaderText="Leave&lt;br&gt;days" HtmlEncode="False" />
            <asp:BoundField DataField="Late" HeaderText="Late&lt;br&gt;hours" HtmlEncode="False" />
            <asp:BoundField DataField="UT" HeaderText="Undertime&lt;br&gt;hours" HtmlEncode="False" />
            <asp:BoundField DataField="RegHrs" HeaderText="Regular&lt;br&gt;hours" HtmlEncode="False" />
            <asp:BoundField DataField="RegND1" HeaderText="Regular&lt;br&gt;ND1" HtmlEncode="False" />
            <asp:BoundField DataField="RegND2" HeaderText="Regular&lt;br&gt;ND2" HtmlEncode="False" />
            <asp:TemplateField HeaderText="More">
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" ToolTip="Select" ImageUrl="~/Graphics/edit.gif"
                        OnClick="ibEdit_Click" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
    </asp:GridView>
    <asp:HiddenField ID="hfUpdate" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ddlShow"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>
    
    <asp:HiddenField ID="hfEmpID" runat="server" />
</asp:Content>

