﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.ApplicationBlocks.Data;

public partial class zAdminSQL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(tbConn.Text, CommandType.Text, tbText.Text);
            if (ds.Tables.Count > 0)
            {
                gvResult.DataSource = ds.Tables[0];
                gvResult.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }

    }
}
