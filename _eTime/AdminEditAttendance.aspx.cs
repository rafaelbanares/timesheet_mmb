using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class AdminEditAttendance : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hfUpdate.Value = "";        
        base.GetUserSession();        

        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            body.Attributes.Add("onload", "load();");
            //body.Attributes.Add("onfocus", "if (window.popupWindow) {window.popupWindow.close()}");
            body.Attributes.Add("onfocus", "closewindow();");


            hfEmpID.Value = _Url["EmpID"];
            lblFullname.Text = _Url["EmpName"];

            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();

            BindGrid();
        }
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            //,new SqlParameter("@SecID", 0)
            ,new SqlParameter("@Search", hfEmpID.Value)
            ,new SqlParameter("@StartDate", txtDateStart.Text)
            ,new SqlParameter("@EndDate", txtDateEnd.Text)
            ,new SqlParameter("@Filter", ddlShow.SelectedItem.Value )
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceLoadByDateRange", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = sender as ImageButton;
        Literal litDate = ib.Parent.Parent.FindControl("litDate") as Literal;

        SecureUrl secureUrl = new SecureUrl(
            string.Format("AttendanceEdit.aspx?EmpID={0}&Date={1}", hfEmpID.Value, litDate.Text));

        //... needs postback in order to create a secured url.
        hfUpdate.Value = secureUrl.ToString();    
    }

    protected void ddlShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

}
