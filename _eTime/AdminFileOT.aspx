<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="AdminFileOT.aspx.cs" Inherits="AdminFileOT" Title="Bisneeds - Timekeeping - Overtime Filing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Overtime Authorization Form"></asp:Label><br />
    <br />
    <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname, MI"></asp:Label><br />
    <br />
    
<asp:UpdatePanel ID="updOT" UpdateMode="Conditional" runat="server">
  <ContentTemplate>
  
    <table class="TableBorder1" style="font-family: Verdana">
        <tr>
            <td align="right">
                Date filed:</td>
            <td>
                <asp:TextBox ID="txtDateFiled" runat="server" CssClass="TextBoxDate" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Overtime date:</td>
            <td>
                <asp:TextBox ID="txtOTDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtOTDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" OnClientClick="return false;" />
                    
                <ajaxToolkit:MaskedEditValidator id="mevOTDate" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtOTDate" 
                    ControlExtender="meeOTDate" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Overtime start:</td>
            <td>
                <asp:TextBox ID="txtin1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox>
                <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1"
                    ControlToValidate="txtin1" Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Time is invalid" EmptyValueMessage="Overtime start cannot be blank" IsValidEmpty="False"></ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Overtime end:</td>
            <td>
                <asp:TextBox ID="txtout1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox>
                <ajaxToolkit:MaskedEditValidator
                    ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="txtout1"
                    Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid" EmptyValueMessage="Overtime end cannot be blank" IsValidEmpty="False"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
        <tr>
            <td align="right">
                Reason for overtime:</td>
            <td>
                <asp:TextBox ID="txtReason" runat="server" CssClass="ControlDefaults" Height="74px"
                    MaxLength="100" TextMode="MultiLine" Width="501px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReason"
                    ErrorMessage="Reason for overtime cannot be blank">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" CssClass="ControlDefaults" OnClick="btnSubmit_Click"
                    Text="Save" />
                <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    <ajaxToolkit:MaskedEditExtender ID="meeOTDate" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtOTDate">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleOTDate" runat="server" PopupButtonID="ibtxtOTDate"
        TargetControlID="txtOTDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtin1">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtout1">
    </ajaxToolkit:MaskedEditExtender>   
  </ContentTemplate>
</asp:UpdatePanel>    

</asp:Content>

