using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;


using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;

//using Bns.DataTools;
//using Bns.UI;
//using Bns.Utils;
//using Bns.Constants;
using Bns.AttendanceUI;
using BNS.Attendance.DataObjects;


public partial class Viewer_Viewer : KioskPageUI //System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        base.OverwriteRender(false);
        if (!IsPostBack)
        {
            ReportDao oRptDAL = new ReportDao(_KioskSession.DB, _KioskSession.CompanyID);
            List<DataSet> dsReportData = oRptDAL.GetReportData(ReportID, ReportParameters);
            //ViewReportPDF(oRptDAL.GetData(ReportID, ReportParameters), oRptDAL.GetSubReportData(ReportID, ReportParameters));

            if (ReportParameters.ReportName == "AttendanceSummaryTimeSheet.rpt" || ReportParameters.ReportName == "AttendanceSummaryOT.rpt")
                ViewReportForceExcel(dsReportData[0]);
            else if (ReportParameters.ReportName == "Client Profitability")
                CreateCSVfile(dsReportData[0], "Client Profitability");
            else
                ViewReportPDF(dsReportData[0], dsReportData[1]);
            CleanUp();
        }
    }

    private void ViewReportForceExcel(DataSet ds)
    {
        base.GetUserSession();
        Response.ContentType = "application/ms-excel";
        //Response.AddHeader("Content-Disposition", "inline;filename=rawdata.xls");
        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + ReportParameters.ReportName.Substring(0, ReportParameters.ReportName.IndexOf(".")) + ".xls\"");

        Response.Buffer = true;
        this.EnableViewState = false;

        //DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, "rat_AttendanceSummaryLoweOT");
        //new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        DataGrid dg = new DataGrid();
        dg.DataSource = ds.Tables[0];
        dg.AllowPaging = false;
        dg.DataBind();
        dg.RenderControl(htw);

        Response.Write(sw.ToString());
        Response.End();
    }

    public void CreateCSVfile(DataSet ds, string sfilename)
    {
        DataTable dtable = ds.Tables[0];

        base.GetUserSession();
        Response.ContentType = "application/csv";
        Response.AddHeader("Content-Disposition", "inline;filename=" + sfilename + ".csv" );
        Response.Buffer = true;
        this.EnableViewState = false;

        StringWriter sw = new StringWriter();
        int icolcount = dtable.Columns.Count;

        //.... Add column names
        //for (int i = 0; i < icolcount; i++)
        //{
        //    sw.Write(dtable.Columns[i]);
        //    if (i < icolcount - 1)
        //    {
        //        sw.Write(",");
        //    }

        //} 
        //sw.Write(sw.NewLine);
        foreach (DataRow drow in dtable.Rows)
        {
            for (int i = 0; i < icolcount; i++)
            {
                if (!Convert.IsDBNull(drow[i]))
                {
                    string s = drow[i].ToString();
                    if (s.Contains(","))
                        sw.Write("\"" + s + "\"");
                    else
                        sw.Write(s);
                }
                if (i < icolcount - 1)
                {
                    sw.Write(",");
                }
            } 
            sw.Write(sw.NewLine);
        }
        Response.Write(sw.ToString());
        Response.End();
        
        sw.Close();
    }

    #region ReportParameters
    private ReportParameters ReportParameters
    {
        get { return (ReportParameters)Session["ReportParameters"]; }
    }
    #endregion
    #region ReportType    
    private int ReportID
    {
        get
        {
            Bns.AttUtils.SecureUrl url = new Bns.AttUtils.SecureUrl(Request.Url.PathAndQuery);
            if (url["rpt"] == "") throw new Exception("Report does not exist");
            return Convert.ToInt32(url["rpt"]);
        }
    }
    #endregion

    private void ViewReportPDF(DataSet dsReport, DataSet dsSubReport)
    {
        if (dsReport == null || dsReport.Tables.Count == 0) return;

        ExportOptions crExportOptions;
        DiskFileDestinationOptions crDiskFileDestinationOptions;
        String exportPath = Server.MapPath("TEMP") + "\\";
       
        //load the report
        ReportDocument crReport = new ReportDocument();  
        crReport.Load(Request.PhysicalApplicationPath + "Reports\\" + ReportParameters.ReportName);

        // set main report datasource
        if (dsReport.Tables.Count > 1)
             crReport.SetDataSource(dsReport);
        else crReport.SetDataSource(dsReport.Tables[0]);

        //set subreports datasource        
         SetSubReportData(crReport, dsSubReport); 


         // Load report parameters
         LoadReportParameters(crReport);

        crDiskFileDestinationOptions = new DiskFileDestinationOptions();
        crExportOptions = crReport.ExportOptions;

        //set the filename property for the DestinationOptions class -- excel uses same filename (pdf extention but excel file)
        string fname = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Ticks.ToString() + ".pdf";
        crDiskFileDestinationOptions.DiskFileName = exportPath + fname;

        //set the required report ExportOptions properties
        crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
        crExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

        if (ReportParameters.Export.ToLower() == "excel")
            crExportOptions.ExportFormatType = ExportFormatType.Excel;            
        else
            crExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

        try
        {
            // Export the report to PDF
            crReport.Export();

            // View in Acrobat 
            Response.ClearContent();
            Response.ClearHeaders();

            if (ReportParameters.Export.ToLower() == "excel")
                //Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + ReportParameters.ReportName.Substring(0, ReportParameters.ReportName.IndexOf(".")) + ".xls\"");
            else
                Response.ContentType = "application/pdf";

            Response.WriteFile(exportPath + fname);
            Response.Flush();
            Response.Close();
            File.Delete(exportPath + fname);
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }


    #region LoadReportParameters
    private void LoadReportParameters(ReportDocument oRptDocument)
    {
        ParameterFieldDefinitions crParamFieldDefinitions = oRptDocument.DataDefinition.ParameterFields;
        ParameterFieldDefinition[] crParamFieldDef = new ParameterFieldDefinition[crParamFieldDefinitions.Count];
        ParameterValues crParameterValues = new ParameterValues();
        ParameterDiscreteValue[] crParamDiscreteValue = new ParameterDiscreteValue[crParamFieldDefinitions.Count];
        ReportParameters rptParam = this.ReportParameters;
        for (int i = 0; i < crParamFieldDefinitions.Count; i++)
        {
            crParamDiscreteValue[i] = new ParameterDiscreteValue();
            
            if (crParamFieldDefinitions[i].ParameterFieldName == "CompanyName")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i];
                crParamDiscreteValue[i].Value = rptParam.CompanyName;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "RptTitle")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i]; 
                crParamDiscreteValue[i].Value = rptParam.ReportTitle;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "RptSubTitle1")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i]; 
                crParamDiscreteValue[i].Value = rptParam.ReportSubTitle1;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "RptSubTitle2")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i];
                crParamDiscreteValue[i].Value = rptParam.ReportSubTitle2;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "RptSubTitle3")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i]; 
                crParamDiscreteValue[i].Value = rptParam.ReportSubTitle3;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "Grouping1")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i]; 
                crParamDiscreteValue[i].Value = rptParam.GroupBy1;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "Grouping2")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i];
                crParamDiscreteValue[i].Value = rptParam.GroupBy2 ;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "Sorting")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i];
                crParamDiscreteValue[i].Value = rptParam.SortBy;
            }
            else if (crParamFieldDefinitions[i].ParameterFieldName == "IsExcel")
            {
                crParamFieldDef[i] = crParamFieldDefinitions[i];
                crParamDiscreteValue[i].Value = rptParam.IsExcel;
            }

            else
            {
                if (crParamFieldDefinitions[i].ParameterFieldName.Substring(0, 3) != "Pm-")
                {
                    crParamFieldDef[i] = crParamFieldDefinitions[i];
                    crParamDiscreteValue[i].Value = "";
                }
            }
        }

        // Add parameter values 
        for (int i = 0; i < crParamFieldDefinitions.Count; i++)
        {
            if (crParamFieldDefinitions[i].ParameterFieldName.Substring(0, 3) != "Pm-")
            {
                crParameterValues = crParamFieldDef[i].CurrentValues;
                crParameterValues.Add(crParamDiscreteValue[i]);
                crParamFieldDef[i].ApplyCurrentValues(crParameterValues);
            }
        }
    }
    #endregion
    #region SetSubReportData
    private void SetSubReportData(ReportDocument crReport, DataSet dsSubReport)
    {
        if (crReport.Subreports.Count == 0 || dsSubReport == null) return;     
        ReportDocument crSubreportDoc = null;
        for (int i = 0; i < crReport.Subreports.Count; i++)
        {       
            crSubreportDoc = crReport.Subreports[i];
            crSubreportDoc.SetDataSource(dsSubReport.Tables[i]);
        }
    } 
    #endregion

    #region SetSubReportDataSource 
    //private void SetSubReportDataSource(ReportDocument crReport, DataSet dsSubReport)
    //{
    //    if (crReport.Subreports.Count == 0) return;

    //    SubreportObject crSubreportObject;
    //    ReportDocument crSubreportDocument;
    //    int i = 0;
    //    foreach (Section crSection in crReport.ReportDefinition.Sections)
    //    {
    //        foreach (ReportObject crReportObject in crSection.ReportObjects)
    //        {
    //            if (crReportObject.Kind == ReportObjectKind.SubreportObject)
    //            {
    //                crSubreportObject = (SubreportObject)crReportObject;
    //                crSubreportDocument = crSubreportObject.OpenSubreport(crSubreportObject.SubreportName);
    //                crSubreportDocument.SetDataSource(dsSubReport.Tables[i]);
    //                i++;
    //                if (crReport.Subreports.Count == i) return;
    //            }
    //        }
    //    }
    //} 
    #endregion
    #region CleanUp
    private void CleanUp()
    {
        if (Session["ReportParameters"] != null) Session.Remove("ReportParameters");
    }
    #endregion

    
    //private UserSession _UserSession;
}
