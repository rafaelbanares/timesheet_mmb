using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class Level1 : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            UpdateLabels();
            BindGrid();
        }
    }

    private void UpdateLabels()
    {
        lblModule.Text = _KioskSession.Level[0] + " File Maintenance";
        lbNew.Text = "New " + _KioskSession.Level[0] + " Definition";
        ibNew.ToolTip = "New " + _KioskSession.Level[0] + " Definition"; 
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_Level1LoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_Level1LoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@IsBlankData", true), new SqlParameter("@DBname", _KioskSession.DB) });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        hfIsAdd.Value = "1";
        gvMain.DataBind();        
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
            if (ibDelete != null)
                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
            else
            {
                if (hfIsAdd.Value == "0")
                {
                    TextBox txtCode = e.Row.FindControl("txtCode") as TextBox;
                    txtCode.Enabled = false;
                }
            }
        }
    }

    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        Literal litCode = gvMain.Rows[e.RowIndex].FindControl("litCode") as Literal;

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_Level1Delete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@Code", litCode.Text), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox txtCode = gvMain.Rows[e.RowIndex].FindControl("txtCode") as TextBox;
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@Code", txtCode.Text));
        sqlparam.Add(new SqlParameter("@Description", txtDescription.Text));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (hfIsAdd.Value == "0")
        {
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_Level1Update", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_Level1Insert", sqlparam.ToArray());
        }

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }

}
