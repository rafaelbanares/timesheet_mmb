using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class popupLevel : KioskPageUI
{
    //const string emptycodevalue = "               "; //... space 15 (default value)
    const string emptycodevalue = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            btnOK.Attributes.Add("onclick", 
                    string.Format("updateparent({0},{1},{2}); return false;", hfLevels.ClientID, hfDesc.ClientID, hfURL.ClientID ));
            PopulateScreenDefault();
            hfLevels.Value = _Url["Levels"];
            UpdateDropdown();
            //UpdateLevels();
        }
    }

    private void PopulateScreenDefault()
    {
        lblModule.Text =
            _KioskSession.Level[0] + " / " +
            _KioskSession.Level[1] + " / " +
            _KioskSession.Level[2] + " / " +
            _KioskSession.Level[3] + " Selection ";

        lblLevel1.Text = _KioskSession.Level[0] + ":";
        lblLevel2.Text = _KioskSession.Level[1] + ":";
        lblLevel3.Text = _KioskSession.Level[2] + ":";
        lblLevel4.Text = _KioskSession.Level[3] + ":";

        BindGrid(1);

        string[] levels = _Url["Levels"].Split('~');
        if (levels.Length == 4)
        {
            ddlLevel1.SelectedValue = levels[0];
            ddlLevel2.SelectedValue = levels[1];
            ddlLevel3.SelectedValue = levels[2];
            ddlLevel4.SelectedValue = levels[3];
        }        
    }

    private void BindGrid(int nlevel)
    {
        //... bind only if needed.
        DropDownList[] ddlLevel = { ddlLevel1, ddlLevel2, ddlLevel3, ddlLevel4 };

        //for (int i = nlevel; i <= 4; i++)
        //    ddlLevel[i - 1].SelectedIndex = -1;


        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString,
                CommandType.StoredProcedure,
                MainDB + ".dbo.usa_LevelNLoadByCompanyID",
                new SqlParameter[]
            {
                new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                new SqlParameter("@Level", nlevel),
                new SqlParameter("@Level1Code", ddlLevel1.SelectedValue),
                new SqlParameter("@Level2Code", ddlLevel2.SelectedValue),
                new SqlParameter("@Level3Code", ddlLevel3.SelectedValue),
                new SqlParameter("@DBname", _KioskSession.DB)
            });

        for (int i = 0; i < 4; i++)
        {
            if (nlevel <= i+1)
            {
                if (ds.Tables[i].Rows.Count > 0)
                {
                    ddlLevel[i].DataSource = ds.Tables[i];
                    ddlLevel[i].DataBind();

                    //ddlLevel[i].Items.Insert(0, new ListItem("---- Select All ----", ""));
                    ddlLevel[i].Items.Insert(0, new ListItem("", emptycodevalue));
                }

                if (ddlLevel[i].Items.Count == 0)
                    ddlLevel[i].Items.Insert(0, new ListItem("", emptycodevalue));                
            }
        }
    }

    protected void ddlLevel1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLevel2.SelectedIndex = -1;
        ddlLevel3.SelectedIndex = -1;
        ddlLevel4.SelectedIndex = -1;

        BindGrid(2);
        UpdateLevels();
    }
    protected void ddlLevel2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLevel3.SelectedIndex = -1;
        ddlLevel4.SelectedIndex = -1;

        BindGrid(3);
        UpdateLevels();
    }
    protected void ddlLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLevel4.SelectedIndex = -1;

        BindGrid(4);
        UpdateLevels();
    }

    protected void ddlLevel4_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateDropdown();
    }

    private void UpdateDropdown()
    {
        string selected = ddlLevel4.SelectedValue;

        if (selected.Trim() != String.Empty)
        {
            DataRow rowParent = SqlHelper.ExecuteDataset(_ConnectionString,
                    CommandType.StoredProcedure,
                    MainDB + ".dbo.usa_Level4GetParents",
                    new SqlParameter[]
                {
                    new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                    new SqlParameter("@Level4Code", selected),
                    new SqlParameter("@DBname", _KioskSession.DB)
                }).Tables[0].Rows[0];

            ddlLevel3.SelectedValue = rowParent["Level3Code"].ToString().Trim();
            ddlLevel2.SelectedValue = rowParent["Level2Code"].ToString().Trim();
            //ddlLevel1.SelectedValue = rowParent["Level1Code"].ToString().Trim();

            BindGrid(2);
            ddlLevel4.SelectedValue = selected;
        }

        UpdateLevels();
    }

    private void UpdateLevels()
    {
        hfLevels.Value =
                ddlLevel1.SelectedValue.Trim() + "~" +
                ddlLevel2.SelectedValue.Trim() + "~" +
                ddlLevel3.SelectedValue.Trim() + "~" +
                ddlLevel4.SelectedValue.Trim();

        //...result of displaying LevelX: LevelX Description will result to a garbled multi line textbox
        //
        //hfDesc.Value =//lblLevel1.Text + ddlLevel1.SelectedItem.Text.Trim() + 
        //        Environment.NewLine +
        //        lblLevel2.Text + ddlLevel2.SelectedItem.Text.Trim() + Environment.NewLine +
        //        lblLevel3.Text + ddlLevel3.SelectedItem.Text.Trim() + Environment.NewLine +
        //        lblLevel4.Text + ddlLevel4.SelectedItem.Text.Trim();

        hfDesc.Value =
                Environment.NewLine +
                ddlLevel2.SelectedItem.Text.Trim() + Environment.NewLine +
                ddlLevel3.SelectedItem.Text.Trim() + Environment.NewLine +
                ddlLevel4.SelectedItem.Text.Trim();

        SecureUrl secureUrl = new SecureUrl(
        string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));

        hfURL.Value = secureUrl.ToString();
    }
}
