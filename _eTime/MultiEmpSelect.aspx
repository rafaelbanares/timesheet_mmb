<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="MultiEmpSelect.aspx.cs" Inherits="MultiEmpSelect" Title="Bisneeds - Timekeeping - Multiple Employee Selection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript">

function chkEmp(c,s)
{
    var chk=$get(c).checked;
    var emp=$get(s).innerHTML.replace(' ','~');
    var holder = $get("<%=txtEmps.ClientID%>");

    if (chk)
    {
        holder.value = holder.value + emp;
    }
    else
    {
        holder.value = holder.value.replace(emp,'');        
        $get("<%=chkAll.ClientID%>").checked = false;
        progchanged('1');        
    }
}

function initialload(parentEmpsID)
{
    var emps = window.opener.document.getElementById(parentEmpsID).value;
    $get("<%=txtEmps.ClientID%>").value = emps;
    //alert($get("<%=txtEmps.ClientID%>").value);
    $get("<%=btnRefresh.ClientID%>").click();
}

function progchanged(v)
{
    $get("<%=hfProgChanged.ClientID%>").value = v;
}

function updateparent()
{
    window.opener.assignEmpID($get("<%=txtEmps.ClientID%>").value);
}

</script>


<div style="width:900px">
<asp:UpdatePanel ID="updEmp" UpdateMode="Conditional" runat="server">
<ContentTemplate>
    <asp:CheckBox ID="chkAll" runat="server" Text="Select/Unselect all employees" AutoPostBack="True" OnCheckedChanged="chkAll_CheckedChanged" /><br />
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="DataGridStyle" 
        OnPageIndexChanging="gvEmp_PageIndexChanging" OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" PageSize="20" Width="100%">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>                    
                    <asp:CheckBox ID="chkEmp" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>        
            <asp:TemplateField HeaderText="EmployeeID" SortExpression="EmployeeID">
                <ItemTemplate>                    
                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FullName" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litName" runat="server" Text='<%# Bind("Fullname") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department" SortExpression="Description">
                <ItemTemplate>
                    <asp:Literal ID="litDesc" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <br />
    <asp:TextBox ID="txtEmps" runat="server"></asp:TextBox>
    <asp:HiddenField ID="hfProgChanged" runat="server" Value="0" />
    <asp:HiddenField ID="hfEmps" runat="server" />
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnRefresh" />
</Triggers>
</asp:UpdatePanel>
<asp:Button ID="btnRefresh" runat="server" OnClick="btnRefresh_Click" Style="display: none" Text="Refresh" ValidationGroup="refresh" />
<div align="center" style="width:100%">
    <asp:Button ID="btnOK" runat="server" Text="OK" style="width: 100px" />
</div>

</div>
</asp:Content>

