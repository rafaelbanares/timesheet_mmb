using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class AdminFileLeave : KioskPageUI
{
    /*
     * Requires the following query string
     * EmpID - 
     * EmpName - 
    */

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            //body.Attributes.Add("onload", "toggledisplay(0);");
            string s = string.Format("toggledisplay($get('{0}').checked ? 2:0);", rbMultiple.ClientID);
            body.Attributes.Add("onload", s);

            populateScreenDefaults();
        }
    }

    private void populateScreenDefaults()
    {
        lblFullname.Text = _Url["EmpName"];

        string date = _Url["Date"];
        bool isFromCorrectAttendance = (date != "");

        string today;
        if (isFromCorrectAttendance)
        {
            today = date;
            lbBack.Visible = true;
            rbMultiple.Style.Add("display","none"); //visible = false;
        }
        else
        {
            today = Tools.ShortDate(DateTime.Now.ToShortDateString(), _KioskSession.DateFormat);
            lbBack.Visible = false;
        }

        txtDateFiled.Text = today;
        txtDateLeave.Text = today;
        txtDateStart.Text = today;
        txtDateEnd.Text = today;

        //... populate leave dropdown
        DataSet dsLeave = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT Code, Description FROM " + _KioskSession.DB + ".dbo.LeaveCode WHERE CompanyID = @CompanyID",
                        new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlLeave.DataSource = dsLeave.Tables[0];
        ddlLeave.DataBind();
        ddlLeave.Items.Insert(0, "");

        //... populate leave balances
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmployeeID", _Url["EmpID"])
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveBalanceByEmployeeID", spParams);
        gvLCredit.DataSource = ds.Tables[0];
        gvLCredit.DataBind();       
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        double totalFiledDays = 0;

        if (!rbMultiple.Checked)
        {
            txtDateStart.Text = txtDateLeave.Text;
            txtDateEnd.Text = txtDateLeave.Text;
            totalFiledDays = rbHalfday.Checked ? 0.5 : 1.0;
        }
        else
        {
            //... Date range must be 2 days onwards if chosen multiple dates.
            DateTime startdate = DateTime.Parse(txtDateStart.Text);
            DateTime enddate = DateTime.Parse(txtDateEnd.Text).AddDays(1);

            totalFiledDays = enddate.Subtract(startdate).TotalDays;

            if (totalFiledDays < 1)
            {
                lblError.Text = "Invalid date range, leave must be more than 1 day";
                return;
            }
        }

        SqlParameter[] spParams = new SqlParameter[] 
        {
            new SqlParameter("@DBname", _KioskSession.DB),
            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
            new SqlParameter("@EmployeeID", _Url["EmpID"]),
            new SqlParameter("@StartDate", txtDateStart.Text),
            new SqlParameter("@EndDate", txtDateEnd.Text),
            new SqlParameter("@Code", ddlLeave.SelectedItem.Value),
            new SqlParameter("@DateFiled", txtDateFiled.Text)
        };

        DataSet dsValidation = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveValidations", spParams);

        bool isExists = dsValidation.Tables[0].Rows.Count > 0;
        bool isCheckBalance = bool.Parse(dsValidation.Tables[1].Rows[0]["CheckBalance"].ToString());
        double filingdays = double.Parse(dsValidation.Tables[1].Rows[0]["FilingDays"].ToString());

        double actualleavedays = double.Parse(dsValidation.Tables[2].Rows[0]["ActualLeaveDays"].ToString());
        double daysbeforeleave = double.Parse(dsValidation.Tables[3].Rows[0]["ActualLeaveDays"].ToString());


       //... Check first if exists
       if (isExists)
        {
            lblError.Text = "Leave already exists";
            return;
        }
        else if (actualleavedays == 0)
        {
            lblError.Text = "No valid leave days. Check dates if it falls on holiday or restday";
            return;
        }
        else if (filingdays > daysbeforeleave)
        {
            lblError.Text = ddlLeave.SelectedItem.Value + " should be filed " + filingdays + " working days in advance";
            return;            
        }
        else if (isCheckBalance && float.Parse(dsValidation.Tables[1].Rows[0]["Balance"].ToString()) < totalFiledDays)
        {
            lblError.Text = "No available leave credits";
            return;
        }

        if (rbMultiple.Checked)
            totalFiledDays = actualleavedays;

        //... the filed leave is approved since admin entered the filed leave
        LeaveTransInsert("Approved", totalFiledDays);
    }
    private void LeaveTransInsert(string status, double leavedays)
    {
        string empid = _Url["EmpID"];
        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));

        sqlparam.Add(new SqlParameter("@EmployeeID", empid));
        sqlparam.Add(new SqlParameter("@DateFiled", txtDateFiled.Text));
        sqlparam.Add(new SqlParameter("@StartDate", txtDateStart.Text));
        sqlparam.Add(new SqlParameter("@EndDate", txtDateEnd.Text));
        sqlparam.Add(new SqlParameter("@Days", leavedays));
        sqlparam.Add(new SqlParameter("@Approver", "" ));
        sqlparam.Add(new SqlParameter("@Code", ddlLeave.SelectedItem.Value));
        sqlparam.Add(new SqlParameter("@Reason", txtReason.Text));
        sqlparam.Add(new SqlParameter("@Status", status));
        sqlparam.Add(new SqlParameter("@Stage", "2")); //-- final stage

        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveInsert", sqlparam.ToArray());

        //... update attendance and update leave balance are done inside the SP.

        /*
        //... Update attendance
        UpdateAttendance();

        //... Auto Update leave balance
        SqlParameter[] spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", empid)
                ,new SqlParameter("@Year", DateTime.Now.Year)
                ,new SqlParameter("@Month", DateTime.Now.Month)
            };
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCreditUpdateEarnedAndTaken", spParams);
        */

        //... Correct attendance
        string date = _Url["Date"];
        string targeturl = "BlankPageIframe.aspx?moduledesc=Filing of Leaves Module&message=Leave successfully added";
        if (date != "")
            targeturl = string.Format("AttendanceEdit.aspx?EmpID={0}&Date={1}", empid, date);

        SecureUrl url = new SecureUrl(targeturl);
        Response.Redirect(url.ToString());
    }

    protected void lbBack_Click(object sender, EventArgs e)
    {
        SecureUrl url = new SecureUrl(string.Format("AttendanceEdit.aspx?EmpID={0}&Date={1}", _Url["EmpID"], _Url["Date"]));
        Response.Redirect(url.ToString());
    }

    private void UpdateAttendance()
    {
        // -- update attendance is done on the stored procedure
        /*
        DateTime dstart = DateTime.Parse(txtDateStart.Text);
        DateTime dend = DateTime.Parse(txtDateEnd.Text);

        for (DateTime curDate = dstart; curDate <= dend; curDate = curDate.AddDays(1))
        {
            string sql = "UPDATE " + _KioskSession.DB + ".dbo.Attendance "
                        + " SET LeaveCode = @Code, Leave = CASE WHEN Absent > @Days THEN @Days ELSE Absent END "
                        + " WHERE CompanyId=@CompanyID AND EmployeeID=@EmployeeID AND Absent>0 AND Date = @Date ";

            decimal days = Convert.ToDecimal(txtLeaveDays.Text);
            if (days > 1)
                days = 1;

            SqlParameter[] parms = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _Url["EmpID"])
                ,new SqlParameter("@Date", curDate)
                ,new SqlParameter("@Days", days)
                ,new SqlParameter("@Code", ddlLeave.SelectedItem.Value )
            };

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.Text, sql, parms);
        }
        */
    }


}
