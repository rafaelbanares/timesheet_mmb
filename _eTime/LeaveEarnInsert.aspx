﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveEarnInsert.aspx.cs" Inherits="LeaveEarnInsert" Title="Bisneeds - Timekeeping - Leaves Earning Processing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Leaves Earning Processing"></asp:Label>
<br /><br />


<table width="600px" border="1">
    <tr>
        <td align="right">
            Transaction Date:
        </td>
        <td colspan="1">
            <asp:TextBox ID="txtTranDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            <asp:ImageButton ID="ibtxtTranDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                ToolTip="Click to choose date" OnClientClick="return false;" />

            <ajaxToolkit:MaskedEditValidator id="mevTranDate" runat="server" InvalidValueBlurredMessage="*"
                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtTranDate" 
                ControlExtender="meeTranDate" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
            </ajaxToolkit:MaskedEditValidator>
        </td>
        <td align="right">
            Leave Code:</td>
        <td>
            <asp:DropDownList ID="ddlLeave" runat="server" CssClass="ControlDefaults">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="right">
            Applicable Year:
        </td>
        <td>
            <asp:TextBox ID="txtAppYear" runat="server" CssClass="TextBoxInteger" Width="40px"></asp:TextBox>
            <ajaxToolkit:MaskedEditValidator id="mevAppYear" 
                ControlExtender="meeAppYear" ControlToValidate="txtAppYear"  runat="server"
                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                IsValidEmpty="False" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                MinimumValue="2008" MinimumValueBlurredText="*" MinimumValueMessage="Can accept only 2008-2079"
                MaximumValue="2079" MaximumValueBlurredText="*" MaximumValueMessage="Can accept only 2008-2079" >*
            </ajaxToolkit:MaskedEditValidator>

        </td>
        <td align="right">
            Vaild From:</td>
        <td>
            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            <asp:ImageButton ID="ibtxtDateStart" runat="server" 
                ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" 
                ToolTip="Click to choose date" />
            <ajaxToolkit:MaskedEditValidator ID="mevDateStart" runat="server" 
                ControlExtender="meeDateStart" ControlToValidate="txtDateStart" 
                EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" IsValidEmpty="False" 
                ErrorMessage="*" InvalidValueBlurredMessage="*" 
                InvalidValueMessage="Cannot accept invalid value" 
                MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"
                MaximumValue="06/01/2079" MaximumValueBlurredText="*" MaximumValueMessage="Cannot accept date"
                >*</ajaxToolkit:MaskedEditValidator>

        </td>
    </tr>
    <tr>
        <td align="right">
            Applicable Month:
        </td>
        <td>
            <asp:TextBox ID="txtAppMonth" runat="server" CssClass="TextBoxInteger" Width="20px"></asp:TextBox>
            <ajaxToolkit:MaskedEditValidator id="mevAppMonth" 
                ControlExtender="meeAppMonth" ControlToValidate="txtAppMonth" runat="server" 
                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                IsValidEmpty="False" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                MinimumValue="1" MinimumValueBlurredText="*" MinimumValueMessage="Can accept only 1-12"
                MaximumValue="12" MaximumValueBlurredText="*" MaximumValueMessage="Can accept only 1-12" >*
            </ajaxToolkit:MaskedEditValidator>
        </td>
        <td align="right">
            Valid To:</td>
        <td>
            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            <asp:ImageButton ID="ibtxtDateEnd" runat="server" 
                ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" 
                ToolTip="Click to choose date" />
            <ajaxToolkit:MaskedEditValidator ID="mevDateEnd" runat="server" 
                ControlExtender="meeDateEnd" ControlToValidate="txtDateEnd"
                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" IsValidEmpty="False" 
                MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"
                MaximumValue="06/01/2079" MaximumValueBlurredText="*" MaximumValueMessage="Cannot accept date"
                >*
            </ajaxToolkit:MaskedEditValidator>
        </td>
    </tr>

    <tr>
        <td align="right">Remarks:</td>
        <td colspan="3">
            <asp:TextBox ID="txtReason" runat="server" CssClass="ControlDefaults" Height="74px"
                MaxLength="50" TextMode="MultiLine" Width="301px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReason"
                ErrorMessage="Reason for overtime cannot be blank">*</asp:RequiredFieldValidator>
        </td>
    </tr>

    <tr>
        <td align="right">&nbsp;</td>
        <td colspan="3">
            <br />
            <asp:Button ID="btnSubmit" runat="server" CssClass="ControlDefaults" Text="Process" OnClick="btnSubmit_Click" />
            <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>

    <ajaxToolkit:MaskedEditExtender ID="meeAppYear" runat="server" ErrorTooltipEnabled="True"
        Mask="9999" MaskType="Number" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtAppYear">
    </ajaxToolkit:MaskedEditExtender>

    <ajaxToolkit:MaskedEditExtender ID="meeAppMonth" runat="server" ErrorTooltipEnabled="True"
        Mask="99" MaskType="Number" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtAppMonth">
    </ajaxToolkit:MaskedEditExtender>

    <ajaxToolkit:MaskedEditExtender ID="meeTranDate" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtTranDate">
    </ajaxToolkit:MaskedEditExtender>

    <ajaxToolkit:CalendarExtender ID="caleTranDate" runat="server" PopupButtonID="ibtxtTranDate"
        TargetControlID="txtTranDate">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>

    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>

    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>


</asp:Content>

