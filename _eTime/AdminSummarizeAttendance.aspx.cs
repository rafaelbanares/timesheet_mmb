using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bns.AttendanceUI;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttUtils;
using BNS.TK.Business;
using BNS.TK.Entities;

public partial class AdminSummarizeAttendance : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            DataRow drPeriod = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                    MainDB + ".dbo.usa_AttendanceSummarizeGetPeriod", new SqlParameter[] {
                                new SqlParameter("@DBName", _KioskSession.DB)
                                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                                }).Tables[0].Rows[0];

            //rowID, StartDate, EndDate, PayStart, PayEnd, Posted, Finalized
            txtTranStart.Text = ((DateTime) drPeriod["StartDate"]).ToString(_KioskSession.DateFormat);
            txtTranEnd.Text = ((DateTime)drPeriod["EndDate"]).ToString(_KioskSession.DateFormat);
            txtPayStart.Text = ((DateTime)drPeriod["PayStart"]).ToString(_KioskSession.DateFormat);
            txtPayEnd.Text = ((DateTime)drPeriod["PayEnd"]).ToString(_KioskSession.DateFormat);

            hfRowID.Value = drPeriod["rowID"].ToString();

            //txtTranStart.Text= _KioskSession.StartDate.ToShortDateString();
            //txtTranEnd.Text  = _KioskSession.EndDate.ToShortDateString();
            //txtPayStart.Text = txtTranStart.Text;
            //txtPayEnd.Text   = txtTranEnd.Text;

            //no used ^^
            //btnSummarize.Focus();
        }
    }

    private void ReprocessAttendance()
    {        
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(DateTime.Parse(txtTranStart.Text), DateTime.Parse(txtTranEnd.Text), "");
    }

    private void Summarize(bool forceSummarize)
    {
        //process attendance to make sure
        ReprocessAttendance();


        lblMessage.Text = "";
        lblMessage.ForeColor = System.Drawing.Color.Red;
        btnForceSummarize.Visible = false ;

        DataSet dsCheck = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                                MainDB + ".dbo.usa_AttendanceSummarizeCheck", new SqlParameter[] {
                                new SqlParameter("@DBName", _KioskSession.DB)
                                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                                ,new SqlParameter("@PayStart",  txtPayStart.Text)
                                ,new SqlParameter("@PayEnd",    txtPayEnd.Text)
                                ,new SqlParameter("@TransStart",txtTranStart.Text )
                                ,new SqlParameter("@TransEnd",  txtTranEnd.Text)
                                });

        if (int.Parse(dsCheck.Tables[0].Rows[0]["SumUnProcessed"].ToString()) > 0)
        {
            //lblMessage.Text = "Some attendance corrections were not yet processed, please click <b>Process Correction</b> on the menu";
        }
        else if (int.Parse(dsCheck.Tables[1].Rows[0]["SumPayPeriod"].ToString()) > 0)
        {
            lblMessage.Text = "Payroll period was already summarized, please check your payroll period";
        }
        else if (int.Parse(dsCheck.Tables[2].Rows[0]["SumTransPeriod"].ToString()) > 0)
        {
            lblMessage.Text = "Transaction period was already summarized, please check your transaction period";
        }
        else if (!forceSummarize && int.Parse(dsCheck.Tables[0].Rows[0]["SumErroneous"].ToString()) > 0)
        {
            lblMessage.Text = "There are still some erroneous attendance transactions, if you click force summarize, erroneous transactions will be marked as absent";
            btnForceSummarize.Visible = true;
        }

        if (lblMessage.Text == "")
        {
            //... Sumarized reopened workdistribution
            SummarizeReOpen();

            //... VL or Absent if work distribution is not filed, or incomplete
            ProcessIncompleteWorkDistribution();

            //... Summarize current attendance
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure,
                                MainDB + ".dbo.usa_AttendanceSummarize", new SqlParameter[] {
                                new SqlParameter("@CompanyID",     _KioskSession.CompanyID)
                                ,new SqlParameter("@PayStart",      txtPayStart.Text)
                                ,new SqlParameter("@PayEnd",        txtPayEnd.Text)
                                ,new SqlParameter("@TransStart",    txtTranStart.Text )
                                ,new SqlParameter("@TransEnd",      txtTranEnd.Text)
                                ,new SqlParameter("@CreatedBy",     _KioskSession.UID)
                                ,new SqlParameter("@CreatedDate",   DateTime.Now)
                                ,new SqlParameter("@LastUpdBy",     _KioskSession.UID)
                                ,new SqlParameter("@LastUpDate",    DateTime.Now)
                            });
            lblMessage.ForeColor = System.Drawing.Color.Black;
            lblMessage.Text = "Summarized completed.";
            btnForceSummarize.Enabled = false;
            btnSummarize.Enabled = false;
        }
    }

    private void SummarizeReOpen()
    {
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure,
                                 MainDB + ".dbo.usa_AttendanceSummarizeReOpen", new SqlParameter[] {
                                new SqlParameter("@CompanyID",     _KioskSession.CompanyID)
                                ,new SqlParameter("@PayStart",      txtPayStart.Text)
                                ,new SqlParameter("@PayEnd",        txtPayEnd.Text)
                                ,new SqlParameter("@CreatedBy",     _KioskSession.UID)
                                ,new SqlParameter("@CreatedDate",   DateTime.Now)
                                ,new SqlParameter("@LastUpdBy",     _KioskSession.UID)
                                ,new SqlParameter("@LastUpDate",    DateTime.Now)
                            });
    }

    private void ProcessIncompleteWorkDistribution()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceIncompleteWorkDistribGet",
                    new SqlParameter[] {
                                new SqlParameter("@CompanyID",     _KioskSession.CompanyID)
                                ,new SqlParameter("@TransStart",    txtTranStart.Text )
                                ,new SqlParameter("@TransEnd",      txtTranEnd.Text) });

        foreach (DataRow dr in ds.Tables[0].Rows)
            dr.SetModified();

        //... needed for batch update, for time credits
        SqlConnection connection = new SqlConnection(_ConnectionString);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter();

            //... process no workdistrib and incomplete work distrib
            SqlCommand command = new SqlCommand(MainDB + ".dbo.usa_AttendanceIncompleteWorkDistrib", connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
            command.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Char, 15)).SourceColumn = "EmployeeID";
            command.Parameters.Add(new SqlParameter("@Date", SqlDbType.SmallDateTime)).SourceColumn = "Date";
            //command.Parameters.Add(new SqlParameter("@DBname", dbCredential.DB));

            adapter.UpdateCommand = command;

            //... if single employee disable batch transaction.
            //adapter.UpdateBatchSize = singleemp ? 1 : 0;
            adapter.UpdateBatchSize = ds.Tables[0].Rows.Count == 1 ? 1 : 0;
            //adapter.UpdateBatchSize = 0;
            command.UpdatedRowSource = UpdateRowSource.None;
            command.CommandTimeout = 0;

            adapter.Update(ds);
        }
        catch (Exception ex)
        {
            throw (new Exception(ex.ToString()));
        }
        finally
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
            }
        }
    }

    protected void btnSummarize_Click(object sender, EventArgs e)
    {
        Summarize(false);        
    }

    protected void btnForceSummarize_Click(object sender, EventArgs e)
    {
        Summarize(true);
    }

}
