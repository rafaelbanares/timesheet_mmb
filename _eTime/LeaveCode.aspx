<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveCode.aspx.cs" Inherits="LeaveCode" Title="Bisneeds - Timekeeping - Leave Codes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Leave Code Settings"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Leave Code" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Leave Code</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" Width="680px">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>

                    <asp:TemplateField HeaderText="Leave Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCode" runat="server" CssClass="TextBoxCode" Text='<%# Bind("Code") %>' Width="76px" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>                        
                            <asp:Literal ID="litCode" runat="server" Text='<%# Bind("Code") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Description") %>' MaxLength="30" Width="190px"></asp:TextBox>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Literal ID="litDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                        </ItemTemplate>                       
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Earning">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEarning" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Earning") %>' Width="80px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeEarning" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="99.9999" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtEarning">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litEarning" runat="server" Text='<%# Bind("Earning") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>                  
                    
                    <asp:TemplateField HeaderText="Year&lt;br&gt;Total">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtYearTotal" runat="server" CssClass="ControlDefaults" Text='<%# Bind("YearTotal") %>' Width="70px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeYearTotal" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="999.99" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtYearTotal">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litYearTotal" runat="server" Text='<%# Bind("YearTotal") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Days Filing&lt;br&gt;in Advance">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFilingDays" runat="server" CssClass="ControlDefaults" Text='<%# Bind("FilingDays") %>' Width="75px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeFilingDays" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="999.99" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtFilingDays">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litFilingDays" runat="server" Text='<%# Bind("FilingDays") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Check&lt;br&gt;Balance">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkCheckBalance" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgCheckBalance" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center"/>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="With&lt;br&gt;Pay">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkWithPay" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgWithPay" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center"/>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

