﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class LeaveEarnInsert : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        lblError.Text = "";
        if (!IsPostBack)
        {
            ScreenDefaults();
        }
    }

    private void ScreenDefaults()
    {
        string qry2 =
            "select '' as Code, '---ALL---' as Description  union all " +
            "select Code, Description  from " + _KioskSession.DB + ".dbo.LeaveCode where companyid='" + _KioskSession.CompanyID + "' and yeartotal > 0 and withpay = 1 ";

        DataSet dsCode = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, qry2);
        ddlLeave.DataSource = dsCode.Tables[0];
        ddlLeave.DataTextField = "Description";
        ddlLeave.DataValueField = "Code";
        ddlLeave.DataBind();

        DateTime now = DateTime.Now;
        string today = Tools.ShortDate(now.ToShortDateString(), _KioskSession.DateFormat);

        //.... assign default values, this values can be change by the user
        txtAppMonth.Text = now.Month.ToString();
        txtAppYear.Text = now.Year.ToString();

        txtDateStart.Text = "01/01/" + now.Year.ToString();
        txtDateEnd.Text = "12/31/" + now.Year.ToString();

        //.... example if date is March 27, default applicable month is for April
        if (now.Day > 1)
        {
            if (now.Month + 1 == 13)
            {
                txtAppMonth.Text = "1";
                txtAppYear.Text = (now.Year + 1).ToString();
            }
            else
            {
                txtAppMonth.Text = (now.Month + 1).ToString();
            }
        }
    
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@TranDate", txtTranDate.Text ));
        sqlparam.Add(new SqlParameter("@LeaveCode", ddlLeave.SelectedValue));
        sqlparam.Add(new SqlParameter("@AppYear", txtAppYear.Text ));
        sqlparam.Add(new SqlParameter("@AppMonth", txtAppMonth.Text ));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        string ErrMsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveEarnInsertValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();

        if (ErrMsg != "")
        {
            lblError.Text = ErrMsg;
        }
        else
        {
            sqlparam.Add(new SqlParameter("@ValidFrom", Tools.DefaultNull(txtDateStart.Text)));
            sqlparam.Add(new SqlParameter("@ValidTo", Tools.DefaultNull(txtDateEnd.Text)));
            sqlparam.Add(new SqlParameter("@Remarks", txtReason.Text));

            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

            string result = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveEarnInsert", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();

            lblError.Text = result + " leave earnings added";
        }
    }

}
