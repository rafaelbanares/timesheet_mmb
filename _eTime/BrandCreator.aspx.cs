﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

using Microsoft.ApplicationBlocks.Data;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class BrandCreator : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            gvMain.Columns[0].HeaderText = _KioskSession.WorkFor;
            gvMain.Columns[1].HeaderText = _KioskSession.WorkForSub;

            BindGrid(false);
        }
    }

    private void BindGrid(bool isAdd)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForCreatorLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@IsBlankData", isAdd), new SqlParameter("@DBname", _KioskSession.DB) });

        if (isAdd)
        {
            //ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);
            ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);

            gvMain.DataSource = ds.Tables[0];
            gvMain.EditIndex = 0; // ds.Tables[0].Rows.Count - 1;
            gvMain.DataBind();
        }
        else
        {
            gvMain.DataSource = ds.Tables[0];
            gvMain.DataBind();
        }
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid(false);
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvMain.EditIndex)
            {
                (e.Row.FindControl("ibDelete") as ImageButton).Visible = gvMain.EditIndex == -1;
                (e.Row.FindControl("ibEdit") as ImageButton).Visible = gvMain.EditIndex == -1;
            }
            else
            {
                string workForID = DataBinder.Eval(e.Row.DataItem, "WorkForID").ToString();
                string workForSubID = DataBinder.Eval(e.Row.DataItem, "WorkForSubID").ToString();
                string employeeID = DataBinder.Eval(e.Row.DataItem, "EmployeeID").ToString();

                DropDownList ddlWorkFor = e.Row.FindControl("ddlWorkFor") as DropDownList;
                DataSet dsWorkFor = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                    "SELECT WorkForID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkFor a WHERE CompanyID = @CompanyID " +
                    "   AND EXISTS (SELECT TOP 1 1 FROM " + _KioskSession.DB + ".dbo.WorkForSub b WHERE a.WorkForID = b.WorkForID ) ORDER BY ShortDesc " +
                    " SELECT EmployeeID, FullName FROM " + _KioskSession.DB + ".dbo.Employee WHERE CompanyID = @CompanyID ORDER BY FullName "
                    , new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

                ddlWorkFor.DataSource = dsWorkFor.Tables[0];
                ddlWorkFor.DataBind();

                if (workForID == "")
                    workForID = ddlWorkFor.SelectedItem.Value;

                ddlWorkFor.SelectedValue = ddlWorkFor.Items.FindByValue(workForID).Value;

                DropDownList ddlWorkForSub = e.Row.FindControl("ddlWorkForSub") as DropDownList;
                populateWorkForSub(workForID, workForSubID, ddlWorkForSub);

                DropDownList ddlEmployeeID = e.Row.FindControl("ddlEmployeeID") as DropDownList;
                ddlEmployeeID.DataSource = dsWorkFor.Tables[1];
                ddlEmployeeID.DataBind();

                if (employeeID != "")
                    ddlEmployeeID.SelectedValue = ddlEmployeeID.Items.FindByValue(employeeID).Value;
            }
        }
    }
    protected void ddlWorkFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlWorkFor = (DropDownList)sender;
        DropDownList ddlWorkForSub = ddlWorkFor.Parent.FindControl("ddlWorkForSub") as DropDownList;

        populateWorkForSub(ddlWorkFor.SelectedValue, "0", ddlWorkForSub);
    }

    private void populateEmployee(DropDownList ddlEmployeeID)
    {
    }

    

    private void populateWorkForSub(string workForID, string workForSubID, DropDownList ddlWorkForSub)
    {
        DataSet dsWorkForSub = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            " SELECT WorkForSubID, ShortDesc FROM " + _KioskSession.DB + ".dbo.WorkForSub WHERE CompanyID = @CompanyID AND WorkForID = @WorkForID ORDER BY ShortDesc "             
            ,new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@WorkForID", workForID) });

        ddlWorkForSub.DataSource = dsWorkForSub.Tables[0];
        ddlWorkForSub.DataBind();

        if (ddlWorkForSub.Items.FindByValue(workForSubID) != null)
            ddlWorkForSub.SelectedValue = ddlWorkForSub.Items.FindByValue(workForSubID).Value;
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["rowID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForCreatorDelete",
            new SqlParameter[] { 
                new SqlParameter("@rowID", key), 
                new SqlParameter("@DBname", _KioskSession.DB) 
            });

        gvMain.EditIndex = -1;
        BindGrid(false);

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid(false);
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["rowID"].ToString();

        DropDownList ddlWorkForSub = gvMain.Rows[e.RowIndex].FindControl("ddlWorkForSub") as DropDownList;
        DropDownList ddlEmployeeID = gvMain.Rows[e.RowIndex].FindControl("ddlEmployeeID") as DropDownList;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@rowID", key));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@WorkForSubID", ddlWorkForSub.SelectedValue));
        sqlparam.Add(new SqlParameter("@EmployeeID", ddlEmployeeID.SelectedValue));
        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        string errmsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForCreatorInsertUpdate",
            sqlparam.ToArray()).Tables[0].Rows[0]["ErrMsg"].ToString();

        if (errmsg != "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "insertfail", "alert('" + errmsg + "');", true);
        }
        else
        {
            gvMain.EditIndex = -1;
            BindGrid(false);
        }
    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
}
