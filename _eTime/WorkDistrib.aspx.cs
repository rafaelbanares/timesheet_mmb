﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using System.Transactions;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;
using BNS.TK.Business;
using BNS.TK.Entities;

/* BR: If timein and out is greater than 5 deduct a 1 hour break*/

public partial class WorkDistrib : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            InitializeGridHeaderText();
            populateLeaveDropdown();
            PopulateAllControls();

            if (chkEarlyLogin.Checked && ApproverNo() != "")
                ScriptManager.RegisterStartupScript(this, typeof(Page), "statusApprove", "alert('ALERT: This employee filed for early log-in. Make sure that reason for working early is valid before approving.');", true);
        }
    }

    #region InitializeGridHeaderText
    private void InitializeGridHeaderText()
    {
        gvMain.Columns[0].HeaderText = _KioskSession.WorkFor;
        gvMain.Columns[1].HeaderText = _KioskSession.WorkForSub;
    }
    #endregion

    #region ApproverNo
    private string ApproverNo()
    {
        if (_Url["approverno"] == null)
            return "";
        else
            return _Url["approverno"];
    }
    #endregion

    #region ApprovalMode
    private bool ApprovalMode()
    {
        return (ApproverNo() != "");
    }
    #endregion

    #region Url Properties
    private string dateUrl 
     {
         get { return _Url["Date"]; }
     }
    private string employeeIdUrl
    {
        get { return _Url["EmpID"].Trim(); }
    }
    #endregion

    #region IsResigned
    private bool IsResigned()
    {
        Employee emp = new Employee();
        emp.Query.AddResultColumn(Employee.ColumnNames.DateTerminated); 
        emp.Where.CompanyID.Value = _KioskSession.CompanyID;
        emp.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
        emp.Where.EmployeeID.Value = employeeIdUrl;
        emp.Query.Load("");
        return (emp.RowCount == 1 && emp.s_DateTerminated != "");
    }
    #endregion

    #region IsOTExempt
    private bool IsOTExempt()
    {
        Employee emp = new Employee();
        emp.Query.AddResultColumn(Employee.ColumnNames.OTExempt);
        emp.Where.CompanyID.Value = _KioskSession.CompanyID;
        emp.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
        emp.Where.EmployeeID.Value = employeeIdUrl;
        emp.Query.Load("");
        return (emp.RowCount == 1 && emp.OTExempt);
    }
    #endregion

    //called on initial load and btnRevise_Click()
    private void PopulateAllControls()
    {        
        SetInitialValues();

        if (ApprovalMode())
            btnRecall.Text = "Recall Approval";

        //Disable leave if already resigned
        if (!ApprovalMode() && IsResigned())
        {
            ddlLeave.SelectedIndex = 0;
            txtLvHours.Text = "";
            ddlLeave.Enabled = false;
            txtLvHours.Enabled = false;
        }

        int offbal = OffsetEarn.OffsetBalance(_KioskSession.CompanyID, employeeIdUrl, DateTime.Parse(dateUrl));
        if (offbal >= 60)
            lblOffsetbal.Text = string.Format("{0} hrs,  {1} mins", offbal / 60, offbal % 60);
        else
            lblOffsetbal.Text = string.Format("{0} mins", offbal);
    }

    //called on reload()
    //ie: on initial load and btnRevise_Click()
    private void SetInitialValues()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, "tk_lowe.dbo.usa_WorkDistHdrGetInsert",
            new SqlParameter[] { 
                new SqlParameter("@DBname", _KioskSession.DB) 
                , new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                , new SqlParameter("@EmployeeID", employeeIdUrl) 
                , new SqlParameter("@Date", dateUrl) 
                , new SqlParameter("@AuthBy", _KioskSession.OTApproverID) 
                , new SqlParameter("@LastUpdBy", _KioskSession.UID) 
                , new SqlParameter("@LastUpdDate", DateTime.Now) 
            });

        hfWorkDistID.Value = ds.Tables[0].Rows[0]["WorkDistID"].ToString();
        hfOTCode.Value = ds.Tables[1].Rows[0]["OTCode"].ToString().Trim();

        lblDate.Text = dateUrl;
        txtRemarks.Text = ds.Tables[0].Rows[0]["Reason"].ToString().Trim();
        txtRemarks2.Text = ds.Tables[1].Rows[0]["leavereason"].ToString().Trim();
        txtNewRemarks.Text = txtRemarks.Text;
        txtNewRemarks2.Text = txtRemarks2.Text;

        
        string origin1 = ds.Tables[0].Rows[0]["orig_In1"].ToString();
        string origout1 = ds.Tables[0].Rows[0]["orig_Out1"].ToString();
        string in1 = ds.Tables[0].Rows[0]["new_in1"].ToString();
        string out1 = ds.Tables[0].Rows[0]["new_out1"].ToString();

        string status = ds.Tables[0].Rows[0]["status"].ToString();
        bool completed = (bool)ds.Tables[0].Rows[0]["completed"];

        if (origin1 != "")
        {
            lblOrigIn.Text = DateTime.Parse(origin1).ToShortDateString() + " " + DateTime.Parse(origin1).ToShortTimeString();
            txtOrig_In.Text = DateTime.Parse(origin1).ToShortDateString() + " " + DateTime.Parse(origin1).ToShortTimeString();
        }
        if (origout1 != "")
        {
            lblOrigOut.Text = DateTime.Parse(origout1).ToShortDateString() + " " + DateTime.Parse(origout1).ToShortTimeString();
            txtOrig_Out.Text = DateTime.Parse(origout1).ToShortDateString() + " " + DateTime.Parse(origout1).ToShortTimeString();
        }

        txtDateIn.Text = (in1 == "") ? dateUrl : String.Format("{0:MM/dd/yyyy}", DateTime.Parse(in1));
        txtDateOut.Text = (out1 == "") ? dateUrl : String.Format("{0:MM/dd/yyyy}", DateTime.Parse(out1));
        txtIn1.Text = (in1 == "") ? "" : String.Format("{0:hh:mm tt}", DateTime.Parse(in1));
        txtOut1.Text = (out1 == "") ? "" : String.Format("{0:hh:mm tt}", DateTime.Parse(out1));
        txtLvHours.Text = double.Parse(ds.Tables[0].Rows[0]["leaveHours"].ToString()).ToString("#0.00");
        ddlLeave.SelectedValue = ddlLeave.Items.FindByValue(ds.Tables[0].Rows[0]["leaveCode"].ToString().Trim()).Value;
        hfCompleted.Value = completed ? "1" : "0";

        txtNewDateIn.Text = txtDateIn.Text;
        txtNewTimeIn.Text = txtIn1.Text;
        txtNewDateOut.Text = txtDateOut.Text;
        txtNewTimeOut.Text = txtOut1.Text;        
        txtNewRemarks.Text = txtRemarks.Text;

        //not sure why this was put here in the middle
        PopulateComputedValueControls();
        BindGrid();

        bool absent = (txtIn1.Text == "" && txtOut1.Text == "");
        bool incomplete = ((txtIn1.Text == "" && txtOut1.Text != "") || (txtIn1.Text != "" && txtOut1.Text == ""));
        bool regularday = hfOTCode.Value == "REG";

        if (completed)
            lblStatus.Text = status;

        if (status.ToLower().TrimEnd() == "declined")
        {
            pnlDclReason.Visible = true;
            lblDeclinedReason.Text = ds.Tables[0].Rows[0]["DeclinedReason"].ToString();
        }

        bool notyetapproved = status.ToLower() != "approved";
        bool declined = status.ToLower() == "declined";
        bool finalized = (bool)ds.Tables[1].Rows[0]["Finalized"];
        bool approvalmode = ApprovalMode();
        if (finalized)
            approvalmode = true; // if finalized set approvalmode = true, so that work distribution is uneditable.

        btnRevise.Visible = !approvalmode && completed;
        btnRoute.Visible = !approvalmode && !completed;
        lbEditInOut.Visible = !approvalmode && !completed;
        

        lbNew.Enabled = !completed && !absent && !incomplete;
        txtIn1.Enabled = !completed;
        txtOut1.Enabled = !completed;
        txtRemarks.Enabled = btnRevise.Visible == false && notyetapproved; //!completed;

        ddlLeave.Enabled = regularday && !completed;
        txtLvHours.Enabled = regularday && !completed;
        txtRemarks2.Enabled = regularday && !completed;

        lblAction.Visible = approvalmode && !finalized && notyetapproved && !declined;
        btnApprove.Visible = approvalmode && !finalized && notyetapproved && !declined;
        btnDecline.Visible = approvalmode && !finalized && notyetapproved && !declined;
        btnRecall.Visible = approvalmode && !notyetapproved; //&& !finalized;

        txtDateIn.Enabled = txtIn1.Enabled;
        txtDateOut.Enabled = txtOut1.Enabled;
    }


    private void PopulateComputedValueControls()
    {
        Attendance att = new Attendance();
        if (att.LoadByPrimaryKey(_KioskSession.CompanyID, employeeIdUrl, DateTime.Parse(dateUrl)))
        {
            lblRegHrs.Text = att.Reghrs.ToString("#0.00");
            lblExcessHrs.Text = att.Excesshrs.ToString("#0.00");
            lblTime.Text = (double.Parse(lblRegHrs.Text) + double.Parse(lblExcessHrs.Text)).ToString("#0.00");
            

            //refresh computed values
            if (att.Latemins >= 60)
                lblLate.Text = string.Format("{0} ({1} hrs,  {2} mins)", att.Late, att.Latemins / 60, att.Latemins % 60);
            else
                lblLate.Text = string.Format("{0} ({1} mins)", att.Late, att.Latemins % 60);

            if (att.UTmins >= 60)
                lblUT.Text = string.Format("{0} ({1} hrs,  {2} mins)", att.UT, att.UTmins / 60, att.UTmins % 60);
            else
                lblUT.Text = string.Format("{0} ({1} mins)", att.UT, att.UTmins % 60);

            if (att.First8mins >= 60)
                lblOT.Text = string.Format("{0} ({1} hrs,  {2} mins)", att.First8, att.First8mins / 60, att.First8mins % 60);
            else
                lblOT.Text = string.Format("{0} ({1} mins)", att.First8, att.First8mins % 60);

            //show/hide
            lblLate.Visible = (att.Late > 0);
            lblLatelabel.Visible = (att.Late > 0);
            lblUT.Visible = (att.UT > 0);
            lblUTlabel.Visible = (att.UT > 0);
            lblOT.Visible = (att.First8mins > 0);
            lblOTlabel.Visible = (att.First8mins > 0);
            chkEarlyLogin.Visible = hfOTCode.Value == "REG";
            chkEarlyLogin.Checked = att.EarlyWork;

            txtEditOfstHrs.Text = (att.Late + att.UT).ToString("#0.00");
            lblEditOfsMins.Text = (att.Latemins + att.UTmins).ToString("#0.00");

            //for OTExempt employees only
            lbFileOffset.Visible = (IsOTExempt()) && (hfCompleted.Value != "1") && (att.Latemins + att.UTmins) > 0 ? true : false;
            
            int trdut = att.Latemins + att.UTmins;
            hfOfsMins.Value = trdut.ToString();
            if (trdut >= 60)
                lblEditOfsMins.Text = string.Format("({0} hrs,  {1} mins)", trdut / 60, trdut % 60);
            else
                lblEditOfsMins.Text = string.Format("({0} mins)", trdut % 60);
                        
            //display currenly applied offset
            int offsetmins;
            decimal offsethrs = GetOffsetUsed(DateTime.Parse(dateUrl), out offsetmins);
            lblOffsetlabel.Visible = (offsetmins > 0) ? true : false;
            lblOffset.Visible = lblOffsetlabel.Visible;
            lbCancelOfs.Visible = lblOffsetlabel.Visible && (hfCompleted.Value != "1");
            
            if (offsetmins >= 60)
                lblOffset.Text = string.Format("{0:n} ({1} hrs,  {2} mins)", offsethrs, offsetmins / 60, offsetmins % 60);
            else
                lblOffset.Text = string.Format("{0:n} ({1} mins)", offsethrs, offsetmins % 60);
            
            //show new button if there is rendered hours
            if (att.Rendered > 0)
                lbNew.Enabled = true;
        }
        else
        {
            lblTime.Text = "0.00";
            lblExcessHrs.Text = "0.00";
            lblRegHrs.Text = "0.00";
            lblLate.Visible = false;
            lblLatelabel.Visible = false;
            lblUT.Visible = false;
            lblUTlabel.Visible = false;
            lblOT.Visible = false;
            lblOTlabel.Visible = false;
            chkEarlyLogin.Visible = false;
            lbFileOffset.Visible = false;
            lbCancelOfs.Visible = false;
        }        
    }

    private decimal GetOffsetUsed(DateTime date, out int offsetmins)
    {
        OffsetTrans ofs = new OffsetTrans();
        ofs.Where.CompanyID.Value = _KioskSession.CompanyID;
        ofs.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
        ofs.Where.EmployeeID.Value = _Url["EmpID"].Trim();
        ofs.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);        
        ofs.Where.DateUsed.Value = date;
        ofs.Query.Load("");
        offsetmins = 0;
        if (ofs.RowCount > 0)
        {
            offsetmins = ofs.OffsetMins;
            return Convert.ToDecimal(offsetmins)/60M;
        }
        return 0;
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistGetByWorkDistID",
            new SqlParameter[] { 
                new SqlParameter("@DBname", _KioskSession.DB) 
                , new SqlParameter("@WorkDistID", hfWorkDistID.Value )
            });

        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";

        
        UpdateTotals(ds);
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistGetByWorkDistID",
            new SqlParameter[] {
                new SqlParameter("@DBname", _KioskSession.DB) 
                , new SqlParameter("@WorkDistID", hfWorkDistID.Value )
                , new SqlParameter("@IsBlankData", true)
            });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvMain.DataBind();
        hfIsAdd.Value = "1";
    }

    private void UpdateTotals(DataSet ds)
    {
        decimal total = 0.00M;
        foreach (DataRow dr in ds.Tables[0].Rows)
            total += (decimal)dr["Total"];

        lblRunning.Text = total.ToString();
        btnRoute.Enabled = false;

        lblStatus.Text = "incomplete/erroneous";

        //... leave entry is always completed if regular hours is not
        AssignLeaveHours();
        bool completed = wholeDayLeaveCompleted();
        decimal diff = 0;
        if (lblTime.Text != "0.00")
        {
            diff = decimal.Parse(lblTime.Text) - total;            
            lblDiff.Text = diff.ToString();
            lblDiff.ForeColor = completed ? System.Drawing.Color.Black : System.Drawing.Color.Red;
        }
        else
            lblDiff.Text = "0.00";

        if (diff == 0 && TimeEntryExist_or_LeaveSelected())
        {
            lblStatus.Text = "OK, ready for routing";
            btnRoute.Enabled = true;
        }
    }
    private bool TimeEntryExist_or_LeaveSelected()
    {
        Decimal reghrs = Decimal.Parse(lblTime.Text);
        decimal lvhrs = 0;
        decimal.TryParse(txtLvHours.Text, out lvhrs);

        return (reghrs > 0) || (reghrs == 0 && lvhrs > 0 && ddlLeave.SelectedIndex > 0);
    }

    private void AssignLeaveHours()
    {        
        //... assign leave hours only if regularday and not completed
        if (hfCompleted.Value != "1" && hfOTCode.Value == "REG")
        {            
            if (txtIn1.Text == "" || txtOut1.Text == "")
            {                
                if (txtLvHours.Text.TrimEnd() == "" || Decimal.Parse(txtLvHours.Text) == 0)
                    txtLvHours.Text = "8.00";
                

                if (ddlLeave.SelectedIndex == 0)
                    ddlLeave.SelectedValue = ddlLeave.Items.FindByValue("VL").Value;
            }
        }
    }

    private bool wholeDayLeaveCompleted()
    {
        bool retval = false;
        string code = ddlLeave.SelectedItem.Value;
        double hours = 0;
        double.TryParse(txtLvHours.Text, out hours);

        bool incomplete = ((txtIn1.Text == "" && txtOut1.Text != "") || (txtIn1.Text != "" && txtOut1.Text == ""));
        bool absent = (txtIn1.Text == "" && txtOut1.Text == "");

        //... leave balance might be less than 8 hours
        retval = !incomplete && (code != "" && hours <= 8.00 && hours > 0 && absent && gvMain.Rows.Count == 0);

        return retval;
    }
                          
    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");

            bool isoffset = (DataBinder.Eval(e.Row.DataItem, "WorkForID").ToString() == "-1");

            if (gvMain.EditIndex == -1)
            {
                ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;

                ibDelete.Visible = (hfCompleted.Value == "0") && !isoffset;
                ibEdit.Visible = (hfCompleted.Value == "0") && !isoffset;

                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
            }
            else
            {
                if (e.Row.RowIndex == gvMain.EditIndex)
                {
                    if (hfOTCode.Value == "REG")
                    {
                        TextBox txtRstOT = e.Row.FindControl("txtRstOT") as TextBox;
                        txtRstOT.Enabled = false;

                        if (decimal.Parse(lblExcessHrs.Text) == 0)
                        {
                            TextBox txtOT = e.Row.FindControl("txtOT") as TextBox;
                            txtOT.Enabled = false;
                        }
                    }
                    else
                    {
                        TextBox txtRegHrs = e.Row.FindControl("txtRegHrs") as TextBox;
                        TextBox txtOT = e.Row.FindControl("txtOT") as TextBox;

                        txtRegHrs.Enabled = false;
                        txtOT.Enabled = false;
                    }

                    string workForID = DataBinder.Eval(e.Row.DataItem, "WorkForID").ToString();
                    string workForSubID = DataBinder.Eval(e.Row.DataItem, "WorkForSubID").ToString();
                    string ProjID = DataBinder.Eval(e.Row.DataItem, "ProjID").ToString();
                                        
                    DropDownList ddlWorkFor = e.Row.FindControl("ddlWorkFor") as DropDownList;
                    string sql = "SELECT a.WorkForID, a.ShortDesc " +
                                    "FROM " + _KioskSession.DB + ".dbo.WorkFor a " +
                                    "WHERE " +
                                    "    a.CompanyID = @CompanyID AND " +
                                    "   (a.WorkForID = @workForID OR (a.Active = 1 AND " +
                                    "    EXISTS (SELECT TOP 1 1 FROM " + _KioskSession.DB + ".dbo.WorkForSub b " +
                                    "			 INNER JOIN " + _KioskSession.DB + ".dbo.WorkForSubProj c ON b.WorkForSubID = c.WorkForSubID " +
                                    "			 WHERE a.WorkForID = b.WorkForID AND b.Active = 1 AND c.Active = 1))) " +
                                    "ORDER BY a.ShortDesc ";

                    DataSet dsWorkFor = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, sql,
                                    new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@workForID", workForID) });

                    ddlWorkFor.DataSource = dsWorkFor;
                    ddlWorkFor.DataBind();
                    ddlWorkFor.Items.Insert(0, new ListItem("Idle Time", "0"));
                    ddlWorkFor.SelectedValue = ddlWorkFor.Items.FindByValue(workForID).Value;

                    DropDownList ddlWorkForSub = e.Row.FindControl("ddlWorkForSub") as DropDownList;
                    populateWorkForSub(workForID, workForSubID, ddlWorkForSub);

                    DropDownList ddlWorkForSubProj = e.Row.FindControl("ddlWorkForSubProj") as DropDownList;
                    populateWorkForSubProj(workForSubID, ProjID, ddlWorkForSubProj);
                }
                else if (isoffset)
                {
                    ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
                    ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                    ibDelete.Visible = false;
                    ibEdit.Visible = false;
                }
            }
        }
    }

    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["row_ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistDtlDelete",
            new SqlParameter[] { 
                new SqlParameter("@row_ID", key), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (validateEntries())
        {
            string key = gvMain.DataKeys[e.RowIndex].Values["row_ID"].ToString();

            DropDownList ddlWorkFor = gvMain.Rows[e.RowIndex].FindControl("ddlWorkFor") as DropDownList;
            DropDownList ddlWorkForSub = gvMain.Rows[e.RowIndex].FindControl("ddlWorkForSub") as DropDownList;
            DropDownList ddlWorkForSubProj = gvMain.Rows[e.RowIndex].FindControl("ddlWorkForSubProj") as DropDownList;

            string[] s = ddlWorkForSubProj.SelectedItem.Value.Split('~');

            TextBox txtTask = gvMain.Rows[e.RowIndex].FindControl("txtTask") as TextBox;
            TextBox txtRegHrs = gvMain.Rows[e.RowIndex].FindControl("txtRegHrs") as TextBox;
            TextBox txtOT = gvMain.Rows[e.RowIndex].FindControl("txtOT") as TextBox;
            TextBox txtRstOT = gvMain.Rows[e.RowIndex].FindControl("txtRstOT") as TextBox;


            List<SqlParameter> sqlparam = new List<SqlParameter>();

            sqlparam.Add(new SqlParameter("@WorkForID", ddlWorkFor.SelectedValue));
            sqlparam.Add(new SqlParameter("@WorkForSubID", ddlWorkForSub.SelectedValue));
            sqlparam.Add(new SqlParameter("@ProjID", s[0]));
            sqlparam.Add(new SqlParameter("@Task", txtTask.Text));
            sqlparam.Add(new SqlParameter("@RegHrs", txtRegHrs.Text));
            sqlparam.Add(new SqlParameter("@OT", txtOT.Text));
            sqlparam.Add(new SqlParameter("@RstOT", txtRstOT.Text));
            sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
            sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

            if (hfIsAdd.Value == "1")
            {
                sqlparam.Add(new SqlParameter("@WorkDistID", hfWorkDistID.Value));
                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistDtlInsert", sqlparam.ToArray());
            }
            else
            {
                sqlparam.Add(new SqlParameter("@row_ID", key));
                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistDtlUpdate", sqlparam.ToArray());
            }

            gvMain.EditIndex = -1;
            BindGrid();
        }
    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }

    protected void ddlWorkFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlWorkFor = (DropDownList)sender;
        DropDownList ddlWorkForSub = ddlWorkFor.Parent.FindControl("ddlWorkForSub") as DropDownList;
        DropDownList ddlWorkForSubProj = ddlWorkForSub.Parent.FindControl("ddlWorkForSubProj") as DropDownList;

        //populateWorkForSub(ddlWorkFor.SelectedValue,"0", ddlWorkForSub );
        populateWorkForSub(ddlWorkFor.SelectedValue, "0" , ddlWorkForSub);
        populateWorkForSubProj(ddlWorkForSub.SelectedValue, "0", ddlWorkForSubProj);    
    }

    protected void ddlWorkForSub_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlWorkForSub = (DropDownList)sender;
        DropDownList ddlWorkForSubProj = ddlWorkForSub.Parent.FindControl("ddlWorkForSubProj") as DropDownList;

        populateWorkForSubProj(ddlWorkForSub.SelectedValue, "0", ddlWorkForSubProj);
    }


    private void populateWorkForSub(string workForID, string workForSubID, DropDownList ddlWorkForSub)
    {
        if (workForID == "0")
        {
            ddlWorkForSub.Items.Clear();
            ddlWorkForSub.Items.Insert(0, new ListItem("Idle Time", "0"));
        }
        else
        {
            string sql = "SELECT a.WorkForSubID, a.ShortDesc " +
                            "FROM " + _KioskSession.DB + ".dbo.WorkForSub a " +
                            "WHERE " +
                            "	a.CompanyID = @CompanyID AND " +
                            "	a.WorkForID = @WorkForID AND " +
                            "	(a.WorkForSubID = @workForSubID OR (a.Active = 1 AND " +
                            "	EXISTS (SELECT TOP 1 1 FROM " + _KioskSession.DB + ".dbo.WorkForSubProj b WHERE a.WorkForSubID = b.WorkForSubID AND b.Active = 1))) " +
                            "ORDER BY a.ShortDesc ";

            DataSet dsWorkForSub = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, sql,
                            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@WorkForID", workForID), new SqlParameter("@workForSubID", workForSubID) });

            ddlWorkForSub.DataSource = dsWorkForSub;
            ddlWorkForSub.DataBind();
            //ddlWorkForSub.Items.Insert(0, new ListItem("Idle Time", "0"));
            if (workForSubID != "0")
                ddlWorkForSub.SelectedValue = ddlWorkForSub.Items.FindByValue(workForSubID).Value;
        }
    }

    private void populateWorkForSubProj(string workForSubID, string projID, DropDownList ddlWorkForSubProj)
    {
        if (workForSubID == "0")
        {
            ddlWorkForSubProj.Items.Clear();
            ddlWorkForSubProj.Items.Insert(0, new ListItem("Idle Time", "0~Idle Time"));
        }
        else
        {
            string sql = "SELECT " +
                            "	cast(ProjID as varchar(10)) + '~' + Description as ProjIDDesc, " +
                            "	ShortDesc " +
                            "FROM " + _KioskSession.DB + ".dbo.WorkForSubProj " +
                            "WHERE " +
                            "	CompanyID = @CompanyID AND " +
                            "	WorkForSubID = @WorkForSubID AND " +
                            "	(ProjID = @projID OR Active = 1) " +
                            "ORDER BY shortdesc ";
            DataSet dsWorkForSubProj = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, sql,
                            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@WorkForSubID", workForSubID), new SqlParameter("@projID", projID) });

            ddlWorkForSubProj.DataSource = dsWorkForSubProj;
            ddlWorkForSubProj.DataBind();
            //ddlWorkForSubProj.Items.Insert(0, new ListItem("Idle Time", "0"));
            if (projID != "0")
            {
                for (int i = 0; i < ddlWorkForSubProj.Items.Count; i++)
                    if (ddlWorkForSubProj.Items[i].Value.Contains(projID + "~"))
                    {
                        ddlWorkForSubProj.SelectedIndex = i;
                        break;
                    }
            }
            else
            {
                TextBox txtTask = ddlWorkForSubProj.Parent.FindControl("txtTask") as TextBox;
                txtTask.Text = ddlWorkForSubProj.SelectedItem.Text;
            }

        }
    }


    private bool validateEntries()
    {
        bool retval = true;
        decimal totreghrs = decimal.Parse(lblRegHrs.Text);
        decimal totexcess = decimal.Parse(lblExcessHrs.Text);

        // before updating must identify if reghrs is valid.
        decimal hrsReg = 0;
        decimal hrsOT = 0;
        decimal hrsRstOT = 0;

        for (int i = 0; i < gvMain.Rows.Count; i++)
        {
            if (gvMain.EditIndex == i)
            {
                TextBox txtRegHrs = gvMain.Rows[i].FindControl("txtRegHrs") as TextBox;
                TextBox txtRstOT = gvMain.Rows[i].FindControl("txtRstOT") as TextBox;
                TextBox txtOT = gvMain.Rows[i].FindControl("txtOT") as TextBox;

                if (txtRegHrs.Text.Trim() == "") txtRegHrs.Text = "0.00";
                if (txtRstOT.Text.Trim() == "") txtRstOT.Text = "0.00";
                if (txtOT.Text.Trim() == "") txtOT.Text = "0.00";

                hrsReg += decimal.Parse(txtRegHrs.Text);
                hrsRstOT += decimal.Parse(txtRstOT.Text);
                hrsOT += decimal.Parse(txtOT.Text);
            }
            else
            {
                hrsReg += decimal.Parse((gvMain.Rows[i].FindControl("lblRegHrs") as Label).Text);
                hrsRstOT += decimal.Parse((gvMain.Rows[i].FindControl("lblRstOT") as Label).Text);
                hrsOT += decimal.Parse((gvMain.Rows[i].FindControl("lblOT") as Label).Text);
            }
        }

        if (hrsReg > totreghrs)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "scriptreghrs"
                , string.Format("alert('Regular hours cannot exceed {0}');", totreghrs), true);
            retval = false;
        }
        else if (hrsOT + hrsRstOT > totexcess)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "scriptothrs"
                , string.Format("alert('OT hours cannot exceed {0}');", totexcess), true);
            retval = false;
        }
        return retval;
    }

    private bool CheckBirthDayLeave()
    {
        if (ddlLeave.SelectedItem.Value.TrimEnd() == "BL")
        {
            Employee emp = new Employee();
            emp.Query.AddResultColumn(Employee.ColumnNames.BirthDate);
            emp.Where.CompanyID.Value = _KioskSession.CompanyID;
            emp.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            emp.Where.EmployeeID.Value = _KioskSession.EmployeeID;
            emp.Query.Load("");
            if (emp.RowCount == 1 && emp.s_BirthDate != "" && emp.BirthDate.Month == DateTime.Parse(_Url["Date"]).Month)
                return true;
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "blleave", "alert('You can only use birthday leave within the month of your actual birthday.');", true);
                return false;
            }
        }

        return true;
    }


    private bool SickLeaveAlert()
    {
        if (ddlLeave.SelectedItem.Value.TrimEnd() == "SL")
        {
            btnRoute.Attributes.Add("onclick", "return window.confirm('ALERT: This employee filed for a sick leave, check adjacent dates before approving. Sick leaves for 3 days or more.\\n" +
                            "require submission of medical certificate to Human Resources. Remind your staff as needed.\\n Approve Leave?' );");
        }
        else
            btnRoute.Attributes.Remove("onclick"); 

        return true;
    }

    private bool ValidateLeaveHours()
    {
        decimal lvhrs = 0;
        decimal.TryParse(txtLvHours.Text, out lvhrs);
        if (lvhrs != 0)
        {
            if (lvhrs == 8 || lvhrs == 4)
                return true;
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Leave", "alert('You can only file wholeday(8hrs) or halfday(4hrs) leave.');", true);
                return false;
            }
        }
        else
            return true;
    }

    protected void btnRoute_Click(object sender, EventArgs e)
    {
        if (!ValidateLeaveHours())
            return;

        //... check leave balance first
        if (!CheckLeaveBalance())
            return;

        if (!CheckBirthDayLeave())
            return;

        string in1 = Utils.TimeToDateTimeString(txtDateIn.Text, txtIn1.Text, "");
        string out1 = Utils.TimeToDateTimeString(txtDateOut.Text, txtOut1.Text, in1);

        string leavecode = ddlLeave.SelectedItem.Value.Trim();
        double leavehours = txtLvHours.Text.Trim() == "" ? 0 : (double.Parse(txtLvHours.Text.Trim()))  ;

        if (leavehours == 0) leavecode = "";
        if (leavecode == "") leavehours = 0.00;
        
        if (leavehours > 0 && txtRemarks2.Text.TrimEnd() == "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "rmk", "alert('Please enter reason of leave.');", true);
            return;
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _Url["EmpID"].Trim()));
        sqlparam.Add(new SqlParameter("@Date", lblDate.Text));

        sqlparam.Add(new SqlParameter("@WorkDistID", hfWorkDistID.Value));
        sqlparam.Add(new SqlParameter("@new_in1", Tools.DefaultNull(in1) ));
        sqlparam.Add(new SqlParameter("@new_out1", Tools.DefaultNull(out1) ));

        sqlparam.Add(new SqlParameter("@leaveCode", leavecode ));
        sqlparam.Add(new SqlParameter("@leaveHours", leavehours ));

        sqlparam.Add(new SqlParameter("@reason", txtRemarks2.Text ));
        sqlparam.Add(new SqlParameter("@completed", 1 ));

        sqlparam.Add(new SqlParameter("@Status", "For Approval"));
        sqlparam.Add(new SqlParameter("@Stage", "0"));
        sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));

        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistHdrUpdateTime", sqlparam.ToArray());

        ReprocessAttendance();

        ScriptManager.RegisterStartupScript(this, typeof(Page), "statusupdate", "statusupdate();window.close();", true);
    }

    private bool CheckLeaveBalance()
    {
        bool retval = true;
        if (ddlLeave.SelectedItem.Value != "")
        {
            List<SqlParameter> sqlparam = new List<SqlParameter>();
            sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
            sqlparam.Add(new SqlParameter("@EmployeeID", _Url["EmpID"].Trim()));
            sqlparam.Add(new SqlParameter("@Date", lblDate.Text));
            sqlparam.Add(new SqlParameter("@Code", ddlLeave.SelectedItem.Value));
            sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

            DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveValidationPerDay", sqlparam.ToArray());

            bool checkBalance = (bool)ds.Tables[0].Rows[0]["CheckBalance"];
            if (checkBalance)
            {
                double earned = Convert.ToDouble(ds.Tables[0].Rows[0]["Earned"]);
                double taken = Convert.ToDouble(ds.Tables[0].Rows[0]["Taken"]);

                if (double.Parse(txtLvHours.Text)/8.0 > (earned - taken))
                {
                    retval = false;
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "checkbalance", "alert('Not enough leave balance');", true);
                }
            }
        }
        return retval;
    }


    //reset to "For Approval"
    protected void btnRevise_Click(object sender, EventArgs e)
    {        
        using (TransactionScope ts = new TransactionScope())
        {
            WorkDistHdr hdr = new WorkDistHdr();
            if (hdr.LoadByPrimaryKey(Convert.ToInt32(hfWorkDistID.Value)))
            {
                hdr.Completed = false;
                hdr.Status = "For Approval";
                hdr.Stage = 0;
                hdr.LastUpdBy = _KioskSession.UID;
                hdr.LastUpdDate = DateTime.Now;
                hdr.Save();
            }


            BNS.TK.Entities.LeaveTrans leaveDtl = new BNS.TK.Entities.LeaveTrans();
            leaveDtl.Where.CompanyID.Value = _KioskSession.CompanyID;
            leaveDtl.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            leaveDtl.Where.EmployeeID.Value = _Url["EmpID"].Trim();
            leaveDtl.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            leaveDtl.Where.StartDate.Value = DateTime.Parse(lblDate.Text);
            leaveDtl.Query.Load("");

            //set leave status only if not already approved
            if (leaveDtl.RowCount == 1 && (leaveDtl.Status.TrimEnd().ToUpper() != "APPROVED"))
            {                
                leaveDtl.Status = "For Approval";
                leaveDtl.Stage = 0;
                leaveDtl.LastUpdBy = _KioskSession.UID;
                leaveDtl.LastUpdDate = DateTime.Now;
                leaveDtl.Save();
            }

            ts.Complete();
        }
        
        PopulateAllControls();
    }

    private bool IsFinalApprover()
    {
        EmployeeApprover appr = new EmployeeApprover();
        string sql = "SELECT Approver3, Approver4 FROM EmployeeApprover WHERE CompanyID = {0} AND EmployeeID = {1} ";

        object[] parameters = { _KioskSession.CompanyID, _Url["EmpID"].Trim() };
        appr.DynamicQuery(sql, parameters);
        if (appr.RowCount > 0)
        {
            return (appr.s_Approver3.TrimEnd() == "" && appr.s_Approver4.TrimEnd() == "");
        }
        return false;
    }


    protected void btnApprove_Click(object sender, EventArgs e)
    {
        //(approver 1 or 2) AND (approver 3 or approver 4)
        string newStatus = "Approved";
        int newStage = 2;
        if (ApproverNo() == "1" || ApproverNo() == "2")
        {
            newStage = 1;
            if (IsFinalApprover())
                newStatus = "Approved";
            else
                newStatus = "For Approval";
        }
        
        ApprovedDeny(sender, newStatus, newStage);

        lblStatus.Text = newStatus;
        ReprocessAttendance();
        
        if (newStatus == "For Approval")
            ScriptManager.RegisterStartupScript(this, typeof(Page), "statusApprove", "alert('Save completed, transaction queued to the next approver.');window.close();", true);
        else
            ScriptManager.RegisterStartupScript(this, typeof(Page), "statusApprove", "alert('Save completed, status is now Approved');window.close();", true);
            
    }


    //private bool xFinalApproverx()
    //{
    //    DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeInfo",
    //        new SqlParameter[] { 
    //            new SqlParameter("@DBname", _KioskSession.DB) 
    //            , new SqlParameter("@CompanyID", _KioskSession.CompanyID)
    //            , new SqlParameter("@EmployeeID", _Url["EmpID"].Trim()) 
    //        });

    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        DataRow emp = ds.Tables[0].Rows[0];

    //        string approver3 = (Convert.IsDBNull(emp["Approver3ID"])) ? "" : emp["Approver3ID"].ToString().TrimEnd();
    //        string approver4 = (Convert.IsDBNull(emp["Approver4ID"])) ? "" : emp["Approver4ID"].ToString().TrimEnd();

    //        return (approver3 == "" && approver4 == "");          
    //    }

    //    return false;
    //}

    
    private void ApprovedDeny(object sender, string newStatus, int newStage)
    {
        string id = hfWorkDistID.Value;

        string leavecode = ddlLeave.SelectedItem.Value.Trim();
        double leavehours = txtLvHours.Text.Trim() == "" ? 0 : double.Parse(txtLvHours.Text.Trim());

        if (leavehours == 0) leavecode = "";
        if (leavecode == "") leavehours = 0.00;

     
        //string in1 = txtIn1.Text; // Utils.TimeToDateTimeString(lblDate.Text, txtIn1.Text, "");
        //string out1 = txtOut1.Text; // Utils.TimeToDateTimeString(lblDate.Text, txtOut1.Text, in1);
        string in1 = Utils.TimeToDateTimeString(txtDateIn.Text, txtIn1.Text, "");
        string out1 = Utils.TimeToDateTimeString(txtDateOut.Text, txtOut1.Text, in1);

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _Url["EmpID"].Trim() ));
        sqlparam.Add(new SqlParameter("@Date", lblDate.Text ));
        sqlparam.Add(new SqlParameter("@new_in1", Tools.DefaultNull(in1) ));
        sqlparam.Add(new SqlParameter("@new_out1", Tools.DefaultNull(out1) ));
        sqlparam.Add(new SqlParameter("@loweTimeCreditDays", ConfigurationManager.AppSettings.Get("Lowe-TimeCreditDays")));
        sqlparam.Add(new SqlParameter("@leaveCode", leavecode));
        sqlparam.Add(new SqlParameter("@leaveHours", leavehours));

        sqlparam.Add(new SqlParameter("@WorkDistID", id));
        sqlparam.Add(new SqlParameter("@Status", newStatus));
        sqlparam.Add(new SqlParameter("@Stage", newStage));
        sqlparam.Add(new SqlParameter("@Remarks", txtRemarks.Text));
        sqlparam.Add(new SqlParameter("@DeclinedReason", txtDecReason.Text));
        sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistHdrUpdate", sqlparam.ToArray());
    }


    private void populateLeaveDropdown()
    {
        BNS.TK.Entities.LeaveCode leavecode = new BNS.TK.Entities.LeaveCode();
        string sql = "SELECT RTRIM(Code) AS Code, Description FROM LeaveCode WHERE CompanyID = {0} and code <> 'OFS' order by Description ";
        object[] parameters = { _KioskSession.CompanyID };
        leavecode.DynamicQuery(sql, parameters);

        //bind
        ddlLeave.DataSource = leavecode.DefaultView;
        ddlLeave.DataBind();
        ddlLeave.Items.Insert(0, "");
    }

     

    protected void ddlWorkForSubProj_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlWorkForSubProj = (DropDownList)sender;
        TextBox txtTask = ddlWorkForSubProj.Parent.FindControl("txtTask") as TextBox;

        string[] s = ddlWorkForSubProj.SelectedItem.Value.Split('~');
        txtTask.Text = s[1];
    }


    private void ReprocessAttendance()
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(DateTime.Parse(lblDate.Text), DateTime.Parse(lblDate.Text), employeeIdUrl);

        PopulateComputedValueControls();
        BindGrid();
    }
    
    protected void btnRecall_Click(object sender, EventArgs e)
    {
        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@WorkDistID", hfWorkDistID.Value));
        //sqlparam.Add(new SqlParameter("@Status", "For Approval" ));
        sqlparam.Add(new SqlParameter("@Status", "Declined"));
        sqlparam.Add(new SqlParameter("@Stage", "0" ));
        sqlparam.Add(new SqlParameter("@Remarks", txtRemarks.Text ));
        sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID ));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID ));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        //... save modified in and out
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkDistHdrRecall", sqlparam.ToArray());

        ScriptManager.RegisterStartupScript(this, typeof(Page), "statusRecall", "alert('Recall completed');window.close();", true);
    }


    private bool ValidateEditInputs()
    {
        DateRange dr = new DateRange();
        if (!dr.ValidatePeriod(txtNewDateIn.Text + " " + txtNewTimeIn.Text, txtNewDateOut.Text + " " + txtNewTimeOut.Text))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('" + dr.message + "');", true);
            return false;
        }
        if (txtNewTimeIn.Text.TrimEnd() == "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter new Time In');", true);
            return false;
        }
        if (txtNewTimeOut.Text.TrimEnd() == "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter new Time Out');", true);
            return false;
        }
        return true;
    }
 
    protected void btnPopSave_Click(object sender, EventArgs e)
    {
        if (!ValidateEditInputs()) return;
        
        mpe.Hide();
        try
        {
            string empid = _Url["EmpID"].Trim();
            string date = _Url["Date"];

            Attendance att = new Attendance();
            if (att.LoadByPrimaryKey(_KioskSession.CompanyID, empid, DateTime.Parse(date)))
            {
                DateTime newIn = DateTime.Parse(txtNewDateIn.Text + " " + txtNewTimeIn.Text);
                DateTime newOut = DateTime.Parse(txtNewDateOut.Text + " " + txtNewTimeOut.Text);

                att.In1 = newIn;
                att.Out1 = newOut;
                att.EarlyWork = chkEarlyLogin.Checked;            
                att.Save();


                //update work distrib
                WorkDistHdr wd = new WorkDistHdr();
                wd.Where.CompanyID.Value = _KioskSession.CompanyID;
                wd.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
                wd.Where.EmployeeID.Value = empid;
                wd.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
                wd.Where.Date1.Value = DateTime.Parse(date);
                wd.Query.Load("");
                if (wd.RowCount == 1)
                {
                    wd.New_in1 = newIn;
                    wd.New_out1 = newOut;
                    wd.Reason = txtNewRemarks.Text;
                    wd.Save();
                }

                //upate values
                txtDateIn.Text = String.Format("{0:MM/dd/yyyy}", newIn);
                txtDateOut.Text = String.Format("{0:MM/dd/yyyy}", newOut);
                txtIn1.Text = String.Format("{0:hh:mm tt}", newIn);
                txtOut1.Text = String.Format("{0:hh:mm tt}", newOut);

                //reprocess
                ReprocessAttendance();

                txtRemarks.Text = txtNewRemarks.Text;
                if (chkEarlyLogin.Checked)
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsmsg", "alert('You applied for early login. Adjustments will be reflected in your timesheet upon approval.');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('" + ex.Message + "');", true);
        }
    }

    protected void FileOffset(DateTime attDate, decimal totalLateUT_hrs, int totalLateUT_mins)
    {
        try
        {            
            //create leave detail
            OffsetTrans ofstran = new OffsetTrans();
            ofstran.AddNew();
            ofstran.CompanyID = _KioskSession.CompanyID;
            ofstran.EmployeeID = _Url["EmpID"].Trim();
            ofstran.DateFiled = DateTime.Now;
            ofstran.DateUsed = attDate;
            ofstran.OffsetMins = totalLateUT_mins;
            ofstran.Reason = txtNewRemarks2.Text;
            ofstran.Status = "Approved";
            ofstran.CreatedBy = _KioskSession.EmployeeID;
            ofstran.CreatedDate = DateTime.Now;
            ofstran.LastUpdBy = _KioskSession.EmployeeID;
            ofstran.LastUpdDate = DateTime.Now;
            ofstran.Save();

            WorkDistDtl.CreateUpdateWorkDistOffset(_KioskSession.CompanyID, _Url["EmpID"].Trim(), attDate, Convert.ToDecimal(totalLateUT_mins / 60.0M));            
             
            //reprocess
            ReprocessAttendance();

            //refresh computed values
            //UpdateValues(_Url["EmpID"].Trim(), attDate.ToShortDateString());
        }
        catch
        {
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('" + ex.Message + "');", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('Error occured during save.');", true);
        }
    }

    /// <summary>
    /// returns the Excess hrs of the prev working day
    /// todo: hardcoded sat/sunday as restday
    /// </summary>
    /// <param name="attDate"></param>
    /// <returns></returns>
    //private decimal PrevDayExcesshrs()
    //{
    //    DateTime prevDate = DateTime.Parse(_Url["Date"]).AddDays(-1);
    //    if (prevDate.DayOfWeek == DayOfWeek.Saturday || prevDate.DayOfWeek == DayOfWeek.Sunday)
    //        prevDate = prevDate.AddDays(-1);
    //    if (prevDate.DayOfWeek == DayOfWeek.Saturday || prevDate.DayOfWeek == DayOfWeek.Sunday)
    //        prevDate = prevDate.AddDays(-1);

        
    //    string empid = _Url["EmpID"].Trim();
    //    Attendance att = new Attendance();
    //    if (att.LoadByPrimaryKey(_KioskSession.CompanyID, empid, prevDate))
    //    {
    //        return att.Excesshrs;
    //    }

    //    return 0;
    //}

    protected void lbFileOffset_Click(object sender, EventArgs e)
    {
        //if (PrevDayOThrs() < 2)
        //{
        //    ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('Must have at least 2 hours overtime (previous day) to file an offset.');", true);
        //    return;
        //}
    }
    protected void btnOffsetSave_Click(object sender, EventArgs e)
    {
        decimal tot_hrs;
        int tot_mins;

        if (txtEditOfstHrs.Text.TrimEnd() == "") return;
        if (!Decimal.TryParse(txtEditOfstHrs.Text, out tot_hrs)) return;
        if (!int.TryParse(hfOfsMins.Value, out tot_mins)) return;

        mpe.Hide();

        if (OffsetBalanceMins(DateTime.Parse(_Url["Date"])) < tot_mins)
            ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsbalance", "alert('Not enough offset balance');", true);
        else
        {
            FileOffset(DateTime.Parse(_Url["Date"]), tot_hrs, tot_mins);            
        }
    }

    private void CancelOffset(DateTime attDate)
    {
        try
        {
            OffsetTrans ofs = new OffsetTrans();
            ofs.Where.CompanyID.Value = _KioskSession.CompanyID;
            ofs.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            ofs.Where.EmployeeID.Value = _Url["EmpID"].Trim();
            ofs.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
            ofs.Where.DateUsed.Value = attDate;
            ofs.Query.Load("");

            if (ofs.RowCount == 1)
            {
                ofs.MarkAsDeleted();
                ofs.Save();

                WorkDistDtl.DeleteWorkDistOffset(_KioskSession.CompanyID, _Url["EmpID"].Trim(), attDate);            
            }

            //reprocess
            ReprocessAttendance();

        }
        catch
        {
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('" + ex.Message + "');", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('Error occured during save.');", true);
        }
    }

    #region OffsetBalance
    private decimal OffsetBalanceMins(DateTime attDate)
    {
        return OffsetEarn.OffsetBalance(_KioskSession.CompanyID, _Url["EmpID"].Trim(), attDate);
    }
    #endregion


    private bool BirthdayLeaveAllowed()
    {
        Employee emp = new Employee();
        emp.Query.AddResultColumn(Employee.ColumnNames.BirthDate);
        emp.Where.CompanyID.Value = _KioskSession.CompanyID;
        emp.Query.AddConjunction(MMB.DataObject.WhereParameter.Conj.AND_);
        emp.Where.EmployeeID.Value = _KioskSession.EmployeeID;
        emp.Query.Load("");
        return (emp.RowCount == 1 && emp.s_BirthDate != "" && emp.BirthDate.Month == DateTime.Parse(_Url["Date"]).Month);
    }

    protected void lbCancelOfs_Click(object sender, EventArgs e)
    {
        CancelOffset(DateTime.Parse(_Url["Date"]));
    }
    protected void ddlLeave_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLeave.SelectedValue.TrimEnd() == "SL")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertsl", "if (!confirm('You must submit a medical certificate to Human Resources for sick leave applications lasting for 3 consecutive days or more. Management reserves the right to disapprove/cancel sick leave applications for 3 days or more without proper medical documentation as per policy.')) ResetLeave();", true);
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertsl", "alert('You must submit a medical certificate to Human Resources for sick leave');", true);
        }
    }

    protected void btnDeclineWD_Click(object sender, EventArgs e)
    {
        ApprovedDeny(sender, "Declined", 0);

        lblStatus.Text = "Declined";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "statusDecline", "alert('Save completed, status is now declined');window.close();", true);
    }
}