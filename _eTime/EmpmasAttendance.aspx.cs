using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;
using BNS.TK.Entities;

public partial class EmpmasAttendance : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            //SecureUrl urlLeave = new SecureUrl(string.Format(@"popupApprover.aspx?hfID={0}&lbID={1}", hfApproverLeave.ClientID, txtApproverLeave.ClientID));
            SecureUrl urlOT = new SecureUrl(string.Format(@"popupApprover.aspx?hfID={0}&lbID={1}&compID={2}", hfApproverOT.ClientID, txtApproverOT.ClientID, hfApprvrOTCompID.ClientID));
            SecureUrl urlHistory = new SecureUrl(string.Format(@"EmpmasAttendanceAudit.aspx?EmployeeID={0}&EmployeeName={1}", _Url["EmpID"], _Url["EmpName"]));
            SecureUrl urlCredits = new SecureUrl(string.Format("LeaveCreditsEntry.aspx?EmpID={0}&EmpName={1}", _Url["EmpID"], _Url["EmpName"]));

            //btnApproverLeave.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlLeave) );
            btnApproverOT.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlOT));
            lnkHistory.Attributes.Add("onclick", string.Format("openHistory('{0}');return false;", urlHistory));
            lnkLeaveCredits.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlCredits));

            populateScreenDefault();
            hfEmpID.Value = _Url["EmpID"];
            
            if (hfEmpID.Value != "")
            {                
                bindData();
                txtEmployeeID.Enabled = false;
                lblFullname.Text = _Url["EmpName"];
            }
            else
            {
                lblFullname.Text = "Add New Employee";
                txtDateEmployed.Text = DateTime.Today.ToString(_KioskSession.DateFormat);
            }
            updateurl();
        }
    }

    private void populateScreenDefault()
    {
        //... populate Shift dropdown
        DataSet dsShift = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT ShiftCode, Description FROM " + _KioskSession.DB + ".dbo.Shift WHERE CompanyID = @CompanyID",
                        new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlShift.DataSource = dsShift.Tables[0];
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, "");
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        btnSection.Attributes.Remove("onclick");
        btnSection.Attributes.Add("onclick", "openwindow(); return false;");

        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    private void bindData()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, 
                MainDB + ".dbo.usa_EmployeeLoadByPrimaryKey",
                new SqlParameter[] { 
                        new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                        new SqlParameter("@EmployeeID", hfEmpID.Value), 
                        new SqlParameter("@DBName", _KioskSession.DB) });

        if (ds.Tables[0].Rows.Count == 1)
        {
            DataRow dr = ds.Tables[0].Rows[0];

            txtEmployeeID.Text = dr["EmployeeID"].ToString();
            txtLastName.Text = dr["LastName"].ToString();
            txtFirstName.Text = dr["FirstName"].ToString();
            txtMiddleName.Text = dr["MiddleName"].ToString();
            ddlGender.Text = dr["Gender"].ToString();
            txtAddress1.Text = dr["Address1"].ToString();
            txtAddress2.Text = dr["Address2"].ToString();            
            txtBirthDate.Text = Tools.ShortDate(dr["BirthDate"], _KioskSession.DateFormat);

            hfLevelDisp.Value =
                dr["Level1Disp"].ToString() + "\r" +
                dr["Level2Disp"].ToString() + "\r" +
                dr["Level3Disp"].ToString() + "\r" +
                dr["Level4Disp"].ToString();
            
            //... brute force correction, the space is need if the length is zero....
            if (dr["Level1Disp"].ToString().Length == 0)
                txtDepartment.Text = " " + hfLevelDisp.Value;
            else
                txtDepartment.Text = hfLevelDisp.Value;

            ddlEmploymentType.Text = dr["EmploymentType"].ToString();
            ddlRank.Text = dr["Rank"].ToString();
            txtPosition.Text = dr["Position"].ToString();
            txtDateEmployed.Text = Tools.ShortDate(dr["DateEmployed"], _KioskSession.DateFormat);
            txtDateRegularized.Text = Tools.ShortDate(dr["DateRegularized"], _KioskSession.DateFormat);
            txtDateTerminated.Text = Tools.ShortDate(dr["DateTerminated"], _KioskSession.DateFormat);

            txtSwipeID.Text = dr["SwipeID"].ToString();
            hfApproverOT.Value = dr["ApproverOT"].ToString();
            hfApprvrOTCompID.Value = dr["ApprvrOTCompID"].ToString();
            //hfApproverLeave.Value = dr["ApproverLeave"].ToString();
            //txtApproverLeave.Text = dr["ApproverLeaveName"].ToString();
            txtApproverOT.Text = dr["ApproverOTName"].ToString();

            ddlShift.SelectedValue = dr["ShiftCode"].ToString();
            hfLevels.Value = dr["Levels"].ToString();

            string restday = dr["RestDay"].ToString();
            cblRestDay1.Items[0].Selected = restday.Contains("6"); // Saturday
            cblRestDay1.Items[1].Selected = restday.Contains("7"); // Sunday
            cblRestDay2.Items[0].Selected = restday.Contains("1"); // Monday
            cblRestDay2.Items[1].Selected = restday.Contains("2"); // Tuesday
            cblRestDay2.Items[2].Selected = restday.Contains("3"); // Wednessday
            cblRestDay2.Items[3].Selected = restday.Contains("4"); // Thursday
            cblRestDay2.Items[4].Selected = restday.Contains("5"); // Friday

            chkTardyExempt.Checked = (bool)dr["TardyExempt"];
            chkUTExempt.Checked = (bool)dr["UTExempt"];
            chkOTExempt.Checked = (bool)dr["OTExempt"];
            chkNonswiper.Checked = (bool)dr["Nonswiper"];
        }
    }

    private void saveData()
    {
        bool IsAddMode = (hfEmpID.Value == "");

        // check for referenctial integrity
        if (IsAddMode)
        {
            List<SqlParameter> sqlparameters = new List<SqlParameter>();
            sqlparameters.Add(new SqlParameter("@DBName", _KioskSession.DB));
            sqlparameters.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
            sqlparameters.Add(new SqlParameter("@EmployeeID", txtEmployeeID.Text));
            sqlparameters.Add(new SqlParameter("@LastName", txtLastName.Text));
            sqlparameters.Add(new SqlParameter("@FirstName", txtFirstName.Text));
            sqlparameters.Add(new SqlParameter("@MiddleName", txtMiddleName.Text));
            sqlparameters.Add(new SqlParameter("@BirthDate", Tools.DefaultNull(txtBirthDate.Text)));

            string errormessage = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_EmployeeValidation", sqlparameters.ToArray()).Tables[0].Rows[0][0].ToString();
            if (errormessage.Length > 0)
            {
                if (!ClientScript.IsStartupScriptRegistered("checkemployee"))
                {
                    ClientScript.RegisterStartupScript(typeof(Page), "checkemployee", "<script language=JavaScript>alert('" + errormessage + "');</script>");
                    return;
                }
            }
            //ClientScript.RegisterStartupScript(typeof(Page), "hidewindow", "<script language=JavaScript>parent.sectionWindow.hide();</script>");
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@DBName", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));

        sqlparam.Add(new SqlParameter("@EmployeeID", txtEmployeeID.Text));
        sqlparam.Add(new SqlParameter("@LastName", txtLastName.Text));
        sqlparam.Add(new SqlParameter("@FirstName", txtFirstName.Text));
        sqlparam.Add(new SqlParameter("@MiddleName", txtMiddleName.Text));
        sqlparam.Add(new SqlParameter("@Gender", ddlGender.SelectedValue));
        sqlparam.Add(new SqlParameter("@Address1", txtAddress1.Text));
        sqlparam.Add(new SqlParameter("@Address2", txtAddress2.Text));
        sqlparam.Add(new SqlParameter("@BirthDate", Tools.DefaultNull(txtBirthDate.Text)));

        sqlparam.Add(new SqlParameter("@EmploymentType", ddlEmploymentType.SelectedValue));
        sqlparam.Add(new SqlParameter("@Rank", ddlRank.SelectedValue));
        sqlparam.Add(new SqlParameter("@Position", txtPosition.Text));
        sqlparam.Add(new SqlParameter("@DateEmployed", Tools.DefaultNull((txtDateEmployed.Text))));
        sqlparam.Add(new SqlParameter("@DateRegularized", Tools.DefaultNull((txtDateRegularized.Text))));
        sqlparam.Add(new SqlParameter("@DateTerminated", Tools.DefaultNull((txtDateTerminated.Text))));

        sqlparam.Add(new SqlParameter("@TardyExempt", chkTardyExempt.Checked ));
        sqlparam.Add(new SqlParameter("@UTExempt", chkUTExempt.Checked  ));
        sqlparam.Add(new SqlParameter("@OTExempt", chkOTExempt.Checked ));
        sqlparam.Add(new SqlParameter("@NonSwiper", chkNonswiper.Checked ));

        sqlparam.Add(new SqlParameter("@ShiftCode", ddlShift.SelectedItem.Value ));
        sqlparam.Add(new SqlParameter("@RestDay", Get_Restday() ));

        sqlparam.Add(new SqlParameter("@SwipeID", txtSwipeID.Text  ));
        //sqlparam.Add(new SqlParameter("@ApproverLeave", hfApproverLeave.Value ));
        sqlparam.Add(new SqlParameter("@ApproverLeave", hfApproverOT.Value));
        sqlparam.Add(new SqlParameter("@ApproverOT", hfApproverOT.Value ));

        sqlparam.Add(new SqlParameter("@ApprvrOTCompID", hfApprvrOTCompID.Value));
        sqlparam.Add(new SqlParameter("@ApprvrLVCompID", hfApprvrOTCompID.Value));

        string[] LevelIDs = hfLevels.Value.Split('~');
        if (LevelIDs.Length != 4)
            LevelIDs = new string[4] { "", "", "", "" };

        string[] LevelDisp = hfLevelDisp.Value.Replace("\n","").Split('\r');
        if (LevelDisp.Length != 4)
            LevelDisp = new string[4] { "", "", "", "" };

        sqlparam.Add(new SqlParameter("@Level1", LevelIDs[0]));
        sqlparam.Add(new SqlParameter("@Level2", LevelIDs[1]));
        sqlparam.Add(new SqlParameter("@Level3", LevelIDs[2]));
        sqlparam.Add(new SqlParameter("@Level4", LevelIDs[3]));

        sqlparam.Add(new SqlParameter("@Level1Disp", LevelDisp[0] ));
        sqlparam.Add(new SqlParameter("@Level2Disp", LevelDisp[1]));
        sqlparam.Add(new SqlParameter("@Level3Disp", LevelDisp[2]));
        sqlparam.Add(new SqlParameter("@Level4Disp", LevelDisp[3]));

        //sqlparam.Add(new SqlParameter("@SecID", DBNull.Value));
        //sqlparam.Add(new SqlParameter("@Division", txtDivision.Text ) );
        //sqlparam.Add(new SqlParameter("@Department", txtDepartment.Text ) );
        //sqlparam.Add(new SqlParameter("@Section", txtSection.Text ) );
        //sqlparam.Add(new SqlParameter("@CostCenter", txtCostCenter.Text));

        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (IsAddMode)
        {
            //...insert to audittrail
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_EmployeeAudit", sqlparam.ToArray()); 

            //insert new
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_EmployeeInsert", sqlparam.ToArray());

            //add 5 days SL
            //CreateSLcreditForNewEmployee();

            txtEmployeeID.Enabled = false;
            hfEmpID.Value = txtEmployeeID.Text;
        }
        else
        {
            AuditChange checker = CheckForChanges();

            if (checker.HasChanges)
            {
                //... update data
                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_EmployeeUpdate", sqlparam.ToArray()); 

                //... add parameter for audittrail
                sqlparam.Add(new SqlParameter("@Changes", checker.Changes ));

                //...insert to audittrail
                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_EmployeeAudit", sqlparam.ToArray()); 
            }
        }

        /*
        //... need to update txtDepartment because the control is disabled        
        //... brute for correction, the space is need if the length is zero....
        if (LevelDisp[0].Length == 0)
            txtDepartment.Text = " " + hfLevelDisp.Value;
        else
            txtDepartment.Text = hfLevelDisp.Value;
         */

        bindData();
    }

    #region for audit trail
    private AuditChange CheckForChanges()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                MainDB + ".dbo.usa_EmployeeLoadByPrimaryKey",
                new SqlParameter[] { 
                        new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                        new SqlParameter("@EmployeeID", hfEmpID.Value), 
                        new SqlParameter("@DBName", _KioskSession.DB) });

        DataRow dr = ds.Tables[0].Rows[0];

        AuditChange checker = new AuditChange();
        checker.AddCompare("LastName", txtLastName.Text.Trim(), dr["LastName"].ToString().Trim());
        checker.AddCompare("FirstName", txtFirstName.Text.Trim(), dr["FirstName"].ToString().Trim());
        checker.AddCompare("MiddleName", txtMiddleName.Text.Trim(), dr["MiddleName"].ToString().Trim());
        checker.AddCompare("Gender", ddlGender.Text, dr["Gender"].ToString());
        checker.AddCompare("Address1", txtAddress1.Text.Trim(), dr["Address1"].ToString());
        checker.AddCompare("Address2", txtAddress2.Text.Trim(), dr["Address2"].ToString());
        checker.AddCompare("BirthDate", txtBirthDate.Text.Trim(), Tools.ShortDate(dr["BirthDate"], _KioskSession.DateFormat));
        checker.AddCompare("EmploymentType", ddlEmploymentType.Text, dr["EmploymentType"].ToString() );
        checker.AddCompare("Rank", ddlRank.Text, dr["Rank"].ToString());
        checker.AddCompare("Position", txtPosition.Text.Trim(), dr["Position"].ToString().Trim() );
        checker.AddCompare("DateEmployed", txtDateEmployed.Text, Tools.ShortDate(dr["DateEmployed"], _KioskSession.DateFormat));
        checker.AddCompare("DateRegularized", txtDateRegularized.Text, Tools.ShortDate(dr["DateRegularized"], _KioskSession.DateFormat) );
        checker.AddCompare("DateTerminated", txtDateTerminated.Text, Tools.ShortDate(dr["DateTerminated"], _KioskSession.DateFormat));
        checker.AddCompare("TardyExempt", chkTardyExempt.Checked, (bool)dr["TardyExempt"]);
        checker.AddCompare("UTExempt", chkUTExempt.Checked, (bool)dr["UTExempt"]);
        checker.AddCompare("OTExempt", chkOTExempt.Checked, (bool)dr["OTExempt"]);
        checker.AddCompare("NonSwiper", chkNonswiper.Checked, (bool)dr["Nonswiper"]);
        checker.AddCompare("SwipeID", txtSwipeID.Text.Trim(), dr["SwipeID"].ToString());
        checker.AddCompare("ApproverOT", hfApproverOT.Value, dr["ApproverOT"].ToString() );
        //checker.AddCompare("ApproverLeave", hfApproverLeave.Value, dr["ApproverLeave"].ToString() );
        checker.AddCompare("ShiftCode", ddlShift.SelectedValue, dr["ShiftCode"].ToString());
        checker.AddCompare("RestDay", Get_Restday(), dr["RestDay"].ToString());

        string[] LevelIDs = hfLevels.Value.Split('~');
        if (LevelIDs.Length != 4)
            LevelIDs = new string[4] { "", "", "", "" };

        string[] LevelDisp = hfLevelDisp.Value.Replace("\n", "").Split('\r');
        if (LevelDisp.Length != 4)
            LevelDisp = new string[4] { "", "", "", "" };

        string[] LvlIDServer = dr["Levels"].ToString().Split('~');

        checker.AddCompare("Level1Disp", LevelDisp[0], dr["Level1Disp"].ToString() );
        checker.AddCompare("Level2Disp", LevelDisp[1], dr["Level2Disp"].ToString() );
        checker.AddCompare("Level3Disp", LevelDisp[2], dr["Level3Disp"].ToString() );
        checker.AddCompare("Level4Disp", LevelDisp[3], dr["Level4Disp"].ToString() );

        checker.AddCompare("Level1", LevelIDs[0], LvlIDServer[0]);
        checker.AddCompare("Level2", LevelIDs[1], LvlIDServer[1]);
        checker.AddCompare("Level3", LevelIDs[2], LvlIDServer[2]);
        checker.AddCompare("Level4", LevelIDs[3], LvlIDServer[3]);

        return checker;
    }
    #endregion

    private void CreateSLcreditForNewEmployee()
    {
        LeaveEarn lvearn = new LeaveEarn();
        lvearn.AddNew();
        lvearn.CompanyID = _KioskSession.CompanyID;
        lvearn.TranDate = DateTime.Today;
        lvearn.LeaveCode = "SL";
        lvearn.AppYear = DateTime.Today.Year;
        lvearn.AppMonth = 0;
        lvearn.Remarks = "SL credit for new employee";
        lvearn.LeaveEarnType = "";
        lvearn.SystemCreated = true;
        lvearn.LeaveEarnType = "Earning";
        lvearn.CreatedBy = _KioskSession.EmployeeID;
        lvearn.CreatedDate = DateTime.Now;        
        lvearn.Save();

        LeaveEarnDtl dtl = new LeaveEarnDtl();
        dtl.AddNew();
        dtl.LeaveEarnID = (int) lvearn.LeaveEarnID;
        dtl.CompanyID = _KioskSession.CompanyID;
        dtl.EmployeeID = txtEmployeeID.Text;
        dtl.Leavecode = "SL";
        dtl.Earned = 5;
        dtl.Save();
    }

    private string Get_Restday()
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 2; i++)
            if (cblRestDay1.Items[i].Selected)
                sb.Append(cblRestDay1.Items[i].Value);

        for (int i = 0; i < 5; i++)
            if (cblRestDay2.Items[i].Selected)
                sb.Append(cblRestDay2.Items[i].Value);

        return sb.ToString();
    }

    protected void lbSave_Click(object sender, EventArgs e)
    {
        saveData();
    }
    protected void imgSave_Click(object sender, ImageClickEventArgs e)
    {
        saveData();
    }

    public class AuditChange
    {
        StringBuilder sbChanges;

        bool _HasChanges;
        public bool HasChanges { get { return _HasChanges; } }
        public string Changes { get { return sbChanges.ToString(); } }

        public AuditChange()
        {
            sbChanges = new StringBuilder();
            _HasChanges = false;
        }

        public void AddCompare(string rowdefinition, string value1, string value2)
        {
            if (value1 != value2)
            {
                _HasChanges = true;
                sbChanges.Append(rowdefinition + ",");
            }
        }

        public void AddCompare(string rowdefinition, bool value1, bool value2)
        {
            if (value1 != value2)
            {
                _HasChanges = true;
                sbChanges.Append(rowdefinition + ",");
            }
        }
    }

}


