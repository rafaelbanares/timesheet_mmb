using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using BNS.TK.Entities;

public partial class eTimeHome : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        lblMessage.Text = "";
        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            populateScreenDefault();
            //populate_Dropdown();
            ShowLockStatus();
        }
    }

    private void populateScreenDefault()
    {
        DataRow drSummary = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_eTimeHomeSummary",
                        new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@StartDate", txtDateStart.Text) ,
                            new SqlParameter("@EndDate", txtDateEnd.Text) 
                        }
                    ).Tables[0].Rows[0];

        //... for approval
        int unapproveDTR = (int)drSummary["UnApproveDTR"];
        int unapproveleave = (int)drSummary["UnApproveLeave"];
        int unapproveot = (int)drSummary["UnApproveOT"];

        //... force disable leave approval for lowe
        unapproveleave = 0;

        //... unprocess
        int unprocess = (int)drSummary["UnProcessed"];

        hlDTR.Text = string.Format("({0}) Work distribution waiting for approval", unapproveDTR);
        hlUnapproveLeave.Text = string.Format("({0}) Filed leave(s) waiting for approval", unapproveleave);
        hlUnapproveOT.Text = string.Format("({0}) Filed overtime waiting for approval", unapproveot);

        hlDTR.NavigateUrl = unapproveDTR < 1 ? "" : "WorkDistribApprList.aspx?approverno=9";
        hlUnapproveLeave.NavigateUrl = unapproveleave < 1 ? "" : "AdminLeaveApproval.aspx"; //"AdminLeave.aspx";
        hlUnapproveOT.NavigateUrl = unapproveot < 1 ? "" : "AdminOTApproval.aspx"; //"AdminOT.aspx";

        
        //... clear status message
        lblMessage.Text = "";
        txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
        txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
    }

    private void populate_Dropdown()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, 
            "SELECT rtrim(CompanyID) as CompanyID, CompanyName FROM " + _KioskSession.DB + ".dbo.Company ORDER BY CompanyName ");
        ddlCompany.DataSource = ds;
        ddlCompany.DataBind();

        ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(_KioskSession.CompanyID));
    }
        
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        UpdateUserDefaults();
        lblMessage.Text = "Default dates successfully updated";
    }

    protected void btnCompany_Click(object sender, EventArgs e)
    {
        UpdateUserDefaults();
        lblMessage.Text = "Default company successfully updated";
    }

    private void UpdateUserDefaults()
    {
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_UserDefaultsUpdate",
                new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@ID", _KioskSession.UID),
                            new SqlParameter("@AttFrom", txtDateStart.Text),
                            new SqlParameter("@AttTo", txtDateEnd.Text),
                            new SqlParameter("@AttCompanyID", ddlCompany.SelectedItem.Value) 
                        });

        _KioskSession.StartDate = DateTime.Parse(txtDateStart.Text);
        _KioskSession.EndDate = DateTime.Parse(txtDateEnd.Text);
        _KioskSession.CompanyID = ddlCompany.SelectedItem.Value.Trim();
        _KioskSession.CompanyName = ddlCompany.SelectedItem.Text.Trim();
        base.SetUserSession(_KioskSession);

        populateScreenDefault();
    }

    private void ShowLockStatus()
    {
        RunData rd = new RunData();
        rd.LoadAll();
        lblLockStat.Text = (rd.KioskLocked) ? "Locked" : "Available";
        btnLockUnlock.Text = (rd.KioskLocked) ? "Unlock Khronos" : "Lock Khronos";
    }

    private void ToggleLockStatus()
    {
        RunData rd = new RunData();
        rd.LoadAll();
        //toggle        
        rd.KioskLocked = (!rd.KioskLocked);        
        rd.Save();

        //refresh display
        ShowLockStatus();
    }

    protected void btnLockUnlock_Click(object sender, EventArgs e)
    {
        ToggleLockStatus();
    }
}
