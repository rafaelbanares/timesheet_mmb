<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="Holiday.aspx.cs" Inherits="Holiday" Title="Bisneeds - Timekeeping - Holiday" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Holiday Definitions"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Holiday Definition" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Holiday Definition</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" Width="600px" DataKeyNames="ID">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>

                    <asp:TemplateField HeaderText="Date">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtDate" runat="server" Width="72px" Text='<%# Bind("Date") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtDate" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="txtDate"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleDate" runat="server" TargetControlID="txtDate" PopupButtonID="ibtxtDate"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Date"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Description") %>' MaxLength="30" Width="190px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Legal">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkLegal" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgLegal" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center"/>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Half day">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkHalfday" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgHalfday" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center"/>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

