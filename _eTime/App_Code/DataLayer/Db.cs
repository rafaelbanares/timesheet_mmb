using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;

namespace BNS.Attendance.DataObjects
{
    public static class Db
    {
        private static readonly string _masterDB = ConfigurationManager.AppSettings.Get("MasterDB");
        private static readonly string _conn = ConfigurationManager.AppSettings.Get("Connection");
        private static readonly string _applicationCode = "PAYROLL";
        private static readonly string _secu_schema = ".sec.";

        public static string MasterDB
        {
            get { return _masterDB; }
        }

        public static string ConnectionString
        {
            get { return _conn; }
        }

        public static string SecuSchema
        {
            get { return _secu_schema; }
        }
        public static string MasterDbSchema
        {
            get { return _masterDB + _secu_schema; }
        }


        public static string GetClientDB(int clientID)
        {
            string clientDB = (string)SqlHelper.ExecuteScalar(_conn, CommandType.StoredProcedure, Db.MasterDB + ".dbo.usa_GetClientDB",
                                    new SqlParameter[] { new SqlParameter("@clientID", clientID), new SqlParameter("@appid", _applicationCode) });
            return clientDB;
        }

    }
}
