using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Net.Mail;
using BNS.Framework.Log;


/// <summary>
/// This implementation of Global brings the model back to VS 2003 global.asax.
/// </summary>
public class Global : HttpApplication
{
    /// <summary>
    /// Event handler for application start event. Initializes logging.
    /// </summary>
    protected void Application_Start(Object sender, EventArgs e)
    {
        // Initialize logging facility
        InitializeLogger();
    }

    /// <summary>
    /// Event handler for session start event. Initializes shopping cart.
    /// </summary>
    protected void Session_Start(Object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Initializes logging facility with severity level and observer(s).
    /// Private helper method.
    /// </summary>
    private void InitializeLogger()
    {
        // Read and assign application wide logging severity
        string severity = ConfigurationManager.AppSettings.Get("LogSeverity");
        SingletonLogger.Instance.Severity = (LogSeverity)Enum.Parse(typeof(LogSeverity), severity, true);

        // Send log messages to database (observer pattern)
        ILog log = new ObserverLogToDatabase();
        SingletonLogger.Instance.Attach(log);

        // Send log messages to email (observer pattern)
        string from = "notification@yourcompany.com";
        string to = "webmaster@yourcompany.com";
        string subject = "Webmaster: please review";
        SmtpClient smtpClient = new SmtpClient("mail.yourcompany.com");

        log = new ObserverLogToEmail(from, to, subject, smtpClient);
        SingletonLogger.Instance.Attach(log);

        // Send log messages to a file
        log = new ObserverLogToFile(@"C:\Temp\eTimeLog.txt");
        SingletonLogger.Instance.Attach(log);

        // Send log message to event log
        //log = new ObserverLogToEventlog();
        //SingletonLogger.Instance.Attach(log);
    }    
}
