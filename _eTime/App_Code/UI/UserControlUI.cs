using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace Bns.AttendanceUI
{
    /// <summary>
    /// Summary description for UserControlUI
    /// </summary>
    public class UserControlUI : System.Web.UI.UserControl
    {
        protected string _ConnectionString = ConfigurationManager.AppSettings.Get("Connection");
        protected static readonly String MainDB = ConfigurationManager.AppSettings.Get("MasterDB");
        protected Kiosk _KioskSession;
        

        public UserControlUI()
        {

        }
            

        public void GetUserSession()
        {
            _KioskSession = (Kiosk)Session["kiosk"];
        }
    }



}
