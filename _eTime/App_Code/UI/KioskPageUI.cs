using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttUtils;
using System.Text.RegularExpressions;

namespace Bns.AttendanceUI
{
    /// <summary>
    /// Summary description for KioskPageUI
    /// </summary>
    public class KioskPageUI : System.Web.UI.Page
    {
        protected Kiosk _KioskSession;
        protected SecureUrl _Url;
        protected bool _OverwriteRender = true;
        protected string _ConnectionString = ConfigurationManager.AppSettings.Get("Connection");
        protected static readonly String MainDB = ConfigurationManager.AppSettings.Get("MasterDB"); 

        private static readonly Regex REGEX_BETWEEN_TAGS = new Regex(@">\s+<", RegexOptions.Compiled);
        private static readonly Regex REGEX_LINE_BREAKS = new Regex(@"\n\s+", RegexOptions.Compiled);


        /* disabled page compression, advatange: it will redirect to the actual line number of the error.
        /// <summary>
        /// Initializes the <see cref="T:System.Web.UI.HtmlTextWriter"></see> object and calls on the child 
        /// controls of the <see cref="T:System.Web.UI.Page"></see> to render.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"></see> that receives the page content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (_OverwriteRender)
            {
                if (!IsPostBack)
                {
                    using (HtmlTextWriter htmlwriter = new HtmlTextWriter(new System.IO.StringWriter()))
                    {
                        base.Render(htmlwriter);
                        string html = htmlwriter.InnerWriter.ToString();

                        html = REGEX_BETWEEN_TAGS.Replace(html, "> <");
                        html = REGEX_LINE_BREAKS.Replace(html, string.Empty);

                        writer.Write(html.Trim());
                    }
                }
                else
                {
                    base.Render(writer);
                }
            }
        }
        */

        /* disabled temporarily saving session to viewstate
        protected override void SavePageStateToPersistenceMedium(object state)
        {
            //base.SavePageStateToPersistenceMedium(state);
            Session["viewstate" + Request.Url.AbsolutePath] = state;
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            //return base.LoadPageStateFromPersistenceMedium(); 
            return Session["viewstate" + Request.Url.AbsolutePath];
        }
        */

        protected void OverwriteRender(bool overwrite)
        {
            _OverwriteRender = overwrite;
        }

        protected void GetUserSession()
        {
            object session = (object)Session["kiosk"];

            if (session == null)
                Response.Redirect("LoginEmployee.aspx");

            _KioskSession = (Kiosk)session;
            //_KioskSession = (Kiosk)Session["kiosk"];
            
        }

        protected void SetUserSession(Kiosk kiosk)
        {
            Session["kiosk"] = kiosk;
        }

        protected virtual DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
        {
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    else
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                }
                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            _Url = new SecureUrl(Request.Url.PathAndQuery);
        }

        protected string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }
        protected string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }
    }


    public struct Kiosk
    {
        private const string USER_SESSION_ID = "BNS_USER_SESSION";
        string _CompanyID;
        string _CompanyName;
        string _DB;
        string _UID;

        string _EmployeeID;
        string _LeaveApproverID;
        string _OTApproverID;

        string _EmployeeName;
        string _LeaveApproverName;
        string _OTApproverName;
        string _Levels;
        string _Position;

        string _Level1Data;
        string _Level2Data;
        string _Level3Data;
        string _Level4Data;

        bool _WorkForEnabled;
        string _WorkFor;
        string _WorkForSub;

        DateTime _StartDate;
        DateTime _EndDate;

        bool _IsTimeIn;
            
        public string UID { get { return _UID; } set { _UID = value; } }
        public string CompanyID { get { return _CompanyID; } set { _CompanyID = value; } }
        public string CompanyName { get { return _CompanyName; } set { _CompanyName = value; } }
        public string DB { get { return _DB; } set { _DB = value; } }

        public string EmployeeID { get { return _EmployeeID; } set { _EmployeeID = value; } }
        public string LeaveApproverID { get { return _LeaveApproverID; } set { _LeaveApproverID = value; } }
        public string OTApproverID { get { return _OTApproverID; } set { _OTApproverID = value; } }

        public string EmployeeName { get { return _EmployeeName; } set { _EmployeeName = value; } }
        public string LeaveApproverName { get { return _LeaveApproverName; } set { _LeaveApproverName = value; } }
        public string OTApproverName { get { return _OTApproverName; } set { _OTApproverName = value; } }

        public string Levels { get { return _Levels; } set { _Levels = value; } }
        public string[] Level { get { return _Levels.Split('~'); } }
        public string Position { get { return _Position; } set { _Position = value; } }
        public string Level1Data { get { return _Level1Data; } set { _Level1Data = value; } }
        public string Level2Data { get { return _Level2Data; } set { _Level2Data = value; } }
        public string Level3Data { get { return _Level3Data; } set { _Level3Data = value; } }
        public string Level4Data { get { return _Level4Data; } set { _Level4Data = value; } }

        public bool WorkForEnabled { get { return _WorkForEnabled; } set { _WorkForEnabled = value; } }
        public string WorkFor { get { return _WorkFor; } set { _WorkFor = value; } }
        public string WorkForSub { get { return _WorkForSub; } set { _WorkForSub = value; } }

        public string DateFormat { get { return "MM/dd/yyyy"; ; } }

        public DateTime StartDate { get { return _StartDate; } set { _StartDate = value; } }
        public DateTime EndDate { get { return _EndDate; } set { _EndDate = value; } }
        public bool IsLocked { get; set; }

        /*
        public static Kiosk GetSession()
        {
            Kiosk usrSession; 
            if ((System.Web.HttpContext.Current.Session[USER_SESSION_ID] == null)) 
            {             
                // No current session object exists, use private constructor to 
                // create an instance, place it into the session
                usrSession = new Kiosk();
                System.Web.HttpContext.Current.Session[USER_SESSION_ID] = usrSession; 
            }
            else 
            {
                // Retrieve the instance that was already created 
                usrSession = (Kiosk)System.Web.HttpContext.Current.Session[USER_SESSION_ID];
            } 
            
            // Return the single instance of this class that was stored in the session 
            return usrSession; 
        }
        */

    }

}