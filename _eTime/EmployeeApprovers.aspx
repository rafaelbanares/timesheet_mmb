<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="EmployeeApprovers.aspx.cs" Inherits="EmployeeApprovers" Title="EmployeeApprovers List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
    function EndRequestHandler(sender, args) {
       if (args.get_error() == undefined)
        {
            var action = $get("<%=hfErrorMsg.ClientID%>").value;
            if (action)
            {            
                alert(action);
            }
        }
       else
           alert('There was an error' + args.get_error().message);
    }
    function openSelectEmp(sUrl)
    {
        popupWindow=wopen(sUrl,"popupWindow",500,500);
    }
    function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue, openerCompanyClientID, openerCompanyValue)
    {
        $get(openerEmpClientID).value = openerEmpValue;
        $get(openerNameClientID).value = openerNameValue;
        $get(openerCompanyClientID).value = openerCompanyValue;
        closewindow(popupWindow);
    }
</script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Approver Lists Admin"></asp:Label><br />
            <br />
            <table width="100%">
              <tr>
                <td align="right" style="width: 80px">
                    <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click" ValidationGroup="emplist" />
                  </td>
              </tr>
            </table>
        </td>
        <td align="right">
        </td>
        <td align="center">
        </td>
    </tr>
</table>

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20"
        Width="100%" AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowDeleting="gvEmp_RowDeleting" 
            OnRowEditing="gvEmp_RowEditing" OnRowUpdating="gvEmp_RowUpdating" 
            DataKeyNames="EmployeeID" onrowcommand="gvEmp_RowCommand">
        <Columns>                        
            <asp:BoundField DataField="FullName" HeaderText="Employee Name" ItemStyle-Width="210px" ReadOnly="true">
            </asp:BoundField>
            <asp:TemplateField HeaderText="level 1 main approver" SortExpression="Approver1Name" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAppr1" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Approver1Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAppr1" runat="server" Text="..." CssClass="ControlDefaults" />
                    <asp:RequiredFieldValidator
                        ID="rfvAppr1" runat="server" ControlToValidate="txtAppr1" ErrorMessage="Approver cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hfAppr1" runat="server" Value='<%# Bind("Approver1") %>' />
                        <asp:HiddenField ID="hfCompID" runat="server" />
                        <asp:HiddenField ID="hfEmpid" runat="server" Value='<%# Bind("EmployeeID") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAppr1" runat="server" Text='<%# Bind("Approver1Name") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level 1 back-up approver" SortExpression="Approver2Name" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAppr2" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Approver2Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAppr2" runat="server" Text="..." CssClass="ControlDefaults" />
                    <asp:ImageButton ID="ibClear1" runat="server" CommandName="clear1" ToolTip="Remove Level 1 back up approver" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure you want to remove Level 1 back up approver?' );" />
                    <asp:HiddenField ID="hfAppr2" runat="server" Value='<%# Bind("Approver2") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAppr2" runat="server" Text='<%# Bind("Approver2Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level 2 main approver" SortExpression="Approver3Name" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAppr3" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Approver3Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAppr3" runat="server" Text="..." CssClass="ControlDefaults" />
                    <asp:ImageButton ID="ibClear2" runat="server" CommandName="clear2" ToolTip="Remove Level 2 main approver" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure you want to remove Level 2 main approver?' );" />
                    <asp:HiddenField ID="hfAppr3" runat="server" Value='<%# Bind("Approver3") %>' />                     
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAppr3" runat="server" Text='<%# Bind("Approver3Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level 2 back up approver" SortExpression="Approver4Name" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAppr4" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Approver4Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAppr4" runat="server" Text="..." CssClass="ControlDefaults" />
                        <asp:ImageButton ID="ibClear3" runat="server" CommandName="clear3" ToolTip="Remove Level 2 back up approver" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure you want to remove Level 2 back up approver?' );" />
                    <asp:HiddenField ID="hfAppr4" runat="server" Value='<%# Bind("Approver4") %>' />                     
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAppr4" runat="server" Text='<%# Bind("Approver4Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemStyle Width="42px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                    <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <asp:HiddenField ID="hfIsAdd" runat="server" />
        <asp:HiddenField ID="hfErrorMsg" runat="server" />
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibSearch"></asp:AsyncPostBackTrigger>        
    </Triggers>
    
</asp:UpdatePanel>
</asp:Content>

