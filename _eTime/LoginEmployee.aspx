
<%@ Page Language="C#" MasterPageFile="~/AttendanceLogin.master" AutoEventWireup="true" CodeFile="LoginEmployee.aspx.cs" Inherits="Attendance_LoginEmployee"%>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script language="javascript" type="text/javascript">
    try{
        self.resizeTo(screen.width,screen.height); 
        self.moveTo(0,0);}
    catch(e){}
</script>    
    <br />
    <table border="1" style="background-color:ghostwhite; border-width:1; border-color:dodgerblue">
     <tr>
      <td>
        <strong>Employee sign in</strong>
        <hr />
        <table border="0" cellpadding="0" cellspacing="3">
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td align="right">
                    Employee ID:</td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" Width="150px" MaxLength="15"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right">
                    Password:</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="150px" MaxLength="15"></asp:TextBox></td>
            </tr>
            <tr style="display:none">
                <td align="right">
                    Company:</td>
                <td>
                    <asp:TextBox ID="txtCompanyID" runat="server" Enabled="False" Width="150px">EW01</asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td align="right">
                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtUsername"
                        ErrorMessage="- Employee ID is required"></asp:RequiredFieldValidator><br />
                    </td>
            </tr>
        </table>
      </td>
     </tr>
    </table>
    
</asp:Content>

