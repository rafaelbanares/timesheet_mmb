using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class popupEmployee : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");
            body.Attributes.Add("onload", "load();");

            populate_Dropdown();
            BindGrid();
        }
    }

    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", ddlCompany.SelectedValue) //_KioskSession.CompanyID)
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            //,new SqlParameter("@Level1", levels[0])
            //,new SqlParameter("@Level2", levels[1])
            //,new SqlParameter("@Level3", levels[2])
            //,new SqlParameter("@Level4", levels[3])
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeList", spParams);
        return ds;

        /*
        //TextBox txtSearch = Master.FindControl("txtSearch") as TextBox;
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@SecID", "")
            ,new SqlParameter("@EmpList", "")
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GetEmployeeDeptList", spParams);
        return ds;
        */
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    protected override DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        gvEmp.SelectedIndex = -1;
        return base.SortDataTable(dataTable, isPageIndexChanging);
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ibSelect_Click(object sender, ImageClickEventArgs e)
    {
        Control ibButton = (Control)sender;
        Label lblID = (Label)ibButton.Parent.Parent.FindControl("lblID");
        Literal litName = (Literal)ibButton.Parent.Parent.FindControl("litName");

        hfEmpID.Value = lblID.Text;
        hfName.Value = litName.Text;
    }
    private void populate_Dropdown()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT rtrim(CompanyID) as CompanyID, CompanyName FROM " + _KioskSession.DB + ".dbo.Company ORDER BY CompanyName ");
        ddlCompany.DataSource = ds;
        ddlCompany.DataBind();

        ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(_KioskSession.CompanyID));
    }

}
