﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="UploadTimeBiometrics.aspx.cs" Inherits="UploadTimeBiometrics" Title="Bisneeds - Timekeeping - Upload Time Transaction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript">

	function UpdateImg(ctrl,imgsrc) {
		var img = $get(ctrl);
		img.src = imgsrc;
	}

    function toggle(isVisible)
    {
        if (isVisible)
        {
            setTimeout("UpdateImg('<%=imgProcess.ClientID%>','Graphics/indicator.gif');",50);            
            $get("<%=lblStatus.ClientID%>").innerHTML = "Uploading... please wait...";
            $get("<%=imgProcess.ClientID%>").style.display = "";
        }
        else
        {
            $get("<%=lblStatus.ClientID%>").innerHTML = "";
            $get("<%=imgProcess.ClientID%>").style.display = "none";
        }
    }
</script>

<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
    Font-Underline="False" Text="Upload Transaction"></asp:Label><br />
<br />

<table cellpadding="0" class="TableBorder1" style="width: 574px">
    <tr>
        <td class="DataGridHeaderStyle">
            Please select a file to upload:
        </td>
    </tr>
    <tr>
        <td>
            Filename:
            <asp:FileUpload ID="fuWorkfile" runat="server" CssClass="ControlDefaults" Width="499px" /><br /><br />
            <asp:Button ID="btnSend" runat="server" CssClass="ControlDefaults" OnClick="btnSend_Click" Text="Send" OnClientClick="toggle(true);" /><br /><br />
            <asp:Label ID="lblStatus" runat="server"></asp:Label>
            <asp:Image ID="imgProcess" runat="server" ImageUrl="~/Graphics/indicator.gif" style="display:none;" /></td>
    </tr>
</table>

<asp:Label ID="lblSummary" runat="server"></asp:Label><br />
<br />


<asp:Panel ID="pnlInvalid" runat="server" Visible="false">
    <asp:Label ID="lblCount" runat="server" 
        Text="There are n invalid transactions." ForeColor="Red" ></asp:Label>
    <br />
    Would you like to continue?
    <asp:Button ID="btnYes" runat="server" onclick="btnYes_Click" Text="Yes" />
    &nbsp;
    <asp:Button ID="btnNo" runat="server" onclick="btnNo_Click" Text="No" />
    <br />

    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle" >
        <Columns>
            <asp:BoundField DataField="Invalid Swipe ID" HeaderText="Invalid Swipe ID" SortExpression="Invalid Swipe ID" />
            <asp:BoundField DataField="Transaction Count" HeaderText="Transaction Count" SortExpression="Transaction Count" />
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>
</asp:Panel>


</asp:Content>
