﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;


public partial class AdminOTApproval : KioskPageUI
{
    private readonly string stage = "2";        //final stage

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            BindGrid();
            updateurl();
        }
    }
    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        lnkLevel.Attributes.Remove("onclick");
        lnkLevel.Attributes.Add("onclick", "openwindow(); return false;");
        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        btnForApproval.Enabled = (gvEmp.EditIndex == -1 && CountStatus("For Approval") > 0);
        btnForFinalApproval.Enabled = (gvEmp.EditIndex == -1 && CountStatus("For Final Approval") > 0);
    }
    private DataSet GetData()
    {
        string[] levels = hfLevels.Value.Split('~');
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Level1", levels[0])
            ,new SqlParameter("@Level2", levels[1])
            ,new SqlParameter("@Level3", levels[2])
            ,new SqlParameter("@Level4", levels[3]) 
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            ,new SqlParameter("@StartDate", txtDateStart.Text )
            ,new SqlParameter("@EndDate", txtDateEnd.Text )
            ,new SqlParameter("@Filter", ddlShow.SelectedItem.Value)
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTApproval", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                ibEdit.Attributes.Add("onclick", "this.style.display='none'");
                if ((bool)DataBinder.Eval(e.Row.DataItem, "Posted") == true)
                    ibEdit.Visible = false;

                Label lblReason = e.Row.FindControl("lblReason") as Label;
                string reason = lblReason.Text;
                if (reason.Length > 20)
                {
                    lblReason.ToolTip = reason;
                    lblReason.Text = reason.Substring(0, 15) + "...";
                }
            }

            /* List of status
             * stage = 0 - For Approval
             * stage = 1 - For Final Approval
             * stage = 2 - Status Approved or Declined
             */

            Literal litStage = e.Row.FindControl("litStage") as Literal;
            litStage.Text = DataBinder.Eval(e.Row.DataItem, "Status").ToString();

            //if (litStage.Text == "0" || litStage.Text == "2")
            //    litStage.Text = gvEmp.DataKeys[e.Row.RowIndex].Values["status"].ToString();
            //else if (litStage.Text == "1")
            //    litStage.Text = "For Final Approval";
            //else
            //    litStage.Text = "&nbsp;";

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Literal litEmployeeID = gvEmp.Rows[e.RowIndex].FindControl("litEmployeeID") as Literal;
        Literal litDate = gvEmp.Rows[e.RowIndex].FindControl("litDate") as Literal;
        string id = gvEmp.DataKeys[e.RowIndex].Values["id"].ToString();
        string status = gvEmp.DataKeys[e.RowIndex].Values["status"].ToString();

        TextBox tbin1 = gvEmp.Rows[e.RowIndex].FindControl("tbin1") as TextBox;
        TextBox tbout1 = gvEmp.Rows[e.RowIndex].FindControl("tbout1") as TextBox;
        TextBox tbApprovedOT = gvEmp.Rows[e.RowIndex].FindControl("tbApprovedOT") as TextBox;
        TextBox tbReason = gvEmp.Rows[e.RowIndex].FindControl("tbReason") as TextBox;

        DateTime in1;
        DateTime out1;
        string othours = "0.00";

        in1 = DateTime.Parse(litDate.Text + " " + tbin1.Text);
        out1 = DateTime.Parse(litDate.Text + " " + tbout1.Text);

        if (in1 > out1)
            out1 = out1.AddHours(24);

        if (litDate.Text.Length > 0 && tbin1.Text.Length > 0 && tbout1.Text.Length > 0)
        {
            TimeSpan ts = out1.Subtract(in1);
            othours = MMB.Core.Utils.TimeToDecimalRounded(ts.Hours, ts.Minutes);
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
        sqlparam.Add(new SqlParameter("@ID", id));

        sqlparam.Add(new SqlParameter("@OTDate", litDate.Text));
        sqlparam.Add(new SqlParameter("@OTstart", in1 ));
        sqlparam.Add(new SqlParameter("@OTend", out1 ));

        //... validation not needed since OTDate is not changed
        //... string ErrMsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();

        sqlparam.Add(new SqlParameter("@ApprovedOT", othours));
        sqlparam.Add(new SqlParameter("@Reason", tbReason.Text));
        sqlparam.Add(new SqlParameter("@Status", "Approved"));
        sqlparam.Add(new SqlParameter("@Stage", stage));

        sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdate", sqlparam.ToArray());

        gvEmp.EditIndex = -1;
        BindGrid();
    }

    private void UpdateBulk(string filterstatus)
    {
        //... count first if there is records to be udpated
        if (CountStatus(filterstatus) == 0)
            return;

        string ids = "";

        foreach (GridViewRow row in gvEmp.Rows)
        {
            Literal litStage = row.FindControl("litStage") as Literal;
            if (litStage.Text.ToLower() == filterstatus.ToLower())
                ids += "," + gvEmp.DataKeys[row.RowIndex].Values["id"].ToString();
        }

        if (ids.Length > 0)
        {
            ids = "(" + ids.Substring(1) + ")";

            List<SqlParameter> sqlparam = new List<SqlParameter>();
            //sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
            sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
            sqlparam.Add(new SqlParameter("@IDs", ids));
            sqlparam.Add(new SqlParameter("@Status", "Approved"));
            sqlparam.Add(new SqlParameter("@Stage", stage));
            sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdateBulk", sqlparam.ToArray());
        }

        gvEmp.EditIndex = -1;
        BindGrid();
    }

    private int CountStatus(string filterstatus)
    {
        int retval = 0;
        foreach (GridViewRow row in gvEmp.Rows)
        {
            Literal litStage = row.FindControl("litStage") as Literal;
            if (litStage.Text.ToLower() == filterstatus.ToLower())
                retval++;
        }
        return retval;
    }
    protected void btnForApproval_Click(object sender, EventArgs e)
    {
        UpdateBulk("For Approval");
    }
    protected void btnForFinalApproval_Click(object sender, EventArgs e)
    {
        UpdateBulk("For Final Approval");
    }
    protected void ddlShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        hfLevels.Value = "~~~";
        txtLevels.Text = "";
        BindGrid();
        updateurl();
    }

}
