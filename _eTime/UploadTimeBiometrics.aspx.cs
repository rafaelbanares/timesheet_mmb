﻿using System;
using System.IO;
using Bns.AttendanceUI;

using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using MMB.Core;


public partial class UploadTimeBiometrics : KioskPageUI
{
    string strFile;
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        pnlInvalid.Visible = false;
        lblSummary.Text = "";
        if (fuWorkfile.HasFile)
        {
            try
            {
                strFile = Server.MapPath("~/UploadedFiles/" + fuWorkfile.FileName);
                fuWorkfile.SaveAs(strFile);

                BulkInsert();
            }
            catch (Exception ex)
            {
                lblStatus.Text = "ERROR: " + ex.Message.ToString();
            }
        }
        else
        {
            lblStatus.Text = "You have not specified a file.";
        }
    }

    private void BulkInsert()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeTransBiometricsNoEmployee", new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB)
                            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                            ,new SqlParameter("@FilePath", strFile) }
                        );

        int invalidCount = ds.Tables[1].Rows.Count;

        if (invalidCount > 0)
        {
            lblCount.Text = "There are " + invalidCount.ToString() + " invalid transactions";

            pnlInvalid.Visible = true;
            gvEmp.DataSource = ds.Tables[1];
            gvEmp.DataBind();
        }
        else
        {
            Proceed();
        }

    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        Proceed();
    }

    protected void Proceed()
    {
        pnlInvalid.Visible = false;
        try
        {
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeTransBiometricsAttendance", new SqlParameter[] {
                        new SqlParameter("@DBName", _KioskSession.DB)
                        ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                        ,new SqlParameter("@FilePath", strFile) }
                    );

            lblSummary.ForeColor = System.Drawing.Color.Black;
            lblSummary.Text = "Uploading completed...";
        }
        catch //(Exception ex)
        {
            lblSummary.ForeColor = System.Drawing.Color.Red;
            lblSummary.Text = "The file uploaded was not recognized by the system";
            //lblSummary.Text = "ERROR: " + ex.Message.ToString();
        }
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        pnlInvalid.Visible = false;
    }
}
