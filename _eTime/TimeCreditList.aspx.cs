﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;

public partial class TimeCreditList : KioskPageUI
{
    protected readonly string sBackColor_Restday = "#CC99FF";
    protected readonly string sBackColor_Holiday = "#ffd700";
    protected readonly string sBackColor_RstHoliday = "#f4a460";
    protected readonly string sBackColor_LeaveWhole = "#90ee90";
    protected readonly string sBackColor_LeaveHalf = "#CCFF66";
    protected readonly string sBackColor_Absent = "#c0c0c0";

    protected void Page_Load(object sender, EventArgs e)
    {
        hfUpdate.Value = "";
        base.GetUserSession();

        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            body.Attributes.Add("onload", "load();");
            //body.Attributes.Add("onfocus", "if (window.popupWindow) {window.popupWindow.close()}");
            body.Attributes.Add("onfocus", "closewindow();");

            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();

            BindGrid();
            updateurl();
        }
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        lnkLevel.Attributes.Remove("onclick");
        lnkLevel.Attributes.Add("onclick", "openwindow(); return false;");
        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    private void BindGrid()
    {
        DataSet ds = GetData();

        gvEmp.DataSource = SortDataTable(ds.Tables[0], true);
        gvEmp.DataBind();

        updateGrid(ds.Tables[1]);
    }

    private void updateGrid(DataTable dt)
    {
        for (int i = 0; i < gvEmp.Rows.Count; i++)
        {
            Label lblTardy = gvEmp.Rows[i].FindControl("lblTardy") as Label;
            Label lblUndertime = gvEmp.Rows[i].FindControl("lblUndertime") as Label;

            string tcId = gvEmp.DataKeys[i].Values["tcID"].ToString();

            DataRow[] drTardy = dt.Select("tcID = " + tcId);

            StringBuilder sbTardy = new StringBuilder();
            StringBuilder sbUT = new StringBuilder();

            foreach (DataRow dr in drTardy)
            {
                string s = Convert.ToDateTime(dr["Date"]).ToString(_KioskSession.DateFormat) + " - " + dr["Used"].ToString();

                if (dr["TardyUT"].ToString() == "U")
                    sbUT.Append(sbUT.Length != 0 ? "<br>" + s : s);
                else
                    sbTardy.Append(sbTardy.Length != 0 ? "<br>" + s : s);
            }
            lblTardy.Text = sbTardy.ToString();
            lblUndertime.Text = sbUT.ToString();
        }
    }

    private DataSet GetData()
    {
        string[] levels = hfLevels.Value.Split('~');
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Level1", levels[0])
            ,new SqlParameter("@Level2", levels[1])
            ,new SqlParameter("@Level3", levels[2])
            ,new SqlParameter("@Level4", levels[3])
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            ,new SqlParameter("@StartDate", txtDateStart.Text)
            ,new SqlParameter("@EndDate", txtDateEnd.Text)
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeCreditsLoadByDateRange", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        hfLevels.Value = "~~~";
        txtLevels.Text = "";
        BindGrid();
        updateurl();
    }

}
