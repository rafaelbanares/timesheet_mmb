﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class LeaveExpiryProcessing : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        lblError.Text = "";

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (DateTime.Today <= DateTime.Parse(txtTranDate.Text)  )
        {
            lblError.Text = "Transaction Date must be greater than today";
        }
        else
        {
            List<SqlParameter> sqlparam = new List<SqlParameter>();
            sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
            sqlparam.Add(new SqlParameter("@TranDate", txtTranDate.Text));
            sqlparam.Add(new SqlParameter("@IsYearEnd", rbYes.Checked ));
            sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

            string result = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveExpiryCarryOverProcess", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();

            lblError.Text = result + " processed";
        }
    }
}
