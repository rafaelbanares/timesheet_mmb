using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class AttendanceLists : KioskPageUI
{
    protected readonly string sBackColor_Restday = "#CC99FF";
    protected readonly string sBackColor_Holiday = "#ffd700";
    protected readonly string sBackColor_RstHoliday = "#f4a460";
    protected readonly string sBackColor_LeaveWhole = "#90ee90";
    protected readonly string sBackColor_LeaveHalf = "#CCFF66";
    protected readonly string sBackColor_Absent = "#c0c0c0";

    protected void Page_Load(object sender, EventArgs e)
    {
        hfUpdate.Value = "";
        base.GetUserSession();

        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            body.Attributes.Add("onload", "load();");
            //body.Attributes.Add("onfocus", "if (window.popupWindow) {window.popupWindow.close()}");
            body.Attributes.Add("onfocus", "closewindow();");

            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            
            BindGrid();
            updateurl();
        }
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();

        lnkLevel.Attributes.Remove("onclick");
        lnkLevel.Attributes.Add("onclick", "openwindow(); return false;");
        //btnSection.Attributes.Add("onclick", "openwindow('" + secureUrl.ToString() + "'); return false;");
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    private DataSet GetData()
    {
        string[] levels = hfLevels.Value.Split('~');
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Level1", levels[0])
            ,new SqlParameter("@Level2", levels[1])
            ,new SqlParameter("@Level3", levels[2])
            ,new SqlParameter("@Level4", levels[3])
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
            ,new SqlParameter("@StartDate", txtDateStart.Text)
            ,new SqlParameter("@EndDate", txtDateEnd.Text)
            ,new SqlParameter("@Filter", ddlShow.SelectedItem.Value)
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceLoadByDateRange", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                #region assign background color
                string otcode = DataBinder.Eval(e.Row.DataItem, "OTCode").ToString().Trim();
                string leavecode = DataBinder.Eval(e.Row.DataItem, "LeaveCode").ToString().Trim();
                decimal leavedays = (decimal)DataBinder.Eval(e.Row.DataItem, "Leave");
                decimal absentdays = (decimal)DataBinder.Eval(e.Row.DataItem, "Absent");

                if (otcode == "RST")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Restday);
                else if (otcode == "SPL" || otcode == "LGL")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Holiday);
                else if (otcode == "SPLRST" || otcode == "LGLRST")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_RstHoliday);
                else if (leavecode != "" && leavedays >= 1)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_LeaveWhole);
                else if (leavecode != "" && leavedays < 1)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_LeaveHalf);
                else if (absentdays > 0)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Absent);
                #endregion

            }

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }


    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = sender as ImageButton;
        Literal litEmpID = ib.Parent.Parent.FindControl("litEmpID") as Literal;
        Literal litDate = ib.Parent.Parent.FindControl("litDate") as Literal;

        SecureUrl secureUrl = new SecureUrl(
            string.Format("AttendanceEdit.aspx?EmpID={0}&Date={1}", litEmpID.Text, litDate.Text));

        //... needs postback in order to create a secured url.
        hfUpdate.Value = secureUrl.ToString();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void ddlShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        hfLevels.Value = "~~~";
        txtLevels.Text = "";
        BindGrid();
        updateurl();
    }

}
