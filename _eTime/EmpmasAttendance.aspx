<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="EmpmasAttendance.aspx.cs" Inherits="EmpmasAttendance" Title="Bisneeds - Timekeeping - Employee Masterfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>
    <script type="text/javascript">
    function openwindow()
    {
        popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
    }
    function updatelevels(ids, levels, url)
    {
        $get('<%= hfLevels.ClientID %>').value = ids;        
        $get('<%= txtDepartment.ClientID %>').value = levels;
        $get('<%= hfLevelDisp.ClientID %>').value = levels;
        $get('<%= hfURL.ClientID %>').value = url;
        
        closewindow(popupWindow);
    }
    function openSelectEmp(sUrl)
    {
        popupWindow=wopen(sUrl,"popupWindow",1000,600);
    }
    function openHistory(sUrl)
    {
        popupWindow=wopen(sUrl,"popupWindow",800,578);
    }
    function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue, openerCompanyClientID, openerCompanyValue)
    {
        $get(openerEmpClientID).value = openerEmpValue;
        $get(openerNameClientID).value = openerNameValue;
        $get(openerCompanyClientID).value = openerCompanyValue;
        closewindow(popupWindow);
    }
    </script>

    <table cellpadding="0" class="TableBorder1" width="640">
    <tr>
        <td>
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Employee Maintenance -"></asp:Label>
            &nbsp;<asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large"
                Text="Lastname, Firstname S."></asp:Label>        
        </td>
        <td>| &nbsp;
            <asp:LinkButton ID="lnkLeaveCredits" runat="server">Leave Credits</asp:LinkButton> &nbsp; | &nbsp;
            <asp:LinkButton ID="lnkHistory" runat="server" Visible="false">View History</asp:LinkButton>
        </td>
    </tr>
    </table>
        
    <br />
    <table cellpadding="0" class="TableBorder1" width="640">
        <tr>
            <td class="DataGridHeaderStyle" colspan="6">
                Basic Information</td>
        </tr>
        <tr>
            <td>
                Employee ID.</td>
            <td>
                <asp:TextBox ID="txtEmployeeID" runat="server" CssClass="ControlDefaults" MaxLength="10"
                    Width="90px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtEmployeeID" runat="server" ControlToValidate="txtEmployeeID"
                    ErrorMessage="Emplyee Number is required">*</asp:RequiredFieldValidator></td>
            <td style="color: #000000">
                Gender:</td>
            <td>
                <asp:DropDownList ID="ddlGender" runat="server" CssClass="ControlDefaults">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value="M">Male</asp:ListItem>
                    <asp:ListItem Value="F">Female</asp:ListItem>
                </asp:DropDownList>
                        </td>
            <td style="color: #000000">
                Birth Date:</td>
            <td>
                <asp:TextBox ID="txtBirthDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtBirthDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditValidator ID="mevBirthDate"
                        runat="server" ControlExtender="meeBirthDate" ControlToValidate="txtBirthDate"
                        Display="Dynamic"
                        ErrorMessage="*" InvalidValueBlurredMessage="*" 
                    InvalidValueMessage="Invalid Date of Birth"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
        <tr>
            <td>
                Last Name:</td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server" CssClass="ControlDefaults" MaxLength="30"
                    Width="180px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtLastName" runat="server" ControlToValidate="txtLastName"
                    ErrorMessage="Lastname is required">*</asp:RequiredFieldValidator></td>
            <td>
                Address:</td>
            <td colspan="3">
                <asp:TextBox ID="txtAddress1" runat="server" CssClass="ControlDefaults" MaxLength="100"
                    Width="230px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                First Name:</td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server" CssClass="ControlDefaults" MaxLength="30"
                    Width="180px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtFirstName" runat="server" ControlToValidate="txtFirstName"
                    ErrorMessage="Firstname is required">*</asp:RequiredFieldValidator></td>
            <td>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtAddress2" runat="server" CssClass="ControlDefaults" MaxLength="100"
                    Width="230px"></asp:TextBox></td>
        </tr>
        <tr>
            <td valign="top">
                Middle Name:</td>
            <td valign="top">
                <asp:TextBox ID="txtMiddleName" runat="server" CssClass="ControlDefaults" MaxLength="30"
                    Width="180px"></asp:TextBox></td>
            <td>
                Department:</td>
            <td colspan="3" valign="top">
                <asp:TextBox ID="txtDepartment" runat="server" CssClass="ControlDefaults"
                    Width="225px" TextMode="MultiLine" Height="62px" Enabled="False"></asp:TextBox><asp:Button ID="btnSection" runat="server"
                        CssClass="ControlDefaults" Text="..." ToolTip="Select Division/Department/Section" /></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" class="TableBorder1" width="640">
        <tr>
            <td class="DataGridHeaderStyle" colspan="4">
                Employment Information</td>
        </tr>
        <tr>
            <td>
                Employment Type:</td>
            <td>
                <asp:DropDownList ID="ddlEmploymentType" runat="server" CssClass="ControlDefaults">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="U">Consultant</asp:ListItem>
                    <asp:ListItem Value="C">Contractual</asp:ListItem>
                    <asp:ListItem Value="P">Probationary</asp:ListItem>
                    <asp:ListItem Value="R">Regular</asp:ListItem>
                </asp:DropDownList></td>
            <td>
                Date Employed:</td>
            <td>
                <asp:TextBox ID="txtDateEmployed" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateEmployed" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditValidator ID="mevDateEmployed"
                        runat="server" ControlExtender="meeDateEmployed" ControlToValidate="txtDateEmployed"
                        Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Date employed is required"
                        ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Invalid Date of Employment"
                        IsValidEmpty="False"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
        <tr>
            <td>
                Rank:</td>
            <td>
                <asp:DropDownList ID="ddlRank" runat="server" CssClass="ControlDefaults">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="M">Manager</asp:ListItem>
                    <asp:ListItem Value="R">Rank &amp; File</asp:ListItem>
                    <asp:ListItem Value="S">Supervisor</asp:ListItem>
                </asp:DropDownList></td>
            <td>
                Date Regularized:</td>
            <td>
                <asp:TextBox ID="txtDateRegularized" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateRegularized" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditValidator ID="mevDateRegularized"
                        runat="server" ControlExtender="meeDateRegularized" ControlToValidate="txtDateRegularized"
                        Display="Dynamic" ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Invalid Date Regularized"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
        <tr>
            <td style="height: 20px">
                Position:</td>
            <td style="height: 20px">
                <asp:TextBox ID="txtPosition" runat="server" CssClass="ControlDefaults" MaxLength="50"
                    Width="200px"></asp:TextBox></td>
            <td style="height: 20px">
                Date Resigned:</td>
            <td style="height: 20px">
                <asp:TextBox ID="txtDateTerminated" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateTerminated" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditValidator ID="mevDateTerminated"
                        runat="server" ControlExtender="meeDateTerminated" ControlToValidate="txtDateTerminated"
                        Display="Dynamic" ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Invalid Date Terminated"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" class="TableBorder1" width="640">
        <tr>
            <td class="DataGridHeaderStyle" colspan="4">
                Attendance Information</td>
        </tr>
        <tr>
            <td valign="top">
    
    <table>
        <tr>
            <td>
                Machine Swipe ID:                
            </td>
            <td>
                <asp:TextBox ID="txtSwipeID" runat="server" CssClass="ControlDefaults" MaxLength="15" Width="90px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtSwipeID" runat="server" ControlToValidate="txtSwipeID"
                    ErrorMessage="Machine Swipe ID is required">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtApproverOT" runat="server" CssClass="ControlDefaults" MaxLength="10"
                    Width="155px" Enabled="False" Visible="False"></asp:TextBox>
                <asp:Button ID="btnApproverOT" runat="server" 
                        CssClass="ControlDefaults" Text="..." 
                    ToolTip="Select overtime approver" Visible="False" /></td>
        </tr>
        <tr>
            <td>
                Base Shift:</td>
            <td>
                <asp:DropDownList ID="ddlShift" runat="server" DataTextField="Description" DataValueField="ShiftCode" CssClass="ControlDefaults">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvddlShift" runat="server" ControlToValidate="ddlShift"
                    ErrorMessage="Base shift is required">*</asp:RequiredFieldValidator></td>
        </tr>
        </table>
            </td>
            <td valign="top">
    
    <table>
        <tr>
            <td valign="top">
                <span style="text-decoration: underline">
                Base Restday:<br />
                </span>
                <br />
                <asp:CheckBoxList ID="cblRestDay1" runat="server">
                    <asp:ListItem Selected="True" Value="6">Saturday</asp:ListItem>
                    <asp:ListItem Selected="True" Value="7">Sunday</asp:ListItem>
                </asp:CheckBoxList>
            </td>
            <td valign="top">
                <asp:CheckBoxList ID="cblRestDay2" runat="server">
                    <asp:ListItem Value="1">Monday</asp:ListItem>
                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                    <asp:ListItem Value="3">Wednessday</asp:ListItem>
                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                    <asp:ListItem Value="5">Friday</asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>    
    </table>
            </td>
            <td valign="top">
    <table>
        <tr>
            <td valign="top">
                <span style="text-decoration: underline">
                Exemptions:</span><br />
                <br />
                <asp:CheckBox ID="chkTardyExempt" runat="server" Text="Tardy Exempt" /><br />
                <asp:CheckBox ID="chkUTExempt" runat="server" Text="Undertime Exempt" /><br />
                <asp:CheckBox ID="chkOTExempt" runat="server" Text="Overtime Exempt" /><br />
                <asp:CheckBox ID="chkNonswiper" runat="server" Text="Non-swiper" /></td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
    <br /><asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Graphics/save_big.gif"
        OnClick="imgSave_Click" />
    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="ControlDefaults" OnClick="lbSave_Click">Save Changes</asp:LinkButton><br />
    <br />
    <ajaxToolkit:MaskedEditExtender ID="meeBirthDate" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtBirthDate">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleBirthDate" runat="server" PopupButtonID="ibtxtBirthDate"
        TargetControlID="txtBirthDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEmployed" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEmployed">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEmployed" runat="server" PopupButtonID="ibtxtDateEmployed"
        TargetControlID="txtDateEmployed">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateRegularized" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateRegularized">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateRegularized" runat="server" PopupButtonID="ibtxtDateRegularized"
        TargetControlID="txtDateRegularized">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateTerminated" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateTerminated">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateTerminated" runat="server" PopupButtonID="ibtxtDateTerminated"
        TargetControlID="txtDateTerminated">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="hfEmpID" runat="server" />
    <asp:HiddenField ID="hfLevels" runat="server" Value="~~~" />
    <asp:HiddenField ID="hfURL" runat="server" /><asp:HiddenField ID="hfLevelDisp" runat="server" Value="~~~" />
    <asp:HiddenField ID="hfApproverOT" runat="server" />
    <asp:HiddenField ID="hfApprvrOTCompID" runat="server" />
    <br />


</asp:Content>


