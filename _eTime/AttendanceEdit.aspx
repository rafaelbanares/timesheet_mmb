<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="AttendanceEdit.aspx.cs" Inherits="AttendanceEdit" Title="Bisneeds - Timekeeping - Edit Attendance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript">
function EndRequestHandler(sender, args) {    
   if (args.get_error() == undefined)
    {
        var action = $get("<%=hfUpdate.ClientID%>").value;        
        if (action == "")
        {
            // do nothing....
        }        
        else if (action == "close")
        {
            window.opener.closechild();
            //window.opener.closewindow();
        }
        else 
        {
            alert(action);
        }
    }
   else
       alert('There was an error' + args.get_error().message);
}

function load() {
   Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);   
}
</script>

    <asp:UpdatePanel id="upAtt" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <table bgcolor="white">
            <tr>
                <td class="ControlDefaults" colspan="2" valign="top">
                    
                    <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large"></asp:Label></td>
                <td class="ControlDefaults" colspan="1" valign="top" align="right">
                    Employee ID.
                    <asp:TextBox ID="txtEmployeeID" runat="server" CssClass="ControlDefaults" 
                        Width="70px" ValidationGroup="FiltGrp" Enabled="False"></asp:TextBox>
                    <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                        ToolTip="Go" ValidationGroup="FiltGrp" Visible="False" /></td>
            </tr>
            <tr>
                <td colspan="2" valign="bottom" class="ControlDefaults">
                    </td>
                <td class="ControlDefaults" colspan="1" valign="top" align="right">
                    Transaction date:
                    <asp:TextBox ID="txtDate" runat="server" CssClass="TextBoxDate" 
                        ValidationGroup="FiltGrp" Enabled="False"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator id="mevDate" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Cannot accept invalid value" 
                        ValidationGroup="FiltGrp" ControlToValidate="txtDate" 
                        ControlExtender="meeDate" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                        IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" 
                        MinimumValueMessage="Cannot accept date" Display="Dynamic"  >*
                    </ajaxToolkit:MaskedEditValidator>
                    <asp:ImageButton ID="ibtxtDate" runat="server" 
                        ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;"
                        ToolTip="Click to choose date" Visible="False" /></td>
            </tr>
            <tr>
                <td class="ControlDefaults" colspan="3" valign="top">
                    <hr />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table class="TableBorder1" width="100%">
                        <tr>
                            <td colspan="2" class="Grayheader">
                                <asp:Label ID="lblRegHrs" runat="server" Text="Regular Hours" Width="240px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right">
                                &nbsp;Regular Hours:</td>
                            <td>
                                <asp:TextBox ID="txtReghrs" runat="server" CssClass="ControlDefaults" Width="50px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Regular ND1:</td>
                            <td>
                                <asp:TextBox ID="txtRegnd1" runat="server" CssClass="ControlDefaults" Width="50px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Regular ND2:</td>
                            <td>
                                <asp:TextBox ID="txtRegnd2" runat="server" CssClass="ControlDefaults" Width="50px"></asp:TextBox></td>
                        </tr>
                    </table>
                    <table class="TableBorder1" width="100%">
                        <tr>
                            <td align="right">
                                Remarks:</td>
                            <td>
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="ControlDefaults" Width="120px"></asp:TextBox></td>
                        </tr>
                    </table>
                    <br />
            <table class="TableBorder1" width="100%">
                <tr>
                    <td colspan="2" class="Grayheader">
                        <asp:Label ID="lblLateUT" runat="server" Text="Late and Undertime" Width="240px"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 19px">
                        <span style="text-decoration: underline">
                        Deductible</span></td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;Late:</td>
                    <td>
                        <asp:TextBox ID="txtLate" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;Undertime:</td>
                    <td>
                        <asp:TextBox ID="txtUT" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span style="text-decoration: underline">
                        Non-Deductible/Excused</span></td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;Late:</td>
                    <td>
                        <asp:TextBox ID="txtExcusedLate" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;Undertime:</td>
                    <td>
                        <asp:TextBox ID="txtExcusedUT" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox>
                    </td>
                </tr>
            </table>
                </td>
                <td valign="top">
            
            <table class="TableBorder1" width="100%">
                <tr>
                    <td colspan="2" class="Grayheader" align="left">
                        <asp:Label ID="lblOT" runat="server" Text="Overtime" Width="240px"></asp:Label></td>
                </tr>
                <tr>
                    <td align="right">
                        OT Code:<asp:Label ID="lblOTCode" runat="server" Font-Bold="True"></asp:Label></td>
                    <td align="right">
                        <asp:CheckBox
                        ID="chkValid" runat="server" CssClass="ControlDefaults" Text="Valid Overtime?" /></td>
                </tr>
                <tr>
                    <td align="right">
                        Excess Hours:
                        <asp:TextBox ID="txtExcessHrs" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        First 8:<asp:TextBox ID="txtFirst8" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                    <td>
                        OT ND1:<asp:TextBox ID="txtOTND1" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right">
                        Greater 8:<asp:TextBox ID="txtGreater8" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                    <td>
                        OT ND2:<asp:TextBox ID="txtOTND2" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right">
                        OT No Premium:<asp:TextBox ID="txtNoPremium" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" colspan="2"><hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:HyperLink ID="hlOT" runat="server" EnableTheming="True" Target="_self">Authorized Overtime</asp:HyperLink></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        From:<asp:TextBox ID="txtAuthStart" runat="server" CssClass="ControlDefaults" Width="56px" Enabled="False"></asp:TextBox>
                        To:<asp:TextBox ID="txtAuthEnd" runat="server" CssClass="ControlDefaults" Width="56px" Enabled="False"></asp:TextBox></td>
                </tr>
            </table>
            <br />
           
           <table class="TableBorder1" width="100%">
                <tr>
                    <td colspan="2" class="Grayheader">
                        <asp:Label ID="lblAbsencesLeaves" runat="server" Text="Absences and Leaves" Width="240px"></asp:Label>
                    </td>
                </tr>
               <tr>
                   <td align="right">
                   </td>
                   <td>
                   </td>
               </tr>
                <tr>
                    <td align="right">
                        Leave Code:</td>
                    <td>
                        <asp:TextBox ID="txtLeaveCode" runat="server" CssClass="ControlDefaults" Width="40px" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right">Leave Days:</td>
                    <td>
                        <asp:TextBox ID="txtLeaveDays" runat="server" CssClass="ControlDefaults" Width="40px" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right">
                        Absent Days:</td>
                    <td>
                        <asp:TextBox ID="txtAbsentDays" runat="server" CssClass="ControlDefaults" Width="40px"></asp:TextBox></td>
                </tr>
               <tr>
                   <td align="center" colspan="2">
                       <asp:HyperLink ID="hlLeave" runat="server" Target="_self">File a leave or view balance here</asp:HyperLink></td>
               </tr>
            </table>
                </td>
                <td valign="top">

        <table class="TableBorder1" width="100%">
            <tr>
                <td class="Grayheader" colspan="2" align="left">
                    <asp:Label ID="lblTrans" runat="server" Text="Time Transaction" Width="240px"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">
                    Time In:</td>
                <td>
                    <asp:TextBox ID="txtin1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevin1" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Time is invalid" Display="Dynamic" 
                        ValidationGroup="Trans" ControlToValidate="txtin1" ControlExtender="mein1" ErrorMessage="mevin1" />
                    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server"  ErrorTooltipEnabled="True" 
                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                        TargetControlID="txtin1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    Time Out:</td>
                <td>
                    <asp:TextBox ID="txtout1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevout1" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Time is invalid" Display="Dynamic" 
                        ValidationGroup="Trans" ControlToValidate="txtout1" ControlExtender="meout1" /><ajaxToolkit:MaskedEditExtender ID="meout1" runat="server"  ErrorTooltipEnabled="True" 
                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                        TargetControlID="txtout1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />

                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr style="display:none">
                <td align="right">
                    Time In2:</td>
                <td>
                    <asp:TextBox ID="txtin2" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevin2" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Time is invalid" Display="Dynamic" 
                        ValidationGroup="Trans" ControlToValidate="txtin2" ControlExtender="mein2" /><ajaxToolkit:MaskedEditExtender ID="mein2" runat="server"  ErrorTooltipEnabled="True" 
                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                        TargetControlID="txtin2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                </td>
            </tr>
            <tr style="display:none">
                <td align="right">
                    Time Out2:</td>
                <td>
                    <asp:TextBox ID="txtout2" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevout2" runat="server" InvalidValueBlurredMessage="*"
                        InvalidValueMessage="Time is invalid" Display="Dynamic" 
                        ValidationGroup="Trans" ControlToValidate="txtout2" ControlExtender="meout2" /><ajaxToolkit:MaskedEditExtender ID="meout2" runat="server"  ErrorTooltipEnabled="True" 
                        MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                        TargetControlID="txtout2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <hr />
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    Shift:</td>
                <td>
                    <asp:DropDownList ID="ddlShift" runat="server" DataTextField="Description" DataValueField="ShiftCode">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="right">
                    Restday:</td>
                <td>
                    <asp:TextBox ID="txtRestDay" runat="server" Text='<%# Bind("restdaycode") %>' Width="50px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right">
                    Error:</td>
                <td>
                    <asp:TextBox ID="txtError" runat="server" CssClass="ControlDefaults" 
                        Width="120px" Enabled="False"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblProcessed" runat="server" Text="attendance status"></asp:Label></td>
            </tr>

        </table>
                    <br />
                    <asp:Button ID="btnProcess" runat="server" Text="Save Process" CssClass="ControlDefaults" OnClick="btnProcess_Click" ValidationGroup="Trans;FiltGrp" />            
                    <asp:Button ID="btnSave" runat="server" Text="Save Manual" CssClass="ControlDefaults" OnClick="btnSave_Click" ValidationGroup="Trans" /><br />
                    </td>

            </tr>
            <tr>
                <td valign="top">
            
                </td>
                <td valign="top">
                </td>
                <td valign="top">
                    </td>
            </tr>
        </table>
        
        <ajaxToolkit:MaskedEditExtender ID="meeDate" runat="server" ErrorTooltipEnabled="True"
            Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError" TargetControlID="txtDate">
        </ajaxToolkit:MaskedEditExtender>    
        <ajaxToolkit:CalendarExtender ID="caleDate" runat="server" PopupButtonID="ibtxtDate"
            TargetControlID="txtDate" PopupPosition="BottomRight">
        </ajaxToolkit:CalendarExtender>
            <asp:HiddenField ID="hfUpdate" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

