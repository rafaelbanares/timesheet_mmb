﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class EmpHeaderFilter : UserControlUI
{
    public string Display { get {return ddlDisplay.SelectedItem.Value;} }
    public string Search { get { return txtSearch.Text; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!Page.IsPostBack)
        {
            PopulateScreenDefaults();
            UpdateURL();

            txtSearch.Attributes.Add("onchange", "refresh();return false;");
            txtSearch.Attributes.Add("onkeypress", "return enterpress(event);");
        }
    }

    public void UpdateURL()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupDateFilter.aspx?DateStart={0}&DateEnd={1}"
                                        , hdDateStart.Value, hdDateEnd.Value));

        hfURL.Value = secureUrl.ToString();
        lnkFilter.Attributes.Remove("onclick");
        lnkFilter.Attributes.Add("onclick", "openwindow();return false");
    }

    public void PopulateScreenDefaults()
    {
        lblFullname.Text = _KioskSession.EmployeeName + " (" + _KioskSession.EmployeeID.Trim() + ")";

        SecureUrl secureUrl = new SecureUrl(string.Format("EmployeeInfo.aspx?CompID={0}&EmpID={1}", _KioskSession.CompanyID, _KioskSession.EmployeeID.Trim()));
        lbEmpInfo.Attributes.Add("onclick", "return openEmpInfo('" + secureUrl.ToString() + "');");
    }

}
