<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="Approvers.aspx.cs" Inherits="Approvers" Title="Approvers List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
    function EndRequestHandler(sender, args) {
       if (args.get_error() == undefined)
        {
            var action = $get("<%=hfErrorMsg.ClientID%>").value;
            if (action)
            {            
                alert(action);
            }
        }
       else
           alert('There was an error' + args.get_error().message);
    }
    function openSelectEmp(sUrl)
    {
        popupWindow=wopen(sUrl,"popupWindow",500,500);
    }
    function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue, openerCompanyClientID, openerCompanyValue)
    {
        $get(openerEmpClientID).value = openerEmpValue;
        $get(openerNameClientID).value = openerNameValue;
        $get(openerCompanyClientID).value = openerCompanyValue;
        closewindow(popupWindow);
    }
</script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Approver Lists Admin"></asp:Label><br />
            <br />
            <table width="100%">
              <tr>
                <td align="right" style="width: 80px">
                    <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click" ValidationGroup="emplist" />
                  </td>
              </tr>
            </table>
        </td>
        <td align="right">
        </td>
        <td align="center">
        </td>
    </tr>
</table>
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Holiday Definition" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Approver</asp:LinkButton><br />

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20"
        Width="100%" AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowDeleting="gvEmp_RowDeleting" 
            OnRowEditing="gvEmp_RowEditing" OnRowUpdating="gvEmp_RowUpdating" 
            DataKeyNames="ApproverID" onrowcommand="gvEmp_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="level 1 main approver" SortExpression="ApproverName" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAppr" runat="server" CssClass="ControlDefaults" Text='<%# Bind("ApproverName") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAppr" runat="server" Text="..." CssClass="ControlDefaults" />
                    <asp:RequiredFieldValidator
                        ID="rfvAppr" runat="server" ControlToValidate="txtAppr" ErrorMessage="Approver cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hfAppr" runat="server" Value='<%# Bind("ApproverID") %>' />
                        <asp:HiddenField ID="hfApprCompID" runat="server" Value='<%# Bind("ApprCompID") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAppr" runat="server" Text='<%# Bind("ApproverName") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level 1 back-up approver" SortExpression="Alternative1" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAlt1" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Alternative1Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAlt1" runat="server" Text="..." CssClass="ControlDefaults" />
                    <asp:ImageButton ID="ibClear1" runat="server" CommandName="clear1" ToolTip="Remove Level 1 back up approver" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure you want to remove Level 1 back up approver?' );" />
                    <asp:HiddenField ID="hfAlt1" runat="server" Value='<%# Bind("Alternative1") %>' />
                    <asp:HiddenField ID="hfAlt1CompID" runat="server" Value='<%# Bind("Alt1CompID") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAlt1" runat="server" Text='<%# Bind("Alternative1Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level 2 main approver" SortExpression="Alternative2" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAlt2" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Alternative2Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAlt2" runat="server" Text="..." CssClass="ControlDefaults" />
                    <asp:ImageButton ID="ibClear2" runat="server" CommandName="clear2" ToolTip="Remove Level 2 main approver" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure you want to remove Level 2 main approver?' );" />
                    <asp:HiddenField ID="hfAlt2" runat="server" Value='<%# Bind("Alternative2") %>' /> 
                    <asp:HiddenField ID="hfAlt2CompID" runat="server" Value='<%# Bind("Alt2CompID") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAlt2" runat="server" Text='<%# Bind("Alternative2Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level 2 back up approver" SortExpression="Alternative3" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtAlt3" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Alternative3Name") %>' Width="200px" ValidationGroup="Trans"></asp:TextBox><asp:Button
                        ID="btnAlt3" runat="server" Text="..." CssClass="ControlDefaults" />
                        <asp:ImageButton ID="ibClear3" runat="server" CommandName="clear3" ToolTip="Remove Level 2 back up approver" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure you want to remove Level 2 back up approver?' );" />
                    <asp:HiddenField ID="hfAlt3" runat="server" Value='<%# Bind("Alternative3") %>' /> 
                    <asp:HiddenField ID="hfAlt3CompID" runat="server" Value='<%# Bind("Alt3CompID") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label id="lblAlt3" runat="server" Text='<%# Bind("Alternative3Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemStyle Width="42px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                    <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <asp:HiddenField ID="hfIsAdd" runat="server" />
        <asp:HiddenField ID="hfErrorMsg" runat="server" />
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibSearch"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ibNew"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="lbNew"></asp:AsyncPostBackTrigger>
    </Triggers>
    
</asp:UpdatePanel>
</asp:Content>

