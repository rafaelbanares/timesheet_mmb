using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class OTRoundoff : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTRoundoffLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTRoundoffLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@IsBlankData", true), new SqlParameter("@DBname", _KioskSession.DB) });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvMain.DataBind();
        hfIsAdd.Value = "1";
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
            if (ibDelete != null)
                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTRoundoffDelete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@ID", key), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        TextBox txtLO = gvMain.Rows[e.RowIndex].FindControl("txtLO") as TextBox;
        TextBox txtUP = gvMain.Rows[e.RowIndex].FindControl("txtUP") as TextBox;
        TextBox txtRounded = gvMain.Rows[e.RowIndex].FindControl("txtRounded") as TextBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@LO", txtLO.Text));
        sqlparam.Add(new SqlParameter("@UP", txtUP.Text));
        sqlparam.Add(new SqlParameter("@Rounded", txtRounded.Text));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (hfIsAdd.Value == "0")
        {
            sqlparam.Add(new SqlParameter("@ID", key));
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTRoundoffUpdate", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTRoundoffInsert", sqlparam.ToArray());
        }

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
}