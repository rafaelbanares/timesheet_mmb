using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class AdminFileOT : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            populateScreenDefaults();
        }
    }
    private void populateScreenDefaults()
    {
        string today = Tools.ShortDate(DateTime.Now.ToShortDateString(), _KioskSession.DateFormat);
        lblFullname.Text = _Url["EmpName"];
        
        txtDateFiled.Text = today;
        txtOTDate.Text = today;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //... the filed overtime is approved since admin entered the filed OT
        OTAuthInsert("Approved");
    }
    private void OTAuthInsert(string status)
    {
        DateTime in1;
        DateTime out1;
        string othours = "0.00";

        in1 = DateTime.Parse(txtOTDate.Text + " " + txtin1.Text);
        out1 = DateTime.Parse(txtOTDate.Text + " " + txtout1.Text);

        if (in1 > out1)
            out1 = out1.AddHours(24);

        if (txtOTDate.Text.Length > 0 && txtin1.Text.Length > 0 && txtout1.Text.Length > 0)
        {
            TimeSpan ts = out1.Subtract(in1);
            othours = MMB.Core.Utils.TimeToDecimalRounded(ts.Hours, ts.Minutes);
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _Url["EmpID"]));

        sqlparam.Add(new SqlParameter("@OTDate", txtOTDate.Text));
        sqlparam.Add(new SqlParameter("@OTstart", in1));
        sqlparam.Add(new SqlParameter("@OTend", out1));

        string ErrMsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();
        if (ErrMsg != "")
        {
            lblError.Text = ErrMsg;
        }
        else
        {
            sqlparam.Add(new SqlParameter("@DateFiled", txtDateFiled.Text));
            sqlparam.Add(new SqlParameter("@ApprovedOT", othours));
            sqlparam.Add(new SqlParameter("@Reason", txtReason.Text));
            sqlparam.Add(new SqlParameter("@Status", status));
            sqlparam.Add(new SqlParameter("@AuthBy", ""));

            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
            sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthInsert", sqlparam.ToArray());

            SecureUrl url = new SecureUrl("BlankPageIframe.aspx?moduledesc=Filing of Overtime Module&message=overtime successfully added");
            Response.Redirect(url.ToString());
        }
    }
}
