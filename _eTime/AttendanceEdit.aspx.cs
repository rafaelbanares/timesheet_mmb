using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;
using System.Collections.Generic;

public partial class AttendanceEdit : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hfUpdate.Value = "";
        base.GetUserSession();
        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");
            body.Attributes.Add("onload", "load();");

            string empid = _Url["EmpID"].Trim();
            string date = _Url["Date"];

            PopulateShift();
            SetInitialValues(empid, date);

            ViewState["OldEmpID"] = txtEmployeeID.Text;
            ViewState["OldDate"] = txtDate.Text;
        }
    }
    protected void btnProcess_Click(object sender, EventArgs e)
    {
        Compute();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveAttendance();
    }
    private void PopulateShift()
    {
        //... populate Shift dropdown
        DataSet dsShift = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
            "SELECT ShiftCode, Description FROM " + _KioskSession.DB + ".dbo.Shift WHERE CompanyID = @CompanyID",
                        new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlShift.DataSource = dsShift.Tables[0];
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, "");
    }
    public void SetInitialValues(string empID, string date)
    {
        txtEmployeeID.Text = empID;
        txtDate.Text = date;

        PopulateAttendance();
    }

    private void PopulateAttendance()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceLoadByDate",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@EmployeeID", txtEmployeeID.Text), new SqlParameter("@Date", txtDate.Text), new SqlParameter("@DBName", _KioskSession.DB) });

        if (ds.Tables[0].Rows.Count > 0)
        {
            DataRow row = ds.Tables[0].Rows[0];

            lblFullname.Text = row["FullName"].ToString();
            lblOTCode.Text = row["OTcode"].ToString();

            txtin1.Text = Utils.GetTime(row["in1"].ToString());
            txtout1.Text = Utils.GetTime(row["out1"].ToString());
            txtin2.Text = Utils.GetTime(row["in2"].ToString());
            txtout2.Text = Utils.GetTime(row["out2"].ToString());

            txtAuthStart.Text = Utils.GetTime(row["OTAuthStart"].ToString());
            txtAuthEnd.Text = Utils.GetTime(row["OTAuthEnd"].ToString());

            txtReghrs.Text = row["Reghrs"].ToString();
            txtRegnd1.Text = row["Regnd1"].ToString();
            txtRegnd2.Text = row["Regnd2"].ToString();

            txtLate.Text = row["Late"].ToString();
            txtUT.Text = row["UT"].ToString();
            txtExcusedLate.Text = row["ExcusedLate"].ToString();
            txtExcusedUT.Text = row["ExcusedUT"].ToString();
            txtLeaveCode.Text = row["Leavecode"].ToString();
            txtLeaveDays.Text = row["Leave"].ToString();
            txtAbsentDays.Text = row["Absent"].ToString();

            txtExcessHrs.Text = row["Excesshrs"].ToString();
            txtFirst8.Text = row["First8"].ToString();
            txtGreater8.Text = row["Greater8"].ToString();
            txtOTND1.Text = row["NDot1"].ToString();
            txtOTND2.Text = row["NDot2"].ToString();
            txtNoPremium.Text = row["OTNoPremium"].ToString();

            txtError.Text = row["ErrorDesc"].ToString();
            txtRemarks.Text = row["Remarks"].ToString();

            chkValid.Checked = (bool)row["Valid"];

            ddlShift.SelectedValue = row["ShiftCode"].ToString();
            txtRestDay.Text = row["RestdayCode"].ToString();

            if ((bool)row["Posted"])
            {
                lblProcessed.Text = "Attendance is already posted";
                btnSave.Enabled = false;
                btnProcess.Enabled = false;
            }
            else
            {
                lblProcessed.Text = "unposted";
                btnSave.Enabled = true;
                btnProcess.Enabled = true;
            }
        }
        else
        {
            hfUpdate.Value = "No processed attendance for employee on the specified date, values reset";

            //... restore old value
            txtEmployeeID.Text = ViewState["OldEmpID"].ToString();
            txtDate.Text = ViewState["OldDate"].ToString();
        }

        SecureUrl secureLeave = new SecureUrl(
            string.Format("AdminFileLeave.aspx?EmpID={0}&EmpName={1}&Date={2}",
                                    txtEmployeeID.Text, lblFullname.Text, txtDate.Text));
        hlLeave.NavigateUrl = secureLeave.ToString();

        SecureUrl secureOT = new SecureUrl(
        string.Format("AdminOTbyEmploye.aspx?EmpID={0}&EmpName={1}&Date={2}",
                            txtEmployeeID.Text, lblFullname.Text, txtDate.Text));
        hlOT.NavigateUrl = secureOT.ToString();
    }

    private void Compute()
    {
        DBCredential dbc = new DBCredential(_ConnectionString,MainDB,_KioskSession.UID,_KioskSession.CompanyID,_KioskSession.DB);
        //AttendanceProcessBulk  process = new AttendanceProcessBulk(dbc);

        string in1 = Utils.TimeToDateTimeString(txtDate.Text, txtin1.Text, "");
        string out1 = Utils.TimeToDateTimeString(txtDate.Text, txtout1.Text, in1);
        string in2 = Utils.TimeToDateTimeString(txtDate.Text, txtin2.Text, out1);
        string out2 = Utils.TimeToDateTimeString(txtDate.Text, txtout2.Text, in2);

        //rsb - commented
        //Bns.SpecificProcess.ClientSpecificProcess process = new Bns.SpecificProcess.ClientSpecificProcess(dbc);
        //process.Specific = ConfigurationManager.AppSettings.Get("AttendanceProcess");

        //process.ExecuteOneTrans( txtEmployeeID.Text,
        //                         DateTime.Parse(txtDate.Text),
        //                         ddlShift.SelectedItem.Value,
        //                         txtRestDay.Text,
        //                         new string[] {in1,out1,in2,out2} );

        PopulateAttendance();
    }

    private void SaveAttendance()
    {
        string in1 = Utils.TimeToDateTimeString(txtDate.Text, txtin1.Text, "");
        string out1 = Utils.TimeToDateTimeString(txtDate.Text, txtout1.Text, in1);
        string in2 = Utils.TimeToDateTimeString(txtDate.Text, txtin2.Text, out1);
        string out2 = Utils.TimeToDateTimeString(txtDate.Text, txtout2.Text, in2);

        //... do not allow manual save for incomplete transactions, can be save if both have no value (absent)
        if ((in1 == "" && out1 != "") || (out1 == "" && in1 != ""))
        {
            hfUpdate.Value = "Cannot continue to manual save attendance, please complete the time transaction (in and out)";
            return;
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", txtEmployeeID.Text));
        sqlparam.Add(new SqlParameter("@Date", txtDate.Text));

        sqlparam.Add(new SqlParameter("@in1", Tools.DefaultNull(in1)));
        sqlparam.Add(new SqlParameter("@out1", Tools.DefaultNull(out1)));
        sqlparam.Add(new SqlParameter("@in2", Tools.DefaultNull(in2)));
        sqlparam.Add(new SqlParameter("@out2", Tools.DefaultNull(out2)));

        sqlparam.Add(new SqlParameter("@Late", Tools.DefaultZero(txtLate.Text)));
        sqlparam.Add(new SqlParameter("@UT", Tools.DefaultZero(txtUT.Text)));
        sqlparam.Add(new SqlParameter("@Absent", Tools.DefaultZero(txtAbsentDays.Text)));
        sqlparam.Add(new SqlParameter("@Leave", Tools.DefaultZero(txtLeaveDays.Text)));
        sqlparam.Add(new SqlParameter("@Excesshrs", Tools.DefaultZero(txtExcessHrs.Text)));

        sqlparam.Add(new SqlParameter("@Remarks", txtRemarks.Text));
        sqlparam.Add(new SqlParameter("@ErrorDesc", txtError.Text));
        sqlparam.Add(new SqlParameter("@Processed", true));

        sqlparam.Add(new SqlParameter("@Valid", chkValid.Checked));
        sqlparam.Add(new SqlParameter("@First8", txtFirst8.Text));
        sqlparam.Add(new SqlParameter("@Greater8", txtGreater8.Text));
        sqlparam.Add(new SqlParameter("@NDot1", Tools.DefaultZero(txtOTND1.Text)));
        sqlparam.Add(new SqlParameter("@NDot2", Tools.DefaultZero(txtOTND2.Text)));
        sqlparam.Add(new SqlParameter("@Reghrs", Tools.DefaultZero(txtReghrs.Text)));
        sqlparam.Add(new SqlParameter("@Regnd1", Tools.DefaultZero(txtRegnd1.Text)));
        sqlparam.Add(new SqlParameter("@Regnd2", Tools.DefaultZero(txtRegnd2.Text)));
        sqlparam.Add(new SqlParameter("@ExcusedLate", Tools.DefaultZero(txtExcusedLate.Text)));
        sqlparam.Add(new SqlParameter("@ExcusedUT", Tools.DefaultZero(txtExcusedUT.Text)));

        sqlparam.Add(new SqlParameter("@Edited", true));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        int result = SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceUpdate", sqlparam.ToArray());
        if (result == 0)
        {
            hfUpdate.Value = "Unable to save attendance";
        }
        hfUpdate.Value = "close";
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        PopulateAttendance();
    }
}
