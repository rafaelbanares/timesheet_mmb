
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Threading
Imports System.Diagnostics
Imports System.Collections

Namespace MMB.DataObject

    Public Class TransactionMgr

        Protected Sub New()

        End Sub

        Public Sub BeginTransaction()

            If hasRolledBack Then Throw New Exception("Transaction Rolledback")

            txCount = txCount + 1

        End Sub

        Public Sub CommitTransaction()

            If hasRolledBack Then Throw New Exception("Transaction Rolledback")

            txCount = txCount - 1

            If txCount = 0 Then
                Dim tx As Transaction

                For Each tx In Me.transactions.Values
                    tx.sqlTx.Commit()
                    tx.sqlTx.Dispose()
                Next

                Me.transactions.Clear()
            End If

        End Sub

        Public Sub RollbackTransaction()

            If Not hasRolledBack And txCount > 0 Then
                Dim tx As Transaction

                For Each tx In Me.transactions.Values
                    tx.sqlTx.Rollback()
                    tx.sqlTx.Dispose()

                    Dim cn As IDbConnection = tx.sqlTx.Connection

                    If Not cn Is Nothing AndAlso cn.State = ConnectionState.Open Then
                        cn.Close()
                    End If
                Next

                Me.transactions.Clear()
                Me.txCount = 0
            End If

        End Sub

        Public Sub Enlist(ByVal cmd As IDbCommand, ByVal Entity As BusinessEntity)

            If txCount = 0 Or Not Entity._notRecommendedConnection Is Nothing Then
                cmd.Connection = CreateSqlConnection(Entity)
            Else

                Dim connStr As String = Entity._config
                If Not Entity._raw = "" Then connStr = Entity._raw

                Dim tx As Transaction = CType(Me.transactions(connStr), Transaction)

                If tx Is Nothing Then
                    tx = New Transaction
                    Dim sqlConn As IDbConnection = CreateSqlConnection(Entity)

                    If Not _isolationLevel = IsolationLevel.Unspecified Then
                        tx.sqlTx = sqlConn.BeginTransaction(_isolationLevel)
                    Else
                        tx.sqlTx = sqlConn.BeginTransaction()
                    End If

                    Me.transactions(connStr) = tx
                End If

                cmd.Connection = tx.sqlTx.Connection
                cmd.Transaction = tx.sqlTx
            End If

        End Sub

        Public Sub DeEnlist(ByVal cmd As IDbCommand, ByVal Entity As BusinessEntity)

            If Not Entity._notRecommendedConnection Is Nothing Then
                cmd.Connection = Nothing
            Else
                ' We do nothing basically if there is a transaction going on
                If txCount = 0 Then
                    cmd.Connection.Dispose()
                End If
            End If

        End Sub

        Private Function CreateSqlConnection(ByVal entity As BusinessEntity) As IDbConnection

            Dim cn As IDbConnection

            If Not entity._notRecommendedConnection Is Nothing Then
                ' This is assumed to be open
                cn = entity._notRecommendedConnection
            Else
                cn = entity.CreateIDbConnection()
                
                If Not entity._raw = "" Then
                    cn.ConnectionString = entity._raw
                Else
                    cn.ConnectionString = ConfigurationManager.AppSettings(entity._config)
                End If

                '- rsb -
                cn.ConnectionString = DecryptPass(cn.ConnectionString)
                '--o--

                cn.Open()
            End If

            Return cn

        End Function

        '- rsb -
        Private Function DecryptPass(ByVal connStr As String) As String
            Dim upass As String = ""
            If connStr.ToUpper.IndexOf("PWD=") >= 0 Then
                upass = connStr.Substring(connStr.ToUpper.IndexOf("PWD=") + 4)
            Else
                upass = connStr.Substring(connStr.ToUpper.IndexOf("PASSWORD=") + 9)
            End If
            If (upass.IndexOf(";") >= 0) Then
                upass = upass.Substring(0, upass.IndexOf(";"))
            End If
            Return connStr.Replace(upass, Decrypt(upass))
        End Function

        Public Function Decrypt(ByVal Data As String) As String
            Dim dEC_data() As Byte = Convert.FromBase64String(Data)
            Dim dEC_Str As String = System.Text.ASCIIEncoding.ASCII.GetString(dEC_data)
            Decrypt = dEC_Str
        End Function
        '--o--

        ' We might have multple transactions going at the same time.
        ' There's one per connnection string
        Private Class Transaction
            Public sqlTx As IDbTransaction = Nothing
        End Class

        Private transactions As New Hashtable
        Private txCount As Integer = 0
        Private hasRolledBack As Boolean = False

#Region "Shared"
        Public Shared Function ThreadTransactionMgr() As TransactionMgr

            Dim txMgr As TransactionMgr = Nothing

            Dim obj As Object = Thread.GetData(txMgrSlot)

            If Not obj Is Nothing Then
                txMgr = CType(obj, TransactionMgr)
            Else
                txMgr = New TransactionMgr
                Thread.SetData(txMgrSlot, txMgr)
            End If

            Return txMgr

        End Function

        Public Shared Sub ThreadTransactionMgrReset()

            Dim txMgr As TransactionMgr = TransactionMgr.ThreadTransactionMgr()

            Try
                If txMgr.txCount > 0 AndAlso txMgr.hasRolledBack = False Then
                    txMgr.RollbackTransaction()
                End If
            Catch ex As Exception
                ' At this point we're not worried about a failure
            End Try

            Thread.SetData(txMgrSlot, Nothing)
        End Sub

        Public Shared Property IsolationLevel() As IsolationLevel
            Get
                Return _isolationLevel
            End Get
            Set(ByVal Value As IsolationLevel)
                _isolationLevel = Value
            End Set
        End Property

        Private Shared _isolationLevel As IsolationLevel = IsolationLevel.Unspecified
        Private Shared txMgrSlot As LocalDataStoreSlot = Thread.AllocateDataSlot()
#End Region

    End Class

End Namespace
