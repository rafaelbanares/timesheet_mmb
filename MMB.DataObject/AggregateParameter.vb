Imports System.Collections
Imports System.Data

Namespace MMB.DataObject

    Public Class AggregateParameter

        Public Enum Func
            Avg = 1
            Count
            Max
            Min
            StdDev
            Var
            Sum
        End Enum
        Public Sub New(ByVal column As String, ByVal param As IDataParameter)
            Me._column = column
            Me._alias = column
            Me._param = param
            Me._function = AggregateParameter.Func.Sum
        End Sub
        Public ReadOnly Property IsDirty() As Boolean
            Get
                Return _isDirty
            End Get
        End Property

        Public ReadOnly Property Column() As String
            Get
                Return _column
            End Get
        End Property

        Public ReadOnly Property Param() As IDataParameter
            Get
                Return _param
            End Get
        End Property
        Public Property Value() As Object
            Get
                Return _value
            End Get
            Set(ByVal TheValue As Object)
                _value = TheValue
                _isDirty = True
            End Set
        End Property
        Public Property [Function]() As Func
            Get
                Return _function
            End Get
            Set(ByVal Value As Func)
                _function = Value
                _isDirty = True
            End Set
        End Property
        Public Property [Alias]() As String
            Get
                Return _alias
            End Get
            Set(ByVal Value As String)
                _alias = Value
                _isDirty = True
            End Set
        End Property

        Private _value As Object = Nothing
        Private _param As IDataParameter
        Private _column As String
        Private _function As Func = AggregateParameter.Func.Sum
        Private _alias As String = String.Empty
        Private _isDirty As Boolean = False

    End Class

End Namespace
