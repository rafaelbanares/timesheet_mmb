﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BNS.TK.Business;

namespace BNS.TK.UnitTest
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestGracePeriod()
        {
            TimeEntry projectedShift = new TimeEntry();
            TimeEntry payable = new TimeEntry();

            ///preProjected shift:	08:30 - 12:30 - 01:30 - 05:30
            ///prePayable:          08:45 - 12:30 - 01:30 - 05:30

            projectedShift.SetTimeEntry("12/01/2011", "08:30", "12:30", "13:30", "17:30");
            payable.SetTimeEntry("12/01/2011", "08:45", "12:30", "13:30", "17:30");

            ///Projected shift:	    08:45 - 12:45 - 01:45 - 05:45
            ///Payable:             08:45 - 12:45 - 01:45 - 05:30

            ProcessAttend proc = new ProcessAttend("LOWE", "211002");
            //proc.ApplyGracePeriod(projectedShift, payable, 30);

            Assert.AreEqual(projectedShift.In1, DateTime.Parse("12/01/2011 08:45"));
            Assert.AreEqual(projectedShift.Out1, DateTime.Parse("12/01/2011 12:45"));
            Assert.AreEqual(projectedShift.In2, DateTime.Parse("12/01/2011 13:45"));
            Assert.AreEqual(projectedShift.In2, DateTime.Parse("12/01/2011 17:45"));

            Assert.AreEqual(payable.In1, DateTime.Parse("12/01/2011 08:45"));
            Assert.AreEqual(payable.Out1, DateTime.Parse("12/01/2011 12:45"));
            Assert.AreEqual(payable.In2, DateTime.Parse("12/01/2011 13:45"));
            Assert.AreEqual(payable.In2, DateTime.Parse("12/01/2011 17:30"));
        }
    }
}
