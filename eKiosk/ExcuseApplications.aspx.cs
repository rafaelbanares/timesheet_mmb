﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;
using BNS.TK.Business;

public partial class ExcuseApplications : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (!IsPostBack)
        {
            
            PopulateYear();
            BindGrid();
        }
    }

    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }
    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    private void BindGrid()
    {
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.DataBind();
    }

    private DataSet GetData()
    {
        string sdate = "01/01/" + ddlYear.SelectedValue;
        string edate = "12/31/" + ddlYear.SelectedValue;

        ExcusedTardyUT xqs = new ExcusedTardyUT();
        string sql = "SELECT * FROM ExcusedTardyUT WHERE CompanyID = {0} and EmployeeID = {1} and ExcuseDate between {2} and {3} ORDER BY ExcuseDate ";

        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID, sdate.ToDate(), edate.ToDate() };
        xqs.DynamicQuery(sql, parameters);

        return xqs.ToDataSet();
    }


    protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmployees.EditIndex)
            {
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                ibEdit.Visible = status.IsNullOrEmpty() || status.IsEqual(BNS.TK.Business.TKWorkFlow.Status.ForApproval);
            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }

    }
    protected void gvEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.PageIndex = e.NewPageIndex;
        gvEmployees.DataBind();
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        hfIsAdd.Value = "";
        ImageButton ibtn = (ImageButton)sender;
        DateTime date = (ibtn.Parent.Parent.FindControl("hfDate") as HiddenField).Value.ToDate();

        ExcusedTardyUT xqs = new ExcusedTardyUT();
        if (xqs.LoadByPrimaryKey(_KioskSession.CompanyID, _KioskSession.EmployeeID, date))
        {
            txtOBdate.Text = xqs.s_ExcuseDate;
            hdAppDate.Value = xqs.s_ExcuseDate;
            txtStartDate.Text = xqs.StartDate.ToString("MM/dd/yyyy");
            txtStartTime.Text = xqs.StartDate.ToString("hh:mm tt");
            txtEndDate.Text = xqs.EndDate.ToString("MM/dd/yyyy");
            txtEndTime.Text = xqs.EndDate.ToString("hh:mm tt");
            txtRemarks.Text = xqs.Remarks;

            txtOBdate.Enabled = false;
            txtStartDate.Enabled = false;
            txtEndDate.Enabled = false;

            mpe.Show();
        }
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    private bool ValidateEditInputs()
    {
        DateRange dr = new DateRange();
        if (!dr.ValidatePeriod(txtStartDate.Text + " " + txtStartTime.Text, txtEndDate.Text + " " + txtEndTime.Text))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('" + dr.message + "');", true);
            return false;
        }
        if (txtStartDate.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter start date');", true);
            return false;
        }
        if (txtStartTime.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter start time');", true);
            return false;
        }

        if (txtEndDate.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter end date');", true);
            return false;
        }
        if (txtEndTime.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter end time');", true);
            return false;
        }

        if (txtRemarks.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter remarks');", true);
            return false;
        }

        if (hfIsAdd.Value == "1"  && txtOBdate.Text.ToDate() != DateTime.Today)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('You can only file Excused UT dated today.');", true);            
            return false;
        }

        var dt = GetData().Tables[0];
        var sumObject = dt.AsEnumerable()
            .Sum(e => e.Field<DateTime>("EndDate")
            .Subtract(e.Field<DateTime>("StartDate")).Minutes).ToString();

        var totalHours = Convert.ToInt32(sumObject);
        var startDate = Convert.ToDateTime(txtStartDate.Text + " " + txtStartTime.Text);
        var endDate = Convert.ToDateTime(txtEndDate.Text + " " + txtEndTime.Text);
        totalHours += endDate.Subtract(startDate).Minutes;

        if (totalHours > 60)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('You can only file a total of 1 hour per month.');", true);
            return false;
        }

        return true;
    }

    protected void btnEditSave_Click(object sender, EventArgs e)
    {
        if (!ValidateEditInputs())
        {
            mpe.Show();
            return;
        }

        mpe.Hide();

        try
        {
            string empid = _KioskSession.EmployeeID;            
            ExcusedTardyUT xqs = new ExcusedTardyUT();

            if (hfIsAdd.Value == "1")
            {
                xqs.AddNew();
                xqs.CompanyID = _KioskSession.CompanyID;
                xqs.WholeDay = false;   //TODO
                xqs.EmployeeID = empid;
                xqs.s_ExcuseDate = txtOBdate.Text;
                xqs.s_StartDate = txtStartDate.Text + " " + txtStartTime.Text;
                xqs.s_EndDate = txtEndDate.Text + " " + txtEndTime.Text;
                xqs.Status = TKWorkFlow.Status.ForApproval;
                xqs.Stage = 0;
                xqs.CreatedBy = _KioskSession.EmployeeID;
                xqs.CreatedDate = DateTime.Now;
            }
            else
            {
                if (xqs.LoadByPrimaryKey(_KioskSession.CompanyID, empid, DateTime.Parse(hdAppDate.Value)))
                {
                    //time is the only portion that is editable
                    xqs.s_StartDate = xqs.StartDate.Date.ToString("MM/dd/yyyy") + " " + txtStartTime.Text;
                    xqs.s_EndDate = xqs.EndDate.Date.ToString("MM/dd/yyyy") + " " + txtEndTime.Text;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('Data not found.');", true);
                    return;
                }
            }

            xqs.Remarks = txtRemarks.Text;
            xqs.LastUpdBy = _KioskSession.EmployeeID;
            xqs.LastUpdDate = DateTime.Now;
            //save
            xqs.Save();


            //send email
            if (hfIsAdd.Value == "1")
            {
                //Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos for approval", "You have a pending excused tardy for approval.");
            }

            //ReprocessAttendance(date.ToDate());
            BindGrid(); //remove this if reprocessed

            //ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsmsg", "alert('Save Successful.');", true);

        }
        catch (SqlException sqex)
        {
            if (sqex.Number == 2627)  //'PK violation
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('You already have application in this date. Please check.');", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('" + sqex.Message.Replace("'", "") + "');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('" + ex.Message.Replace("'", "") + "');", true);
        }
        
    }
    protected void lnkApplyExcuse_Click(object sender, EventArgs e)
    {
        hfIsAdd.Value = "1";
        txtOBdate.Text = "";
        txtStartDate.Text = "";
        txtStartTime.Text = "";
        txtEndDate.Text = "";
        txtEndTime.Text = "";
        txtRemarks.Text = "";

        txtOBdate.Enabled = true;
        txtStartDate.Enabled = true;
        txtEndDate.Enabled = true;

        mpe.Show();
    }

    protected void txtOBdate_TextChanged(object sender, EventArgs e)
    {
        mpe.Show();
        if (txtOBdate.Text.IsFilled())
        {
            txtStartDate.Text = txtOBdate.Text;
            txtEndDate.Text = txtOBdate.Text;
            txtStartTime.Focus();
        }
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}