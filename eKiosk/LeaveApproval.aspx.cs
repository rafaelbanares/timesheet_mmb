//rsb: currently not in used. not called by any module
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class Attendance_LeaveApproval : KioskPageUI
{
    string approverno;
    string stage;

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        //... must check if right query string is passed
        if (_Url["approverno"] == "")
            Response.Redirect("LoginEmployee.aspx");
        else
        {
            approverno = _Url["approverno"];
            stage = (approverno == "1" ? "1" : "2");
        }

        if (!IsPostBack)
        {
            populateScreenDefaults();
        }
    }

    private void populateScreenDefaults()
    {
        string id = _Url["LeaveID"];

        DataRow drleave = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveLoadByLeaveID",
                        new SqlParameter[] { 
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@ID", id )                        
                        }).Tables[0].Rows[0];

        hfLeaveID.Value = id;
        hfEmployeeID.Value = drleave["EmployeeID"].ToString();
        lblFullname.Text = drleave["FullName"].ToString();
        txtDateFiled.Text = Tools.ShortDate(drleave["DateFiled"].ToString(), _KioskSession.DateFormat);
        txtDateStart.Text = drleave["StartDate"].ToString();
        txtDateEnd.Text = drleave["EndDate"].ToString();
        txtLeaveCode.Text = drleave["Code"].ToString();
        txtLeaveDays.Text = drleave["Days"].ToString();
        txtReason.Text = drleave["Reason"].ToString();
    }

    private void LeaveTransUpdate(string status)
    {
        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", hfEmployeeID.Value));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@ID", hfLeaveID.Value));
        sqlparam.Add(new SqlParameter("@StartDate", txtDateStart.Text));
        sqlparam.Add(new SqlParameter("@EndDate", txtDateEnd.Text));
        sqlparam.Add(new SqlParameter("@Days", txtLeaveDays.Text));
        sqlparam.Add(new SqlParameter("@Reason", txtReason.Text));
        sqlparam.Add(new SqlParameter("@Code", txtLeaveCode.Text));
        sqlparam.Add(new SqlParameter("@Status", status));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveUpdate", sqlparam.ToArray());
    }

    private void GoBack()
    {
        Response.Redirect("LeaveApprover.aspx");
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        LeaveTransUpdate("Approved");
        GoBack();
    }
    protected void btnDeclined_Click(object sender, EventArgs e)
    {
        LeaveTransUpdate("Declined");
        GoBack();
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        GoBack();
    }

}
