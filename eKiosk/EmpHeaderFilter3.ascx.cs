﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class EmpHeaderFilter3 : UserControlUI
{
    public string Display { get {return ddlDisplay.SelectedItem.Value;} }
    public string Search { get { return txtSearch.Text; } }
    public string DateFrom { get { return txtPrdFrom.Text; } }
    public string DateTo { get { return txtPrdTo.Text; } }
    
    public event EventHandler Refresh_Click;

    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {
        this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
        this.Load += new System.EventHandler(this.Page_Load);
        if (!Page.IsPostBack)
        {
            //PopulateYear();
            txtPrdFrom.Text = DateTime.Today.AddDays(-30).ToString("MM/dd/yyyy");
            txtPrdTo.Text = DateTime.Today.ToString("MM/dd/yyyy");
        }
    }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!Page.IsPostBack)
        {            
            
        }
    }

    protected void ddlDisplay_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnRefresh_Click(sender, e);
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        if (txtPrdFrom.Text.IsNullOrEmpty() || txtPrdTo.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertmsg", "alert('Please inpute date.');", true);
            return;
        }
        if (!(txtPrdFrom.Text.IsDate() && txtPrdTo.Text.IsDate()))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertmsg", "alert('Invalid date.');", true);
            return;
        }

        if (Refresh_Click != null)
        {
            Refresh_Click(sender, e);
        }
    }
}
