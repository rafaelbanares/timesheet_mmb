﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using BNS.Framework.Encryption;
using BNS.TK.Entities;
using BNS.TK.Business;

public partial class TimeMachine2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litScript.Text = "";
        if (!IsPostBack)
        {

        }
    }

    protected void bOK_Click(object sender, EventArgs e)
    {
        TimeInOut();
    }


    protected void TimeInOut()
    {
        string companyid = Helper.Config.DefaultCompanyID();
        
        Employee emp = new Employee();
        emp.Query.AddResultColumn(Employee.ColumnNames.FullName);
        emp.Query.AddResultColumn(Employee.ColumnNames.Password);
        emp.Where.CompanyID.Value = companyid;
        emp.Where.EmployeeID.Value = tbUID.Text;
        emp.Query.Load("AND");
        if (emp.RowCount == 0)
        {
            lblError.Text = "Employee ID not found.";
            return;
        }

        string dbpassword = emp.s_Password.Trim();
        if (dbpassword.Length > 0 && tbPWD.Text.Trim().Length == 0)
        {
            lblError.Text = "- Password is required";
            return;
        }
        if ((dbpassword.Length == 0 && tbPWD.Text.Length > 0) ||
            (dbpassword.Length > 0 && Crypto.ActionDecrypt(dbpassword) != tbPWD.Text))
        {
            lblError.Text = "Invalid password.";
            return;
        }

        lblError.Text = "";
        lblMessage.Text = emp.FullName;

        try
        {
            BNS.TK.Entities.TimeTrans timeTrans = new BNS.TK.Entities.TimeTrans();
            timeTrans.AddNew();
            timeTrans.CompanyID = companyid;
            timeTrans.EmployeeID = tbUID.Text;
            timeTrans.TimeIO = DateTime.Now;
            timeTrans.IO = hdIO.Value;
            timeTrans.Station = " ";
            timeTrans.IOCode = " ";
            timeTrans.Posted = false;
            timeTrans.Save();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            return;
        }

        ShowEntries();

    }

    private void ShowEntries()
    {
        string companyid = Helper.Config.DefaultCompanyID();        

        string sql = "select TimeIO, [IO] " +
                    "from TimeTrans " +
                    "where companyID = {0} and " +
                    "	EmployeeID = {1} and " +
                    "	Convert(varchar(10), TimeIO, 101) = {2}";
        object[] parameters = { companyid, tbUID.Text, DateTime.Today.ToString("MM/dd/yyyy") };

        BNS.TK.Entities.TimeTrans timeTrans = new BNS.TK.Entities.TimeTrans();
        timeTrans.DynamicQuery(sql, parameters);
        //bind
        gvwEntries.DataSource = timeTrans.DefaultView;
        gvwEntries.DataBind();

        gvwEntries.Visible = (timeTrans.RowCount > 0);
        
    }
        
}