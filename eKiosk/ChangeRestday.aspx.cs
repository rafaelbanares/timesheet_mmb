﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;
using BNS.TK.Entities;
using BNS.TK.Business;


public partial class ChangeRestday : KioskPageUI
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        base.GetUserSession();
        if (!IsPostBack)
        {
            PopulateYear();
            BindGrid();
        }
    }

    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    private void BindGrid(bool addnew = false)
    {       
        string sdate = "01/01/" + ddlYear.SelectedValue;
        string edate = "12/31/" + ddlYear.SelectedValue;

        BNS.TK.Entities.ChangeShift cs = new BNS.TK.Entities.ChangeShift();
        string sql = "SELECT " +
                        "	cr.ID, " +
                        "	cr.StartDate, " +
                        "	cr.EndDate, " +
                        "	cr.RestDayCode, " +
                        "	cr.Status " +
                        "FROM ChangeRestDay cr " +
                        "WHERE  " +
                        "	cr.CompanyID = {0} AND  " +
                        "	cr.EmployeeID = {1} AND " +
                        "   (cr.StartDate <= {3} and cr.EndDate >= {2}) " +
                        "ORDER BY EndDate DESC ";
        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID, sdate.ToDate(), edate.ToDate() };
        cs.DynamicQuery(sql, parameters);

        if (addnew)
        {
            cs.AddNew();
            gvMain.EditIndex = cs.RowCount - 1;
        }

        gvMain.DataSource = cs.DefaultView;
        gvMain.DataBind();

        hfIsAdd.Value = (addnew) ? "1" : "0";
    }

    
    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string RestDaycode = DataBinder.Eval(e.Row.DataItem, "RestDayCode").ToString();
            if (e.Row.RowIndex != gvMain.EditIndex)
            {
                ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;

                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().TrimEnd().ToUpper();
                ibDelete.Visible = status.IsEqual(TKWorkFlow.Status.ForApproval);
                ibEdit.Visible = status.IsEqual(TKWorkFlow.Status.ForApproval);

                if (ibDelete != null)
                {
                    ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
                    Literal litDescription = e.Row.FindControl("litDescription") as Literal;
                    litDescription.Text = MMB.Core.DateHelper.DecodeDOW(RestDaycode);
                }
            }
            else
            {
                //edit mode
                CheckBoxList cblRestDay = e.Row.FindControl("cblRestDay") as CheckBoxList;
                //RestDaycode  1,7
                for (int i = 0; i < cblRestDay.Items.Count; i++)
                {
                    cblRestDay.Items[i].Selected = i == 0 ? RestDaycode.Contains("7") : RestDaycode.Contains(i.ToString());
                }
            }
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        BNS.TK.Entities.ChangeRestDay cs = new BNS.TK.Entities.ChangeRestDay();
        if (cs.LoadByPrimaryKey(key.ToInt()))
        {
            cs.MarkAsDeleted();
            cs.Save();
        }

        gvMain.EditIndex = -1;
        BindGrid(false);



    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {       
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        TextBox txtStart = gvMain.Rows[e.RowIndex].FindControl("txtStart") as TextBox;
        TextBox txtEnd = gvMain.Rows[e.RowIndex].FindControl("txtEnd") as TextBox;

        StringBuilder sb = new StringBuilder();
        CheckBoxList cblRestDay = gvMain.Rows[e.RowIndex].FindControl("cblRestDay") as CheckBoxList;
        for (int i = 0; i < cblRestDay.Items.Count; i++)
        {
            if (cblRestDay.Items[i].Selected)
                sb.Append((i == 0 ? 7 : i).ToString());
        }

        BNS.TK.Entities.ChangeRestDay cs = new BNS.TK.Entities.ChangeRestDay();
        if (hfIsAdd.Value == "1")
        {
            cs.AddNew();
            cs.CompanyID = _KioskSession.CompanyID;
            cs.EmployeeID = _KioskSession.EmployeeID;
            
            cs.Status = TKWorkFlow.Status.ForApproval;
            cs.Stage = 0;
            cs.CreatedBy = _KioskSession.UID;
            cs.CreatedDate = DateTime.Now;
        }
        else
        {
            cs.LoadByPrimaryKey(key.ToInteger());
        }
        cs.StartDate = txtStart.Text.ToDate();
        cs.EndDate = txtEnd.Text.ToDate();
        cs.RestDayCode = sb.ToString();
        cs.LastUpdBy = _KioskSession.UID;
        cs.LastUpdDate = DateTime.Now;
        cs.Save();

        //send email
        if (hfIsAdd.Value == "1")
        {
            //Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos for approval", "You have a pending change of restday for approval.");
        }

        gvMain.EditIndex = -1;
        BindGrid(false);
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}