<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="eKioskHome.aspx.cs" Inherits="eKioskHome" Title="eKiosk TimeKeeping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">    
    <script type="text/javascript" src="css/common_uno.js"></script>    
    <br />
    <hr />
    <br />        
    <table style="width:100%">
        <tr class="DataGridHeaderStyle">
            <td>Welcome to Employee Self-Service Kiosk</td>
            <td>Time-in</td>
            <td>Time-out</td>
        </tr>
        <tr>
            <td>
                Attendance Date: <asp:Label ID="lblAttendanceDate" runat="server"></asp:Label>
            </td>
            <td><asp:Label ID="lblTimeIn" runat="server" Text="(No time-in)"></asp:Label></td>
            <td><asp:Label ID="lblTimeOut" runat="server" Text="(still on-work)"></asp:Label></td>
        </tr>
        <tr>
            <td>Attendance Shift: <asp:Label ID="lblShift" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>        
    </table>
    <br />
    <br />
    <table style="width:100%" cellpadding="0" cellspacing="0">
        <tr  class="DataGridHeaderStyle">
            <td>Locking</td>
            <td></td>
        </tr>
        <tr>
            <td style="width:30%" >Personal transactions remaining time:</td>
            <td><asp:Label ID="lblEmpLockStat" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td><asp:Label ID="lblApprLockLabel" runat="server" Text="Approval transactions remaining time:"></asp:Label></td>
            <td><asp:Label ID="lblApprLockStat" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
    </table>
    <br />
    <br />
    <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle" Width="100%" DataKeyNames="SummaryNo" OnRowDataBound="gvMain_RowDataBound">
        <Columns>
            <asp:BoundField DataField="Description" ItemStyle-Wrap="false" />
            <asp:TemplateField HeaderText="Attendance">
                <ItemTemplate>
                    <asp:HyperLink ID="hlAttend" runat="server" Text='<%# Bind("Attend") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Leaves">
                <ItemTemplate>
                    <asp:HyperLink ID="hlLeave" runat="server" Text='<%# Bind("Leave") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Overtime">
                <ItemTemplate>
                    <asp:HyperLink ID="hlOT" runat="server" Text='<%# Bind("OT") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
<%--        <asp:TemplateField HeaderText="Change Shift">
                <ItemTemplate>
                    <asp:HyperLink ID="hlChShft" runat="server" Text='<%# Bind("CHSHIFT") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="12%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Change Restday">
                <ItemTemplate>
                    <asp:HyperLink ID="hlChRstd" runat="server" Text='<%# Bind("CHRSTD") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="12%" />
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="OB">
                <ItemTemplate>
                    <asp:HyperLink ID="hlOB" runat="server" Text='<%# Bind("OB") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="10%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Excused<br>UT">
                <ItemTemplate>
                    <asp:HyperLink ID="hlExcused" runat="server" Text='<%# Bind("Excused") %>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle Width="10%" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>
    <br />
    
    <asp:Label ID="lblExpiry" runat="server" Text="Your password will expire on {0}. You have {1} days left to change your password"></asp:Label>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    
    <asp:Panel ID="pnlLocked" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
            <div><br />
                <table style="width:100%;">                    
                    <tr>                        
                        <td style="width:100%; text-align:center">
                        Kronos is currently locked by your administrator.</td>
                    </tr>
                    <tr>
                        <td style="width:100%; text-align:center">
                        <br />                            
                            <asp:Button ID="btnLockedOk" runat="server" Text="Ok" 
                                onclick="btnLockedOk_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="mpe2" runat="server"
          TargetControlId="lbLocked" 
          PopupControlID="pnlLocked"
          BackgroundCssClass="modalBackground"  />
          <asp:LinkButton ID="lbLocked" runat="server" style="display:none">show modal</asp:LinkButton>
          &nbsp;
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

