﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DefaultDate : System.Web.UI.UserControl
{
    public event EventHandler BubbleClick;

    protected void OnBubbleClick(EventArgs e)
    {
        if (BubbleClick != null)
        {
            BubbleClick(this, e);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        OnBubbleClick(e);
    }

    private void SetDefault()
    {
        if (txtPrdFrom.Text.TrimEnd() == "" && txtPrdTo.Text.TrimEnd() == "") 
        {
            txtPrdFrom.Text = DateTime.Today.AddDays(-30).ToString("MM/dd/yyyy");
            txtPrdTo.Text = DateTime.Today.ToString("MM/dd/yyyy");
        }  
    }

    public string PerdioFrom
    {
        get { return txtPrdFrom.Text; }
        set { txtPrdFrom.Text = value; }
    }
    public string PerdioTo
    {
        get { return txtPrdTo.Text; }
        set { txtPrdTo.Text = value; }
    }
}
