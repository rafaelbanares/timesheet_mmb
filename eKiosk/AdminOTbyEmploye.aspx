<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="AdminOTbyEmploye.aspx.cs" Inherits="AdminOTbyEmploye" Title="Overtime Authorization Filling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <table width="100%">
        <tr>
            <td>
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Overtime Form"></asp:Label><br />
                <br />
                <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname S."></asp:Label></td>
            <td>
                <table>
                    <tr>
                        <td>
                            Filter From:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                OnClientClick="return false;" ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                                ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                OnClientClick="return false;" ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                                ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                        <td align="left">
                            <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                                ToolTip="Go" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="Add overtime" /><asp:LinkButton ID="lbNew" runat="server"
            CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">Add overtime</asp:LinkButton><br />

    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False"
                CssClass="DataGridStyle" OnRowDataBound="gvEmp_RowDataBound" PageSize="16" Width="100%" OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowDeleting="gvEmp_RowDeleting" OnRowEditing="gvEmp_RowEditing" OnRowUpdating="gvEmp_RowUpdating" DataKeyNames="ID">
                <Columns>
                    <asp:TemplateField HeaderText="OT date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtOTDate" runat="server" CssClass="ControlDefaults" Text='<%# Bind("OTDate", "{0:MM/dd/yyyy}") %>'
                                Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtOTDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditExtender ID="meeOTDate" runat="server"
                                    ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"
                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtOTDate">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleOTDate" runat="server" PopupButtonID="ibtxtOTDate"
                                TargetControlID="txtOTDate">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litOTDate" runat="server" Text='<%# Bind("OTDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="OT start">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbOTStart" runat="server" Text='<%# Bind("OTStart", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox><ajaxToolkit:MaskedEditValidator
                                ID="mevOTStart" runat="server" ControlExtender="meOTStart" ControlToValidate="tbOTStart"
                                Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                                ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                            <ajaxToolkit:MaskedEditExtender ID="meOTStart" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                                Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                OnInvalidCssClass="MaskedEditError" TargetControlID="tbOTStart">
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lOTStart" runat="server" Text='<%# Bind("OTStart", "{0:hh:mm tt}") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="OT end">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbOTEnd" runat="server" Text='<%# Bind("OTEnd", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox><ajaxToolkit:MaskedEditValidator
                                ID="mevOTEnd" runat="server" ControlExtender="meOTEnd" ControlToValidate="tbOTEnd"
                                Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                                ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator><ajaxToolkit:MaskedEditExtender
                                    ID="meOTEnd" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True" Mask="99:99"
                                    MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                    OnInvalidCssClass="MaskedEditError" TargetControlID="tbOTEnd">
                                </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lOTEnd" runat="server" Text='<%# Bind("OTEnd", "{0:hh:mm tt}") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Computed&lt;br&gt;hours">
                        <EditItemTemplate>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Literal ID="litApprOT" runat="server" Text='<%# Bind("ApprovedOT") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Reason">
                        <ItemStyle VerticalAlign="Top" Width="40%" />
                        <EditItemTemplate>
                            <asp:TextBox ID="tbReason" runat="server" Text='<%# Bind("Reason") %>' Width="90%" MaxLength="100"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle VerticalAlign="Top" Width="42px" />
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF"
                                ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                    </asp:TemplateField>                    
                </Columns>                
                <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
                <HeaderStyle CssClass="DataGridHeaderStyle" />
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
            <asp:AsyncPostBackTrigger ControlID="ibGo" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:LinkButton ID="lbBack" runat="server" OnClick="lbBack_Click" ValidationGroup="none">Back to correct attendance</asp:LinkButton><br />
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

