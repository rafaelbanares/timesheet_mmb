using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;
using BNS.TK.Business;

public partial class OTAuthList : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            PopulateYear();
            BindGrid(true);
            ddlStatus.SelectedIndex = 1;   //default to for approval
        }
    }


    #region BindGrid
    private void BindGrid(bool retainsorting)
    {
        gvLAppr.DataSource = SortDataTable(GetOTAuthForApproval().Tables[0], retainsorting);
        gvLAppr.DataBind();
    }
    #endregion

    #region GetOTAuthForApproval
    private DataSet GetOTAuthForApproval()
    {
        string sdate = "01/01/" + ddlYear.SelectedValue;
        string edate = "12/31/" + ddlYear.SelectedValue;

        string sql = "SELECT " +
                        "   a.ID, " +
                        "	DateFiled, " +
                        "	OTDate, " +
                        "	OTstart, " +
                        "	OTend, " +
                        "	ApprovedOT, " +
                        "	Reason = b.[Description], " +
                        "	Status, " +
                        "	Stage " +
                        "FROM OTAuthorization a LEFT JOIN OTReason b " +
                        "ON a.CompanyID = b.CompanyID and " +
                        "   a.Reason = b.ID " +
                        "WHERE " +
                        "	a.CompanyID = {0}	AND " +
                        "	a.EmployeeID = {1} AND " +
                        "	(a.OTDate BETWEEN {2} AND {3}) AND " +
                        "	({4}='' or a.[Status] = {4}) " +
                        "ORDER BY a.OTDate DESC ";
        object[] parameters = { _KioskSession.CompanyID,
                                _KioskSession.EmployeeID,
                                sdate.ToDate(),
                                edate.ToDate(),
                                ddlStatus.SelectedItem.Value};
        BNS.TK.Entities.OTAuthorization auth = new BNS.TK.Entities.OTAuthorization();
        auth.DynamicQuery(sql, parameters);
        return auth.ToDataSet();
    }

    #endregion


    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = sender as ImageButton;
        HiddenField hfID = ib.Parent.Parent.FindControl("hfID") as HiddenField;

        //SecureUrl su = new SecureUrl("OTAuthApproval.aspx?OTAuthID=" + hfID.Value);
        SecureUrl su = new SecureUrl("OTAuthEmployee.aspx?id=" + hfID.Value);
        Response.Redirect(su.ToString());
    }

    protected void gvLAppr_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvLAppr.PageIndex;
        BindGrid(false);

        gvLAppr.PageIndex = pageIndex;
    }
    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }


    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void lnkApplyOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTAuthEmployee.aspx", false);
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(true);
    }

    protected void gvLAppr_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvLAppr.EditIndex)
            {
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                int stage = DataBinder.Eval(e.Row.DataItem, "Stage").ToString().ToInt();
                string date = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "OTDate").ToString(), _KioskSession.DateFormat);

                ibEdit.Visible = status.IsNullOrEmpty() || (status.IsEqual(BNS.TK.Business.TKWorkFlow.Status.ForApproval) && stage == 0);

                //check if locked
                if (DateLockedForEmployee(Convert.ToDateTime(date)))
                {
                    ibLocked.Visible = true;
                    ibEdit.Visible = false;
                }
            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    private bool DateLockedForEmployee(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Employee_lock_start != "" && tp.s_Employee_lock_end != "")
            {
                return (DateTime.Today >= tp.Employee_lock_start && DateTime.Today <= tp.Employee_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }
}
