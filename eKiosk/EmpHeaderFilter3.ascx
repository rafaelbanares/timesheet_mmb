﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmpHeaderFilter3.ascx.cs" Inherits="EmpHeaderFilter3" %> 
 <style type="text/css">
     .style1
     {
         width: 22px;
     }
     .style2
     {
         width: 40px;
     }
     .style3
     {
         width: 110px;
     }
 </style>
 <table width="100%">
  <tr>  
  <td align="left" style="width: 348px;"> Employee Search:<asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" Width="220px"></asp:TextBox></td>
  <td align="left" > <asp:Button ID="btnRefresh" runat="server" Text="Search" /></td>
  <td align="left" > Status:
      <asp:DropDownList ID="ddlDisplay" runat="server" CssClass="ControlDefaults" 
          onselectedindexchanged="ddlDisplay_SelectedIndexChanged" 
          AutoPostBack="True">
          <asp:ListItem>Invalid Entries</asp:ListItem>
          <asp:ListItem Value="for approval">For Approval</asp:ListItem>
          <asp:ListItem Value="approved">Approved</asp:ListItem>
          <asp:ListItem Value="declined">Declined</asp:ListItem>
          <asp:ListItem Value="all">All</asp:ListItem>
      </asp:DropDownList>
  </td>
  <td align="left" class="style2" >From:</td>
  <td align="left" class="style3" >
        <asp:TextBox ID="txtPrdFrom" runat="server" Height="17px" Width="70px"></asp:TextBox>
        <ajaxToolkit:CalendarExtender ID="txtPrdFrom_CE" runat="server" Enabled="True" TargetControlID="txtPrdFrom" PopupButtonID="ibtxtPrdFrom">
        </ajaxToolkit:CalendarExtender>
        <asp:ImageButton ID="ibtxtPrdFrom" runat="server" ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" ToolTip="Click to choose date" />
        <ajaxToolkit:MaskedEditValidator ID="mevPrdFrom" runat="server" ControlExtender="meePrdFrom" ControlToValidate="txtPrdFrom" MinimumValue="01/01/2000" >*
        </ajaxToolkit:MaskedEditValidator>
    </td>
    <td align="left" class="style1" >To:</td>
    <td align="left" >
        <asp:TextBox ID="txtPrdTo" runat="server" Height="17px" Width="70px"></asp:TextBox>
        <ajaxToolkit:CalendarExtender ID="txtPrdTo_CE" runat="server" Enabled="True" TargetControlID="txtPrdTo" PopupButtonID="ibtxtPrdTo">
        </ajaxToolkit:CalendarExtender>
        <asp:ImageButton ID="ibtxtPrdTo" runat="server" ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" ToolTip="Click to choose date" />
        <ajaxToolkit:MaskedEditValidator ID="mevPrdTo" runat="server" ControlExtender="meePrdTo" ControlToValidate="txtPrdTo"  MinimumValue="01/01/2000" >*
        </ajaxToolkit:MaskedEditValidator>
    </td>
  </tr>
</table>
<div style="display:none">
    <ajaxToolkit:MaskedEditExtender ID="meePrdFrom" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtPrdFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="meePrdTo" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtPrdTo">
    </ajaxToolkit:MaskedEditExtender>
</div>





