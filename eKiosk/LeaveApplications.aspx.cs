﻿using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class LeaveApplications : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindLeaveType();
            BindStatus();
            PopulateYear(Request.Params["year"] == null ? 0 : Request.Params["year"].ToInt());
            BindGrid();
        }
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = sender as ImageButton;
        HiddenField hfID = ib.Parent.Parent.FindControl("hfID") as HiddenField;

        SecureUrl su = new SecureUrl("LeaveEmployee.aspx?id=" + hfID.Value);
        Response.Redirect(su.ToString());
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    #region BindLeaveType
    private void BindLeaveType()
    {
        BNS.TK.Entities.LeaveCode leavecode = new BNS.TK.Entities.LeaveCode();
        leavecode.Query.AddResultColumn(BNS.TK.Entities.LeaveCode.ColumnNames.Code);
        leavecode.Query.AddResultColumn(BNS.TK.Entities.LeaveCode.ColumnNames.Description);
        leavecode.Where.CompanyID.Value = _KioskSession.CompanyID;
        leavecode.Query.AddOrderBy(BNS.TK.Entities.LeaveCode.ColumnNames.Description, MMB.DataObject.WhereParameter.Dir.ASC);
        leavecode.Query.Load();


        ddlLeaveType.DataSource = leavecode.DefaultView;
        ddlLeaveType.DataValueField = BNS.TK.Entities.LeaveCode.ColumnNames.Code;
        ddlLeaveType.DataTextField = BNS.TK.Entities.LeaveCode.ColumnNames.Description;
        ddlLeaveType.DataBind();
        ddlLeaveType.Items.Insert(0, new ListItem("All", ""));

        //set pass query string
        if (_Url["type"].IsFilled())
        {
            ddlLeaveType.SelectedValue = _Url["type"].PadRight(10);
        }
    }
    #endregion

    #region BindStatus
    private void BindStatus()
    {
        ddlStatus.Items.Clear();
        ddlStatus.Items.Add(new ListItem("All", ""));
        ddlStatus.Items.Add(new ListItem(TKWorkFlow.Status.ForApproval, TKWorkFlow.Status.ForApproval));
        ddlStatus.Items.Add(new ListItem(TKWorkFlow.Status.Approved, TKWorkFlow.Status.Approved));
        ddlStatus.Items.Add(new ListItem(TKWorkFlow.Status.Declined, TKWorkFlow.Status.Declined));

        //set pass query string
        if (_Url["status"].IsFilled())
        {
            ddlStatus.SelectedValue = _Url["status"];
        }
    }
    #endregion

    private void PopulateYear(int selectedYear = 0)
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = selectedYear == 0 ? 
            DateTime.Today.Year.ToString() : selectedYear.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    private DataSet GetData()
    {
        int year = ddlYear.SelectedValue.ToInt();
        string sql = "SELECT " +
                        "	ID, " +
                        "	lc.Description as LeaveDescr, " +
                        "	StartDate, " +
                        "	EndDate, " +
                        "	[Days], " +
                        "	[Status], " +
                        "	(" +
                        "       Reason + " +
                        "       CASE WHEN DeclinedReason IS NULL " +
                        "           THEN '' " +
                        "           ELSE ' | ' + DeclinedReason " +
                        "       END " +
                        "   ) AS Reason " +
                        "FROM LeaveTransHdr lv INNER JOIN LeaveCode lc " +
                        "ON  " +
                        "	lv.CompanyID = lc.CompanyID AND " +
                        "	lv.Code = lc.Code " +
                        "WHERE  " +
                        "	lv.CompanyID = {0} AND " +
                        "	lv.EmployeeID = {1} AND  " +
                        "	year(lv.StartDate) = {2} AND " +
                        "	({3} = '' or lv.[Status] = {3}) AND " +
                        "	({4} = '' or lv.[Code] = {4}) " +
                        "ORDER BY lv.StartDate DESC ";

        object[] parameters =  { _KioskSession.CompanyID,
                                   _KioskSession.EmployeeID,
                                   year,
                                   ddlStatus.SelectedValue.SafeTrim(),
                                   ddlLeaveType.SelectedValue.SafeTrim()
                               };
        BNS.TK.Entities.LeaveTransHdr leave = new BNS.TK.Entities.LeaveTransHdr();
        leave.DynamicQuery(sql, parameters);

        return leave.ToDataSet();

    }


    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow) return;

        if (e.Row.RowIndex != gvEmp.EditIndex)
        {
            string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().Trim().ToLower();
            if (status != "for approval")
            {
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                ibEdit.Visible = false;
            }
        }

    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void lnkApplyLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("LeaveEmployee.aspx", false);
    }
}