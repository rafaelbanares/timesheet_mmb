﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="WorkForSub.aspx.cs" Inherits="WorkForSub"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Level1 Definitions"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Level1 Definition" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Level1 Definition</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" DataKeyNames="WorkForSubID">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>
                    <asp:TemplateField HeaderText="Client">
                        <ItemStyle Width="290px" />
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlParent" runat="server" DataTextField="ShortDesc" DataValueField="WorkForID" Width="250px"> 
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlParent" runat="server" ControlToValidate="ddlParent" 
                                ErrorMessage="Client cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>                        
                        <ItemTemplate>
                            <asp:Label ID="lblMemberOf" runat="server" Text='<%# Bind("MemberOf") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Brand Short Description">
                        <ItemStyle Width="240px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtShortDesc" runat="server" Width="220px" Text='<%# Bind("ShortDesc") %>' CssClass="ControlDefaults" MaxLength="25"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvShortDesc" runat="server" ControlToValidate="txtShortDesc" 
                                ErrorMessage="Short description cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label id="lblShortDesc" runat="server" Text='<%# Bind("ShortDesc") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Brand Description">
                        <ItemStyle Width="370px" />
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Description") %>' MaxLength="150" Width="350px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" 
                                ErrorMessage="Description cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkActive" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="imgActive" runat="server" ImageUrl="~/Graphics/check.gif" />
                        </ItemTemplate>                
                        <ItemStyle HorizontalAlign="Center" />                
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                            <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" 
                                    OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );"/>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
