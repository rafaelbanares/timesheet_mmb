﻿using Bns.AttendanceUI;
using BNS.TK.Business;
using BNS.TK.Entities;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class OBApproval : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserSession();

        if (_Url["approverno"] == string.Empty)
            Response.Redirect("LoginEmployee.aspx");

        if (!IsPostBack)
        {
            BindGrid();
            EmpHeader1.ApproverNo = ApproverNo();
        }
    }

    private string ApproverNo()
    {
        if (_Url["approverno"] == null)
            return "";
        else
            return _Url["approverno"];
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        btnForApproval.Visible = (gvEmp.EditIndex == -1 && CountStatus(TKWorkFlow.Status.ForApproval) > 0);
    }
    private DataSet GetData()
    {
        string sdate = "01/01/" + EmpHeader1.Year;
        string edate = "12/31/" + EmpHeader1.Year;

        string empWhere = "";
        if (ApproverNo() == "4")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver4 = {4}) ";
        else if (ApproverNo() == "3")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver3 = {4}) ";
        else if (ApproverNo() == "2")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver2 = {4}) ";
        else if (ApproverNo() == "1")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver1 = {4}) ";
         
        //status filter
        string status = EmpHeader1.Display.ToLower();
        if (EmpHeader1.Display.ToLower() == TKWorkFlow.Status.ForApproval.ToLower() && (ApproverNo() == "1" || ApproverNo() == "2"))
            empWhere += " AND a.Stage = 0 ";
        else if (EmpHeader1.Display.ToLower() == TKWorkFlow.Status.ForApproval.ToLower() && (ApproverNo() == "3" || ApproverNo() == "4"))
            empWhere += " AND a.Stage = 1 ";


        string sql = "SELECT " +
                        "	a.CompanyID " +
                        "	,a.EmployeeID " +
                        "	,b.FullName " +
                        "	,OBDate " +
                        "	,StartDate " +
                        "	,EndDate " +
                        "	,[Status] " +
                        "	,a.Stage " +
                        "	,Remarks " +
                        "   ,(SELECT x.Posted " +
                        "       FROM OTAuthorization x " +
                        "       WHERE x.OTDate = a.OBDate " +
                        "       AND x.CompanyID = a.CompanyID " +
                        "       AND x.EmployeeID = a.EmployeeID " +
                        "   ) as Posted " +
                        "FROM OBTrans a INNER JOIN Employee b " +
                        "ON	a.CompanyID=b.CompanyID AND  " +
                        "	a.EmployeeID=b.EmployeeID  " +
                        "WHERE  " +
                        "	a.CompanyID = {0} AND " +
                        "	(a.OBDate between {1} and {2}) AND " +
                        "	({3}='' or a.[Status] = {3}) " + empWhere +
                        "   AND b.FullName like {5} " +
                        "ORDER BY a.EmployeeID, a.OBDate DESC ";

        object[] parameters = { _KioskSession.CompanyID, 
                                  sdate.ToDate(), 
                                  edate.ToDate(), 
                                  (EmpHeader1.Display.IsNullOrEmpty() || EmpHeader1.Display.ToLower()=="all") ? "" : EmpHeader1.Display.TrimEnd(), 
                                  _KioskSession.EmployeeID,
                                  EmpHeader1.Search + "%" };

        BNS.TK.Entities.OBTrans ob = new BNS.TK.Entities.OBTrans();
        ob.DynamicQuery(sql, parameters);

        return ob.ToDataSet();
    }

   

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                //Label lblreason = e.Row.FindControl("lblreason") as Label;
                //Literal lblRestdayDescr = e.Row.FindControl("litRestdayDescr") as Literal;
                //lblRestdayDescr.Text = DateHelper.DecodeDOW(DataBinder.Eval(e.Row.DataItem, "RestDayCode").ToString().TrimEnd());
                
                //lblreason.ToolTip = lblreason.Text.TrimEnd();
                //lblreason.Text = lblreason.Text.TrimEnd().ToEllipse(20);

                ImageButton ibRecall = e.Row.FindControl("ibRecall") as ImageButton;
                ImageButton ibApproved = e.Row.FindControl("ibApproved") as ImageButton;
                ImageButton ibDeny = e.Row.FindControl("ibDeny") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().TrimEnd().ToUpper();

                var posted = DataBinder.Eval(e.Row.DataItem, "Posted").ToString();
                var isPosted = string.IsNullOrWhiteSpace(posted) ? false : posted.ToBoolean();                

                ibRecall.Visible = (!isPosted) && (status.IsEqual(TKWorkFlow.Status.Approved) || status.IsEqual(TKWorkFlow.Status.Declined));
                ibApproved.Visible = (status.IsEqual(TKWorkFlow.Status.ForApproval));
                ibDeny.Visible = (status.IsEqual(TKWorkFlow.Status.ForApproval));
                ibApproved.Attributes.Add("onclick", "return window.confirm('Approve OB?' );");

            }

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }


    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    private void ApprovedDeny(object sender, string newStatus, int rowindex, int newStage)
    {
        HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
        Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;
        string obdate = gvEmp.DataKeys[rowindex].Values["obdate"].ToString();
        

        BNS.TK.Entities.OBTrans ob = new BNS.TK.Entities.OBTrans();
        if (ob.LoadByPrimaryKey(hfCompanyID.Value, litEmployeeID.Text, obdate.ToDate()))
        {
            ob.Status = newStatus;
            ob.Stage = newStage;
            if (newStatus.ToLower().TrimEnd() == TKWorkFlow.Status.Declined.ToLower())
                ob.s_DeclinedReason = txtDecReason.Text;

            ob.LastUpdBy = _KioskSession.UID;
            ob.LastUpdDate = DateTime.Now;
            ob.Save();
        }

        
        //Helper.SendMail(Helper.GetEmailAddress(hfCompanyID.Value, litEmployeeID.Text), "Kronos OB change of status", "Your DTR was acted upon by your supervisor.");


        gvEmp.EditIndex = -1;
        BindGrid();

        ReprocessAttendance(litEmployeeID.Text, obdate.ToDate(), obdate.ToDate());
    }

    private void ReprocessAttendance(string empid, DateTime from, DateTime to)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(from, to, empid);
    }


    private void Recall(object sender, int rowindex)
    {
        var row = gvEmp.Rows[rowindex];
        var companyId = ((HiddenField)row.FindControl("hfCompanyID")).Value;
        var employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
        var obDate = gvEmp.DataKeys[rowindex].Values["obdate"].ToString().ToDate();
        var ob = new OBTrans();

        try
        {
            if (!ob.LoadByPrimaryKey(companyId, employeeId, obDate))
                throw new Exception("Overtime not found.");

            ob.Status = TKWorkFlow.Status.ForApproval;
            ob.Stage = 0;
            ob.Remarks = string.Empty;
            ob.DeclinedReason = string.Empty;
            ob.LastUpdBy = _KioskSession.UID;
            ob.LastUpdDate = DateTime.Now;
            ob.Save();
        }
        catch (SqlException sqex)
        {
            lblError.Text = sqex.Message;
        }

        gvEmp.EditIndex = -1;
        BindGrid();

        ReprocessAttendance(employeeId, obDate, obDate);

        //HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
        //Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;
        //string obdate = gvEmp.DataKeys[rowindex].Values["obdate"].ToString();


        //BNS.TK.Entities.OBTrans ob = new BNS.TK.Entities.OBTrans();
        //if (ob.LoadByPrimaryKey(hfCompanyID.Value, litEmployeeID.Text, obdate.ToDate()))
        //{
        //    ob.Status = TKWorkFlow.Status.Declined;
        //    ob.Stage = 0;
        //    ob.s_DeclinedReason = "Recalled";
        //    ob.LastUpdBy = _KioskSession.UID;
        //    ob.LastUpdDate = DateTime.Now;
        //    ob.Save();
        //}

        //gvEmp.EditIndex = -1;
        //BindGrid();

        //ReprocessAttendance(litEmployeeID.Text, obdate.ToDate(), obdate.ToDate());
    }

    protected void btnForApproval_Click(object sender, EventArgs e)
    {
        var ob = new OBTrans();
        var userId = _KioskSession.UID;
        var date = DateTime.Now;
        var approverNo = _Url["approverno"].ToInt();
        var startDate = new DateTime();
        var endDate = new DateTime();

        string status, newStatus, companyId, employeeId;
        short newStage;
        DateTime tempDate;
        bool approvable;

        foreach (GridViewRow row in gvEmp.Rows)
        {
            status = ((Literal)row.FindControl("litStatus")).Text;
            approvable = ((ImageButton)row.FindControl("ibApproved")).Visible;

            if (status == TKWorkFlow.Status.ForApproval && approvable)
            {
                tempDate = DateTime.Parse(gvEmp.DataKeys[row.RowIndex].Values["obdate"].ToString());

                startDate = startDate == DateTime.MinValue ? tempDate : startDate > tempDate ? tempDate : startDate;
                endDate = endDate == DateTime.MinValue ? tempDate : endDate < tempDate ? tempDate : endDate;

                newStatus = TKWorkFlow.Status.Approved;
                newStage = 2;
                employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;

                if (approverNo == 1 || approverNo == 2)
                {
                    newStage = 1;

                    if (!IsFinalApprover(employeeId))
                        newStatus = TKWorkFlow.Status.ForApproval;
                }

                companyId = ((HiddenField)row.FindControl("hfCompanyID")).Value;

                if (ob.LoadByPrimaryKey(companyId, employeeId, tempDate))
                {
                    ob.Status = newStatus;
                    ob.Stage = newStage;
                    ob.LastUpdBy = userId;
                    ob.LastUpdDate = date;
                    ob.Save();
                }
            }
        }

        if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
        {
            gvEmp.EditIndex = -1;
            BindGrid();

            ReprocessAttendance(string.Empty, startDate, endDate);
        }
    }

    private int CountStatus(string filterstatus)
    {
        int retval = 0;
        foreach (GridViewRow row in gvEmp.Rows)
        {
            Literal litStatus = row.FindControl("litStatus") as Literal;
            if (litStatus.Text.ToLower() == filterstatus.ToLower())
                retval++;
        }
        return retval;
    }


    private bool IsFinalApprover(string empid)
    {
        EmployeeApprover appr = new EmployeeApprover();
        string sql = "SELECT Approver3, Approver4 FROM EmployeeApprover WHERE CompanyID = {0} AND EmployeeID = {1} ";

        object[] parameters = { _KioskSession.CompanyID, empid.Trim() };
        appr.DynamicQuery(sql, parameters);
        if (appr.RowCount > 0)
        {
            return (appr.s_Approver3.IsNullOrEmpty() && appr.s_Approver4.IsNullOrEmpty());
        }
        return false;
    }

    //called onclick of gridview approve or decline button
    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        Literal litEmployeeID = gvEmp.Rows[gvEmp.SelectedIndex].FindControl("litEmployeeID") as Literal;
        string empid = litEmployeeID.Text;

        string newStatus = TKWorkFlow.Status.Approved;
        int newStage = 2;
        if (ApproverNo() == "1" || ApproverNo() == "2")
        {
            newStage = 1;
            if (IsFinalApprover(empid))
                newStatus = TKWorkFlow.Status.Approved;
            else
                newStatus = TKWorkFlow.Status.ForApproval;
        }

        ApprovedDeny(sender, newStatus, gvEmp.SelectedIndex, newStage);
    }

    //called onclick of grid delete button
    //this will just show popup to get the reason of decline
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        txtDecReason.Text = "";
        hfdecRowid.Value = e.RowIndex.ToString();
        mpe3.Show();
    }

    //called onclick fo refresh button
    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper().TrimEnd() == "RECALL")
        {
            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            int rowIndex = row.RowIndex;
            Recall(sender, rowIndex);
        }
    }

    private bool DateLockedForApprover(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Approver_lock_start != "" && tp.s_Approver_lock_end != "")
            {
                return (DateTime.Today >= tp.Approver_lock_start && DateTime.Today <= tp.Approver_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }
    protected void btnDecline_Click(object sender, EventArgs e)
    {
        int rowindex;
        if (int.TryParse(hfdecRowid.Value, out rowindex))
            ApprovedDeny(sender, "Declined", rowindex, 0);
    }


}
