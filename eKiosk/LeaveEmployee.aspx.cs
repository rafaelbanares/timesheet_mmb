using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Business;
using BNS.TK.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

public partial class Attendance_LeaveEmployee : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        lblError.Text = "";

        if (!IsPostBack)
        {
            //todo: should inform the user why he is bieng redirected
            // do not allow filing of leave for resigned employees
            if (_KioskSession.IsResigned)
                Response.Redirect("LoginEmployee.aspx");

            populateScreenDefaults();
            PopulateControls();
        }
    }
    
    private void populateScreenDefaults()
    {
        string today = Tools.ShortDate(DateTime.Now.ToShortDateString(), _KioskSession.DateFormat);

        txtDateFiled.Text = today;
        txtDateLeave.Text = today;
        txtDateStart.Text = today;
        txtDateEnd.Text = today;

        BNS.TK.Entities.LeaveCode lc = new BNS.TK.Entities.LeaveCode();
        lc.Query.AddResultColumn(BNS.TK.Entities.LeaveCode.ColumnNames.Code);
        lc.Query.AddResultColumn(BNS.TK.Entities.LeaveCode.ColumnNames.Description);
        lc.Where.CompanyID.Value = _KioskSession.CompanyID;
        lc.Query.AddOrderBy(BNS.TK.Entities.LeaveCode.ColumnNames.Description, MMB.DataObject.WhereParameter.Dir.ASC);
        lc.Query.Load();
        DataSet dsLeave = lc.ToDataSet();

        ddlLeave.DataSource = dsLeave.Tables[0];
        ddlLeave.DataBind();
        ddlLeave.Items.Insert(0, "");
        
        ////... populate leave balances
        gvLCredit.DataSource = LeaveBalances().Tables[0];
        gvLCredit.DataBind();        
    }

    private void PopulateControls()
    {
        if (isAddMode()) return;

        txtDateFiled.Enabled = false;

        int id = _Url["id"].ToInteger();
        var leave = new LeaveTransHdr();
        if (leave.LoadByPrimaryKey(id))
        {
            if (leave.EmployeeID.Trim() != _KioskSession.UID.Trim())
                Response.Redirect("LoginEmployee.aspx");

            var days = (double)leave.Days;
            if (days > 1)
            {
                rbMultiple.Checked = true;
                txtDateStart.Text = leave.s_StartDate;
                txtDateEnd.Text = leave.s_EndDate;

                singleleave.Attributes["style"] = "display: none;";
                multileave.Attributes["style"] = "display: ;";
            }  
            else
            {
                if (days == 1)
                    rbWholeday.Checked = true;
                else if (days == 0.5)
                    rbHalfday.Checked = true;

                txtDateLeave.Text = leave.s_StartDate;
            }

            txtDateFiled.Text = leave.s_DateFiled;
            ddlLeave.SelectedValue = leave.Code;
            txtReason.Text = leave.Reason;
        }
    }

    private bool isAddMode()
    {
        return (_Url["id"].IsNullOrEmpty()) ? true : false;
    }

    private DataSet LeaveBalances()
    {
        string sql = "SELECT " +
                        "	EmployeeID = coalesce(used.EmployeeID, earn.EmployeeID), " +
                        "	LeaveCode = coalesce(lc.Code, used.LeaveCode, earn.LeaveCode), " +
                        "	LeaveDescr = lc.[Description], " +
                        "	DaysApproved = coalesce(used.DaysApproved, 0), " +
                        "	DaysForApproval = coalesce(used.DaysForApproval, 0), " +
                        "	DaysEarned = coalesce(earn.earned, 0), " +
                        "	Balance = coalesce(earn.earned, 0) - coalesce(used.DaysApproved, 0) - coalesce(used.DaysForApproval, 0) " +
                        "FROM " +
                        "	( " +
                        "	SELECT " +
                        "		EmployeeID = coalesce(a.EmployeeID, b.EmployeeID), " +
                        "		LeaveCode = coalesce(a.Code, b.Code), " +
                        "		DaysApproved = coalesce(a.Days, 0), " +
                        "		DaysForApproval = coalesce(b.Days, 0) " +
                        "	FROM udf_LeaveTransSummary({0}, {1}, {2}, {3}, '') a " +
                        "	FULL OUTER JOIN udf_LeaveTransSummary({0}, {1}, {2}, {4}, '') b " +
                        "	ON " +
                        "		a.EmployeeID = b.EmployeeID and " +
                        "		a.Code = b.Code " +
                        "	) used " +
                        "FULL OUTER JOIN udf_LeaveEarnSummary({0}, {1}, {2}, '') earn " +
                        "ON " +
                        "	used.EmployeeID = earn.EmployeeID and " +
                        "	used.LeaveCode = earn.LeaveCode " +
                        "INNER JOIN LeaveCode lc " +
                        "ON lc.Code = coalesce(used.LeaveCode, earn.LeaveCode) " +
                        "WHERE lc.CompanyID = {0} ";

        object[] parameters =  { _KioskSession.CompanyID, 
                                   _KioskSession.EmployeeID,
                                   DateTime.Today.Year,
                                   TKWorkFlow.Status.Approved,
                                   TKWorkFlow.Status.ForApproval
                               };
        BNS.TK.Entities.LeaveTrans leave = new BNS.TK.Entities.LeaveTrans();
        leave.DynamicQuery(sql, parameters);

        return leave.ToDataSet();

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        
        double totalFiledDays = 0;

        if (!rbMultiple.Checked)
        {
            txtDateStart.Text = txtDateLeave.Text;
            txtDateEnd.Text = txtDateLeave.Text;
            totalFiledDays = rbHalfday.Checked ? 0.5 : 1.0;
        }
        else
        {            
            if (!ClientScript.IsStartupScriptRegistered("displaymulti"))
            {
                ClientScript.RegisterStartupScript(typeof(Page), "displaymulti",
                @"<script language=JavaScript> toggledisplay(2); </script>");
            }

            //... Date range must be 2 days onwards if chosen multiple dates.
            DateTime startdate = DateTime.Parse(txtDateStart.Text);
            DateTime enddate = DateTime.Parse(txtDateEnd.Text).AddDays(1);

            totalFiledDays = enddate.Subtract(startdate).TotalDays;

            if (totalFiledDays < 1)
            {
                lblError.Text = "Invalid date range, leave must be more than 1 day";
                return;
            }
        }
        
        SqlParameter[] spParams = new SqlParameter[] 
        {
            new SqlParameter("@ID", isAddMode() ? 0 : _Url["id"].ToInteger()),
            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
            new SqlParameter("@EmployeeID", _KioskSession.EmployeeID),
            new SqlParameter("@StartDate", txtDateStart.Text),
            new SqlParameter("@EndDate", txtDateEnd.Text),
            new SqlParameter("@Code", ddlLeave.SelectedItem.Value),
            new SqlParameter("@DateFiled", txtDateFiled.Text),
            new SqlParameter("@appYear", DateTime.Today.Year)
        };

        DataSet dsValidation = SqlHelper.ExecuteDataset(
            _ConnectionString, 
            CommandType.StoredProcedure, 
            _KioskSession.DB + ".dbo.usa_LeaveValidations", 
            spParams);

        //... Check first if exists
        bool isExists = dsValidation.Tables[0].Rows.Count > 0 && isAddMode();
        bool isCheckBalance = (bool)dsValidation.Tables[1].Rows[0]["CheckBalance"];
        double filingdays = double.Parse(dsValidation.Tables[1].Rows[0]["FilingDays"].ToString());
        double actualleavedays = double.Parse(dsValidation.Tables[2].Rows[0]["ActualLeaveDays"].ToString());
        double daysbeforeleave = double.Parse(dsValidation.Tables[3].Rows[0]["ActualLeaveDays"].ToString()) - 1;
        double currTotalLeaveDays = double.Parse(dsValidation.Tables[4].Rows[0]["Total"].ToString());
        if (daysbeforeleave < 0)
            daysbeforeleave = 0;

        if (rbMultiple.Checked)
            totalFiledDays = actualleavedays;

        if (isExists)
        {
            lblError.Text = "Leave already exists";
            return;
        }
        else if (currTotalLeaveDays > 0.5) 
        {
            lblError.Text = "Leave has exceeded number of valid days.";
            return;
        }
        else if (actualleavedays == 0)
        {
            lblError.Text = "No valid leave days. Check dates if it falls on holiday or restday";
            return;
        }
        else if (filingdays > daysbeforeleave)
        {
            lblError.Text = ddlLeave.SelectedItem.Value + " should be filed " + filingdays + " working days in advance";
            return;
        }
        else if (float.Parse(dsValidation.Tables[1].Rows[0]["Balance"].ToString()) < totalFiledDays && isCheckBalance)
        {
            lblError.Text = "No available leave credits";
            return;
        }


        if (Save(totalFiledDays))
        {
            //sender email
            //Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos leave for approval", "You have a pending leave for approval.");

            Response.Redirect("LeaveApplications.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("LeaveApplications.aspx");
    }

    private bool Save(double leavedays)
    {
        DataSet dsLeaveDate = GetPossibleLeaveDates();
        using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
        {
            var hdr = new LeaveTransHdr();
            var leave = new BNS.TK.Entities.LeaveTrans();
            var id = _Url["id"].ToInteger();

            try
            {
                if (isAddMode())
                {
                    hdr.AddNew();
                    hdr.CompanyID = _KioskSession.CompanyID;
                    hdr.EmployeeID = _KioskSession.EmployeeID;
                    hdr.DateFiled = txtDateFiled.Text.ToDate();
                    hdr.StartDate = txtDateStart.Text.ToDate();
                    hdr.EndDate = txtDateEnd.Text.ToDate();
                    hdr.Days = (decimal)leavedays;
                    hdr.Cnv_Hours = (decimal)(leavedays / 8);
                    hdr.Approver = " ";
                    hdr.Code = ddlLeave.SelectedItem.Value;
                    hdr.Reason = txtReason.Text;
                    hdr.Status = "For Approval";
                    hdr.Stage = 0;
                    hdr.Posted = false;
                    hdr.CreatedBy = _KioskSession.UID;
                    hdr.CreatedDate = DateTime.Now;
                    hdr.LastUpdBy = _KioskSession.UID;
                    hdr.LastUpdDate = DateTime.Now;
                    hdr.Save();

                    
                }
                else
                {
                    if (hdr.LoadByPrimaryKey(id) && hdr.Status.Trim() == "For Approval")
                    {
                        hdr.StartDate = txtDateStart.Text.ToDate();
                        hdr.EndDate = txtDateEnd.Text.ToDate();
                        hdr.Days = (decimal)leavedays;
                        hdr.Cnv_Hours = (decimal)(leavedays / 8);
                        hdr.Code = ddlLeave.SelectedItem.Value;
                        hdr.Reason = txtReason.Text;
                        hdr.LastUpdBy = _KioskSession.UID;
                        hdr.LastUpdDate = DateTime.Now;
                        hdr.Save();

                        leave.Where.LeaveHdrID.Value = id;
                        leave.Where.CompanyID.Value = _KioskSession.CompanyID;
                        leave.Query.Load();
                        leave.DeleteAll();
                        leave.Save();
                    }
                }

                foreach (DataRow dr in dsLeaveDate.Tables[0].Rows)
                {
                    leave.AddNew();
                    leave.CompanyID = _KioskSession.CompanyID;
                    leave.LeaveHdrID = hdr.ID;
                    leave.LeaveDate = (DateTime)dr["Date"];
                    leave.Days = (decimal)((leavedays < 1) ? .5 : 1);
                    leave.Cnv_Hours = (leave.Days * 8);
                    leave.Cnv_Mins = (int)(leave.Days * 8 * 60);
                }

                leave.Save();
            }
            catch (Exception ex_)
            {

                lblError.Text = ex_.Message;
                return false;
            }

            ts.Complete();
        }

        return true;
    }

    private DataSet GetPossibleLeaveDates()
    {
        SqlParameter[] spParams = new SqlParameter[]
        {
            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
            new SqlParameter("@EmployeeID", _KioskSession.EmployeeID),
            new SqlParameter("@StartDate", txtDateStart.Text),
            new SqlParameter("@EndDate", txtDateEnd.Text)
        };

        return SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_GetPossibleLeaveDates", spParams);
    }

    private BNS.TK.Entities.Holiday LoadHolidays(DateTime sdate, DateTime edate)
    {
        BNS.TK.Entities.Holiday hol = new BNS.TK.Entities.Holiday();
        string sql = "select [date] from Holiday where CompanyID = {0} AND ([DATE] between {1} and {2})";
        object[] parameters = { _KioskSession.CompanyID, sdate, edate };
        hol.DynamicQuery(sql, parameters);
        hol.Sort = "Date";

        return hol;
    }

    

}
