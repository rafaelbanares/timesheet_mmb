﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmpHeaderFilter.ascx.cs" Inherits="EmpHeaderFilter" %>

<style type="text/css">
    .style1
    {
        width: 91px;
    }
    .style3
    {
        width: 153px;
    }
    .style4
    {
        width: 361px;
    }
</style>

<script type="text/javascript">
function openEmpInfo(url) {
    popupWindow=wopen(url,"popupWindow",820,300);
    return false;
}
function openwindow() {
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",230,260);
}
function defaultdates() {
    $get('<%=  hdDateSelection.ClientID %>').value = '';
    $get('<%=  Parent.FindControl("btnFU").ClientID %>').click();
}
function enterpress(e) {
    var evt = e ? e : window.event;
    if (evt.keyCode == 13) {
        refresh();
        return false;
    }
}
function refresh() {
    $get('<%=  Parent.FindControl("btnFU").ClientID %>').click();
}
function updatedates(sdate, edate) {
    $get('<%= hdDateStart.ClientID %>').value = sdate;
    $get('<%= hdDateEnd.ClientID %>').value = edate;

    $get('<%=  hdDateSelection.ClientID %>').value = 'filtered';
    $get('<%=  Parent.FindControl("btnFU").ClientID %>').click();

    closewindow(popupWindow);
}
function canceldates()
{
}


</script>

<table width="100%">
  <tr>
      <td valign="top" class="style4">    
          <asp:LinkButton ID="lbEmpInfo" runat="server" Visible="False"><asp:Label ID="lblFullname" runat="server" Font-Bold="True"></asp:Label></asp:LinkButton>
      </td>
      <td align="left" valign="top" class="style3">
      </td>
      <td align="left" valign="top" class="style1">
      </td>
      <td align="left" valign="top">
      </td>
  </tr>
  <tr>  
  <td align="left" valign="top" class="style4">
    Employee Search:
    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" Width="220px"></asp:TextBox>
  </td>
  <td align="left" valign="top" class="style3">
      Status:
      <asp:DropDownList ID="ddlDisplay" runat="server" CssClass="ControlDefaults">
          <asp:ListItem Value="for approval">For Approval</asp:ListItem>
          <asp:ListItem Value="approved">Approved</asp:ListItem>
          <asp:ListItem Value="declined">Declined</asp:ListItem>
          <asp:ListItem Value="all">All</asp:ListItem>
      </asp:DropDownList>
  </td>
  <td align="left" valign="top" class="style1">
      <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClientClick="refresh();return false;" />
  </td>
  <td align="center" valign="top">
    <asp:LinkButton ID="lnkFilter" runat="server">Select Date Filter</asp:LinkButton>
  </td>
  </tr>
</table>
<asp:HiddenField ID="hfURL" runat="server" />
<asp:HiddenField ID="hdDateSelection" runat="server" />
<asp:HiddenField ID="hdDateStart" runat="server" />
<asp:HiddenField ID="hdDateEnd" runat="server" />




