using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities; 

public partial class EmployeeApprovers : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        hfErrorMsg.Value = "";

        if (!IsPostBack)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("bMain");

            body.Attributes.Add("onload", "load();");
            BindGrid();
        }
    }
    private void BindGrid()
    {
        hfIsAdd.Value = "0";
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }
  
 
    
    private DataSet GetData()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmployeeID", "")
            ,new SqlParameter("@Search", txtSearch.Text.Trim())
        };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_EmployeeApproversList", spParams);
        return ds;
    }

    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex == gvEmp.EditIndex)
            {
                Button btnAppr1 = e.Row.FindControl("btnAppr1") as Button;
                Button btnAppr2 = e.Row.FindControl("btnAppr2") as Button;
                Button btnAppr3 = e.Row.FindControl("btnAppr3") as Button;
                Button btnAppr4 = e.Row.FindControl("btnAppr4") as Button;

                TextBox txtAppr1 = e.Row.FindControl("txtAppr1") as TextBox;
                TextBox txtAppr2 = e.Row.FindControl("txtAppr2") as TextBox;
                TextBox txtAppr3 = e.Row.FindControl("txtAppr3") as TextBox;
                TextBox txtAppr4 = e.Row.FindControl("txtAppr4") as TextBox;

                HiddenField hfAppr1 = e.Row.FindControl("hfAppr1") as HiddenField;
                HiddenField hfAppr2 = e.Row.FindControl("hfAppr2") as HiddenField;
                HiddenField hfAppr3 = e.Row.FindControl("hfAppr3") as HiddenField;
                HiddenField hfAppr4 = e.Row.FindControl("hfAppr4") as HiddenField;

                HiddenField hfCompID = e.Row.FindControl("hfCompID") as HiddenField;


                SecureUrl urlAppr1 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAppr1.ClientID, txtAppr1.ClientID, hfCompID.ClientID));
                SecureUrl urlAppr2 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAppr2.ClientID, txtAppr2.ClientID, hfCompID.ClientID));
                SecureUrl urlAppr3 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAppr3.ClientID, txtAppr3.ClientID, hfCompID.ClientID));
                SecureUrl urlAppr4 = new SecureUrl(string.Format(@"popupEmployeeCompany.aspx?hfID={0}&lbID={1}&compID={2}", hfAppr4.ClientID, txtAppr4.ClientID, hfCompID.ClientID));

                btnAppr1.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAppr1));
                btnAppr2.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAppr2));
                btnAppr3.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAppr3));
                btnAppr4.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAppr4));

                txtAppr1.Enabled = false;
                txtAppr2.Enabled = false;
                txtAppr3.Enabled = false;
                txtAppr4.Enabled = false;

            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string empid = gvEmp.DataKeys[e.RowIndex].Values["EmployeeID"].ToString();
        
        //SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.Text, "UPDATE TK_LOWE.dbo.EmployeeApprover WHERE CompanyID = @CompanyID AND EmployeeID = @EmployeeID",
        //    new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@EmployeeID", empid) });


        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", empid));
        sqlparam.Add(new SqlParameter("@Approver1", ""));
        sqlparam.Add(new SqlParameter("@Approver2", ""));
        sqlparam.Add(new SqlParameter("@Approver3", ""));
        sqlparam.Add(new SqlParameter("@Approver4", ""));
        sqlparam.Add(new SqlParameter("@UserID", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@date", DateTime.Now));


        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_EmployeeApproverUpdate", sqlparam.ToArray());

        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        HiddenField hfEmpid = gvEmp.Rows[e.RowIndex].FindControl("hfEmpid") as HiddenField;
        HiddenField hfAppr1 = gvEmp.Rows[e.RowIndex].FindControl("hfAppr1") as HiddenField;
        HiddenField hfAppr2 = gvEmp.Rows[e.RowIndex].FindControl("hfAppr2") as HiddenField;
        HiddenField hfAppr3 = gvEmp.Rows[e.RowIndex].FindControl("hfAppr3") as HiddenField;
        HiddenField hfAppr4 = gvEmp.Rows[e.RowIndex].FindControl("hfAppr4") as HiddenField;


        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID",  _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", hfEmpid.Value));
        sqlparam.Add(new SqlParameter("@Approver1", hfAppr1.Value));
        sqlparam.Add(new SqlParameter("@Approver2", hfAppr2.Value));
        sqlparam.Add(new SqlParameter("@Approver3", hfAppr3.Value));
        sqlparam.Add(new SqlParameter("@Approver4", hfAppr4.Value));
        sqlparam.Add(new SqlParameter("@UserID", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@date", DateTime.Now));


        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_EmployeeApproverUpdate", sqlparam.ToArray());

        gvEmp.EditIndex = -1;
        BindGrid();
    }
    

    private void RemoveApprover(string employeeId, int approverLevelToRemove)
    {
        EmployeeApprover appr = new EmployeeApprover();
        if (appr.LoadByPrimaryKey(_KioskSession.CompanyID, employeeId))
        {
            if (approverLevelToRemove == 2)
            {
                appr.s_Approver2 = "";
                appr.Save(); 
            }
            else if (approverLevelToRemove == 3)
            {
                appr.s_Approver3 = "";
                appr.Save();
            }
            else if (approverLevelToRemove == 4)
            {
                appr.s_Approver4 = "";
                appr.Save();
            }
        }

    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.IndexOf("clear") < 0) return;

        GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
        if (row == null) return;
        HiddenField hfEmpid = gvEmp.Rows[row.RowIndex].FindControl("hfEmpid") as HiddenField;
        if (hfEmpid == null || hfEmpid.Value.Trim() == "")
            return;

        if (e.CommandName == "clear1")
        {
            //level 1 backup approver
            RemoveApprover(hfEmpid.Value, 2);
            gvEmp.EditIndex = -1;
            BindGrid();
        }
        else if (e.CommandName == "clear2")
        {
            //level 2 main approver
            RemoveApprover(hfEmpid.Value, 3);
            gvEmp.EditIndex = -1;
            BindGrid();
        }
        else if (e.CommandName == "clear3")
        {
            //level 2 backup approver
            RemoveApprover(hfEmpid.Value, 4);
            gvEmp.EditIndex = -1;
            BindGrid();
        }
    }
}
