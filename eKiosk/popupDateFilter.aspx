﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="popupDateFilter.aspx.cs" Inherits="popupDateFilter" Title="Date Filter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript">
    function updateparent(sdate,edate)
    {    
        window.opener.updatedates($get(sdate).value, $get(edate).value);
        window.close();
    }
</script>

<asp:Label ID="lblHeader" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" Text="Select Date Filter"></asp:Label><br />
<br />
<div style="width: 220px; height: 220px">
<table width="100%">
    <tr>
        <td>
            Date From:
        </td>
        <td align="left">
            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                OnClientClick="return false;" ToolTip="Click to choose date" />
            <ajaxToolkit:MaskedEditValidator ID="mevDateStart" runat="server" ControlExtender="meeDateStart"
                ControlToValidate="txtDateStart" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
            </ajaxToolkit:MaskedEditValidator>
        </td>
    </tr>
    <tr>
        <td>
            To:
        </td>
        <td align="left">
            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
            <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                OnClientClick="return false;" ToolTip="Click to choose date" />
            <ajaxToolkit:MaskedEditValidator ID="mevDateEnd" runat="server" ControlExtender="meeDateEnd"
                ControlToValidate="txtDateEnd" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
            </ajaxToolkit:MaskedEditValidator>
        </td>
    </tr>

    <tr>
        <td align="center" colspan="2">
            <hr />
            <asp:Button ID="btnOK" runat="server" Text="Set Date Filter" Width="140" onclick="btnOK_Click"></asp:Button><br /><br />
            <asp:Button ID="btnRemove" runat="server" Text="No Date Filter" Width="140" OnClientClick="window.opener.defaultdates();window.close();return false;" ></asp:Button><br /><br />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="140" OnClientClick="window.close();return false;">
            </asp:Button>
        </td>
    </tr>

</table>
</div>

    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        PopupPosition="BottomRight" TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        PopupPosition="BottomRight" TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

</asp:Content>


