using Bns.AttendanceUI;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

public partial class Attendance_LeaveBalances : KioskPageUI
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            PopulateYear();
            BindLeaveTrans(true);
            BindLeaveCredit();            
        }
    }

    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    #region Data Bindings
    #region BindLeaveCredit
    private void BindLeaveCredit()
    {
        DataSet ds = GetLeaveCredit();
        DataTable dt = ds.Tables[0];
        gvLCredit.DataSource = SortDataTable(dt, false);
        gvLCredit.DataBind();

        string sdate = ds.Tables[1].Rows[0]["LastLeaveDate"].ToString().Trim();

        if (sdate != "")
            lblLeaveBalance.Text = "Leave balance as of " + sdate;
        else
            lblLeaveBalance.Text = "No leave transaction";
    }
    #endregion
    #region BindLeaveTrans
    private void BindLeaveTrans(bool retainsort)
    {
        gvLTrans.DataSource = SortDataTable(GetLeaveTrans().Tables[0], retainsort);
        gvLTrans.DataBind();
    }
    #endregion
    #region GetLeaveTrans
    private DataSet GetLeaveTrans()
    {
        string sdate = "1/1/" + ddlYear.SelectedItem.Text;
        string edate = "12/31/" + ddlYear.SelectedItem.Text;

        SqlParameter[] spParams = new SqlParameter[] {
                    new SqlParameter("@DBName", _KioskSession.DB)
                    ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                    ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                    ,new SqlParameter("@StartDate", sdate )
                    ,new SqlParameter("@EndDate", edate )
                    ,new SqlParameter("@year", int.Parse(ddlYear.SelectedItem.Text) )
                };
        
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveEarnedTakenByEmployeeID", spParams);
        return ds;
    }
    #endregion
    #region GetLeaveCredit
    private DataSet GetLeaveCredit()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveBalanceByEmployeeID", spParams);
        return ds;
    }
    #endregion


    #endregion

    #region btnApply_Click
    protected void btnApply_Click(object sender, EventArgs e)
    {
        Response.Redirect("LeaveEmployee.aspx");

        /*
        SecureUrl url=new SecureUrl("LeaveEmployee.aspx?EmployeeID="+_KioskSession.EmployeeID
                                                + "&ApproverID="+_KioskSession.LeaveApproverID
                                                + "&Updater=employee");
        Response.Redirect(url.ToString());
        */
    }
    #endregion

    protected void gvLTrans_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvLTrans.PageIndex;
        BindLeaveTrans(false);

        gvLTrans.PageIndex = pageIndex;
    }
    protected void gvLTrans_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvLTrans.DataKeys[e.RowIndex].Values["ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveDelete",
            new SqlParameter[] { new SqlParameter("@ID", key), 
                                 new SqlParameter("@LastUpdBy", _KioskSession.EmployeeID),
                                 new SqlParameter("@LastUpdDate", DateTime.Now),
                                 new SqlParameter("@DBname", _KioskSession.DB) });
        gvLTrans.EditIndex = -1;
        BindLeaveTrans(false);

    }
    protected void btnFU_Click(object sender, EventArgs e)
    {
        EmpHeader1.UpdateURL();
        BindLeaveTrans(false);
    }

    protected void gvLTrans_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //... for LOWE, leave deletion is done in Work distribution entries

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (e.Row.RowIndex != gvLTrans.EditIndex)
        //    {
        //        string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().Trim().ToLower();

        //        ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
        //        if (!(status == "for approval" || status == "declined"))
        //            ibDelete.Visible = false;
        //        else
        //            ibDelete.Visible = true;

        //    }
        //}
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindLeaveTrans(false);
    }
}
