<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveEmployee.aspx.cs" Inherits="Attendance_LeaveEmployee" %>
<%@ Register src="EmployeeHeader.ascx" tagname="EmployeeHeader" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript">
function toggledisplay(n)
{
    var sl = document.getElementById('<%= singleleave.ClientID %>');
    var ml = document.getElementById('<%= multileave.ClientID %>');

    //alert(n);
    if (n != 2) {
        sl.style.display = "";
        ml.style.display = "none";
    } 
    else {
        sl.style.display = "none";
        ml.style.display = "";
    }
}
</script>
    <asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="Filing of Leave"></asp:Label><br />    
    <br />
    <hr /> 
    <table class="TableBorder1">
        <tr>
            <td align="right">
                Date filed:</td>
            <td colspan="2">
                <asp:TextBox ID="txtDateFiled" runat="server" CssClass="TextBoxDate" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" valign="bottom">
                &nbsp;</td>
            <td valign="top">
                <asp:RadioButton ID="rbWholeday" runat="server" Text="Whole day leave (1 day)" 
                    Checked="True" GroupName="daygrp" onclick="toggledisplay(0);" />
                <br />
                <asp:RadioButton ID="rbHalfday" runat="server" Text="Half day leave (0.5 day)" 
                    GroupName="daygrp" onclick="toggledisplay(1);" />
                <br />
                <asp:RadioButton ID="rbMultiple" runat="server" Text="More than one day leave" 
                    GroupName="daygrp" onclick="toggledisplay(2);" />
                <br />
                <br />
            </td>
            <td align="left" valign="top">
            
            <table id="singleleave" runat="server">
                    <tr>
                        <td>Date of Leave:</td>
                        <td>
                            <asp:TextBox ID="txtDateLeave" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateLeave" runat="server" 
                                ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" 
                                ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator ID="mevDateLeave" runat="server" 
                                ControlExtender="meeDateLeave" ControlToValidate="txtDateLeave" Display="None" 
                                EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                ErrorMessage="*" InvalidValueBlurredMessage="*" 
                                InvalidValueMessage="Cannot accept invalid value" IsValidEmpty="False" 
                                MinimumValue="01/01/2008" MinimumValueBlurredText="*" 
                                MinimumValueMessage="Cannot accept date">*
                            </ajaxToolkit:MaskedEditValidator>                        
                        </td>
                    </tr>
                </table>                                
                <table id="multileave" style="display:none" runat="server">
                    <tr>
                        <td align="right">
                            Date start:</td>
                        <td>
                            <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateStart" runat="server" 
                                ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" 
                                ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator ID="mevDateStart" runat="server" 
                                ControlExtender="meeDateStart" ControlToValidate="txtDateStart" Display="None" 
                                EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                ErrorMessage="*" InvalidValueBlurredMessage="*" 
                                InvalidValueMessage="Cannot accept invalid value" IsValidEmpty="False" 
                                MinimumValue="01/01/2008" MinimumValueBlurredText="*" 
                                MinimumValueMessage="Cannot accept date">*</ajaxToolkit:MaskedEditValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Date end:</td>
                        <td>
                            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtDateEnd" runat="server" 
                                ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" 
                                ToolTip="Click to choose date" />
                            <ajaxToolkit:MaskedEditValidator ID="mevDateEnd" runat="server" 
                                ControlExtender="meeDateEnd" ControlToValidate="txtDateEnd" Display="None" 
                                EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                                ErrorMessage="*" InvalidValueBlurredMessage="*" 
                                InvalidValueMessage="Cannot accept invalid value" IsValidEmpty="False" 
                                MinimumValue="01/01/2008" MinimumValueBlurredText="*" 
                                MinimumValueMessage="Cannot accept date">*
                            </ajaxToolkit:MaskedEditValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                </table>
            
            </td>
        </tr>
        <tr>
            <td align="right">
                Leave:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlLeave" runat="server" CssClass="ControlDefaults" Width="200px" DataTextField="Description" DataValueField="Code">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvLeave" runat="server" ErrorMessage="Leave code is required" ControlToValidate="ddlLeave">*</asp:RequiredFieldValidator>                 
                                            </td>
        </tr>
        <tr>
            <td align="right">
                Reason for leave:</td>
            <td colspan="2">
                <asp:TextBox ID="txtReason" runat="server" CssClass="ControlDefaults" Height="42px"
                    TextMode="MultiLine" Width="501px" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReason"
                    ErrorMessage="Reason for leave cannot be blank">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;
                    <asp:Button ID="btnSubmit" runat="server" CssClass="ControlDefaults" Text="Submit" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="ControlDefaults" Text="Cancel" CausesValidation="False" OnClick="btnCancel_Click" />&nbsp;<asp:Label 
                    ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    <hr />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium"
        Font-Underline="False" Text="Leave Balances"></asp:Label>&nbsp;<br />
    <br />
    <asp:GridView ID="gvLCredit" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
        Width="500px">
        <Columns>
            <asp:BoundField DataField="LeaveDescr" HeaderText="Leave" />
            <asp:BoundField DataField="DaysEarned" HeaderText="Earned" DataFormatString="{0:F2}" />
            <asp:BoundField DataField="DaysApproved" HeaderText="Availed" DataFormatString="{0:F2}" />
            <asp:BoundField DataField="DaysForApproval" HeaderText="Pending" DataFormatString="{0:F2}" />
            <asp:BoundField DataField="Balance" HeaderText="Balance" DataFormatString="{0:F2}" />
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>

    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>
        
    <ajaxToolkit:MaskedEditExtender ID="meeDateLeave" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateLeave">
    </ajaxToolkit:MaskedEditExtender>
    
    <ajaxToolkit:CalendarExtender ID="caleDateLeave" runat="server" PopupButtonID="ibtxtDateLeave"
        TargetControlID="txtDateLeave">
    </ajaxToolkit:CalendarExtender>
    
</asp:Content>
