<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveBalances.aspx.cs" Inherits="Attendance_LeaveBalances"  %>
<%@ Register src="EmpHeaderDate.ascx" tagname="EmpHeaderDate" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>
    <uc1:EmpHeaderDate ID="EmpHeader1" runat="server" />    
    <div style="text-align:right">
    <table style="width:100%;">
        <tr>
            <td></td>
            <td>Year</td>
            <td style="width:170px"><asp:DropDownList ID="ddlYear" runat="server" Width="80px">
            </asp:DropDownList>&nbsp;&nbsp;<asp:Button ID="btnRefresh" runat="server" Text="Refresh" onclick="btnRefresh_Click" />
            </td>
            <td style="width:30%">
                
            </td>
        </tr>
    </table>
    </div>
    <hr />
    <asp:Label ID="lblLeaveBalance" runat="server" Text="Leave Balance as of: " Font-Bold="True"></asp:Label>    
    <br />
    <asp:GridView ID="gvLCredit" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle" Width="500px">
        <Columns>
            <asp:BoundField DataField="Code" HeaderText="Leave" />
            <asp:BoundField DataField="Earned" HeaderText="Earned" />
            <asp:BoundField DataField="Taken" HeaderText="Taken" />
            <asp:BoundField DataField="Balance" HeaderText="Balance" />
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>
    <br />
    <strong>Leave Transactions</strong><br />
    
    <asp:GridView ID="gvLTrans" runat="server" AutoGenerateColumns="False" 
        CssClass="DataGridStyle" Width="950px" AllowSorting="True" 
        onsorting="gvLTrans_Sorting" DataKeyNames="ID" 
        onrowdatabound="gvLTrans_RowDataBound" 
        onrowdeleting="gvLTrans_RowDeleting">
        <Columns>
            <asp:BoundField DataField="Code" HeaderText="Leave" SortExpression="Code" />
            <asp:BoundField DataField="DateFiled" HeaderText="Date filed" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="DateFiled" />
            <asp:BoundField DataField="StartDate" HeaderText="Start date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="StartDate"  />
            <asp:BoundField DataField="EndDate" HeaderText="End date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="EndDate" />

            <asp:BoundField DataField="Earned" HeaderText="Leave Earned" >
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Days" HeaderText="Leave Taken"  >

                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>

            <asp:BoundField DataField="Reason" HeaderText="Reason" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />

            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ibDelete" runat="server" ImageUrl="~/Graphics/delete.gif" CommandName="delete" ToolTip="Delete" Visible ="false" 
                        OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );"></asp:ImageButton>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" Width="42px"></ItemStyle>
            </asp:TemplateField>

        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>
    <asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none" />
</asp:Content>

