﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;  

public partial class LeaveCreditsEntry : KioskPageUI
{
    string _EmployeeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        _EmployeeID = _Url["EmpID"];

        base.GetUserSession();
        if (!IsPostBack)
        {
            lblFullname.Text = _Url["EmpName"];
            BindGrid();
        }
    }

    private DataSet GetData()
    {
        string sql = "SELECT " +
                       "	b.Trandate, " +
                       "	a.LeaveCode, " +
                       "	a.Earned, " +
                       "	b.ValidFrom, " +
                       "	b.ValidTo, " +
                       "	b.Remarks, " +
                       "	c.[Description] as LeaveDesc " +
                       "FROM dbo.LeaveEarnDtl a " +
                       "INNER JOIN dbo.LeaveEarn b ON a.CompanyID = b.CompanyID and a.LeaveEarnID = b.LeaveEarnID " +
                       "INNER JOIN dbo.LeaveCode c ON c.CompanyID = a.CompanyID and c.Code = a.LeaveCode " +
                       "WHERE  " +
                       "	a.CompanyID = {0} AND  " +
                       "	a.EmployeeID = {1} " +
                       "ORDER BY b.Trandate ";

        object[] parameters = { _KioskSession.CompanyID, _EmployeeID };
        LeaveEarn lv = new LeaveEarn();
        lv.DynamicQuery(sql, parameters);
        return lv.ToDataSet();
    }

    private void BindGrid()
    {
        gvEmp.DataSource = GetData().Tables[0].DefaultView;  
        gvEmp.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {        
        DataSet ds = GetData();
        DataRow dr = ds.Tables[0].NewRow();
        dr["Trandate"] = DateTime.Now;
        ds.Tables[0].Rows.Add(dr); 
        
        gvEmp.DataSource = ds.Tables[0];
        gvEmp.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvEmp.DataBind();
        hfIsAdd.Value = "1";
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            if (e.Row.RowIndex == gvEmp.EditIndex)
            {
                DropDownList ddlLeave = e.Row.FindControl("ddlLeave") as DropDownList;
                DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                    " SELECT RTRIM(Code) AS CODE, Description FROM " + _KioskSession.DB + ".dbo.LeaveCode a WHERE CompanyID = @CompanyID "
                    , new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

                ddlLeave.DataSource = ds.Tables[0];
                ddlLeave.DataBind();
                ddlLeave.SelectedValue = ddlLeave.Items.FindByValue("VL").Value;
            }
        }
    }

    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox txtTranDate = gvEmp.Rows[e.RowIndex].FindControl("txtTranDate") as TextBox;
        DropDownList ddlLeave = gvEmp.Rows[e.RowIndex].FindControl("ddlLeave") as DropDownList;
        TextBox txtLD = gvEmp.Rows[e.RowIndex].FindControl("txtLD") as TextBox;
        TextBox txtValidFrom = gvEmp.Rows[e.RowIndex].FindControl("txtValidFrom") as TextBox;
        TextBox txtValidTo = gvEmp.Rows[e.RowIndex].FindControl("txtValidTo") as TextBox;
        TextBox txtRemarks = gvEmp.Rows[e.RowIndex].FindControl("txtRemarks") as TextBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _Url["EmpID"]));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        sqlparam.Add(new SqlParameter("@TranDate", txtTranDate.Text ));
        sqlparam.Add(new SqlParameter("@LeaveCode", ddlLeave.SelectedItem.Value));
        sqlparam.Add(new SqlParameter("@Earned", txtLD.Text ));

        if (txtValidFrom.Text != "" && txtValidTo.Text != "")
        {
            sqlparam.Add(new SqlParameter("@ValidFrom", txtValidFrom.Text));
            sqlparam.Add(new SqlParameter("@ValidTo", txtValidTo.Text));
        }

        sqlparam.Add(new SqlParameter("@Remarks", txtRemarks.Text ));
        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

        //string ErrMsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();
        //if (ErrMsg != "")
        //    ScriptManager.RegisterStartupScript(this, typeof(Page), "OTError", "alert('" + ErrMsg + "');", true);

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCreditInsert", sqlparam.ToArray());

        gvEmp.EditIndex = -1;
        gvEmp.SelectedIndex = -1;
        BindGrid();
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        gvEmp.EditIndex = -1;
        gvEmp.EditIndex = -1;
        BindGrid();
    }
}