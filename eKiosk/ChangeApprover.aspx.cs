﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class ChangeApprover : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        lblStatus.Text = "";

        if (!IsPostBack)
        {
            SecureUrl urlAppr = new SecureUrl(string.Format(@"popupApprover.aspx?hfID={0}&lbID={1}&compID={2}", hfApprover.ClientID, lblApprover1.ClientID, hfCompanyID.ClientID));
            btnChange.Attributes.Add("onclick", string.Format("openSelectEmp('{0}');return false;", urlAppr));

            PopulateScreen();        
        }
    }

    private void RefreshScreen()
    {
        SqlParameter[] spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", hfCompanyID.Value)
                ,new SqlParameter("@ApproverID", hfApprover.Value)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproversGetByApprover1", spParams);
        DataRowCollection dr = ds.Tables[0].Rows;

        if (dr.Count > 0)
        {
            lblApprover1.Text = dr[0]["Approver1Name"].ToString();
            lblApprover2.Text = dr[0]["Approver2Name"].ToString();
            lblApprover3.Text = dr[0]["Approver3Name"].ToString();
        }
        else
        {
            lblApprover1.Text = "None";
            lblApprover2.Text = "None";
            lblApprover3.Text = "None";
        }

        btnSave.Enabled = true;
    }

    private void PopulateScreen()
    {
        SqlParameter[] spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproversByEmployeeGet", spParams);
        DataRowCollection dr = ds.Tables[0].Rows;

        if (dr.Count > 0)
        {
            lblApprover1.Text = string.Format("{0} ({1})", dr[0]["Approver1Name"].ToString().Trim(), dr[0]["Approver1"].ToString());
            lblApprover2.Text = string.Format("{0} ({1})", dr[0]["Approver2Name"].ToString().Trim(), dr[0]["Approver2"].ToString());
            lblApprover3.Text = string.Format("{0} ({1})", dr[0]["Approver3Name"].ToString().Trim(), dr[0]["Approver3"].ToString());
        }
        else
        {
            lblApprover1.Text = "None";
            lblApprover2.Text = "None";
            lblApprover3.Text = "None";
        }
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        RefreshScreen();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlParameter[] spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverID", hfApprover.Value)
                ,new SqlParameter("@ApproverCompID", hfCompanyID.Value)
            };

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ApproversByEmployeeUpdate", spParams);

        _KioskSession.OTApproverID = hfApprover.Value;
        _KioskSession.OTApproverName = lblApprover1.Text;

        base.SetUserSession(_KioskSession);

        EmpHeader1.GetUserSession();
        EmpHeader1.PopulateScreenDefaults();
        
        lblStatus.Text = "Change of approver was successful";

    }
}
