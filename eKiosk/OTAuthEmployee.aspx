<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="OTAuthEmployee.aspx.cs" Inherits="Attendance_OTAuthEmployee"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    
    <asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="File Overtime Authorization"></asp:Label>
    <br />
    <br />        
    <hr />
    <table class="TableBorder1" style="font-family: Verdana">
        <tr>
            <td align="right">
                Date filed:</td>
            <td>
                <asp:TextBox ID="txtDateFiled" runat="server" CssClass="TextBoxDate" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Overtime date:</td>
            <td>
                <asp:TextBox ID="txtOTDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtOTDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" OnClientClick="return false;" />
                <ajaxToolkit:MaskedEditValidator id="mevOTDate" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtOTDate" 
                    ControlExtender="meeOTDate" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" Display="None" >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Overtime start:</td>
            <td>
                <asp:TextBox ID="txtin1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox>
                <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1"
                    ControlToValidate="txtin1" Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Time is invalid" EmptyValueMessage="Overtime start cannot be blank" IsValidEmpty="False"></ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Overtime end:</td>
            <td>
                <asp:TextBox ID="txtout1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox>
                <ajaxToolkit:MaskedEditValidator
                    ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="txtout1"
                    Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid" EmptyValueMessage="Overtime end cannot be blank" IsValidEmpty="False"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
        <tr style="display:none; color: #000000">
            <td align="right">
                Approved number of hours:</td>
            <td>
                <asp:TextBox ID="txtOTHours" runat="server" CssClass="ControlDefaults" Width="40px">0.00</asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Reason:</td>
            <td>
                <asp:DropDownList ID="ddlReason" runat="server" Width="507px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                Remarks:</td>
            <td>
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="ControlDefaults" Height="74px"
                    MaxLength="100" TextMode="MultiLine" Width="501px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlReason"
                    ErrorMessage="Reason for overtime cannot be blank">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
            <td>
                &nbsp;
                <asp:Button ID="btnSubmit" runat="server" CssClass="ControlDefaults" OnClick="btnSubmit_Click"
                    Text="Submit" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="ControlDefaults"
                    OnClick="btnCancel_Click" Text="Cancel" />
                <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    <ajaxToolkit:MaskedEditExtender ID="meeOTDate" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtOTDate">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleOTDate" runat="server" PopupButtonID="ibtxtOTDate"
        TargetControlID="txtOTDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeOTHours" runat="server" ErrorTooltipEnabled="True"
        InputDirection="RightToLeft" Mask="99.99" MaskType="Number" MessageValidatorTip="true"
        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtOTHours">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtin1">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtout1">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>

