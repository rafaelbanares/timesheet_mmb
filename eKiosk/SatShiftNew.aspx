<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="SatShiftNew.aspx.cs" Inherits="SatShiftNew" Title="Create Saturday Shift for the Year" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <table cellpadding="0" class="TableBorder1" style="width: 400px">
        <tr>
            <td class="DataGridHeaderStyle" colspan="2">
                Generate Saturday Shift
            </td>
        </tr>
        <tr>
            
            <td align="right">
                Saturday Date Start (Working):
            </td>
            <td align="left">
                <asp:TextBox ID="txtSatStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtSatStart" runat="server" 
                    ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" 
                    ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator ID="mevSatStart" runat="server" 
                    ControlExtender="meeSatStart" ControlToValidate="txtSatStart" 
                    EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    ErrorMessage="*" InvalidValueBlurredMessage="*" 
                    InvalidValueMessage="Cannot accept invalid value" IsValidEmpty="False" 
                    MinimumValue="01/01/2008" MinimumValueBlurredText="*" 
                    MinimumValueMessage="Cannot accept date">*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Saturday Date End:</td>
            <td align="left">
                <asp:TextBox ID="txtSatEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtSatEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    OnClientClick="return false;" ToolTip="Click to choose date" />

                <ajaxToolkit:MaskedEditValidator id="mevSatEnd" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtSatEnd" 
                    ControlExtender="meeSatEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="Button1" runat="server" Text="Generate" 
                    onclick="Button1_Click" />
            </td>
        </tr>
    </table>
    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
    <br />

    <ajaxToolkit:MaskedEditExtender ID="meeSatStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtSatStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleSatStart" runat="server" PopupButtonID="ibtxtSatStart"
        TargetControlID="txtSatStart">
    </ajaxToolkit:CalendarExtender>
    
    <ajaxToolkit:MaskedEditExtender ID="meeSatEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtSatEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleSatEnd" runat="server" PopupButtonID="ibtxtSatEnd"
        TargetControlID="txtSatEnd">
    </ajaxToolkit:CalendarExtender>
    
</asp:Content>

