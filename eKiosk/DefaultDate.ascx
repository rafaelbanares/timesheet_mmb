﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefaultDate.ascx.cs" Inherits="DefaultDate" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 388px; height: 29px;">
    <tr>
        <td>Period&nbsp;&nbsp;</td>
        <td>
            <asp:TextBox ID="txtPrdFrom" runat="server" Height="17px" Width="80px"></asp:TextBox>
            <asp:ImageButton ID="ibtxtPrdFrom" runat="server" ImageUrl="~/Graphics/calendar.gif"
                OnClientClick="return false;" ToolTip="Click to choose date" />
            <ajaxToolkit:CalendarExtender ID="txtPrdFrom_CE" runat="server" Enabled="True" TargetControlID="txtPrdFrom" PopupButtonID="ibtxtPrdFrom">
            </ajaxToolkit:CalendarExtender>
        </td>
        <td>&nbsp;&nbsp;To&nbsp;</td>
        <td>
            <asp:TextBox ID="txtPrdTo" runat="server" Height="17px" Width="80px"></asp:TextBox>
            <asp:ImageButton ID="ibtxtPrdTo" runat="server" ImageUrl="~/Graphics/calendar.gif"
                OnClientClick="return false;" ToolTip="Click to choose date" />
            <ajaxToolkit:CalendarExtender ID="txtPrdTo_CE" runat="server" Enabled="True" TargetControlID="txtPrdTo" PopupButtonID="ibtxtPrdTo">
            </ajaxToolkit:CalendarExtender>&nbsp;
        </td>
        <td><asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" Text="Search" />
        </td>
    </tr>
</table>
