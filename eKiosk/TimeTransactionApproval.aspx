﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="TimeTransactionApproval.aspx.cs" Inherits="TimeTransactionApproval" Title="Time Transaction Approval" %>
<%@ Register src="EmpHeaderFilter3.ascx" tagname="ucFilter" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
	<script type="text/javascript" src="css/common_uno.js"></script>    
	<script type="text/javascript">
		function ValidateGrp(grp) {
			Page_ClientValidate(grp);
		}
		//close popup on esc
		function pageLoad(sender, args) {
			if (!args.get_isPartialLoad()) {
				$addHandler(document, "keydown", onKeyDown);
			}
		}
		function onKeyDown(e) {
			if (e && e.keyCode == Sys.UI.Key.esc) {
				$find('ctl00_phmc_mpe').hide();
			}
		}   
	</script>

<asp:UpdatePanel id="up1" runat="server">
	<ContentTemplate>    
	<asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="DTR For My Approval"></asp:Label>
	<br />
	<br />        
	<table style="width: 661px">
		<tr>
			<td>Employee:</td>
			<td><asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" Width="220px"></asp:TextBox></td>
			<td>Date From:</td>
			<td>
				<asp:TextBox ID="txtPrdFrom" runat="server" Height="17px" Width="70px"></asp:TextBox>
				<ajaxToolkit:CalendarExtender ID="txtPrdFrom_CE" runat="server" Enabled="True" TargetControlID="txtPrdFrom" PopupButtonID="ibtxtPrdFrom">
				</ajaxToolkit:CalendarExtender>
				<asp:ImageButton ID="ibtxtPrdFrom" runat="server" ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" ToolTip="Click to choose date" />
				<ajaxToolkit:MaskedEditValidator ID="mevPrdFrom" runat="server" ControlExtender="meePrdFrom" ControlToValidate="txtPrdFrom" MinimumValue="01/01/2000" >*
				</ajaxToolkit:MaskedEditValidator>
			</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>
				<asp:DropDownList ID="ddlDisplay" runat="server" CssClass="ControlDefaults" 
				  onselectedindexchanged="ddlDisplay_SelectedIndexChanged" 
				  AutoPostBack="True" Width="225px">
				  <asp:ListItem Value="for approval">For Approval</asp:ListItem>
				  <asp:ListItem>Invalid Entries</asp:ListItem>
				  <asp:ListItem Value="approved">Approved</asp:ListItem>
				  <asp:ListItem Value="declined">Declined</asp:ListItem>
				  <asp:ListItem Value="all">All</asp:ListItem>
			  </asp:DropDownList>
			</td>
			<td>Date To:</td>
			<td>
				<asp:TextBox ID="txtPrdTo" runat="server" Height="17px" Width="70px"></asp:TextBox>
				<ajaxToolkit:CalendarExtender ID="txtPrdTo_CE" runat="server" Enabled="True" TargetControlID="txtPrdTo" PopupButtonID="ibtxtPrdTo">
				</ajaxToolkit:CalendarExtender>
				<asp:ImageButton ID="ibtxtPrdTo" runat="server" ImageUrl="~/Graphics/calendar.gif" OnClientClick="return false;" ToolTip="Click to choose date" />
				<ajaxToolkit:MaskedEditValidator ID="mevPrdTo" runat="server" ControlExtender="meePrdTo" ControlToValidate="txtPrdTo"  MinimumValue="01/01/2000" >*
				</ajaxToolkit:MaskedEditValidator>
			</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3">
				<asp:Button ID="btnRefresh" runat="server" Text = "Search" 
					onclick="btnRefresh_Click" />&nbsp;
				</td>
		</tr>
	</table>
	<div style="display:none">
		<ajaxToolkit:MaskedEditExtender ID="meePrdFrom" runat="server" ErrorTooltipEnabled="True"
			Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
			OnInvalidCssClass="MaskedEditError" TargetControlID="txtPrdFrom">
		</ajaxToolkit:MaskedEditExtender>
		<ajaxToolkit:MaskedEditExtender ID="meePrdTo" runat="server" ErrorTooltipEnabled="True"
			Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
			OnInvalidCssClass="MaskedEditError" TargetControlID="txtPrdTo">
		</ajaxToolkit:MaskedEditExtender>
	</div>
	<hr />
	<asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
		AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
			AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
			OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
			OnRowEditing="gvEmp_RowEditing" Width="100%" 
			DataKeyNames="EmployeeID,Date" onrowdeleting="gvEmp_RowDeleting" 
			onselectedindexchanged="gvEmp_SelectedIndexChanged" 
			onrowcommand="gvEmp_RowCommand">
		<Columns>
			<asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
				<ItemTemplate>
					<asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
					<asp:HiddenField ID="hfCompanyID" runat="server" Value='<%# Bind("CompanyID") %>' />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Employee Name" DataField="FullName" 
				SortExpression="FullName" ItemStyle-Width="250px" >
			<ItemStyle Width="250px" />
			</asp:BoundField>
			<asp:BoundField HeaderText="Date" DataField="Date" SortExpression="Date" 
				DataFormatString="{0:d}" ItemStyle-Width="80px" >
			<ItemStyle Width="80px" />
			</asp:BoundField>
			<asp:BoundField DataField="orig_in1" HeaderText="Actual<br> Time IN" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="100px" HtmlEncode="False" >
			<ItemStyle Width="100px" />
			</asp:BoundField>
			<asp:BoundField DataField="orig_out1" HeaderText="Actual<br>Time OUT" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="100px" HtmlEncode="False" >
			<ItemStyle Width="100px" />
			</asp:BoundField>
			<asp:BoundField DataField="in1" HeaderText="New<br> Time IN" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="100px" HtmlEncode="False" >
			<ItemStyle Width="100px" />
			</asp:BoundField>
			<asp:BoundField DataField="out1" HeaderText="New<br>Time OUT" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="100px" HtmlEncode="False" >
			<ItemStyle Width="100px" />
			</asp:BoundField>

			<asp:TemplateField HeaderText="Reason" ItemStyle-Width="150px">
				<ItemTemplate>
					<asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>                    
				</ItemTemplate>
				<ItemStyle Width="150px" />
			</asp:TemplateField>

			<asp:TemplateField HeaderText="Status" ItemStyle-Width="100px">
				<ItemTemplate>
					<asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>                    
				</ItemTemplate>
				<ItemStyle Width="100px" />
			</asp:TemplateField>

			<asp:TemplateField ItemStyle-Width="62px">
				<ItemTemplate>
					<asp:ImageButton ID="ibApproved" runat="server" ToolTip="Approve" ImageUrl="~/Graphics/check.gif" CommandName="Select" />
					<asp:ImageButton ID="ibRecall" runat="server" CommandName="Recall" ToolTip="Recall" ImageUrl="~/Graphics/cancel.gif" OnClientClick="return window.confirm('Recall DTR?' );" />
					<asp:ImageButton ID="ibDeny" runat="server" ToolTip="Decline" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm('Decline DTR?' );" CommandName="Delete" />
					<asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
					<asp:ImageButton ID="ibEdit" runat="server" 
						ImageUrl="~/Graphics/edit_small.gif" OnClick="ibEdit_Click" ToolTip="Edit" />
					<asp:HiddenField ID="hfDate" runat="server" Value='<%# Bind("Date") %>' />
				</ItemTemplate>
				<ItemStyle Width="62px" />
			</asp:TemplateField>
			
		</Columns>
		<RowStyle Height="22px" />
		<EmptyDataTemplate>
			<span style="color:Red">No record found</span>
		</EmptyDataTemplate>
		<PagerStyle HorizontalAlign="Right" />
		<HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
		<SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
	</asp:GridView>
	<br />
	<br />
	<table>
		<tr align='left'>
			<td style="width:220px">
				<asp:Button ID="btnForApproval" runat="server" 
                    Text="Approve all - For Approval" Width="220px" 
                    onclick="btnForApproval_Click" Visible="False" /></td>
			<td style="width:50px">
				<asp:Button ID="Button1" runat="server" Text="Button" style="display:none" />
			</td>
			<td style="width:220px">
				&nbsp;</td>
		</tr>
	</table>
	<asp:HiddenField ID="hdEmpID" runat="server" />
	<asp:Panel ID="pnlDecline" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
		<div><br />
			<table style="width:100%;" cellpadding="1" cellspacing="1">
				<tr>
					<td>Reason</td>
					<td>
						<asp:TextBox ID="txtDecReason" runat="server" CssClass="ControlDefaults" 
							Height="65px" MaxLength="100" TextMode="MultiLine" 
							ToolTip="" Width="340px"></asp:TextBox>
						<asp:RequiredFieldValidator ID="rfvDecReason" runat="server" ErrorMessage="required" ControlToValidate="txtDecReason" ValidationGroup="declinepop"></asp:RequiredFieldValidator>
					</td>                    
				</tr>
				<tr>
					<td colspan="2" style="width:100%; text-align:center">
						<asp:HiddenField ID="HiddenField1" runat="server" />
					<br />
						<asp:Button ID="btnDecline" runat="server" 
							OnClientClick="ValidateGrp('declinepop')" Text="Decline" 
							onclick="btnDecline_Click" />
						&nbsp;
						<asp:Button ID="btnDecReasonCancel" runat="server" Text="Cancel" />                  
					</td>
				</tr>
			</table>
		</div>
	</asp:Panel>
	<ajaxToolkit:ModalPopupExtender ID="mpe3" runat="server"
	  TargetControlId="Button1"
	  PopupControlID="pnlDecline"
	  CancelControlID="btnDecReasonCancel"
	  BackgroundCssClass="modalBackground"  />
	  
		<asp:HiddenField ID="hfdecRowid" runat="server" />
	  
	</ContentTemplate>
	<Triggers>    
		<asp:AsyncPostBackTrigger ControlID="btnFU"></asp:AsyncPostBackTrigger>
	</Triggers>
</asp:UpdatePanel>
<asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none"  />

<%--------------------------------------------------------------- modal popup layout starts here -----------------------------%>
	<div id="pnlDtrEntry" style="width: 600px; display: none;" >    
		<div class="popup_Container">
			<div class="popup_Titlebar" id="PopupHeader">
				<div class="TitlebarLeft">Attendance Entries</div>
				<div class="TitlebarRight" onclick="$get('ctl00_phmc_btnPopCancel').click();"> </div>
			</div>
			<div class="popup_Body" style="width:98%;" >
				<asp:UpdatePanel runat="server" ID="UpdatePanel2">
				<ContentTemplate>
				<asp:HiddenField ID="hdDatePop" runat="server" />
				<asp:Label ID="lblPopErrorMsg" runat="server" ForeColor="Red"></asp:Label>
				<table style="border-color: #FFFFFF; width:95%; " border="0" border="1" cellpadding="1" cellspacing="1">
					<tr>
						<td colspan="2" style="width:100%; height:25; text-align:center; background-color:#4682b4; color:White;">
							Actual IN/OUT</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>IN</td>
						<td>
							<asp:Label ID="txtOrig_In" runat="server" Font-Bold="true" Width="150px"></asp:Label>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>OUT</td>
						<td>
							<asp:Label ID="txtOrig_Out" runat="server" Font-Bold="true" Width="150px"></asp:Label>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="width:100%; height:25; text-align:center; background-color:#4682b4; color:White;">
							Requested IN/OUT</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>IN</td>
						<td>
							<asp:TextBox ID="txtNewDateIn" runat="server" AutoPostBack="False" 
								OnChange="ValidateGrp('DtrEntryGrp');" ValidationGroup="DtrEntryGrp" Width="70px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="meNewDateIn" runat="server" 
								ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewDateIn">
							</ajaxToolkit:MaskedEditExtender>
							<asp:TextBox ID="txtNewTimeIn" runat="server" AutoPostBack="False" 
								ValidationGroup="DtrEntryGrp" Width="60px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="meNewIn" runat="server" AcceptAMPM="True" 
								ErrorTooltipEnabled="True" Mask="99:99" MaskType="Time" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewTimeIn">
							</ajaxToolkit:MaskedEditExtender>
							<ajaxToolkit:MaskedEditValidator ID="mveNewIn" runat="server" 
								ControlExtender="meNewIn" ControlToValidate="txtNewTimeIn" Display="Dynamic" 
								EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" 
								InvalidValueMessage="Time is invalid" ValidationGroup="DtrEntryGrp">
							</ajaxToolkit:MaskedEditValidator>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>OUT</td>
						<td>
							<asp:TextBox ID="txtNewDateOut" runat="server" AutoPostBack="False" 
								OnChange="ValidateGrp('DtrEntryGrp');" ValidationGroup="DtrEntryGrp" Width="70px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" 
								ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewDateOut">
							</ajaxToolkit:MaskedEditExtender>
							<asp:TextBox ID="txtNewTimeOut" runat="server" AutoPostBack="False" 
								ValidationGroup="DtrEntryGrp" Width="60px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="meNewOut" runat="server" AcceptAMPM="True" 
								ErrorTooltipEnabled="True" Mask="99:99" MaskType="Time" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewTimeOut">
							</ajaxToolkit:MaskedEditExtender>
							<ajaxToolkit:MaskedEditValidator ID="mveNewOut" runat="server" 
								ControlExtender="meNewOut" ControlToValidate="txtNewTimeOut" Display="Dynamic" 
								EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" 
								InvalidValueMessage="Time is invalid" ValidationGroup="DtrEntryGrp">
							</ajaxToolkit:MaskedEditValidator>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td valign="top">Remarks&nbsp;&nbsp;</td>
						<td>
							<asp:TextBox ID="txtNewRemarks" runat="server" CssClass="ControlDefaults" 
								Height="58px" MaxLength="100" TextMode="MultiLine" Width="417px" 
								ValidationGroup="DtrEntryGrp"></asp:TextBox>
							<asp:RequiredFieldValidator ID="rfvNewRem" runat="server" 
								ControlToValidate="txtNewRemarks" ErrorMessage="required" 
								ValidationGroup="DtrEntryGrp"></asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td valign="top">
							Status</td>
						<td>
							<asp:Label ID="txtStatus" runat="server" Font-Bold="False" Width="150px"></asp:Label>
						</td>
					</tr>
				</table>
				</ContentTemplate>
				</asp:UpdatePanel>
			</div>
			<hr style="color: #add8e6"/>
			<div class="popup_Buttons">
				<asp:Button ID="btnEditSave" runat="server" Text="Save" OnClientClick="ValidateGrp('DtrEntryGrp')" onclick="btnEditSave_Click"/>
				<asp:Button ID="btnPopCancel" runat="server" Text="Cancel" />
			</div>
		</div>
	</div>
	<%--------------------------------------------------------------- modal popup layout ends -----------------------------%>        

   <asp:LinkButton ID="lbEditInOut" runat="server"></asp:LinkButton>       
	<ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" 
		BackgroundCssClass="ModalPopupBG"
		CancelControlID="btnPopCancel" 
		TargetControlID="lbEditInOut"
		PopupControlID="pnlDtrEntry" 
		Drag="true" 
		PopupDragHandleControlID="PopupHeader">
	</ajaxToolkit:ModalPopupExtender>

</asp:Content>

