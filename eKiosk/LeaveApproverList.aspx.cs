﻿using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Business;
using BNS.TK.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LeaveApproverList : KioskPageUI
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserSession();
        
        if (_Url["approverno"] == string.Empty)
            Response.Redirect("LoginEmployee.aspx");
        
        if (!IsPostBack)
        {
            BindGrid();
            EmpHeader1.ApproverNo = _Url["approverno"];
        }
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        btnForApproval.Visible = (gvEmp.EditIndex == -1 && CountStatus(TKWorkFlow.Status.ForApproval) > 0);
    }

    private DataSet GetData()
    {
        string sdate = "01/01/" + EmpHeader1.Year;
        string edate = "12/31/" + EmpHeader1.Year;
        var approverNo = _Url["approverno"];

        string empWhere = "";
        if (approverNo == "4")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver4 = {4}) ";
        else if (approverNo == "3")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver3 = {4}) ";
        else if (approverNo == "2")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver2 = {4}) ";
        else if (approverNo == "1")
            empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver1 = {4}) ";

        string status = EmpHeader1.Display.ToLower();
        if (EmpHeader1.Display.IsEqual(TKWorkFlow.Status.ForApproval)  && (approverNo == "1" || approverNo == "2"))
            empWhere += " AND a.Stage = 0 ";
        else if (EmpHeader1.Display.IsEqual(TKWorkFlow.Status.ForApproval) && (approverNo == "3" || approverNo == "4"))
            empWhere += " AND a.Stage = 1 ";


        string sql = "SELECT " +
                        "	a.ID " +
                        "	,a.CompanyID " +
                        "	,a.EmployeeID " +
                        "	,e.FullName " +
                        "	,a.DateFiled " +
                        "	,a.StartDate " +
                        "	,a.EndDate " +
                        "	,a.[Days] " +
                        "	,a.Approver " +
                        "	,a.Code " +
                        "	,a.Reason " +
                        "	,a.DeclinedReason " +
                        "	,a.Stage " +
                        "	,a.[Status] " +
                        "	,a.Posted " +
                        "FROM LeaveTransHdr a INNER JOIN Employee e  " +
                        "ON a.CompanyId = e.CompanyID and a.EmployeeID = e.EmployeeID " +
                        "WHERE " +
                        "	a.CompanyID = {0} AND " +
                        "	(a.StartDate <= {2} AND a.EndDate >= {1} ) AND " +
                        "	({3}='' or a.[Status] = {3}) " + empWhere +
                        "   AND e.FullName like {5} " +
                        "ORDER BY Fullname, StartDate DESC ";

        object[] parameters = { _KioskSession.CompanyID, 
                                  sdate.ToDate(), 
                                  edate.ToDate(), 
                                  (EmpHeader1.Display.IsNullOrEmpty() || EmpHeader1.Display.ToLower()=="all") ? "" : EmpHeader1.Display.TrimEnd(), 
                                  _KioskSession.EmployeeID,
                                  EmpHeader1.Search + "%" };

        BNS.TK.Entities.LeaveTransHdr leaveHdr = new BNS.TK.Entities.LeaveTransHdr();
        leaveHdr.DynamicQuery(sql, parameters);

        return leaveHdr.ToDataSet();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                Label lblreason = e.Row.FindControl("lblreason") as Label;
                string decreason = DataBinder.Eval(e.Row.DataItem, "DeclinedReason").ToString().TrimEnd();
                string reason = lblreason.Text.TrimEnd();
                if (decreason.TrimEnd() != "") 
                    reason += " | " + decreason;

                if (reason.Length > 18)
                {
                    lblreason.ToolTip = reason;
                    lblreason.Text = reason.Substring(0, 15) + "...";
                }

                ImageButton ibRecall = e.Row.FindControl("ibRecall") as ImageButton;
                ImageButton ibApproved = e.Row.FindControl("ibApproved") as ImageButton;
                ImageButton ibDeny = e.Row.FindControl("ibDeny") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;
                Literal litLeave = e.Row.FindControl("litLeave") as Literal;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                bool isPosted = DataBinder.Eval(e.Row.DataItem, "Posted").ToString().ToBoolean();

                TKWorkFlow wf = new TKWorkFlow(status, isPosted);
                ibRecall.Visible = wf.HasRecallAction;
                ibApproved.Visible = wf.HasApproveAction;
                ibDeny.Visible = wf.HasDeclineAction;
                ibApproved.Attributes.Add("onclick", "return window.confirm('Approve Leave?' );");

                //if (isPosted)
                //    ibRecall.Visible = false;

                //check if locked
                string datefrom = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "StartDate").ToString(), _KioskSession.DateFormat);
                string dateto = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "StartDate").ToString(), _KioskSession.DateFormat);
                if (DateLockedForApprover(Convert.ToDateTime(datefrom)) || DateLockedForApprover(Convert.ToDateTime(dateto)))
                {
                    ibLocked.Visible = true;
                    ibApproved.Visible = false;
                    ibRecall.Visible = false;
                    ibDeny.Visible = false;

                }

                //hide for tobys
                //TODO: should be configurable
                if (_KioskSession.IsAdminMode)
                {
                    if (ibApproved != null)
                        ibApproved.Visible = false;
                    if (ibDeny != null)
                        ibDeny.Visible = false;
                }
            }
            else
            {
                string leavecode = DataBinder.Eval(e.Row.DataItem, "Code").ToString();
                string companyID = DataBinder.Eval(e.Row.DataItem, "CompanyID").ToString();

                DropDownList ddlLeave = e.Row.FindControl("ddlLeave") as DropDownList;
                //... populate leave dropdown
                //... notice that Code, Code was used not Code, Description to save screen space (UI will be garbled if description was used)
                DataSet dsLeave = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
                    "SELECT Code, Code as Description FROM " + _KioskSession.DB + ".dbo.LeaveCode WHERE CompanyID = @CompanyID",
                                new SqlParameter[] { new SqlParameter("@CompanyID", companyID) });

                ddlLeave.DataSource = dsLeave.Tables[0];
                ddlLeave.DataBind();
                ddlLeave.Items.Insert(0, "");

                ddlLeave.SelectedValue = leavecode;
            }

            Literal litStage = e.Row.FindControl("litStage") as Literal;
            litStage.Text = DataBinder.Eval(e.Row.DataItem, "Status").ToString(); //(litStage.Text == "1" ? "For Final Approval" : "For Approval");

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    private void ApprovedDeny(object sender, string newStatus, int rowindex, int newStage)
    {
        var id = gvEmp.DataKeys[rowindex].Values["id"].ToString().ToInt();

        var hdr = new LeaveTransHdr();
        if (hdr.LoadByPrimaryKey(id))
        {
            var userId = _KioskSession.UID;

            hdr.Status = newStatus;
            hdr.Stage = (short)newStage;
            hdr.Approver = userId;
            hdr.LastUpdBy = userId;
            hdr.LastUpdDate = DateTime.Now;

            if (newStatus == TKWorkFlow.Status.Declined)
                hdr.DeclinedReason = txtDecReason.Text;

            hdr.Save(); 
        }

        var empRow = gvEmp.Rows[rowindex];
        var companyId = ((HiddenField)empRow.FindControl("hfCompanyID")).Value;
        var employeeId = ((Literal)empRow.FindControl("litEmployeeID")).Text;
        var startDate = DateTime.Parse(((Literal)empRow.FindControl("litStartDate")).Text);
        var endDate = DateTime.Parse(((Literal)empRow.FindControl("litEndDate")).Text);

        //Helper.SendMail(Helper.GetEmailAddress(companyId, employeeId), "Kronos Leave change of status", "Your DTR was acted upon by your supervisor.");

        gvEmp.EditIndex = -1;
        BindGrid();

        ReprocessAttendance(employeeId, startDate, endDate);
    }

    private void Recall(object sender, int rowindex)
    {
        var row = gvEmp.Rows[rowindex];
        var employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
        var date = ((Literal)gvEmp.Rows[rowindex].FindControl("litDate")).Text.ToDate();
        var id = gvEmp.DataKeys[rowindex].Values["id"].ToString().ToInt();

        var hdr = new LeaveTransHdr();
        try
        {
            if (!hdr.LoadByPrimaryKey(id))
                throw new Exception("Leave Transaction not found.");

            if (hdr.Posted)
                throw new Exception("Cannot recall posted Leave Transaction.");

            hdr.Status = TKWorkFlow.Status.ForApproval;
            hdr.Stage = 0;
            hdr.Approver = " ";
            hdr.DeclinedReason = string.Empty;
            hdr.LastUpdBy = _KioskSession.UID;
            hdr.LastUpdDate = DateTime.Now;
            hdr.Save();
        }
        catch (SqlException sqex)
        {
            lblError.Text = sqex.Message;
        }

        gvEmp.EditIndex = -1;
        BindGrid();
        ReprocessAttendance(employeeId, date);
    }

    private void ReprocessAttendance(string empid, DateTime date, DateTime? date2 = null)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(date, date2 == null ? date : (DateTime)date2, empid);
    } 

    protected void btnForApproval_Click(object sender, EventArgs e)
    {
        var hdr = new LeaveTransHdr();
        var userId = _KioskSession.UID;
        var date = DateTime.Now;
        var approverNo = _Url["approverno"].ToInt();
        var startDate = new DateTime();
        var endDate = new DateTime();

        string status, newStatus, employeeId;
        short newStage;
        DateTime tempStartDate, tempEndDate;
        int id;
        bool approvable;

        foreach (GridViewRow row in gvEmp.Rows)
        {
            status = ((Literal)row.FindControl("litStage")).Text;
            approvable = ((ImageButton)row.FindControl("ibApproved")).Visible;

            if (status == TKWorkFlow.Status.ForApproval && approvable)
            {
                tempStartDate = DateTime.Parse(((Literal)row.FindControl("litStartDate")).Text);
                tempEndDate = DateTime.Parse(((Literal)row.FindControl("litEndDate")).Text);

                startDate = startDate == DateTime.MinValue ? tempStartDate : startDate > tempStartDate ? tempStartDate : startDate;
                endDate = endDate == DateTime.MinValue ? tempEndDate : endDate < tempEndDate ? tempEndDate : endDate;

                newStatus = TKWorkFlow.Status.Approved;
                newStage = 2;

                if (approverNo == 1 || approverNo == 2)
                {
                    employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
                    newStage = 1;

                    if (!IsFinalApprover(employeeId))
                        newStatus = TKWorkFlow.Status.ForApproval;
                }

                id = gvEmp.DataKeys[row.RowIndex].Values["id"].ToString().ToInt();

                if (hdr.LoadByPrimaryKey(id))
                {
                    hdr.Status = newStatus;
                    hdr.Stage = newStage;
                    hdr.Approver = userId;
                    hdr.LastUpdBy = userId;
                    hdr.LastUpdDate = date;
                    hdr.Save();
                }
            }   
        }

        if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
        {
            gvEmp.EditIndex = -1;
            BindGrid();

            ReprocessAttendance(string.Empty, startDate, endDate);
        }
    }

    private int CountStatus(string filterstatus)
    {
        int retval = 0;
        foreach (GridViewRow row in gvEmp.Rows)
        {
            Literal litStage = row.FindControl("litStage") as Literal;
            if (litStage.Text.ToLower() == filterstatus.ToLower())
                retval++;
        }
        return retval;
    }

    private bool IsFinalApprover(string empid)
    {
        var appr = new EmployeeApprover();
        var sql = "SELECT COUNT(1) FROM EmployeeApprover WHERE CompanyID = {0} AND EmployeeID = {1} "
            + "AND ((NULLIF(RTRIM(Approver3), '') IS NOT NULL OR (NULLIF(RTRIM(Approver4), '') IS NOT NULL)))";

        object[] parameters = { _KioskSession.CompanyID, empid.Trim() };
        appr.DynamicQuery(sql, parameters);

        return (int)appr.ToDataSet().Tables[0].Rows[0][0] == 0;
    }

    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        var approverNo = _Url["approverno"].ToInt();
        var newStatus = TKWorkFlow.Status.Approved;
        var newStage = 2;

        if (approverNo == 1 || approverNo == 2)
        {
            var employeeId = ((Literal)gvEmp.SelectedRow.FindControl("litEmployeeID")).Text;
            newStage = 1;

            if (!IsFinalApprover(employeeId))
                newStatus = TKWorkFlow.Status.ForApproval;
        }

        ApprovedDeny(sender, newStatus, gvEmp.SelectedIndex, newStage);
    }

    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        txtDecReason.Text = string.Empty;
        hfdecRowid.Value = e.RowIndex.ToString();
        mpe3.Show();
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper().TrimEnd() == "RECALL")
        {
            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            int rowIndex = row.RowIndex;
            Recall(sender, rowIndex);
        }
    }

    private bool DateLockedForApprover(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Approver_lock_start != "" && tp.s_Approver_lock_end != "")
            {
                return (DateTime.Today >= tp.Approver_lock_start && DateTime.Today <= tp.Approver_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }
    protected void btnDecline_Click(object sender, EventArgs e)
    {        
        int rowindex;
        if (int.TryParse(hfdecRowid.Value, out rowindex))
            ApprovedDeny(sender, TKWorkFlow.Status.Declined, rowindex, 0);
    }

    
}
