﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveSummary.aspx.cs" Inherits="LeaveSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<div><asp:Label runat="server" Text="My Leave Balances" Font-Size="Large"></asp:Label></div>
<br />    
    <div>
    <table style="width:100%;">
        <tr>
            <td style="width:15px">Year&nbsp;</td>
            <td style="width:270px"><asp:DropDownList ID="ddlYear" runat="server" Width="80px" 
                    AutoPostBack="True" onselectedindexchanged="ddlYear_SelectedIndexChanged">
            </asp:DropDownList>&nbsp;&nbsp;</td>
            <td>
            </td>
        </tr>
    </table>
    </div>
    <hr />
    <br />
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" Width="80%" onrowdatabound="gvEmp_RowDataBound" >
        <Columns>
            <asp:BoundField HeaderText="LeaveCode" DataField="LeaveCode"  ItemStyle-Width="100px" Visible="false"/>
            <asp:BoundField HeaderText="Leave Type" DataField="LeaveDescr"  ItemStyle-Width="200px" />
            <asp:BoundField HeaderText="Earned" DataField="DaysEarned" DataFormatString="{0:F2}" ItemStyle-Width="100px" /> 
            <asp:BoundField HeaderText="Availed" DataField="DaysApproved" DataFormatString="{0:F2}" ItemStyle-Width="100px" Visible="false" /> 
            <asp:TemplateField HeaderText="Availed" ItemStyle-Width="100px" >
                <ItemTemplate>
                    <asp:HyperLink ID="hlAvailed" runat="server" CssClass="linknoDecor" NavigateUrl="" Text='<%# Convert.ToDecimal(Eval("DaysApproved")).ToString("0.00") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Pending Approval" DataField="DaysForApproval" DataFormatString="{0:F2}"  ItemStyle-Width="100px" Visible="false"/>

            <asp:TemplateField HeaderText="Pending Approval" ItemStyle-Width="100px" >
                <ItemTemplate>
                    <asp:HyperLink ID="hlPending" runat="server" CssClass="linknoDecor" NavigateUrl="" Text='<%# Convert.ToDecimal(Eval("DaysForApproval")).ToString("0.00") %>' ></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Available Balance" DataField="Balance" DataFormatString="{0:F2}"  ItemStyle-Width="100px" />            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>    
</asp:Content>

