﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;
using BNS.TK.Entities;
using BNS.TK.Business;


public partial class TimeTransactionApproval : KioskPageUI
{
    
    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);        
    }
    #endregion

    
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();        
        if (!IsPostBack)
        {
            DefaultValues();
            BindGrid();
            ShowModuleCaption();
        }
    }

    private void DefaultValues()
    {
        txtPrdFrom.Text = _KioskSession.StartDate.ToString("MM/dd/yyyy");
        txtPrdTo.Text = _KioskSession.EndDate.ToString("MM/dd/yyyy");

    }

    private string ApproverNo()
    {
        if (_Url["approverno"] == null)
            return "";
        else
            return _Url["approverno"];
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        btnForApproval.Visible = (gvEmp.EditIndex == -1 && CountStatus(TKWorkFlow.Status.ForApproval) > 0);
    }
    private DataSet GetData()
    {
        string sdate = txtPrdFrom.Text;
        string edate = txtPrdTo.Text;

        string empWhere = "";
        if (ApproverNo() != "9")    //not admin
        {
            if (ApproverNo() == "4")
                empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver4 = {4}) ";
            else if (ApproverNo() == "3")
                empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver3 = {4}) ";
            else if (ApproverNo() == "2")
                empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver2 = {4}) ";
            else if (ApproverNo() == "1")
                empWhere = " AND a.EmployeeID <> {4} AND a.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver1 = {4}) ";

            //status filter            
            if (ddlDisplay.SelectedItem.Text.IsEqual("Invalid Entries"))
                empWhere += " AND a.Remarks = 'Invalid' ";
            else if (ddlDisplay.SelectedItem.Text.IsEqual(TKWorkFlow.Status.ForApproval) && (ApproverNo() == "1" || ApproverNo() == "2"))
                empWhere += " AND a.Stage = 0 ";
            else if (ddlDisplay.SelectedItem.Text.IsEqual(TKWorkFlow.Status.ForApproval) && (ApproverNo() == "3" || ApproverNo() == "4"))
                empWhere += " AND a.Stage = 1 ";
        }
        else
        {
            //status filter            
            if (ddlDisplay.SelectedItem.Text.IsEqual("Invalid Entries"))
                empWhere += " AND a.Remarks = 'Invalid' ";
        }

        string sql = "SELECT TOP 200 " +
                        "	a.CompanyID, " +
                        "	a.EmployeeID, " +
                        "	e.FullName, " +
                        "	a.Date, " +
                        "	a.orig_in1, " +
                        "	a.orig_out1, " +
                        "	a.in1, " +
                        "	a.out1, " +
                        "	a.[Status], " +
                        "	a.Stage, " +
                        "	a.Reason, " +
                        "	a.DeclinedReason, " +
                        "	a.Posted " +
                        "FROM Attendance a INNER JOIN Employee e " +
                        "ON a.CompanyID=e.CompanyID AND a.EmployeeID=e.EmployeeID " +
                        "WHERE " +
                        "	a.CompanyID = {0} AND " +
                        "	(a.[Date] between {1} AND {2} ) AND " +
                        "	({3}='' or a.[Status] = {3}) " + empWhere +
                        "   AND e.FullName + ' ' + e.EmployeeID like {5} " +
                        "ORDER BY a.EmployeeID, a.Date DESC ";

        object[] parameters = { _KioskSession.CompanyID,
                                  sdate.ToDate(),
                                  edate.ToDate(),
                                  (ddlDisplay.SelectedItem.Text.IsEqual("Invalid Entries") || ddlDisplay.SelectedItem.Text.ToLower()=="all") ? "" : ddlDisplay.SelectedItem.Text.TrimEnd(),
                                  _KioskSession.EmployeeID,
                                  "%" + txtSearch.Text.Trim() + "%" };

        BNS.TK.Entities.Attendance att = new BNS.TK.Entities.Attendance();
        att.DynamicQuery(sql, parameters);

        return att.ToDataSet();
    }


    private void ShowModuleCaption()
    {
        lblModule.Text = "DTR View as ";
        if (ApproverNo() == "1")
            lblModule.Text += "Level 1 Main Approver";
        else if (ApproverNo() == "2")
            lblModule.Text += "Level 1 Backup Approver";
        else if (ApproverNo() == "3")
            lblModule.Text += "Level 2 Main Approver";
        else if (ApproverNo() == "4")
            lblModule.Text += "Level 2 Backup Approver";
        else if (ApproverNo() == "9")
            lblModule.Text += "Administrator";
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                Label lblreason = e.Row.FindControl("lblreason") as Label;
                lblreason.ToolTip = lblreason.Text.TrimEnd();
                lblreason.Text = lblreason.Text.TrimEnd().ToEllipse(20);
                
                ImageButton ibRecall = e.Row.FindControl("ibRecall") as ImageButton;
                ImageButton ibApproved = e.Row.FindControl("ibApproved") as ImageButton;
                ImageButton ibDeny = e.Row.FindControl("ibDeny") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                bool isPosted = DataBinder.Eval(e.Row.DataItem, "Posted").ToString().ToBoolean();
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                 
                //approver actions
                TKWorkFlow wf = new TKWorkFlow(status, isPosted);
                ibRecall.Visible = wf.HasRecallAction;
                ibApproved.Visible = wf.HasApproveAction;
                ibDeny.Visible = wf.HasDeclineAction;
                ibEdit.Visible = _KioskSession.IsAdmin && (status.IsNullOrEmpty() || status.IsEqual(BNS.TK.Business.TKWorkFlow.Status.ForApproval));

                Literal litEmployeeID = e.Row.FindControl("litEmployeeID") as Literal;
                Label lblStatus = e.Row.FindControl("lblStatus") as Label;
                if (lblStatus.Text.IsEqual(TKWorkFlow.Status.Declined))
                {
                    lblStatus.ToolTip = DataBinder.Eval(e.Row.DataItem, "DeclinedReason").ToString();
                }
                

                //check if locked
                string datefrom = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "Date").ToString(), _KioskSession.DateFormat);
                string dateto = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "Date").ToString(), _KioskSession.DateFormat);
                if (DateLockedForApprover(Convert.ToDateTime(datefrom)) || DateLockedForApprover(Convert.ToDateTime(dateto)))
                {
                    ibLocked.Visible = true;
                    ibApproved.Visible = false;
                    ibRecall.Visible = false;
                    ibDeny.Visible = false;
                }

                ibApproved.Attributes.Add("onclick", string.Format("return window.confirm('Approve DTR of Emp.ID: {0}, Date: {1} ?' );", litEmployeeID.Text.TrimEnd(), datefrom));
                
            }
            
            //e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            //e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
     
    private void ApprovedDeny(object sender, string newStatus, int rowindex, int newStage)
    {
        HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
        Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;        
        DateTime date = gvEmp.Rows[rowindex].Cells[2].Text.ToDate();
        
        BNS.TK.Entities.Attendance att = new BNS.TK.Entities.Attendance();
        if (att.LoadByPrimaryKey(hfCompanyID.Value, litEmployeeID.Text, date))
        {
            att.Status = newStatus;
            att.Stage = newStage;
            if (newStatus.IsEqual(TKWorkFlow.Status.Declined))
                att.s_DeclinedReason = txtDecReason.Text;

            att.LastUpdBy = _KioskSession.UID;
            att.LastUpdDate = DateTime.Now;
            att.Save();
        }

        //send email
        //Helper.SendMail(Helper.GetEmailAddress(hfCompanyID.Value, litEmployeeID.Text), "Kronos DTR change of status", "Your DTR was acted upon by your supervisor.");

        gvEmp.EditIndex = -1;
        BindGrid();

        ReprocessAttendance(litEmployeeID.Text, date, date);
    }

    private void Recall(object sender, GridViewRow gvRow)
    {
        var row = gvEmp.Rows[gvRow.RowIndex];
        var companyId = ((HiddenField)row.FindControl("hfCompanyID")).Value;
        var employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
        var date = row.Cells[2].Text.ToDate();
        var att = new Attendance();
        try
        {
            if (!att.LoadByPrimaryKey(companyId, employeeId, date))
                throw new Exception("Time Transaction not found.");

            if (att.Posted)
                throw new Exception("Cannot recall posted Time Transaction.");

            att.Status = TKWorkFlow.Status.ForApproval;
            att.Stage = 0;
            att.DeclinedReason = string.Empty;
            att.LastUpdBy = _KioskSession.UID;
            att.LastUpdDate = DateTime.Now;
            att.Save();
        }
        catch (SqlException sqex)
        {
            lblPopErrorMsg.Text = sqex.Message;
            mpe.Show();
            return;
        }

        if (att.LoadByPrimaryKey(companyId, employeeId, date))
        {
            att.Status = TKWorkFlow.Status.ForApproval;
            att.Stage = 0;
            att.Posted = false;
            att.DeclinedReason = string.Empty;
            att.LastUpdBy = _KioskSession.UID;
            att.LastUpdDate = DateTime.Now;
            att.Save();
        }

        gvEmp.EditIndex = -1;
        BindGrid();
        ReprocessAttendance(employeeId, date, date);

        //HiddenField hfCompanyID = gvEmp.Rows[row.RowIndex].FindControl("hfCompanyID") as HiddenField;
        //Literal litEmployeeID = gvEmp.Rows[row.RowIndex].FindControl("litEmployeeID") as Literal;
        //DateTime date = gvEmp.Rows[row.RowIndex].Cells[2].Text.ToDate();

        //BNS.TK.Entities.Attendance att = new BNS.TK.Entities.Attendance();
        //if (att.LoadByPrimaryKey(hfCompanyID.Value, litEmployeeID.Text, date))
        //{
        //    att.Status = TKWorkFlow.Status.Declined;
        //    att.Stage = 0;
        //    att.s_DeclinedReason = "Recalled";
        //    att.LastUpdBy = _KioskSession.UID;
        //    att.LastUpdDate = DateTime.Now;
        //    att.Save();
        //}

        //gvEmp.EditIndex = -1;
        //BindGrid();

        //ReprocessAttendance(litEmployeeID.Text, date, date);
    }

    protected void btnForApproval_Click(object sender, EventArgs e)
    {
        var att = new Attendance();
        var userId = _KioskSession.UID;
        var date = DateTime.Now;
        var approverNo = _Url["approverno"].ToInt();
        var startDate = new DateTime();
        var endDate = new DateTime();

        string status, newStatus, companyId, employeeId;
        short newStage;
        DateTime tempDate;
        bool approvable;

        foreach (GridViewRow row in gvEmp.Rows)
        {
            status = ((Label)row.FindControl("lblStatus")).Text;
            approvable = ((ImageButton)row.FindControl("ibApproved")).Visible;

            if (status == TKWorkFlow.Status.ForApproval && approvable)
            {
                tempDate = gvEmp.Rows[row.RowIndex].Cells[2].Text.ToDate();

                startDate = startDate == DateTime.MinValue ? tempDate : startDate > tempDate ? tempDate : startDate;
                endDate = endDate == DateTime.MinValue ? tempDate : endDate < tempDate ? tempDate : endDate;

                newStatus = TKWorkFlow.Status.Approved;
                newStage = 2;
                employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;

                if (approverNo == 1 || approverNo == 2)
                {
                    newStage = 1;

                    if (!IsFinalApprover(employeeId))
                        newStatus = TKWorkFlow.Status.ForApproval;
                }

                companyId = ((HiddenField)row.FindControl("hfCompanyID")).Value;

                if (att.LoadByPrimaryKey(companyId, employeeId, tempDate))
                {
                    att.Status = newStatus;
                    att.Stage = newStage;
                    att.LastUpdBy = userId;
                    att.LastUpdDate = date;
                    att.Save();
                }
            }
        }

        if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
        {
            gvEmp.EditIndex = -1;
            BindGrid();

            ReprocessAttendance(string.Empty, startDate, endDate);
        }
    }

    private int CountStatus(string filterstatus)
    {
        int retval = 0;
        foreach (GridViewRow row in gvEmp.Rows)
        {
            Label litStatus = row.FindControl("lblStatus") as Label;
            if (litStatus.Text.ToLower() == filterstatus.ToLower())
                retval++;
        }
        return retval;
    }

    private void ReprocessAttendance(string empid, DateTime from, DateTime to)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(from, to, empid);
    }


    private bool IsFinalApprover(string empid)
    {
        EmployeeApprover appr = new EmployeeApprover();
        string sql = "SELECT Approver3, Approver4 FROM EmployeeApprover WHERE CompanyID = {0} AND EmployeeID = {1} ";

        object[] parameters = { _KioskSession.CompanyID, empid.Trim() };
        appr.DynamicQuery(sql, parameters);
        if (appr.RowCount > 0)
        {
            return (appr.s_Approver3.IsNullOrEmpty() && appr.s_Approver4.IsNullOrEmpty());
        }
        return false;
    }

    //called onclick of gridview approve or decline button
    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        Literal litEmployeeID = gvEmp.Rows[gvEmp.SelectedIndex].FindControl("litEmployeeID") as Literal;
        string empid = litEmployeeID.Text;
        
        string newStatus = TKWorkFlow.Status.Approved;
        int newStage = 2;
        if (ApproverNo() == "1" || ApproverNo() == "2")
        {
            newStage = 1;
            if (IsFinalApprover(empid))
                newStatus = TKWorkFlow.Status.Approved;
            else
                newStatus = TKWorkFlow.Status.ForApproval;
        }

        ApprovedDeny(sender, newStatus, gvEmp.SelectedIndex, newStage);
    }

    //called onclick of grid delete button
    //this will just show popup to get the reason of decline
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {        
        txtDecReason.Text = "";
        hfdecRowid.Value = e.RowIndex.ToString();
        mpe3.Show();
    }

    //called onclick fo refresh button
    protected void btnFU_Click(object sender, EventArgs e)
    {        
        BindGrid();
    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper().TrimEnd() == "RECALL")
        {
            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            int rowIndex = row.RowIndex;
            

            Recall(sender, row);
        }
    }

    private bool DateLockedForApprover(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Approver_lock_start != "" && tp.s_Approver_lock_end != "")
            {
                return (DateTime.Today >= tp.Approver_lock_start && DateTime.Today <= tp.Approver_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }
    protected void btnDecline_Click(object sender, EventArgs e)
    {        
        int rowindex;
        if (int.TryParse(hfdecRowid.Value, out rowindex))
            ApprovedDeny(sender, "Declined", rowindex, 0);
    }

    //temp code only
    protected void ddlDisplay_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {        
        ImageButton ibtn = (ImageButton)sender;
        DateTime date = (ibtn.Parent.Parent.FindControl("hfDate") as HiddenField).Value.ToDate();
        lblPopErrorMsg.Text = "";

        string empid = (ibtn.Parent.Parent.FindControl("litEmployeeID") as Literal).Text;

        Attendance att = new Attendance();
        if (att.LoadByPrimaryKey(_KioskSession.CompanyID, empid, date))
        {
            hdEmpID.Value = att.EmployeeID;
            hdDatePop.Value = date.ToShortDateString();
            txtOrig_In.Text = att.s_Orig_in1.IsNullOrEmpty() ? "" : att.Orig_in1.ToString("MM/dd/yyyy hh:mm tt");
            txtOrig_Out.Text = att.s_Orig_out1.IsNullOrEmpty() ? "" : att.Orig_out1.ToString("MM/dd/yyyy hh:mm tt");

            txtNewDateIn.Text = (att.s_In1.IsNullOrEmpty()) ? date.ToString("MM/dd/yyyy") : String.Format("{0:MM/dd/yyyy}", att.In1);
            txtNewDateOut.Text = (att.s_Out1.IsNullOrEmpty()) ? date.ToString("MM/dd/yyyy") : String.Format("{0:MM/dd/yyyy}", att.Out1);
            txtNewTimeIn.Text = (att.s_In1.IsNullOrEmpty()) ? "" : String.Format("{0:hh:mm tt}", att.In1);
            txtNewTimeOut.Text = (att.s_Out1.IsNullOrEmpty()) ? "" : String.Format("{0:hh:mm tt}", att.Out1);
            txtNewRemarks.Text = att.s_Reason;
            txtStatus.Text = att.s_Status;

            mpe.Show();
        }
    }

    private bool ValidateEditInputs()
    {
        DateRange dr = new DateRange();
        if (!dr.ValidatePeriod(txtNewDateIn.Text + " " + txtNewTimeIn.Text, txtNewDateOut.Text + " " + txtNewTimeOut.Text))
        {
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('" + dr.message + "');", true);
            lblPopErrorMsg.Text = dr.message;
            return false;
        }
        if (!(txtNewTimeIn.Text.IsNullOrEmpty() && txtNewTimeOut.Text.IsNullOrEmpty()))
        {
            if (txtNewTimeIn.Text.IsNullOrEmpty())
            {
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter new Time In');", true);
                lblPopErrorMsg.Text = "Please enter requested Time In.";
                return false;
            }
            if (txtNewTimeOut.Text.IsNullOrEmpty())
            {
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter new Time Out');", true);
                lblPopErrorMsg.Text = "Please enter requested Time Out";
                return false;
            }
        }


        if (txtNewRemarks.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter reason.');", true);
            return false;
        }

        DateTime origIn = (txtOrig_In.Text.TrimEnd() == "") ? DateTime.MinValue : txtOrig_In.Text.ToDate();
        DateTime origOut = (txtOrig_Out.Text.TrimEnd() == "") ? DateTime.MinValue : txtOrig_Out.Text.ToDate();
        DateTime newIn = (txtNewTimeIn.Text.TrimEnd() == "") ? DateTime.MinValue : (txtNewDateIn.Text + " " + txtNewTimeIn.Text).ToDate();
        DateTime newOut = (txtNewTimeOut.Text.TrimEnd() == "") ? DateTime.MinValue : (txtNewDateOut.Text + " " + txtNewTimeOut.Text).ToDate();
        if (origIn == newIn && origOut == newOut)
        {
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter requested In/Out.');", true);
            lblPopErrorMsg.Text = "Please enter requested In/Out.";
            return false;
        }

        return true;
    }

    private void ReprocessAttendance(DateTime date)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(date, date, _KioskSession.EmployeeID);

        BindGrid();
    }

    protected void btnEditSave_Click(object sender, EventArgs e)
    {
        if (!ValidateEditInputs())
        {
            mpe.Show();
            return;
        }

        mpe.Hide();
        try
        {
            string date = hdDatePop.Value;

            Attendance att = new Attendance();
            if (att.LoadByPrimaryKey(_KioskSession.CompanyID, hdEmpID.Value, DateTime.Parse(date)))
            {
                DateTime newIn = DateTime.Parse(txtNewDateIn.Text + " " + txtNewTimeIn.Text);
                DateTime newOut = DateTime.Parse(txtNewDateOut.Text + " " + txtNewTimeOut.Text);

                //change status only if in/out was changed                
                if (newIn != att.In1 || newOut != att.Out1)
                {
                    att.Status = BNS.TK.Business.TKWorkFlow.Status.Approved;  //auto approved for admin
                    att.Stage = 2;
                }

                if (txtNewTimeIn.Text.IsNullOrEmpty() && txtNewTimeOut.Text.IsNullOrEmpty())
                {
                    att.s_In1 = "";
                    att.s_Out1 = "";
                }
                else
                {
                    att.In1 = newIn;
                    att.Out1 = newOut;
                }
                att.Reason = txtNewRemarks.Text;
                att.EarlyWork = false;
                //save
                att.Save();

                ReprocessAttendance(date.ToDate());

                
                ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsmsg", "alert('Successfuly saved.');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('" + ex.Message + "');", true);
        }
    }
}
