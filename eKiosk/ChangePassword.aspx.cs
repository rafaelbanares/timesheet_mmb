using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.Framework.Encryption;

public partial class ChangePassword :  KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        lblError.Text = "";
        lblErrorSQ.Text = "";
        if (!IsPostBack)
        {
            if (_Url["mode"] == "nopassword")
                txtOldPwd.Enabled = false;
            else if (_Url["mode"] == "havepassword")
                txtOldPwd.Enabled = true;
            else
                Response.Redirect("LoginEmployee.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("eKioskHome.aspx");
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (txtNewPwd.Text != txtRetype.Text)
        {
            lblError.Text = "New password did not match your re-typed password";
            return;
        }

        string oldpassword = Crypto.ActionEncrypt(txtOldPwd.Text);
        string newpassword = Crypto.ActionEncrypt(txtNewPwd.Text);

        string password = (string)SqlHelper.ExecuteScalar(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangePassword",
                        new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@EmployeeID", _KioskSession.EmployeeID),
                            new SqlParameter("@OldPassword", oldpassword),
                            new SqlParameter("@NewPassword", newpassword )
                        });

        if (password == "" && txtOldPwd.Enabled)
        {
            lblError.Text = "Old password is invalid.";
            return;
        }
        else if (oldpassword == newpassword)
        {
            lblError.Text = "Please enter a new different password";
            return;
        }

        //<span style='color:Red'>Password was successfully changed</span>
        SecureUrl su = new SecureUrl(@"BlankPage.aspx?Message=Password was successfully changed");
        Response.Redirect(su.ToString());
    }

    protected void btnSQ_Click(object sender, EventArgs e)
    {
        if (txtAnswer.Text != txtAnswer2.Text)
        {
            lblErrorSQ.Text = "New secret answer did not match your re-typed secret answer";
            return;
        }

        string oldpassword = Crypto.ActionEncrypt(txtPassword.Text);

        string password = (string)SqlHelper.ExecuteScalar(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_ChangeSecretQuestion",
                        new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@EmployeeID", _KioskSession.EmployeeID),
                            new SqlParameter("@OldPassword", oldpassword ),
                            new SqlParameter("@NewSecretQuestion", txtNewSQ.Text ),
                            new SqlParameter("@NewAnswer", txtAnswer.Text)
                        });

        if (password == "")
        {
            lblErrorSQ.Text = "Current password is invalid";
            return;
        }

        SecureUrl su = new SecureUrl(@"BlankPage.aspx?Message=Secret question/answer was successfully changed");
        Response.Redirect(su.ToString());
    }
}
