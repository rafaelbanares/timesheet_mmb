using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class LeaveCode : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCodeLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCodeLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@IsBlankData", true), new SqlParameter("@DBname", _KioskSession.DB) });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvMain.DataSource = ds.Tables[0];
        gvMain.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvMain.DataBind();
        hfIsAdd.Value = "1";
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //... databinder will return empty string for new row
            bool checkbalance = bool.Parse(DataBinder.Eval(e.Row.DataItem, "CheckBalance").ToString());
            bool withpay = bool.Parse(DataBinder.Eval(e.Row.DataItem, "WithPay").ToString());

            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
            if (ibDelete != null)
            {
                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");

                Image imgCheckBalance = e.Row.FindControl("imgCheckBalance") as Image;
                Image imgWithPay = e.Row.FindControl("imgWithPay") as Image;

                imgCheckBalance.Visible = checkbalance;
                imgWithPay.Visible = withpay;
            }
            else
            {
                CheckBox chkCheckBalance = e.Row.FindControl("chkCheckBalance") as CheckBox;
                CheckBox chkWithPay = e.Row.FindControl("chkWithPay") as CheckBox;

                chkCheckBalance.Checked = checkbalance;
                chkWithPay.Checked = withpay;
            }
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete

        Literal litCode = gvMain.Rows[e.RowIndex].FindControl("litCode") as Literal;

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCodeDelete",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@Code", litCode.Text), new SqlParameter("@DBname", _KioskSession.DB) });

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox txtCode = gvMain.Rows[e.RowIndex].FindControl("txtCode") as TextBox;
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;

        TextBox txtEarning = gvMain.Rows[e.RowIndex].FindControl("txtEarning") as TextBox;
        TextBox txtYearTotal = gvMain.Rows[e.RowIndex].FindControl("txtYearTotal") as TextBox;
        TextBox txtFilingDays = gvMain.Rows[e.RowIndex].FindControl("txtFilingDays") as TextBox;
        CheckBox chkCheckBalance = gvMain.Rows[e.RowIndex].FindControl("chkCheckBalance") as CheckBox;
        CheckBox chkWithPay = gvMain.Rows[e.RowIndex].FindControl("chkWithPay") as CheckBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@Code", txtCode.Text ));
        sqlparam.Add(new SqlParameter("@Description", txtDescription.Text));

        sqlparam.Add(new SqlParameter("@Earning", txtEarning.Text));
        sqlparam.Add(new SqlParameter("@YearTotal", txtYearTotal.Text));
        sqlparam.Add(new SqlParameter("@FilingDays", txtFilingDays.Text));

        sqlparam.Add(new SqlParameter("@CheckBalance", chkCheckBalance.Checked));
        sqlparam.Add(new SqlParameter("@Withpay", chkWithPay.Checked));

        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        if (hfIsAdd.Value == "0")
        {
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCodeUpdate", sqlparam.ToArray());
        }
        else
        {
            sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveCodeInsert", sqlparam.ToArray());
        }

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }

}
