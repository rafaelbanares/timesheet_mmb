﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;
using BNS.TK.Business;

public partial class OBApplications : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (!IsPostBack)
        {
            
            PopulateYear();
            BindGrid();
        }
    }

    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }
    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    private void BindGrid()
    {
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.DataBind();
    }

    private DataSet GetData()
    {
        string sdate = "01/01/" + ddlYear.SelectedValue;
        string edate = "12/31/" + ddlYear.SelectedValue;

        OBTrans ob = new OBTrans();
        string sql = "SELECT * FROM OBTrans WHERE CompanyID = {0} and EmployeeID = {1} and OBdate between {2} and {3} ORDER BY OBDate";

        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID, sdate.ToDate(), edate.ToDate() };
        ob.DynamicQuery(sql, parameters);

        return ob.ToDataSet(); ;
    }


    protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmployees.EditIndex)
            {
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                ibEdit.Visible = status.IsNullOrEmpty() || status.IsEqual(BNS.TK.Business.TKWorkFlow.Status.ForApproval);
            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }

    }
    protected void gvEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.PageIndex = e.NewPageIndex;
        gvEmployees.DataBind();
    }

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        hfIsAdd.Value = "";
        ImageButton ibtn = (ImageButton)sender;
        DateTime date = (ibtn.Parent.Parent.FindControl("hfDate") as HiddenField).Value.ToDate();

        OBTrans ob = new OBTrans();
        if (ob.LoadByPrimaryKey(_KioskSession.CompanyID, _KioskSession.EmployeeID, date))
        {
            //txtStartDate.Enabled = false;
            //txtEndDate.Enabled = false;
            txtOBdate.Enabled = false;
            txtOBdate.Text = ob.s_OBDate;
            txtAppDate.Text = ob.s_CreatedDate;
            hdAppDate.Value = ob.s_OBDate;
            //txtStartDate.Text = ob.StartDate.ToString("MM/dd/yyyy");
            txtStartTime.Text = ob.StartDate.ToString("hh:mm tt");
            //txtEndDate.Text = ob.EndDate.ToString("MM/dd/yyyy");
            txtEndTime.Text = ob.EndDate.ToString("hh:mm tt");
            txtRemarks.Text = ob.Remarks;

            mpe.Show();
        }
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    private bool ValidateEditInputs()
    {
        DateRange dr = new DateRange();
        //if (!dr.ValidatePeriod(txtStartDate.Text + " " + txtStartTime.Text, txtEndDate.Text + " " + txtEndTime.Text))
        if (!dr.ValidatePeriod(txtOBdate.Text + " " + txtStartTime.Text, txtOBdate.Text + " " + txtEndTime.Text))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('" + dr.message + "');", true);
            return false;
        }
        if (txtOBdate.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter an OB date');", true);
            return false;
        }
        //if (txtStartDate.Text.IsNullOrEmpty())
        //{
        //    ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter start date');", true);
        //    return false;
        //}
        if (txtStartTime.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter start time');", true);
            return false;
        }

        //if (txtEndDate.Text.IsNullOrEmpty())
        //{
        //    ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter end date');", true);
        //    return false;
        //}
        if (txtEndTime.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter end time');", true);
            return false;
        }

        if (txtRemarks.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter remarks');", true);
            return false;
        }

        return true;
    }

    protected void btnEditSave_Click(object sender, EventArgs e)
    {
        if (!ValidateEditInputs())
        {
            mpe.Show();
            return;
        }

        mpe.Hide();

        try
        {
            string empid = _KioskSession.EmployeeID;
           
            OBTrans ob = new OBTrans();

            if (hfIsAdd.Value == "1")
            {
                ob.AddNew();
                ob.CompanyID = _KioskSession.CompanyID;
                ob.EmployeeID = empid;
                ob.s_OBDate = txtOBdate.Text;
                ob.s_StartDate = txtOBdate.Text + " " + txtStartTime.Text;
                ob.s_EndDate = txtOBdate.Text + " " + txtEndTime.Text;
                ob.Status = TKWorkFlow.Status.ForApproval;
                ob.Stage = 0;
                ob.CreatedBy = _KioskSession.EmployeeID;
                ob.CreatedDate = DateTime.Now;                
            }
            else
            {

                if (ob.LoadByPrimaryKey(_KioskSession.CompanyID, empid, DateTime.Parse(hdAppDate.Value)))
                {
                    ob.s_StartDate = txtOBdate.Text + " " + txtStartTime.Text;
                    ob.s_EndDate = txtOBdate.Text + " " + txtEndTime.Text;
                    //ob.s_StartDate = ob.StartDate.Date.ToString("MM/dd/yyyy") + " " + txtStartTime.Text;
                    //ob.s_EndDate = ob.EndDate.Date.ToString("MM/dd/yyyy") + " " + txtEndTime.Text;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsmsg", "alert('Data not found.');", true);
                    return;
                }
            }
            
            ob.Remarks = txtRemarks.Text;
            ob.LastUpdBy = _KioskSession.EmployeeID;
            ob.LastUpdDate = DateTime.Now;
            //save
            ob.Save();
            
            if (hfIsAdd.Value == "1")
            {
                //Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos for approval", "You have a pending excused tardy for approval.");
            }

            BindGrid();
            
        }
        catch (SqlException sqex)
        {
            if (sqex.Number == 2627)  //'PK violation
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('You already have application in this date. Please check.');", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('" + sqex.Message.Replace("'", "") + "');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "Kronos", "alert('" + ex.Message.Replace("'"," ") + "');", true);
        }
    }
    protected void lnkApplyOB_Click(object sender, EventArgs e)
    {
        hfIsAdd.Value = "1";
        txtOBdate.Text = "";
        txtAppDate.Text = DateTime.Now.ToShortDateString();
        //txtStartDate.Text = "";
        txtStartTime.Text = "";
        //txtEndDate.Text = "";
        txtEndTime.Text = "";
        txtRemarks.Text = "";
        mpe.Show();
    }

    protected void txtOBdate_TextChanged(object sender, EventArgs e)
    {
        mpe.Show();
        //if (txtOBdate.Text.IsFilled())
        //{
        //    txtStartDate.Text = txtOBdate.Text;
        //    txtEndDate.Text = txtOBdate.Text;
        //    txtStartTime.Focus();
        //}
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}