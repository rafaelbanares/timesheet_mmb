<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="eTimeHome.aspx.cs" Inherits="eTimeHome" Title="MMB Kronos - Home page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<br />
<br />
<br />
<table width="800" border="1">
    <tr>
        <td colspan="2" class="DataGridHeaderStyle">eTime Home - Summary and Default 
            Settings</td>
    </tr>
    <tr>
        <td valign="top">
            <br />
            <asp:Label ID="lbSumm" runat="server" Font-Names="Arial" Font-Size="Large" Text="Summary"></asp:Label><br />
            <br />
            <asp:HyperLink ID="hlDTR" runat="server">{0} DTR waiting for approval</asp:HyperLink><br />
            <asp:HyperLink ID="hlUnapproveLeave" runat="server">{0} Leaves waiting for approval</asp:HyperLink><br />
            <asp:HyperLink ID="hlUnapproveOT" runat="server">{0} Overtime waiting for approval</asp:HyperLink><br />
            <%--<asp:HyperLink ID="hlCShift" runat="server">{0} Shift waiting for approval</asp:HyperLink><br />
            <asp:HyperLink ID="hlCRestday" runat="server">{0} Restday waiting for approval</asp:HyperLink><br />--%>
            <asp:HyperLink ID="hlOB" runat="server">{0} OB waiting for approval</asp:HyperLink><br />
            <br />
        </td>
        <td valign="top">
            
            <br />
            <asp:HiddenField ID="hdCompanyID" runat="server" />
            <asp:HiddenField ID="hdCompanyName" runat="server" />
            <br />
            <asp:Label ID="lbDefDate" runat="server" Font-Names="Arial" Font-Size="Large" 
                Text="Current Cut-off Period"></asp:Label><br />
            <br />
            <table>
                <tr>
                    <td>
                        Attendance From:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" Visible="False" />

                        <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Cannot accept invalid value" ValidationGroup="FiltGrp" ControlToValidate="txtDateStart" 
                            ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        To:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" Visible="False" />

                        <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Cannot accept invalid value" ValidationGroup="FiltGrp" ControlToValidate="txtDateEnd" 
                            ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date"  >*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        &nbsp;
                        <asp:Button ID="btnUpdate" runat="server" CssClass="ControlDefaults" 
                            Text="Update Default Dates" onclick="btnUpdate_Click" Width="150px" />
                    </td>
                </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td>Kronos is currently</td>
                        <td><asp:Label ID="lblLockStat" runat="server" Text=""/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><asp:Button ID="btnLockUnlock" runat="server" onclick="btnLockUnlock_Click" 
                                Text="Lock Kronos" Width="105px" /></td>
                    </tr>
                </table>
                <br />
                
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                
        </td>
    </tr>
</table>

    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

</asp:Content>
