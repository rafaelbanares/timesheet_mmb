﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;


public partial class TimeCredits : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = GetData();
        
        gvEmployees.DataSource = SortDataTable(ds.Tables[0], true);
        gvEmployees.DataBind();

        updateGrid(ds.Tables[1]);
    }

    private void updateGrid(DataTable dt)
    {
        for (int i=0; i < gvEmployees.Rows.Count; i++)
        {
            Label lblTardy = gvEmployees.Rows[i].FindControl("lblTardy") as Label;
            Label lblUndertime = gvEmployees.Rows[i].FindControl("lblUndertime") as Label;

            string tcId = gvEmployees.DataKeys[i].Values["tcID"].ToString();

            DataRow[] drTardy = dt.Select("tcID = " + tcId);

            StringBuilder sbTardy = new StringBuilder();
            StringBuilder sbUT = new StringBuilder();

            foreach (DataRow dr in drTardy)
            {
                string s = Convert.ToDateTime(dr["Date"]).ToString(_KioskSession.DateFormat) + " - " + dr["Used"].ToString();

                if (dr["TardyUT"].ToString() == "U")
                    sbUT.Append(sbUT.Length != 0 ? "<br>" + s : s);
                else
                    sbTardy.Append(sbTardy.Length != 0 ? "<br>" + s : s);
            }
            lblTardy.Text = sbTardy.ToString();
            lblUndertime.Text = sbUT.ToString();
        }
    }

    private DataSet GetData()
    {
        SqlParameter[] spParams;

        if (((HiddenField)EmpHeader1.FindControl("hdDateSelection")).Value == "")
            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)           
            };
        else
        {
            string sdate = ((HiddenField)EmpHeader1.FindControl("hdDateStart")).Value;
            string edate = ((HiddenField)EmpHeader1.FindControl("hdDateEnd")).Value;

            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@StartDate", sdate)
                ,new SqlParameter("@EndDate",edate )
            };
        }

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeCreditsLoadByEmployeeID", spParams);
        return ds;
    }


    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }

    protected void gvEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.PageIndex = e.NewPageIndex;
        gvEmployees.DataBind();
    }

    protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        EmpHeader1.UpdateURL();
        BindGrid();
    }


    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {

    }
}
