﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestNumericInput.aspx.cs" Inherits="TestNumericInput" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<script type="text/javascript">
    function validatenumber(event, obj) {
        var code = (event.which) ? event.which : event.keyCode;
        var character = String.fromCharCode(code);

        if ((code >= 48 && code <= 57)) {
            if (obj.value == "0")
                return false;
            return true;
        }
        else if (code == 46) { // Check dot
            if (obj.value.indexOf(".") < 0) {
                if (obj.value.length == 0)
                    obj.value = "0";
                return true;
            }
        }
        else if (code == 8 || code == 116) { // Allow backspace, F5
            return true;
        }
        else if (code >= 37 && code <= 40) { // Allow directional arrows
            return true;
        }

        return false;
    }

    function validatefield(obj) { // Remove dot if last character
        if (obj.value.indexOf(".") == obj.value.length - 1) {
            obj.value = obj.value.substring(0, obj.value.length - 1)
        } // Clear text box if not a number, incase user drags/drop letter into box
        else if (isNaN(obj.value)) {
            obj.value = "0.0";
        }
    }
</script> 

    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" onkeypress="return validatenumber(event, this);" onblur="validatefield(this);" onpaste="return false;" runat="server"></asp:TextBox>
    </div>
    </form>
</body>
</html>
