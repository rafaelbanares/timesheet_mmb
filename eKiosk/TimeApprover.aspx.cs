﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;


public partial class TimeApprover : KioskPageUI
{
    protected readonly string sBackColor_Restday = "#CC99FF";
    protected readonly string sBackColor_Holiday = "#ffd700";
    protected readonly string sBackColor_RstHoliday = "#f4a460";
    protected readonly string sBackColor_LeaveWhole = "#90ee90";
    protected readonly string sBackColor_LeaveHalf = "#CCFF66";
    protected readonly string sBackColor_Absent = "#c0c0c0";

    string approverno;
    string stage;

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        //... must check if right query string is passed
        if (_Url["approverno"] == "")
            Response.Redirect("LoginEmployee.aspx");
        else
        {
            approverno = _Url["approverno"];
            stage = (approverno == "1" ? "1" : "2");
        }

        if (!IsPostBack)
        {
            //btnForFinalApproval.Visible = (approverno != "1");

            BindGrid();
        }
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        //btnForApproval.Enabled = (gvEmp.EditIndex == -1 && CountStatus("For Approval") > 0);
        //btnForFinalApproval.Enabled = (gvEmp.EditIndex == -1 && CountStatus("For Final Approval") > 0);

    }
    private DataSet GetData()
    {
        SqlParameter[] spParams;

    if (((HiddenField)EmpHeader1.FindControl("hdDateSelection")).Value == "")
            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverNo", approverno)
            };
        else
        {
            string sdate = ((HiddenField)EmpHeader1.FindControl("hdDateStart")).Value;
            string edate = ((HiddenField)EmpHeader1.FindControl("hdDateEnd")).Value;

            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverNo", approverno)
                ,new SqlParameter("@StartDate", sdate )
                ,new SqlParameter("@EndDate", edate )
            };
        }

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_TimeApprover", spParams);
        return ds;
    }

    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                Label lblreason = e.Row.FindControl("lblreason") as Label;
                string reason = lblreason.Text;
                if (reason.Length > 25)
                {
                    lblreason.ToolTip = reason;
                    lblreason.Text = reason.Substring(0, 15) + "...";
                }

                string origin1 = gvEmp.DataKeys[e.Row.RowIndex].Values["orig_In1"].ToString();
                string origout1 = gvEmp.DataKeys[e.Row.RowIndex].Values["orig_Out1"].ToString();

                string newin1 = DataBinder.Eval(e.Row.DataItem, "new_in1").ToString();
                string newout1 = DataBinder.Eval(e.Row.DataItem, "new_out1").ToString();

                
                Label lblNewIn1 = e.Row.FindControl("lblNewIn1") as Label;
                Label lblNewOut1 = e.Row.FindControl("lblNewOut1") as Label;

                lblNewIn1.ForeColor = (origin1 != newin1) ? System.Drawing.Color.Red : System.Drawing.Color.Gray;
                lblNewOut1.ForeColor = (origout1 != newout1) ? System.Drawing.Color.Red : System.Drawing.Color.Gray;

                #region assign background color
                string otcode = DataBinder.Eval(e.Row.DataItem, "OTCode").ToString().Trim();
                string leavecode = DataBinder.Eval(e.Row.DataItem, "LeaveCode").ToString().Trim();
                decimal leavedays = (decimal)DataBinder.Eval(e.Row.DataItem, "Leave");
                decimal absentdays = (decimal)DataBinder.Eval(e.Row.DataItem, "Absent");

                if (otcode == "RST")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Restday);
                else if (otcode == "SPL" || otcode == "LGL")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Holiday);
                else if (otcode == "SPLRST" || otcode == "LGLRST")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_RstHoliday);
                else if (leavecode != "" && leavedays >= 1)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_LeaveWhole);
                else if (leavecode != "" && leavedays < 1)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_LeaveHalf);
                else if (absentdays > 0)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Absent);
                #endregion

            }
            Literal litStage = e.Row.FindControl("litStage") as Literal;
            litStage.Text = (litStage.Text == "1" ? "For Final Approval" : "For Approval");

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hfCompanyID = gvEmp.Rows[e.RowIndex].FindControl("hfCompanyID") as HiddenField;
        Literal litEmployeeID = gvEmp.Rows[e.RowIndex].FindControl("litEmployeeID") as Literal;
        Literal litDate = gvEmp.Rows[e.RowIndex].FindControl("litDate") as Literal;
        //Label lbloshift = gvEmp.Rows[e.RowIndex].FindControl("lbloshift") as Label;
        //Label lblorestday = gvEmp.Rows[e.RowIndex].FindControl("lblorestday") as Label;

        string origin1 = gvEmp.DataKeys[e.RowIndex].Values["orig_In1"].ToString();
        string origout1 = gvEmp.DataKeys[e.RowIndex].Values["orig_Out1"].ToString();

        TextBox tbin1 = gvEmp.Rows[e.RowIndex].FindControl("tbin1") as TextBox;
        TextBox tbout1 = gvEmp.Rows[e.RowIndex].FindControl("tbout1") as TextBox;
        TextBox tbreason = gvEmp.Rows[e.RowIndex].FindControl("tbreason") as TextBox;

        string shiftcode = gvEmp.DataKeys[e.RowIndex].Values["orig_shiftcode"].ToString();
        string restcode = gvEmp.DataKeys[e.RowIndex].Values["orig_restcode"].ToString();

        //string in1 = Utils.TimeToDateTimeString(litDate.Text, origin1, "");
        //string out1 = Utils.TimeToDateTimeString(litDate.Text, origout1, in1);
        string in1 = origin1;
        string out1 = origout1;

        string newin1 = Utils.TimeToDateTimeString(litDate.Text, tbin1.Text, "");
        string newout1 = Utils.TimeToDateTimeString(litDate.Text, tbout1.Text, newin1);
        
        if (in1 != newin1 || out1 != newout1)
        {
            List<SqlParameter> sqlparam = new List<SqlParameter>();

            sqlparam.Add(new SqlParameter("@CompanyID", hfCompanyID.Value));
            sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
            sqlparam.Add(new SqlParameter("@Date", litDate.Text));
            sqlparam.Add(new SqlParameter("@orig_shiftcode", shiftcode));
            sqlparam.Add(new SqlParameter("@orig_restcode", restcode));
            sqlparam.Add(new SqlParameter("@orig_in1", Tools.DefaultNull(in1)));
            sqlparam.Add(new SqlParameter("@orig_out1", Tools.DefaultNull(out1)));
            sqlparam.Add(new SqlParameter("@orig_in2", null));     //...temporarily not used
            sqlparam.Add(new SqlParameter("@orig_out2", null));     //...temporarily not used

            //...these 2 parameters are used by lowe, it is ignored in standard processing
            sqlparam.Add(new SqlParameter("@processSpecific", ConfigurationManager.AppSettings.Get("AttendanceProcess")));
            sqlparam.Add(new SqlParameter("@loweTimeCreditDays", ConfigurationManager.AppSettings.Get("Lowe-TimeCreditDays")));

            sqlparam.Add(new SqlParameter("@new_in1", Tools.DefaultNull(newin1)));
            sqlparam.Add(new SqlParameter("@new_out1", Tools.DefaultNull(newout1)));
            sqlparam.Add(new SqlParameter("@new_in2", null));       //...temporarily not used
            sqlparam.Add(new SqlParameter("@new_out2", null));      //...temporarily not used
            sqlparam.Add(new SqlParameter("@reason", tbreason.Text));
            sqlparam.Add(new SqlParameter("@Status", "For Approval"));
            sqlparam.Add(new SqlParameter("@Stage", stage == "2" ? "1" : "0"));
            sqlparam.Add(new SqlParameter("@approvedby", ""));
            sqlparam.Add(new SqlParameter("@lastupdby", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@lastupddate", DateTime.Now));
            sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceApprovalUpdateInsert", sqlparam.ToArray());
        }

        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void ibApproved_Click(object sender, ImageClickEventArgs e)
    {
        //... if approved by approver 1, approver 2 or approver 3 is still needed.
        string status = (approverno == "1" ? "For Approval" : "Approved");
        
        ApprovedDeny(sender, status);
    }

    protected void ibDeny_Click(object sender, ImageClickEventArgs e)
    {
        ApprovedDeny(sender, "Declined");
    }

    private void ApprovedDeny(object sender, string status)
    {
        ImageButton ibApprove = (ImageButton)sender;
        Literal litEmployeeID = (Literal)ibApprove.Parent.FindControl("litEmployeeID");
        Literal litDate = (Literal)ibApprove.Parent.FindControl("litDate");
        HiddenField hfCompanyID = (HiddenField)ibApprove.Parent.FindControl("hfCompanyID") as HiddenField;

        if (status.ToLower() != "declined" && _KioskSession.ApproverRuleOR_OR)
        {
            status = "Approved";
            stage = "2";
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();

        sqlparam.Add(new SqlParameter("@CompanyID", hfCompanyID.Value));
        sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
        sqlparam.Add(new SqlParameter("@Date", litDate.Text));

        //...these 2 parameters are used by lowe, it is ignored in standard processing
        sqlparam.Add(new SqlParameter("@processSpecific", ConfigurationManager.AppSettings.Get("AttendanceProcess")));
        sqlparam.Add(new SqlParameter("@loweTimeCreditDays", ConfigurationManager.AppSettings.Get("Lowe-TimeCreditDays")));
        
        sqlparam.Add(new SqlParameter("@status", status));
        sqlparam.Add(new SqlParameter("@stage", stage));
        sqlparam.Add(new SqlParameter("@approvedby", _KioskSession.EmployeeID));
        sqlparam.Add(new SqlParameter("@lastupdby", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@lastupddate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_AttendanceApprovalUpdate", sqlparam.ToArray());
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    private void UpdateBulk(string filterstatus)
    {
        //... count first if there is records to be udpated
        if (CountStatus(filterstatus) == 0)
            return;

        //... if approved by approver 1, approver 2 or approver 3 is still needed.
        string updatestatus = (approverno == "1" ? "For Approval" : "Approved");

        if (updatestatus.ToLower() != "declined" && _KioskSession.ApproverRuleOR_OR)
        {
            updatestatus = "Approved";
            stage = "2";
        }

        //... needed for batch update
        SqlConnection connection = new SqlConnection(_ConnectionString);
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM " + _KioskSession.DB + ".dbo.AttendanceApproval WHERE 1=0", connection);
            DataSet dsAttendanceApproval = new DataSet();
            adapter.Fill(dsAttendanceApproval);

            foreach (GridViewRow row in gvEmp.Rows)
            {
                Literal litEmployeeID = (Literal)row.FindControl("litEmployeeID");
                Literal litDate = (Literal)row.FindControl("litDate");
                Literal litStage = row.FindControl("litStage") as Literal;
                HiddenField hfCompanyID = row.FindControl("hfCompanyID") as HiddenField;

                if (litStage.Text.ToLower() == filterstatus.ToLower())
                {
                    DataRow drAttAppr = dsAttendanceApproval.Tables[0].NewRow();

                    drAttAppr["CompanyID"] = hfCompanyID.Value;
                    drAttAppr["EmployeeID"] = litEmployeeID.Text;
                    drAttAppr["Date"] = litDate.Text;
                    drAttAppr["Status"] = updatestatus;
                    drAttAppr["stage"] = stage;

                    drAttAppr["approvedby"] = _KioskSession.EmployeeID;
                    drAttAppr["lastupdby"] = _KioskSession.UID;
                    drAttAppr["lastupddate"] = DateTime.Now;

                    dsAttendanceApproval.Tables[0].Rows.Add(drAttAppr);
                }
            }

            #region batch update - "no insert to attendance being done here", just pure update
            SqlCommand command = new SqlCommand(MainDB + ".dbo.usa_AttendanceApprovalUpdate", connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@CompanyID", SqlDbType.Char, 10)).SourceColumn = "CompanyID";
            command.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Char, 15)).SourceColumn = "EmployeeID";
            command.Parameters.Add(new SqlParameter("@Date", SqlDbType.SmallDateTime)).SourceColumn = "Date";

            //...these 2 parameters are used by lowe, it is ignored in standard processing
            command.Parameters.Add(new SqlParameter("@processSpecific", SqlDbType.VarChar, 50)).Value = ConfigurationManager.AppSettings.Get("AttendanceProcess");
            command.Parameters.Add(new SqlParameter("@loweTimeCreditDays", SqlDbType.Int)).Value = ConfigurationManager.AppSettings.Get("Lowe-TimeCreditDays");

            command.Parameters.Add(new SqlParameter("@status", SqlDbType.Char, 15)).SourceColumn = "Status";
            command.Parameters.Add(new SqlParameter("@stage", SqlDbType.Char, 15)).SourceColumn = "Stage";
            command.Parameters.Add(new SqlParameter("@approvedby", SqlDbType.Char, 15)).SourceColumn = "ApprovedBy";
            command.Parameters.Add(new SqlParameter("@LastUpdBy", SqlDbType.Char, 15)).SourceColumn = "LastUpdBy";
            command.Parameters.Add(new SqlParameter("@LastUpdDate", SqlDbType.SmallDateTime)).SourceColumn = "LastUpdDate";
            command.Parameters.Add(new SqlParameter("@DBname", _KioskSession.DB));

            adapter.InsertCommand = command;

            //... if single employee disable batch transaction.
            //adapter.UpdateBatchSize = singleemp ? 1 : 0;
            adapter.UpdateBatchSize = dsAttendanceApproval.Tables[0].Rows.Count == 1 ? 1 : 0;
            //adapter.UpdateBatchSize = 0;
            command.UpdatedRowSource = UpdateRowSource.None;
            command.CommandTimeout = 0;

            adapter.Update(dsAttendanceApproval);
            #endregion
        }
        catch (Exception ex)
        {
            throw (new Exception(ex.ToString()));
        }
        finally
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
            }
        }
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    private int CountStatus(string filterstatus)
    {
        int retval = 0;
        foreach (GridViewRow row in gvEmp.Rows)
        {
            Literal litStage = row.FindControl("litStage") as Literal;
            if (litStage.Text.ToLower() == filterstatus.ToLower())
                retval++;
        }
        return retval;
    }

    protected void btnForApproval_Click(object sender, EventArgs e)
    {
        //UpdateBulk("For Approval");
    }
    protected void btnForFinalApproval_Click(object sender, EventArgs e)
    {
        //UpdateBulk("For Final Approval");
    }
    protected void btnFU_Click(object sender, EventArgs e)
    {
        EmpHeader1.UpdateURL();
        BindGrid();
    }


}
