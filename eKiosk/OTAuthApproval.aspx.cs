using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class Attendance_OTAuthApproval : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            populateScreenDefaults();
        }
    }

    private void populateScreenDefaults()
    {
        string id = _Url["OTAuthID"];

        DataRow drOTAuth = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthByID",
                        new SqlParameter[] { 
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@ID", id )                        
                        }).Tables[0].Rows[0];

        hfOTAuthID.Value = id;

        lblFullname.Text = drOTAuth["FullName"].ToString();
        txtDateFiled.Text = Tools.ShortDate(drOTAuth["DateFiled"].ToString(), _KioskSession.DateFormat);
        txtOTDate.Text = drOTAuth["OTDate"].ToString();

        txtin1.Text = Tools.GetTime(drOTAuth["OTstart"].ToString());
        txtout1.Text = Tools.GetTime(drOTAuth["OTend"].ToString());

        txtOTHours.Text = drOTAuth["ApprovedOT"].ToString();
        txtReason.Text = drOTAuth["Reason"].ToString();
    }

    private void OTAuthUpdate(string status)
    {
        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", hfEmployeeID.Value));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@ID", hfOTAuthID.Value));

        sqlparam.Add(new SqlParameter("@OTDate", txtOTDate.Text ));
        sqlparam.Add(new SqlParameter("@OTstart", txtin1.Text ));
        sqlparam.Add(new SqlParameter("@OTend ", txtout1.Text ));
        sqlparam.Add(new SqlParameter("@ApprovedOT", txtOTHours.Text));
        sqlparam.Add(new SqlParameter("@Reason", txtReason.Text ));
        sqlparam.Add(new SqlParameter("@Status", status));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdate", sqlparam.ToArray());
    }

    private void GoBack()
    {
        Response.Redirect("OTAuthListApprover.aspx");
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        OTAuthUpdate("Approved");
        GoBack();
    }
    protected void btnDeclined_Click(object sender, EventArgs e)
    {
        OTAuthUpdate("Declined");
        GoBack();
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        GoBack();
    }


}
