//.... check work distribution must not allow idle time 

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using Microsoft.ApplicationBlocks.Data;
using BNS.Attendance.DataObjects;

public partial class PayslipView : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            LoadReports();
            updateurl();
        }
    }

    private void LoadReports()
    {                
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString2, CommandType.Text,
                        "Select ReportID, ReportName from dbo.Reports Where ReportType = @reportType and Active=1 ORDER BY ReportName", 
                        new SqlParameter[] { new SqlParameter("@reportType", "P") });

        grvQuarterly.DataSource = ds.Tables[0];
        grvQuarterly.DataBind();

        var dsPayPeriod = SqlHelper.ExecuteDataset(_ConnectionString3, CommandType.Text,
            "SELECT CAST(CONVERT(DATE, PayDate) AS VARCHAR(MAX)) AS PayDate, PayrollNo FROM PayPeriod " +
            "WHERE CompanyID = @CompanyID AND PayrollStatus = 'POSTED' " +
            "ORDER BY PayDate DESC",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID) });

        ddlDate.DataSource = dsPayPeriod.Tables[0];
        ddlDate.DataBind();
    }
    
    protected void grvQuarterly_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    #region SaveReportParametersToSession
    private void SaveReportParametersToSession(string reportID)
    {
        var ds = SqlHelper.ExecuteDataset(_ConnectionString2, CommandType.Text,
                                       "Select CRName, CRTitle From Reports Where ReportID = @reportID",
                                       new SqlParameter[] { new SqlParameter("@reportID", reportID) });

        if (ds.Tables[0].Rows.Count == 0) throw new Exception("Invalid Report");

        var oPayParam = new PayslipParameters()
        {
            ReportName = ds.Tables[0].Rows[0]["CRName"].ToString(),
            ReportTitle = ds.Tables[0].Rows[0]["CRTitle"].ToString(),
            CompanyName = _KioskSession.CompanyName,
            CompanyID = _KioskSession.CompanyID,
            EmployeeID = _KioskSession.EmployeeID,
            ApplYear = ddlDate.SelectedItem.Text.ToDate().Year,
            TranDate = ddlDate.SelectedItem.Text.ToDate()
        };
     
        Session["PayslipParameters"] = oPayParam;
    }
    #endregion

    protected void lnkReport_Click(object sender, EventArgs e)
    {
        Control pressed = (Control)sender;
        HtmlInputHidden txtRptID = (HtmlInputHidden)pressed.Parent.Parent.FindControl("hdnID");
        if (txtRptID != null)
        {
            SaveReportParametersToSession(txtRptID.Value);
            SecureUrl secureUrl = new SecureUrl(string.Format("{0}?rpt={1}", "./Viewer/Viewer.aspx", txtRptID.Value));

            ClientScript.RegisterStartupScript(typeof(Page), "open", Bns.AttUtils.Tools.WindowPopup(secureUrl.ToString(), ""), false);
        }
    }

    private void updateurl()
    {
        SecureUrl secureUrl = new SecureUrl(string.Format("popupLevel.aspx?Levels={0}", hfLevels.Value));
        hfURL.Value = secureUrl.ToString();
    }
    
}
