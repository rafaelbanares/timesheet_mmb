using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;

public partial class Attendance_LeaveApprover : KioskPageUI
{
    string approverno;
    //string stage;

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        //... temporarily hardcoded
        approverno = "0";
        if (!IsPostBack)
            BindGrid();
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }
    private DataSet GetData()
    {
        SqlParameter[] spParams;

        if (((HiddenField)EmpHeader1.FindControl("hdDateSelection")).Value == "")
            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverNo", approverno)

            };
        else
        {
            string sdate = ((HiddenField)EmpHeader1.FindControl("hdDateStart")).Value;
            string edate = ((HiddenField)EmpHeader1.FindControl("hdDateEnd")).Value;

            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverNo", approverno)
                ,new SqlParameter("@StartDate", sdate )
                ,new SqlParameter("@EndDate", edate )
            };
        }

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_LeaveApprover", spParams);
        return ds;
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblreason = e.Row.FindControl("lblreason") as Label;
            string reason = lblreason.Text;
            if (reason.Length > 18)
            {
                lblreason.ToolTip = reason;
                lblreason.Text = reason.Substring(0, 15) + "...";
            }

            Literal litStage = e.Row.FindControl("litStage") as Literal;
            litStage.Text = (litStage.Text == "1" ? "For Final Approval" : "For Approval");

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        EmpHeader1.UpdateURL();
        BindGrid();
    }

}
