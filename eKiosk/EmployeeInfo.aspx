﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="EmployeeInfo.aspx.cs" Inherits="EmployeeInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<br />
<asp:Label ID="lblFullname" runat="server" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;
<asp:Label ID="lblResigned" runat="server" Font-Bold="true" ForeColor="Red" Visible="false" Text="(Resigned)"></asp:Label>
<hr />

<table width="800px">
<tr>
<td valign="top">
    <table width="100%">
        <tr><td align="right">Employee ID:</td>
            <td><asp:Label ID="lblEmployeeID" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Machine ID:</td>
            <td> <asp:Label ID="lblMachineID" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Date Employed:</td>
            <td><asp:Label ID="lblDateEmployed" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Date Regularized:</td>
            <td><asp:Label ID="lblDateRegularized" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>

        <tr><td align="right">Tardy Exempt:</td>
            <td><asp:Label ID="lblTardyExempt" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Undertime Exempt:</td>
            <td><asp:Label ID="lblUTExempt" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Overtime Exempt:</td>
            <td><asp:Label ID="lblOTExempt" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Non-swiper:</td>
            <td><asp:Label ID="lblNonSwiper" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Company ID:</td>
            <td><asp:Label ID="lblCompanyID" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>

    </table>
</td>

<td valign="top">
    <table width="100%">    
        <tr><td align="right">Position:</td>
            <td><asp:Label ID="lblPosition" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Home Shift:</td>
            <td><asp:Label ID="lblShift" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">RestDay:</td>
            <td><asp:Label ID="lblRestDay" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Level 1 main approver:</td>
            <td><asp:Label ID="lblApprover1" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Level 1 back-up approver:</td>
            <td><asp:Label ID="lblApprover2" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right">Level 2 main approver:</td>
            <td><asp:Label ID="lblApprover3" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
    
        <tr><td align="right">Level 2 back-up approver:</td>
            <td><asp:Label ID="lblApprover4" runat="server" Font-Bold="True"></asp:Label> </td>
        </tr>
        <tr><td align="right"><asp:Label ID="lblLevel2Disp" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblLevel2" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr><td align="right"><asp:Label ID="lblLevel3Disp" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblLevel3" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr><td align="right"><asp:Label ID="lblLevel4Disp" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblLevel4" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
    </table>
</td>
</tr>
</table>


</asp:Content>

