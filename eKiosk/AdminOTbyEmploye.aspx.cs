using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;


public partial class AdminOTbyEmploye : KioskPageUI
{
    string _EmployeeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        _EmployeeID = _Url["EmpID"];

        base.GetUserSession();
        if (!IsPostBack)
        {
            //... link to correctattendance visibility
            lbBack.Visible = (_Url["Date"] != "");

            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();

            lblFullname.Text = _Url["EmpName"];
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthByEmpRange",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _EmployeeID), 
                new SqlParameter("@StartDate", txtDateStart.Text), 
                new SqlParameter("@EndDate", txtDateEnd.Text), 
                new SqlParameter("@DBname", _KioskSession.DB)             
            });
        gvEmp.DataSource = ds.Tables[0];
        gvEmp.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthByEmpRange",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@EmployeeID", _EmployeeID), 
                new SqlParameter("@StartDate", txtDateStart.Text), 
                new SqlParameter("@EndDate", txtDateEnd.Text), 
                new SqlParameter("@IsBlankData", true),
                new SqlParameter("@DBname", _KioskSession.DB)             
            });

        ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

        gvEmp.DataSource = ds.Tables[0];
        gvEmp.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvEmp.DataBind();
        hfIsAdd.Value = "1";
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");

            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;

                if ((bool)DataBinder.Eval(e.Row.DataItem, "Posted") == true)
                {
                    ibEdit.Visible = false;
                    ibDelete.Visible = false;
                }
                else
                {
                    ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
                }
            }        
        }
    }
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvEmp.DataKeys[e.RowIndex].Values["ID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthDelete",
            new SqlParameter[] {
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@ID", key), 
                new SqlParameter("@DBname", _KioskSession.DB),
                new SqlParameter("@LastUpdBy", _KioskSession.UID),
                new SqlParameter("@LastUpdDate", DateTime.Now)
            });

        gvEmp.EditIndex = -1;
        gvEmp.SelectedIndex = -1;
        BindGrid();

    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        gvEmp.SelectedIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvEmp.DataKeys[e.RowIndex].Values["ID"].ToString();

        TextBox txtOTDate = gvEmp.Rows[e.RowIndex].FindControl("txtOTDate") as TextBox;
        TextBox tbOTStart = gvEmp.Rows[e.RowIndex].FindControl("tbOTStart") as TextBox;
        TextBox tbOTEnd = gvEmp.Rows[e.RowIndex].FindControl("tbOTEnd") as TextBox;
        TextBox tbReason = gvEmp.Rows[e.RowIndex].FindControl("tbReason") as TextBox;

        DateTime in1;
        DateTime out1;
        string othours = "0.00";

        in1 = DateTime.Parse(txtOTDate.Text + " " + tbOTStart.Text);
        out1 = DateTime.Parse(txtOTDate.Text + " " + tbOTEnd.Text);

        if (in1 > out1)
            out1 = out1.AddHours(24);

        if (txtOTDate.Text.Length > 0 && tbOTStart.Text.Length > 0 && tbOTEnd.Text.Length > 0)
        {
            TimeSpan ts = out1.Subtract(in1);
            othours = Bns.AttUtils.Tools.TimeToDecimalRounded(ts.Hours, ts.Minutes);
            if (othours.ToDecimal()  < 1)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OTError", "alert('Overtime hours should be at least 1 hour.');", true);
                return;
            }
        }

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@EmployeeID", _Url["EmpID"]));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@OTDate", txtOTDate.Text));
        sqlparam.Add(new SqlParameter("@OTstart", in1));
        sqlparam.Add(new SqlParameter("@OTend", out1));

        if (hfIsAdd.Value == "0")
            sqlparam.Add(new SqlParameter("@ID", key));

        string ErrMsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();
        if (ErrMsg != "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OTError", "alert('" + ErrMsg + "');", true);
        }
        else
        {
            sqlparam.Add(new SqlParameter("@ApprovedOT", othours));
            sqlparam.Add(new SqlParameter("@Reason", tbReason.Text));
            sqlparam.Add(new SqlParameter("@Status", "Approved"));
            sqlparam.Add(new SqlParameter("@Stage", "2"));

            sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
            sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

            if (hfIsAdd.Value == "0")
            {
                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdate", sqlparam.ToArray());
            }
            else
            {
                sqlparam.Add(new SqlParameter("@DateFiled", DateTime.Now));
                sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
                sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthInsert", sqlparam.ToArray());
            }

            gvEmp.EditIndex = -1;
            gvEmp.SelectedIndex = -1;
            BindGrid();
        }
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        gvEmp.EditIndex = -1;
        gvEmp.EditIndex = -1;
        BindGrid();
    }
    protected void lbBack_Click(object sender, EventArgs e)
    {
        SecureUrl url = new SecureUrl(string.Format("AttendanceEdit.aspx?EmpID={0}&Date={1}", _Url["EmpID"], _Url["Date"]));
        Response.Redirect(url.ToString());
    }
}