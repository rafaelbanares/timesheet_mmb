﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveApproverList.aspx.cs" Inherits="LeaveApproverList" Title="Leave Approver Lists" %>
<%@ Register src="EmpHeaderFilter2.ascx" tagname="EmpHeaderFilter2" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>
    <script type="text/javascript">
        function ValidateGrp(grp) {
            Page_ClientValidate(grp);
        }
    </script>

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="Leave Applications For My Approval"></asp:Label>   
    <br />
    <br />
    <uc1:EmpHeaderFilter2 ID="EmpHeader1" runat="server" />
    <hr />
        <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" Width="100%" 
            DataKeyNames="id,status" onrowdeleting="gvEmp_RowDeleting" 
            onselectedindexchanged="gvEmp_SelectedIndexChanged" 
            onrowcommand="gvEmp_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                    <asp:HiddenField ID="hfCompanyID" runat="server" Value='<%# Bind("CompanyID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litFullname" runat="server" Text='<%# Bind("FullName") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Date Filed" SortExpression="DateFiled">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "DateFiled"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date" SortExpression="StartDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txtStart" runat="server" Text='<%# Bind("StartDate", "{0:MM/dd/yyyy}") %>'
                        CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtStart" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                    </asp:ImageButton>&nbsp;<asp:RequiredFieldValidator ID="rfvStart" runat="server"
                        ErrorMessage="Date start is required" ControlToValidate="txtStart" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                            ID="meeStart" runat="server" TargetControlID="txtStart" OnInvalidCssClass="MaskedEditError"
                            OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                            Mask="99/99/9999" ErrorTooltipEnabled="True">
                        </ajaxToolkit:MaskedEditExtender>
                    <ajaxToolkit:CalendarExtender ID="caleStart" runat="server" TargetControlID="txtStart"
                        PopupButtonID="ibtxtStart">
                    </ajaxToolkit:CalendarExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litStartDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "StartDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="End Date" SortExpression="EndDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEnd" runat="server" Text='<%# Bind("EndDate", "{0:MM/dd/yyyy}") %>'
                        CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                    <asp:ImageButton ID="ibtxtEnd" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                    </asp:ImageButton>
                    <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ErrorMessage="Date end is required"
                        ControlToValidate="txtEnd" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                            ID="meeEnd" runat="server" TargetControlID="txtEnd" OnInvalidCssClass="MaskedEditError"
                            OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                            Mask="99/99/9999" ErrorTooltipEnabled="True">
                        </ajaxToolkit:MaskedEditExtender>
                    <ajaxToolkit:CalendarExtender ID="caleEnd" runat="server" TargetControlID="txtEnd"
                        PopupButtonID="ibtxtEnd">
                    </ajaxToolkit:CalendarExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litEndDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "EndDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="No. of days">
                <EditItemTemplate>
                    <asp:TextBox ID="txtLeaveDays" runat="server" CssClass="ControlDefaults" Width="40px" ValidationGroup="Trans">1.00</asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litDays" runat="server" Text='<%# Bind("Days") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Leave">
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlLeave" runat="server" CssClass="ControlDefaults" Width="60px" DataTextField="Description" DataValueField="Code">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvLeave" runat="server" ErrorMessage="Leave code is required" ControlToValidate="ddlLeave" ValidationGroup="Trans">*</asp:RequiredFieldValidator></td>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litLeave" runat="server" Text='<%# Bind("Code") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason">
                <EditItemTemplate>
                    <asp:TextBox ID="tbreason" runat="server" Text='<%# Bind("reason") %>' 
                        Width="100px" MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReason" runat="server" 
                        ControlToValidate="tbreason" 
                        ErrorMessage="Please indicate a reason for corrrections" 
                        ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>                    
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status" SortExpression="Stage">
                <ItemTemplate>
                    <asp:Literal ID="litStage" runat="server" Text='<%# Bind("Stage") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle Width="62px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibApproved" runat="server" ToolTip="Approve" 
                        ImageUrl="~/Graphics/check.gif" 
                        CommandName="Select" />
                    <asp:ImageButton ID="ibRecall" runat="server" CommandName="recall" ToolTip="Recall" ImageUrl="~/Graphics/cancel.gif" OnClientClick="return window.confirm('Recall Leave?' );" />
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" Visible ="false" />
                    <asp:ImageButton ID="ibDeny" runat="server" ToolTip="Decline" 
                        ImageUrl="~/Graphics/delete.gif"                         
                        OnClientClick="return window.confirm('Decline Leave?' );" 
                        CommandName="Delete" />
                    <asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <br />
    <br />
    <table>
        <tr align='left'>
            <td style="width:220px">
                <asp:Button ID="btnForApproval" runat="server" 
                    Text="Approve all - For Approval" Width="220px" 
                    onclick="btnForApproval_Click" Visible="False" /></td>
            <td style="width:50px">
                <asp:Button ID="Button1" runat="server" Text="Button" style="display:none" />
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlDecline" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
        <div><br />
            <table style="width:100%;" cellpadding="1" cellspacing="1">
                <tr>
                    <td>Reason</td>
                    <td>
                        <asp:TextBox ID="txtDecReason" runat="server" CssClass="ControlDefaults" 
                            Height="65px" MaxLength="100" TextMode="MultiLine" 
                            ToolTip="" Width="340px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDecReason" runat="server" ErrorMessage="required" ControlToValidate="txtDecReason" ValidationGroup="declinepop"></asp:RequiredFieldValidator>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2" style="width:100%; text-align:center">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    <br />
                        <asp:Button ID="btnDecline" runat="server" 
                            OnClientClick="ValidateGrp('declinepop')" Text="Decline" 
                            onclick="btnDecline_Click" />
                        &nbsp;
                        <asp:Button ID="btnDecReasonCancel" runat="server" Text="Cancel" />                  
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpe3" runat="server"
      TargetControlId="Button1"
      PopupControlID="pnlDecline"
      CancelControlID="btnDecReasonCancel"
      BackgroundCssClass="modalBackground"  />
      
        <asp:HiddenField ID="hfdecRowid" runat="server" />
      
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="btnFU"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
<asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none"  />


</asp:Content>

