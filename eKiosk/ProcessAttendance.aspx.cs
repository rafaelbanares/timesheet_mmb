﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Business;
using BNS.TK.Entities;

public partial class ProcessAttendance : KioskPageUI
{
    //const string emptycodevalue = "               "; //... space 15 (default value)
    const string emptycodevalue = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        lblMessage.Text = "";
        
        if (!IsPostBack)
        {
            PopulateScreenDefault();
            DisableDropdown(0);

            txtTranStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtTranEnd.Text = _KioskSession.EndDate.ToShortDateString();
        }
    }

    private void PopulateScreenDefault()
    {
        rbLevel1.Text = "One " + _KioskSession.Level[0];
        rbLevel2.Text = "One " + _KioskSession.Level[1];
        rbLevel3.Text = "One " + _KioskSession.Level[2];
        rbLevel4.Text = "One " + _KioskSession.Level[3];
    }

    private void BindGrid(int nlevel)
    {
        if (nlevel > 0)
        {
            DropDownList[] ddlLevel = { ddlLevel1, ddlLevel2, ddlLevel3, ddlLevel4, ddlOneEmployee };

            DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString,
                    CommandType.StoredProcedure,
                    MainDB + ".dbo.usa_SelectionsByCompanyID",
                    new SqlParameter[]
            {
                new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                new SqlParameter("@DropdownNo", nlevel),
                new SqlParameter("@DBname", _KioskSession.DB)
            });

            int i = nlevel - 1;

            ddlLevel[i].DataSource = ds.Tables[0];
            ddlLevel[i].DataBind();
            ddlLevel[i].Items.Insert(0, new ListItem("", emptycodevalue));
        }
    }

    protected void ddlLevel4_SelectedIndexChanged(object sender, EventArgs e)
    {
        //UpdateDropdown();
    }

    private void UpdateDropdown()
    {
        string selected = ddlLevel4.SelectedValue;

        if (selected.Trim() != String.Empty)
        {
            DataRow rowParent = SqlHelper.ExecuteDataset(_ConnectionString,
                    CommandType.StoredProcedure,
                    MainDB + ".dbo.usa_Level4GetParents",
                    new SqlParameter[]
                {
                    new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                    new SqlParameter("@Level4Code", selected),
                    new SqlParameter("@DBname", _KioskSession.DB)
                }).Tables[0].Rows[0];

            ddlLevel3.SelectedValue = rowParent["Level3Code"].ToString().Trim();
            ddlLevel2.SelectedValue = rowParent["Level2Code"].ToString().Trim();
            ddlLevel1.SelectedValue = rowParent["Level1Code"].ToString().Trim();

            BindGrid(2);
            BindGrid(3);
            //ddlLevel4.SelectedValue = selected;
        }
    }

    protected void rbAllEmp_CheckedChanged(object sender, EventArgs e)
    {
        DisableDropdown(0);
    }
    protected void rbLevel1_CheckedChanged(object sender, EventArgs e)
    {
        DisableDropdown(1);
    }
    protected void rbLevel2_CheckedChanged(object sender, EventArgs e)
    {
        DisableDropdown(2);
    }
    protected void rbLevel3_CheckedChanged(object sender, EventArgs e)
    {
        DisableDropdown(3);
    }
    protected void rbLevel4_CheckedChanged(object sender, EventArgs e)
    {
        DisableDropdown(4);
    }
    protected void rbOneEmp_CheckedChanged(object sender, EventArgs e)
    {
        DisableDropdown(5);
    }

    private void DisableDropdown(int nlevel)
    {
        DropDownList[] ddlLevel = { ddlLevel1, ddlLevel2, ddlLevel3, ddlLevel4, ddlOneEmployee };
        for (int i = 0; i < 5; i++)
        {
            ddlLevel[i].SelectedIndex = -1;
            ddlLevel[i].Enabled = (i + 1 == nlevel);
        }
            
        BindGrid(nlevel);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        //rsb disable during testing
        //if (DateTime.Parse(txtTranStart.Text) >= DateTime.Today || DateTime.Parse(txtTranEnd.Text) >= DateTime.Today)
        //{
        //    if (!ClientScript.IsStartupScriptRegistered("firstin"))
        //    {
        //        ClientScript.RegisterStartupScript(typeof(Page), "firstin",
        //            @"<script language=JavaScript>alert('Cannot process future date or date today.')</script>");
        //        return;
        //    }
        //}

        string empfilter = "";

        if (rbAllEmp.Checked)
            empfilter = "";
        else if (rbLevel1.Checked)
            empfilter = " AND e.Level1 = '" + ddlLevel1.SelectedValue + "' ";
        else if (rbLevel2.Checked)
            empfilter = " AND e.Level2 = '" + ddlLevel2.SelectedValue + "' ";
        else if (rbLevel3.Checked)
            empfilter = " AND e.Level3 = '" + ddlLevel3.SelectedValue + "' ";
        else if (rbLevel4.Checked)
            empfilter = " AND e.Level4 = '" + ddlLevel4.SelectedValue + "' ";
        else if (rbOneEmp.Checked)
            empfilter = " AND e.EmployeeID = '" + ddlOneEmployee.SelectedValue + "' ";

        //SqlParameter[] spParams = new SqlParameter[] {
        //    new SqlParameter("@DBName", _KioskSession.DB)
        //    ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
        //    ,new SqlParameter("@sdate", txtTranStart.Text)
        //    ,new SqlParameter("@edate", txtTranEnd.Text)
        //    ,new SqlParameter("@filter", empfilter )
        //};

        //SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_GenerateAttendanceFromTimeTrans", spParams);

        ReprocessAttendance(DateTime.Parse(txtTranStart.Text), DateTime.Parse(txtTranEnd.Text));

        lblMessage.Text = "Processing completed...";
        Response.Redirect("AttendanceProcess.aspx");

    }


    private void ReprocessAttendance(DateTime from, DateTime to)
    {
        string emp = "";
        if (rbOneEmp.Checked)
            emp = ddlOneEmployee.SelectedValue;

        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(from, to, emp);
    }
}
