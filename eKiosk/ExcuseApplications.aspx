﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ExcuseApplications.aspx.cs" Inherits="ExcuseApplications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
    function ValidateGrp(grp) {
        Page_ClientValidate(grp);
    }

    //close popup on esc
    function pageLoad(sender, args) {
        if (!args.get_isPartialLoad()) {
            $addHandler(document, "keydown", onKeyDown);
        }
    }
    function onKeyDown(e) {
        if (e && e.keyCode == Sys.UI.Key.esc) {
            $find('ctl00_phmc_mpe').hide();
        }
    } 
    </script>
<asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="My Excuse UT Applications"></asp:Label>   
    <br />    
    <br />
    <div style="width:95%; text-align:right">
        Year:&nbsp;<asp:DropDownList ID="ddlYear" runat="server" Width="80px" 
            AutoPostBack="true" onselectedindexchanged="ddlYear_SelectedIndexChanged" ></asp:DropDownList>
    </div>
    <hr />        
    <asp:LinkButton ID="lnkApplyExcuse" runat="server" onclick="lnkApplyExcuse_Click" >Apply for Excuse</asp:LinkButton>
    <br />
    <br />
    <asp:UpdatePanel ID="updPnl1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <div style="min-height:300px">
            <asp:GridView ID="gvEmployees" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="31"
                Width="95%" onrowdatabound="gvEmployees_RowDataBound" 
                onpageindexchanging="gvEmployees_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ExcuseDate" HeaderText="Excuse Date" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="120px" HtmlEncode="False" ></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="Excuse<br>Time IN" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="120px" HtmlEncode="False" ></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="Excuse<br>Time OUT" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="120px" HtmlEncode="False" ></asp:BoundField>                    
                    <asp:TemplateField HeaderText="Remarks" >
                        <ItemTemplate >
                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:BoundField DataField="Status" HeaderText="Status" HtmlEncode="False" Visible="true" ItemStyle-Width="80px" ></asp:BoundField>                
                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit_small.gif" OnClick="ibEdit_Click" ToolTip="Request" />
                            <asp:HiddenField ID="hfDate" runat="server" Value='<%# Bind("ExcuseDate") %>' />
                        </ItemTemplate>
                        <ItemStyle Width="50px"></ItemStyle>
                    </asp:TemplateField>

                </Columns>
                <RowStyle Height="22px" />
                <EmptyDataTemplate>
                    <span style="color:Red">No record found</span>
                </EmptyDataTemplate>
                <PagerStyle HorizontalAlign="Right" />
                <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
            </asp:GridView>
            <asp:Button ID="btnFU" runat="server" Text="" onclick="btnFU_Click" style="display:none" />
            <br />
        </div>        
        <br />       
        <asp:HiddenField ID="hfIsAdd" runat="server" /> 
        <%--<asp:LinkButton ID="lbEditOB" Text = "" runat="server"></asp:LinkButton>
            <asp:Panel ID="pnlPopup" runat="server" Width="600px" Style="display: none" CssClass="modalPopup">
            <div><br />

            </div>
        </asp:Panel>
        <asp:HiddenField ID="hfIsAdd" runat="server" />
        <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server"
          TargetControlId="lbEditOB" 
          PopupControlID="pnlPopup"       
          CancelControlID="btnPopCancel"
          BackgroundCssClass="modalBackground"  />  --%>
       </ContentTemplate>
   </asp:UpdatePanel>
    <%--------------------------------------------------------------- modal popup layout starts here -----------------------------%>
    <div id="pnlExcuseEntry" style="width: 600px; display: none;" >    
        <div class="popup_Container">
            <div class="popup_Titlebar" id="PopupHeader">
                <div class="TitlebarLeft">Excused UT</div>
                <div class="TitlebarRight" onclick="$get('ctl00_phmc_btnPopCancel').click();"> </div>
            </div>
            <div class="popup_Body" style="width:98%;" >
                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
	            <ContentTemplate>
                <asp:HiddenField ID="hdDatePop" runat="server" />
                <asp:Label ID="lblPopErrorMsg" runat="server" ForeColor="Red"></asp:Label>
                     <table>
                        <tr>
                            <td><asp:Label ID="Label1" runat="server" Text="Excuse Date"></asp:Label></td>
                            <td><asp:TextBox ID="txtOBdate" runat="server" Width="75px" AutoPostBack="True" 
                                    ontextchanged="txtOBdate_TextChanged" ></asp:TextBox>
                                <asp:HiddenField ID="hdAppDate" runat="server" />
                                <ajaxToolkit:MaskedEditExtender
                                    ID="mex1" runat="server" TargetControlID="txtOBdate" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                            </td>
                        </tr>    
                        <tr>
                            <td><asp:Label ID="Label2" runat="server" Text="Excuse Time Start"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtStartDate" runat="server" Width="75px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender
                                    ID="meSdate" runat="server" TargetControlID="txtStartDate" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                                <asp:TextBox ID="txtStartTime" runat="server" Width="60px"></asp:TextBox>
                                <ajaxToolkit:MaskedEditExtender ID="meStartTime" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                                    Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtStartTime">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:MaskedEditValidator ID="mveStartTime" runat="server" 
                                    ControlExtender="meStartTime" ControlToValidate="txtStartTime"
                                        Display="Dynamic" EmptyValueBlurredText="*" 
                                    InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                                        ValidationGroup="modaltrans">
                                </ajaxToolkit:MaskedEditValidator>                            
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label3" runat="server" Text="Excuse Time End"></asp:Label></td>
                            <td><asp:TextBox ID="txtEndDate" runat="server" Width="75px"></asp:TextBox>
                                <ajaxToolkit:MaskedEditExtender
                                    ID="meEndDate" runat="server" TargetControlID="txtEndDate" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                                <asp:TextBox ID="txtEndTime" runat="server" Width="60px"></asp:TextBox>
                                <ajaxToolkit:MaskedEditExtender ID="meEndTime" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                                    Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                    OnInvalidCssClass="MaskedEditError" TargetControlID="txtEndTime">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:MaskedEditValidator ID="mveEndTime" runat="server" 
                                    ControlExtender="meEndTime" ControlToValidate="txtEndTime"
                                        Display="Dynamic" EmptyValueBlurredText="*" 
                                    InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                                        ValidationGroup="modaltrans">
                                </ajaxToolkit:MaskedEditValidator>                            
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label5" runat="server" Text="Remarks"></asp:Label></td>
                            <td><asp:TextBox ID="txtRemarks" runat="server" Height="53px" TextMode="MultiLine" 
                                    Width="261px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" 
                                    ControlToValidate="txtRemarks" ErrorMessage="* required" 
                                    ValidationGroup="modaltrans"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>               
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <hr style="color: #add8e6"/>
            <div class="popup_Buttons">
                <asp:Button ID="btnEditSave" runat="server" Text="Save" OnClientClick="ValidateGrp('modaltrans')" onclick="btnEditSave_Click"/>
                <asp:Button ID="btnPopCancel" runat="server" Text="Cancel" />

            </div>
        </div>
    </div>
    <%--------------------------------------------------------------- modal popup layout ends -----------------------------%>     
    <asp:LinkButton ID="lbEditXcuse" Text = "" runat="server"></asp:LinkButton>
    <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server"
        TargetControlId="lbEditXcuse" 
        PopupControlID="pnlExcuseEntry"
        CancelControlID="btnPopCancel"
        Drag="true"
        PopupDragHandleControlID="PopupHeader"
        BackgroundCssClass="modalBackground"  />

</asp:Content>

