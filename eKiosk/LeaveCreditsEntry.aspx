﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="LeaveCreditsEntry.aspx.cs" Inherits="LeaveCreditsEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Leave Credits Entry"></asp:Label><br />
                <br />
                <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname S."></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="Add leave credits" /><asp:LinkButton ID="lbNew" runat="server"
            CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">Add leave credits</asp:LinkButton><br />

    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False"
                CssClass="DataGridStyle" OnRowDataBound="gvEmp_RowDataBound" PageSize="16" Width="100%" OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" OnRowUpdating="gvEmp_RowUpdating">
                <Columns>
                
                    <asp:TemplateField HeaderText="Transaction Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTranDate" runat="server" CssClass="ControlDefaults" Text='<%# Bind("TranDate", "{0:MM/dd/yyyy}") %>'
                                Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtTranDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditExtender ID="meeTranDate" runat="server"
                                    ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"
                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtTranDate">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleTranDate" runat="server" PopupButtonID="ibtxtTranDate"
                                TargetControlID="txtTranDate">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litTranDate" runat="server" Text='<%# Bind("TranDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Leave Description">
                        <ItemStyle Width="150px" />
                        <ItemTemplate>
                            <asp:Label ID="lblLeave" runat="server" Text='<%# Bind("LeaveDesc") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLeave" runat="server" CssClass="ControlDefaults" 
                                DataTextField="Description" DataValueField="Code" ValidationGroup="Trans"></asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Leave Days" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("Earned") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLD" runat="server" Text='<%# Bind("Earned") %>' ValidationGroup="Trans" Width="90px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="meeLD" runat="server" ErrorTooltipEnabled="True"
                                InputDirection="RightToLeft" Mask="99999.99" MaskType="Number" MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtLD" AcceptNegative="Left" >
                            </ajaxToolkit:MaskedEditExtender>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Valid From">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtValidFrom" runat="server" CssClass="ControlDefaults" Text='<%# Bind("ValidFrom", "{0:MM/dd/yyyy}") %>'
                                Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtValidFrom" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditExtender ID="meeValidFrom" runat="server"
                                    ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"
                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtValidFrom">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleValidFrom" runat="server" PopupButtonID="ibtxtValidFrom"
                                TargetControlID="txtValidFrom">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litValidFrom" runat="server" Text='<%# Bind("ValidFrom", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Valid To">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtValidTo" runat="server" CssClass="ControlDefaults" Text='<%# Bind("ValidTo", "{0:MM/dd/yyyy}") %>'
                                Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtValidTo" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditExtender ID="meeValidTo" runat="server"
                                    ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"
                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtValidTo">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleValidTo" runat="server" PopupButtonID="ibtxtValidTo"
                                TargetControlID="txtValidTo">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litValidTo" runat="server" Text='<%# Bind("ValidTo", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Remarks">
                        <ItemStyle VerticalAlign="Top" Width="210px" />
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Bind("Remarks") %>' Width="200px" MaxLength="100"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle VerticalAlign="Top" Width="42px" />
                        <ItemTemplate>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF"
                                ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                    </asp:TemplateField>                    

                </Columns>                
                <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
                <HeaderStyle CssClass="DataGridHeaderStyle" />
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

