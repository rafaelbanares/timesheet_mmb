<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="SatShiftDisplay.aspx.cs" Inherits="SatShiftDisplay" Title="Saturday Shift Display" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>
    <script type="text/javascript">
    function openWD(sUrl) {
        popupWorkDist = wopen(sUrl, "popupWorkDist", 400, 580);
    }
    function openSwitch(sUrl) {
        popupWorkDist = wopen(sUrl, "popupSwitch", 330, 190);
    }
    function refresh() {
        $get("<%=btnRefresh.ClientID%>").click();
        //alert('changes were saved');
    }

    </script>
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Saturday Shift Settings"></asp:Label><br /><br />
        
    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" style="display:none" onclick="btnRefresh_Click"  />
        
    <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" 
        CssClass="DataGridStyle" Width="800px" 
        onrowdatabound="gvMain_RowDataBound" >
        <Columns>
            <asp:BoundField DataField="WeekFrom" HeaderText="Week<br>From" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"  />
            <asp:BoundField DataField="WeekTo" HeaderText="Week<br>To" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"  />
            <asp:BoundField DataField="ShiftDesc" HeaderText="Weekday<br>Shift" HtmlEncode="false" />
            <asp:BoundField DataField="SatDate" HeaderText="Saturday<br>Date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false" />
            
            <asp:TemplateField HeaderText="All Employee<br>Saturday Shift">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkSwitch" runat="server">Switch</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Warehouse<br>Department">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkSelect" runat="server">Select 9am-2:30pm Shift Warehouse Employees</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>

</asp:Content>

