﻿using System;
using Bns.AttendanceUI;

using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using Bns.Attendance;

public partial class SatShiftNew : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        lblError.Text = "";
    }

    private bool validate()
    {
        if (DateTime.Parse(txtSatStart.Text).DayOfWeek != DayOfWeek.Saturday || DateTime.Parse(txtSatEnd.Text).DayOfWeek != DayOfWeek.Saturday)
        {
            lblError.Text = "Start date and end date must fall on a saturday";
            return false;
        }
        return true;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (validate())
        {
            SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usp_CompSaturday_Generate",
                new SqlParameter[] {
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                            ,new SqlParameter("@appyear", DateTime.Now.Year ) 
                            ,new SqlParameter("@satfrom", txtSatStart.Text) 
                            ,new SqlParameter("@satto", txtSatEnd.Text) 
                            ,new SqlParameter("@updatedby", _KioskSession.UID)             
            }
            );
            Response.Redirect("SatShiftDisplay.aspx");
        }
    }
}
