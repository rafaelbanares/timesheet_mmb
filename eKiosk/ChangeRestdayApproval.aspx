﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ChangeRestdayApproval.aspx.cs" Inherits="ChangeRestdayApproval" Title="Change Restday Approval" %>
<%@ Register src="EmpHeaderFilter2.ascx" tagname="EmpHeaderFilter2" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <uc1:EmpHeaderFilter2 ID="EmpHeader1" runat="server" />
    <hr />
        <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowEditing="gvEmp_RowEditing" Width="100%" 
            DataKeyNames="id,status" onrowdeleting="gvEmp_RowDeleting" 
            onselectedindexchanged="gvEmp_SelectedIndexChanged" 
            onrowcommand="gvEmp_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                    <asp:HiddenField ID="hfCompanyID" runat="server" Value='<%# Bind("CompanyID") %>' />
                </ItemTemplate>
            </asp:TemplateField>            
            
            <asp:BoundField HeaderText="Employee Name" DataField="FullName" SortExpression="FullName" />
            <asp:BoundField HeaderText="Date Filed" DataField="DateFiled" SortExpression="DateFiled" DataFormatString="{0:d}" /> 
            <asp:BoundField HeaderText="Start Date" DataField="StartDate" SortExpression="StartDate" DataFormatString="{0:d}" />
            <asp:BoundField HeaderText="End Date" DataField="EndDate" SortExpression="EndDate" DataFormatString="{0:d}" />
            <asp:TemplateField HeaderText="Restday">
                <ItemTemplate>
                    <asp:Literal ID="litRestdayDescr" runat="server" Text='<%# Bind("RestdayDescr") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason">                
                <ItemTemplate>
                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>                    
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Literal ID="litStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle Width="62px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibApproved" runat="server" ToolTip="Approve" ImageUrl="~/Graphics/check.gif" CommandName="Select" />
                    <asp:ImageButton ID="ibRecall" runat="server" CommandName="recall" ToolTip="Recall" ImageUrl="~/Graphics/cancel.gif" OnClientClick="return window.confirm('Recall Restday?' );" />
                    <asp:ImageButton ID="ibDeny" runat="server" ToolTip="Decline" ImageUrl="~/Graphics/delete.gif" OnClientClick="return window.confirm('Decline Restday?' );" CommandName="Delete" />
                    <asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <br />
    <br />
    <table>
        <tr align='left'>
            <td style="width:220px">
                &nbsp;</td>
            <td style="width:50px">
                <asp:Button ID="Button1" runat="server" Text="Button" style="display:none" />
            </td>
            <td style="width:220px">
                &nbsp;</td>
        </tr>
    </table>

    <asp:Panel ID="pnlDecline" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
        <div><br />
            <table style="width:100%;" cellpadding="1" cellspacing="1">
                <tr>
                    <td>Reason</td>
                    <td>
                        <asp:TextBox ID="txtDecReason" runat="server" CssClass="ControlDefaults" 
                            Height="65px" MaxLength="100" TextMode="MultiLine" 
                            ToolTip="" Width="340px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDecReason" runat="server" ErrorMessage="required" ControlToValidate="txtDecReason" ValidationGroup="declinepop"></asp:RequiredFieldValidator>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2" style="width:100%; text-align:center">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    <br />
                        <asp:Button ID="btnDecline" runat="server" 
                            OnClientClick="ValidateGrp('declinepop')" Text="Decline" 
                            onclick="btnDecline_Click" />
                        &nbsp;
                        <asp:Button ID="btnDecReasonCancel" runat="server" Text="Cancel" />                  
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpe3" runat="server"
      TargetControlId="Button1"
      PopupControlID="pnlDecline"
      CancelControlID="btnDecReasonCancel"
      BackgroundCssClass="modalBackground"  />
      
        <asp:HiddenField ID="hfdecRowid" runat="server" />
      
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="btnFU"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
<asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none"  />


</asp:Content>

