<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="TransPeriodList.aspx.cs" Inherits="TransPeriodList" Title="MMB Kronos - TransPeriodList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Period Definitions"></asp:Label><br />
    <br />    
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click" ToolTip="New Period" />
        <asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">New Period</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" 
                OnRowUpdating="gvMain_RowUpdating" Width="800px" 
                DataKeyNames="rowID">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>                
                    <asp:TemplateField HeaderText="Attendance<BR>Cut-off Start">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtStartDate" runat="server" Width="72px" Text='<%# Bind("StartDate") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtStartDate" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeStartDate" runat="server" TargetControlID="txtStartDate"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleStartDate" runat="server" TargetControlID="txtStartDate" PopupButtonID="ibtxtStartDate"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litStartDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "StartDate"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Attendance<BR>Cut-off End">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtEndDate" runat="server" Width="72px" Text='<%# Bind("EndDate") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtEndDate" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meeEndDate" runat="server" TargetControlID="txtEndDate"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="caleEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="ibtxtEndDate"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litEndDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "EndDate"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Payroll<BR>Period Start">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtPayStart" runat="server" Width="72px" Text='<%# Bind("PayStart") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtPayStart" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meePayStart" runat="server" TargetControlID="txtPayStart"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="calePayStart" runat="server" TargetControlID="txtPayStart" PopupButtonID="ibtxtPayStart"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litPayStart" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "PayStart"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Payroll<BR>Period End">
                        <ItemStyle Width="120px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtPayEnd" runat="server" Width="72px" Text='<%# Bind("PayEnd") %>' CssClass="TextBoxDate" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton id="ibtxtPayEnd" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:MaskedEditExtender id="meePayEnd" runat="server" TargetControlID="txtPayEnd"
                                OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" 
                                MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True">
                            </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender id="calePayEnd" runat="server" TargetControlID="txtPayEnd" PopupButtonID="ibtxtPayEnd"></ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal id="litPayEnd" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "PayEnd"), "MM/dd/yyyy") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Posted" HeaderText="Posted" HtmlEncode="False" ItemStyle-Width="100px" ReadOnly="true"></asp:BoundField>
                    
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>            
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>                        
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

