<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="OTAuthApproval.aspx.cs" Inherits="Attendance_OTAuthApproval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Overtime Approval"></asp:Label><br />
    <br />
    <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname, MI"></asp:Label><br />
    <br />
    <asp:HiddenField ID="hfOTAuthID" runat="server" />
    <br />
    <table class="TableBorder1" style="font-family: Verdana">
        <tr>
            <td align="right">
                Date filed:</td>
            <td>
                <asp:TextBox ID="txtDateFiled" runat="server" CssClass="TextBoxDate" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Overtime date:</td>
            <td>
                <asp:TextBox ID="txtOTDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtOTDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator id="mevOTDate" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtOTDate" 
                    ControlExtender="meeOTDate" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" Display="None" >*
                </ajaxToolkit:MaskedEditValidator>

            </td>
        </tr>
        <tr style="color: #000000">
            <td align="right">
                Overtime start:</td>
            <td>
                <asp:TextBox ID="txtin1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox>
                <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1"
                    ControlToValidate="txtin1" Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Time is invalid" ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Overtime end:</td>
            <td>
                <asp:TextBox ID="txtout1" runat="server" CssClass="ControlDefaults" Width="56px"></asp:TextBox>
                <ajaxToolkit:MaskedEditValidator ID="mevout1" runat="server" ControlExtender="meout1"
                    ControlToValidate="txtout1" Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Time is invalid" ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator></td>
        </tr>
        <tr style="color: #000000">
            <td align="right">
                Approved number of hours:</td>
            <td>
                <asp:TextBox ID="txtOTHours" runat="server" CssClass="ControlDefaults" Width="40px">0.00</asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Reason for overtime:</td>
            <td>
                <asp:TextBox ID="txtReason" runat="server" CssClass="ControlDefaults" Height="74px"
                    MaxLength="100" TextMode="MultiLine" Width="501px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReason"
                    ErrorMessage="Reason for overtime cannot be blank">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
            <td>
                &nbsp;<asp:Button ID="btnApproved" runat="server" CssClass="ControlDefaults" OnClick="btnApproved_Click"
                    Text="Approve" />
                <asp:Button ID="btnDeclined" runat="server" CssClass="ControlDefaults" OnClick="btnDeclined_Click"
                    OnClientClick="return confirm('Are you sure to decline this overtime?')" Text="Decline" />
                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="ControlDefaults"
                    OnClick="btnClose_Click" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    <ajaxToolkit:MaskedEditExtender ID="meeOTDate" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtOTDate">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleOTDate" runat="server" PopupButtonID="ibtxtOTDate"
        TargetControlID="txtOTDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeOTHours" runat="server" ErrorTooltipEnabled="True"
        InputDirection="RightToLeft" Mask="99.99" MaskType="Number" MessageValidatorTip="true"
        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtOTHours">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtin1">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtout1">
    </ajaxToolkit:MaskedEditExtender>
    <asp:HiddenField ID="hfEmployeeID" runat="server" />


</asp:Content>

