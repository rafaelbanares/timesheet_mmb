using Bns.AttendanceUI;
using BNS.TK.Entities;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TransPeriodList : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid(bool addnew = false)
    {
        var date1 = DateTime.Parse(DateTime.Today.Year - 1 + "-12-01");
        var date2 = DateTime.Parse(DateTime.Today.Year + 1 + "-01-31");

        var sql = "select * from TransPeriod where companyid = {0} and StartDate between {1} and {2} ";
        object[] parameters = { _KioskSession.CompanyID, date1, date2 };
        
        var tp = new TransPeriod();
        tp.DynamicQuery(sql, parameters);

        if (addnew)
        {
            tp.AddNew();
            gvMain.EditIndex = tp.RowCount - 1;
        }

        gvMain.DataSource = tp.DefaultView;
        gvMain.DataBind();
        hfIsAdd.Value = (addnew) ? "1" : "0";
    }
 

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{        
        //}
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {   
    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["rowID"].ToString();
        TextBox txtStartDate = gvMain.Rows[e.RowIndex].FindControl("txtStartDate") as TextBox;
        TextBox txtEndDate = gvMain.Rows[e.RowIndex].FindControl("txtEndDate") as TextBox;
        TextBox txtPayStart = gvMain.Rows[e.RowIndex].FindControl("txtPayStart") as TextBox;
        TextBox txtPayEnd = gvMain.Rows[e.RowIndex].FindControl("txtPayEnd") as TextBox;
        try
        {
            TransPeriod tp = new TransPeriod();
            if (hfIsAdd.Value == "1")
            {
                tp.AddNew();
                tp.CompanyID = _KioskSession.CompanyID;
                tp.s_StartDate = txtStartDate.Text;
                tp.s_EndDate = txtEndDate.Text;
                tp.Current = false;
                tp.Posted = false;
                tp.Finalized = false;            
            }
            else
            {
                //quick and dirty fix to edit primary key
                //better to change primary key to rowID
                UpdatePeriodKey(key.ToInteger(), txtStartDate.Text.ToDate(), txtEndDate.Text.ToDate());

                if (!tp.LoadByPrimaryKey(_KioskSession.CompanyID, txtStartDate.Text.ToDate(), txtEndDate.Text.ToDate())) 
                    throw new Exception("Error occured during saving.");
            }
         
            // this line below will cause concurrency violation error
            //tp.s_StartDate = txtStartDate.Text;
            //tp.s_EndDate = txtEndDate.Text;
            tp.s_PayStart = txtPayStart.Text;
            tp.s_PayEnd = txtPayEnd.Text;
            tp.AppYear = txtStartDate.Text.ToDate().Year;
        
            tp.Save();

            UpdateCurrent();

            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertmsg", "alert('Save Successful');", true);
        }
        catch (Exception ex)
        {            
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertmsg", "alert('Save Failed.');", true);
        }
        
         
        gvMain.EditIndex = -1;
        BindGrid();
    }

    private void UpdatePeriodKey(int rowID, DateTime startDate, DateTime endDate)
    {
        TransPeriod tp = new TransPeriod();
        object[] parameters = { startDate, endDate, rowID };
        tp.DynamicQuery("update TransPeriod set StartDate = {0}, EndDate = {1} where rowID = {2} ", parameters);
        tp = null;        
    }

    private void UpdateCurrent()
    {
        var tp = new TransPeriod();
        tp.DynamicQuery("UPDATE [TransPeriod] SET [Current] = 0 WHERE [Current] = 1", new object[] { });
        tp.DynamicQuery("UPDATE [TransPeriod] SET [Current] = 1 WHERE [Posted] = 0 " +
            "AND [rowID] = (SELECT MIN(rowID) FROM [TransPeriod] WHERE Posted = 0)", new object[] { });
        tp = null;
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
    
    
}