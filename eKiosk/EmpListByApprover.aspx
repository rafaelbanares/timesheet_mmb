﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="EmpListByApprover.aspx.cs" Inherits="EmpListByApprover" Title="MMB Kronos - Select Employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript" src="css/common_uno.js"></script>

<script type="text/javascript">
function EndRequestHandler(sender, args) {
   if (args.get_error() == undefined)
    {
        var action = $get("<%=hfUpdate.ClientID%>").value;
        if (action)
        {
            frames['fbody'].location.href = action;
            //popupWindow=wopen(action,"popupWindow",screen.width-100,screen.height-50);
        }
    }
   else
       alert('There was an error' + args.get_error().message);
}
function resizeIframe(oiframe) {  
    oiframe.height = document.frames[oiframe.id].document.body.scrollHeight+250;
}
</script>


<table width="100%">
  <tr>
    <td valign="top">
        <div style="width:300px">
        <table width="100%" class="TableBorder1" >
          <tr>
            <td><asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="205px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click" ValidationGroup="emplist" />
            </td>
          </tr>
        </table>

        <asp:UpdatePanel ID="updEmp" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CssClass="DataGridStyle" OnPageIndexChanging="gvEmp_PageIndexChanging"
            OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" PageSize="20" Width="100%" OnDataBound="gvEmp_DataBound">
            <Columns>
                <asp:TemplateField HeaderText="Emp.ID" SortExpression="EmployeeID">
                    <ItemTemplate>
                        <asp:Literal ID="litID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FullName" SortExpression="FullName">
                    <ItemTemplate>
                        <asp:Literal ID="litName" runat="server" Text='<%# Bind("Fullname") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>        
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="ibSelect" runat="server" ImageUrl="~/Graphics/select.gif" OnClick="ibSelect_Click" ValidationGroup="emplist" CommandName="select" ToolTip="Select" />
                    </ItemTemplate>
                </asp:TemplateField>       
            </Columns>
            <EmptyDataTemplate>
                <span style="color:Red">No record found</span>
            </EmptyDataTemplate>
            <PagerStyle HorizontalAlign="Right" />
            <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
        </asp:GridView>
            <asp:HiddenField ID="hfUpdate" runat="server" />
            <asp:HiddenField ID="hfModule" runat="server" />
            <asp:HiddenField ID="hfLevels" runat="server" Value="~~~" />
            <asp:HiddenField ID="hfURL" runat="server" />
        </ContentTemplate>
         <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        </Triggers>
        </asp:UpdatePanel>
        </div>
    </td>
    
    <td valign="top" width="100%">
        <iframe name="fbody" id="fbody" src="<%=hfUpdate.Value%>" width="100%" height="100%" frameborder="0" onload="resizeIframe(this);">
        </iframe>
    </td>

  </tr>
</table>
</asp:Content>