﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ChangeRestday.aspx.cs" Inherits="ChangeRestday" Title="MMB Kronos - Change of Restday" %>
<%@ Register src="EmpHeaderDate.ascx" tagname="EmpHeaderDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>    
    <asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="My Change of RestDay Applications"></asp:Label>
    <br />
    <br />        
    <div style="width:650px; text-align:right">
        Year:&nbsp;<asp:DropDownList ID="ddlYear" runat="server" Width="80px" 
            AutoPostBack="true" onselectedindexchanged="ddlYear_SelectedIndexChanged" ></asp:DropDownList>
    </div>
    <hr />    
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="Add change of RestDay" /><asp:LinkButton ID="lbNew" runat="server"
            CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">Add change of RestDay</asp:LinkButton>
    <br />
    <br />
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" CssClass="DataGridStyle" Width="650px" OnRowUpdating="gvMain_RowUpdating"
                OnRowEditing="gvMain_RowEditing" OnRowDeleting="gvMain_RowDeleting" OnRowDataBound="gvMain_RowDataBound"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" DataKeyNames="ID" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField HeaderText="Start date" SortExpression="StartDate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtStart" runat="server" Text='<%# Bind("StartDate", "{0:MM/dd/yyyy}") %>'
                                CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtStart" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                            </asp:ImageButton>
                            <asp:RequiredFieldValidator ID="rfvStart" runat="server" ErrorMessage="Date start is required"
                                ControlToValidate="txtStart" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                                    ID="meeStart" runat="server" TargetControlID="txtStart" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleStart" runat="server" TargetControlID="txtStart"
                                PopupButtonID="ibtxtStart">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litStart" runat="server" Text='<%# Bind("StartDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End date" SortExpression="EndDate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEnd" runat="server" Text='<%# Bind("EndDate", "{0:MM/dd/yyyy}") %>'
                                CssClass="ControlDefaults" Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtEnd" runat="server" ToolTip="Click to choose date" ImageUrl="~/Graphics/calendar.gif">
                            </asp:ImageButton>
                            <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ErrorMessage="Date end is required"
                                ControlToValidate="txtEnd" ValidationGroup="Trans">*</asp:RequiredFieldValidator><ajaxToolkit:MaskedEditExtender
                                    ID="meeEnd" runat="server" TargetControlID="txtEnd" OnInvalidCssClass="MaskedEditError"
                                    OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" MaskType="Date"
                                    Mask="99/99/9999" ErrorTooltipEnabled="True">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleEnd" runat="server" TargetControlID="txtEnd"
                                PopupButtonID="ibtxtEnd">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litEnd" runat="server" Text='<%# Bind("EndDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RestDay" SortExpression="Code">
                        <EditItemTemplate>
                            <asp:CheckBoxList ID="cblRestDay" runat="server" Width="221px" Height="58px">
                                <asp:ListItem>Sunday</asp:ListItem>
                                <asp:ListItem>Monday</asp:ListItem>
                                <asp:ListItem>Tuesday</asp:ListItem>
                                <asp:ListItem>Wednessday</asp:ListItem>
                                <asp:ListItem>Thursday</asp:ListItem>
                                <asp:ListItem>Friday</asp:ListItem>
                                <asp:ListItem>Saturday</asp:ListItem>
                            </asp:CheckBoxList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="Status" />

                    <asp:TemplateField>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans"
                                CommandName="update" ToolTip="Update"></asp:ImageButton>
                            <asp:ImageButton ID="ibCancel" runat="server" ImageUrl="~/Graphics/cancel.GIF" CommandName="cancel" ToolTip="Cancel">
                            </asp:ImageButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit.gif" CommandName="edit" ToolTip="Edit">
                            </asp:ImageButton>
                            <asp:ImageButton ID="ibDelete" runat="server" ImageUrl="~/Graphics/delete.gif" CommandName="delete" ToolTip="Delete">
                            </asp:ImageButton>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top" Width="42px"></ItemStyle>
                    </asp:TemplateField>
                </Columns>                
                <EmptyDataTemplate>
                    <span style="color:Red">No record found</span>
                </EmptyDataTemplate>
                <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server"></asp:HiddenField>
            <asp:Button ID="btnFU" runat="server" Text="" style="display:none" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="lbNew"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    
</asp:Content>

