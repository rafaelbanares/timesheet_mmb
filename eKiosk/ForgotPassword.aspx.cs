﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using Bns.AttendanceUI;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using BNS.Framework.Encryption;
using System.Configuration;

public partial class ForgotPassword : System.Web.UI.Page
{
    Kiosk settings;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _ConnectionString = ConfigurationManager.AppSettings.Get("Connection");

            object session = (object)Session["kiosk"];
            if (session == null)
                Response.Redirect("LoginEmployee.aspx");

            settings = (Kiosk)session;

            ViewState["kiosk"] = (object)settings;

            txtEmpID.Text = settings.EmployeeID;

            DataTable dtEmp = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, "SELECT Question, Answer FROM " + settings.DB + ".dbo.Employee WHERE CompanyID = @CompanyID AND EmployeeID = @EmployeeID",
                    new SqlParameter[] {
                    new SqlParameter("@CompanyID",  settings.CompanyID),
                    new SqlParameter("@EmployeeID", txtEmpID.Text )
                }).Tables[0];

            lblQuestion.Text = dtEmp.Rows[0]["Question"].ToString(); ;
            hfAnswer.Value = dtEmp.Rows[0]["Answer"].ToString();

            Session["kiosk"] = null;
        }
        else
        {
            settings = (Kiosk) ViewState["kiosk"];
        }

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string _ConnectionString = ConfigurationManager.AppSettings.Get("Connection");
        string masterdb = Helper.Config.MasterDB();

        if (txtNewPwd.Text != txtRetype.Text)
        {
            lblError.Text = "New password did not match your re-typed password";
            return;
        }
        else if (txtAnswer.Text.ToLower() != hfAnswer.Value.ToLower())
        {
            lblError.Text = "Incorrect answer";
            return;
        }

        string oldpassword = "forgotten";
        string newpassword = Crypto.ActionEncrypt(txtNewPwd.Text);

        string password = (string)SqlHelper.ExecuteScalar(_ConnectionString, CommandType.StoredProcedure, masterdb + ".dbo.usa_ChangePassword",
                        new SqlParameter[] {
                            new SqlParameter("@DBName", settings.DB),
                            new SqlParameter("@CompanyID", settings.CompanyID),
                            new SqlParameter("@EmployeeID", settings.EmployeeID),
                            new SqlParameter("@OldPassword", oldpassword),
                            new SqlParameter("@NewPassword", newpassword )
                        });

        Session["kiosk"] = (object) settings;
        //Response.Redirect("eKioskHome.aspx");
        

        //create authentication ticket
        FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, settings.EmployeeID, DateTime.Now, DateTime.Now.AddMinutes(30), true, "");
        HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(tkt));
        ck.Expires = tkt.Expiration;
        ck.Path = FormsAuthentication.FormsCookiePath;
        Response.Cookies.Add(ck);

        if (IsAdminMode())
            Response.Redirect("eTimeHome.aspx", true);
        else
            Response.Redirect("eKioskHome.aspx", true);
    }

    #region IsAdminMode
    private bool IsAdminMode()
    {
        if (Request.QueryString["admin"] == null) return false;
        return (Request.QueryString["admin"].Trim() == "1");
    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("LoginEmployee.aspx", true);
    }
}
