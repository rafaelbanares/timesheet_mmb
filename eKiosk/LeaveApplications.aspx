﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveApplications.aspx.cs" Inherits="LeaveApplications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<div><asp:Label runat="server" Text="My Leave Applications" Font-Size="Large"></asp:Label></div>
<br />    
    <div>
    <table style="width:100%;">
        <tr>
            <td style="width:15px">Leave&nbsp;Type&nbsp;</td>
            <td style="width:210px"><asp:DropDownList ID="ddlLeaveType" runat="server" Width="200px" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlLeaveType_SelectedIndexChanged" >
            </asp:DropDownList></td>
            <td style="width:15px">Status&nbsp;</td>
            <td style="width:120px"><asp:DropDownList ID="ddlStatus" runat="server" Width="105px" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlStatus_SelectedIndexChanged" >
            </asp:DropDownList></td>
            <td style="width:15px">Year&nbsp;</td>
            <td style="width:120px"><asp:DropDownList ID="ddlYear" runat="server" Width="90px" 
                    AutoPostBack="True" onselectedindexchanged="ddlYear_SelectedIndexChanged">
            </asp:DropDownList></td>
            <td>
            </td>
        </tr>
    </table>
    </div>
    <hr />    
    <br />
    <asp:LinkButton ID="lnkApplyLeave" runat="server" onclick="lnkApplyLeave_Click" >Apply for Leave</asp:LinkButton>
    <br />
    <br />
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" DataKeyNames="ID" 
            AllowSorting="True" Width="80%" onrowdatabound="gvEmp_RowDataBound" 
            onpageindexchanging="gvEmp_PageIndexChanging" >
        <Columns>
            <asp:BoundField HeaderText="Leave Type" DataField="LeaveDescr"  ItemStyle-Width="200px" />
            <asp:BoundField HeaderText="Start Date" DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="100px" /> 
            <asp:BoundField HeaderText="End Date" DataField="EndDate" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="100px" />            
            <asp:BoundField HeaderText="Days Applied" DataField="Days" DataFormatString="{0:F2}" ItemStyle-Width="100px" /> 
            <asp:BoundField HeaderText="Reason" DataField="Reason" ItemStyle-Width="250px" />
            <asp:BoundField HeaderText="Status" DataField="Status" ItemStyle-Width="100px" />
            <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit_small.gif" OnClick="ibEdit_Click" ToolTip="Edit" />
                    <asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
                    <asp:HiddenField ID="hfID" runat="server" Value='<%# Bind("ID") %>' />
                </ItemTemplate>
                <ItemStyle Width="50px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>    
</asp:Content>

