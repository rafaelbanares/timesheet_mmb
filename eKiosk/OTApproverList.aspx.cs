﻿using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Business;
using BNS.TK.Entities;
using Microsoft.ApplicationBlocks.Data;
using MMB.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OTApproverList : KioskPageUI
{
    string approverno;
    string stage;

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUserSession();

        if (_Url["approverno"] == string.Empty)
            Response.Redirect("LoginEmployee.aspx");

        approverno = _Url["approverno"];

        if (!IsPostBack)
            BindGrid();
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();

        btnForApproval.Visible = (gvEmp.EditIndex == -1 && CountStatus("For Approval") > 0);
    }
    private DataSet GetData()
    {
        SqlParameter[] spParams;

        if (((HiddenField)EmpHeader1.FindControl("hdDateSelection")).Value == "")
            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverNo", approverno)
                ,new SqlParameter("@Filter", EmpHeader1.Display )
                ,new SqlParameter("@Search", EmpHeader1.Search )
            };
        else
        {
            string sdate = ((HiddenField)EmpHeader1.FindControl("hdDateStart")).Value;
            string edate = ((HiddenField)EmpHeader1.FindControl("hdDateEnd")).Value;

            spParams = new SqlParameter[] {
                new SqlParameter("@DBName", _KioskSession.DB)
                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                ,new SqlParameter("@EmployeeID", _KioskSession.EmployeeID)
                ,new SqlParameter("@ApproverNo", approverno)
                ,new SqlParameter("@StartDate", sdate )
                ,new SqlParameter("@EndDate", edate )
                ,new SqlParameter("@Filter", EmpHeader1.Display )
                ,new SqlParameter("@Search", EmpHeader1.Search )
            };
        }
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTApprover", spParams);
        return ds;
    }

    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                Label lblreason = e.Row.FindControl("lblreason") as Label;
                string decreason = DataBinder.Eval(e.Row.DataItem, "DeclinedReason").ToString().TrimEnd();
                string reason = lblreason.Text;
                if (decreason.TrimEnd() != "")
                    reason += " | " + decreason;

                if (reason.Length > 18)
                {
                    lblreason.ToolTip = reason;
                    lblreason.Text = reason.Substring(0, 15) + "...";
                }

                //rsb
                ImageButton ibRecall = e.Row.FindControl("ibRecall") as ImageButton;
                ImageButton ibApproved = e.Row.FindControl("ibApproved") as ImageButton;
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                ImageButton ibDeny = e.Row.FindControl("ibDeny") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().TrimEnd().ToUpper();
                var isPosted = (bool)DataBinder.Eval(e.Row.DataItem, "Posted");
                ibRecall.Visible = (!isPosted) && (status == "APPROVED" || status == "DECLINED");
                ibApproved.Visible = (status == "FOR APPROVAL");
                ibEdit.Visible = (!isPosted) && (_KioskSession.IsAdminMode);
                ibDeny.Visible = (status == "FOR APPROVAL");

                //check if locked
                string date = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "OTDate").ToString(), _KioskSession.DateFormat);
                if (DateLockedForApprover(Convert.ToDateTime(date)))
                {
                    ibLocked.Visible = true;
                    ibEdit.Visible = false;
                    ibApproved.Visible = false;
                    ibRecall.Visible = false;
                    ibDeny.Visible = false;

                }
            }

            Literal litStage = e.Row.FindControl("litStage") as Literal;
            //litStage.Text = (litStage.Text == "1" ? "For Final Approval" : "For Approval");
            litStage.Text = DataBinder.Eval(e.Row.DataItem, "Status").ToString();

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, false);
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);        
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }

    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hfCompanyID = gvEmp.Rows[e.RowIndex].FindControl("hfCompanyID") as HiddenField;
        Literal litEmployeeID = gvEmp.Rows[e.RowIndex].FindControl("litEmployeeID") as Literal;
        Literal litDate = gvEmp.Rows[e.RowIndex].FindControl("litDate") as Literal;
        string id = gvEmp.DataKeys[e.RowIndex].Values["id"].ToString();
        string status = gvEmp.DataKeys[e.RowIndex].Values["status"].ToString();

        TextBox tbin1 = gvEmp.Rows[e.RowIndex].FindControl("tbin1") as TextBox;
        TextBox tbout1 = gvEmp.Rows[e.RowIndex].FindControl("tbout1") as TextBox;
        TextBox tbApprovedOT = gvEmp.Rows[e.RowIndex].FindControl("tbApprovedOT") as TextBox;
        TextBox tbReason = gvEmp.Rows[e.RowIndex].FindControl("tbReason") as TextBox;

        string in1 = Utils.TimeToDateTimeString(litDate.Text, tbin1.Text, "");
        string out1 = Utils.TimeToDateTimeString(litDate.Text, tbout1.Text, in1);

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@CompanyID", hfCompanyID.Value));
        sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
        sqlparam.Add(new SqlParameter("@ID", id));

        sqlparam.Add(new SqlParameter("@OTDate", litDate.Text));
        sqlparam.Add(new SqlParameter("@OTstart", Tools.DefaultNull(in1)));
        sqlparam.Add(new SqlParameter("@OTend", Tools.DefaultNull(out1)));

        sqlparam.Add(new SqlParameter("@ApprovedOT", tbApprovedOT.Text));
        sqlparam.Add(new SqlParameter("@Reason", tbReason.Text));

        //...stage should not be updated since it is just being edited
        sqlparam.Add(new SqlParameter("@Status", "For Approval"));
        sqlparam.Add(new SqlParameter("@Stage", stage=="2" ? "1" : "0" ) );
        sqlparam.Add(new SqlParameter("@AuthBy", ""));

        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        //... validation not needed since OTDate is not changed
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdate", sqlparam.ToArray());

        gvEmp.EditIndex = -1;
        BindGrid();
    }

    private void ApprovedDeny(object sender, string newStatus, int rowindex, int newStage)
    {
        //var row = gvEmp.Rows[rowindex];

        var id = (int)gvEmp.DataKeys[rowindex].Values["id"];
        var approver = _KioskSession.UID;

        var otAuth = new OTAuthorization();
        if (otAuth.LoadByPrimaryKey(id))
        {
            otAuth.Status = newStatus;
            otAuth.Stage = (short)newStage;
            otAuth.AuthBy = approver;
            otAuth.LastUpdBy = approver;
            otAuth.LastUpdDate = DateTime.Now;
            if (newStatus == TKWorkFlow.Status.Declined)
            {
                otAuth.DeclinedReason = txtDecReason.Text;
            }
            otAuth.Save();
        }

        //Helper.SendMail(Helper.GetEmailAddress(hfCompanyID.Value, litEmployeeID.Text), "Kronos Overtime change of status", "Your DTR was acted upon by your supervisor.");

        var row = gvEmp.Rows[rowindex];
        ReprocessAttendance(((Literal)row.FindControl("litEmployeeID")).Text,
            DateTime.Parse(((Literal)row.FindControl("litDate")).Text));

        gvEmp.EditIndex = -1;
        BindGrid();
    }

    //private void ApprovedDeny(object sender, string newStatus, int rowindex, int newStage)
    //{
    //    HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
    //    Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;
    //    Literal litDate = gvEmp.Rows[rowindex].FindControl("litDate") as Literal;
    //    string id = gvEmp.DataKeys[rowindex].Values["id"].ToString();

    //    Label lblin1 = gvEmp.Rows[rowindex].FindControl("lblin1") as Label;
    //    Label lblout1 = gvEmp.Rows[rowindex].FindControl("lblout1") as Label;
    //    TextBox tbApprovedOT = gvEmp.Rows[rowindex].FindControl("tbApprovedOT") as TextBox;
    //    TextBox tbReason = gvEmp.Rows[rowindex].FindControl("tbReason") as TextBox;

    //    string in1 = Utils.TimeToDateTimeString(litDate.Text, lblin1.Text, "");
    //    string out1 = Utils.TimeToDateTimeString(litDate.Text, lblout1.Text, in1);

    //    //if (status.ToLower() != "declined" && _KioskSession.ApproverRuleOR_OR)
    //    //{
    //    //    status = "Approved";
    //    //    stage = "2";
    //    //}

    //    List<SqlParameter> sqlparam = new List<SqlParameter>();
    //    sqlparam.Add(new SqlParameter("@CompanyID", hfCompanyID.Value));
    //    sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
    //    sqlparam.Add(new SqlParameter("@ID", id));

    //    sqlparam.Add(new SqlParameter("@OTDate", litDate.Text));
    //    sqlparam.Add(new SqlParameter("@OTstart", Tools.DefaultNull(in1)));
    //    sqlparam.Add(new SqlParameter("@OTend", Tools.DefaultNull(out1)));

    //    sqlparam.Add(new SqlParameter("@Status", newStatus));
    //    sqlparam.Add(new SqlParameter("@Stage", newStage));
    //    if (newStatus.ToLower().TrimEnd() == "declined" )
    //        sqlparam.Add(new SqlParameter("@DeclinedReason", txtDecReason.Text));
    //    else
    //        sqlparam.Add(new SqlParameter("@DeclinedReason", ""));

    //    sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));

    //    sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
    //    sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
    //    sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

    //    //... validation not needed since OTDate is not changed
    //    SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdateStatus", sqlparam.ToArray());


    //    //Helper.SendMail(Helper.GetEmailAddress(hfCompanyID.Value, litEmployeeID.Text), "Kronos Overtime change of status", "Your DTR was acted upon by your supervisor.");

    //    gvEmp.EditIndex = -1;
    //    BindGrid();

    //    ReprocessAttendance(litEmployeeID.Text, DateTime.Parse(litDate.Text));
    //}

    private void Recall(object sender, int rowindex)
    {
        var row = gvEmp.Rows[rowindex];
        var employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
        var otDate = ((Literal)row.FindControl("litDate")).Text.ToDate();
        var id = gvEmp.DataKeys[rowindex].Values["id"].ToString().ToInt();
        var otAuth = new OTAuthorization();
        try
        {
            if (!otAuth.LoadByPrimaryKey(id))
                throw new Exception("Overtime not found.");

            if (otAuth.Posted)
                throw new Exception("Cannot recall posted Overtime.");

            otAuth.Status = TKWorkFlow.Status.ForApproval;
            otAuth.Stage = 0;
            otAuth.AuthBy = "0";
            otAuth.DeclinedReason = string.Empty;
            otAuth.LastUpdBy = _KioskSession.UID;
            otAuth.LastUpdDate = DateTime.Now;
            otAuth.Save();
        }
        catch (SqlException sqex)
        {
            lblError.Text = sqex.Message;
        }

        gvEmp.EditIndex = -1;
        BindGrid();
        ReprocessAttendance(employeeId, otDate);

        //HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
        //Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;
        //Literal litDate = gvEmp.Rows[rowindex].FindControl("litDate") as Literal;
        //string id = gvEmp.DataKeys[rowindex].Values["id"].ToString();

        //Label lblin1 = gvEmp.Rows[rowindex].FindControl("lblin1") as Label;
        //Label lblout1 = gvEmp.Rows[rowindex].FindControl("lblout1") as Label;
        //TextBox tbApprovedOT = gvEmp.Rows[rowindex].FindControl("tbApprovedOT") as TextBox;
        //TextBox tbReason = gvEmp.Rows[rowindex].FindControl("tbReason") as TextBox;

        //string in1 = Utils.TimeToDateTimeString(litDate.Text, lblin1.Text, "");
        //string out1 = Utils.TimeToDateTimeString(litDate.Text, lblout1.Text, in1);

    
        //List<SqlParameter> sqlparam = new List<SqlParameter>();
        //sqlparam.Add(new SqlParameter("@CompanyID", hfCompanyID.Value));
        //sqlparam.Add(new SqlParameter("@EmployeeID", litEmployeeID.Text));
        //sqlparam.Add(new SqlParameter("@ID", id));

        //sqlparam.Add(new SqlParameter("@OTDate", litDate.Text));
        //sqlparam.Add(new SqlParameter("@OTstart", Tools.DefaultNull(in1)));
        //sqlparam.Add(new SqlParameter("@OTend", Tools.DefaultNull(out1)));

        ////sqlparam.Add(new SqlParameter("@Status", "For Approval"));
        //sqlparam.Add(new SqlParameter("@Status", "Declined"));
        //sqlparam.Add(new SqlParameter("@Stage", "0"));
        //sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.UID));

        //sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        //sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        //sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));

        ////... validation not needed since OTDate is not changed
        //SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdateStatus", sqlparam.ToArray());

        //gvEmp.EditIndex = -1;
        //BindGrid();

        //ReprocessAttendance(litEmployeeID.Text, DateTime.Parse(litDate.Text));
    }

    private void ReprocessAttendance(string empid, DateTime date, DateTime? date2 = null)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(date, date2 == null ? date : (DateTime)date2, empid);
    }

    //rsb 6/12/2012 - commented because am not sure if this works.
    //
    //private void UpdateBulk(string filterstatus)
    //{
    //    //... count first if there is records to be udpated
    //    if (CountStatus(filterstatus) == 0)
    //        return;

    //    //... if approved by approver 1, approver 2 or approver 3 is still needed.
    //    string updatestatus = (approverno == "1" ? "For Approval" : "Approved");
    //    string ids = "";

    //    if (_KioskSession.ApproverRuleOR_OR)
    //    {
    //        updatestatus = "Approved";
    //        stage = "2";
    //    }

    //    foreach (GridViewRow row in gvEmp.Rows)
    //    {
    //        Literal litStage = row.FindControl("litStage") as Literal;
    //        if (litStage.Text.ToLower() == filterstatus.ToLower())
    //            ids += "," + gvEmp.DataKeys[row.RowIndex].Values["id"].ToString();
    //    }

    //    if (ids.Length > 0)
    //    {
    //        ids = "(" + ids.Substring(1) + ")";

    //        List<SqlParameter> sqlparam = new List<SqlParameter>();
    //        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
    //        sqlparam.Add(new SqlParameter("@IDs", ids));
    //        sqlparam.Add(new SqlParameter("@Status", updatestatus));
    //        sqlparam.Add(new SqlParameter("@Stage", stage));
    //        sqlparam.Add(new SqlParameter("@Approver", _KioskSession.UID));
    //        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
    //        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

    //        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthUpdateBulk", sqlparam.ToArray());
    //    }

    //    gvEmp.EditIndex = -1;
    //    BindGrid();
    //    //ReprocessAttendance();
    //}

    protected void btnForApproval_Click(object sender, EventArgs e)
    {
        var otAuth = new OTAuthorization();
        var userId = _KioskSession.UID;
        var date = DateTime.Now;
        var approverNo = _Url["approverno"].ToInt();
        var startDate = new DateTime();
        var endDate = new DateTime();

        string status, newStatus, employeeId;
        short newStage;
        DateTime tempDate;
        int id;
        bool approvable;

        foreach (GridViewRow row in gvEmp.Rows)
        {
            status = ((Literal)row.FindControl("litStage")).Text;
            approvable = ((ImageButton)row.FindControl("ibApproved")).Visible;

            if (status == TKWorkFlow.Status.ForApproval && approvable)
            {
                tempDate = DateTime.Parse(((Literal)row.FindControl("litDate")).Text);

                startDate = startDate == DateTime.MinValue ? tempDate : startDate > tempDate ? tempDate : startDate;
                endDate = endDate == DateTime.MinValue ? tempDate : endDate < tempDate ? tempDate : endDate;

                newStatus = TKWorkFlow.Status.Approved;
                newStage = 2;

                if (approverNo == 1 || approverNo == 2)
                {
                    employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
                    newStage = 1;

                    if (!IsFinalApprover(employeeId))
                        newStatus = TKWorkFlow.Status.ForApproval;
                }

                id = gvEmp.DataKeys[row.RowIndex].Values["id"].ToString().ToInt();

                if (otAuth.LoadByPrimaryKey(id))
                {
                    otAuth.Status = newStatus;
                    otAuth.Stage = newStage;
                    otAuth.AuthBy = userId;
                    otAuth.LastUpdBy = userId;
                    otAuth.LastUpdDate = date;
                    otAuth.Save();
                }
            }
        }

        if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
        {
            gvEmp.EditIndex = -1;
            BindGrid();

            ReprocessAttendance(string.Empty, startDate, endDate);
        }
    }

    private int CountStatus(string filterstatus)
    {
        int retval = 0;
        foreach (GridViewRow row in gvEmp.Rows)
        {
            Literal litStage = row.FindControl("litStage") as Literal;
            if (litStage.Text.ToLower() == filterstatus.ToLower())
                retval++;
        }
        return retval;
    }

    private bool IsFinalApprover(string empid)
    {
        EmployeeApprover appr = new EmployeeApprover();
        string sql = "SELECT Approver3, Approver4 FROM EmployeeApprover WHERE CompanyID = {0} AND EmployeeID = {1} ";

        object[] parameters = { _KioskSession.CompanyID, empid.Trim() };
        appr.DynamicQuery(sql, parameters);
        if (appr.RowCount > 0)
        {
            return (appr.s_Approver3.TrimEnd() == "" && appr.s_Approver4.TrimEnd() == "");
        }
        return false;
    }

    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        var newStatus = TKWorkFlow.Status.Approved;
        var newStage = 2;

        if (approverno == "1" || approverno == "2")
        {
            var employeeId = ((Literal)gvEmp.SelectedRow.FindControl("litEmployeeId")).Text;

            newStage = 1;
            if (!IsFinalApprover(employeeId))
                newStatus = TKWorkFlow.Status.ForApproval;
        }

        ApprovedDeny(sender, newStatus, gvEmp.SelectedIndex, newStage);
    }
    
    private void ReprocessAttendance(string empid, DateTime date)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(date, date, empid);

    }
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        txtDecReason.Text = "";
        hfdecRowid.Value = e.RowIndex.ToString();
        mpe3.Show();
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        gvEmp.EditIndex = -1;
        EmpHeader1.UpdateURL();
        BindGrid();
    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper().TrimEnd() == "RECALL")
        {
            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            int rowIndex = row.RowIndex;
            Recall(sender, rowIndex);
        }
    }

    private bool DateLockedForApprover(DateTime date)
    {
        var tp = GetTransPeriod(date);
        tp.Filter = string.Format("StartDate <= '{0}' AND EndDate >= '{0}'", date.ToShortDateString());

        if (tp.RowCount > 0)
        {
            if (tp.s_Approver_lock_start != string.Empty && tp.s_Approver_lock_end != string.Empty)
            {
                var today = DateTime.Today;
                return (today >= tp.Approver_lock_start && today <= tp.Approver_lock_end);
            }
        }

        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }

    protected void btnDecline_Click(object sender, EventArgs e)
    {
        int rowindex;
        if (int.TryParse(hfdecRowid.Value, out rowindex))
            ApprovedDeny(sender, "Declined", rowindex, 0);
    }
}
