<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="PayslipView.aspx.cs" Inherits="PayslipView" Title="MMB Kronos - Payslip" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;     
    $get('<%= hfURL.ClientID %>').value = url;
    
    closewindow(popupWindow);
}

function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue)
{
    $get(openerEmpClientID).value = openerEmpValue;
    $get(openerNameClientID).value = openerNameValue;
    closewindow(popupWindow);
}

</script>

<table style="width:920px" border="1">
<tr>
    <td valign="top">    
    <br />
    <table cellpadding="0" class="TableBorder1" style="width: 450px">
        <tr>
            <td class="DataGridHeaderStyle" colspan="3" style="height: 12px">
                Report Options</td>
        </tr>
        <tr>
            <td align="right" >Pay Date:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlDate" runat="server" CssClass="ControlDefaults" 
                    DataTextField="PayDate" DataValueField="PayrollNo" ValidationGroup="Trans">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
  </td>
  <td valign="top">
  <br />
    <asp:GridView ID="grvQuarterly" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
        OnRowDataBound="grvQuarterly_RowDataBound">
        <Columns>
            <asp:BoundField DataField="ReportID" Visible="False" />
            <asp:TemplateField HeaderText="Select Reports">
                <HeaderStyle Width="300px" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnkReport" runat="server" CssClass="Report" OnClick="lnkReport_Click"
                        Text='<%# Bind("Reportname") %>'></asp:LinkButton>
                    <input id="hdnID" runat="server" type="hidden" value='<%# Eval("ReportID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
        <EmptyDataTemplate>
            &nbsp;
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
  </td>
</tr>

</table>
    <asp:HiddenField ID="hfLevels" runat="server" Value="~~~" />
    <asp:HiddenField ID="hfURL" runat="server" />

</asp:Content>

