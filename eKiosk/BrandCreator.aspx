<%@ Page Title="" Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="BrandCreator.aspx.cs" Inherits="BrandCreator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Project Creators"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Project Creator" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Project Creator</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" 
                OnRowUpdating="gvMain_RowUpdating" DataKeyNames="rowID" Width="793px">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>
                    <asp:TemplateField HeaderText="FlexHeader-WorkFor">
                        <ItemStyle Width="230px" />
                        <ItemTemplate>
                            <asp:Label ID="lblWorkFor" runat="server" Text='<%# Bind("WorkForShortDesc") %>' ></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlWorkFor" runat="server" CssClass="ControlDefaults" Width="220px" 
                                DataTextField="ShortDesc" DataValueField="WorkForID" 
                                ValidationGroup="Trans" AutoPostBack="true" 
                                onselectedindexchanged="ddlWorkFor_SelectedIndexChanged" ></asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="FlexHeader-WorkForSub">
                        <ItemStyle Width="230px" />
                        <ItemTemplate>
                            <asp:Label ID="lblWorkForSub" runat="server" Text='<%# Bind("WorkForSubShortDesc") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlWorkForSub" runat="server" CssClass="ControlDefaults" Width="220px" 
                                DataTextField="ShortDesc" DataValueField="WorkForSubID" ValidationGroup="Trans"></asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Project Creator">
                        <ItemStyle Width="330px" />
                        <ItemTemplate>
                            <asp:Label ID="lblEmployeeID" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEmployeeID" runat="server" CssClass="ControlDefaults" Width="320px" 
                                DataTextField="FullName" DataValueField="EmployeeID" ValidationGroup="Trans"></asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                                        
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                            <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                        </EditItemTemplate>
                        <HeaderTemplate>
                            Action
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" 
                                    OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );"/>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

