<%@ Page Language="C#" MasterPageFile="~/AttendanceLogin.master" AutoEventWireup="true" CodeFile="Timer.aspx.cs" Inherits="Timer" Title="eKiosk Login Logout System" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript">
    function startTime()
    {
    var d=new Date();
    // add a zero in front of numbers<10
    var mm=addZero(d.getMonth());
    var dd=addZero(d.getDay());
    var yy=addZero(d.getFullYear());
    var h= addZero(d.getHours());
    var m= addZero(d.getMinutes());
    var s= addZero(d.getSeconds());

    document.getElementById('lbDate').innerHTML = mm+"/"+dd+"/"+yy;
    document.getElementById('lbTime').innerHTML = h+":"+m+":"+s;
    t=setTimeout('startTime()',1000);
    }

    function addZero(i)
    {
    if (i<10)
      {
      i="0" + i;
      }
    return i;
    }

    document.onkeypress = keyIO;
    function keyIO(e)
    {
        e=e||event;
        var keynum;
        var keychar;

        if(window.event) // IE
            keynum = e.keyCode;
        else if(e.which) // Netscape/Firefox/Opera
            keynum = e.which;

        document.getElementById("tbKeyCode").value = keynum;
        if (keynum == 73 || keynum == 105 || keynum == 79 || keynum == 111)
        {        
            assignIO();
        }
        return false;    
    }

    function assignIO()
    {
        var keynum  = document.getElementById("tbKeyCode").value;    
        document.onkeypress = "";
        document.getElementById('dio').style.display = "none";
        document.getElementById('dlogin').style.display = "";
        
        document.getElementById('tbUID').value = "";
        document.getElementById('tbPWD').value = "";
        
        if (keynum == 73 || keynum == 105)
            document.getElementById('bOK').value = "Time IN";
        if (keynum == 79 || keynum == 111)
            document.getElementById('bOK').value = "Time OUT";
        
        document.getElementById('tbUID').focus();
    }
</script>
    <table width="100%">
        <tr>
            <td align="center">
                <br />
                <br />
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbDate" runat="server" Font-Size="XX-Large"></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbTime" runat="server" Font-Size="XX-Large"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <asp:Label ID="lblMessage" runat="server" Font-Size="Large" Text="Login successful" Visible="False"></asp:Label><br />
                <br />
                <br />
                <br />
                
                <div id="dio">
                    <asp:Label ID="lblIO" runat="server" Text="Press (I) for Time In or (O) for Time Out"></asp:Label>
                    <asp:TextBox ID="tbKeyCode" runat="server" style="display:none"></asp:TextBox></div>                
                <div id="dlogin" style="display:none">
                    <table>
                    <tr>
                        <td>
                            User ID:
                        </td>
                        <td>
                            <asp:TextBox ID="tbUID" runat="server" MaxLength="15" Width="108px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rvUID" runat="server" ErrorMessage="User ID is required" ControlToValidate="tbUID" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="tbPWD" runat="server" MaxLength="60" Width="108px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rvPWD" runat="server" ErrorMessage="Password is required" ControlToValidate="tbPWD" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="bOK" runat="server" Text="TimeIO" OnClick="bOK_Click" />&nbsp;
                            <asp:Button ID="bCancel" runat="server" Text="Cancel" ValidationGroup="None" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="vs" runat="server" />
                        </td>
                    </tr>
                </table>
                </div>
                <asp:Literal ID="litScript" runat="server"></asp:Literal></td>            
        </tr>
    </table>
</asp:Content>

