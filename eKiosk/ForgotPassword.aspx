﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AttendanceLogin.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<table border="1" style="background-color:ghostwhite; border-width:1; border-color:dodgerblue">
 <tr>
  <td>
    <strong>Secret Question:</strong>
    <hr />

    <table border="0" style="width:100%" cellpadding="1" cellspacing="0" >
        <tr>
            <td colspan="2">                
                <asp:Label ID="lblQuestion" runat="server" Text="This is a sample question, some question might be longer that this question?"></asp:Label>
                <hr />
                
            </td>        
        </tr>
        <tr>
            <td align="right">Employee ID:</td>
            <td><asp:TextBox ID="txtEmpID" runat="server" Width="150px" MaxLength="15" Enabled="false" autocomplete="off"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">Answer:</td>
            <td><asp:TextBox ID="txtAnswer" runat="server" Width="150px" MaxLength="50" autocomplete="off"></asp:TextBox></td>
        </tr>
       <tr>
            <td align="right"><asp:Label ID="lblNewPwd" runat="server" Text="New Password"></asp:Label></td>
            <td><asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" MaxLength="12" Width="150px" ></asp:TextBox></td>   
       </tr>
       <tr>
            <td align="right"><asp:Label ID="lblRetype" runat="server" Text="Re-type New Password"></asp:Label></td>
            <td><asp:TextBox ID="txtRetype" runat="server" TextMode="Password" MaxLength="12" Width="150px" ></asp:TextBox></td>   
       </tr>
       <tr>
            <td colspan="2">
            <hr />
                <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" ValidationGroup="trans" /> &nbsp;&nbsp;
                <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" /></td>
       </tr>
    </table>
    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br />

    <asp:RegularExpressionValidator
        ID="rvNew" runat="server" 
        Display="None"
        ErrorMessage="Password should be 6-12 characters" 
        ValidationExpression="^.{6,12}$" ControlToValidate="txtNewPwd" 
        ValidationGroup="trans"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator
        ID="RequiredFieldValidator1" runat="server"
        Display="None"
        ErrorMessage="Answer cannot be blank"
        ControlToValidate="txtAnswer" 
        ValidationGroup="trans"></asp:RequiredFieldValidator>        
    <asp:RequiredFieldValidator
        ID="rfv" runat="server"
        Display="None"
        ErrorMessage="New password cannot be blank"
        ControlToValidate="txtNewPwd" 
        ValidationGroup="trans"></asp:RequiredFieldValidator>        
    
    <asp:ValidationSummary ID="vsTrans2" runat="server" ShowMessageBox="True" 
        ShowSummary="False" ValidationGroup="trans" />
  </td>
 </tr>
</table>    
    
    <asp:HiddenField ID="hfAnswer" runat="server" />
    
</asp:Content>

