<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AdminLeaveBalance.aspx.cs" Inherits="AdminLeaveBalance" Title="MMB Kronos - Leave Balances" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
    function openwindow(url) {
        popupWindow = wopen($get('<%= hfURL.ClientID %>').value, "popupWindow", 480, 300);
    }
    function updatelevels(ids, levels, url) {
        $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;

        closewindow(popupWindow);
    }    
</script>   

    <table width="100%">
        <tr>
            <td valign="top">
                <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                    Font-Underline="False" Text="Leave Balance Admin"></asp:Label><br />
                <br />
                <table>
                  <tr>
                    <td align="right">Select Leave :</td>
                    <td><asp:DropDownList ID="ddlCode" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnSelectedIndexChanged="ddlCode_SelectedIndexChanged"></asp:DropDownList></td>
                  </tr>
                  <tr>
                    <td align="right">
                        <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist"></asp:TextBox>&nbsp;<asp:ImageButton
                            ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" ValidationGroup="emplist" OnClick="ibSearch_Click" />
                    </td>
                  </tr>
                    <tr>
                        <td align="right">Year :</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="ControlDefaults" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>               
            <td align="right">
            <table>
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lnkClear" runat="server" ToolTip="Clear Selection" 
                            onclick="lnkClear_Click">clear</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                            Width="279px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
    
    <asp:UpdatePanel id="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>            
            <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle" OnSelectedIndexChanged="gvEmp_SelectedIndexChanged" DataKeyNames="EmployeeID,Code" OnSorting="gvEmp_Sorting" AllowPaging="True" AllowSorting="True" PageSize="20" OnPageIndexChanging="gvEmp_PageIndexChanging" Width="800px">
                <Columns>
                    <asp:BoundField DataField="EmployeeID" HeaderText="Employee ID" SortExpression="EmployeeID" />
                    <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="FullName" />
                    <asp:BoundField DataField="Code" HeaderText="Leave" SortExpression="Code" />
                    <asp:BoundField DataField="Earned" HeaderText="Earned" />
                    <asp:BoundField DataField="Taken" HeaderText="Taken" />
                    <asp:BoundField DataField="Balance" HeaderText="Balance" />
                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle VerticalAlign="Top" />
                        <ItemTemplate>
                            <asp:ImageButton ID="ibSelect" runat="server" ImageUrl="~/Graphics/select.gif" CommandName="select" ToolTip="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
            </asp:GridView>
            
            <asp:HiddenField ID="hfLevels" runat="server" Value="~~~" />
            <asp:HiddenField ID="hfURL" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ibSearch"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ddlCode"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>

    <br />
    
    <asp:UpdatePanel id="up2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>            
        <strong>Leave Transactions</strong><br />
        
        <asp:GridView ID="gvLTrans" runat="server" AutoGenerateColumns="False" 
            CssClass="DataGridStyle" Width="950px" AllowSorting="True" 
            onsorting="gvLTrans_Sorting" DataKeyNames="ID" 
            onrowdatabound="gvLTrans_RowDataBound" 
            onrowdeleting="gvLTrans_RowDeleting">
            <Columns>
                <asp:BoundField DataField="Code" HeaderText="Leave" SortExpression="Code" />
                <asp:BoundField DataField="DateFiled" HeaderText="Date filed" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="DateFiled" />
                <asp:BoundField DataField="StartDate" HeaderText="Start date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="StartDate"  />
                <asp:BoundField DataField="EndDate" HeaderText="End date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="EndDate" />

                <asp:BoundField DataField="Earned" HeaderText="Leave Earned" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Days" HeaderText="Leave Taken"  >

                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>

                <asp:BoundField DataField="Reason" HeaderText="Reason" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />

                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibDelete" runat="server" ImageUrl="~/Graphics/delete.gif" CommandName="delete" ToolTip="Delete" 
                            OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );"></asp:ImageButton>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="42px"></ItemStyle>
                </asp:TemplateField>

            </Columns>
            <HeaderStyle CssClass="DataGridHeaderStyle" />
        </asp:GridView>

            &nbsp;
        </ContentTemplate>
    </asp:UpdatePanel>    
</asp:Content>

