<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TimeMachine.aspx.cs" Inherits="TimeMachine" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>eKiosk Login/Logout System</title>

<script type="text/javascript">
function startTime()
{
    var d=new Date();
    // add a zero in front of numbers<10
    var mm = getMonthString(d); //addZero(d.getMonth()+1);
    var dd=addZero(d.getDate());
    var yy=addZero(d.getFullYear());
    var h= addZero(d.getHours());
    var m= addZero(d.getMinutes());
    var s = addZero(d.getSeconds());

    var hours = h == 0 ? "12" : h > 12 ? h - 12 : h;
    var ampm = h < 12 ? "AM" : "PM";

    document.getElementById('lbDate').innerHTML = mm + " " + dd + ", " + yy;  //mm+"/"+dd+"/"+yy;
    document.getElementById('lbTime').innerHTML = hours + ":" + m + ":" + s + " " + ampm;
    t=setTimeout('startTime()',1000);
}

function getMonthString(d) {    
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    return month[d.getMonth()]; 
}

function addZero(i)
{
  if (i<10)
  {
     i="0" + i;
  }
  return i;
}

document.onkeypress = keyIO;
function keyIO(e)
{
    e=e||event;
    var keynum;
    var keychar;

    if(window.event) // IE
        keynum = e.keyCode;
    else if(e.which) // Netscape/Firefox/Opera
        keynum = e.which;

    document.getElementById("tbKeyCode").value = keynum;
    if (keynum == 73 || keynum == 105 || keynum == 79 || keynum == 111)
    {
        assignIO();
    }
    return false;    
}

function assignIO()
{
    var keynum  = document.getElementById("tbKeyCode").value;    
    document.onkeypress = "";
    document.getElementById('dio').style.display = "none";
    document.getElementById('dlogin').style.display = "";
    
    document.getElementById('tbUID').value = "";
    document.getElementById('tbPWD').value = "";
    
    if (keynum == 73 || keynum == 105)
        document.getElementById('bOK').value = "Time IN";
    if (keynum == 79 || keynum == 111)
        document.getElementById('bOK').value = "Time OUT";
    
    document.getElementById('tbUID').focus();
}

</script>

</head>
<body id="bmain" onload="startTime()">
    <form id="form1" runat="server" >
    <div style="width:100%; text-align: center; vertical-align: middle;">    
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="1" 
            style="background-color:#CCCCFF; border-width:1; border-color:dodgerblue; width: 500px; " 
            align="center">
        <tr>
            <td align="center">
                <br />
                <br />
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbDate" runat="server" Font-Size="XX-Large"></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbTime" runat="server" Font-Size="XX-Large"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <asp:Label ID="lblMessage" runat="server" Font-Size="Large"></asp:Label><br />
                <asp:Label ID="lblEntries1" runat="server" Font-Size="Medium"></asp:Label><br />
                <asp:Label ID="lblEntries2" runat="server" Font-Size="Medium"></asp:Label><br />
                <asp:Label ID="lblEntries3" runat="server" Font-Size="Medium"></asp:Label><br />
                <asp:Label ID="lblEntries4" runat="server" Font-Size="Medium"></asp:Label><br />
                <asp:Label ID="lblEntries5" runat="server" Font-Size="Medium"></asp:Label><br />
                <br />
                
                <div id="dio">
                    <asp:Label ID="lblIO" runat="server" Text="Press (I) for Time In or (O) for Time Out"></asp:Label>
                    <br />
                    <a href="LoginEmployee.aspx" style="font-family: Arial, Helvetica, sans-serif; font-size: small">Kronos Login</a>
                    <asp:TextBox ID="tbKeyCode" runat="server" style="display:none"></asp:TextBox></div>                
                <div id="dlogin" style="display:none">                
                    <table>
                    <tr>
                        <td>
                            User ID:
                        </td>
                        <td>
                            <asp:TextBox ID="tbUID" runat="server" MaxLength="15" Width="108px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rvUID" runat="server" ErrorMessage="User ID is required" ControlToValidate="tbUID" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="tbPWD" runat="server" MaxLength="60" Width="108px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rvPWD" runat="server" ErrorMessage="Password is required" ControlToValidate="tbPWD" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="bOK" runat="server" Text="TimeIO" OnClick="bOK_Click" />&nbsp;
                            <asp:Button ID="bCancel" runat="server" Text="Cancel" ValidationGroup="None" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="vs" runat="server" />
                        </td>
                    </tr>
                </table>
                </div>
                <asp:Literal ID="litScript" runat="server"></asp:Literal></td>            
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
