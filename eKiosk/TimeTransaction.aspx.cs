using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;
using BNS.TK.Business;

public partial class TimeTransaction : KioskPageUI
{
    protected readonly string sBackColor_HoliRestday = "#c0c0c0";
    protected readonly string sBackColor_Erroneous = "#819FF7";
    protected readonly string sBackColor_LeaveWhole = "#90ee90";
    protected readonly string sBackColor_Absent = "#F3F781";
    protected readonly string sBackColor_ForApproval = "#f4a460";
    protected readonly string sBackColor_Declined = "#FF4500";

    #region OnInit
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
        dfd1.BubbleClick += new EventHandler(dfd1_BubbleClick);
    }
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {

        base.GetUserSession();
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (!IsPostBack)
        {
            SetDefaultDate();
            BindGrid();
        }
    }

    private void SetDefaultDate()
    {
        dfd1.PerdioFrom = _KioskSession.StartDate.ToString("MM/dd/yyyy");
        dfd1.PerdioTo = _KioskSession.EndDate.ToString("MM/dd/yyyy");
    }

    private void BindGrid()
    {
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.DataBind();
    }

    private DataSet GetData()
    {
        //CONVERT(VARCHAR, CAST(b.in1 AS TIME), 108)

        Attendance att = new Attendance();
        string sql = "SELECT TOP 200 " +
                        "	a.Date, " +
                        "	b.shortname as shiftname, " +
                        "	CONVERT(VARCHAR, CAST(b.in1 AS TIME), 108) as sin1,  " +
                        "   c.otstart, " +
                        "	CONVERT(VARCHAR, CAST(b.out2 AS TIME), 108) as sout2,  " +
                        "   c.otend, " +
                        "   DATEDIFF(hh, a.in1, a.out1) as totalreghrs, " +
                        "   (a.late + a.ut) as lateut, " +
                        "   c.approvedot, " +
                        "   a.regnd1, " +
                        "	CONVERT(VARCHAR, CAST(a.in1 AS TIME), 108) as in1, " +
                        "	CONVERT(VARCHAR, CAST(a.out1 AS TIME), 108) as out1, " +
                        "	a.in2, " +
                        "	a.out2, " +
                        "	a.Late, " +
                        "	a.UT, " +
                        "	a.Absent, " +
                        "	a.Leave, " +
                        "	a.Excesshrs, " +
                        "	a.Remarks, " +
                        "	a.Supervisor, " +
                        "	a.OTcode, " +
                        "	a.Leavecode, " +
                        "	a.restdaycode, " +
                        "	[Status]=case when a.Status='for approval' and a.Stage > 0 then 'For Final Approval' else a.Status end, " +
                        "	a.Stage, " +
                        "   IIF(d.ID IS NULL, 'No', 'Yes') AS Verified, " +
                        "   changed = cast(case when (coalesce(a.in1,'')<>coalesce(a.orig_in1,'')) or (coalesce(a.out1,'')<>coalesce(a.orig_out1,'')) then 1 else 0 end as bit) " +
                        //"   (select x.timeio from TimeTrans x where convert(date, x.TimeIO) = a.Date and x.io = 'I' and x.EmployeeID = a.EmployeeID and x.CompanyID = a.CompanyID) as raw1, " +
                        //"   (select x.timeio from TimeTrans x where convert(date, x.TimeIO) = a.Date and x.io = 'O' and x.EmployeeID = a.EmployeeID and x.CompanyID = a.CompanyID) as raw2 " +
                        "FROM Attendance a " +
                        "LEFT OUTER JOIN Shift b ON a.CompanyID = b.CompanyID AND a.shiftcode = b.shiftcode " +
                        "LEFT OUTER JOIN OTAuthorization c ON a.CompanyID = c.CompanyID AND a.EmployeeID = c.EmployeeID AND a.Date = c.OTDate AND c.Status = 'Approved' " +
                        "LEFT OUTER JOIN VerifiedTrans d ON a.CompanyID = d.CompanyID AND a.EmployeeID = d.EmployeeID AND a.Date BETWEEN d.StartDate AND d.EndDate " +
                        "WHERE " +
                        "	a.CompanyID = {0} AND " +
                        "	a.EmployeeID = {1} AND " +
                        "	a.Date BETWEEN {2} and {3} " +
                        "ORDER BY a.Date ";


        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID, dfd1.PerdioFrom.ToDate(), dfd1.PerdioTo.ToDate() };
        att.DynamicQuery(sql, parameters);

        return att.ToDataSet();
    }


    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }

    protected void gvEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvEmp.DataSource = SortDataTable(gvEmp.DataSource as DataTable, true);
        gvEmployees.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmployees.PageIndex = e.NewPageIndex;
        gvEmployees.DataBind();
    }

    protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmployees.EditIndex)
            {

                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString();
                int stage = DataBinder.Eval(e.Row.DataItem, "Stage").ToString().ToInt();

                string date = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "Date").ToString(), _KioskSession.DateFormat);
                string url = string.Format("WorkDistrib.aspx?Date={0}&EmpID={1}", date, _KioskSession.EmployeeID);
                SecureUrl su = new SecureUrl(url);

                string otcode = DataBinder.Eval(e.Row.DataItem, "OTCode").ToString().Trim();
                decimal leavedays = (decimal)DataBinder.Eval(e.Row.DataItem, "Leave");
                decimal absent = (decimal)DataBinder.Eval(e.Row.DataItem, "Absent");
                string leavecode = DataBinder.Eval(e.Row.DataItem, "LeaveCode").ToString().Trim();
                //Label lblStage = e.Row.FindControl("lblStage") as Label;
                Label lblRemarks = e.Row.FindControl("lblRemarks") as Label;
                Label lblRstDay = e.Row.FindControl("lblRstDay") as Label;
                bool changed = bool.Parse(DataBinder.Eval(e.Row.DataItem, "Changed").ToString().SafeTrim());

                #region assign background color            
                if (lblRemarks.Text.ToLower() == "invalid")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Erroneous);

                else if (leavedays >= 1)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_LeaveWhole);

                else if (status.ToLower() == "for approval" || status.ToLower() == "for final approval")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_ForApproval);

                else if (status.ToLower() == "declined")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Declined);

                else if (otcode != "REG")
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_HoliRestday);

                else if (absent != 0)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(sBackColor_Absent);
                #endregion

                //lblRstDay.Text = GetWeekDays(DataBinder.Eval(e.Row.DataItem, "restdaycode").ToString());
                //lblStage.Text = lblStage.Text == "0" ? "No" : "Yes";
                lblRstDay.Text = date.ToDate().DayOfWeek.ToString().Substring(0, 3);
                ibEdit.Visible = status.IsNullOrEmpty() || (status.IsEqual(BNS.TK.Business.TKWorkFlow.Status.ForApproval) && stage == 0);

                //check if locked
                if (DateLockedForEmployee(Convert.ToDateTime(date)))
                {
                    ibLocked.Visible = true;
                    ibEdit.Visible = false;
                }
            }
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    private string GetWeekDays(string s)
    {
        string[] wds = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
        string ret = "";
        foreach (char c in s.Trim())
        {
            ret += wds[c.ToString().ToInteger() - 1] + ",";
        }
        return ret.RemoveEnd(",");
    }

    private bool DateLockedForEmployee(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Employee_lock_start != "" && tp.s_Employee_lock_end != "")
            {
                return (DateTime.Today >= tp.Employee_lock_start && DateTime.Today <= tp.Employee_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }

    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    private bool validatePeriod()
    {
        DateRange drange = new DateRange();
        if (!drange.ValidatePeriod(dfd1.PerdioFrom, dfd1.PerdioTo))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertmsg", "alert('" + drange.message + "');", true);
            return false;
        }
        return true;
    }

    private void dfd1_BubbleClick(object sender, EventArgs e)
    {
        if (validatePeriod())
            BindGrid();
    }
    protected void lnkPrev_Click(object sender, EventArgs e)
    {
        DateTime dt;
        if (dfd1.PerdioFrom != "" && DateTime.TryParse(dfd1.PerdioFrom, out dt))
        {
            dfd1.PerdioTo = DateTime.Parse(dfd1.PerdioFrom).AddDays(-1).ToString("MM/dd/yyyy");
            dfd1.PerdioFrom = DateTime.Parse(dfd1.PerdioTo).AddDays(-30).ToString("MM/dd/yyyy");
            BindGrid();
        }
        else
        {
            SetDefaultDate();
            BindGrid();
        }
    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        DateTime dt;
        if (dfd1.PerdioTo != "" && DateTime.TryParse(dfd1.PerdioTo, out dt))
        {
            dfd1.PerdioFrom = DateTime.Parse(dfd1.PerdioTo).AddDays(1).ToString("MM/dd/yyyy");
            dfd1.PerdioTo = DateTime.Parse(dfd1.PerdioTo).AddDays(30).ToString("MM/dd/yyyy");
            BindGrid();
        }
        else
        {
            SetDefaultDate();
            BindGrid();
        }
    }

    #region Reference:HowToGetDataKeyOfGridview
    //private void ibtnHowToGetDataKeyOfGridview_click(object sender, ImageClickEventArgs e)
    //{
    //    ImageButton ibtn = (ImageButton)sender;
    //    GridViewRow row = (GridViewRow)ibtn.NamingContainer;
    //    int key = gvEmployees.DataKeys[row.RowIndex].Value.ToString().ToInt();
    //}
    #endregion

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ibtn = (ImageButton)sender;
        DateTime date = (ibtn.Parent.Parent.FindControl("hfDate") as HiddenField).Value.ToDate();
        lblPopErrorMsg.Text = "";

        Attendance att = new Attendance();
        if (att.LoadByPrimaryKey(_KioskSession.CompanyID, _KioskSession.EmployeeID, date))
        {
            hdDatePop.Value = date.ToShortDateString();
            txtOrig_In.Text = att.s_Orig_in1.IsNullOrEmpty() ? "" : att.Orig_in1.ToString("MM/dd/yyyy hh:mm tt");
            txtOrig_Out.Text = att.s_Orig_out1.IsNullOrEmpty() ? "" : att.Orig_out1.ToString("MM/dd/yyyy hh:mm tt");

            txtNewDateIn.Text = (att.s_In1.IsNullOrEmpty()) ? date.ToString("MM/dd/yyyy") : String.Format("{0:MM/dd/yyyy}", att.In1);
            txtNewDateOut.Text = (att.s_Out1.IsNullOrEmpty()) ? date.ToString("MM/dd/yyyy") : String.Format("{0:MM/dd/yyyy}", att.Out1);
            txtNewTimeIn.Text = (att.s_In1.IsNullOrEmpty()) ? "" : String.Format("{0:hh:mm tt}", att.In1);
            txtNewTimeOut.Text = (att.s_Out1.IsNullOrEmpty()) ? "" : String.Format("{0:hh:mm tt}", att.Out1);
            txtNewRemarks.Text = att.s_Reason;
            txtStatus.Text = att.s_Status;

            mpe.Show();
        }
    }

    private bool ValidateEditInputs()
    {
        DateRange dr = new DateRange();
        if (!dr.ValidatePeriod(txtNewDateIn.Text + " " + txtNewTimeIn.Text, txtNewDateOut.Text + " " + txtNewTimeOut.Text))
        {
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('" + dr.message + "');", true);
            lblPopErrorMsg.Text = dr.message;
            return false;
        }
        if (!(txtNewTimeIn.Text.IsNullOrEmpty() && txtNewTimeOut.Text.IsNullOrEmpty()))
        {
            if (txtNewTimeIn.Text.IsNullOrEmpty())
            {
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter new Time In');", true);
                lblPopErrorMsg.Text = "Please enter requested Time In.";
                return false;
            }
            if (txtNewTimeOut.Text.IsNullOrEmpty())
            {
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter new Time Out');", true);
                lblPopErrorMsg.Text = "Please enter requested Time Out";
                return false;
            }
        }


        if (txtNewRemarks.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter reason.');", true);
            return false;
        }

        DateTime origIn = (txtOrig_In.Text.TrimEnd() == "") ? DateTime.MinValue : txtOrig_In.Text.ToDate();
        DateTime origOut = (txtOrig_Out.Text.TrimEnd() == "") ? DateTime.MinValue : txtOrig_Out.Text.ToDate();
        DateTime newIn = (txtNewTimeIn.Text.TrimEnd() == "") ? DateTime.MinValue : (txtNewDateIn.Text + " " + txtNewTimeIn.Text).ToDate();
        DateTime newOut = (txtNewTimeOut.Text.TrimEnd() == "") ? DateTime.MinValue : (txtNewDateOut.Text + " " + txtNewTimeOut.Text).ToDate();
        if (origIn == newIn && origOut == newOut)
        {
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter requested In/Out.');", true);
            lblPopErrorMsg.Text = "Please enter requested In/Out.";
            return false;
        }

        return true;
    }

    private void ReprocessAttendance(DateTime date)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(date, date, _KioskSession.EmployeeID);

        BindGrid();
    }

    protected void btnEditSave_Click(object sender, EventArgs e)
    {
        if (!ValidateEditInputs())
        {
            mpe.Show();
            return;
        }

        mpe.Hide();
        try
        {
            string empid = _KioskSession.EmployeeID;
            string date = hdDatePop.Value;

            Attendance att = new Attendance();
            if (att.LoadByPrimaryKey(_KioskSession.CompanyID, empid, DateTime.Parse(date)))
            {
                DateTime newIn = DateTime.Parse(txtNewDateIn.Text + " " + txtNewTimeIn.Text);
                DateTime newOut = DateTime.Parse(txtNewDateOut.Text + " " + txtNewTimeOut.Text);

                //set for approval only if in/out was changed
                bool sentForApproval = false;
                if (newIn != att.In1 || newOut != att.Out1)
                {
                    att.Status = BNS.TK.Business.TKWorkFlow.Status.ForApproval;
                    att.Stage = 0;
                    sentForApproval = true;
                }

                if (txtNewTimeIn.Text.IsNullOrEmpty() && txtNewTimeOut.Text.IsNullOrEmpty())
                {
                    att.s_In1 = "";
                    att.s_Out1 = "";
                }
                else
                {
                    att.In1 = newIn;
                    att.Out1 = newOut;
                }
                att.Reason = txtNewRemarks.Text;
                att.EarlyWork = false;
                //save
                att.Save();

                ReprocessAttendance(date.ToDate());

                if (sentForApproval)
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsmsg", "alert('Attendance changes sent for approval.');", true);
                else
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "ofsmsg", "alert('Successfuly saved.');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "offset2", "alert('" + ex.Message + "');", true);
        }
    }

    protected void btnVerify_Click(object sender, EventArgs e)
    {
        var tblPeriod = SqlHelper.ExecuteDataset(_ConnectionString2, CommandType.StoredProcedure,
                    "dbo.usa_AttendanceSummarizeGetPeriod", new SqlParameter[] {
                                new SqlParameter("@DBName", _KioskSession.DB)
                                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                                }).Tables[0];

        if (tblPeriod.Rows.Count > 0)
        {
            var drPeriod = tblPeriod.Rows[0];
            //txtTranStart.Text = ((DateTime)drPeriod["StartDate"]).ToString(_KioskSession.DateFormat);
            //txtTranEnd.Text = ((DateTime)drPeriod["EndDate"]).ToString(_KioskSession.DateFormat);
            //txtPayStart.Text = ((DateTime)drPeriod["PayStart"]).ToString(_KioskSession.DateFormat);
            //txtPayEnd.Text = ((DateTime)drPeriod["PayEnd"]).ToString(_KioskSession.DateFormat);

            var startDate = (DateTime)drPeriod["StartDate"];
            var endDate = (DateTime)drPeriod["EndDate"];

            var vt = new VerifiedTrans();

            if (!vt.LoadByPrimaryKey(_KioskSession.CompanyID,
                _KioskSession.EmployeeID,
                startDate,
                endDate))
            {
                vt.AddNew();
                vt.CompanyID = _KioskSession.CompanyID;
                vt.EmployeeID = _KioskSession.EmployeeID;
                vt.StartDate = startDate;
                vt.EndDate = endDate;
                vt.DateVerified = DateTime.Now;
                vt.Save();
            }
        }

        BindGrid();
    }
}
