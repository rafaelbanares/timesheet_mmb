﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class WorkFor : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            UpdateLabels();
            BindGrid(false);
        }
        Form.DefaultButton = "";
        //Page.Title = "MMB Kronos - " + lblModule.Text;
    }

    private void UpdateLabels()
    {
        lblModule.Text = _KioskSession.WorkFor + " File Maintenance";
        lbNew.Text = "New " + _KioskSession.WorkFor + " Definition";
        ibNew.ToolTip = "New " + _KioskSession.WorkFor + " Definition";
    }

    private void BindGrid(bool isAdd)
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForLoadByCompanyID",
            new SqlParameter[] { new SqlParameter("@CompanyID", _KioskSession.CompanyID), new SqlParameter("@DBname", _KioskSession.DB) });

        if (isAdd)
        {
            ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
            //ds.Tables[0].Rows.Add(ds.Tables[1].Rows[0].ItemArray);

            gvMain.DataSource = ds.Tables[0];
            gvMain.EditIndex = 0; //ds.Tables[0].Rows.Count - 1;
            gvMain.DataBind();

            TextBox txtShortDesc = gvMain.Rows[gvMain.EditIndex].FindControl("txtShortDesc") as TextBox;
            txtShortDesc.Attributes["onfocus"] = "javascript:this.select();";
            txtShortDesc.Focus();
        }
        else
        {
            gvMain.DataSource = ds.Tables[0];
            gvMain.DataBind();
        }
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid(false);
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvMain.EditIndex)
            {
                bool isUsed = DataBinder.Eval(e.Row.DataItem, "isUsed").ToString() == "1";
                (e.Row.FindControl("ibDelete") as ImageButton).Visible = (!isUsed && gvMain.EditIndex == -1);
                (e.Row.FindControl("ibEdit") as ImageButton).Visible = gvMain.EditIndex == -1;

                //rsb
                bool active = bool.Parse(DataBinder.Eval(e.Row.DataItem, "Active").ToString());
                Image imgActive = e.Row.FindControl("imgActive") as Image;
                imgActive.Visible = active;
            }
            else
            {
                bool active = true;
                bool.TryParse(DataBinder.Eval(e.Row.DataItem, "Active").ToString(), out active);
                CheckBox chkActive = e.Row.FindControl("chkActive") as CheckBox;
                chkActive.Checked = active;

            }
            
        }
    }

    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //... a JavaScript confirmation was triggered first
        //..delete
        string key = gvMain.DataKeys[e.RowIndex].Values["WorkForID"].ToString();

        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForDelete",
            new SqlParameter[] { 
                new SqlParameter("@CompanyID", _KioskSession.CompanyID), 
                new SqlParameter("@WorkForID", key), 
                new SqlParameter("@DBname", _KioskSession.DB) 
            });

        gvMain.EditIndex = -1;
        BindGrid(false);

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid(false);
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["WorkForID"].ToString();

        TextBox txtShortDesc = gvMain.Rows[e.RowIndex].FindControl("txtShortDesc") as TextBox;
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;
        CheckBox chkActive = gvMain.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;

        List<SqlParameter> sqlparam = new List<SqlParameter>();
        sqlparam.Add(new SqlParameter("@WorkForID", key));
        sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
        sqlparam.Add(new SqlParameter("@ShortDesc", txtShortDesc.Text));
        sqlparam.Add(new SqlParameter("@Description", txtDescription.Text));
        sqlparam.Add(new SqlParameter("@Active", chkActive.Checked));
        sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
        sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));
        sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
        sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));

        string errmsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_WorkForInsertUpdate", 
            sqlparam.ToArray()).Tables[0].Rows[0]["ErrMsg"].ToString();

        if (errmsg != "")
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "insertfail", "alert('" + errmsg + "');", true);
        }
        else
        {
            gvMain.EditIndex = -1;
            BindGrid(false);
        }
    }
    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }


}
