<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="UploadTimeTransaction.aspx.cs" Inherits="UploadTimeTransaction" Title="MMB Kronos - Upload Time Transaction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript">

	function UpdateImg(ctrl,imgsrc) {
		var img = $get(ctrl);
		img.src = imgsrc;
	}

	function toggle(isVisible)
	{
		if (isVisible)
		{
			setTimeout("UpdateImg('<%=imgProcess.ClientID%>','Graphics/indicator.gif');",50);            
			$get("<%=lblStatus.ClientID%>").innerHTML = "Uploading... please wait...";
			$get("<%=imgProcess.ClientID%>").style.display = "";
		}
		else
		{
			$get("<%=lblStatus.ClientID%>").innerHTML = "";
			$get("<%=imgProcess.ClientID%>").style.display = "none";
		}
	}
</script>

	<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
		Font-Underline="False" Text="Upload Transaction"></asp:Label><br />
	<br />

	<table cellpadding="0" class="TableBorder1" style="width: 574px">
		<tr>
			<td class="DataGridHeaderStyle">
				Please select a file to upload:
			</td>
		</tr>
		<tr>
			<td>
				Filename:
				<asp:FileUpload ID="fuWorkfile" runat="server" CssClass="ControlDefaults" Width="499px" /><br /><br />
				<asp:Button ID="btnSend" runat="server" CssClass="ControlDefaults" OnClick="btnSend_Click" Text="Upload" OnClientClick="toggle(true);return true;" Width="59px" /><br /><br />
				<asp:Label ID="lblStatus" runat="server"></asp:Label>
				<asp:Image ID="imgProcess" runat="server" ImageUrl="~/Graphics/indicator.gif" style="display:none;" /></td>
		</tr>
	</table>

	<br />
	<asp:Label ID="lblSummary" runat="server"></asp:Label><br />
	<br />
	<br />
	<asp:Label ID="lblMissing" runat="server" 
		Text="The following ID's were not registered in the system. Please check." 
		ForeColor="Red" Visible="False"></asp:Label>
	<br />
	<br />
	<asp:GridView ID="GridView1" runat="server" 
		AutoGenerateColumns="False" Width="221px">
		<Columns>
			<asp:BoundField DataField="EID" HeaderText="ID" />
		</Columns>
	</asp:GridView>
</asp:Content>

