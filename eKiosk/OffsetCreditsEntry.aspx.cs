﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Transactions;

using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;

public partial class OffsetCreditsEntry : KioskPageUI
{
    string _EmployeeID;

    protected void Page_Load(object sender, EventArgs e)
    {
        _EmployeeID = _Url["EmpID"];

        base.GetUserSession();
        if (!IsPostBack)
        {
            lblFullname.Text = _Url["EmpName"];
            BindGrid();
        }
    }


    private DataSet GetEmployeeOffset(bool insertBlank)
    {
        OffsetEarn ofsearn = new OffsetEarn();
        object[] parameters = { _KioskSession.CompanyID, _EmployeeID };
        string sql = "SELECT " +
                    "	b.Trandate, a.[hours], a.[minutes], b.Remarks " +
                    "FROM OffsetEarnDtl a INNER JOIN OffsetEarn b " +
                    "ON a.OffsetEarnID = b.OffsetEarnID " +
                    "WHERE a.CompanyID = {0} AND a.EmployeeID = {1} " +
                    "ORDER BY b.Trandate ";
        ofsearn.DynamicQuery(sql, parameters);
        DataSet ds = ofsearn.ToDataSet();
        if (insertBlank)
        {
            DataRow dr = ds.Tables[0].NewRow();
            dr["Trandate"] = DateTime.Today;
            dr["Hours"] = 0;
            dr["Minutes"] = 0;
            dr["Remarks"] = "Add offset credit";
            ds.Tables[0].Rows.Add(dr);
        }
        return ds;
    }

    private void BindGrid()
    {
        DataSet ds = GetEmployeeOffset(false);
        gvEmp.DataSource = ds.Tables[0];
        gvEmp.DataBind();
        hfIsAdd.Value = "0";
    }

    private void BindGridAdd()
    {        
        DataSet ds = GetEmployeeOffset(true);
        gvEmp.DataSource = ds.Tables[0];
        gvEmp.EditIndex = ds.Tables[0].Rows.Count - 1;

        gvEmp.DataBind();
        hfIsAdd.Value = "1";
    }
    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void gvEmp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmp.EditIndex = -1;
        BindGrid();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                //Label lblDays = e.Row.FindControl("lblDays") as Label;
                //Label lblHours = e.Row.FindControl("lblHours") as Label;
                //Label lblMinutes = e.Row.FindControl("lblMinutes") as Label;

                //if (lblDays.Text.TrimEnd() == "0") 
                //    lblDays.Text = "";
                //if (lblHours.Text.TrimEnd() == "0")
                //    lblHours.Text = "";
                //if (lblMinutes.Text.TrimEnd() == "0")
                //    lblMinutes.Text = "";
            }
        }
    }

    private bool validNumber(string s)
    {        
        int n;
        if (s.TrimEnd() == "") return true;

        if (!int.TryParse(s, out n))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertmsg", "alert('Invalid number. Please check.');", true);
            return false;
        }        
        return true;
    }

    protected void gvEmp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox txtTranDate = gvEmp.Rows[e.RowIndex].FindControl("txtTranDate") as TextBox;
        TextBox txtHours = gvEmp.Rows[e.RowIndex].FindControl("txtHours") as TextBox;
        TextBox txtMinutes = gvEmp.Rows[e.RowIndex].FindControl("txtMinutes") as TextBox;
        TextBox txtRemarks = gvEmp.Rows[e.RowIndex].FindControl("txtRemarks") as TextBox;

        //validations
        if (!validNumber(txtHours.Text)) return;
        if (!validNumber(txtMinutes.Text)) return;
        
        using (TransactionScope ts = new TransactionScope())
        {
            OffsetEarn ofsEarn = new OffsetEarn();
            ofsEarn.AddNew();
            ofsEarn.CompanyID = _KioskSession.CompanyID;
            ofsEarn.s_TranDate = txtTranDate.Text;
            ofsEarn.AppYear = DateTime.Today.Year;
            ofsEarn.Remarks = txtRemarks.Text;
            ofsEarn.CreatedBy = _KioskSession.UID;
            ofsEarn.CreatedDate = DateTime.Now;
            ofsEarn.Save();

            OffsetEarnDtl ofsDtl = new OffsetEarnDtl();
            ofsDtl.AddNew();
            ofsDtl.OffsetEarnID = (int) ofsEarn.OffsetEarnID;
            ofsDtl.CompanyID = _KioskSession.CompanyID;
            ofsDtl.EmployeeID = _Url["EmpID"];
            ofsDtl.Days = 0;
            ofsDtl.Hours = int.Parse(txtHours.Text);
            ofsDtl.Minutes = int.Parse(txtMinutes.Text);
            ofsDtl.Save();

            ts.Complete();
        }
        
        
        gvEmp.EditIndex = -1;
        gvEmp.SelectedIndex = -1;
        BindGrid();
    }


    


    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGridAdd();
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGridAdd();
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        gvEmp.EditIndex = -1;
        gvEmp.EditIndex = -1;
        BindGrid();
    }
}