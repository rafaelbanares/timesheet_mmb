﻿using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class LeaveSummary : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            PopulateYear();
            BindGrid();            
        }
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }

    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    //
    // DO NOT USE THE STATUS In LeaveTrans.
    // USE THE STATUS IN LeaveTransHdr instead
    private DataSet GetData()
    {
        int year = ddlYear.SelectedValue.ToInt();
        string sql = "SELECT " +
                        "	EmployeeID = coalesce(used.EmployeeID, earn.EmployeeID), " +
                        "	LeaveCode = coalesce(lc.Code, used.LeaveCode, earn.LeaveCode), " +
                        "	LeaveDescr = lc.[Description], " +
                        "	DaysApproved = coalesce(used.DaysApproved, 0), " +
                        "	DaysForApproval = coalesce(used.DaysForApproval, 0), " +
                        "	DaysEarned = coalesce(earn.earned, 0), " +
                        "	Balance = coalesce(earn.earned, 0) - coalesce(used.DaysApproved, 0) - coalesce(used.DaysForApproval, 0) " +
                        "FROM " +
                        "	( " +
                        "	SELECT " +
                        "		EmployeeID = coalesce(a.EmployeeID, b.EmployeeID), " +
                        "		LeaveCode = coalesce(a.Code, b.Code), " +
                        "		DaysApproved = coalesce(a.Days, 0), " +
                        "		DaysForApproval = coalesce(b.Days, 0) " +
                        "	FROM udf_LeaveTransSummary({0}, {1}, {2}, {3}, '') a " +
                        "	FULL OUTER JOIN udf_LeaveTransSummary({0}, {1}, {2}, {4}, '') b " +
                        "	ON " +
                        "		a.EmployeeID = b.EmployeeID and " +
                        "		a.Code = b.Code " +
                        "	) used " +
                        "FULL OUTER JOIN udf_LeaveEarnSummary({0}, {1}, {2}, '') earn " +
                        "ON " +
                        "	used.EmployeeID = earn.EmployeeID and " +
                        "	used.LeaveCode = earn.LeaveCode " +
                        "RIGHT JOIN LeaveCode lc " +
                        "ON lc.Code = coalesce(used.LeaveCode, earn.LeaveCode) " +
                        "WHERE lc.CompanyID = {0} ";
        object[] parameters =  { _KioskSession.CompanyID, 
                                   _KioskSession.EmployeeID,
                                   year,
                                   TKWorkFlow.Status.Approved,
                                   TKWorkFlow.Status.ForApproval
                               };
        BNS.TK.Entities.LeaveTrans leave = new BNS.TK.Entities.LeaveTrans();
        leave.DynamicQuery(sql, parameters);

        return leave.ToDataSet();

    }

    
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow) return;

        if (e.Row.RowIndex != gvEmp.EditIndex)
        {
            HyperLink hlAvailed = e.Row.FindControl("hlAvailed") as HyperLink;
            HyperLink hlPending = e.Row.FindControl("hlPending") as HyperLink;
            string leavecode = DataBinder.Eval(e.Row.DataItem, "LeaveCode").ToString().SafeTrim();

            if (hlAvailed.Text.ToDecimal() > 0)
            {
                hlAvailed.Font.Bold = true;
                hlAvailed.NavigateUrl = "LeaveApplications.aspx?year=" + ddlYear.SelectedValue;
                //hlAvailed.NavigateUrl = (new SecureUrl(string.Format("LeaveApplications.aspx?type={0}&status={1}&year={2}", leavecode, TKWorkFlow.Status.Approved, ddlYear.SelectedValue))).ToString();
            }
            
            if (hlPending.Text.ToDecimal() > 0)
            {
                hlPending.Font.Bold = true;
                hlPending.NavigateUrl = "LeaveApplications.aspx?year=" + ddlYear.SelectedValue;
                //hlPending.NavigateUrl = (new SecureUrl(string.Format("LeaveApplications.aspx?type={0}&status={1}&year={2}", leavecode, TKWorkFlow.Status.ForApproval, ddlYear.SelectedValue))).ToString();
            }
        }
        
    }
}