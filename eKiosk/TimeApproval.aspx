<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="TimeApproval.aspx.cs" Inherits="TimeApproval" Title="DTR Corrections " %>
<%@ Register src="EmpHeaderDate.ascx" tagname="EmpHeaderDate" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <script type="text/javascript" src="css/common_uno.js"></script>
    <uc1:EmpHeaderDate ID="EmpHeader1" runat="server" />
    <hr />
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" 
            OnRowUpdating="gvEmp_RowUpdating" Width="100%" 
            DataKeyNames="orig_shiftcode,orig_restcode">
        <Columns>

            <asp:TemplateField HeaderText="Date">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "Date"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Day">
                <ItemTemplate>
                    <asp:Literal ID="litDay" runat="server" Text='<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "Date").ToString()).DayOfWeek.ToString().Substring(0,3).ToLower() %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Shift In">
                <ItemTemplate>
                    <asp:Literal ID="litShiftIn" runat="server" Text='<%# Bind("ShiftIn") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Shift Out">
                <ItemTemplate>
                    <asp:Literal ID="litShiftOut" runat="server" Text='<%# Bind("ShiftOut") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="In1">
                <ItemTemplate>
                    <asp:Literal ID="litIn1" runat="server" Text='<%# Bind("orig_In1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Out1">
                <ItemTemplate>
                    <asp:Literal ID="litOut1" runat="server" Text='<%# Bind("orig_Out1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="New In1">
                <EditItemTemplate>
                    <asp:TextBox ID="tbin1" runat="server" Text='<%# Bind("new_In1", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1" ControlToValidate="tbin1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbin1">
                    </ajaxToolkit:MaskedEditExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litNewIn1" runat="server" Text='<%# Bind("new_In1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="New Out1">
                <EditItemTemplate>
                    <asp:TextBox ID="tbout1" runat="server" Text='<%# Bind("new_Out1", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="tbout1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbout1">
                    </ajaxToolkit:MaskedEditExtender>                    
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litNewOut1" runat="server" Text='<%# Bind("new_Out1", "{0:hh:mm tt}") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Literal ID="litStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason for correction">
                <EditItemTemplate>
                    <asp:TextBox ID="tbreason" runat="server" Text='<%# Bind("reason") %>' 
                        Width="250px" MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReason" runat="server" 
                        ControlToValidate="tbreason" 
                        ErrorMessage="Please indicate a reason for corrrections" 
                        ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblreason" runat="server" Text='<%# Bind("reason") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="320px" />
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle Width="42px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <asp:HiddenField ID="hfErrorMsg" runat="server" />
    
        <br />
        <br />
    <hr />
    <table>
        <tr align='left'>
            <td><strong>Legend:</strong></td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_Restday%>"></td><td style="width:120px">- Restday</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_Holiday%>"></td><td style="width:120px">- Holiday</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_RstHoliday%>"></td><td style="width:120px">- Restday/Holiday</td>
        </tr>
        <tr>
            <td colspan='7'>
            </td>
        </tr>
        <tr align='left'>
            <td></td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_LeaveWhole%>"></td><td>- Wholeday Leave</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_LeaveHalf%>"></td><td>- Partial Leave</td>
            <td style="width:20px; height:22px; background-color:<%=sBackColor_Absent%>"></td><td>- Absent</td>
        </tr>
    </table>
    
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="btnFU"></asp:AsyncPostBackTrigger>
    </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none" />

</asp:Content>

