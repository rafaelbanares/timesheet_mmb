using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BNS.Framework.Encryption;

public partial class Zinc : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Label1.Text = Encrypt(TextBox1.Text);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Label1.Text = Decrypt(TextBox1.Text);
    }

    private string Encrypt(string s)
    {
        string encryptionKey = ConfigurationManager.AppSettings.Get("EncryptionKey");
        string iVector = ConfigurationManager.AppSettings.Get("InitializationVector");
        return Crypto.Encrypt(s, encryptionKey, iVector);
    }
    private string Decrypt(string s)
    {
        string encryptionKey = ConfigurationManager.AppSettings.Get("EncryptionKey");
        string iVector = ConfigurationManager.AppSettings.Get("InitializationVector");
        return Crypto.Decrypt(s, encryptionKey, iVector);
    }

}
