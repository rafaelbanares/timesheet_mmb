﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ChangeApprover.aspx.cs" Inherits="ChangeApprover" Title="Change Approver" %>
<%@ Register src="EmployeeHeader.ascx" tagname="EmployeeHeader" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue, openerCompanyClientID, openerCompanyValue)
{
    $get(openerEmpClientID).value = openerEmpValue;
    $get(openerCompanyClientID).value = openerCompanyValue;
    
    //$get(openerNameClientID).innerHTML = openerNameValue; -- doesn't need this, just for the sake of consistency in eTime
    $get('<% =btnRefresh.ClientID %>').click();
    closewindow(popupWindow);
}
function openSelectEmp(sUrl)
{
    popupWindow=wopen(sUrl,"popupWindow",1000,600);
}
</script>

<asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <uc1:EmployeeHeader ID="EmpHeader1" runat="server" />
    <hr />
    <br />

    <asp:Button ID="btnSave" runat="server" Text="Save Changes" Enabled="False" onclick="btnSave_Click" />
    <br />
    <br />

    <table width="500px">
        <tr>
            <td style="width:90px">
                Approver 1:
            </td>
            <td>
                <asp:Label ID="lblApprover1" runat="server"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                Approver 2:
            </td>
            <td>
                <asp:Label ID="lblApprover2" runat="server"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                Approver 3:
            </td>
            <td>
                <asp:Label ID="lblApprover3" runat="server"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Button ID="btnChange" runat="server" Text="Select New Approver" />
            </td>
        </tr>
    </table>
    <br />

    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
        
    <asp:HiddenField ID="hfApprover" runat="server" />
    <asp:HiddenField ID="hfCompanyID" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnRefresh" />
    </Triggers>
</asp:UpdatePanel>


<asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display:none" onclick="btnRefresh_Click" />

</asp:Content>

