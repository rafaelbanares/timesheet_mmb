using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class Attendance_OTAuthListApprover : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindOTAuth();
        }
    }

    #region Data Bindings
    #region BindOTAuth
    private void BindOTAuth()
    {
        gvLAppr.DataSource = SortDataTable(GetOTAuthForApproval().Tables[0], false);
        gvLAppr.DataBind();
    }
    #endregion
    #region GetOTAuthForApproval
    private DataSet GetOTAuthForApproval()
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@DBName", _KioskSession.DB)
            ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@ApproverID", _KioskSession.EmployeeID)
            };

        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthByApproverID", spParams);
        if (ds.Tables[0].Rows.Count <1)
        {
            SecureUrl su = new SecureUrl("BlankPage.aspx?Message=There's no filed overtime for your approval");
            Response.Redirect(su.ToString());
        }

        return ds;
    }
    #endregion
    #endregion

    protected void ibEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = sender as ImageButton;
        HiddenField hfID = ib.Parent.Parent.FindControl("hfID") as HiddenField;
        
        SecureUrl su = new SecureUrl("OTAuthApproval.aspx?OTAuthID=" + hfID.Value);
        Response.Redirect(su.ToString());
    }

}
