﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Bns.AttendanceUI;
using Bns.AttUtils;

public partial class EmpHeaderFilter2 : UserControlUI
{
    public string Display { get {return ddlDisplay.SelectedItem.Value;} }
    public string Search { get { return txtSearch.Text; } }
    public string Year { get { return ddlYear.SelectedItem.Value; } }
    public string ApproverNo { get { return hdApprNo.Value; } set { hdApprNo.Value = value; } }



    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
        if (!Page.IsPostBack)
        {
            PopulateYear();            
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!Page.IsPostBack)
        {
            PopulateScreenDefaults();
            //ShowApproverCaption();
            txtSearch.Attributes.Add("onchange", "refresh();return false;");
            txtSearch.Attributes.Add("onkeypress", "return enterpress(event);");
        }
    }

    //private void ShowApproverCaption()
    //{
    //    if (this.ApproverNo == "1")
    //        lblFunction.Text = "as Level 1 Main Approver";
    //    else if (this.ApproverNo == "2")
    //        lblFunction.Text = "as Level 1 Backup Approver";
    //    else if (this.ApproverNo == "3")
    //        lblFunction.Text = "as Level 2 Main Approver";
    //    else if (this.ApproverNo == "4")
    //        lblFunction.Text = "as Level2 Backup Approver";
    //}

    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

    public void PopulateScreenDefaults()
    {
        lblFullname.Text = _KioskSession.EmployeeName + " (" + _KioskSession.EmployeeID.Trim() + ")";

        SecureUrl secureUrl = new SecureUrl(string.Format("EmployeeInfo.aspx?CompID={0}&EmpID={1}", _KioskSession.CompanyID, _KioskSession.EmployeeID.Trim()));
        lbEmpInfo.Attributes.Add("onclick", "return openEmpInfo('" + secureUrl.ToString() + "');");
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(Page), "refresh", "refresh();", true);
    }
    protected void ddlDisplay_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(Page), "refresh", "refresh();", true);

    }
    
}
