using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using BNS.TK.Entities;

public partial class eTimeHome : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.AccessLevel = PageAccessLevel.etime;
        base.GetUserSession();

        lblMessage.Text = "";
        if (!IsPostBack)
        {
            txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
            txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
            populateScreenDefault();
            ShowLockStatus();
        }
    }

    private void populateScreenDefault()
    {
        DataRow drSummary = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_eTimeHomeSummary",
                        new SqlParameter[] {
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@StartDate", txtDateStart.Text.ToDate()) ,
                            new SqlParameter("@EndDate", txtDateEnd.Text.ToDate()) 
                        }
                    ).Tables[0].Rows[0];

        //... for approval
        int unapprovedDTR = (int)drSummary["DTR_FA"];
        int unapprovedLeave = (int)drSummary["Leave_FA"];
        int unapprovedOT = (int)drSummary["OT_FA"];
        //int unapprovedShift = (int)drSummary["CShift_FA"];
        //int unapprovedRestday = (int)drSummary["CRestd_FA"];
        int unapprovedOB = (int)drSummary["OB_FA"];

        hlDTR.Text = string.Format("({0}) DTR waiting for approval", unapprovedDTR);
        hlUnapproveLeave.Text = string.Format("({0}) Filed leave(s) waiting for approval", unapprovedLeave);
        hlUnapproveOT.Text = string.Format("({0}) Filed overtime waiting for approval", unapprovedOT);
        //hlCShift.Text = string.Format("({0}) Shift waiting for approval", unapprovedShift);
        //hlCRestday.Text = string.Format("({0}) Restday waiting for approval", unapprovedRestday);
        hlOB.Text = string.Format("({0}) Filed OB waiting for approval", unapprovedOB);

        hlDTR.NavigateUrl = "TimeTransactionApproval.aspx?approverno=9";
        hlUnapproveLeave.NavigateUrl = "LeaveApproverList.aspx?approverno=9"; //"AdminLeave.aspx";
        hlUnapproveOT.NavigateUrl = "OTApproverList.aspx?approverno=9"; //"AdminOT.aspx";
        //hlCShift.NavigateUrl = "ChangeShiftApproval.aspx?approverno=9";
        //hlCRestday.NavigateUrl = "ChangeRestdayApproval.aspx?approverno=9";
        hlOB.NavigateUrl = "OBApproval.aspx?approverno=9";

        //hlDTR.NavigateUrl = unapprovedDTR < 1 ? "" : "TimeTransactionApproval.aspx?approverno=9";
        //hlUnapproveLeave.NavigateUrl = unapprovedLeave < 1 ? "" : "LeaveApproverList.aspx?approverno=9"; //"AdminLeave.aspx";
        //hlUnapproveOT.NavigateUrl = unapprovedOT < 1 ? "" : "OTApproverList.aspx?approverno=9"; //"AdminOT.aspx";
        //hlCShift.NavigateUrl = unapprovedShift < 1 ? "" : "ChangeShiftApproval.aspx?approverno=9";
        //hlCRestday.NavigateUrl = unapprovedRestday < 1 ? "" : "ChangeRestdayApproval.aspx?approverno=9"; 

        //... clear status message
        lblMessage.Text = "";
        txtDateStart.Text = _KioskSession.StartDate.ToShortDateString();
        txtDateEnd.Text = _KioskSession.EndDate.ToShortDateString();
    }

    //private void populate_Dropdown()
    //{
    //    DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text,
    //        "SELECT rtrim(CompanyID) as CompanyID, CompanyName FROM " + _KioskSession.DB + ".dbo.Company ORDER BY CompanyName ");
    //    hdCompanyID.Value = ds.Tables[0].Rows[0]["CompanyID"].ToString().TrimEnd();
    //    hdCompanyName.Value = ds.Tables[0].Rows[0]["CompanyName"].ToString().TrimEnd();
    //}
        
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        UpdateUserDefaults();
        lblMessage.Text = "Default dates successfully updated";
    }

    protected void btnCompany_Click(object sender, EventArgs e)
    {
        UpdateUserDefaults();
        lblMessage.Text = "Default company successfully updated";
    }

    private void UpdateUserDefaults()
    {
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_UserDefaultsUpdate",
                new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID),
                            new SqlParameter("@ID", _KioskSession.UID),
                            new SqlParameter("@AttFrom", txtDateStart.Text),
                            new SqlParameter("@AttTo", txtDateEnd.Text),
                            new SqlParameter("@AttCompanyID", hdCompanyID.Value) 
                        });

        _KioskSession.StartDate = DateTime.Parse(txtDateStart.Text);
        _KioskSession.EndDate = DateTime.Parse(txtDateEnd.Text);
        //_KioskSession.CompanyID = hdCompanyID.Value;
        //_KioskSession.CompanyName = hdCompanyName.Value;
        base.SetUserSession(_KioskSession);

        populateScreenDefault();
    }

    private void ShowLockStatus()
    {
        RunData rd = new RunData();
        rd.LoadAll();
        lblLockStat.Text = (rd.KioskLocked) ? "Locked" : "Available";
        btnLockUnlock.Text = (rd.KioskLocked) ? "Unlock Kronos" : "Lock Kronos";
    }

    private void ToggleLockStatus()
    {
        RunData rd = new RunData();
        rd.LoadAll();
        //toggle        
        rd.KioskLocked = (!rd.KioskLocked);        
        rd.Save();

        //refresh display
        ShowLockStatus();
    }

    protected void btnLockUnlock_Click(object sender, EventArgs e)
    {
        ToggleLockStatus();
    }
}
