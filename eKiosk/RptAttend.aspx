<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="RptAttend.aspx.cs" Inherits="RptAttend" Title="MMB Kronos - Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;
    
    closewindow(popupWindow);
}

function updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue)
{
    $get(openerEmpClientID).value = openerEmpValue;
    $get(openerNameClientID).value = openerNameValue;
    closewindow(popupWindow);
}

</script>

<table style="width:920px" border="1">
<tr>
    <td valign="top">
    <table cellpadding="0" class="TableBorder1" style="width: 450px">
        <tr>
            <td class="DataGridHeaderStyle" style="height: 12px">
                <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                    Width="98%"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    
    <table cellpadding="0" class="TableBorder1" style="width: 450px">
        <tr>
            <td class="DataGridHeaderStyle" colspan="3" style="height: 12px">
                Report Options</td>
        </tr>
        <tr>    
            <td style="width: 142px" align="right">
                Transaction From:
            </td>
            <td align="left">
                <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    OnClientClick="return false;" ToolTip="Click to choose date" />                    
                <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                    ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
            <td>
                <asp:CheckBox ID="chkExcel" runat="server" Text="Export to Excel" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 142px">
                Transaction
                To:</td>
            <td align="left">
                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    OnClientClick="return false;" ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                    ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right">Group By:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlGroupBy" runat="server" CssClass="ControlDefaults"
                    Width="226px">
                    <asp:ListItem Value="N">None</asp:ListItem>
                    <asp:ListItem Value="DEPT" Selected="True">Department</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="right" >Sort By:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlSortBy" runat="server" CssClass="ControlDefaults" Width="226px">
                    <asp:ListItem Value="NAME">Employee Name</asp:ListItem>
                    <asp:ListItem Value="EMPID">Employee ID</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="right" >Employee ID:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="ControlDefaults" 
                    DataTextField="FullName" DataValueField="EmployeeID" ValidationGroup="Trans">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">Status</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ControlDefaults" 
                    DataTextField="DisplayName" DataValueField="Descripytion" ValidationGroup="Trans">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
  </td>
  <td valign="top">
  <br />
    <asp:GridView ID="grvQuarterly" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
        OnRowDataBound="grvQuarterly_RowDataBound">
        <Columns>
            <asp:BoundField DataField="ReportID" Visible="False" />
            <asp:TemplateField HeaderText="Select Reports">
                <HeaderStyle Width="300px" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnkReport" runat="server" CssClass="Report" OnClick="lnkReport_Click"
                        Text='<%# Bind("Reportname") %>'></asp:LinkButton>
                    <input id="hdnID" runat="server" type="hidden" value='<%# Eval("ReportID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
        <EmptyDataTemplate>
            &nbsp;
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
  </td>
</tr>

</table>

    
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibDateStart"
        TargetControlID="txtDateStart" PopupPosition="BottomRight">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibDateEnd"
        TargetControlID="txtDateEnd" PopupPosition="BottomRight">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="hfLevels" runat="server" Value="~~~" />
    <asp:HiddenField ID="hfURL" runat="server" />

</asp:Content>

