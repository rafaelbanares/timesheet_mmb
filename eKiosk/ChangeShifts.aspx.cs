﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;
using BNS.TK.Business;


public partial class ChangeShifts : KioskPageUI
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        base.GetUserSession();
        if (!IsPostBack)
        {
            PopulateYear();
            BindGrid();
        }
    }
    private void PopulateYear()
    {
        ddlYear.Items.Clear();
        ddlYear.DataSource = GetYearLookup();        
        ddlYear.DataBind();
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();
    }

    private List<ListItem> GetYearLookup()
    {
        var year = DateTime.Today.Year + 1;
        var list = new List<ListItem>();

        for (int i = 0; i < 4; i++)
            list.Add(new ListItem((year - i).ToString(), (year - i).ToString()));

        return list;
    }

   
    private void BindGrid(bool addnew = false)
    {
        string sdate = "01/01/" + ddlYear.SelectedValue;
        string edate = "12/31/" + ddlYear.SelectedValue;
        BNS.TK.Entities.ChangeShift cs = new BNS.TK.Entities.ChangeShift();
        string sql = "SELECT " +
                        "	cs.ID, " +
                        "	cs.StartDate, " +
                        "	cs.EndDate, " +
                        "	cs.ShiftCode, " +
                        "	s.[Description], " +
                        "	cs.[Status] " +
                        "FROM ChangeShift cs " +
                        "INNER JOIN Shift s ON cs.CompanyID = s.CompanyID AND cs.ShiftCode = s.ShiftCode " +
                        "WHERE  " +
                        "	cs.CompanyID = {0} AND  " +
                        "	cs.EmployeeID = {1} AND " +
                        "   (cs.StartDate <= {3} and cs.EndDate >= {2}) " +
                        "ORDER BY cs.EndDate DESC ";
        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID, sdate.ToDate(), edate.ToDate() };
        cs.DynamicQuery(sql, parameters);

        if (addnew)
        {
            cs.AddNew();
            gvMain.EditIndex = cs.RowCount - 1;            
        }
        
        gvMain.DataSource = cs.DefaultView;        
        gvMain.DataBind();

        hfIsAdd.Value = (addnew) ? "1" : "0";
        
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvMain.EditIndex)
            {
                ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;
                ImageButton ibEdit = e.Row.FindControl("ibEdit") as ImageButton;
                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().TrimEnd().ToUpper();
                ibDelete.Visible = status.IsEqual(TKWorkFlow.Status.ForApproval);
                ibEdit.Visible = status.IsEqual(TKWorkFlow.Status.ForApproval);
                if (ibDelete != null)
                {
                    ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
                }
            }
            else
            {
                //edit mode
                DropDownList ddlShift = e.Row.FindControl("ddlShift") as DropDownList;
                //... populate Shift dropdown         
                BNS.TK.Entities.Shift shift = new BNS.TK.Entities.Shift();
                shift.Query.AddResultColumn("Shiftcode");
                shift.Query.AddResultColumn("Description");
                shift.Where.CompanyID.Value = _KioskSession.CompanyID;
                shift.Query.Load();

                ddlShift.DataSource = shift.DefaultView; //dsShift.Tables[0];
                ddlShift.DataBind();
                ddlShift.Items.Insert(0, "");

                ddlShift.SelectedValue = DataBinder.Eval(e.Row.DataItem, "ShiftCode").ToString();
            }
        }
    }
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        BNS.TK.Entities.ChangeShift cs = new BNS.TK.Entities.ChangeShift();
        if (cs.LoadByPrimaryKey(key.ToInt()))
        {
            cs.MarkAsDeleted();
            cs.Save();
        }

        gvMain.EditIndex = -1;
        BindGrid();

    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ID"].ToString();

        TextBox txtStart = gvMain.Rows[e.RowIndex].FindControl("txtStart") as TextBox;
        TextBox txtEnd = gvMain.Rows[e.RowIndex].FindControl("txtEnd") as TextBox;
        DropDownList ddlShift = gvMain.Rows[e.RowIndex].FindControl("ddlShift") as DropDownList;

        BNS.TK.Entities.ChangeShift cs = new BNS.TK.Entities.ChangeShift();
        if (hfIsAdd.Value == "1")
        {
            cs.AddNew();
            cs.CompanyID = _KioskSession.CompanyID;
            cs.EmployeeID = _KioskSession.EmployeeID;
            cs.Status = TKWorkFlow.Status.ForApproval;
            cs.Stage = 0;
            cs.CreatedBy = _KioskSession.UID;
            cs.CreatedDate = DateTime.Now;
        }
        else
        {
            cs.LoadByPrimaryKey(key.ToInteger());
        }        
        cs.StartDate = txtStart.Text.ToDate();
        cs.EndDate = txtEnd.Text.ToDate();
        cs.ShiftCode = ddlShift.SelectedItem.Value.ToInteger();
        cs.LastUpdBy = _KioskSession.UID;
        cs.LastUpdDate = DateTime.Now;
        cs.Save();

        //send email
        if (hfIsAdd.Value == "1")
        {
            //Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos for approval", "You have a pending change of shift for approval.");
        }

        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
    protected void ibGo_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}