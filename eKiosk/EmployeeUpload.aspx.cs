﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using Bns.AttendanceUI;
using ClosedXML.Excel;
using BNS.TK.Entities;

public partial class EmployeeUpload : KioskPageUI
{
    private string[] _header = { "CompanyID", "EmployeeID", "LastName", "FirstName", "MiddleName", "SwipeID", "Group", "Division", "Department", "EmploymentType", "PayrollType", "Gender", "Address1", "Address2", "Position", "BirthDate", "DateEmployed", "DateRegularized", "DateTerminated","Rank", "TardyExempt", "UTExempt", "OTExempt", "ShiftCode", "RestDay", "Password", "UserLevel", "Email", "Question", "Answer", "Level1 Approver", "Level1 Backup Approver", "Level2 Approver", "Level2 Backup Approver" };

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (fuWorkfile.HasFile)
        {
            try
            {
                string strFile = Server.MapPath("~/UploadedFiles/" + fuWorkfile.FileName);

                lblStatus.Text = "Uploading....";
                fuWorkfile.SaveAs(strFile);

                ProcessExcel(strFile);

                lblStatus.Text = "";
            }
            catch (Exception ex)
            {
                lblSummary.ForeColor = System.Drawing.Color.Red;
                lblSummary.Text = ex.Message; //"The file uploaded was not recognized by the system";
            }
        }
        else
        {
            lblStatus.Text = "You have not specified a file.";
        }
    }

    private void ProcessExcel(string excelFile)
    {
        Guid guid = Guid.NewGuid();
        var workbook = new XLWorkbook(excelFile);
        var ws = workbook.Worksheet(1);

        if (!ValidateHeader(ws))
        {
            lblSummary.Text = "Invalid excel template header.";
            return;
        }

        EmployeeTmp emp = new EmployeeTmp();
        int row = 2;

        string sCompanyID;
        string sEmployeeID;
        string sLastName;
        string sFirstName;
        string sMiddleName;
        string sSwipeID;
        string sGroup;
        string sDivision;
        string sDepartment;
        string sEmploymentType;
        string sPayrollType;
        string sGender;
        string sAddress1;
        string sAddress2;
        string sPosition;
        string sBirthDate;
        string sDateEmployed;
        string sDateRegularized;
        string sDateTerminated;
        string sRank;
        string sTardyExempt;
        string sUTExempt;
        string sOTExempt;
        string sShiftCode;
        string sRestDay;
        string sPassword;
        string sUserLevel;
        string sEmail;
        string sQuestion;
        string sAnswer;
        string sLevel1Approver;
        string sLevel1BackupApprover;
        string sLevel2Approver;
        string sLevel2BackupApprover;

        while (true)
        {
            int col = 0;

            sCompanyID = ws.Cell(row, ++col).Value.ToString();
            sEmployeeID = ws.Cell(row, ++col).Value.ToString();

            if (sCompanyID.IsNullOrEmpty() && sEmployeeID.IsNullOrEmpty()) break;

            sLastName = ws.Cell(row, ++col).Value.ToString();
            sFirstName = ws.Cell(row, ++col).Value.ToString();
            sMiddleName = ws.Cell(row, ++col).Value.ToString();
            sSwipeID = ws.Cell(row, ++col).Value.ToString();
            sGroup = ws.Cell(row, ++col).Value.ToString();
            sDivision = ws.Cell(row, ++col).Value.ToString();
            sDepartment = ws.Cell(row, ++col).Value.ToString();
            sEmploymentType = ws.Cell(row, ++col).Value.ToString();
            sPayrollType = ws.Cell(row, ++col).Value.ToString();
            sGender = ws.Cell(row, ++col).Value.ToString();
            sAddress1 = ws.Cell(row, ++col).Value.ToString();
            sAddress2 = ws.Cell(row, ++col).Value.ToString();
            sPosition = ws.Cell(row, ++col).Value.ToString();
            sBirthDate = ws.Cell(row, ++col).Value.ToString();
            sDateEmployed = ws.Cell(row, ++col).Value.ToString();
            sDateRegularized = ws.Cell(row, ++col).Value.ToString();
            sDateTerminated = ws.Cell(row, ++col).Value.ToString();
            sRank = ws.Cell(row, ++col).Value.ToString();
            sTardyExempt = ws.Cell(row, ++col).Value.ToString();
            sUTExempt = ws.Cell(row, ++col).Value.ToString();
            sOTExempt = ws.Cell(row, ++col).Value.ToString();
            sShiftCode = ws.Cell(row, ++col).Value.ToString();
            sRestDay = ws.Cell(row, ++col).Value.ToString();
            sPassword = ws.Cell(row, ++col).Value.ToString();
            sUserLevel = ws.Cell(row, ++col).Value.ToString();
            sEmail = ws.Cell(row, ++col).Value.ToString();
            sQuestion = ws.Cell(row, ++col).Value.ToString();
            sAnswer = ws.Cell(row, ++col).Value.ToString();
            sLevel1Approver = ws.Cell(row, ++col).Value.ToString();
            sLevel1BackupApprover = ws.Cell(row, ++col).Value.ToString();
            sLevel2Approver = ws.Cell(row, ++col).Value.ToString();
            sLevel2BackupApprover = ws.Cell(row, ++col).Value.ToString();
            

            //insert
            emp.AddNew();
            emp.SessionID = guid;
            emp.CompanyID = sCompanyID;
            emp.EmployeeID = sEmployeeID;
            emp.s_LastName = sLastName;
            emp.s_FirstName = sFirstName;
            emp.s_MiddleName = sMiddleName;
            emp.s_SwipeID = sSwipeID;
            emp.s_Level1 = "";
            emp.s_Level2 = sGroup;
            emp.s_Level3 = sDivision;
            emp.s_Level4 = sDepartment;
            emp.s_EmploymentType = sEmploymentType;
            emp.PayrollType = sPayrollType;
            emp.s_Gender = sGender;
            emp.s_Address1 = sAddress1;
            emp.s_Address2 = sAddress2;
            emp.s_Position = sPosition;
            emp.s_BirthDate =  sBirthDate;
            emp.s_DateEmployed = sDateEmployed;
            emp.s_DateRegularized =  sDateRegularized;
            emp.s_Rank = sRank;
            emp.TardyExempt = (sTardyExempt.ToUpper().Trim() == "YES");
            emp.UTExempt = (sUTExempt.ToUpper().Trim() == "YES");
            emp.OTExempt = (sOTExempt.ToUpper().Trim() == "YES");
            emp.ShiftCode = (sShiftCode.IsNullOrEmpty() ? 0 : sShiftCode.ToInteger());
            emp.s_RestDay = sRestDay;
            emp.s_Password = sPassword;
            emp.UserLevel =  0;
            emp.s_Email = sEmail;
            emp.s_Question = sQuestion;
            emp.s_Answer = sAnswer;
            emp.s_Level1Approver = sLevel1Approver;
            emp.s_Level1BackupApprover = sLevel1BackupApprover;
            emp.s_Level2Approver = sLevel2Approver;
            emp.s_Level2BackupApprover = sLevel2BackupApprover;
            emp.NonSwiper = false;
            emp.PwdNeverExpire = true;
            emp.AccountIsLocked = false;
            emp.CreatedBy = _KioskSession.EmployeeID;
            emp.CreatedDate = DateTime.Now;
             
            row++;
        }

        // save
        emp.Save();

        //backend process
        //string conn = System.Configuration.ConfigurationManager.AppSettings.Get("Connection");
        //SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@sessionid", SqlDbType.UniqueIdentifier) };
        //sqlParameters[0].Value = guid;

        //DataSet dsErrors = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_EmployeeUploadSession", sqlParameters);


        lblSummary.ForeColor = System.Drawing.Color.Black;
        lblSummary.Text = "Update Successful";
    }


    private bool ValidateHeader(IXLWorksheet ws)
    {
        for (int i = 0; i < _header.Length; i++)
        {
            if (ws.Cell(1, i+1).Value.ToString().ToUpper().Trim() != _header[i].ToUpper().Trim())
                return false;
        }
        return true;
    }

}