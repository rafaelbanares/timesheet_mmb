using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using BNS.TK.Entities;

public partial class eKioskHome : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();

        if (!IsPostBack)
        {            
            populateScreenDefault();
            SetExpiration();
            DisplayLockInfo();
            ShowInOut();
        }
    }

    private void populateScreenDefault()
    {
        DataSet dsKiosk = SqlHelper.ExecuteDataset(_ConnectionString2, CommandType.StoredProcedure, "dbo.usa_KioskByEmployeeIDWithIO",
                        new SqlParameter[] {
                            new SqlParameter("@DBName", _KioskSession.DB),
                            new SqlParameter("@CompanyID", _KioskSession.CompanyID) ,
                            new SqlParameter("@Date", DateTime.Today.ToString("yyyy-MM-dd")) ,
                            new SqlParameter("@EmployeeID", _KioskSession.EmployeeID) ,
                        }
                    );

        if (dsKiosk.Tables[0].Rows.Count > 0)
        {
            lblAttendanceDate.Text = ((DateTime)dsKiosk.Tables[0].Rows[0]["Date"]).ToShortDateString();
            lblShift.Text = dsKiosk.Tables[0].Rows[0]["ShiftIn"].ToString() + " - "
                            + dsKiosk.Tables[0].Rows[0]["ShiftOut"].ToString();
        }

        //... bind attendance grid / transaction details
        gvMain.DataSource = dsKiosk.Tables[2];  
        gvMain.DataBind();

        
        //... bind password settings
        bool isPwdNeverExpire = (bool)dsKiosk.Tables[3].Rows[0]["PwdNeverExpire"];
            
        if (!isPwdNeverExpire)
        {
            int daysValid = (int)dsKiosk.Tables[3].Rows[0]["PwdDaysValid"];
            DateTime expiryDate = (DateTime)dsKiosk.Tables[3].Rows[0]["PwdExpireDate"];

            if (daysValid < 90)
                lblExpiry.Text = string.Format("Your password will expire on {0}. You have {1} days left to change your password", expiryDate.ToShortDateString(), daysValid);
            else
                lblExpiry.Visible = false;

        }
        else
        {
            lblExpiry.Visible = false;
        }

        //... supress temporarily
        lblTimeIn.Text = "";
        lblTimeOut.Text = "";
        
    }
    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlAttend = e.Row.FindControl("hlAttend") as HyperLink;
            HyperLink hlLeave = e.Row.FindControl("hlLeave") as HyperLink;
            HyperLink hlOT = e.Row.FindControl("hlOT") as HyperLink;
            //HyperLink hlChShft = e.Row.FindControl("hlChShft") as HyperLink;
            //HyperLink hlChRstd = e.Row.FindControl("hlChRstd") as HyperLink;
            HyperLink hlOB = e.Row.FindControl("hlOB") as HyperLink;
            HyperLink hlExcused = e.Row.FindControl("hlExcused") as HyperLink;

            int attFiled = (int)DataBinder.Eval(e.Row.DataItem, "Attend");
            int leavefiled = (int)DataBinder.Eval(e.Row.DataItem, "Leave");
            int otfiled = (int)DataBinder.Eval(e.Row.DataItem, "OT");
            //int csfiled = (int)DataBinder.Eval(e.Row.DataItem, "CHSHIFT");
            //int crfiled = (int)DataBinder.Eval(e.Row.DataItem, "CHRSTD");
            int obfiled = (int)DataBinder.Eval(e.Row.DataItem, "OB");
            int excfiled = (int)DataBinder.Eval(e.Row.DataItem, "Excused");

            string summaryno = gvMain.DataKeys[e.Row.RowIndex].Values["SummaryNo"].ToString();
            if (summaryno == "0")   //employee
            {
                hlAttend.Text = (attFiled > 0) ? string.Format("DTR ({0}) For Approval", attFiled) : "Time Transactions";
                hlAttend.NavigateUrl = "TimeTransaction.aspx";

                hlOT.Text = (otfiled > 0) && !_KioskSession.OTExempt 
                    ? string.Format("({0}) For Approval", otfiled) : "Overtime Transactions";
                hlOT.NavigateUrl = "OTAuthList.aspx";
                hlOT.Enabled = !_KioskSession.OTExempt;
                

                hlLeave.Text = (leavefiled > 0) ? string.Format("({0}) For Approval", leavefiled) : "Leave Transactions";
                hlLeave.NavigateUrl = "LeaveApplications.aspx";

                //hlChShft.Text = (csfiled > 0) ? string.Format("({0}) For Approval", csfiled) : "Change Shift";
                //hlChShft.NavigateUrl = "ChangeShifts.aspx";

                //hlChRstd.Text = (crfiled > 0) ? string.Format("({0}) For Approval", crfiled) : "Change Restday";
                //hlChRstd.NavigateUrl = "ChangeRestday.aspx";

                hlOB.Text = (obfiled > 0) ? string.Format("({0}) For Approval", obfiled) : "OB";
                hlOB.NavigateUrl = "OBApplications.aspx";

                hlExcused.Text = (excfiled > 0) && (!_KioskSession.UTExempt && !_KioskSession.TardyExempt) 
                    ? string.Format("({0}) For Approval", excfiled) : "Excused";
                hlExcused.NavigateUrl = "ExcuseApplications.aspx";
                hlExcused.Enabled = !_KioskSession.UTExempt && !_KioskSession.TardyExempt;
                

            }
            else
            {
                string addtldescr = "approval"; 
                string link = "{0}?approverno=" + summaryno;

                hlAttend.Text = string.Format("({0}) {1}", attFiled, addtldescr);
                hlAttend.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "TimeTransactionApproval.aspx")).ToString();
                //hlAttend.Visible = (attFiled > 0);
                
                hlOT.Text = string.Format("({0}) {1}", otfiled, addtldescr );                
                hlOT.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "OTApproverList.aspx")).ToString();
                //hlOT.Visible = (otfiled > 0);

                hlLeave.Text = string.Format("({0}) {1}", leavefiled, addtldescr);
                hlLeave.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "LeaveApproverList.aspx")).ToString();
                //hlLeave.Visible = (leavefiled > 0);

                //hlChShft.Text = string.Format("({0}) {1}", csfiled, addtldescr);
                //hlChShft.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "ChangeShiftApproval.aspx")).ToString();
                ////hlChShft.Visible = (csfiled > 0);

                //hlChRstd.Text = string.Format("({0}) {1}", crfiled, addtldescr);
                //hlChRstd.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "ChangeRestdayApproval.aspx")).ToString();
                ////hlChRstd.Visible = (crfiled > 0);

                hlOB.Text = string.Format("({0}) {1}", obfiled, addtldescr);
                hlOB.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "OBApproval.aspx")).ToString();
                //hlOB.Visible = (obfiled > 0);

                hlExcused.Text = string.Format("({0}) {1}", excfiled, addtldescr);
                hlExcused.NavigateUrl = new Bns.AttUtils.SecureUrl(string.Format(link, "ExcuseApproval.aspx")).ToString();
                //hlExcused.Visible = (excfiled > 0);
            }
        }
    }


    private void ShowInOut()
    {
        string sql =
                "SELECT " +
                "	timein = ( " +
                "	select min(TimeIO) " +
                "	from TimeTrans " +
                "	where companyID = {0} and " +
                "		EmployeeID = {1} and " +
                "		Convert(varchar(10), TimeIO, 101) = {2} and " +
                "		[IO] = 'I' " +
                "	 ), " +
                "	timeout = ( " +
                "	select max(TimeIO) " +
                "	from TimeTrans " +
                "	where companyID = {0} and " +
                "		EmployeeID = {1} and " +
                "		Convert(varchar(10), TimeIO, 101) = {2} and " +
                "		[IO] = 'O' " +
                "	)";
        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID, DateTime.Today.ToString("MM/dd/yyyy") };
        BNS.TK.Entities.TimeTrans timeTrans = new BNS.TK.Entities.TimeTrans();
        timeTrans.DynamicQuery(sql, parameters);
        if (timeTrans.RowCount > 0)
        {
            if (!Convert.IsDBNull(timeTrans.GetColumn("timein")))
                lblTimeIn.Text = Convert.ToDateTime(timeTrans.GetColumn("timein")).ToString("hh:mm tt");

            if (!Convert.IsDBNull(timeTrans.GetColumn("timeout")))
                lblTimeOut.Text = Convert.ToDateTime(timeTrans.GetColumn("timeout")).ToString("hh:mm tt");            
        }

 
    }

    private void SetExpiration()
    {
        RunData rd = new RunData();
        rd.LoadAll();

        Kiosk kiosk = (Kiosk)Session["kiosk"];
        kiosk.IsLocked = rd.KioskLocked;
        Session["kiosk"] = kiosk;

        if (rd.KioskLocked)
        {
            //show lock message            
            mpe2.Show();
        }
    }

    #region SetExpiration_old
    private void SetExpiration_old()
    {        
        DataSet ds = TransPeriod.GetLockStatus(_KioskSession.CompanyID, DateTime.Today);
        if (ds.Tables[0].Rows.Count == 0) return;

        if (IsApprover())
        {
            if (ds.Tables[0].Rows[0]["appr_lock_status"].ToString() == "LOCKED")
            {
                lblEmpLockStat.ForeColor = System.Drawing.Color.Red;
                lblEmpLockStat.Text = "Currently locked! It will be available on " + Convert.ToDateTime(ds.Tables[0].Rows[0]["approver_lock_end"]).ToString("MMM dd, yyyy");
                Session["kiosk"] = null;
            }
            else
            {
                lblEmpLockStat.Text = "All approvals will be locked on " + Convert.ToDateTime(ds.Tables[0].Rows[0]["approver_lock_start"]).ToString("MMM dd, yyyy");
            }
        }
        else
        {
            if (ds.Tables[0].Rows[0]["emp_lock_status"].ToString() == "LOCKED")
            {
                lblEmpLockStat.ForeColor = System.Drawing.Color.Red;
                lblEmpLockStat.Text = "Currently locked! It will be available on " + Convert.ToDateTime(ds.Tables[0].Rows[0]["employee_lock_end"]).ToString("MMM dd, yyyy");
                Session["kiosk"] = null;
            }
            else
            {
                lblEmpLockStat.Text = "All filing will be locked on " + Convert.ToDateTime(ds.Tables[0].Rows[0]["employee_lock_start"]).ToString("MMM dd, yyyy");
            }
        }
    #endregion

    }

    #region IsApprover
    private bool IsApprover()
    {
        Approver appr = new Approver();
        object[] parameters = { _KioskSession.CompanyID, _KioskSession.EmployeeID };
        string sql = "Select top 1 1 From EmployeeApprover " +
                    "Where CompanyID = {0} and " +
                    "  (Approver1 = {1} or " +
                    "   Approver2 = {1} or " +
                    "   Approver3 = {1} or " +
                    "   Approver4 = {1}) ";
        appr.DynamicQuery(sql, parameters);
        return (appr.RowCount == 1);
    }
    #endregion

    private void DisplayLockInfo()
    {
        DateTime today = DateTime.Now;
        TransPeriod tp = GetTransPeriod(today);
        tp.Filter = "StartDate <= '" + today.ToShortDateString() + "' AND EndDate >= '" + today.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            //employee locking
            if (tp.s_Employee_lock_start != "" && tp.s_Employee_lock_end != "")
            {
                if (today >= tp.Employee_lock_start && today <= tp.Employee_lock_end)
                {
                    //currently locked
                    lblEmpLockStat.Text = "Personal Transactions Locked";
                }
                else
                {
                    if (tp.Employee_lock_start > today)
                    {
                        TimeSpan tsTimeLeft = tp.Employee_lock_start.Subtract(today);
                        lblEmpLockStat.Text = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", tsTimeLeft.Days, tsTimeLeft.Hours, tsTimeLeft.Minutes, tsTimeLeft.Seconds);
                    }
                }
            }

            //approver locking
            lblApprLockLabel.Visible = IsApprover();
            if (IsApprover() && tp.s_Approver_lock_start != "" && tp.s_Approver_lock_end != "")
            {
                if (today >= tp.Approver_lock_start && today <= tp.Approver_lock_end)
                {
                    //currently locked
                    lblApprLockStat.Text = "Approver Transactions Locked";
                }
                else
                {
                    if (tp.Approver_lock_start > today)
                    {
                        TimeSpan tsTimeLeft = tp.Approver_lock_start.Subtract(today);
                        lblApprLockStat.Text = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", tsTimeLeft.Days, tsTimeLeft.Hours, tsTimeLeft.Minutes, tsTimeLeft.Seconds);
                    }
                }
            }
        }
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        //if (Cache["cacheLockingInfo"] == null)
        //{
        //    TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
        //    Cache["cacheLockingInfo"] = tp;
        //    return tp;
        //}
        //else
        //{
        //    return (TransPeriod)Cache["cacheLockingInfo"];
        //}

        TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
        Cache["cacheLockingInfo"] = tp;
        return tp;
    }

    protected void btnLockedOk_Click(object sender, EventArgs e)
    {
        Session["kiosk"] = null;
        Response.Redirect("LoginEmployee.aspx");
    }
}
