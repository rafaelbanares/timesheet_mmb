<%@ Page Language="C#" MasterPageFile="~/AttendanceLogin.master" AutoEventWireup="true" CodeFile="LoginEmployee.aspx.cs" Inherits="Attendance_LoginEmployee"%>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script language="javascript" type="text/javascript">
    try{
        self.resizeTo(screen.width,screen.height); 
        self.moveTo(0,0);}
    catch(e){}
    </script>    
    <br />
    <table border="1" style="background-color:ghostwhite; border-width:1; border-color:dodgerblue">
     <tr>
      <td>        
        <asp:Label ID="lblSignin" runat="server" Text="Employee sign in" Font-Bold="true"></asp:Label>
        <hr />
        <table border="0" cellpadding="0" cellspacing="3">
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td align="right">
                    Employee ID:</td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" Width="150px" MaxLength="15"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right">
                    Password:</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="150px" MaxLength="15"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblCompany" runat="server" Text="Company:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlCompany" runat="server" Width="155px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td><td></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td align="right">
                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" />&nbsp;
                    <asp:Button ID="btnForgot" runat="server" Text="Forgot Password" 
                        onclick="btnForgot_Click" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtUsername"
                        ErrorMessage="- Employee ID is required"></asp:RequiredFieldValidator></td>
            </tr>
        </table>
      </td>
     </tr>
    </table>
    <asp:HiddenField ID="hfAttempt" runat="server" Value="0" />    
    <asp:LinkButton ID="lnkTimeMachine" runat="server" 
        onclick="lnkTimeMachine_Click" Visible="False" >Time In/Out</asp:LinkButton>&nbsp;&nbsp;
    <br />
    <asp:LinkButton ID="lnkTestAppr" runat="server" onclick="lnkTestAppr_Click" 
        Visible="False" ToolTip="Level1 test appr">L1</asp:LinkButton>&nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="lnkTestAppr2" runat="server" onclick="lnkTestAppr2_Click" 
        Visible="False" ToolTip="Level2 test appr">L2</asp:LinkButton>&nbsp;&nbsp;&nbsp; 
    <asp:LinkButton ID="lnkAdminMode" runat="server" Visible="False" onclick="lnkAdminMode_Click">admin</asp:LinkButton>&nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="lnkKioskMode" runat="server" Visible="False" onclick="lnkKioskMode_Click" >kiosk</asp:LinkButton>
</asp:Content>

