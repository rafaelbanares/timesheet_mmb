<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveApprover.aspx.cs" Inherits="Attendance_LeaveApprover" %>
<%@ Register src="EmpHeaderFilter.ascx" tagname="EmpHeaderFilter" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <script type="text/javascript" src="css/common_uno.js"></script>

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <uc1:EmpHeaderFilter ID="EmpHeader1" runat="server" />
    <hr />
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                    <asp:HiddenField ID="hfCompanyID" runat="server" Value='<%# Bind("CompanyID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litFullname" runat="server" Text='<%# Bind("FullName") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Filed" SortExpression="DateFiled">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "DateFiled"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date" SortExpression="StartDate">
                <ItemTemplate>
                    <asp:Literal ID="litStartDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "StartDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date" SortExpression="EndDate">
                <ItemTemplate>
                    <asp:Literal ID="litEndDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "EndDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="No. of days">
                <ItemTemplate>
                    <asp:Literal ID="litDays" runat="server" Text='<%# Bind("Days") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="Leave">
                <ItemTemplate>
                    <asp:Literal ID="litLeave" runat="server" Text='<%# Bind("Code") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Reason">
                <ItemTemplate>
                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="Stage">
                <ItemTemplate>
                    <asp:Literal ID="litStage" runat="server" Text='<%# Bind("Stage") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="btnFU"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
<asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none"  />
</asp:Content>



