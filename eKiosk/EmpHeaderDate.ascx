﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmpHeaderDate.ascx.cs" Inherits="EmpHeaderDate" %>

<script type="text/javascript">
function openEmpInfo(url) {
    popupWindow = wopen(url, "popupWindow", 820, 300);
    return false;
}
function openwindow() {
    popupWindow = wopen($get('<%= hfURL.ClientID %>').value, "popupWindow", 230, 260);
}
function defaultdates() {
    $get('<%=  hdDateSelection.ClientID %>').value = '';
    $get('<%=  Parent.FindControl("btnFU").ClientID %>').click();
}
function updatedates(sdate, edate) {
    $get('<%= hdDateStart.ClientID %>').value = sdate;
    $get('<%= hdDateEnd.ClientID %>').value = edate;

    $get('<%=  hdDateSelection.ClientID %>').value = 'filtered';
    $get('<%=  Parent.FindControl("btnFU").ClientID %>').click();

    closewindow(popupWindow);
}
function refresh() {
    $get('<%=  Parent.FindControl("btnFU").ClientID %>').click();
}
function canceldates()
{
}
</script>

<table width="100%">
  <tr>
  <td valign="top">
    <asp:LinkButton ID="lbEmpInfo" runat="server" Visible="False"><asp:Label ID="lblFullname" runat="server" Font-Bold="True"></asp:Label></asp:LinkButton>
  </td>
  <td align="left" valign="top">
    <asp:LinkButton ID="lnkFilter" runat="server" Visible="False">Select Date Filter</asp:LinkButton> &nbsp;&nbsp;
    <asp:LinkButton ID="lnkRefresh" runat="server" OnClientClick="refresh();return false;" Visible="False">Refresh List</asp:LinkButton>
  </td>
  </tr>
</table>
<asp:HiddenField ID="hfURL" runat="server" />
<asp:HiddenField ID="hdDateSelection" runat="server" />
<asp:HiddenField ID="hdDateStart" runat="server" />
<asp:HiddenField ID="hdDateEnd" runat="server" />


