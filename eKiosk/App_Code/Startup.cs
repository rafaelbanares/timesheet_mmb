﻿using BNS.TK.Business;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Owin;
using Owin;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Web;

[assembly: OwinStartup(typeof(Startup))]

public class Startup
{
    public void Configuration(IAppBuilder app)
    {
        GlobalConfiguration.Configuration
               .UseSqlServerStorage(ConfigurationManager.AppSettings.Get("connection2"));

        app.UseHangfireDashboard("/jobs", new DashboardOptions
        {
            IsReadOnlyFunc = (DashboardContext context) => false,
            Authorization = new[] { new MyAuthorizationFilter() },
            AppPath = VirtualPathUtility.ToAbsolute("~")
        });

        //http://localhost:61134/eTimeHome.aspx

        app.UseHangfireServer(new BackgroundJobServerOptions
        {
            HeartbeatInterval = new TimeSpan(0, 1, 0),
            ServerCheckInterval = new TimeSpan(0, 1, 0),
            SchedulePollingInterval = new TimeSpan(0, 1, 0)
        });

        RecurringJob.AddOrUpdate(
            () => ReprocessAttendance(), Cron.Hourly);
    }

    public void ReprocessAttendance()
    {
        var clientDb = Helper.Config.ClientDB();
        var companyId = Helper.Config.DefaultCompanyID();

        var dt = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Get("connection2"),
            CommandType.StoredProcedure, clientDb + ".dbo.usa_GetEmployeeKiosk",
            new SqlParameter[] {
                new SqlParameter("@CompanyID",  companyId),
                new SqlParameter("@EmployeeID", "")
            }).Tables[1];

        var startDate = DateTime.Parse(dt.Rows[0]["StartDate"].ToString());
        var endDate = DateTime.Parse(dt.Rows[0]["EndDate"].ToString());

        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

        ProcessAttend proc = new ProcessAttend(companyId, "0");
        proc.Process(startDate, endDate);
    }
}