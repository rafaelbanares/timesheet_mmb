using System;

namespace Bns.Attendance
{
    /// <summary>
    /// Summary description for TimeMachine
    /// </summary>
    abstract public class TimeMachine
    {
        string _swipeid;
        string _timetrans;
        string _io;
        string _station;
        string _remarks;

        public string SwipeID { get { return _swipeid; } set { _swipeid = value; } }
        public string Timetrans { get { return _timetrans; } set { _timetrans = value; } }
        public string IO { get { return _io; } set { _io = value; } }
        public string Station { get { return _station; } set { _station = value; } }
        public string Remarks { get { return _remarks; } set { _remarks = value; } }

        public virtual bool SetInfo(string s)
        {
            return true;
        }
    }

    public class TmBiometrics : TimeMachine
    {
        public override bool SetInfo(string s)
        {
            if (s.Length == 32)
            {
                SwipeID = s.Substring(0, 12);
                Timetrans = s.Substring(24, 2)  //month
                            + "/" + s.Substring(26, 2)  //day
                            + "/" + s.Substring(20, 4)  //year
                            + " " + s.Substring(12, 8); // time
                IO = s.Substring(29, 1);
                Station = "";
                Remarks = "";
                return true;
            }
            return false;
        }
    }

    public class TmStandard : TimeMachine
    {
        public override bool SetInfo(string s)
        {
            string[] trans = s.Split(',');
            if (trans.Length >= 3)
            {
                SwipeID = trans[0].Trim();
                Timetrans = trans[1] + " " + trans[2];
                IO = trans[3];
                Station = "";
                Remarks = "";
                return true;
            }
            return false;
        }
    }


}