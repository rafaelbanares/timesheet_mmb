﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.TK.Entities; 
/// <summary>
/// Summary description for CacheHelper
/// </summary>
public static class CacheHelper
{
    public static DataSet GetEmployeeLookup()
    {

        if (HttpContext.Current.Cache["EmployeeDS"] == null)
        {
            Employee emp = new Employee();
            emp.Query.AddResultColumn("EmployeeID");
            emp.Query.AddResultColumn("FullName");
            emp.Query.AddOrderBy("FullName", MMB.DataObject.WhereParameter.Dir.ASC);
            emp.Query.Load();

            HttpContext.Current.Cache["EmployeeDS"] = emp.ToDataSet();
        }
        return (DataSet)HttpContext.Current.Cache["EmployeeDS"];
    }
}