﻿using System;

[Serializable]
public struct Kiosk
{
    string _CompanyID;
    string _CompanyName;
    string _DB;
    string _UID;

    string _EmployeeID;
    string _LeaveApproverID;
    string _OTApproverID;

    string _EmployeeName;
    string _EmployeeName2;
    string _LeaveApproverName;
    string _OTApproverName;
    string _Levels;
    string _Position;

    string _EmailEmployee;
    string _EmailApproverOT;
    string _EmailCC;
    string _EmailBCC;

    string _Level1Data;
    string _Level2Data;
    string _Level3Data;
    string _Level4Data;

    bool _WorkForEnabled;
    string _WorkFor;
    string _WorkForSub;

    bool _IsApprover1;
    bool _IsAdmin;
    bool _OTExempt;
    bool _UTExempt;
    bool _TardyExempt;

    DateTime _StartDate;
    DateTime _EndDate;

    bool _ApproverRuleOR_OR;

    public string UID { get { return _UID; } set { _UID = value; } }
    public string CompanyID { get { return _CompanyID; } set { _CompanyID = value; } }
    public string CompanyName { get { return _CompanyName; } set { _CompanyName = value; } }
    public string DB { get { return _DB; } set { _DB = value; } }

    public string EmployeeID { get { return _EmployeeID; } set { _EmployeeID = value; } }
    public string LeaveApproverID { get { return _LeaveApproverID; } set { _LeaveApproverID = value; } }
    public string OTApproverID { get { return _OTApproverID; } set { _OTApproverID = value; } }

    public string EmployeeName { get { return _EmployeeName; } set { _EmployeeName = value; } }
    public string EmployeeName2 { get { return _EmployeeName2; } set { _EmployeeName2 = value; } }
    public string LeaveApproverName { get { return _LeaveApproverName; } set { _LeaveApproverName = value; } }
    public string OTApproverName { get { return _OTApproverName; } set { _OTApproverName = value; } }

    public string EmailEmployee { get { return _EmailEmployee; } set { _EmailEmployee = value; } }
    public string EmailApproverOT { get { return _EmailApproverOT; } set { _EmailApproverOT = value; } }
    public string EmailCC { get { return _EmailCC; } set { _EmailCC = value; } }
    public string EmailBCC { get { return _EmailBCC; } set { _EmailBCC = value; } }

    public string Levels { get { return _Levels; } set { _Levels = value; } }
    public string[] Level { get { return _Levels.Split('~'); } }
    public string Position { get { return _Position; } set { _Position = value; } }
    public string Level1Data { get { return _Level1Data; } set { _Level1Data = value; } }
    public string Level2Data { get { return _Level2Data; } set { _Level2Data = value; } }
    public string Level3Data { get { return _Level3Data; } set { _Level3Data = value; } }
    public string Level4Data { get { return _Level4Data; } set { _Level4Data = value; } }

    public bool WorkForEnabled { get { return _WorkForEnabled; } set { _WorkForEnabled = value; } }
    public string WorkFor { get { return _WorkFor; } set { _WorkFor = value; } }
    public string WorkForSub { get { return _WorkForSub; } set { _WorkForSub = value; } }

    public bool IsApprover1 { get { return _IsApprover1; } set { _IsApprover1 = value; } }
    public bool IsAdmin { get { return _IsAdmin; } set { _IsAdmin = value; } }
    public bool OTExempt { get { return _OTExempt; } set { _OTExempt = value; } }
    public bool UTExempt { get { return _UTExempt; } set { _UTExempt = value; } }
    public bool TardyExempt { get { return _TardyExempt; } set { _TardyExempt = value; } }

    public string DateFormat { get { return "MM/dd/yyyy"; ; } }

    public DateTime StartDate { get { return _StartDate; } set { _StartDate = value; } }
    public DateTime EndDate { get { return _EndDate; } set { _EndDate = value; } }
    public bool ApproverRuleOR_OR { get { return _ApproverRuleOR_OR; } set { _ApproverRuleOR_OR = value; } }
    public bool IsLocked { get; set; }
    public bool IsResigned { get; set; }
    public bool IsAdminMode { get; set; }
}
