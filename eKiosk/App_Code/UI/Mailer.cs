﻿using System;
using System.Data;
using System.Data.SqlClient;
using Bns.AttUtils;

using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Mailer
/// </summary>
public class Mailer
{
    /*
    protected string FromEmail;
    protected string FromName;
    protected string ToEmail;
    protected string ToName;
    protected string Cc;
    protected string Bcc;
    protected string Subject;
    protected string Body;

    public Mailer()
	{
	}
    */
}

//... class specific for mailing DTR approval
public sealed class MailerDTRApproval : Mailer
{
    public MailerDTRApproval()
	{

    }
    public void Compose(Kiosk kiosk, string conn, string maindb, string date, string in1, string out1, string newin1, string newout1, string reason)
    {
        string sApprove, sDecline;

        sApprove = "http://www.google.com";
        sDecline = "http://www.google.com";

        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, maindb + ".dbo.usa_Mail_DTR_Approval", new SqlParameter[]
            {
                new SqlParameter("@DBName", kiosk.DB)
                ,new SqlParameter("@CompanyID", kiosk.CompanyID)
                ,new SqlParameter("@EmployeeID", kiosk.EmployeeID)
                ,new SqlParameter("@EmployeeName", kiosk.EmployeeName)
                ,new SqlParameter("@EmailEmployee", kiosk.EmailEmployee)

                ,new SqlParameter("@OTApproverID", kiosk.OTApproverID)
                ,new SqlParameter("@OTApproverName", kiosk.OTApproverName )
                ,new SqlParameter("@EmailApproverOT", kiosk.EmailApproverOT)

                ,new SqlParameter("@Date", date)
                ,new SqlParameter("@in1", in1)
                ,new SqlParameter("@out1", out1)
                ,new SqlParameter("@newin1", newin1)
                ,new SqlParameter("@newout1", newout1)
                ,new SqlParameter("@reason", reason)
                ,new SqlParameter("@linkApprove", sApprove)
                ,new SqlParameter("@linkDecline", sDecline)

                ,new SqlParameter("@cc", kiosk.EmailCC )
                ,new SqlParameter("@bcc", kiosk.EmailBCC )
            }
        );
    }
}

