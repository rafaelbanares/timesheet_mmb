using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttUtils;
using System.Text.RegularExpressions;

namespace Bns.AttendanceUI
{
    /// <summary>
    /// Summary description for KioskPageUI
    /// </summary>
    public class KioskPageUI : System.Web.UI.Page
    {
        protected Kiosk _KioskSession;
        protected SecureUrl _Url;
        protected bool _OverwriteRender = true;
        protected string _ConnectionString = ConfigurationManager.AppSettings.Get("Connection");
        protected string _ConnectionString2 = ConfigurationManager.AppSettings.Get("connection2");
        protected string _ConnectionString3 = ConfigurationManager.AppSettings.Get("connection3");
        protected static readonly String MainDB = ConfigurationManager.AppSettings.Get("MasterDB"); 

        private static readonly Regex REGEX_BETWEEN_TAGS = new Regex(@">\s+<", RegexOptions.Compiled);
        private static readonly Regex REGEX_LINE_BREAKS = new Regex(@"\n\s+", RegexOptions.Compiled);

        protected PageAccessLevel AccessLevel { get; set; }

        protected void OverwriteRender(bool overwrite)
        {
            _OverwriteRender = overwrite;
        }

        protected void GetUserSession()
        {            
            object session = (object)Session["kiosk"];

            if (session == null)
                Response.Redirect("LoginEmployee.aspx");

            _KioskSession = (Kiosk)session;

            //rsb quickfix
            if (session != null && _KioskSession.IsLocked)
                Response.Redirect("LoginEmployee.aspx");

            //access level
            if (_KioskSession.IsAdminMode)
            {
                if (AccessLevel == PageAccessLevel.etime || AccessLevel == PageAccessLevel.All) { }
                else
                    Response.Redirect("LoginEmployee.aspx?admin=1");
            }
            else
            {
                if (AccessLevel == PageAccessLevel.ekiosk || AccessLevel == PageAccessLevel.All) { }
                else
                {
                    Response.Redirect("LoginEmployee.aspx");
                }
            }
        }

        protected void SetUserSession(Kiosk kiosk)
        {
            Session["kiosk"] = kiosk;
        }

        protected Kiosk AssignUserSession(DataSet ds, bool isAdminMode)
        {

            string clientdb = Helper.Config.ClientDB();
            string approverrule = Helper.Config.ApproverRule();
            
            DataRowCollection rows = ds.Tables[0].Rows;
            Kiosk kiosk = new Kiosk();

            kiosk.EmployeeID = rows[0]["EmployeeID"].ToString();
            kiosk.CompanyID = rows[0]["CompanyID"].ToString();
            kiosk.CompanyName = rows[0]["CompanyName"].ToString();
            kiosk.DB = clientdb;

            kiosk.UID = rows[0]["EmployeeID"].ToString();
            kiosk.LeaveApproverID = rows[0]["LeaveApproverID"].ToString().Trim();
            kiosk.OTApproverID = rows[0]["OTApproverID"].ToString().Trim();

            kiosk.EmployeeName = rows[0]["EmployeeName"].ToString();
            kiosk.EmployeeName2 = rows[0]["EmployeeName"].ToString();
            //kiosk.EmployeeName2 = rows[0]["FirstName"].ToString().ToPascalCase() + " " + rows[0]["LastName"].ToString().ToPascalCase();
            kiosk.LeaveApproverName = rows[0]["LeaveApproverName"].ToString();
            kiosk.OTApproverName = rows[0]["OTApproverName"].ToString();
            kiosk.Position = rows[0]["Position"].ToString();

            kiosk.EmailEmployee = rows[0]["EmailEmployee"].ToString();
            kiosk.EmailApproverOT = rows[0]["EmailApproverOT"].ToString();
            kiosk.EmailCC = rows[0]["DefaultCC"].ToString();
            kiosk.EmailBCC = rows[0]["DefaultBCC"].ToString();

            kiosk.Level1Data = rows[0]["Level1Data"].ToString();
            kiosk.Level2Data = rows[0]["Level2Data"].ToString();
            kiosk.Level3Data = rows[0]["Level3Data"].ToString();
            kiosk.Level4Data = rows[0]["Level4Data"].ToString();
            kiosk.Levels = rows[0]["CompanyLevels"].ToString();

            kiosk.WorkForEnabled = (bool)rows[0]["WorkForEnabled"];
            kiosk.WorkFor = rows[0]["WorkFor"].ToString();
            kiosk.WorkForSub = rows[0]["WorkForSub"].ToString();

            kiosk.IsAdmin = ((int)rows[0]["UserLevel"]) > 0;
            kiosk.IsApprover1 = ((int)ds.Tables[2].Rows[0][0]) > 0;
            kiosk.ApproverRuleOR_OR = (approverrule == "OR-OR");
            kiosk.OTExempt = (bool)rows[0]["OTExempt"];
            kiosk.UTExempt = (bool)rows[0]["UTExempt"];
            kiosk.TardyExempt = (bool)rows[0]["TardyExempt"];

            if (ds.Tables[1].Rows.Count > 0)
            {
                kiosk.StartDate = (DateTime)ds.Tables[1].Rows[0]["StartDate"];
                kiosk.EndDate = (DateTime)ds.Tables[1].Rows[0]["EndDate"];
                if (kiosk.EndDate >= DateTime.Today)
                    kiosk.EndDate = DateTime.Today.AddDays(-1);
            }
            else
            {
                kiosk.StartDate = DateTime.Today;
                kiosk.EndDate = DateTime.Today;
            }
            
            kiosk.IsResigned = (!Convert.IsDBNull(rows[0]["DateTerminated"]) && Convert.ToDateTime(rows[0]["DateTerminated"]) <= DateTime.Today);
            kiosk.IsAdminMode = isAdminMode;

            return kiosk;
        }


        protected virtual DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
        {
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    else
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                }
                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            _Url = new SecureUrl(Request.Url.PathAndQuery);
        }

        protected string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        protected string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }
        protected string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

    }

    public enum PageAccessLevel
    {
        All=0, ekiosk, etime
    }
}
