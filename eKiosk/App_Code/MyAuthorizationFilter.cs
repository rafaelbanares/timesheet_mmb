﻿using Hangfire.Dashboard;
using Microsoft.Owin;

/// <summary>
/// Summary description for MyAuthorizationFilter
/// </summary>
public class MyAuthorizationFilter : IDashboardAuthorizationFilter
{
    public MyAuthorizationFilter()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public bool Authorize(DashboardContext context)
    {
        var owinContext = new OwinContext(context.GetOwinEnvironment());

        return owinContext.Authentication.User.Identity.IsAuthenticated;

    }
}