﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using MMB.Core;
using BNS.TK.Entities;   

/// <summary>
/// Summary description for Helper
/// </summary>
public static class Helper
{
    public static class Config
    {
        public static string DefaultCompanyID()
        {
            string defaultcompany = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");
            if (defaultcompany.IsFilled())
            {
                string[] details = defaultcompany.Split('~');
                return details[0];
            }
            return "";
        }

       
        public static string ClientDB()
        {
            string defaultcompany = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");
            if (defaultcompany.IsFilled())
            {
                string[] details = defaultcompany.Split('~');
                return details[1];
            }
            return "";
        }


        public static string DefaultCompanyName()
        {
            string defaultcompany = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");
            if (defaultcompany.IsFilled())
            {
                string[] details = defaultcompany.Split('~');
                return details[2];
            }
            return "";
        }

        public static string ApproverRule()
        {
            string defaultcompany = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultCompany");
            if (defaultcompany.IsFilled())
            {
                string[] details = defaultcompany.Split('~');
                return details[3];
            }
            return "";
        }

        public static string MasterDB()
        {
            return System.Configuration.ConfigurationManager.AppSettings.Get("MasterDB");
        }


    }

    public static SmtpInfos GetConnectionInfos()
    {
        SmtpInfos smtpInfo = new SmtpInfos();
        try
        {            
            if (ConfigurationManager.AppSettings.Get("SmtpInfo") != null)
            {
                string smtp = MMB.Core.AES_Encyption.Decrypt(ConfigurationManager.AppSettings.Get("SmtpInfo"));
                string[] s = smtp.Split('|');
                smtpInfo.SmtpHost = s[0];
                smtpInfo.Port = s[1].ToInteger();
                smtpInfo.UserName = s[2];
                smtpInfo.Password = s[3];
            }
            
            return smtpInfo;
        }
        catch
        {
            throw new Exception("Smtp configuration error.");
        }
    }

    public static void SendMail(string sendTo, string subject, string body)
    {
        SendMail(sendTo, subject, body, null, null);
    }

    public static void SendMail(string sendTo, string subject, string body, string cc)
    {
        SendMail(sendTo, subject, body, cc, null);
    }

    public static void SendMail(string sendTo, string subject, string body, string cc, string[] attachments)
    {
        //if (sendTo.IsNullOrEmpty()) return;

        //try
        //{
        //    SmtpInfos smtpInfo = GetConnectionInfos();
        //    string emailSender = ConfigurationManager.AppSettings.Get("EmailSender");

        //    SmtpHelper.Send(smtpInfo, emailSender, sendTo, cc, subject, body, attachments);
        //}
        //catch(Exception ex)
        //{
        //    AppException exc = new AppException();
        //    exc.AddNew();
        //    exc.Exception = ex.Message;
        //    exc.CreateDate = DateTime.Now;
        //    exc.Save(); 
        //}
        
    }
    public static string GetEmailAddress(string companyid, string employeeid)
    {
        Employee emp = new Employee();
        string sql = "select Email from Employee where CompanyID = {0} and EmployeeID = {1} ";
        object[] parameters = { companyid, employeeid };
        emp.DynamicQuery(sql, parameters); 
        
        if (emp.RowCount == 0) return "";
        
        return emp.Email;
    }

    public static string GetApproverEmailAddress(string companyid, string employeeid)
    {
        Employee emp = new Employee();
        string sql =
            "select " +
            "	approver1Email = (select Email from Employee where EmployeeID = b.approver1), " +
            "	approver2Email = (select Email from Employee where EmployeeID = b.approver2), " +
            "	approver3Email = (select Email from Employee where EmployeeID = b.approver3), " +
            "	approver4Email = (select Email from Employee where EmployeeID = b.approver4) " +
            "from Employee a inner join EmployeeApprover b " +
            "on a.CompanyID = b.CompanyID and " +
            "	a.EmployeeID = b.EmployeeID " +
            "where  " +
            "	a.CompanyID = {0} and  " +
            "	a.EmployeeID = {1} ";

        object[] parameters = { companyid, employeeid };
        emp.DynamicQuery(sql, parameters); 

        if (emp.RowCount == 0) return "";

        string email = "";
        if (!Convert.IsDBNull(emp.GetColumn("approver1Email")) && !emp.GetColumn("approver1Email").ToString().IsNullOrEmpty())
            email += emp.GetColumn("approver1Email").ToString();

        if (!Convert.IsDBNull(emp.GetColumn("approver2Email")) && !emp.GetColumn("approver2Email").ToString().IsNullOrEmpty())
            email += ";" + emp.GetColumn("approver2Email").ToString();

        if (!Convert.IsDBNull(emp.GetColumn("approver3Email")) && !emp.GetColumn("approver3Email").ToString().IsNullOrEmpty())
            email += ";" + emp.GetColumn("approver3Email").ToString();

        if (!Convert.IsDBNull(emp.GetColumn("approver4Email")) && !emp.GetColumn("approver4Email").ToString().IsNullOrEmpty())
            email += ";" + emp.GetColumn("approver4Email").ToString();

        return email;
    }
}