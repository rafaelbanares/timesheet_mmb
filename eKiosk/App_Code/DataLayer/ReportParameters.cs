using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace BNS.Attendance.DataObjects
{
    public class ReportParameters
    {
        public ReportParameters() { }

        public String ReportName
        {
            get { return _reportName; }
            set { _reportName = value; }
        }
        public String CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public String ReportTitle
        {
            get { return _rptTitle; }
            set { _rptTitle = value; }
        }

        public String ReportSubTitle1
        {
            get { return _rptSubTitle1; }
            set { _rptSubTitle1 = value; }
        }

        public String ReportSubTitle2
        {
            get { return _rptSubTitle2; }
            set { _rptSubTitle2 = value; }
        }

        public String ReportSubTitle3
        {
            get { return _rptSubTitle3; }
            set { _rptSubTitle3 = value; }
        }
        public String EmployeeID
        {
            get
            {
                return _employeeID;
            }
            set { _employeeID = value; }
        }

        public String Status
        {
            get
            {
                return _status;
            }
            set { _status = value; }
        }

        public String DB
        {
            get { return _DB; }
            set { _DB = value; }
        }
        public String GroupBy1
        {
            get { return _group1; }
            set { _group1 = value; }
        }
        public String GroupBy2
        {
            get { return _group2; }
            set { _group2 = value; }
        }
        public String SortBy
        {
            get { return _sortby; }
            set { _sortby = value; }
        }
        public String UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }


        public String TranStart
        {
            get { return _tranStart; }
            set { _tranStart = value; }
        }
        public String TranEnd
        {
            get { return _tranEnd; }
            set { _tranEnd = value; }
        }

        public String Level1
        {
            get { return _level1; }
            set { _level1 = value; }
        }
        public String Level2
        {
            get { return _level2; }
            set { _level2 = value; }
        }
        public String Level3
        {
            get { return _level3; }
            set { _level3 = value; }
        }
        public String Level4
        {
            get { return _level4; }
            set { _level4 = value; }
        }

        public String WorkFor
        {
            get { return _workfor; }
            set { _workfor = value; }
        }
        public String WorkForSub
        {
            get { return _workforsub; }
            set { _workforsub = value; }
        }
        public String ProjID
        {
            get { return _projid; }
            set { _projid = value; }
        }

        public String Export
        {
            get { return _export; }
            set { _export = value; }
        }
        public bool AllCompanies
        {
            get { return _allCompanies; }
            set { _allCompanies = value; }
        }
        public bool IsExcel
        {
            get { return _IsExcel; }
            set { _IsExcel = value; }
        }


        private String _tranStart;
        private String _tranEnd;

        private String _level1;
        private String _level2;
        private String _level3;
        private String _level4;
        private String _workfor;
        private String _workforsub;
        private String _projid;

        private String _export = "";

        private String _reportName = "";
        private String _companyName = "";
        private String _rptTitle = "";
        private String _rptSubTitle1 = "";
        private String _rptSubTitle2 = "";
        private String _rptSubTitle3 = "";
        private String _group1 = "";
        private String _group2 = "";
        private String _sortby = "";
        private String _employeeID;
        private String _status;
        private String _DB = "";
        private String _userID;

        private bool _IsExcel;
        private bool _allCompanies = false;
    }
}