using System;

namespace BNS.Attendance.DataObjects
{
    public class PayslipParameters
    {
        public PayslipParameters() { }

        public string CompanyName { get; set; }

        public string Department { get; set; }

        public DateTime CutOffFrom { get; set; }

        public DateTime CutOffTo { get; set; }

        public DateTime PeriodFrom { get; set; }

        public DateTime PeriodTo { get; set; }

        public string ReportName { get; set; }

        public string ReportTitle { get; set; }

        public string CompanyID { get; set; }

        public string EmployeeID { get; set; }

        public string OutTables { get; set; }

        public int ApplYear { get; set; }

        public DateTime TranDate { get; set; }
    }
}