using System;
using System.Collections.Generic;
using System.Text;

namespace BNS.Attendance.DataObjects
{
    public static class Utils
    {
        public static string nullIfEmpty(string s)
        {
            if (s == null || s.TrimEnd() == "") return null;
            return s;
        }
        public static string emptyIfNull(object s)
        {
            if (s == DBNull.Value) return "";
            return s.ToString();
        }
        public static decimal zeroIfNull(object s)
        {
            if (s == DBNull.Value) return 0;
            return (decimal)s;
        }
    }
}
