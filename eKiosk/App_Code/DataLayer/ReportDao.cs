using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;



namespace BNS.Attendance.DataObjects
{
    public class ReportDao
    {
        #region Constructor
        public ReportDao(string clientDB, string companyID)
        {
            _ConnectionString = Db.ConnectionString2;
            _ConnectionString3 = Db.ConnectionString3;
            _clientDB = clientDB;
            _companyID = companyID;
        }
        #endregion

        public List<DataSet> GetPayslipData(int reportID, PayslipParameters payParams)
        {
            var dsPayslipData = new List<DataSet>();
            DataSet dsReportInfo = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                                    "usa_GetReportParameters",
                                    new SqlParameter[] { new SqlParameter("@reportID", reportID),
                                                         new SqlParameter("@debug", 0)});

            if (dsReportInfo.Tables[0].Rows.Count > 0)
            {
                var spName = Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["MainSProc"]);
                var dsMainData = SqlHelper.ExecuteDataset(
                    _ConnectionString3, CommandType.StoredProcedure, spName, 
                    new SqlParameter[] {
                        new SqlParameter("@CompanyID", payParams.CompanyID),
                        new SqlParameter("@EmployeeID", payParams.EmployeeID),
                        new SqlParameter("@TranDate", payParams.TranDate),
                        new SqlParameter("@ApplYear", payParams.ApplYear),
                        new SqlParameter("@OutTables", string.Empty)
                    });

                RenameDataTables(dsMainData, Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["MainTableNames"]));
                dsPayslipData.Add(dsMainData);

                spName = Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["SubSProc"]);
                var dsSubData = SqlHelper.ExecuteDataset(
                    _ConnectionString3, CommandType.StoredProcedure, spName,
                    new SqlParameter[]
                    {
                        new SqlParameter("@CompanyID", payParams.CompanyID),
                        new SqlParameter("@EmployeeID", payParams.EmployeeID),
                        new SqlParameter("@PayrollNo", dsMainData.Tables[2].Rows[0]["PayrollNo"]),
                        new SqlParameter("@OutTables", string.Empty)

                    });

                RenameDataTables(dsSubData, Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["SubTableNames"]));
                dsPayslipData.Add(dsSubData);
            }

            return dsPayslipData;
        }

        public List<DataSet> GetReportData(int reportID, ReportParameters reportParams)
        {
            List<DataSet> dsReportData = new List<DataSet>();
            // ds[0] : reports table row            
            // ds[1] : main sp parameters   (optional)            
            // ds[2] : sub sp parameters    (optional)
            DataSet dsReportInfo = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure,
                                    "usa_GetReportParameters",
                                    new SqlParameter[] { new SqlParameter("@reportID", reportID), 
                                                         new SqlParameter("@debug", 0)});
            if (dsReportInfo.Tables[0].Rows.Count == 0) return dsReportData;

            string spName = Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["MainSProc"]);
            //DataSet dsMainData = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, Db.MasterDB + ".dbo." + spName, GetSqlParams(dsReportInfo.Tables[1], reportParams));
            DataSet dsMainData = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, spName, GetSqlParams(dsReportInfo.Tables[1], reportParams));
            RenameDataTables(dsMainData, Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["MainTableNames"]));
            dsReportData.Add(dsMainData);
            dsReportData.Add(null);

            /* for deletion for reports with sub reports
            if (dsReportInfo.Tables.Count >= 2)  // main report sp
            {
                string spName = Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["MainSProc"]);
                DataSet dsMainData = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, Db.MasterDB + ".dbo." + spName, GetSqlParams(dsReportInfo.Tables[1], reportParams));
                RenameDataTables(dsMainData, Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["MainTableNames"]));
                dsReportData.Add(dsMainData);
            }
            else
            {
                // custom loading of main data
                if (reportID == 31)                                      //Journal Voucher
                    dsReportData.Add(GetJVData(reportParams));
                else if (reportID == 18)                               // Alphalist of employees with no previous employer within this year
                    dsReportData.Add(GetAlphaNoPrevData(reportParams));
                else if (reportID == 25)
                    dsReportData.Add(GetAlphaTerminatedData(reportParams)); //Alphalist of terminated employees
                else
                    dsReportData.Add(null);
            }

            if (dsReportInfo.Tables.Count >= 3)  // subreport sp
            {
                string spName = Utils.emptyIfNull(dsReportInfo.Tables[3].Rows[0]["SubSProc"]);
                DataSet dsSubData = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, Db.MasterDB + ".dbo." + spName, GetSqlParams(dsReportInfo.Tables[2], reportParams));
                RenameDataTables(dsSubData, Utils.emptyIfNull(dsReportInfo.Tables[0].Rows[0]["SubTableNames"]));
                dsReportData.Add(dsSubData);
            }
            else
            {
                // custom loading of subreport data
                if (reportID == 2)                                      //Payroll Register (Detailed)
                    dsReportData.Add(GetPayregDtlSubreportData(reportParams));
                else if (reportID == 14)
                    dsReportData.Add(GetPayslipSubreportData(reportParams)); //Payslip
                else
                    dsReportData.Add(null);
            }
            */

            return dsReportData;
        }

        #region RenameDataTables
        private void RenameDataTables(DataSet ds, string tableNames)
        {
            if (tableNames.TrimEnd() == "") return;
            string[] tns = tableNames.Split(',');
            for (int i = 0; i < tns.Length; i++)
            {
                ds.Tables[i].TableName = tns[i];
            }
        }
        #endregion
        #region FreeDataTable
        private DataTable FreeDataTable(DataSet ds, string tableName)
        {
            DataTable table = ds.Tables[tableName];
            ds.Tables.Remove(tableName);
            return table;
        }
        #endregion
        #region GetSqlParams
        private SqlParameter[] GetSqlParams(DataTable dtParams, ReportParameters repParam)
        {
            if (dtParams.Rows.Count == 0) return null;
            SqlParameter[] sqlParam = new SqlParameter[dtParams.Rows.Count];
            string paramName;
            Object paramValue;

            for (int i = 0; i < dtParams.Rows.Count; i++)
            {
                paramName = dtParams.Rows[i][0].ToString();
                switch (paramName.TrimEnd().ToUpper())
                {
                    case "@COMPANYID":
                        paramValue = _companyID;
                        break;
                    case "@EMPLOYEEID":
                        paramValue = repParam.EmployeeID;
                        break;
                    case "@STATUS":
                        paramValue = repParam.Status;
                        break;
                    case "@USERID":
                        paramValue = repParam.UserID;
                        break;
                    case "@TRANSTART":
                        paramValue = repParam.TranStart;
                        break;
                    case "@TRANEND":
                        paramValue = repParam.TranEnd;
                        break;

                    case "@LEVEL1":
                        paramValue = repParam.Level1;
                        break;
                    case "@LEVEL2":
                        paramValue = repParam.Level2;
                        break;
                    case "@LEVEL3":
                        paramValue = repParam.Level3;
                        break;
                    case "@LEVEL4":
                        paramValue = repParam.Level4;
                        break;
                    case "@WORKFORID":
                        paramValue = repParam.WorkFor;
                        break;
                    case "@WORKFORSUBID":
                        paramValue = repParam.WorkForSub;
                        break;
                    case "@PROJID":
                        paramValue = repParam.ProjID;
                        break;

                    case "@ISALLCOMPANY":
                        paramValue = repParam.AllCompanies;
                        break;
                    case "@DEBUG":
                        paramValue = 0;
                        break;
                    case "@DB":
                        paramValue = repParam.DB;
                        break;
                    default:
                        paramValue = null;
                        break;
                } //select
                sqlParam[i] = new SqlParameter(paramName, paramValue);
            } //endif

            return sqlParam;
        }
        #endregion

        private string _ConnectionString;
        private string _ConnectionString3;
        private string _clientDB;
        private string _companyID;
    }
}

