﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DateRange
/// </summary>
public class DateRange
{
    #region CTOR
    public DateRange()
    {
        //default values
        minimum = DateTime.Parse("1/1/1900");
        maximum = DateTime.Parse("06/06/2079 23:59");
    }
    #endregion


    public DateTime minimum { get; set; }
    public DateTime maximum { get; set; }
    public string message { get; private set; }

    public bool ValidatePeriod(string sfrom, string sto)
    {
        DateTime dt;
        
        if (sfrom == "")
        {
            message = "Please enter starting period";
            return false;
        }
        if (sto == "")
        {
            message = "Please enter ending period";
            return false;
        }
        if (!DateTime.TryParse(sfrom, out dt))
        {
            message = "Invalid starting period";
            return false;
        }
        if (!DateTime.TryParse(sto, out dt))
        {
            message = "Invalid ending period";
            return false;
        }
        //at this point we have a syntactically correct dates
        //validate range values
        DateTime dfrom = DateTime.Parse(sfrom);
        DateTime dto = DateTime.Parse(sto);

        if (dfrom > dto)
        {
            message = "Invalid date range";
            return false;
        }
        if (dfrom < minimum)
        {
            message = "Invalid starting period";
            return false;
        }
        if (dto > maximum)
        {
            message = "Starting period should not exceed ending period";
            return false;
        }

        return true;
    }
}
