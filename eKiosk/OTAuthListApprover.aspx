<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="OTAuthListApprover.aspx.cs" Inherits="Attendance_OTAuthListApprover" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Overtime Approval"></asp:Label><br />
    <br />
    Select overtime authorization category:
    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ControlDefaults">
        <asp:ListItem>No Filter</asp:ListItem>
        <asp:ListItem Selected="True">For Approval</asp:ListItem>
        <asp:ListItem>Approved</asp:ListItem>
        <asp:ListItem>Declined</asp:ListItem>
    </asp:DropDownList><br />
    <asp:GridView ID="gvLAppr" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle">
        <Columns>
            <asp:BoundField DataField="EmployeeID" HeaderText="Employee ID" />
            <asp:BoundField DataField="FullName" HeaderText="Name" />
            <asp:BoundField DataField="DateFiled" HeaderText="Date filed" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" />
            <asp:BoundField DataField="OTDate" HeaderText="OT date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" />
            
            <asp:BoundField DataField="OTStart" HeaderText="OT start" DataFormatString="{0:HH:mm tt}" HtmlEncode="False" />
            <asp:BoundField DataField="OTEnd" HeaderText="OT end" DataFormatString="{0:HH:mm tt}" HtmlEncode="False" />
            
            <asp:BoundField DataField="ApprovedOT" HeaderText="Approved hours" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Reason" HeaderText="Reason" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:BoundField DataField="ApproverName" HeaderText="Approver" />
            
            <asp:TemplateField>
                <ItemStyle VerticalAlign="Top" />
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" OnClick="ibEdit_Click" />
                    <asp:HiddenField ID="hfID" runat="server" Value='<%# Bind("ID") %>' />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>
</asp:Content>

