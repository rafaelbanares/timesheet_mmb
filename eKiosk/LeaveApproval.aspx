<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="LeaveApproval.aspx.cs" Inherits="Attendance_LeaveApproval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Leaves Approval"></asp:Label><br />
    <br />
    <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname, MI"></asp:Label><br />
    <asp:HiddenField ID="hfLeaveID" runat="server" />
    <br />
    <table class="TableBorder1">
        <tr>
            <td align="right">
                Date filed:</td>
            <td>
                <asp:TextBox ID="txtDateFiled" runat="server" CssClass="TextBoxDate" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Date start:</td>
            <td>
                <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator id="mevDateStart" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateStart" 
                    ControlExtender="meeDateStart" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" Display="None" >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Date end:</td>
            <td>
                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                    ToolTip="Click to choose date" />
                <ajaxToolkit:MaskedEditValidator id="mevDateEnd" runat="server" InvalidValueBlurredMessage="*"
                    InvalidValueMessage="Cannot accept invalid value" ControlToValidate="txtDateEnd" 
                    ControlExtender="meeDateEnd" ErrorMessage="*" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank" 
                    IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date" Display="None" >*
                </ajaxToolkit:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Number of days:</td>
            <td>
                <asp:TextBox ID="txtLeaveDays" runat="server" CssClass="ControlDefaults" Width="40px">1.00</asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Leave:</td>
            <td>
                <asp:TextBox ID="txtLeaveCode" runat="server" CssClass="ControlDefaults" Width="64px" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right">
                Reason for leave:</td>
            <td>
                <asp:TextBox ID="txtReason" runat="server" CssClass="ControlDefaults" Height="74px"
                    MaxLength="100" TextMode="MultiLine" Width="501px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReason"
                    ErrorMessage="Reason for leave cannot be blank">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
            <td>
                &nbsp;<asp:Button ID="btnApproved" runat="server" CssClass="ControlDefaults" OnClick="btnApproved_Click"
                    Text="Approve" />
                <asp:Button ID="btnDeclined" runat="server" CssClass="ControlDefaults" OnClick="btnDeclined_Click"
                    OnClientClick="return confirm('Are you sure to decline this leave?')" Text="Decline" />
                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="ControlDefaults"
                    OnClick="btnClose_Click" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsum" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <br />
    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeLeaveDays" runat="server" ErrorTooltipEnabled="True"
        InputDirection="RightToLeft" Mask="99.99" MaskType="Number" MessageValidatorTip="true"
        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtLeaveDays">
    </ajaxToolkit:MaskedEditExtender>
    <asp:HiddenField ID="hfEmployeeID" runat="server" />
</asp:Content>

