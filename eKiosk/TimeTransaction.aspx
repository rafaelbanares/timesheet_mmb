<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="TimeTransaction.aspx.cs" Inherits="TimeTransaction" Title="Daily Time Transaction" %>

<%@ Register src="DefaultDate.ascx" tagname="DefaultDate" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
	<script type="text/javascript" src="css/common_uno.js"></script>
	<script type="text/javascript">
	function ValidateGrp(grp) {
		Page_ClientValidate(grp);
	}

	//close popup on esc
	function pageLoad(sender, args) {
		if (!args.get_isPartialLoad()) {
			$addHandler(document, "keydown", onKeyDown);
		}
	}
	function onKeyDown(e) {
		if (e && e.keyCode == Sys.UI.Key.esc) {
			$find('ctl00_phmc_mpe').hide();
		}
	} 
	</script>
	<asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="Daily Time Record"></asp:Label>   
	<br />
	<br />
	<table cellpadding="0" cellpadding="0" style="width:99%;">
		<tr style="vertical-align:top">
			<td><uc2:DefaultDate ID="dfd1" runat="server" />&nbsp;</td>
			<td>&nbsp;</td>
			<td>
                <asp:Button ID="btnVerify" runat="server" OnClick="btnVerify_Click" Text="Verify" OnClientClick="return confirm('Are you sure you want to verify your DTR?');" />
                &nbsp;&nbsp;<asp:LinkButton ID="lnkPrev" runat="server" onclick="lnkPrev_Click">Prev</asp:LinkButton>
                &nbsp;&nbsp;<asp:LinkButton ID="lnkNext" runat="server" onclick="lnkNext_Click">Next</asp:LinkButton></td>
		</tr>
	</table>
	<hr />
    <br />
	<asp:UpdatePanel ID="updPnl1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
		<div style="min-height:300px">
			<asp:GridView ID="gvEmployees" runat="server" AllowPaging="True" 
				AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="31"
				Width="95%" onrowdatabound="gvEmployees_RowDataBound" 
				onpageindexchanging="gvEmployees_PageIndexChanging">
				<Columns>
					<asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Width="100px" HtmlEncode="False" ></asp:BoundField>
					<%--<asp:BoundField DataField="shiftname" HeaderText="Shift<br>Code" HtmlEncode="False" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" ></asp:BoundField>--%>
					
					<%--<asp:BoundField DataField="totalreghrs" HeaderText="Total<br>Reg Hrs" HtmlEncode="False" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" ></asp:BoundField>--%>
					<%--<asp:BoundField DataField="restdaycode" HeaderText="RestDay" HtmlEncode="False" ItemStyle-Width="0px" Visible="false" ></asp:BoundField>--%>
					<asp:BoundField DataField="sin1" HeaderText="Shift<br>Time IN" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="100px" HtmlEncode="False" ></asp:BoundField>
					<asp:BoundField DataField="sout2" HeaderText="Shift<br>Time OUT" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="100px" HtmlEncode="False" ></asp:BoundField>
					<asp:TemplateField HeaderText="Day" ItemStyle-Width="50px">
						<ItemTemplate >
							<asp:Label ID="lblRstDay" runat="server" ></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="in1" HeaderText="Actual<br>Time IN" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="110px" HtmlEncode="False" ></asp:BoundField>
					<asp:BoundField DataField="out1" HeaderText="Actual<br>Time OUT" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="110px" HtmlEncode="False" ></asp:BoundField>
					<%--<asp:BoundField DataField="lateut" HeaderText="Late / UT" HtmlEncode="False" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" ></asp:BoundField>--%>
					<%--<asp:BoundField DataField="restdaycode" HeaderText="Rest<br>Day" ItemStyle-Width="40px" HtmlEncode="False" ></asp:BoundField>--%>
					<%--<asp:BoundField DataField="otstart" HeaderText="Actual<br> OT Time IN" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="110px" HtmlEncode="False" ></asp:BoundField>
					<asp:BoundField DataField="otend" HeaderText="Actual<br>OT Time OUT" DataFormatString="{0:hh:mm tt}" ItemStyle-Width="110px" HtmlEncode="False" ></asp:BoundField>
					<asp:BoundField DataField="approvedot" HeaderText="Total<br>OT Hrs" HtmlEncode="False" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" ></asp:BoundField>--%>
					<%--<asp:BoundField DataField="regnd1" HeaderText="Total<br>ND Hrs" HtmlEncode="False" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" ></asp:BoundField>--%>
					<asp:TemplateField HeaderText="Remarks">
						<ItemTemplate >
							<asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="Verified" HeaderText="Verified" HtmlEncode="False" ></asp:BoundField>

					<%--<asp:BoundField DataField="Stage" HeaderText="" HtmlEncode="False" Visible="false" ></asp:BoundField>--%>
<%--                    <asp:TemplateField HeaderText="Verified">
						<ItemTemplate >
							<asp:Label ID="lblStage" runat="server" Text='<%# Bind("Stage") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>--%>
					<asp:BoundField DataField="Status" HeaderText="Status" HtmlEncode="False" Visible="true" ItemStyle-Width="80px" ></asp:BoundField>                
					<asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit_small.gif" OnClick="ibEdit_Click" ToolTip="Request" />
							<asp:HiddenField ID="hfDate" runat="server" Value='<%# Bind("Date") %>' />
							<asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
							<asp:HiddenField ID="hfChanged" runat="server" Value='<%# Bind("Changed") %>' />
						</ItemTemplate>
						<ItemStyle Width="50px"></ItemStyle>
					</asp:TemplateField>
				</Columns>
				<RowStyle Height="22px" />
				<EmptyDataTemplate>
					<span style="color:Red">No record found</span>
				</EmptyDataTemplate>
				<PagerStyle HorizontalAlign="Right" />
				<HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
			</asp:GridView>
			<asp:Button ID="btnFU" runat="server" Text="" onclick="btnFU_Click" style="display:none" />
			<br />
		</div>
		<hr />
		<table>
			<tr align='left'>
				<td><strong>Legend:</strong></td>
				<td style="width:20px; height:22px; background-color:#90ee90"></td><td style="width:120px">
				- Leave</td>
				<td style="width:20px; height:22px; background-color:#f4a460"></td><td style="width:120px">- For Approval</td>
				<td style="width:20px; height:22px; background-color:#819FF7"></td><td style="width:180px">
				- Invalid</td>
			</tr>
			<tr>
				<td colspan='7'>
				</td>
			</tr>
			<tr align='left'>
				<td></td>
				<td style="width:20px; height:22px; background-color:#F3F781"></td><td>- Absent</td>
				<td style="width:20px; height:22px; background-color:#c0c0c0"></td><td>- Holiday / Restday</td>
				<td></td><td>&nbsp;</td>
			</tr>
		</table>
		<br />
	   
	   </ContentTemplate>
   </asp:UpdatePanel>
	
	<%--------------------------------------------------------------- modal popup layout starts here -----------------------------%>
	<div id="pnlDtrEntry" style="width: 600px; display: none;" >    
		<div class="popup_Container">
			<div class="popup_Titlebar" id="PopupHeader">
				<div class="TitlebarLeft">Attendance Entries</div>
				<div class="TitlebarRight" onclick="$get('ctl00_phmc_btnPopCancel').click();"> </div>
			</div>
			<div class="popup_Body" style="width:98%;" >
				<asp:UpdatePanel runat="server" ID="UpdatePanel2">
				<ContentTemplate>
				<asp:HiddenField ID="hdDatePop" runat="server" />
				<asp:Label ID="lblPopErrorMsg" runat="server" ForeColor="Red"></asp:Label>
				<table style="border-color: #FFFFFF; width:95%; " border="0" border="1" cellpadding="1" cellspacing="1">
					<tr>
						<td colspan="2" style="width:100%; height:25; text-align:center; background-color:#4682b4; color:White;">
							Actual IN/OUT</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>IN</td>
						<td>
							<asp:Label ID="txtOrig_In" runat="server" Font-Bold="true" Width="150px"></asp:Label>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>OUT</td>
						<td>
							<asp:Label ID="txtOrig_Out" runat="server" Font-Bold="true" Width="150px"></asp:Label>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="width:100%; height:25; text-align:center; background-color:#4682b4; color:White;">
							Requested IN/OUT</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>IN</td>
						<td>
							<asp:TextBox ID="txtNewDateIn" runat="server" AutoPostBack="False" 
								OnChange="ValidateGrp('DtrEntryGrp');" ValidationGroup="DtrEntryGrp" Width="70px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="meNewDateIn" runat="server" 
								ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewDateIn">
							</ajaxToolkit:MaskedEditExtender>
							<asp:TextBox ID="txtNewTimeIn" runat="server" AutoPostBack="False" 
								ValidationGroup="DtrEntryGrp" Width="60px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="meNewIn" runat="server" AcceptAMPM="True" 
								ErrorTooltipEnabled="True" Mask="99:99" MaskType="Time" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewTimeIn">
							</ajaxToolkit:MaskedEditExtender>
							<ajaxToolkit:MaskedEditValidator ID="mveNewIn" runat="server" 
								ControlExtender="meNewIn" ControlToValidate="txtNewTimeIn" Display="Dynamic" 
								EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" 
								InvalidValueMessage="Time is invalid" ValidationGroup="DtrEntryGrp">
							</ajaxToolkit:MaskedEditValidator>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td>OUT</td>
						<td>
							<asp:TextBox ID="txtNewDateOut" runat="server" AutoPostBack="False" 
								OnChange="ValidateGrp('DtrEntryGrp');" ValidationGroup="DtrEntryGrp" Width="70px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" 
								ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewDateOut">
							</ajaxToolkit:MaskedEditExtender>
							<asp:TextBox ID="txtNewTimeOut" runat="server" AutoPostBack="False" 
								ValidationGroup="DtrEntryGrp" Width="60px"></asp:TextBox>
							<ajaxToolkit:MaskedEditExtender ID="meNewOut" runat="server" AcceptAMPM="True" 
								ErrorTooltipEnabled="True" Mask="99:99" MaskType="Time" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
								OnInvalidCssClass="MaskedEditError" TargetControlID="txtNewTimeOut">
							</ajaxToolkit:MaskedEditExtender>
							<ajaxToolkit:MaskedEditValidator ID="mveNewOut" runat="server" 
								ControlExtender="meNewOut" ControlToValidate="txtNewTimeOut" Display="Dynamic" 
								EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" 
								InvalidValueMessage="Time is invalid" ValidationGroup="DtrEntryGrp">
							</ajaxToolkit:MaskedEditValidator>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td valign="top">Remarks&nbsp;&nbsp;</td>
						<td>
							<asp:TextBox ID="txtNewRemarks" runat="server" CssClass="ControlDefaults" 
								Height="58px" MaxLength="100" TextMode="MultiLine" Width="417px" 
								ValidationGroup="DtrEntryGrp"></asp:TextBox>
							<asp:RequiredFieldValidator ID="rfvNewRem" runat="server" 
								ControlToValidate="txtNewRemarks" ErrorMessage="required" 
								ValidationGroup="DtrEntryGrp"></asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr style=" background-color: #d3d3d3">
						<td valign="top">
							Status</td>
						<td>
							<asp:Label ID="txtStatus" runat="server" Font-Bold="False" Width="150px"></asp:Label>
						</td>
					</tr>
				</table>
				</ContentTemplate>
				</asp:UpdatePanel>
			</div>
			<hr style="color: #add8e6"/>
			<div class="popup_Buttons">
				<asp:Button ID="btnEditSave" runat="server" Text="Save" OnClientClick="ValidateGrp('DtrEntryGrp')" onclick="btnEditSave_Click"/>
				<asp:Button ID="btnPopCancel" runat="server" Text="Cancel" />

			</div>
		</div>
	</div>
	<%--------------------------------------------------------------- modal popup layout ends -----------------------------%>        


	<%--------------------------------------------------------------- modal popup layout starts here -----------------------------%>
	<div id="pnlDtrEntry2" style="width: 600px; display: none;" >    
		<div class="popup_Container">
			<div class="popup_Titlebar" id="PopupHeader2">
				<div class="TitlebarLeft">Attendance Entries</div>
				<div class="TitlebarRight" onclick="$get('ctl00_phmc_btnPopCancel').click();"> </div>
			</div>
			<div class="popup_Body" style="width:98%;" >
				<asp:UpdatePanel runat="server" ID="UpdatePanel1">
					<ContentTemplate>
						<asp:Label ID="lblPopErrorMsg2" runat="server" ForeColor="Red"></asp:Label>
						<table style="border-color: #FFFFFF; width:95%; " border="0" cellpadding="1" cellspacing="1">
							<tr>
								<td colspan="2" style="width:100%; text-align:center; background-color:#4682b4; color:White;">
									RAW IN/OUT</td>
							</tr>
							<tr style=" background-color: #d3d3d3">
								<td>IN</td>
								<td>
									<asp:Label ID="lblRaw1" runat="server" Font-Bold="true" Width="150px"></asp:Label>
								</td>
							</tr>
							<tr style=" background-color: #d3d3d3">
								<td>OUT</td>
								<td>
									<asp:Label ID="lblRaw2" runat="server" Font-Bold="true" Width="150px"></asp:Label>
								</td>
							</tr>
						</table>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
	</div>
	<%--------------------------------------------------------------- modal popup layout ends -----------------------------%>      

   <asp:LinkButton ID="lbEditInOut" runat="server"></asp:LinkButton>       
	<ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" 
		BackgroundCssClass="ModalPopupBG"
		CancelControlID="btnPopCancel" 
		TargetControlID="lbEditInOut"
		PopupControlID="pnlDtrEntry" 
		Drag="true" 
		PopupDragHandleControlID="PopupHeader">
	</ajaxToolkit:ModalPopupExtender>

	<asp:LinkButton ID="lbEditInOut2" runat="server"></asp:LinkButton>  
	<ajaxToolkit:ModalPopupExtender ID="mpe2" runat="server" 
		BackgroundCssClass="ModalPopupBG"
		CancelControlID="btnPopCancel" 
		TargetControlID="lbEditInOut2"
		PopupControlID="pnlDtrEntry2" 
		Drag="true" 
		PopupDragHandleControlID="PopupHeader">
	</ajaxToolkit:ModalPopupExtender>

</asp:Content>

