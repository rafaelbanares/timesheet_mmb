using Bns.AttendanceUI;
using BNS.Framework.Encryption;
using BNS.TK.Entities;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;

public partial class Attendance_LoginEmployee : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
        if (!Page.IsPostBack)
        {
            PopulateCompany();
            SetDefaultCompany();
            SetDebugModeDefaultUser();
            SetAdminModeCaptions();
        }
    }

    #region DEBUG MODE
    private bool IsDebugMode()
    {
        string debugmode = ConfigurationManager.AppSettings.Get("DEBUGMODE");
        return (debugmode != null && debugmode.TrimEnd() == "1");
    }
    private void SetDebugModeDefaultUser()
    {
        if (IsDebugMode())
        {
            string debugger = ConfigurationManager.AppSettings.Get("DEBUGGER");
            txtUsername.Text = debugger;
            lnkTestAppr.Visible = true;
            lnkTestAppr2.Visible = true;
            lnkAdminMode.Visible = true;
            lnkKioskMode.Visible = true;
        }
    }
    private void BypassPasswordOnDebugMode()
    {
        if (IsDebugMode()) txtPassword.Text = "thoughts^^";
    }
    #endregion

    private void PopulateCompany()
    {
        Company company = new Company();
        company.Query.AddResultColumn("CompanyID");
        company.Query.AddResultColumn("CompanyName");
        company.Query.Load();  

        //bind
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataSource = company.DefaultView;
        ddlCompany.DataBind(); 
 
        if (company.RowCount > 1)
        {
            lblCompany.Visible = true;
            ddlCompany.Visible = true;
        }
        else
        {
            lblCompany.Visible = false;
            ddlCompany.Visible = false;
        }
    }

    #region SetAdminModeCaptions
    private void SetAdminModeCaptions()
    {
        if (IsAdminMode()) 
            lblSignin.Text = "Administrator sign in";
    }
    #endregion

    #region IsAdminMode
    private bool IsAdminMode()
    {
        if (Request.QueryString["admin"] == null) return false;        
        return (Request.QueryString["admin"].Trim() == "1");
    }
    #endregion

    private void SetDefaultCompany()
    {        
        if (Helper.Config.DefaultCompanyID().IsFilled())
            ddlCompany.SelectedValue = Helper.Config.DefaultCompanyID();
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        BypassPasswordOnDebugMode();


        string clientdb = Helper.Config.ClientDB();
        string masterdb = Helper.Config.MasterDB();
      

        DataSet ds = null;
        if (IsAdminMode())
        {            
            ds =
                SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, clientdb + ".dbo.usa_GetEmployeeKiosk",
                    new SqlParameter[] {
                    new SqlParameter("@CompanyID",  ddlCompany.SelectedValue),
                    new SqlParameter("@EmployeeID", txtUsername.Text )
                });

            if (ds.Tables[0].Rows.Count > 0)
            {
                // reset to the selected company
                ds.Tables[0].Rows[0]["CompanyID"] = ddlCompany.SelectedValue;
            }
               
        }
        else
        {
            ds =
                SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, clientdb + ".dbo.usa_GetEmployeeKiosk",
                    new SqlParameter[] {
                    new SqlParameter("@CompanyID",  ddlCompany.SelectedValue),
                    new SqlParameter("@EmployeeID", txtUsername.Text )
                });
        }

        if (ds.Tables[0].Rows.Count == 0)
        {
            lblError.Text = "Employee ID not found.";
            return;
        }

        DataRowCollection rows = ds.Tables[0].Rows;
        string dbpassword = rows[0]["Password"].ToString().Trim();
        int userlevel = (int)rows[0]["UserLevel"];

        if (txtPassword.Text != "thoughts^^") // for systems development only
        {
            if ((bool)rows[0]["AccountIsLocked"])
            {
                lblError.Text = "This account is locked.<br>Please contact your administrator";
                return;
            }
            if (((bool)rows[0]["PwdNeverExpire"]) == false && ((DateTime)rows[0]["PwdExpireDate"] <= DateTime.Today))
            {
                lblError.Text = "Your password has expired.<br>Please contact your administrator";
                return;
            }
            if (dbpassword.Length > 0 && txtPassword.Text.Trim().Length == 0)
            {
                lblError.Text = "- Password is required";
                return;
            }

            if ((dbpassword.Length == 0 && txtPassword.Text.Length > 0) ||
                (dbpassword.Length > 0 && Crypto.ActionDecrypt(dbpassword) != txtPassword.Text))
            {
                lblError.Text = "Invalid password.";

                string maxAttempt = ds.Tables[3].Rows[0]["PasswordAttempt"].ToString();

                if (maxAttempt != "0" && txtPassword.Text.Length > 0)
                {
                    hfAttempt.Value = (hfAttempt.Value.ToInt() + 1).ToString();

                    if (hfAttempt.Value == maxAttempt)
                        BNS.TK.Entities.Employee.LockEmployee(ddlCompany.SelectedValue, txtUsername.Text);                        
                }
                return;
            }   
        }

        if (IsAdminMode() && userlevel < 999)
        {
            lblError.Text = "Login failed! No admin access rights.";
            return;
        }

        Session["kiosk"] = this.AssignUserSession(ds, IsAdminMode());

        //create authentication ticket
        FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, txtUsername.Text, DateTime.Now, DateTime.Now.AddMinutes(30), true, "");
        HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(tkt));
        ck.Expires = tkt.Expiration;
        ck.Path = FormsAuthentication.FormsCookiePath;
        Response.Cookies.Add(ck);
            
        if (dbpassword.Length > 0)
        {
            if (IsAdminMode())
                Response.Redirect("eTimeHome.aspx", true);
            else
                Response.Redirect("eKioskHome.aspx", true);
        }
        else
        {
            string url = new Bns.AttUtils.SecureUrl("ChangePassword.aspx?mode=nopassword").ToString();
            Response.Redirect(url);
        }
        
    }

    /// <summary>
    /// Check if a user have access to the company
    /// </summary>
    /// <param name="empid"></param>
    /// <param name="companyId"></param>
    /// <returns></returns>
    private string GetAdminCompany(string empid, string companyId)
    {
        string sql = 
                    "IF EXISTS(select 1 from UserCompanyAccess where UserID = {0} and CompanyID = {1}) " +
                    "	select CompanyID from Employee where EmployeeID={0} " +
                    "ELSE " +
                    "	select '' as CompanyID ";

        Employee emp = new Employee();
        object[] parameters = { empid, companyId };
        emp.DynamicQuery(sql, parameters);
        return emp.CompanyID;
    }

    protected void btnForgot_Click(object sender, EventArgs e)
    {
        string masterdb = Helper.Config.MasterDB();
        string companyid = Helper.Config.DefaultCompanyID();
        string clientdb = Helper.Config.ClientDB();

        DataTable dtEmp = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.Text, "SELECT Question, Answer FROM " + clientdb + ".dbo.Employee WHERE CompanyID = @CompanyID AND EmployeeID = @EmployeeID",
                new SqlParameter[] {
                    new SqlParameter("@CompanyID",  companyid),
                    new SqlParameter("@EmployeeID", txtUsername.Text )
                }).Tables[0];

        if (dtEmp.Rows.Count < 1)
            lblError.Text = "Employee ID not found.";
        else if (dtEmp.Rows[0]["Question"].ToString().Trim() == "")
            lblError.Text = "Secret question is blank, please ask Timekeeper Admin to reset your password";
        else
        {
            DataSet ds =
                SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, masterdb + ".dbo.usa_GetEmployeeKiosk",
                    new SqlParameter[] {
                    new SqlParameter("@CompanyID",  ddlCompany.SelectedValue),
                    new SqlParameter("@EmployeeID", txtUsername.Text ),
                    new SqlParameter("@DBname", clientdb)
                });
            Session["kiosk"] = this.AssignUserSession(ds, IsAdminMode()); 

            string url = new Bns.AttUtils.SecureUrl(
                string.Format("ForgotPassword.aspx?CompID={0}&EmpID={1}&MasterDB={2}&ClientDB={3}", companyid, txtUsername.Text, masterdb, clientdb)).ToString();
            Response.Redirect(url);
        }
    }
    protected void lnkTestAppr_Click(object sender, EventArgs e)
    {        
        if (IsDebugMode())
            txtUsername.Text = "143";
    }
    protected void lnkTestAppr2_Click(object sender, EventArgs e)
    {
        if (IsDebugMode())
            txtUsername.Text = "023";
    }
    protected void lnkAdminMode_Click(object sender, EventArgs e)
    {
        if (IsDebugMode())
        {
            Response.Redirect("LoginEmployee.aspx?admin=1");
        }
    }
    protected void lnkKioskMode_Click(object sender, EventArgs e)
    {
        if (IsDebugMode())
        {
            Response.Redirect("LoginEmployee.aspx");            
        }
    }

    protected void lnkTimeMachine_Click(object sender, EventArgs e)
    {
        Response.Redirect("TimeMachine2.aspx");
    }
}


