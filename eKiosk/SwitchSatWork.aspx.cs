﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using System.Text;

public partial class SwitchSatWork : KioskPageUI
{
    DateTime satdate;
    String satdisplay;
    String satswitch;

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        satdate = DateTime.Parse(_Url["Date"]);
        satdisplay = _Url["SatDisplay"].ToString();
        satswitch = _Url["SatSwitch"].ToString();

        if (!IsPostBack)
        {
            lbFrom.Text = satdisplay;
            lbTo.Text = satswitch;
        }
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        SqlParameter[] spParams = new SqlParameter[] {
            new SqlParameter("@CompanyID", _KioskSession.CompanyID)
            ,new SqlParameter("@Cascade", chkCascade.Checked )
            ,new SqlParameter("@StartSat", satdate )
            ,new SqlParameter("@NoWork", lbTo.Text.Trim().ToLower() == "no work" )
            ,new SqlParameter("@updatedby", _KioskSession.UID )
            };
        SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usp_CompSaturday_Switch", spParams);
        ClientScript.RegisterStartupScript(typeof(Page), "done", @"window.opener.refresh();alert('Changes were saved');window.close();", true);
    }
}
