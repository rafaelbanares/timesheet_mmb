﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="OTApproverList.aspx.cs" Inherits="OTApproverList" Title="Overtime Approver List" %>
<%@ Register src="EmpHeaderFilter.ascx" tagname="EmpHeaderFilter" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
    <uc1:EmpHeaderFilter ID="EmpHeader1" runat="server" />
    <hr />
        <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
    <asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" 
            OnRowUpdating="gvEmp_RowUpdating" Width="100%" 
            DataKeyNames="id,status" onrowdeleting="gvEmp_RowDeleting" 
            onselectedindexchanged="gvEmp_SelectedIndexChanged" 
            onrowcommand="gvEmp_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                    <asp:HiddenField ID="hfCompanyID" runat="server" Value='<%# Bind("CompanyID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litFullname" runat="server" Text='<%# Bind("FullName") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="OT Date" SortExpression="OTDate">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "OTDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Day">
                <ItemTemplate>
                    <asp:Literal ID="litDay" runat="server" Text='<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "OTDate").ToString()).DayOfWeek.ToString().Substring(0,3).ToLower() %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="OT Start">
                <EditItemTemplate>
                    <asp:TextBox ID="tbin1" runat="server" Text='<%# Bind("OTStart", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1" ControlToValidate="tbin1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbin1">
                    </ajaxToolkit:MaskedEditExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblIn1" runat="server" Text='<%# Bind("OTStart", "{0:hh:mm tt}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="OT End">
                <EditItemTemplate>
                    <asp:TextBox ID="tbout1" runat="server" Text='<%# Bind("OTEnd", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="tbout1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbout1">
                    </ajaxToolkit:MaskedEditExtender>                    
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblOut1" runat="server" Text='<%# Bind("OTEnd", "{0:hh:mm tt}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="No. of hours">
                <EditItemTemplate>
                    <asp:TextBox ID="tbApprovedOT" runat="server" CssClass="ControlDefaults" Width="40px" ValidationGroup="Trans" Text='<%# Bind("ApprovedOT") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litApprovedOT" runat="server" Text='<%# Bind("ApprovedOT") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason">
                <EditItemTemplate>
                    <asp:TextBox ID="tbReason" runat="server" Text='<%# Bind("Reason") %>' 
                        Width="100px" MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReason" runat="server" 
                        ControlToValidate="tbreason" 
                        ErrorMessage="Please indicate a reason for corrrections" 
                        ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Remarks" SortExpression="Remarks">
                <ItemTemplate>
                    <asp:Literal ID="litRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status" SortExpression="Stage">
                <ItemTemplate>
                    <asp:Literal ID="litStage" runat="server" Text='<%# Bind("Stage") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle Width="62px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibApproved" runat="server" ToolTip="Approve" 
                        ImageUrl="~/Graphics/check.gif" 
                        OnClientClick="return window.confirm('Approve Overtime?' );" 
                        CommandName="Select" />
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                    <asp:ImageButton ID="ibDeny" runat="server" ToolTip="Decline" 
                        ImageUrl="~/Graphics/delete.gif"                         
                        OnClientClick="return window.confirm('Decline Overtime?' );" 
                        CommandName="Delete" />
                    <asp:ImageButton ID="ibRecall" runat="server" CommandName="recall" ToolTip="Recall" ImageUrl="~/Graphics/cancel.gif" OnClientClick="return window.confirm('Recall Leave?' );" />
                    <asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <br />
    <br />
    <table>
        <tr align='left'>
            <td style="width:220px">
                <asp:Button ID="btnForApproval" runat="server" 
                    Text="Approve all - For Approval" Width="220px" 
                    onclick="btnForApproval_Click" Visible="False" /></td>
            <td style="width:50px"><asp:Button ID="Button1" runat="server" Text="Button" style="display:none" /></td>
        </tr>
    </table>

    <asp:Panel ID="pnlDecline" runat="server" Width="400px" Style="display:none" CssClass="modalPopup">
        <div><br />
            <table style="width:100%;" cellpadding="1" cellspacing="1">
                <tr>
                    <td>Reason</td>
                    <td>
                        <asp:TextBox ID="txtDecReason" runat="server" CssClass="ControlDefaults" 
                            Height="65px" MaxLength="100" TextMode="MultiLine" 
                            ToolTip="" Width="340px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDecReason" runat="server" ErrorMessage="required" ControlToValidate="txtDecReason" ValidationGroup="declinepop"></asp:RequiredFieldValidator>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2" style="width:100%; text-align:center">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    <br />
                        <asp:Button ID="btnDecline" runat="server" 
                            OnClientClick="ValidateGrp('declinepop')" Text="Decline" 
                            onclick="btnDecline_Click" />
                        &nbsp;
                        <asp:Button ID="btnDecReasonCancel" runat="server" Text="Cancel" />                  
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpe3" runat="server"
      TargetControlId="Button1"
      PopupControlID="pnlDecline"
      CancelControlID="btnDecReasonCancel"
      BackgroundCssClass="modalBackground"  />
      
        <asp:HiddenField ID="hfdecRowid" runat="server" />
    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="btnFU"></asp:AsyncPostBackTrigger>
    </Triggers>    
</asp:UpdatePanel>

<asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none" />

</asp:Content>

