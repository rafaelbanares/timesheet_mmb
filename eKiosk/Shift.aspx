<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="Shift.aspx.cs" Inherits="Shift" Title="MMB Kronos - Shift Definitions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript">
	function ValidateGrp(grp) {
		Page_ClientValidate(grp);
	}

	//close popup on esc
	function pageLoad(sender, args) {
		if (!args.get_isPartialLoad()) {
			$addHandler(document, "keydown", onKeyDown);
		}
	}
	function onKeyDown(e) {
		if (e && e.keyCode == Sys.UI.Key.esc) {
			$find('ctl00_phmc_mpe').hide();
		}
	}

	function UpdateImg(ctrl,imgsrc) {
		var img = $get(ctrl);
		img.src = imgsrc;
	}

	function toggle(isVisible)
	{
		if (isVisible)
		{
			setTimeout("UpdateImg('<%=imgProcess.ClientID%>','Graphics/indicator.gif');",50);            
			$get("<%=lblStatus.ClientID%>").innerHTML = "Uploading... please wait...";
			$get("<%=imgProcess.ClientID%>").style.display = "";
		}
		else
		{
			$get("<%=lblStatus.ClientID%>").innerHTML = "";
			$get("<%=imgProcess.ClientID%>").style.display = "none";
		}
	}
</script>
	<asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
		Font-Underline="False" Text="Shift Definitions"></asp:Label><br />
	<br />
	<div style="width:100%">
		<div style="display:inline-block;width:45%">
	<asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
		ToolTip="New Shift Code" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
			Font-Bold="True" OnClick="lbNew_Click">New Shift Code</asp:LinkButton>
		</div>
		<div align="right" style="display:inline-block;width:50%;">
			<asp:LinkButton ID="lbUpload" runat="server" CssClass="ControlDefaults" Font-Bold="True" OnClick="lbUpload_Click">Upload Shift Code</asp:LinkButton>
		</div>
	</div><br />
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
				OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
				OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" Width="100%" DataKeyNames="ShiftCode">
				<HeaderStyle CssClass="DataGridHeaderStyle" />
				<Columns>
				
					<asp:TemplateField HeaderText="Description">
						<EditItemTemplate>
							<asp:TextBox ID="txtDescription" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Description") %>' MaxLength="30" Width="190px"></asp:TextBox>
						</EditItemTemplate>                        
						<ItemTemplate>
							<div style="width:190px">
							<asp:Literal ID="litDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Literal><div>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="In1">
						<EditItemTemplate>
							<asp:TextBox ID="txtin1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("in1") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevin1" runat="server" Display="Dynamic" 
								ValidationGroup="Trans" ControlToValidate="txtin1" ControlExtender="mein1" EmptyValueMessage="*" IsValidEmpty="False" InvalidValueMessage="*" /><ajaxToolkit:MaskedEditExtender ID="mein1" runat="server"  ErrorTooltipEnabled="True" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
								TargetControlID="txtin1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litin1" runat="server" Text='<%# Bind("in1") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Out1">
						<EditItemTemplate>
							<asp:TextBox ID="txtout1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("out1") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevout1" runat="server" EmptyValueBlurredText="*"
								InvalidValueMessage="*" Display="Dynamic" 
								ValidationGroup="Trans" ControlToValidate="txtout1" ControlExtender="meout1" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="meout1" runat="server"  ErrorTooltipEnabled="True" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
								TargetControlID="txtout1" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litout1" runat="server" Text='<%# Bind("out1") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="In2">
						<EditItemTemplate>
							<asp:TextBox ID="txtin2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("in2") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevin2" runat="server"
								InvalidValueMessage="*" Display="Dynamic" 
								ValidationGroup="Trans" ControlToValidate="txtin2" ControlExtender="mein2" EmptyValueMessage="*" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="mein2" runat="server"  ErrorTooltipEnabled="True" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
								TargetControlID="txtin2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litin2" runat="server" Text='<%# Bind("in2") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Out2">
						<EditItemTemplate>
							<asp:TextBox ID="txtout2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("out2") %>'></asp:TextBox><ajaxToolkit:MaskedEditValidator id="mevout2" runat="server" InvalidValueBlurredMessage="*" Display="Dynamic" 
								ValidationGroup="Trans" ControlToValidate="txtout2" ControlExtender="meout2" EmptyValueMessage="*" IsValidEmpty="False" /><ajaxToolkit:MaskedEditExtender ID="meout2" runat="server"  ErrorTooltipEnabled="True" 
								MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
								TargetControlID="txtout2" Mask="99:99" MaskType="Time" AcceptAMPM="True" />
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litout2" runat="server" Text='<%# Bind("out2") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Grace Period&lt;br&gt;(minutes)">
						<EditItemTemplate>                            
							<asp:TextBox ID="txtgracein1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("grace_in1") %>'></asp:TextBox>
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litgracein1" runat="server" Text='<%# Bind("grace_in1") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Grace&lt;br&gt;period 2" Visible="false" HeaderStyle-Width="0px">
						<EditItemTemplate>
							<asp:TextBox ID="txtgracein2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("grace_in2") %>'></asp:TextBox>
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litgracein2" runat="server" Text='<%# Bind("grace_in2") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Min OT<br>(minutes)">
						<EditItemTemplate>
							<asp:TextBox ID="txtminot" runat="server" CssClass="ControlDefaults" Width="24px" Text='<%# Bind("minimumot") %>'></asp:TextBox>
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litminot" runat="server" Text='<%# Bind("minimumot") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>
					
					<asp:TemplateField HeaderText="Flexi<br>Shift?">
						<EditItemTemplate>
							<asp:CheckBox ID="chkFlexi" runat="server" />
						</EditItemTemplate>
						<ItemTemplate>
							<asp:Image ID="imgFlexi" runat="server" ImageUrl="~/Graphics/check.gif" />
						</ItemTemplate>                
						<ItemStyle HorizontalAlign="Center" />                
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Flexible&lt;br&gt; (minutes)">
						<EditItemTemplate>                            
							<asp:TextBox ID="txtFlexiin1" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("flexi_in1") %>'></asp:TextBox>
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litFlexiin1" runat="server" Text='<%# Bind("flexi_in1") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Flexi&lt;br&gt;period 2" Visible="false">
						<EditItemTemplate>
							<asp:TextBox ID="txtFlexiin2" runat="server" CssClass="ControlDefaults" Width="56px" Text='<%# Bind("flexi_in2") %>'></asp:TextBox>
						</EditItemTemplate>                        
						<ItemTemplate>
							<asp:Literal ID="litFlexiin2" runat="server" Text='<%# Bind("flexi_in2") %>'></asp:Literal>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Action">
						<ItemStyle Width="42px" />
						<EditItemTemplate>
							<asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
							<asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
						</EditItemTemplate>
						<ItemTemplate>
							<asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
							<asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" />
						</ItemTemplate>
					</asp:TemplateField>
					
					

				</Columns>
			</asp:GridView>
			<asp:HiddenField ID="hfIsAdd" runat="server" />
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="ibNew" />
			<asp:AsyncPostBackTrigger ControlID="lbNew" />
		</Triggers>
	</asp:UpdatePanel>
	<%--------------------------------------------------------------- modal popup layout starts here -----------------------------%>
	<div id="pnlUpload" style="width: 600px; display: none;" >    
		<div class="popup_Container">
			<div class="popup_Titlebar" id="PopupHeader">
				<div class="TitlebarLeft">Upload Shift Code</div>
				<div class="TitlebarRight" onclick="$get('ctl00_phmc_btnPopCancel').click();"> </div>
			</div>
			<div class="popup_Body" style="width:98%;" >
				<asp:UpdatePanel runat="server" ID="UpdatePanel2">
				<ContentTemplate>
				<asp:HiddenField ID="hdDatePop" runat="server" />
				<asp:Label ID="lblPopErrorMsg" runat="server" ForeColor="Red"></asp:Label>
					 <table>
						<tr>
							<%--<td><asp:Label ID="Label2" runat="server" Text="Excel File"></asp:Label></td>--%>
							<td>
								<asp:FileUpload ID="fuWorkfile" runat="server" CssClass="ControlDefaults" Width="499px" /><br /><br />                                
								<asp:Label ID="lblStatus" runat="server"></asp:Label>
								<asp:Image ID="imgProcess" runat="server" ImageUrl="~/Graphics/indicator.gif" style="display:none;" /></td>
							</td>
						</tr>                          
					</table>               
					<br />
					<asp:Label ID="lblSummary" runat="server"></asp:Label><br />
				</ContentTemplate>
				</asp:UpdatePanel>
			</div>
			<hr style="color: #add8e6"/>
			<div class="popup_Buttons">
				<asp:Button ID="btnUpload" runat="server" Text="Upload" OnClientClick="toggle(true);" onclick="btnUpload_Click"/>
				<asp:Button ID="btnPopCancel" runat="server" Text="Close" />

			</div>
		</div>
	</div>
	<%--------------------------------------------------------------- modal popup layout ends -----------------------------%>     
	<asp:LinkButton ID="lbPopupUpload" Text = "" runat="server"></asp:LinkButton>
	<ajaxToolkit:ModalPopupExtender ID="mpe" runat="server"
		TargetControlId="lbUpload" 
		PopupControlID="pnlUpload"
		CancelControlID="btnPopCancel"
		Drag="true"
		PopupDragHandleControlID="PopupHeader"
		BackgroundCssClass="modalBackground"  />
</asp:Content>