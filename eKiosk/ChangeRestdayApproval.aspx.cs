﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Bns.AttendanceUI;
using Bns.AttUtils;
using MMB.Core;
using BNS.TK.Entities;
using BNS.TK.Business;


public partial class ChangeRestdayApproval : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
            EmpHeader1.ApproverNo = ApproverNo();
        }
    }

    private string ApproverNo()
    {
        if (_Url["approverno"] == null)
            return "";
        else
            return _Url["approverno"];
    }

    private void BindGrid()
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.DataBind();
    }
    private DataSet GetData()
    {
        string sdate = "01/01/" + EmpHeader1.Year;
        string edate = "12/31/" + EmpHeader1.Year;

        string empWhere = "";
        if (ApproverNo() == "4")
            empWhere = " AND cr.EmployeeID <> {4} AND cr.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver4 = {4}) ";
        else if (ApproverNo() == "3")
            empWhere = " AND cr.EmployeeID <> {4} AND cr.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver3 = {4}) ";
        else if (ApproverNo() == "2")
            empWhere = " AND cr.EmployeeID <> {4} AND cr.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver2 = {4}) ";
        else if (ApproverNo() == "1")
            empWhere = " AND cr.EmployeeID <> {4} AND cr.EmployeeID IN (SELECT EmployeeID FROM EmployeeApprover WHERE CompanyID = {0} AND Approver1 = {4}) ";

        //status filter
        string status = EmpHeader1.Display.ToLower();
        if (EmpHeader1.Display.ToLower() == TKWorkFlow.Status.ForApproval.ToLower() && (ApproverNo() == "1" || ApproverNo() == "2"))
            empWhere += " AND cr.Stage = 0 ";
        else if (EmpHeader1.Display.ToLower() == TKWorkFlow.Status.ForApproval.ToLower() && (ApproverNo() == "3" || ApproverNo() == "4"))
            empWhere += " AND cr.Stage = 1 ";


        string sql = "SELECT  " +
                        "   cr.CompanyID, " +
                        "	cr.ID,  " +
                        "	cr.EmployeeID,  " +
                        "	e.FullName,  " +
                        "	cr.CreatedDate as DateFiled,  " +
                        "	cr.StartDate,  " +
                        "	cr.EndDate,  " +
                        "	cr.RestDayCode,  " +
                        "	'' as RestdayDescr,  " +
                        "	cr.[Status],  " +
                        "	cr.Stage,  " +
                        "   cr.Reason  " +
                        "FROM ChangeRestDay cr INNER JOIN Employee e  " +
                        "ON cr.CompanyID=e.CompanyID AND cr.EmployeeID=e.EmployeeID " +
                        "WHERE  " +
                        "	cr.CompanyID = {0} AND " +
                        "	(cr.StartDate <= {2} AND cr.EndDate >= {1} ) AND " +
                        "	({3}='' or cr.[Status] = {3}) " + empWhere +
                        "   AND e.FullName like {5} " +
                        "ORDER BY cr.EmployeeID, cr.StartDate DESC ";

        object[] parameters = { _KioskSession.CompanyID, 
                                  sdate.ToDate(), 
                                  edate.ToDate(), 
                                  (EmpHeader1.Display.IsNullOrEmpty() || EmpHeader1.Display.ToLower()=="all") ? "" : EmpHeader1.Display.TrimEnd(), 
                                  _KioskSession.EmployeeID,
                                  EmpHeader1.Search + "%" };

        BNS.TK.Entities.ChangeRestDay cr = new BNS.TK.Entities.ChangeRestDay();
        cr.DynamicQuery(sql, parameters);

        return cr.ToDataSet();
    }

   

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex != gvEmp.EditIndex)
            {
                Label lblreason = e.Row.FindControl("lblreason") as Label;
                Literal lblRestdayDescr = e.Row.FindControl("litRestdayDescr") as Literal;
                lblRestdayDescr.Text = DateHelper.DecodeDOW(DataBinder.Eval(e.Row.DataItem, "RestDayCode").ToString().TrimEnd());
                
                lblreason.ToolTip = lblreason.Text.TrimEnd();
                lblreason.Text = lblreason.Text.TrimEnd().ToEllipse(20);

                ImageButton ibRecall = e.Row.FindControl("ibRecall") as ImageButton;
                ImageButton ibApproved = e.Row.FindControl("ibApproved") as ImageButton;
                ImageButton ibDeny = e.Row.FindControl("ibDeny") as ImageButton;
                ImageButton ibLocked = e.Row.FindControl("ibLocked") as ImageButton;

                string status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().TrimEnd().ToUpper();

                ibRecall.Visible = (status.IsEqual(TKWorkFlow.Status.Approved) || status.IsEqual(TKWorkFlow.Status.Declined));
                ibApproved.Visible = (status.IsEqual(TKWorkFlow.Status.ForApproval));
                ibDeny.Visible = (status.IsEqual(TKWorkFlow.Status.ForApproval));
                ibApproved.Attributes.Add("onclick", "return window.confirm('Approve Restday?' );");

                //check if locked
                string datefrom = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "StartDate").ToString(), _KioskSession.DateFormat);
                string dateto = Tools.ShortDate(DataBinder.Eval(e.Row.DataItem, "StartDate").ToString(), _KioskSession.DateFormat);
                if (DateLockedForApprover(Convert.ToDateTime(datefrom)) || DateLockedForApprover(Convert.ToDateTime(dateto)))
                {
                    ibLocked.Visible = true;
                    ibApproved.Visible = false;
                    ibRecall.Visible = false;
                    ibDeny.Visible = false;
                }

                //Literal litStage = e.Row.FindControl("litStage") as Literal;
                //litStage.Text = DataBinder.Eval(e.Row.DataItem, "Stage").ToString();
            }

            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;

        int pageIndex = gvEmp.PageIndex;
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], false);

        gvEmp.DataBind();
        gvEmp.PageIndex = pageIndex;
    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp.DataSource = SortDataTable(GetData().Tables[0], true);
        gvEmp.PageIndex = e.NewPageIndex;
        gvEmp.DataBind();
    }


    protected void gvEmp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvEmp.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    private void ApprovedDeny(object sender, string newStatus, int rowindex, int newStage)
    {
        HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
        Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;
        string id = gvEmp.DataKeys[rowindex].Values["id"].ToString();
        string startDate = gvEmp.Rows[rowindex].Cells[3].Text.ToString();
        string endDate = gvEmp.Rows[rowindex].Cells[4].Text.ToString();

        BNS.TK.Entities.ChangeRestDay cr = new BNS.TK.Entities.ChangeRestDay();
        if (cr.LoadByPrimaryKey(id.ToInt()))
        {
            cr.Status = newStatus;
            cr.Stage = newStage;
            if (newStatus.ToLower().TrimEnd() == TKWorkFlow.Status.Declined.ToLower())
                cr.s_DeclinedReason = txtDecReason.Text;

            cr.LastUpdBy = _KioskSession.UID;
            cr.LastUpdDate = DateTime.Now;
            cr.Save();
        }

        
        //Helper.SendMail(Helper.GetEmailAddress(hfCompanyID.Value, litEmployeeID.Text), "Kronos Restday change of status", "Your DTR was acted upon by your supervisor.");


        gvEmp.EditIndex = -1;
        BindGrid();

        ReprocessAttendance(litEmployeeID.Text, startDate.ToDate(), endDate.ToDate());
    }

    private void Recall(object sender, int rowindex)
    {
        var row = gvEmp.Rows[rowindex];
        var employeeId = ((Literal)row.FindControl("litEmployeeID")).Text;
        var startDate = row.Cells[3].Text.ToDate();
        var endDate = row.Cells[4].Text.ToDate();
        var id = gvEmp.DataKeys[rowindex].Values["id"].ToString().ToInt();
        var cr = new BNS.TK.Entities.ChangeRestDay();
        try
        {
            if (!cr.LoadByPrimaryKey(id))
                throw new Exception("Change Rest Day not found.");

            cr.Status = TKWorkFlow.Status.ForApproval;
            cr.Stage = 0;
            cr.DeclinedReason = string.Empty;
            cr.LastUpdBy = _KioskSession.UID;
            cr.LastUpdDate = DateTime.Now;
            cr.Save();

        }
        catch (SqlException sqex)
        {
            lblError.Text = sqex.Message;
        }

        gvEmp.EditIndex = -1;
        BindGrid();
        ReprocessAttendance(employeeId, startDate, endDate);

        //HiddenField hfCompanyID = gvEmp.Rows[rowindex].FindControl("hfCompanyID") as HiddenField;
        //Literal litEmployeeID = gvEmp.Rows[rowindex].FindControl("litEmployeeID") as Literal;
        //string id = gvEmp.DataKeys[rowindex].Values["id"].ToString();
        //string startDate = gvEmp.Rows[rowindex].Cells[3].Text.ToString();
        //string endDate = gvEmp.Rows[rowindex].Cells[4].Text.ToString();


        //BNS.TK.Entities.ChangeRestDay cr = new BNS.TK.Entities.ChangeRestDay();
        //if (cr.LoadByPrimaryKey(id.ToInt()))
        //{
        //    cr.Status = TKWorkFlow.Status.Declined;
        //    cr.Stage = 0;
        //    cr.s_DeclinedReason = "Recalled";
        //    cr.LastUpdBy = _KioskSession.UID;
        //    cr.LastUpdDate = DateTime.Now;
        //    cr.Save();
        //}

        //gvEmp.EditIndex = -1;
        //BindGrid();

        //ReprocessAttendance(litEmployeeID.Text, startDate.ToDate(), endDate.ToDate());
    }


    private void ReprocessAttendance(string empid, DateTime from, DateTime to)
    {
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);
        proc.Process(from, to, empid);
    }


    private bool IsFinalApprover(string empid)
    {
        EmployeeApprover appr = new EmployeeApprover();
        string sql = "SELECT Approver3, Approver4 FROM EmployeeApprover WHERE CompanyID = {0} AND EmployeeID = {1} ";

        object[] parameters = { _KioskSession.CompanyID, empid.Trim() };
        appr.DynamicQuery(sql, parameters);
        if (appr.RowCount > 0)
        {
            return (appr.s_Approver3.IsNullOrEmpty() && appr.s_Approver4.IsNullOrEmpty());
        }
        return false;
    }

    //called onclick of gridview approve or decline button
    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        Literal litEmployeeID = gvEmp.Rows[gvEmp.SelectedIndex].FindControl("litEmployeeID") as Literal;
        string empid = litEmployeeID.Text;

        string newStatus = TKWorkFlow.Status.Approved;
        int newStage = 2;
        if (ApproverNo() == "1" || ApproverNo() == "2")
        {
            newStage = 1;
            if (IsFinalApprover(empid))
                newStatus = TKWorkFlow.Status.Approved;
            else
                newStatus = TKWorkFlow.Status.ForApproval;
        }

        ApprovedDeny(sender, newStatus, gvEmp.SelectedIndex, newStage);
    }

    //called onclick of grid delete button
    //this will just show popup to get the reason of decline
    protected void gvEmp_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        txtDecReason.Text = "";
        hfdecRowid.Value = e.RowIndex.ToString();
        mpe3.Show();
    }

    //called onclick fo refresh button
    protected void btnFU_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper().TrimEnd() == "RECALL")
        {
            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            int rowIndex = row.RowIndex;
            Recall(sender, rowIndex);
        }
    }

    private bool DateLockedForApprover(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Approver_lock_start != "" && tp.s_Approver_lock_end != "")
            {
                return (DateTime.Today >= tp.Approver_lock_start && DateTime.Today <= tp.Approver_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }
    protected void btnDecline_Click(object sender, EventArgs e)
    {
        int rowindex;
        if (int.TryParse(hfdecRowid.Value, out rowindex))
            ApprovedDeny(sender, "Declined", rowindex, 0);
    }


}
