﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using Bns.AttendanceUI;
using ClosedXML.Excel;
using BNS.TK.Entities;

public partial class LeaveEarnUpload : KioskPageUI
{

    private string[] _header = { "EMPLOYEE ID", "EMPLOYEE NAME", "LEAVE CODE", "YEAR", "CREDIT (DAYS)", "REMARKS" };

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (fuWorkfile.HasFile)
        {
            try
            {
                string strFile = Server.MapPath("~/UploadedFiles/" + fuWorkfile.FileName);

                lblStatus.Text = "Uploading....";
                fuWorkfile.SaveAs(strFile);

                ProcessExcel(strFile);

                lblStatus.Text = "";
            }
            catch (Exception ex)
            {
                lblSummary.ForeColor = System.Drawing.Color.Red;
                lblSummary.Text = ex.Message; //"The file uploaded was not recognized by the system";
            }
        }
        else
        {
            lblStatus.Text = "You have not specified a file.";
        }
    }

    private void ProcessExcel(string excelFile)
    {
        Guid guid = Guid.NewGuid();
        var workbook = new XLWorkbook(excelFile);
        var ws = workbook.Worksheet(1);

        LeaveEarnTmp_new leaveEarnTmp = new LeaveEarnTmp_new();
        int row = 2;

        while (true)
        {
            string employeeid = ws.Cell(row, 1).Value.ToString();
            if (employeeid.IsNullOrEmpty()) break;

            string leavecode = ws.Cell(row, 3).Value.ToString();
            string appYear = ws.Cell(row, 4).Value.ToString();
            string days = ws.Cell(row, 5).Value.ToString();
            string remarks = ws.Cell(row, 6).Value.ToString();

            if (leavecode.IsFilled() && days.IsDecimal())
            {
                //insert
                leaveEarnTmp.AddNew();
                leaveEarnTmp.SessionID = guid;
                leaveEarnTmp.LeaveEarnID = 0;
                leaveEarnTmp.CompanyID = _KioskSession.CompanyID;
                leaveEarnTmp.TranDate = DateTime.Today;
                leaveEarnTmp.EmployeeID = employeeid;
                leaveEarnTmp.LeaveCode = leavecode;
                if (appYear.IsNumeric())
                    leaveEarnTmp.AppYear = appYear.ToInt();
                else
                    leaveEarnTmp.AppYear = DateTime.Today.Year;
                leaveEarnTmp.AppMonth = 0;
                leaveEarnTmp.Remarks = remarks;
                leaveEarnTmp.LeaveEarnType = " ";
                leaveEarnTmp.Earned = days.ToDecimal();
                leaveEarnTmp.CreatedBy = _KioskSession.EmployeeID;
                leaveEarnTmp.CreatedDate = DateTime.Today;
            }

            row++;
        }

        // save
        leaveEarnTmp.Save();


        string conn = System.Configuration.ConfigurationManager.AppSettings.Get("Connection");
        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@sessionid", SqlDbType.UniqueIdentifier) };
        sqlParameters[0].Value = guid;

        DataSet dsErrors = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_LeaveEarnUploadSession", sqlParameters);


        //if (dsErrors.Tables[0].Rows.Count > 0)
        //{
        //    //show errors
        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //    foreach(DataRow dr in dsErrors.Tables[0].Rows)
        //    {

        //        sb.Append(string.Format("Error in row {0}: {1} <br/>", dr["ExcelRow"].ToString(), dr["ErrorMessage"].ToString()));
        //    }

        //    lblSummary.Text = sb.ToString();
        //}
        //else
        //{           
        //   lblSummary.ForeColor = System.Drawing.Color.Black;
        //   lblSummary.Text = "Update Successful";
        //}
        lblSummary.ForeColor = System.Drawing.Color.Black;
        lblSummary.Text = "Update Successful";
    }


    private bool ValidateHeader(IXLWorksheet ws)
    {
        for (int i = 0; i < _header.Length; i++)
        {
            if (ws.Cell(1, i).Value.ToString().ToUpper().Trim() != _header[i].ToUpper().Trim())
                return false;
        }
        return true;
    }


    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        // Create the workbook and sheet
        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add("Sheet1");

        // header
        for (int i = 0; i < _header.Length; i++)
        {
            ws.Cell(1, i + 1).Value = _header[i].ToUpper().Trim();
            ws.Column(i + 1).Width = 20;
        }
        //formatting
        ws.Range("A1:F1").Style.Font.Bold = true;
        ws.Column(2).Width = 50;
        ws.Column(_header.Length).Width = 50;

        string sql =
            "select EmployeeID, Fullname " +
            "from Employee " +
            "Where CompanyID = {0} and DateTerminated IS NULL " +
            "Order by Fullname ";

        Employee emp = new Employee();
        object[] parameters = { _KioskSession.CompanyID };
        emp.DynamicQuery(sql, parameters);
        if (emp.RowCount > 0)
        {
            int row = 1;
            do
            {
                row++;
                ws.Cell(row, 1).Value = emp.EmployeeID;
                ws.Cell(row, 2).Value = emp.FullName;

                //ws.Cell(row, 3).Value = "VL";
                //ws.Cell(row, 4).Value = "2013";
                //ws.Cell(row, 5).Value = "1";
            } while (emp.MoveNext());
        }
        emp = null;


        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", "attachment;filename=\"LeaveEarn.xlsx\"");

        // Flush the workbook to the Response.OutputStream
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }

        httpResponse.End();
    }
}
