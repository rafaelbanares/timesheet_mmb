﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TimeMachine2.aspx.cs" Inherits="TimeMachine2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kronos Clock</title>
    <script type="text/javascript">    
        var dn;
        c1 = new Image(); c1.src = "Graphics/clock/c1.gif";
        c2 = new Image(); c2.src = "Graphics/clock/c2.gif";
        c3 = new Image(); c3.src = "Graphics/clock/c3.gif";
        c4 = new Image(); c4.src = "Graphics/clock/c4.gif";
        c5 = new Image(); c5.src = "Graphics/clock/c5.gif";
        c6 = new Image(); c6.src = "Graphics/clock/c6.gif";
        c7 = new Image(); c7.src = "Graphics/clock/c7.gif";
        c8 = new Image(); c8.src = "Graphics/clock/c8.gif";
        c9 = new Image(); c9.src = "Graphics/clock/c9.gif";
        c0 = new Image(); c0.src = "Graphics/clock/c0.gif";
        cb = new Image(); cb.src = "Graphics/clock/cb.gif";
        cam = new Image(); cam.src = "Graphics/clock/cam.gif";
        cpm = new Image(); cpm.src = "Graphics/clock/cpm.gif";
        function extract(h, m, s, type) {
            if (!document.images) return;
            if (h <= 9) {
                document.images.a.src = cb.src;
                document.images.b.src = eval("c" + h + ".src");
            }
            else {
                document.images.a.src = eval("c" + Math.floor(h / 10) + ".src");
                document.images.b.src = eval("c" + (h % 10) + ".src");
            }
            if (m <= 9) {
                document.images.d.src = c0.src;
                document.images.e.src = eval("c" + m + ".src");
            }
            else {
                document.images.d.src = eval("c" + Math.floor(m / 10) + ".src");
                document.images.e.src = eval("c" + (m % 10) + ".src");
            }
            if (s <= 9) {
                document.g.src = c0.src;
                document.images.h.src = eval("c" + s + ".src");
            }
            else {
                document.images.g.src = eval("c" + Math.floor(s / 10) + ".src");
                document.images.h.src = eval("c" + (s % 10) + ".src");
            }
            if (dn == "AM") document.j.src = cam.src;
            else document.images.j.src = cpm.src;
        }

        function show3() {
            if (!document.images)
                return;
            var Digital = new Date();
            var hours = Digital.getHours();
            var minutes = Digital.getMinutes();
            var seconds = Digital.getSeconds();
            dn = "AM";
            if ((hours >= 12) && (minutes >= 1) || (hours >= 13)) {
                dn = "PM";
                hours = hours - 12;
            }
            if (hours == 0)
                hours = 12;

            var mm = getMonthString(Digital);
            var dd = Digital.getDate();
            var yy = Digital.getFullYear();
            document.getElementById('lbDate').innerHTML = mm + " " + dd + ", " + yy;

            extract(hours, minutes, seconds, dn);
            setTimeout("show3()", 1000);
        }

        document.onkeypress = keyIO;

        function keyIO(e) {
            e = e || event;
            var keynum;
            var keychar;

            if (window.event) // IE
                keynum = e.keyCode;
            else if (e.which) // Netscape/Firefox/Opera
                keynum = e.which;

            document.getElementById("tbKeyCode").value = keynum;
            if (keynum == 73 || keynum == 105 || keynum == 79 || keynum == 111) {
                assignIO();
            }
            return false;
        }

        function assignIO() {
            var keynum = document.getElementById("tbKeyCode").value;
            document.onkeypress = "";
            document.getElementById('dio').style.display = "none";
            document.getElementById('dlogin').style.display = "";

            document.getElementById('tbUID').value = "";
            document.getElementById('tbPWD').value = "";

            if (keynum == 73 || keynum == 105) {
                document.getElementById('bOK').value = "Time IN";
                document.getElementById('hdIO').value = "I";
            }
            if (keynum == 79 || keynum == 111) {
                document.getElementById('bOK').value = "Time OUT";
                document.getElementById('hdIO').value = "O";
            }

            document.getElementById('tbUID').focus();
        }

        function getMonthString(d) {
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            return month[d.getMonth()];
        }
    </script>
</head>
<body onload="show3()">
    <form id="form1" runat="server">    
    <br />
    <br />
    <br />
    <table border="1" 
            style="background-color:#CCCCFF; border-width:1; border-color:dodgerblue; width: 600px; " 
            align="center">
        <tr>
            <td align="center">
                <div style="background-color:black;width:100%">
                    <img height="90" src="Graphics/clock/cb.gif" width="62" name="a">
                    <img height="80" src="Graphics/clock/cb.gif" width="62" name="b">
                    <img height="80" src="Graphics/clock/colon.gif" width="36" name="c">
                    <img height="80" src="Graphics/clock/cb.gif" width="62" name="d">
                    <img height="80" src="Graphics/clock/cb.gif" width="62" name="e">
                    <img height="80" src="Graphics/clock/colon.gif" width="36" name="f">
                    <img height="80" src="Graphics/clock/cb.gif" width="62" name="g">
                    <img height="80" src="Graphics/clock/cb.gif" width="62" name="h">
                    <img height="80" src="Graphics/clock/cam.gif" width="62" name="j">
                 </div>
            </td>
        </tr>
        <tr>
            <td align="center">                
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbDate" runat="server" Font-Size="XX-Large"></asp:Label><br />
                        </td>
                    </tr>                    
                </table>
                <br />
                <br />                
                <table>                    
                    <tr>
                        <td>                            
                            <asp:Label ID="lblMessage" runat="server" Font-Size="Large"></asp:Label>
                            <br />
                            <asp:GridView ID="gvwEntries" runat="server" AutoGenerateColumns="False" 
                                Width="210px" ShowHeader="False">
                                <Columns>
                                    <asp:BoundField DataField="TimeIO" />
                                    <asp:BoundField DataField="IO" />
                                </Columns>
                            </asp:GridView>                            
                        </td>
                    </tr>                     
                </table>
                <br />
                <div id="dio">
                    <asp:Label ID="lblIO" runat="server" 
                        Text="Press (I) for Time In or (O) for Time Out" Font-Size="X-Large"></asp:Label>                    
                    <asp:TextBox ID="tbKeyCode" runat="server" style="display:none"></asp:TextBox>
                </div>
                <br />
                <div id="dlogin" style="display:none">                
                    <table>
                    <tr>
                        <td>
                            User ID:
                        </td>
                        <td>
                            <asp:TextBox ID="tbUID" runat="server" MaxLength="15" Width="108px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rvUID" runat="server" ErrorMessage="User ID is required" ControlToValidate="tbUID" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="tbPWD" runat="server" MaxLength="60" Width="108px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rvPWD" runat="server" ErrorMessage="Password is required" ControlToValidate="tbPWD" SetFocusOnError="True">*</asp:RequiredFieldValidator>                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="bOK" runat="server" Text="TimeIO" OnClick="bOK_Click" />&nbsp;
                            <asp:Button ID="bCancel" runat="server" Text="Cancel" ValidationGroup="None" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="vs" runat="server" />
                            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                </div>
                <asp:Literal ID="litScript" runat="server"></asp:Literal>
                <asp:HiddenField ID="hdIO" runat="server" />
                <br />
            </td>            
        </tr>
        <tr>
            <td align="center">
                <a href="LoginEmployee.aspx" style="font-family: Arial, Helvetica, sans-serif; font-size: small">Kronos Login</a>
            </td>
        </tr>
    </table>    
    </form>
</body>
</html>
