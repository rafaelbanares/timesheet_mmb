using System;
using System.IO;
using Bns.AttendanceUI;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using MMB.Core;
using BNS.TK.Entities;
using BNS.TK.Business;
using ClosedXML.Excel;

public partial class UploadTimeTransaction : KioskPageUI
{


    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
    }

    // This will just insert time entries in table TimeTrans
    // NO processing of attendance
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (fuWorkfile.HasFile)
        {
            try
            {
                string message = "";
                int rowCount = 0;
                string strFile = Server.MapPath("~/UploadedFiles/" + fuWorkfile.FileName);

                lblStatus.Text = "Uploading....";
                fuWorkfile.SaveAs(strFile);

                if (UploadRawData(strFile, ref message, ref rowCount))
                {
                    lblSummary.ForeColor = System.Drawing.Color.Black;
                    lblStatus.Text = "";
                    lblSummary.Text = string.Format("Successfuly uploaded {0} row(s).", rowCount);
                }
                else
                {
                    lblSummary.ForeColor = System.Drawing.Color.Red;
                    lblStatus.Text = "";
                    lblSummary.Text = message;
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = "";
                lblSummary.ForeColor = System.Drawing.Color.Red;
                //lblSummary.Text = "The file uploaded was not recognized by the system";
                lblSummary.Text = ex.Message;
            }
        }
        else
        {
            lblSummary.Text = "";
            lblStatus.Text = "You have not specified a file.";
        }
    }

    private bool UploadRawData(string strFile, ref string message, ref int rowCount)
    {
        //string rawUploader = ConfigurationManager.AppSettings.Get("RawUploader");
        bool success = true;
        //success = UploadOldRawTorm(strFile, ref message, ref rowCount);
        success = UploadNewRawTorm(strFile, ref message, ref rowCount);
        return success;
    }

    #region UploadRawTimeFromTemp
    private DataSet UploadRawTimeFromTemp(System.Guid guid)
    {
        SqlParameter[] param = { new SqlParameter("@CompanyID", SqlDbType.Char, 10),
                                    new SqlParameter("@sessionid", SqlDbType.UniqueIdentifier) };
        param[0].Value = _KioskSession.CompanyID;
        param[1].Value = guid;

        return SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, _KioskSession.DB + ".dbo.usa_UploadRawTime", param);
    }
    #endregion

    #region UploadRawTorm
    /// <summary>
    /// This will insert time entries in table TimeTrans
    /// </summary>
    /// <param name="file"></param>
    /// <param name="message"></param>
    /// <returns></returns>
    private bool UploadNewRawTorm(string file, ref string message, ref int rowCount)
    {
        var guid = Guid.NewGuid();
        var raw = new RawTime();

        string[] words;

        using (var sr = new StreamReader(file))
        {
            var line = sr.ReadLine();
            DateTime timeIO;

            if (line != "CHECKTIME,USERID,SENSORID,SSN,Badgenumber,CardNo")
            {
                lblSummary.Text = "The file uploaded was not recognized by the system";
                return false;
            }

            while ((line = sr.ReadLine()) != null)
            {
                words = line.Split(',');

                //if (words[3].Length == 5)
                //{
                var validIO = DateTime.TryParseExact(words[0], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out timeIO);

                if (validIO)
                {
                    raw.AddNew();
                    raw.SessionID = guid;
                    raw.CompanyID = _KioskSession.CompanyID;
                    raw.EmployeeID = words[3];
                    raw.TimeIO = timeIO;
                    raw.IO = " ";
                }
                //}
            }
        }

        try
        {
            raw.Save();

            DataSet ds = UploadRawTimeFromTemp(guid);

            rowCount = (int)ds.Tables[1].Rows[0]["insertedRows"];

            if (ds.Tables[0].Rows.Count > 0)
            {
                lblMissing.Visible = true;
                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                lblMissing.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
                lblSummary.Text = "Done";
            }

        }
        catch (Exception ex)
        {
            lblSummary.Text = ex.Message;
            return false;
        }

        return true;
    }

    /// <summary>
    /// This will insert time entries in table TimeTrans
    /// </summary>
    /// <param name="file"></param>
    /// <param name="message"></param>
    /// <returns></returns>
    private bool UploadRawTorm(string file, ref string message, ref int rowCount)
    {
        var guid = Guid.NewGuid();
        var raw = new RawTime();

        string[] words;

        using (var sr = new StreamReader(file))
        {
            var line = sr.ReadLine();

            if (line.Replace("\"", "") != "CHECKTIME,USERID,SENSORID,SSN,Badgenumber,CardNo")
            {
                lblSummary.Text = "The file uploaded was not recognized by the system";
                return false;
            }

            while ((line = sr.ReadLine()) != "Export Log End,")
            {
                words = line.Replace("\"", "").Split(',');

                DateTime timeIO;
                DateTime.TryParseExact(words[2], "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out timeIO);

                raw.AddNew();
                raw.SessionID = guid;
                raw.CompanyID = _KioskSession.CompanyID;
                raw.SwipeID = words[0];
                raw.TimeIO = timeIO;
                raw.IO = " ";
            }
        }

        try
        {
            raw.Save();

            DataSet ds = UploadRawTimeFromTemp(guid);

            rowCount = (int)ds.Tables[1].Rows[0]["insertedRows"];

            if (ds.Tables[0].Rows.Count > 0)
            {
                lblMissing.Visible = true;
                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                lblMissing.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
                lblSummary.Text = "Done";
            }

        }
        catch (Exception ex)
        {
            lblSummary.Text = ex.Message;
            return false;
        }

        return false;
    }

    private bool UploadOldRawTorm(string file, ref string message, ref int rowCount)
    {
        var guid = Guid.NewGuid();
        var raw = new RawTime();

        string[] words;

        using (var sr = new StreamReader(file))
        {
            var line = sr.ReadLine();
            while ((line = sr.ReadLine()) != null)
            {
                words = line.Split('\t');

                if (words[4][0] != 'G')
                {
                    DateTime timeIO;
                    DateTime.TryParseExact(words[1] + words[2], "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out timeIO);

                    raw.AddNew();
                    raw.SessionID = guid;
                    raw.CompanyID = _KioskSession.CompanyID;
                    //raw.SwipeID = words[5];
                    raw.s_EmployeeID = words[4];
                    raw.TimeIO = timeIO;
                    raw.IO = words[3].Contains(" OUT") ? "O" : "I";
                    raw.Station = "";
                    raw.IOCode = " ";
                }
            }
        }

        try
        {
            raw.Save();

            DataSet ds = UploadRawTimeFromTemp(guid);

            rowCount = (int)ds.Tables[1].Rows[0]["insertedRows"];

            if (ds.Tables[0].Rows.Count > 0)
            {
                lblMissing.Visible = true;
                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                lblMissing.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
                lblSummary.Text = "Done";
            }

        }
        catch (Exception ex)
        {
            lblSummary.Text = ex.Message;
            return false;
        }

        return true;
    }
    #endregion

    private bool IsValidDate(string s)
    {
        DateTime outval;
        if (s.IsNullOrEmpty()) return false;
        return DateTime.TryParse(s, out outval);
    }


}

