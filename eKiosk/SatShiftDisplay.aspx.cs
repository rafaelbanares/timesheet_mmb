﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bns.AttendanceUI;

using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using Bns.Attendance;
using Bns.AttUtils;


public partial class SatShiftDisplay : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        DataSet ds = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usp_CompSaturday_Display");
            //new SqlParameter[] { new SqlParameter("@Appyear", txtYear.Text ) });
        gvMain.DataSource = ds.Tables[0];
        gvMain.DataBind();
    }
    protected void gvMain_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string satdisplay = DataBinder.Eval(e.Row.DataItem, "SatDisplay").ToString().Trim();

            LinkButton lnkSelect = e.Row.FindControl("lnkSelect") as LinkButton;
            string url = string.Format("MultiEmpSelectGroup.aspx?Date={0}", DataBinder.Eval(e.Row.DataItem, "SatDate").ToString().Trim() );
            SecureUrl su = new SecureUrl(url);
            lnkSelect.Attributes["onclick"] = "javascript:openWD('" + su.ToString() + "');return false;";

            LinkButton lnkSwitch = e.Row.FindControl("lnkSwitch") as LinkButton;
            string url2 = string.Format("SwitchSatWork.aspx?Date={0}&SatDisplay={1}&SatSwitch={2}", 
                        DataBinder.Eval(e.Row.DataItem, "SatDate").ToString().Trim(),
                        satdisplay,
                        DataBinder.Eval(e.Row.DataItem, "SatSwitch").ToString().Trim()
                );
            lnkSwitch.Text = satdisplay;
            SecureUrl su2 = new SecureUrl(url2);
            lnkSwitch.Attributes["onclick"] = "javascript:openSwitch('" + su2.ToString() + "');return false;";


            if (satdisplay.ToLower() == "no work")
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#cccccc");
            
            e.Row.Attributes.Add("onmouseover", "this.className='hightlighrow'");
            e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
        }
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
}
