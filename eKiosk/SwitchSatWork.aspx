﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="SwitchSatWork.aspx.cs" Inherits="SwitchSatWork" Title="Saturday Option" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

    <table cellpadding="0" class="TableBorder1" style="width: 300px" >
        <tr>
            <td class="DataGridHeaderStyle" colspan="2" style="height: 12px">
                Switch Saturday Work</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <br />
                Do you want to change?
                <br /><br />
            </td>
        </tr>
        <tr>
            <td align="right">
                From : 
            </td>
            <td>
                <asp:Label ID="lbFrom" runat="server" Text="old sat value" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                To : 
            </td>
            <td>
                <asp:Label ID="lbTo" runat="server" Text="new sat value" Font-Bold="true"></asp:Label>                
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <br />
                <asp:CheckBox ID="chkCascade" runat="server" Text="Cascade changes?" />
                <br />
                <span style="color: red">The save process may take awhile  to finish.</span></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <hr />
            <asp:Button ID="btnOK" runat="server" Text="Yes" style="width: 100px" OnClientClick="return confirm('Are you sure with these changes?');"
                onclick="btnOK_Click" />&nbsp;&nbsp;
            <asp:Button ID="btnNo" runat="server" Text="No" style="width: 100px" OnClientClick="window.close();return false;"/>
                
            </td>
        </tr>
    </table>


</asp:Content>

