﻿function validatenumber(event, obj) {
    var code = (event.which) ? event.which : event.keyCode;
    var character = String.fromCharCode(code);

    if ((code >= 48 && code <= 57)) { // check digits

        // Disallow all numbers if the entry is 0
        if (obj.value == "0")
            return false;

        if (!isNaN(obj.value)) {
            if (obj.value == "0.0" && code == 48) {
                alert("Value cannot be less than 0.01");
                return false;
            }
        }

        return true;
    }
    else if (code == 46) { // Check dot
        if (obj.value.indexOf(".") < 0) {
            if (obj.value.length == 0)
                obj.value = "0";

            return true;
        }
    }
    else if (code == 8 || code == 116) { // Allow backspace, F5
        return true;
    }
    else if (code >= 37 && code <= 40) { // Allow directional arrows
        return true;
    }

    return false;
}

function validatefield(obj) { // Remove dot if last character
    if (obj.value.indexOf(".") == obj.value.length - 1) {
        obj.value = obj.value.substring(0, obj.value.length - 1)
    } // Clear text box if not a number, incase user drags/drop letter into box
    else if (isNaN(obj.value)) {
        obj.value = "";
    }
    else if (obj.value <= 0.01) {
        obj.value = "";
        alert("Value cannot be less than 0.01.");
    }
}