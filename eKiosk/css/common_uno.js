

function load() {    
   Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);   
}


function wopen(url, name, w, h)
{
  w += 32;
  h += 96;
  wleft = (screen.width - w) / 2;
  wtop = (screen.height - h) / 2;
  if (wleft < 0) {
    w = screen.width;
    wleft = 0;
  }
  if (wtop < 0) {
    h = screen.height;
    wtop = 0;
  }
  var win = window.open(url,name,
    'width=' + w + ', height=' + h + ', ' +
    'left=' + wleft + ', top=' + wtop + ', ' +
    'location=no, menubar=no, ' +
    'status=yes, toolbar=no, scrollbars=yes, resizable=yes');

  win.resizeTo(w, h);

  win.moveTo(wleft, wtop);
  win.focus();
  return win;
}

function closewindow() {
    if (window.popupWindow) {
        try {
            window.popupWindow.close()
        }
        catch(e) {
        }
    }
}

function closewindow(popWin) {
    if (popWin) {
        try {
            popWin.close();
        }
        catch(e) {
        }
    }
}

