﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="WorkForSubProjEmp.aspx.cs" Inherits="WorkForSubProjEmp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
        Font-Underline="False" Text="Project Definitions"></asp:Label><br />
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="New Project Definition" /><asp:LinkButton ID="lbNew" runat="server" CssClass="ControlDefaults"
            Font-Bold="True" OnClick="lbNew_Click">New Project Definition</asp:LinkButton><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" CssClass="DataGridStyle"
                OnRowCancelingEdit="gvMain_RowCancelingEdit" OnRowDataBound="gvMain_RowDataBound"
                OnRowDeleting="gvMain_RowDeleting" OnRowEditing="gvMain_RowEditing" OnRowUpdating="gvMain_RowUpdating" DataKeyNames="ProjID">
                <HeaderStyle CssClass="DataGridHeaderStyle" />
                <Columns>
                    <asp:TemplateField HeaderText="FlexHeader-WorkFor">
                        <ItemStyle Width="130px" />
                        <ItemTemplate>
                            <asp:Label ID="lblWorkFor" runat="server" Text='<%# Bind("WorkForShortDesc") %>' ></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlWorkFor" runat="server" CssClass="ControlDefaults" Width="120px" 
                                DataTextField="ShortDesc" DataValueField="WorkForID" 
                                ValidationGroup="Trans" AutoPostBack="true" 
                                onselectedindexchanged="ddlWorkFor_SelectedIndexChanged" ></asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="FlexHeader-WorkForSub">
                        <ItemStyle Width="130px" />
                        <ItemTemplate>
                            <asp:Label ID="lblWorkForSub" runat="server" Text='<%# Bind("WorkForSubShortDesc") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlWorkForSub" runat="server" CssClass="ControlDefaults" Width="120px" 
                                DataTextField="ShortDesc" DataValueField="WorkForSubID" ValidationGroup="Trans"></asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Project" SortExpression="ShortDesc">
                        <ItemStyle Width="240px" />
                        <EditItemTemplate>
                            <asp:TextBox id="txtShortDesc" runat="server" Width="220px" Text='<%# Bind("ShortDesc") %>' CssClass="ControlDefaults" MaxLength="25"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvShortDesc" runat="server" ControlToValidate="txtShortDesc" 
                                ErrorMessage="Short description cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label id="lblShortDesc" runat="server" Text='<%# Bind("ShortDesc") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Project Description">
                        <ItemStyle Width="370px" />
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="ControlDefaults" Text='<%# Bind("Description") %>' MaxLength="150" Width="350px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" 
                                ErrorMessage="Description cannot be blank" ValidationGroup="Trans">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                                        
                    <asp:TemplateField>
                        <ItemStyle Width="42px" />
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                            <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                        </EditItemTemplate>
                        <HeaderTemplate>
                            Action
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                            <asp:ImageButton ID="ibDelete" runat="server" CommandName="delete" ToolTip="Delete" ImageUrl="~/Graphics/delete.gif" 
                                    OnClientClick="return window.confirm( 'Are you sure that you want to delete selected item?' );"/>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

