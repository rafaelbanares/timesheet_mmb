<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Zinc.aspx.cs" Inherits="Zinc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Encryption Test</title>
    <link href="css/maia.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />
        <asp:Label ID="Label1" runat="server" ForeColor="White"></asp:Label><br />
        <asp:Button ID="Button1" runat="server" CssClass="maia-button" OnClick="Button1_Click" Text="Garbled" />
        <asp:Button ID="Button2" runat="server" CssClass="maia-button" OnClick="Button2_Click" Text="DeGarbled" /></div>
    </form>
</body>
</html>
