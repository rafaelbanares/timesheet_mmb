﻿<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="AdminOTApproval.aspx.cs" Inherits="AdminOTApproval" Title="Overtime Approval List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>
<script type="text/javascript">
function openwindow(url)
{
    popupWindow=wopen($get('<%= hfURL.ClientID %>').value,"popupWindow",480,300);
}
function updatelevels(ids, levels, url)
{
    $get('<%= hfLevels.ClientID %>').value = ids;        
    $get('<%= txtLevels.ClientID %>').value = levels;
    $get('<%= hfURL.ClientID %>').value = url;
    
    closechild();
}
function closechild()
{
    $get("<%=ibGo.ClientID%>").click();
    closewindow(popupWindow);
}
</script>

<table width="100%">
    <tr>
        <td valign="top">
            <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large"
                Font-Underline="False" Text="Overtime Approval List"></asp:Label><br />
            <br />
            <table width="100%">
              <tr>
                <td align="right" style="width: 80px">
                    <asp:Label id="lblSearch" runat="server" Text="Search:" CssClass="ControlDefaults"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="ControlDefaults" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" 
                        OnClick="ibGo_Click" ValidationGroup="emplist" Enabled="False" /></td>
              </tr>
            </table>
        </td>
        <td align="right">
            <table>
                <tr>
                    <td>
                        Attendance From:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateStart" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateStart" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator ID="mevDateStart" runat="server" ControlExtender="meeDateStart"
                            ControlToValidate="txtDateStart" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td>
                        To:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateEnd" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                        <asp:ImageButton ID="ibtxtDateEnd" runat="server" ImageUrl="~/Graphics/calendar.gif"
                            OnClientClick="return false;" ToolTip="Click to choose date" />
                        <ajaxToolkit:MaskedEditValidator ID="mevDateEnd" runat="server" ControlExtender="meeDateEnd"
                            ControlToValidate="txtDateEnd" EmptyValueBlurredText="*" EmptyValueMessage="Cannot accept blank"
                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Cannot accept invalid value"
                            IsValidEmpty="False" MinimumValue="01/01/2008" MinimumValueBlurredText="*" MinimumValueMessage="Cannot accept date">*
                        </ajaxToolkit:MaskedEditValidator>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Show:&nbsp;<asp:DropDownList ID="ddlShow" runat="server" AutoPostBack="True" CssClass="ControlDefaults" OnSelectedIndexChanged="ddlShow_SelectedIndexChanged">
                            <asp:ListItem Value="all">All Transactions</asp:ListItem>
                            <asp:ListItem Value="For Approval">For Approval</asp:ListItem>
                            <asp:ListItem>For Final Approval</asp:ListItem>
                        </asp:DropDownList></td>
                    <td align="left">
                        <asp:ImageButton ID="ibGo" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                            ToolTip="Go" /></td>
                </tr>
            </table>
        </td>
        <td align="center">
            <table>
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lnkLevel" runat="server">Select Groupings/Department</asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lnkClear" runat="server" ToolTip="Clear Selection" 
                            onclick="lnkClear_Click">clear</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtLevels" runat="server" Enabled="False" Height="62px" TextMode="MultiLine"
                            Width="279px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<hr />

<asp:UpdatePanel id="up1" runat="server">
    <ContentTemplate>    
        <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
<asp:GridView ID="gvEmp" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CssClass="DataGridStyle" PageSize="20" 
            AllowSorting="True" OnRowDataBound="gvEmp_RowDataBound" 
            OnSorting="gvEmp_Sorting" OnPageIndexChanging="gvEmp_PageIndexChanging" 
            OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" 
            OnRowUpdating="gvEmp_RowUpdating" onrowcommand="gvEmp_RowCommand" Width="100%" 
            DataKeyNames="id,status">
        <Columns>
            <asp:TemplateField HeaderText="Emp.ID." SortExpression="EmployeeID">
                <ItemTemplate>
                    <asp:Literal ID="litEmployeeID" runat="server" Text='<%# Bind("EmployeeID") %>'></asp:Literal>
                    <asp:HiddenField ID="hfCompanyID" runat="server" Value='<%# Bind("CompanyID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name" SortExpression="FullName">
                <ItemTemplate>
                    <asp:Literal ID="litFullname" runat="server" Text='<%# Bind("FullName") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="OT Date" SortExpression="OTDate">
                <ItemTemplate>
                    <asp:Literal ID="litDate" runat="server" Text='<%# Bns.AttUtils.Tools.ShortDate(DataBinder.Eval(Container.DataItem, "OTDate"), "MM/dd/yyyy")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Day">
                <ItemTemplate>
                    <asp:Literal ID="litDay" runat="server" Text='<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "OTDate").ToString()).DayOfWeek.ToString().Substring(0,3).ToLower() %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="OT Start">
                <EditItemTemplate>
                    <asp:TextBox ID="tbin1" runat="server" Text='<%# Bind("OTStart", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevin1" runat="server" ControlExtender="mein1" ControlToValidate="tbin1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="mein1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbin1">
                    </ajaxToolkit:MaskedEditExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblIn1" runat="server" Text='<%# Bind("OTStart", "{0:hh:mm tt}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="OT End">
                <EditItemTemplate>
                    <asp:TextBox ID="tbout1" runat="server" Text='<%# Bind("OTEnd", "{0:hh:mm tt}") %>' Width="55px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditValidator ID="mevout1" runat="server" ControlExtender="meout1" ControlToValidate="tbout1"
                        Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Time is invalid"
                        ValidationGroup="Trans"></ajaxToolkit:MaskedEditValidator>
                    <ajaxToolkit:MaskedEditExtender ID="meout1" runat="server" AcceptAMPM="True" ErrorTooltipEnabled="True"
                        Mask="99:99" MaskType="Time" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                        OnInvalidCssClass="MaskedEditError" TargetControlID="tbout1">
                    </ajaxToolkit:MaskedEditExtender>                    
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblOut1" runat="server" Text='<%# Bind("OTEnd", "{0:hh:mm tt}") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="hours">
                <EditItemTemplate>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="litApprovedOT" runat="server" Text='<%# Bind("ApprovedOT") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reason">
                <EditItemTemplate>
                    <asp:TextBox ID="tbReason" runat="server" Text='<%# Bind("Reason") %>' 
                        Width="100px" MaxLength="200"></asp:TextBox>                        
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status" SortExpression="Stage">
                <ItemTemplate>
                    <asp:Literal ID="litStage" runat="server" Text='<%# Bind("Stage") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle Width="62px" />
                <EditItemTemplate>
                    <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF" ValidationGroup="Trans" />
                    <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                    <asp:ValidationSummary ID="vSummTrans" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Trans" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" CommandName="edit" ToolTip="Edit" ImageUrl="~/Graphics/edit.gif" />
                    <asp:ImageButton ID="ibRecall" runat="server" CommandName="recall" ToolTip="Recall" ImageUrl="~/Graphics/cancel.gif" OnClientClick="return window.confirm('Recall Leave?' );" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle Height="22px" />
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>
        <PagerStyle HorizontalAlign="Right" />
        <HeaderStyle CssClass="DataGridHeaderStyle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
    </asp:GridView>
    <table width="100%">
        <tr>
            <td style="width:40%"></td>
            <td align="right"><asp:Button ID="btnForApproval" runat="server" Text="Approve all - For Approval" Width="220px" onclick="btnForApproval_Click" Visible="False" /></td>
            <td align="right"><asp:Button ID="btnForFinalApproval" runat="server" Text="Approve all - For Final Approval" Width="220px" onclick="btnForFinalApproval_Click" Visible="False" /></td>
        </tr>
    </table>

    </ContentTemplate>
    <Triggers>    
        <asp:AsyncPostBackTrigger ControlID="ibGo"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="ddlShow"></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="txtSearch"></asp:AsyncPostBackTrigger>
    </Triggers>    
</asp:UpdatePanel>


    <ajaxToolkit:MaskedEditExtender ID="meeDateStart" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateStart">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateStart" runat="server" PopupButtonID="ibtxtDateStart"
        PopupPosition="BottomRight" TargetControlID="txtDateStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeDateEnd" runat="server" ErrorTooltipEnabled="True"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtDateEnd">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:CalendarExtender ID="caleDateEnd" runat="server" PopupButtonID="ibtxtDateEnd"
        PopupPosition="BottomRight" TargetControlID="txtDateEnd">
    </ajaxToolkit:CalendarExtender>

    <asp:HiddenField
    ID="hfLevels" runat="server" Value="~~~" />
<asp:HiddenField ID="hfURL" runat="server" />

</asp:Content>

