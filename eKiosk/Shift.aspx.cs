using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using ClosedXML.Excel;
using BNS.TK.Business;

public partial class Shift : KioskPageUI
{

    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid(bool addnew = false)
    {        
        BNS.TK.Entities.Shift shift = new BNS.TK.Entities.Shift();
        shift.Where.CompanyID.Value = _KioskSession.CompanyID;
        shift.Query.Load();

        if (addnew)
        {
            shift.AddNew();
            gvMain.EditIndex = shift.RowCount - 1;
        }

        gvMain.DataSource = shift.DefaultView;
        gvMain.DataBind();
        hfIsAdd.Value = (addnew) ? "1" : "0";
    }

    protected void gvMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMain.EditIndex = -1;
        BindGrid();
    }

    protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton ibDelete = e.Row.FindControl("ibDelete") as ImageButton;

            bool viewmode = (ibDelete != null);

            bool flexi = false;
            bool.TryParse(DataBinder.Eval(e.Row.DataItem, "flexi").ToString(), out flexi);
            //bool flexbreak = bool.Parse(DataBinder.Eval(e.Row.DataItem, "flexbreak").ToString());
            //bool otb4shift = bool.Parse(DataBinder.Eval(e.Row.DataItem, "otb4shift").ToString());

            if (viewmode)
            {
                Image imgFlexi = e.Row.FindControl("imgFlexi") as Image;
                Image imgFlexBreak = e.Row.FindControl("imgFlexBreak") as Image;
                Image imgOtB4Shift = e.Row.FindControl("imgOtB4Shift") as Image;

                imgFlexi.Visible = flexi;
                //imgFlexBreak.Visible = flexbreak;
                //imgOtB4Shift.Visible = otb4shift;

                ibDelete.Attributes.Add("OnClick", "return window.confirm( 'Are you sure that you want to delete selected item?' );");
            }
            else
            {
                //... edit mode
                CheckBox chkFlexi = e.Row.FindControl("chkFlexi") as CheckBox;
                //CheckBox chkFlexBreak = e.Row.FindControl("chkFlexBreak") as CheckBox;
                //CheckBox chkOtB4Shift = e.Row.FindControl("chkOtB4Shift") as CheckBox;

                chkFlexi.Checked = flexi;
                //chkFlexBreak.Checked = flexbreak;
                //chkOtB4Shift.Checked = otb4shift;
            }

        }
    }

    
    protected void gvMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {       
        string key = gvMain.DataKeys[e.RowIndex].Values["ShiftCode"].ToString();

        BNS.TK.Entities.Shift shift = new BNS.TK.Entities.Shift();
        if (shift.LoadByPrimaryKey(_KioskSession.CompanyID, key.ToInt()))
        {
            shift.MarkAsDeleted();
            shift.Save();
        }

        gvMain.EditIndex = -1;
        BindGrid();
    }
    protected void gvMain_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMain.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void gvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string key = gvMain.DataKeys[e.RowIndex].Values["ShiftCode"].ToString();
        TextBox txtDescription = gvMain.Rows[e.RowIndex].FindControl("txtDescription") as TextBox;

        TextBox txtin1 = gvMain.Rows[e.RowIndex].FindControl("txtin1") as TextBox;
        TextBox txtout1 = gvMain.Rows[e.RowIndex].FindControl("txtout1") as TextBox;
        TextBox txtin2 = gvMain.Rows[e.RowIndex].FindControl("txtin2") as TextBox;
        TextBox txtout2 = gvMain.Rows[e.RowIndex].FindControl("txtout2") as TextBox;

        TextBox txtgracein1 = gvMain.Rows[e.RowIndex].FindControl("txtgracein1") as TextBox;
        //TextBox txtgracein2 = gvMain.Rows[e.RowIndex].FindControl("txtgracein2") as TextBox;
        TextBox txtminot = gvMain.Rows[e.RowIndex].FindControl("txtminot") as TextBox;
        
        CheckBox chkFlexi = gvMain.Rows[e.RowIndex].FindControl("chkFlexi") as CheckBox;
        TextBox txtFlexiin1 = gvMain.Rows[e.RowIndex].FindControl("txtFlexiin1") as TextBox;
       // TextBox txtFlexiin2 = gvMain.Rows[e.RowIndex].FindControl("txtFlexiin2") as TextBox;

      
        BNS.TK.Entities.Shift shift = new BNS.TK.Entities.Shift();
        if (hfIsAdd.Value == "1")
        {
            shift.AddNew();
            shift.CompanyID = _KioskSession.CompanyID;
            //shift.Shiftcode = key.ToInt();
            shift.CreatedBy = _KioskSession.UID;
            shift.CreatedDate = DateTime.Now;
        }
        else
        {
            shift.LoadByPrimaryKey(_KioskSession.CompanyID, key.ToInt());
        }

        shift.Advanced_in = false;
        shift.Description = txtDescription.Text;
        shift.Shortname = txtDescription.Text;
        shift.In1 = txtin1.Text;
        shift.Out1 = txtout1.Text;
        shift.In2 = txtin2.Text;
        shift.Out2 = txtout2.Text;
        shift.Grace_in1 = txtgracein1.Text.ToInteger();
        shift.Grace_in2 = 0; //txtgracein2.Text.ToInteger();
        shift.Flexi = chkFlexi.Checked;
        shift.Flexi_in1 = txtFlexiin1.Text.ToInteger();
        shift.Flexi_in2 = 0;    //txtFlexiin2.Text.ToInteger();
        shift.Minimumot = txtminot.Text.ToInteger();
        shift.Crossover = false;
        shift.LastUpdBy = _KioskSession.UID;
        shift.LastUpdDate = DateTime.Now;
        shift.Save();
        

        gvMain.EditIndex = -1;
        BindGrid();

    }

    private bool Upload(string excelFile,ref string message)
    {
        XLWorkbook workbook;
        IXLWorksheet ws;

        string header = "DESCRIPTION,IN1,OUT1,IN2,OUT2,FLEXI (YES/NO),MINIMUM OT (MINS),GRACE (MINS),FLEXI (MINS)";
        string companyID = "";
        string description = "";
        string in1;
        string out1;
        string in2;
        string out2;
        bool isFlexi;
        int minimumOT;
        int grace_mins;
        int flexi_mins;
        int row = 2;

        BNS.TK.Entities.Shift shift = new BNS.TK.Entities.Shift();
        shift.Where.CompanyID.Value = "STEPAN";
        shift.Query.Load("");


        try
        {
            workbook = new XLWorkbook(excelFile);
            ws = workbook.Worksheet(1);

            //validate template header
            if (!ValidateExcelHeader(ws, header))
            {

            }

            while (true)
            {
                companyID = ws.Cell(row, 1).GetString();
                description = ws.Cell(row, 1).GetString();
                if (companyID.IsNullOrEmpty() && description.IsNullOrEmpty())
                {
                    break;
                }
                                 
                in1 = ws.Cell(row, 1).GetString();
                out1 = ws.Cell(row, 1).GetString();
                in2 = ws.Cell(row, 1).GetString();
                out2 = ws.Cell(row, 1).GetString();
                isFlexi = ws.Cell(row, 1).GetString().ToBoolean();
                minimumOT = (int)ws.Cell(row, 1).Value;
                grace_mins = (int)ws.Cell(row, 1).Value;
                flexi_mins = (int)ws.Cell(row, 1).Value;

                //shift.AddNew();


                row += 1;
            }
            return true;
        }
        catch(Exception ex)
        {
            message = ex.Message;
            return false;
        }
        finally
        {
            workbook = null;
        }
    }

    private bool ValidateExcelHeader(IXLWorksheet ws, string templateHeader)
    {
        string[] hdr = templateHeader.Split(',');
        string s = null;
        for (int col = 1; col <= hdr.Length; col++)
        {
            s = ws.Cell(1, col).GetString();
            if (s.ToUpper().TrimEnd() != hdr[col - 1].ToUpper().TrimEnd())
            {
                return false;
            }
        }

        return true;
    }


    protected void lbNew_Click(object sender, EventArgs e)
    {
        BindGrid(true);
    }
    protected void ibNew_Click(object sender, ImageClickEventArgs e)
    {
        BindGrid(true);
    }
    
    protected void lbUpload_Click(object sender, EventArgs e)
    {
        mpe.Show();
    }


    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fuWorkfile.HasFile)
        {
            try
            {
                string message = "";
                string strFile = Server.MapPath("~/UploadedFiles/" + fuWorkfile.FileName);

                lblStatus.Text = "Uploading....";
                fuWorkfile.SaveAs(strFile);

                if (Upload(strFile, ref message))
                {
                    lblSummary.ForeColor = System.Drawing.Color.Black;
                    lblStatus.Text = "";
                    lblSummary.Text = "Uploading completed...";
                }
                else
                {
                    lblSummary.ForeColor = System.Drawing.Color.Red;
                    lblStatus.Text = "";
                    lblSummary.Text = message;
                }
            }
            catch //(Exception ex)
            {
                lblStatus.Text = "";
                lblSummary.ForeColor = System.Drawing.Color.Red;
                lblSummary.Text = "The file uploaded was not recognized by the system";
            }
        }
        else
        {
            lblStatus.Text = "";
            lblStatus.Text = "You have not specified a file.";
        }
    }
}