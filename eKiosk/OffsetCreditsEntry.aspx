﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" CodeFile="OffsetCreditsEntry.aspx.cs" Inherits="OffsetCreditsEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" Font-Underline="False" Text="Offset Credits Entry"></asp:Label><br />
                <br />
                <asp:Label ID="lblFullname" runat="server" Font-Names="Arial" Font-Size="Large" Text="Lastname, Firstname S."></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/Graphics/new.gif" OnClick="ibNew_Click"
        ToolTip="Add offset credits" /><asp:LinkButton ID="lbNew" runat="server"
            CssClass="ControlDefaults" Font-Bold="True" OnClick="lbNew_Click">Add offset credits</asp:LinkButton><br />

    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False"
                CssClass="DataGridStyle" OnRowDataBound="gvEmp_RowDataBound" PageSize="16" Width="100%" OnRowCancelingEdit="gvEmp_RowCancelingEdit" OnRowEditing="gvEmp_RowEditing" OnRowUpdating="gvEmp_RowUpdating">
                <Columns>
                
                    <asp:TemplateField HeaderText="Transaction Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTranDate" runat="server" CssClass="ControlDefaults" Text='<%# Bind("TranDate", "{0:MM/dd/yyyy}") %>'
                                Width="70px"></asp:TextBox>
                            <asp:ImageButton ID="ibtxtTranDate" runat="server" ImageUrl="~/Graphics/calendar.gif"
                                ToolTip="Click to choose date" /><ajaxToolkit:MaskedEditExtender ID="meeTranDate" runat="server"
                                    ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"
                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" TargetControlID="txtTranDate">
                                </ajaxToolkit:MaskedEditExtender>
                            <ajaxToolkit:CalendarExtender ID="caleTranDate" runat="server" PopupButtonID="ibtxtTranDate"
                                TargetControlID="txtTranDate">
                            </ajaxToolkit:CalendarExtender>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="litTranDate" runat="server" Text='<%# Bind("TranDate", "{0:MM/dd/yyyy}") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
        
                    <asp:TemplateField HeaderText="Earned (hours:minutes)" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblHours" runat="server" Text='<%# Eval("hours") %>'></asp:Label>hrs,
                            <asp:Label ID="lblMinutes" runat="server" Text='<%# Eval("minutes") %>'></asp:Label>mins
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtHours" runat="server" Text='<%# Bind("hours") %>' ValidationGroup="Trans" Width="30px" ToolTip="hours"></asp:TextBox>hrs
                            <asp:TextBox ID="txtMinutes" runat="server" Text='<%# Bind("minutes") %>' ValidationGroup="Trans" Width="30px" ToolTip="minutes"></asp:TextBox>mins
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Remarks">
                        <ItemStyle VerticalAlign="Top" Width="210px" />
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Bind("Remarks") %>' Width="200px" MaxLength="100"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemStyle VerticalAlign="Top" Width="42px" />
                        <ItemTemplate>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibUpdate" runat="server" CommandName="update" ToolTip="Update" ImageUrl="~/Graphics/update.GIF"
                                ValidationGroup="Trans" />
                            <asp:ImageButton ID="ibCancel" runat="server" CommandName="cancel" ToolTip="Cancel" ImageUrl="~/Graphics/cancel.GIF" />
                        </EditItemTemplate>
                    </asp:TemplateField>                    

                </Columns>                
                <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
                <HeaderStyle CssClass="DataGridHeaderStyle" />
            </asp:GridView>
            <asp:HiddenField ID="hfIsAdd" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibNew" />
            <asp:AsyncPostBackTrigger ControlID="lbNew" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

