<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="OTAuthList.aspx.cs" Inherits="OTAuthList" Title="Overtime Authorization Pending Lists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<script type="text/javascript" src="css/common_uno.js"></script>

    <asp:Label ID="lblModule" runat="server" Font-Size="Large" Text="My Overtime Authorizations"></asp:Label>
    <br />
    <br />
    <div style="width:100%; text-align:right">
        Status:
        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ControlDefaults" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
            <asp:ListItem></asp:ListItem>
            <asp:ListItem Selected="True">For Approval</asp:ListItem>
            <asp:ListItem>Approved</asp:ListItem>
            <asp:ListItem>Declined</asp:ListItem>
        </asp:DropDownList>
        &nbsp;&nbsp;&nbsp;
        Year:&nbsp;<asp:DropDownList ID="ddlYear" runat="server" Width="80px" AutoPostBack="true" onselectedindexchanged="ddlYear_SelectedIndexChanged"></asp:DropDownList>
    </div>
    <hr />
    <br />
    <asp:LinkButton ID="lnkApplyOT" runat="server" onclick="lnkApplyOT_Click" >Apply for OT</asp:LinkButton>
    <br />
    <br />
    <asp:GridView ID="gvLAppr" runat="server" AutoGenerateColumns="False" 
        CssClass="DataGridStyle" Width="100%" AllowSorting="true" 
        onsorting="gvLAppr_Sorting" OnRowDataBound="gvLAppr_RowDataBound">
        <Columns>
            <asp:BoundField DataField="DateFiled" HeaderText="Date filed" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="DateFiled"/>
            <asp:BoundField DataField="OTDate" HeaderText="OT date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" SortExpression="OTDate"/>
            
            <asp:BoundField DataField="OTStart" HeaderText="OT start" DataFormatString="{0:hh:mm tt}" HtmlEncode="False" />
            <asp:BoundField DataField="OTEnd" HeaderText="OT end" DataFormatString="{0:hh:mm tt}" HtmlEncode="False" />
            
            <asp:BoundField DataField="ApprovedOT" HeaderText="Approved hours" >
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Reason" HeaderText="Reason" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:BoundField DataField="Stage" HeaderText="" HtmlEncode="False" Visible="false" ></asp:BoundField>
             <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/Graphics/edit_small.gif" OnClick="ibEdit_Click" ToolTip="Edit" />
                    <asp:ImageButton ID="ibLocked" runat="server" ImageUrl="~/Graphics/locked.jpg" ToolTip="locked" Visible="false" />
                    <asp:HiddenField ID="hfID" runat="server" Value='<%# Bind("ID") %>' />
                </ItemTemplate>
                <ItemStyle Width="50px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            <span style="color:Red">No record found</span>
        </EmptyDataTemplate>

        <HeaderStyle CssClass="DataGridHeaderStyle" />
    </asp:GridView>
    <asp:Button ID="btnFU" runat="server" Text="Force Update" onclick="btnFU_Click" style="display:none" />
</asp:Content>

