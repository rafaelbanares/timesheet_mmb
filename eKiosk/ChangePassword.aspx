<%@ Page Language="C#" MasterPageFile="~/Attendance.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">
<table style="width:560px">
    <tr class="DataGridHeaderStyle">
        <td colspan="2">
            Change Password
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:RegularExpressionValidator
                ID="rvNew" runat="server" 
                Display="Dynamic"
                ErrorMessage="Password should be 6-12 characters" 
                ValidationExpression="^.{6,12}$" ControlToValidate="txtNewPwd" 
                ValidationGroup="trans">
            </asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator
                ID="rfv" runat="server"
                Display="Dynamic"
                ErrorMessage="New password cannot be blank"
                ControlToValidate="txtNewPwd" 
                ValidationGroup="trans"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="lblOldPwd" runat="server" Text="Old Password"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtOldPwd" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox></td>
    </tr>
   <tr>
        <td align="right">
            <asp:Label ID="lblNewPwd" runat="server" Text="New Password"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
        </td>   
   </tr>
   <tr>
        <td align="right">
            <asp:Label ID="lblRetype" runat="server" Text="Re-type New Password"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtRetype" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
        </td>   
   </tr>
   <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnOK" runat="server" OnClick="btnOK_Click" 
                Text="Change Password" ValidationGroup="trans" />
            &nbsp;
            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel"
                Width="65px" /></td>
   </tr>
</table>

    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br />
    <b>Note:</b>&nbsp; Choose a password that is at least six(6) characters long, it may 
    contain numbers (0-9) and upper and lowercase letters (A-Z, a-z). Make sure it 
    is difficult for others to guess!<br /><hr />
    <asp:ValidationSummary ID="vsum1" runat="server" ShowMessageBox="True" 
        ShowSummary="False" ValidationGroup="trans" />
<br />

<table style="width:560px">
    <tr class="DataGridHeaderStyle">
        <td colspan="2">
            Secret Question
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="lblPassword" runat="server" Text="Current Password"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="Dynamic" ControlToValidate="txtPassword" 
                ErrorMessage="Please enter your current password" ValidationGroup="trans2">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="lblNewSQ" runat="server" Text="New Secret Question"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtNewSQ" runat="server" MaxLength="500" Width="250px" autocomplete="off"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvNewSQ" runat="server" Display="Dynamic" ControlToValidate="txtNewSQ" 
                ErrorMessage="Please enter your secret question" ValidationGroup="trans2">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="lblAnswer" runat="server" Text="New Secret Answer"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtAnswer" runat="server" MaxLength="50" Width="250px" autocomplete="off"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAnswer" runat="server" Display="Dynamic" ControlToValidate="txtAnswer" 
                ErrorMessage="Please enter your answer" ValidationGroup="trans2">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="lblAnswer2" runat="server" Text="Re-enter New Secret Answer"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtAnswer2" runat="server" MaxLength="50" Width="250px" autocomplete="off"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnSQ" runat="server" Text="Change Secret Question" 
                ValidationGroup="trans2" onclick="btnSQ_Click" />
            &nbsp;
            <asp:Button ID="btnCancelSQ" runat="server" OnClick="btnCancel_Click" Text="Cancel"
                Width="65px" /></td>
    </tr>
    
</table>
<br />
    <asp:Label ID="lblErrorSQ" runat="server" ForeColor="Red"></asp:Label><br />

    <b>Note:</b>&nbsp; Choose a secret question that only you know the answer to, and 
    that has nothing to do with your password. If you forget your password, we&#39;ll 
    verify your identity by asking you for the answer to this question.<br />
    <br />
    For your security and convenience, make sure that the answer to your secret 
    question is:
    <ul>
        <li>At least five characters in length. </li>
        <li>Something only you know. </li>
        <li>Not related to your password or member name in any way. </li>
        <li>Unlikely to change over time. </li>
        <li>Extremely difficult for others to guess, even if they see your secret question.
            <br />
            <br />
            Some examples of good secret questions are: </li>
        <li>What are the last five digits of my Visa card? </li>
        <li>What are the last five digits of my social security number? </li>
        <li>What is my mother&#39;s maiden name? </li>
    </ul>

    <asp:ValidationSummary ID="vsTrans2" runat="server" ShowMessageBox="True" 
        ShowSummary="False" ValidationGroup="trans2" />


</asp:Content>

