using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

using Bns.AttendanceUI;
using Bns.AttUtils;
using BNS.TK.Entities;

public partial class Attendance_OTAuthEmployee : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            populateScreenDefaults();
            PopulateControls();
        }
    }


    private void populateScreenDefaults()
    {
        string today = Tools.ShortDate(DateTime.Now.ToShortDateString(), _KioskSession.DateFormat);
        txtDateFiled.Text = today;
        txtOTDate.Text = today;

        ddlReason.DataSource = GetOTReasons().Tables[0].DefaultView;
        ddlReason.DataValueField = "ID";
        ddlReason.DataTextField = "Description";
        ddlReason.DataBind();
    }

    private void PopulateControls()
    {
        if (isAddMode()) return;

        txtDateFiled.Enabled = false;
        txtOTDate.Enabled = false;
        ibtxtOTDate.Enabled = false;

        int id = _Url["id"].ToInteger();
        OTAuthorization otAuth = new OTAuthorization();
        if (otAuth.LoadByPrimaryKey(id))
        {
            if (otAuth.EmployeeID.TrimEnd() != _KioskSession.UID.TrimEnd())
            {
                Response.Redirect("LoginEmployee.aspx");
            }
                            
            txtDateFiled.Text = otAuth.s_DateFiled;
            txtOTDate.Text = otAuth.s_OTDate;
            txtin1.Text = otAuth.OTstart.ToString("hh:mm tt");
            txtout1.Text = otAuth.OTend.ToString("hh:mm tt");
            ddlReason.SelectedValue = otAuth.Reason.ToString();
            txtRemarks.Text = otAuth.Remarks;
        }

    }
    private bool isAddMode()
    {
        return (_Url["id"].IsNullOrEmpty()) ? true : false;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DateTime dt;
        if (!DateTime.TryParse(txtOTDate.Text, out dt))
        {
            lblError.Text = "Invalid overtime date.";
            return;
        }
        if (DateLockedForEmployee(DateTime.Parse(txtOTDate.Text)))
        {
            lblError.Text = "Date of overtime was already locked.";
            return;
        }

        var att = new Attendance();
        att.Where.CompanyID.Value = _KioskSession.CompanyID;
        att.Where.EmployeeID.Value = _KioskSession.EmployeeID;
        att.Where.Date1.Value = txtOTDate.Text;
        att.Query.Load();

        var tbl = att.DefaultView.Table;

        //if (tbl.Rows.Count == 0)
        //{
        //    lblError.Text = "Current date has no renderred hours.";
        //    return;
        //}

        if (tbl.Rows.Count > 0)
        {
            var row = tbl.Rows[0];
            var pendingIn = DateTime.Parse(txtin1.Text);
            var pendingOut = DateTime.Parse(txtout1.Text);
            var actualIn = DateTime.Now.Date + ((DateTime)row["in1"]).TimeOfDay;
            var actualOut = DateTime.Now.Date + ((DateTime)row["out1"]).TimeOfDay;

            if (pendingIn > pendingOut)
                pendingIn = pendingIn.AddDays(-1);

            if (actualIn > actualOut)
                actualIn = actualIn.AddDays(-1);

            if (pendingIn < actualIn || pendingOut > actualOut)
            {
                lblError.Text = "Overtime is beyond rederred hours.";
                return;
            }
        }

        if (Save())
        {
            Response.Redirect("OTAuthList.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTAuthList.aspx");
    }

    private bool ValidateEditInputs()
    {
        if (txtin1.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter start date');", true);
            return false;
        }
        if (txtout1.Text.IsNullOrEmpty())
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please enter end date');", true);
            return false;
        }
        if (txtRemarks.Text.ToUpper().Contains("PERSONAL MATTER"))
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertrmk", "alert('Please give detailed reason in remarks.');", true);
            return false;
        }

        return true;
    }

    private bool Save()
    {
        if (!ValidateEditInputs()) return false;

        DateTime in1;
        DateTime out1;
        string othours = "0.00";

        in1 = DateTime.Parse(txtOTDate.Text + " " + txtin1.Text);
        out1 = DateTime.Parse(txtOTDate.Text + " " + txtout1.Text);

        if (in1 > out1)
            out1 = out1.AddHours(24);

        if (txtOTDate.Text.Length > 0 && txtin1.Text.Length > 0 && txtout1.Text.Length > 0)
        {
            TimeSpan ts = out1.Subtract(in1);
            othours = Tools.TimeToDecimalRounded(ts.Hours, ts.Minutes);
        }

        OTAuthorization otAuth = new OTAuthorization();
        try
        {
            if (isAddMode())
            {
                otAuth.AddNew();
                otAuth.CompanyID = _KioskSession.CompanyID;
                otAuth.EmployeeID = _KioskSession.EmployeeID;
                otAuth.DateFiled = DateTime.Today;    //txtDateFiled.Text;
                otAuth.s_OTDate = txtOTDate.Text;
                otAuth.OTstart = in1;
                otAuth.OTend = out1;
                otAuth.Status = "For Approval";
                otAuth.Reason = ddlReason.SelectedValue.ToInteger();
                otAuth.ApprovedOT = Convert.ToDecimal(othours);
                otAuth.Remarks = txtRemarks.Text;
                otAuth.Stage = 0;
                otAuth.AuthBy = "0";
                otAuth.Posted = false;
                otAuth.CreatedBy = _KioskSession.UID;
                otAuth.CreatedDate = DateTime.Now;
                otAuth.LastUpdBy = _KioskSession.UID;
                otAuth.LastUpdDate = DateTime.Now;
            }
            else
            {
                if (otAuth.LoadByPrimaryKey(_Url["id"].ToInteger()) && otAuth.Status.TrimEnd() == "For Approval")
                {
                    in1 = DateTime.Parse(otAuth.OTDate.ToString("MM/dd/yyyy") + " " + txtin1.Text);
                    out1 = DateTime.Parse(otAuth.OTDate.ToString("MM/dd/yyyy") + " " + txtout1.Text);
                    if (in1 > out1)
                        out1 = out1.AddHours(24);

                    otAuth.OTstart = in1;
                    otAuth.OTend = out1;
                    otAuth.Reason = ddlReason.SelectedValue.ToInteger();
                    otAuth.ApprovedOT = Convert.ToDecimal(othours);
                    otAuth.Remarks = txtRemarks.Text;
                    otAuth.LastUpdBy = _KioskSession.UID;
                    otAuth.LastUpdDate = DateTime.Now;
                }
                else
                {
                    throw new Exception("OT not found!");
                }
            }                        
            
            otAuth.Save();
        }
        catch (SqlException sqex)
        {
            if (sqex.Number == 2601)  //'Unique Key violation
                lblError.Text = "Overtime already exists";
            else
            {
                lblError.Text = sqex.Message;
            }                
            return false;
        }
        catch(Exception ex_)
        {
            lblError.Text = ex_.Message;
            return false;
        }

        //send email
       // if (isAddMode())
            //Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos for approval", "You have a pending overtime for approval.");
     
        return true;
    }

    //private bool OTAuthInsertXXXX(string status)
    //{
    //    DateTime in1;
    //    DateTime out1;
    //    string othours = "0.00";

    //    in1 = DateTime.Parse(txtOTDate.Text + " " + txtin1.Text);
    //    out1 = DateTime.Parse(txtOTDate.Text + " " + txtout1.Text);

    //    if (in1 > out1)
    //        out1 = out1.AddHours(24);

    //    if (txtOTDate.Text.Length > 0 && txtin1.Text.Length > 0 && txtout1.Text.Length > 0)
    //    {
    //        TimeSpan ts = out1.Subtract(in1);
    //        othours = Tools.TimeToDecimalRounded(ts.Hours, ts.Minutes);
    //    }
        
    //    List<SqlParameter> sqlparam = new List<SqlParameter>();
    //    sqlparam.Add(new SqlParameter("@DBname", _KioskSession.DB));
    //    sqlparam.Add(new SqlParameter("@CompanyID", _KioskSession.CompanyID));
    //    sqlparam.Add(new SqlParameter("@EmployeeID", _KioskSession.EmployeeID));

    //    sqlparam.Add(new SqlParameter("@OTDate", txtOTDate.Text));
    //    sqlparam.Add(new SqlParameter("@OTstart", in1));
    //    sqlparam.Add(new SqlParameter("@OTend", out1));

    //    string ErrMsg = SqlHelper.ExecuteDataset(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthValidate", sqlparam.ToArray()).Tables[0].Rows[0][0].ToString();

    //    if (ErrMsg != "")
    //    {
    //        lblError.Text = ErrMsg;
    //        return false;
    //    }
    //    else
    //    {
    //        //sqlparam.Add(new SqlParameter("@DateFiled", txtDateFiled.Text));
    //        //sqlparam.Add(new SqlParameter("@ApprovedOT", othours));
    //        //sqlparam.Add(new SqlParameter("@Reason", txtRemarks.Text));
    //        //sqlparam.Add(new SqlParameter("@Status", status));
    //        //sqlparam.Add(new SqlParameter("@Stage", "0"));
    //        //sqlparam.Add(new SqlParameter("@AuthBy", _KioskSession.OTApproverID));

    //        //sqlparam.Add(new SqlParameter("@CreatedBy", _KioskSession.UID));
    //        //sqlparam.Add(new SqlParameter("@CreatedDate", DateTime.Now));
    //        //sqlparam.Add(new SqlParameter("@LastUpdBy", _KioskSession.UID));
    //        //sqlparam.Add(new SqlParameter("@LastUpdDate", DateTime.Now));

    //        //SqlHelper.ExecuteNonQuery(_ConnectionString, CommandType.StoredProcedure, MainDB + ".dbo.usa_OTAuthInsert", sqlparam.ToArray());

    //        OTAuthorization otAuth = new OTAuthorization();
    //        otAuth.AddNew();
    //        otAuth.CompanyID = "";
    //        otAuth.EmployeeID = "";
    //        otAuth.s_OTDate = "";
    //        otAuth.s_OTstart = "";
    //        otAuth.s_OTend = "";

    //        //send email
    //        Helper.SendMail(Helper.GetApproverEmailAddress(_KioskSession.CompanyID, _KioskSession.EmployeeID), "Kronos for approval", "You have a pending overtime for approval.");            
    //    }
    //    return true;
    //}

    private bool DateLockedForEmployee(DateTime date)
    {
        TransPeriod tp = GetTransPeriod(date);
        tp.Filter = "StartDate <= '" + date.ToShortDateString() + "' AND EndDate >= '" + date.ToShortDateString() + "'";
        if (tp.RowCount > 0)
        {
            if (tp.s_Employee_lock_start != "" && tp.s_Employee_lock_end != "")
            {
                return (DateTime.Today >= tp.Employee_lock_start && DateTime.Today <= tp.Employee_lock_end);
            }
        }
        return false;
    }

    private TransPeriod GetTransPeriod(DateTime date)
    {
        if (Cache["cacheLockingInfo"] == null)
        {
            TransPeriod tp = TransPeriod.GetLockInfo(_KioskSession.CompanyID);
            Cache["cacheLockingInfo"] = tp;
            return tp;
        }
        else
        {
            return (TransPeriod)Cache["cacheLockingInfo"];
        }
    }

    private DataSet GetOTReasons()
    {        
        ExcusedTardyUT xqs = new ExcusedTardyUT();
        string sql = "select ID, [Description] from OTReason where companyid = {0} order by [Description] ";

        object[] parameters = { _KioskSession.CompanyID };
        xqs.DynamicQuery(sql, parameters);

        return xqs.ToDataSet(); ;
    }


}
