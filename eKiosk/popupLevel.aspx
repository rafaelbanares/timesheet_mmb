<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true"
    CodeFile="popupLevel.aspx.cs" Inherits="popupLevel" Title="MMB Kronos - Select Groupings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" runat="Server">

    <script type="text/javascript">
        function updateparent(ids, descs, url)
        {
            window.opener.updatelevels(ids.value, descs.value, url.value );
            //alert(url.value);
        }
    </script>

    <asp:Label ID="lblModule" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium"
        Font-Underline="False" Text="Level Selection"></asp:Label><br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tbody>
                    <tr style="display:none">
                        <td width="150">
                            <asp:Label ID="lblLevel1" runat="server" Enabled="False"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLevel1" runat="server" Enabled="False" DataValueField="Code"
                                DataTextField="Description" OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged"
                                Width="350px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLevel2" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLevel2" runat="server" DataValueField="Code" DataTextField="Description"
                                OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged" Width="350px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLevel3" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLevel3" runat="server" DataValueField="Code" DataTextField="Description"
                                OnSelectedIndexChanged="ddlLevel3_SelectedIndexChanged" Width="350px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLevel4" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLevel4" runat="server" DataValueField="Code" DataTextField="Description"
                                OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" Width="350px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                            <asp:HiddenField ID="hfLevels" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfDesc" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfURL" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnOK" runat="server" Text="OK" Width="60"></asp:Button>
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60" OnClientClick="window.close();return false;">
                            </asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
