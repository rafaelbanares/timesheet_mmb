using Bns.AttendanceUI;
using BNS.TK.Business;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

public partial class AdminSummarizeAttendance : KioskPageUI
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.GetUserSession();
        if (!IsPostBack)
        {
            var tblPeriod = SqlHelper.ExecuteDataset(_ConnectionString2, CommandType.StoredProcedure,
                    "dbo.usa_AttendanceSummarizeGetPeriod", new SqlParameter[] {
                                new SqlParameter("@DBName", _KioskSession.DB)
                                ,new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                                }).Tables[0];

            if (tblPeriod.Rows.Count > 0)
            {
                var drPeriod = tblPeriod.Rows[0];

                //rowID, StartDate, EndDate, PayStart, PayEnd, Posted, Finalized
                txtTranStart.Text = ((DateTime)drPeriod["StartDate"]).ToString(_KioskSession.DateFormat);
                txtTranEnd.Text = ((DateTime)drPeriod["EndDate"]).ToString(_KioskSession.DateFormat);
                txtPayStart.Text = ((DateTime)drPeriod["PayStart"]).ToString(_KioskSession.DateFormat);
                txtPayEnd.Text = ((DateTime)drPeriod["PayEnd"]).ToString(_KioskSession.DateFormat);

                hfRowID.Value = drPeriod["rowID"].ToString();
            }

        }
    }

    private bool ReprocessAttendance()
    {        
        ProcessAttend proc = new ProcessAttend(_KioskSession.CompanyID, _KioskSession.UID);

        DateTime tranStart;
        DateTime tranEnd;

        if (DateTime.TryParseExact(txtTranStart.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tranStart) 
            && DateTime.TryParseExact(txtTranEnd.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tranEnd))
        {
            proc.Process(tranStart, tranEnd, "", true);
            return true;
        }

        return false;
        //proc.Process(DateTime.Parse(txtTranStart.Text), DateTime.Parse(txtTranEnd.Text), "");
    }

    private void Summarize(bool forceSummarize)
    {
        //process attendance to make sure
        lblMessage.Text = "";
        lblMessage.ForeColor = System.Drawing.Color.Red;
        //btnForceSummarize.Visible = false ;

        if (!ReprocessAttendance())
        {
            lblMessage.Text = "Invalid transaction date format.";
            return;
        }

        DataSet dsCheck = SqlHelper.ExecuteDataset(_ConnectionString2, CommandType.StoredProcedure,
                                "dbo.usa_AttendanceSummarizeCheck", new SqlParameter[] {
                                new SqlParameter("@CompanyID", _KioskSession.CompanyID)
                                ,new SqlParameter("@PayStart",  txtPayStart.Text)
                                ,new SqlParameter("@PayEnd",    txtPayEnd.Text)
                                ,new SqlParameter("@TransStart",txtTranStart.Text )
                                ,new SqlParameter("@TransEnd",  txtTranEnd.Text)
                                });

        if (dsCheck.Tables[0].Rows[0]["posted"].ToString().ToInt() > 0)
        {
            lblMessage.Text = "Attendance period was already posted, please check.";
        }

        if (!forceSummarize)
        {
            if (dsCheck.Tables[1].Rows[0]["invalid"].ToString().ToInt() > 0)
            {
                lblMessage.Text = "There are still some erroneous DTR transactions, Please correct attendance first before posting.";
            }
            else if (dsCheck.Tables[2].Rows[0]["dtr_fa"].ToString().ToInt() > 0)
            {
                lblMessage.Text = "There are still some DTR transactions for approval, Please approve or decline DTR before posting.";
            }
            else if (dsCheck.Tables[3].Rows[0]["leave_fa"].ToString().ToInt() > 0)
            {
                lblMessage.Text = "There are still some Leave applications for approval, Please approve or decline Leave before posting.";
            }
            else if (dsCheck.Tables[4].Rows[0]["cshift_fa"].ToString().ToInt() > 0)
            {
                lblMessage.Text = "There are still some Change of Shift applications for approval, Please approve or decline Shift before posting.";
            }
            else if (dsCheck.Tables[5].Rows[0]["CRestday_fa"].ToString().ToInt() > 0)
            {
                lblMessage.Text = "There are still some Change of Restday applications for approval, Please approve or decline Restday before posting.";
            }
            else if (dsCheck.Tables[6].Rows[0]["otauthor_fa"].ToString().ToInt() > 0)
            {
                lblMessage.Text = "There are still some Overtime applications for approval, Please approve or decline Overtime before posting.";
            }
        }
        
        if (lblMessage.Text == "")
        {
             
            //... Summarize current attendance
            SqlHelper.ExecuteNonQuery(_ConnectionString2, CommandType.StoredProcedure,
                                "dbo.usa_AttendanceSummarize", new SqlParameter[] {
                                new SqlParameter("@CompanyID",     _KioskSession.CompanyID)
                                ,new SqlParameter("@PayStart",      txtPayStart.Text)
                                ,new SqlParameter("@PayEnd",        txtPayEnd.Text)
                                ,new SqlParameter("@TransStart",    txtTranStart.Text )
                                ,new SqlParameter("@TransEnd",      txtTranEnd.Text)
                                ,new SqlParameter("@CreatedBy",     _KioskSession.UID)
                                ,new SqlParameter("@CreatedDate",   DateTime.Now)
                                ,new SqlParameter("@LastUpdBy",     _KioskSession.UID)
                                ,new SqlParameter("@LastUpDate",    DateTime.Now)
                            });
            lblMessage.ForeColor = System.Drawing.Color.Black;
            lblMessage.Text = "Summarized completed.";
            btnForceSummarize.Enabled = false;
            btnSummarize.Enabled = false;
        }
    }
     

    protected void btnSummarize_Click(object sender, EventArgs e)
    {
        Summarize(false);        
    }

    protected void btnForceSummarize_Click(object sender, EventArgs e)
    {
        Summarize(true);
    }

}
