﻿<%@ Page Language="C#" MasterPageFile="~/AttendanceBlank.master" AutoEventWireup="true" 
    CodeFile="popupApprover.aspx.cs" Inherits="popupApprover" Title="MMB Kronos - Select Approver" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phmc" Runat="Server">

<script type="text/javascript">
function load() {
   Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
}

function EndRequestHandler(sender, args) {
   if (args.get_error() == undefined)
    {
        var openerEmpClientID  = '<%=_Url["hfID"]%>';
        var openerNameClientID = '<%=_Url["lbID"]%>';
        var openerCompanyClientID = '<%=_Url["compID"]%>';

        var openerEmpValue = $get('<%=hfEmpID.ClientID%>').value;
        var openerNameValue = $get('<%=hfName.ClientID%>').value;
        var openerCompanyValue = $get("<%=hfCompanyID.ClientID%>").value;
        
        if (openerEmpValue)
        {
            //alert(openerEmpValue);
            window.opener.updatevalues(openerEmpClientID, openerNameClientID, openerEmpValue, openerNameValue, openerCompanyClientID, openerCompanyValue); 
        }
    }
   else
       alert('There was an error' + args.get_error().message);
}
</script>

<div style="width: 1000px">
    <table width="100%">
        <tr>
            <td align="right" style="width: 80px">
                <asp:Label ID="lblSearch" runat="server" CssClass="ControlDefaults" Text="Search:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" CssClass="ControlDefaults"
                    OnTextChanged="txtSearch_TextChanged" ValidationGroup="emplist" Width="191px"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ibSearch" ToolTip="Search" runat="server" ImageUrl="~/Graphics/go.gif" OnClick="ibGo_Click"
                        ValidationGroup="emplist" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="updEmp" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:GridView id="gvEmp" runat="server" CssClass="DataGridStyle" Width="100%"
                 AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanging="gvEmp_PageIndexChanging" 
                 OnRowDataBound="gvEmp_RowDataBound" OnSorting="gvEmp_Sorting" 
                PageSize="20" onselectedindexchanged="gvEmp_SelectedIndexChanged" 
                DataKeyNames="ApproverID,ApproverName,ApprCompID">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibSelect" runat="server" ValidationGroup="emplist" 
                                ImageUrl="~/Graphics/select.gif" ToolTip="select" CommandName="select"></asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approver #1" SortExpression="ApproverName">
                        <ItemTemplate>
                            <asp:Literal ID="litAppr" runat="server" Text='<%# Bind("Approver1Name") %>'></asp:Literal>
                            <asp:HiddenField ID="hfApprCompID" runat="server" Value='<%# Bind("ApprCompID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approver #2" SortExpression="Alternative1Name">
                        <ItemTemplate>
                            <asp:Literal ID="litAlt1" runat="server" Text='<%# Bind("Alternative1Name") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approver #3" SortExpression="Alternative2Name">
                        <ItemTemplate>
                            <asp:Literal ID="litAlt2" runat="server" Text='<%# Bind("Alternative2Name") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approver #4" SortExpression="Alternative3Name">
                        <ItemTemplate>
                            <asp:Literal ID="litAlt3" runat="server" Text='<%# Bind("Alternative3Name") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle HorizontalAlign="Right"></PagerStyle>
                <EmptyDataTemplate>
                    <span style="color:Red">No record found</span>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy"></SelectedRowStyle>
                <HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
            </asp:GridView>
            &nbsp;&nbsp;
            <asp:HiddenField ID="hfEmpID" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfName" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfCompanyID" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfFunc" runat="server"></asp:HiddenField>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

</asp:Content>
